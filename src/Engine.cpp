﻿#define __ENABLE_D2D 1

#include <DD/Simulation/Engine.h>
#include <DD/Dialog.h>
#include <DD/Profiler.h>
#include <DD/Simulation/ICullableModel.h>
#include <DD/Simulation/ProgressBarSprite.h>
#include <DD/Simulation/QuadModel.h>
#include <DD/SortedList.h>
#include <DD/Sequencer/Iterable2.h>

#include <DD/Gpu/VertexBuffer.h>
#include <DD/Simulation/CompassSprite.h>
#include <DD/Simulation/EmptyVertex.h>
#include <DD/Simulation/StatisticSprite.h>
#include <DD/Simulation/TextureVertex.h>

#include <initializer_list>

DD_TODO_STAMP("Keyboard input - take it form window, instead of async reading.")
DD_TODO_STAMP("Add moving camera depence on delta times.")
DD_TODO_STAMP("Prepare scene - async into the tasks.")
DD_TODO_STAMP("Separation of scene preparation and drawing via command buffer");
DD_TODO_STAMP(
	"Consider using engine namespace. Refactor Engine -> Engine::Core."
	"Then all the objects like dialogs and messages could be separated into its own files"
);
DD_TODO_STAMP("Engine should be referential object.")
DD_TODO_STAMP("Do not register invalid models as zero item buffers...")
DD_TODO_STAMP("Remove QuadModel inclusion.")

using namespace DD;
using namespace DD::Simulation;
using namespace DD::Input;

/************************************************************************/
/* Helpers                                                              */
/************************************************************************/
#pragma region Helpers

template<>
struct SortedListTraitsCompare<Debug::Category> {
	/** Compare function for sorted list. */
	static bool Compare(const Debug::Category & l, const Debug::Category & r) {
		return l->Name() < r->Name();
	}
};

template<>
struct SortedListTraitsCompare<Debug::Variable> {
	/** Compare function for sorted list. */
	static bool Compare(const Debug::Variable & l, const Debug::Variable & r) {
		return l->Category() < r->Category() || l->Name() < r->Name();
	}
};
#pragma endregion

/************************************************************************/
/* EngineConsole                                                        */
/************************************************************************/
#pragma region EngineConsole
/** Console browser for engine. */
struct EngineConsoleImpl
	: public KeyboardEventProcessor::IKeyboardEvent
{
	static void Range(int count, int page, int selected, size_t & begin, size_t & end);
	static void Left(size_t strSize, size_t whiteSize, size_t & lPadding, size_t & rPadding);
	static void Center(size_t strSize, size_t whiteSize, size_t & lPadding, size_t & rPadding);

protected: // properties
	/** State of console browser. */
	enum State { Console, Category, Variable, Search } _state = Console;
	/** History of states. */
	List<State, 16> _stateHistory;
	/** Index of selected category/variable in _categories/_variables. */
	size_t _selectedCategory, _selectedVariable, _selectedSearch;
	/** Text of input. */
	StringLocal<64> _input, _searchInput;
	/** Cursor of input. */
	size_t _inputCursor, _searchCursor;
	/** Categories of console. */
	SortedList<Debug::Category, 128> _categories;
	/** Variables of currently selected category. */
	SortedList<Debug::Variable, 128> _variables;
	/** Variables selected by search. */
	SortedList<Debug::Variable, 256> _searchList;
	/** Process just added category. */
	Delegate<Debug::Category> _onCategoryAdd;
	/** Process for enumeration of categories. */
	Lambda<void(Debug::ICategoryBase *)> _onCategoryEnum, _searchCategory;
	/** Process just added variable. */
	Delegate<Debug::Variable> _onVariableAdd;
	/** Process for enumeration of variables. */
	Lambda<void(Debug::IVariableBase *)> _onVariableEnum, _searchVariable;
	/** Number of items on a page. */
	Debug::i32Range _page;
	/** Variable lsited if State::Variable is set. */
	Debug::Variable _variable;

public: // iKeyboardEvent
	virtual bool iKeyboardEventIsEnabled() const override { return true;}
	virtual void iKeyboardEventProcess(Input::Key key, Input::KeyModifiers modifiers) override;

protected: // methods
	/** Change state. */
	void state(State s) { _stateHistory.Add(_state); _state = s; }
	/** Go to previous state. */
	void stateBack() { if (_stateHistory.IsNotEmpty()) _state = _stateHistory.Last(), _stateHistory.RemoveLast(); }
	/** Convert input into value for currently selected variable.. */
	void setVariable();
	/** Process keyboard input if State::Console is set. */
	void processConsole(Input::Key, Input::KeyModifiers);
	/** Process keyboard input if State::Category is set. */
	void processCategory(Input::Key, Input::KeyModifiers);
	/** Process keyboard input if State::Variable is set. */
	void processVariable(Input::Key, Input::KeyModifiers);
	/** Process keyboard input if State::Search is set. */
	void processSearch(Input::Key, Input::KeyModifiers);
	/** Fill a buffer by text if State::Console is set. */
	void feedConsole(Gpu::D2D::Strings &);
	/** Fill a buffer by text if State::Category is set. */
	void feedCategory(Gpu::D2D::Strings &);
	/** Fill a buffer by text if State::Variable is set. */
	void feedVariable(Gpu::D2D::Strings &);
	/** Fill a buffer by text if State::Search is set. */
	void feedSearch(Gpu::D2D::Strings &);

public: // methods
	/** Fill the text into buffer according to current browser state. */
	void FeedText(Gpu::D2D::Strings &);

public: // constructors
	/** Constructor. */
	EngineConsoleImpl(Debug::i32Range & page);
};

void EngineConsoleImpl::Range(int count, int page, int selected, size_t & begin, size_t & end) {
	// estimate begin and end
	int ebegin = selected - (page / 2);
	int eend = ebegin + page;
	// fix begin and end to keep constant size
	if (ebegin < 0) {
		ebegin = 0;
		eend = Math::Min(ebegin + page, count);
	}
	if (count < eend) {
		eend = count;
		ebegin = Math::Max(eend - page, 0);
	}
	// parse type
	begin = (size_t)ebegin;
	end = (size_t)eend;
}

void EngineConsoleImpl::Center(size_t strSize, size_t whiteSize, size_t & lPadding, size_t & rPadding) {
	if (strSize < whiteSize) {
		size_t rest = whiteSize - strSize;
		lPadding = rest / 2;
		rPadding = rest - lPadding;
	}
	else {
		lPadding = rPadding = 0;
	}
}

void EngineConsoleImpl::Left(size_t strSize, size_t whiteSize, size_t & lPadding, size_t & rPadding) {
	if (strSize < whiteSize) {
		size_t rest = whiteSize - strSize;
		rPadding = rest - 1;
		lPadding = 1;
	}
	else {
		lPadding = rPadding = 0;
	}
}

void EngineConsoleImpl::iKeyboardEventProcess(Key key, KeyModifiers modifiers) {
	switch (_state) {
		case State::Console:  processConsole(key, modifiers);  break;
		case State::Category: processCategory(key, modifiers); break;
		case State::Variable: processVariable(key, modifiers); break;
		case State::Search:   processSearch(key, modifiers);   break;
	}
}

void EngineConsoleImpl::setVariable() {
	Debug::VariableTypes type = _variable->Type();
	switch (type) {
		case Debug::VariableTypes::i8:      {  i8 v; if (_input.TryCast(v)) reinterpret_cast<Debug::      i8&>(_variable)->Value(v, Debug::Sources::ENGINE); } break;
		case Debug::VariableTypes::i8Range: {  i8 v; if (_input.TryCast(v)) reinterpret_cast<Debug:: i8Range&>(_variable)->Value(v, Debug::Sources::ENGINE); } break;
		case Debug::VariableTypes::i16:     { i16 v; if (_input.TryCast(v)) reinterpret_cast<Debug::     i16&>(_variable)->Value(v, Debug::Sources::ENGINE); } break;
		case Debug::VariableTypes::i16Range:{ i16 v; if (_input.TryCast(v)) reinterpret_cast<Debug::i16Range&>(_variable)->Value(v, Debug::Sources::ENGINE); } break;
		case Debug::VariableTypes::i32:     { i32 v; if (_input.TryCast(v)) reinterpret_cast<Debug::     i32&>(_variable)->Value(v, Debug::Sources::ENGINE); } break;
		case Debug::VariableTypes::i32Range:{ i32 v; if (_input.TryCast(v)) reinterpret_cast<Debug::i32Range&>(_variable)->Value(v, Debug::Sources::ENGINE); } break;
		case Debug::VariableTypes::i64:     { i64 v; if (_input.TryCast(v)) reinterpret_cast<Debug::     i64&>(_variable)->Value(v, Debug::Sources::ENGINE); } break;
		case Debug::VariableTypes::i64Range:{ i64 v; if (_input.TryCast(v)) reinterpret_cast<Debug::i64Range&>(_variable)->Value(v, Debug::Sources::ENGINE); } break;
		case Debug::VariableTypes::u8:      {  u8 v; if (_input.TryCast(v)) reinterpret_cast<Debug::      u8&>(_variable)->Value(v, Debug::Sources::ENGINE); } break;
		case Debug::VariableTypes::u8Range: {  u8 v; if (_input.TryCast(v)) reinterpret_cast<Debug:: u8Range&>(_variable)->Value(v, Debug::Sources::ENGINE); } break;
		case Debug::VariableTypes::u16:     { u16 v; if (_input.TryCast(v)) reinterpret_cast<Debug::     u16&>(_variable)->Value(v, Debug::Sources::ENGINE); } break;
		case Debug::VariableTypes::u16Range:{ u16 v; if (_input.TryCast(v)) reinterpret_cast<Debug::u16Range&>(_variable)->Value(v, Debug::Sources::ENGINE); } break;
		case Debug::VariableTypes::u32:     { u32 v; if (_input.TryCast(v)) reinterpret_cast<Debug::     u32&>(_variable)->Value(v, Debug::Sources::ENGINE); } break;
		case Debug::VariableTypes::u32Range:{ u32 v; if (_input.TryCast(v)) reinterpret_cast<Debug::u32Range&>(_variable)->Value(v, Debug::Sources::ENGINE); } break;
		case Debug::VariableTypes::u64:     { u64 v; if (_input.TryCast(v)) reinterpret_cast<Debug::     u64&>(_variable)->Value(v, Debug::Sources::ENGINE); } break;
		case Debug::VariableTypes::u64Range:{ u64 v; if (_input.TryCast(v)) reinterpret_cast<Debug::u64Range&>(_variable)->Value(v, Debug::Sources::ENGINE); } break;
		case Debug::VariableTypes::f32:     { f32 v; if (_input.TryCast(v)) reinterpret_cast<Debug::     f32&>(_variable)->Value(v, Debug::Sources::ENGINE); } break;
		case Debug::VariableTypes::f32Range:{ f32 v; if (_input.TryCast(v)) reinterpret_cast<Debug::f32Range&>(_variable)->Value(v, Debug::Sources::ENGINE); } break;
		case Debug::VariableTypes::f64:     { f64 v; if (_input.TryCast(v)) reinterpret_cast<Debug::     f64&>(_variable)->Value(v, Debug::Sources::ENGINE); } break;
		case Debug::VariableTypes::f64Range:{ f64 v; if (_input.TryCast(v)) reinterpret_cast<Debug::f64Range&>(_variable)->Value(v, Debug::Sources::ENGINE); } break;
		case Debug::VariableTypes::Boolean: { bool v; if (_input.TryCast(v)) reinterpret_cast<Debug::Boolean&>(_variable)->Value(v, Debug::Sources::ENGINE); } break;
		case Debug::VariableTypes::String:  { reinterpret_cast<Debug::String&>(_variable)->Value(_input, Debug::Sources::ENGINE); } break;
		case Debug::VariableTypes::Button:  { reinterpret_cast<Debug::Button&>(_variable)->Press(Debug::Sources::ENGINE); } break;
		case Debug::VariableTypes::Path: { reinterpret_cast<Debug::Path&>(_variable)->Value(_input, Debug::Sources::ENGINE); } break;
		case Debug::VariableTypes::Options: { i32 v; if (_input.TryCast(v)) reinterpret_cast<Debug::Options&>(_variable)->Index(v, Debug::Sources::ENGINE); } break;
		case Debug::VariableTypes::Enum: {
			u32 v;
			if (_input.TryCast(v)) {
				Debug::IEnum & e = reinterpret_cast<Debug::IEnum&>(_variable);
				e->NativeValue(e->Descriptor()->Index2Value(v), Debug::Sources::ENGINE);
			}
		} break;
		default:
			break;

	}
}

void EngineConsoleImpl::processConsole(Key key, KeyModifiers modifiers) {
	switch (key) {
		case Key::Down: {   // move down
			if (_selectedCategory + 1 != _categories.Size())
				_selectedCategory = (_selectedCategory + 1);
		} break;
		case Key::Up: {     // move up
			if (_selectedCategory)
				_selectedCategory = _selectedCategory - 1;
		} break;
		case Key::PgDown: { // move down by page size
			if (_selectedCategory + _page / 2 < _categories.Size())
				_selectedCategory = _selectedCategory + _page / 2;
			else
				_selectedCategory = _categories.Size();
		} break;
		case Key::PgUp: {   // move up by page size
			if (_selectedCategory < _page / 2)
				_selectedCategory = 0;
			else
				_selectedCategory = _selectedCategory - _page / 2;
		} break;
		case Key::Home: {   // move to the beginning
			_selectedCategory = 0;
		} break;
		case Key::End: {    // move to the end
			_selectedCategory = _categories.Size() - 1;
		} break;
		case Key::Enter: {  // enter the category browsing
			Debug::Variables()->Lock();
			Debug::Category & category = _categories[_selectedCategory];
			category->Enumerate(_onVariableEnum);
			category->OnVariableCreate += _onVariableAdd;
			state(State::Category);
			_selectedVariable = 0;
			Debug::Variables()->UnLock();
		} break;
		default: {
			wchar_t c = Keyboard::Key2Char(key, modifiers);
			if (c) {
				_searchCursor = 1;
				_searchInput.Clear().Append(c);
				state(State::Search);
			}
		} break;
	}
}

void EngineConsoleImpl::processCategory(Key key, KeyModifiers modifiers) {
	switch (key) {
		case Key::Esc:    // Escape to the console view
			Debug::Variables()->Lock();
			_categories[_selectedCategory]->OnVariableCreate -= _onVariableAdd;
			_variables.Free();
			stateBack();
			Debug::Variables()->UnLock();
			break;
		case Key::Down:   // increase counter if it is not at the end
			if (_selectedVariable + 1 != _variables.Size())
				_selectedVariable = (_selectedVariable + 1);
			break;
		case Key::Up:     // decrease counter if it is not at the beginning
			if (_selectedVariable)
				_selectedVariable = _selectedVariable - 1;
			break;
		case Key::PgDown: // increase counter by page size
			if (_selectedVariable + _page / 2 < _variables.Size())
				_selectedVariable = (_selectedVariable + _page / 2);
			else
				_selectedVariable = _variables.Size() - 1;
			break;
		case Key::PgUp:   // decrease  counter by page size
			if (_selectedVariable < _page / 2)
				_selectedVariable = 0;
			else
				_selectedVariable = (_selectedVariable - _page / 2);
			break;
		case Key::Home:  // get counter to the beginning
			_selectedVariable = 0;
			break;
		case Key::End:   // get counter to the end
			_selectedVariable = _variables.Size() - 1;
			break;
		case Key::Enter: // enter the variable view
			if (_variables[_selectedVariable]->Source() & Debug::Sources::ENGINE) {
				state(State::Variable);
				_variable = _variables[_selectedVariable];
				_inputCursor = 0;
				_input.Clear();
			}
			break;
		default:
			wchar_t c = Keyboard::Key2Char(key, modifiers);
			if (c) {
				_searchCursor = 1;
				_searchInput.Clear().Append(c);
				state(State::Search);
			}
			break;
	}
}

void EngineConsoleImpl::processVariable(Key key, KeyModifiers modifiers) {
	switch (key) {
		case Key::Left:   // cursor handling
			if (_inputCursor)
				_inputCursor = _inputCursor - 1;
			break;
		case Key::Right:  // cursor handling
			if (_inputCursor + 1 < _input.Length())
				_inputCursor = _inputCursor + 1;
			break;
		case Key::Home:   // cursor handling
			_inputCursor = 0;
			break;
		case Key::End:    // cursor handling
			_inputCursor = _input.Length() ? _input.Length() - 1 : 0;
			break;
		case Key::Enter:  // dialog handling
			setVariable();
		case Key::Esc:    // dialog handling
			stateBack();
			break;
		case Key::Delete: // cursor handling
			if (_inputCursor < _input.Length())
				_input.RemoveChar(_inputCursor);
			break;
		case Key::Backspace: // cursor handling
			if (0 < _inputCursor)
				_input.RemoveChar(--_inputCursor);
			break;
		case Key::S:
			if (modifiers.CtrlPressed())
				_variable->Saved(!_variable->Saved());
			else
				_input.InsertChar(_inputCursor++, Keyboard::Key2Char(key, modifiers));
			break;
		case Key::T:
			if (modifiers.CtrlPressed())
				_variable->Tracked(!_variable->Tracked());
			else
				_input.InsertChar(_inputCursor++, Keyboard::Key2Char(key, modifiers));
			break;
		default: // process text
			wchar_t c = Keyboard::Key2Char(key, modifiers);
			if (c)
				_input.InsertChar(_inputCursor++, c);
			break;
	}
}

void EngineConsoleImpl::processSearch(Key key, KeyModifiers modifiers) {
	switch (key) {
		case Key::Esc:       // dialog handling
			stateBack();
			break;
		case Key::Enter:
			if (_searchList[_selectedSearch]->Source() & Debug::Sources::ENGINE) {
				_variable = _searchList[_selectedSearch];
				state(State::Variable);
				_inputCursor = 0;
				_input.Clear();
			}
			break;
		case Key::Delete:    // cursor handling
			if (_searchCursor < _searchInput.Length())
				_searchInput.RemoveChar(_searchCursor);
			break;
		case Key::Backspace: // cursor handling
			if (0 < _searchCursor)
				_searchInput.RemoveChar(--_searchCursor);
			break;
		case Key::Left:      // cursor handling
			if (_searchCursor)
				_searchCursor = _searchCursor - 1;
			break;
		case Key::Right:     // cursor handling
			if (_searchCursor < _searchInput.Length())
				_searchCursor = _searchCursor + 1;
			break;
		case Key::Home:      // cursor handling
			_searchCursor = 0;
			break;
		case Key::End:       // cursor handling
			_searchCursor = _searchInput.Length() ? _searchInput.Length() : 0;
			break;
		case Key::Down: {   // move down
			if (_selectedSearch + 1 != _searchList.Size())
				_selectedSearch = (_selectedSearch + 1);
		} break;
		case Key::Up: {     // move up
			if (_selectedSearch)
				_selectedSearch = _selectedSearch - 1;
		} break;
		case Key::PgDown: { // move down by page size
			if (_selectedSearch + _page / 2 < _searchList.Size())
				_selectedSearch = _selectedSearch + _page / 2;
			else
				_selectedSearch = _searchList.Size() - 1;
		} break;
		case Key::PgUp: {   // move up by page size
			if (_selectedSearch < _page / 2)
				_selectedSearch = 0;
			else
				_selectedSearch = _selectedSearch - _page / 2;
		} break;
		default:             // input handling
			wchar_t c = Keyboard::Key2Char(key, modifiers);
			if (c) {
				_searchInput.InsertChar(_searchCursor++, c);
				_selectedSearch = 0;
			}
			break;
	}
}

void EngineConsoleImpl::feedConsole(Gpu::D2D::Strings & buffer) {
	const char * format = "| {1:-2} | {0:61} |\n";
	// title
	buffer.Append(Gpu::D2D::Fonts::DEFAULT_RED_GREAT, "Console\n");

	buffer
		.Append('+')
		.AppendFill('-', 4)
		.Append('+')
		.AppendFill('-', 63)
		.Append('+')
		.Append('\n')
		;

	size_t begin, end;
	Range((int)_categories.Size(), _page, (int)_selectedCategory, begin, end);

	for (size_t i = begin; i < _selectedCategory; ++i) {
		// normal item
		buffer.AppendFormat(
			format,
			/* 0 */ _categories[i]->Name(),
			/* 1 */ i + 1
		);
	}

	// highlighted item
	buffer.AppendFormat(
		Gpu::D2D::Fonts::DEFAULT_GREEN,
		format,
		/* 0 */ _categories[_selectedCategory]->Name(),
		/* 1 */ _selectedCategory + 1
	);

	for (size_t i = _selectedCategory + 1; i < end; ++i) {
		buffer.AppendFormat(
			format,
			/* 0 */ _categories[i]->Name(),
			/* 1 */ i + 1
		);
	}

	buffer
		.Append('+')
		.AppendFill('-', 4)
		.Append('+')
		.AppendFill('-', 63)
		.Append('+')
		;
}

void EngineConsoleImpl::feedCategory(Gpu::D2D::Strings & buffer) {
	const char * format = "| {2:!-2} | {0:!40} | {1:!-18} |\n";
	StringLocal<64> valueBuffer;
	// title
	buffer
		.Append(Gpu::D2D::Fonts::DEFAULT_RED_GREAT, "Console / ")
		.Append(Gpu::D2D::Fonts::DEFAULT_RED_GREAT, _categories[_selectedCategory]->Name())
		.Append('\n')
	;

	buffer
		.Append('+')
		.AppendFill('-', 4)
		.Append('+')
		.AppendFill('-', 42)
		.Append('+')
		.AppendFill('-', 20)
		.Append('+')
		.Append('\n')
		;

	size_t begin, end;
	Range((int)_variables.Size(), _page, (int)_selectedVariable, begin, end);

	for (size_t i = begin; i < _selectedVariable; ++i) {
		// normal item
		_variables[i]->ToString(valueBuffer.Clear());
		buffer.AppendFormat(
			format,
			/* 0 */ _variables[i]->Name(),
			/* 1 */ valueBuffer,
			/* 2 */ i + 1
		);
	}

	// highlighted item
	const Gpu::D2D::Fonts & font = _variables[_selectedVariable]->Source() & Debug::Sources::ENGINE ? Gpu::D2D::Fonts::DEFAULT_GREEN : Gpu::D2D::Fonts::DEFAULT_RED;

	_variables[_selectedVariable]->ToString(valueBuffer.Clear());
	buffer.AppendFormat(
		font,
		format,
		/* 0 */ _variables[_selectedVariable]->Name(),
		/* 1 */ valueBuffer,
		/* 2 */ _selectedVariable + 1
	);

	for (size_t i = _selectedVariable + 1; i < end; ++i) {
		_variables[i]->ToString(valueBuffer.Clear());
		buffer.AppendFormat(
			format,
			/* 0 */ _variables[i]->Name(),
			/* 1 */ valueBuffer,
			/* 2 */ i + 1
		);
	}

	buffer
		.Append('+')
		.AppendFill('-', 4)
		.Append('+')
		.AppendFill('-', 42)
		.Append('+')
		.AppendFill('-', 20)
		.Append('+')
		;
}

void EngineConsoleImpl::feedVariable(Gpu::D2D::Strings & buffer) {
	static constexpr size_t chars = 68;
	StringLocal<64> variableValue;
	const String & categoryName = _variable->Category();
	const String & variableName = _variable->Name();
	const String & variableDesc = _variable->Description();
	_variable->ToString(variableValue);

	size_t lCat, rCat, lVar, rVar, lVal, rVal, lEd, rEd;
	Left(/*Category: */ 10 + categoryName.Length(), chars, lCat, rCat);
	Left(/*Variable: */ 10 + variableName.Length(), chars, lVar, rVar);
	Left(/*Current Value: */ 15 + variableValue.Length(), chars, lVal, rVal);
	Left(/*_*/1 + _input.Length(), chars, lEd, rEd);
	wchar_t * input1 = _input.begin();
	wchar_t * input2 = input1 + _inputCursor;
	auto printBuffer = [&](String & hBuffer) {
		size_t lHint, rHint;
		Left(hBuffer.Length(), chars, lHint, rHint);
		buffer.Append('|').AppendFill(' ', lHint).Append(hBuffer).AppendFill(' ', rHint).Append('|').Append('\n');
		hBuffer.Clear();
	};

	// top line
	buffer.Append('+').AppendFill('-', chars).Append('+').Append('\n');
	// padding
	//buffer.Append('|').AppendFill(' ', chars).Append('|').Append('\n');
	// category name
	buffer.Append('|').AppendFill(' ', lCat).Append("Category: ").Append(categoryName).AppendFill(' ', rCat).Append('|').Append('\n');
	// spacing
	//buffer.Append('|').AppendFill(' ', chars).Append('|').Append('\n');
	// variable name
	buffer.Append('|').AppendFill(' ', lVar).Append("Variable: ").Append(variableName).AppendFill(' ', rVar).Append('|').Append('\n');
	StringLocal<128> b;
	printBuffer(b.Append("Is saved (ctrl + s): ").Append(_variable->Saved() ? "true" : "false"));
	printBuffer(b.Append("Is tracked (ctrl + t): ").Append(_variable->Tracked() ? "true" : "false"));
	// spacing
	//buffer.Append('|').AppendFill(' ', chars).Append('|').Append('\n');
	// description value
	StringLocal<128> descBuffer("Description: ");
	for (const Text::StringView16 & line : Sequencer::Drive(variableDesc.SplitSkipEmpty(L'\n'))) {
		for (const Text::StringView16 & word : Sequencer::Drive(line.SplitSkipEmpty(L' '))) {
			if (descBuffer.Length() + word.Length() >= chars) {
				printBuffer(descBuffer);
				descBuffer.Append(' ').Append(' ');
			}
			descBuffer.Append(word).Append(' ');
			// 		if (t == '\n') {
			// 			printBuffer(descBuffer);
			// 			descBuffer.Clear();
			// 		}
			// 		else if (chars - 3 == descBuffer.Length()) {
			// 			printBuffer(descBuffer.Append(t));
			// 			descBuffer.Clear();
			// 		}
			// 		else {
			// 			descBuffer.Append(t);
			// 		}
		}
		if (descBuffer.Length()) {
			printBuffer(descBuffer);
			descBuffer.Append(' ').Append(' ');
		}
	}
	// spacing
	// buffer.Append('|').AppendFill(' ', chars).Append('|').Append('\n');
	// variable value
	buffer.Append('|').AppendFill(' ', lVal).Append("Current Value: ").Append(variableValue).AppendFill(' ', rVal).Append('|').Append('\n');
	// spacing
	buffer.Append('|').AppendFill(' ', chars).Append('|').Append('\n');
	// input value
	buffer.Append('|').AppendFill(' ', lEd).Append(input1, _inputCursor).Append('_').Append(input2, _input.Length() - _inputCursor).AppendFill(' ', rEd).Append('|').Append('\n');
	// hint
	switch (_variable->Type()) {
		case Debug::VariableTypes::Enum: {
			StringLocal<128> hintBuffer;
			Debug::IEnum e = static_cast<Debug::IEnum>(_variable);
			const IEnum::Descriptor * d = e->Descriptor();
			for (u32 i = 0, m = d->Size(); i < m; ++i) {
				if (i % 4 == 0)
					printBuffer(hintBuffer);
				hintBuffer.AppendBatch('[', d->Index2String(i), '(', i, ")] ");
			}
			if (hintBuffer.Length())
				printBuffer(hintBuffer);
		} break;
		case Debug::VariableTypes::Options: {
			StringLocal<128> hintBuffer;
			Debug::OptionsBase * e = static_cast<Debug::OptionsBase *>(_variable.Ptr());
			size_t fill = 0;
			for (u32 i = 0, m = (u32)e->Size(); i < m; ++i) {
				if (fill <= e->At(i).Length())
					fill = e->At(i).Length() + 1;
			}
			for (u32 i = 0, m = (u32)e->Size(); i < m; ++i) {
				if (i == 9)
					--fill;
				hintBuffer
					.AppendBatch('[', e->At(i))
					.AppendFill(fill - e->At(i).Length())
					.AppendBatch('(', i, ")]");
				printBuffer(hintBuffer);
			}
		} break;
	}
	// spacing
	buffer.Append('|').AppendFill(' ', chars).Append('|').Append('\n');
	// bottom line
	buffer.Append('+').AppendFill('-', chars).Append('+');
}

void EngineConsoleImpl::feedSearch(Gpu::D2D::Strings & buffer) {
	_searchList.Clear();
	Debug::Variables()->Enumerate(_searchCategory);

	const char * format = "| {2:!-2} | {0:!40} | {1:!-18} |\n";
	StringLocal<64> valueBuffer;
	StringLocal<128> nameBuffer;
	size_t begin, end, matchBegin, matchEnd, nameBegin, nameEnd, highlightBegin, highlightEnd;
	// title
	buffer.Append(Gpu::D2D::Fonts::DEFAULT_RED_GREAT, "Search / ");
	buffer.Append(Gpu::D2D::Fonts::DEFAULT_GREEN_GREAT, _searchInput.SubView(0, _searchCursor));
	buffer.Append('_');
	buffer.Append(Gpu::D2D::Fonts::DEFAULT_GREEN_GREAT, _searchInput.SubView(_searchCursor));
	buffer.Append('\n');
	buffer.Append('+').AppendFill('-', 4).Append('+').AppendFill('-', 42).Append('+').AppendFill('-', 20).Append('+').Append('\n');

	if (_searchList.Size()) {
		Range((int)_searchList.Size(), _page, (int)_selectedSearch, begin, end);

		for (size_t i = begin; i < _selectedSearch; ++i) {
			// normal item
			_searchList[i]->ToString(valueBuffer.Clear());
			nameBuffer.Clear().AppendBatch(_searchList[i]->Category(), '/', _searchList[i]->Name());
			const String match = nameBuffer.MatchI(_searchInput, matchBegin, matchEnd);
			nameBegin = buffer.Length() + 7;
			nameEnd = nameBegin + 40;
			buffer.AppendFormat(
				format,
				/* 0 */ nameBuffer,
				/* 1 */ valueBuffer,
				/* 2 */ i + 1
			);
			highlightBegin = Math::Min(nameEnd, nameBegin + matchBegin);
			highlightEnd = Math::Min(nameEnd, nameBegin + matchEnd);
			buffer.Color(Gpu::D2D::Fonts::DEFAULT_BLUE, highlightBegin, highlightEnd - highlightBegin);
		}

		// highlighted item
		nameBegin = buffer.Length() + 7;
		nameEnd = nameBegin + 40;
		const Gpu::D2D::Fonts & font = _searchList[_selectedSearch]->Source() & Debug::Sources::ENGINE ? Gpu::D2D::Fonts::DEFAULT_GREEN : Gpu::D2D::Fonts::DEFAULT_RED;
		_searchList[_selectedSearch]->ToString(valueBuffer.Clear());
		nameBuffer.Clear().AppendBatch(_searchList[_selectedSearch]->Category(), '/', _searchList[_selectedSearch]->Name());
		const String match = nameBuffer.MatchI(_searchInput, matchBegin, matchEnd);
		nameBegin = buffer.Length() + 7;
		nameEnd = nameBegin + 40;
		buffer.AppendFormat(
			font,
			format,
			/* 0 */ nameBuffer,
			/* 1 */ valueBuffer,
			/* 2 */ _selectedSearch + 1
		);
		highlightBegin = Math::Min(nameEnd, nameBegin + matchBegin);
		highlightEnd = Math::Min(nameEnd, nameBegin + matchEnd);
		buffer.Color(Gpu::D2D::Fonts::DEFAULT_BLUE, highlightBegin, highlightEnd - highlightBegin);

		for (size_t i = _selectedSearch + 1; i < end; ++i) {
			_searchList[i]->ToString(valueBuffer.Clear());
			nameBuffer.Clear().AppendBatch(_searchList[i]->Category(), '/', _searchList[i]->Name());
			const String match = nameBuffer.MatchI(_searchInput, matchBegin, matchEnd);
			nameBegin = buffer.Length() + 7;
			nameEnd = nameBegin + 40;
			buffer.AppendFormat(
				format,
				/* 0 */ nameBuffer,
				/* 1 */ valueBuffer,
				/* 2 */ i + 1
			);
			highlightBegin = Math::Min(nameEnd, nameBegin + matchBegin);
			highlightEnd = Math::Min(nameEnd, nameBegin + matchEnd);
			buffer.Color(Gpu::D2D::Fonts::DEFAULT_BLUE, highlightBegin, highlightEnd - highlightBegin);
		}
	}
	else {
		buffer.Append("| ");
		buffer.Append(Gpu::D2D::Fonts::DEFAULT_RED, "No debug variable match. Press ESC to get back to console listing.");
		buffer.Append(" |\n");
	}

	buffer.Append('+').AppendFill('-', 4).Append('+').AppendFill('-', 42).Append('+').AppendFill('-', 20).Append('+');
}

void EngineConsoleImpl::FeedText(Gpu::D2D::Strings & buffer) {
	switch (_state) {
		case State::Console:  feedConsole (buffer); break;
		case State::Category: feedCategory(buffer); break;
		case State::Variable: feedVariable(buffer); break;
		case State::Search:   feedSearch(buffer);   break;
	}
}

EngineConsoleImpl::EngineConsoleImpl(Debug::i32Range & page) {
	_page = page;
	_onCategoryEnum = [this](Debug::ICategoryBase * c) { _categories.Add(c); };
	_onVariableEnum = [this](Debug::IVariableBase * v) { _variables.Add(v); };

	_onVariableAdd = [this](Debug::IVariableBase * v) {
		if (_variables.Add(v) <= _selectedVariable) {
			++_selectedVariable;
		}
	};
	_onCategoryAdd = [this](Debug::ICategoryBase * c) {
		if (_categories.Add(c) <= _selectedCategory) {
			++_selectedCategory;
		}
	};

	_searchVariable = [this](Debug::IVariableBase * variable) {
		if (StringLocal<256>::From(variable->Category(), '/', variable->Name()).MatchI(_searchInput).Length()) {
			_searchList.Add(variable);
		}
	};

	_searchCategory = [this](Debug::ICategoryBase * category) {
		category->Enumerate(_searchVariable);
	};

	Debug::Variables()->Lock();
	Debug::Variables()->Enumerate(_onCategoryEnum);
	Debug::Variables()->OnCategoryCreate += _onCategoryAdd;
	Debug::Variables()->UnLock();
}
#pragma endregion

/************************************************************************/
/* EngineStatistic                                                      */
/************************************************************************/
#pragma region EngineStatistic

#pragma endregion


/************************************************************************/
/* ProgressBar                                                          */
/************************************************************************/
#pragma region loader

/**
 * Structure for alignment of progress bars in the engine.
 * Singleton implementation.
 */
struct ProgressBarFactory {
	DD_TODO_STAMP("Move singleton impl to intantiation impl.");
	/** List type. */
	typedef List<Reference<ProgressBarSprite>, 8> list_t;
	/** Lock type. */
	typedef Concurrency::AtomicLock lock_t;

public: // statics
	/** Singleton instance. */
	static ProgressBarFactory & Instance() { static ProgressBarFactory lf; return lf; }

protected:
	/** Storage of lists - thread safe list of refs. */
	Concurrency::ThreadSafeObject<list_t, lock_t> _data;
	/** Definition of grid alignment. */
	float2 _dimensions, _orientation;
	/** Definition of grid alignment. */
	int2 _grid;

private: // methods
	/** Thread unsafe call - must be called responsibly. */
	void recompute(size_t index);

public: // methods
	/** Add loader to factory. */
	void Add(ProgressBarSprite * loader);
	/** Remove loader from factory.*/
	void Remove(ProgressBarSprite * loader);

public: // constructor
	ProgressBarFactory();
};

inline void ProgressBarFactory::recompute(size_t index) {
	// compute box
	Gpu::D2D::Boxes box;

	float2 origins;
	origins.x = _orientation.x > 0.f ? 0.f : 1.f - (1.f / _grid.x);
	origins.y = _orientation.y > 0.f ? 0.f : 1.f - (1.f / _grid.y);

	float2 offsets;
	offsets.x = _orientation.x * (1.f / _grid.x) * (index % _grid.x);
	offsets.y = _orientation.y * (1.f / _grid.y) * (index / _grid.x);
	box.Origins = origins + offsets;

	box.Dimensions = _dimensions;
	box.VAlign = Gpu::D2D::VAligns::VTop;
	box.HAlign = Gpu::D2D::HAligns::HLeft;
	box.Mode = Gpu::D2D::SSModes::Interval;
	// set box
	_data.GetUnsafeAccess().At(index)->Box(box);
}

/** Add loader to factory. */
inline void ProgressBarFactory::Add(ProgressBarSprite * loader) {
	// lock the object
	auto proxy = *_data;
	// index of new item to be recompute
	size_t index = proxy->Size();
	// call remove while expiration
	Delegate<ISprite *> onExpiration(
		[this](ISprite * sprite) {
			Remove(static_cast<ProgressBarSprite *>(sprite));
		}
	);
	onExpiration += [onExpiration](ISprite * sprite) mutable {
		sprite->OnExpiration -= onExpiration;
		onExpiration.Release();
	};
	loader->OnExpiration += onExpiration;
	// add to storage
	proxy->Add(loader);
	// recompute just added loader
	recompute(index);
}

/** Remove loader from factory.*/

inline void ProgressBarFactory::Remove(ProgressBarSprite * loader) {
	// lock the object
	auto proxy = *_data;
	// get index to be removed
	size_t index = proxy->IndexOf(
		[loader](const ProgressBarSprite * item) { return item == loader; }
	);
	// remove
	proxy->RemoveAt(index);
	// reposition the rest
	while (index < proxy->Size()) {
		recompute(index++);
	}
}

// constructor

inline ProgressBarFactory::ProgressBarFactory()
	: _dimensions(0.2f, 0.05f)
	, _orientation(1.f, -1.f)
	, _grid(5, 20)
{}

#pragma endregion

/************************************************************************/
/* Engine                                                               */
/************************************************************************/
#pragma region Engine private
void Engine::establishD3D() {
	// callback for selecting device
	Lambda16<u32(const ArrayView1D<Gpu::Devices::Description> &)> callback =
	[this](const ArrayView1D<Gpu::Devices::Description> & descs) -> u32 {
		for (const Gpu::Devices::Description & desc : descs) {
			_adapters->Add(desc.Name);
		}
		// return selected index
		return _adapters->Index();
	};

	// parameters for device initialization
	Gpu::Devices::InitParams params;
	params.SelectDevice = callback;
	params.Window = _window;

	// init
	_d3d = new Gpu::Devices(params);
	_d3d->AlphaBlending(true);
	_camera.Init(_d3d);
	QuadModel::_CAMERA = &_camera;

	RegisterMessage("Graphic HW: {0}\n", GetDevice3D().GraphicCardName());
}

void Engine::establishD2D() {
#if __ENABLE_D2D
	_d2d = new Gpu::D2D(_d3d);
#endif
}


void Engine::processKeyboardCamera() {
	Units::Time_MicroSecondULL time = GetTime();
	if (time < _keyboardCameraTime || _console.Enabled)
		return;

	_keyboardCameraTime = time + 10000_us;
	f32 gain = _keyboardSensitivity;
	if (_keyboard.Shift()) gain *= 500;
	if (_keyboard.Ctrl()) gain *= 50;
	// if (_keyboard.Esc()) exit2();
	if (_keyboard.W()) _camera.ForwardXZ(gain);
	if (_keyboard.S()) _camera.ForwardXZ(-gain);
	if (_keyboard.D()) _camera.RightXZ(gain);
	if (_keyboard.A()) _camera.RightXZ(-gain);
	if (_keyboard.E() || _keyboard.R()) _camera.TopXZ(gain);
	if (_keyboard.Q() || _keyboard.F()) _camera.TopXZ(-gain);
	//if (_keyboard.Plus())  if (_camera.Fov() > 0.01f) _camera.Fov() -= 0.01f;
	//if (_keyboard.Minus()) if (_camera.Fov() < 1.57f) _camera.Fov() += 0.01f;
}

void Engine::processKeyboard() {
	_keyboardProcessor(_keyboard);
	for (size_t i = 0; i < _keyboardActions.Size(); ++i) {
		auto & action = _keyboardActions[i];
		if (GetKeyboard().Check(action.key) && 250000_us < _renderTime - action.time) {
			action.action(*this);
			action.time = _renderTime;
		}
	}
}

void Engine::processTextMessages() {
	DD_PROFILE_SCOPE_STATS("Engine/Text/Messages")
	auto proxy = *_messages;
	for (Message & message : *proxy) {
		if (message) {
			if (message->Enabled(_renderTimeGap)) {
				message->FeedText(_messaging.Buffer);
			}
			else {
				message.Release();
			}
		}
	}

	if (_frameCount % 3000U == 0) {
		size_t i = proxy->Size();
		while (i--) {
			if ((*proxy)[i] == nullptr) {
				proxy->RemoveAt(i);
			}
		}
	}
}

void Engine::processText() {
	DD_PROFILE_SCOPE_STATS("Engine/Text");

	StringLocal<2048> buffer;

	if (_msgDevice) {
#if DD_DEVICE_STATISTIC
		buffer.AppendCol(StringLocal<512>::Format(
			"+--------------------------------------+\n"
			"| R E S O U R C E S                    |\n"
			"+------+------------------+------------+\n"
			"|  ALL | {0:-14, N} B |            |\n"
			"|   CB | {1:-14, N} B | {6:-10, N} |\n"
			"|   IB | {2:-14, N} B | {7:-10, N} |\n"
			"|   VB | {3:-14, N} B | {8:-10, N} |\n"
			"|   SB | {4:-14, N} B | {9:-10, N} |\n"
			"| RWSB | {5:-14, N} B | {10:-10, N} |\n"
			"+------+------------------+------------+\n",
			/*0*/ _statistics.Bytes,
			/*1*/ _statistics.CBBytes,
			/*2*/ _statistics.IBBytes,
			/*3*/ _statistics.VBBytes,
			/*4*/ _statistics.SBBytes,
			/*5*/ _statistics.RWSBBytes,
			/*6*/ _statistics.CBCount,
			/*7*/ _statistics.IBCount,
			/*8*/ _statistics.VBCount,
			/*9*/ _statistics.SBCount,
			/*10*/ _statistics.RWSBCount
		));

		buffer.AppendCol(StringLocal<320>::Format(
			"+-------------------------+\n"
			"| P R I M I T I V E S     |\n"
			"+----------+--------------+\n"
			"|    POINT | {0:-12, N} |\n"
			"|     LINE | {1:-12, N} |\n"
			"| TRIANGLE | {2:-12, N} |\n"
			"|    DRAWS | {3:-12, N} |\n"
			"+----------+--------------+\n",
			/*0*/ _statistics.Points,
			/*1*/ _statistics.Lines,
			/*2*/ _statistics.Triangles,
			/*3*/ _statistics.DrawCalls
		));
#else
		_d2d->Buffer().Append(L"Device statistics not available. @see DD_DEVICE_STATISTIC\n");
#endif
	}

	if (_msgMT) {
		buffer.Append(StringLocal<256>::Format(
			"+--------------------------+\n"
			"| T H R E A D I N G        |\n"
			"+--------------+-----------+\n"
			"| Global usage | {0:-7,.2} % |\n"
			"| Global tasks | {1:-9, N} |\n"
			"| Engine usage | {2:-7,.2} % |\n"
			"| Engine tasks | {3:-9, N} |\n"
			"+--------------+-----------+\n",
			/*0*/ 100.f * Concurrency::ThreadPool::Instance().NumberOfWorkingThreads() / Concurrency::ThreadPool::Instance().NumberOfThreads(),
			/*1*/ Concurrency::ThreadPool::Instance().NumberOfTasks() + Concurrency::ThreadPool::Instance().NumberOfWorkingThreads(),
			/*2*/ 100.f * _pool.NumberOfWorkingThreads() / (f32)_pool.NumberOfThreads(),
			/*3*/ _pool.NumberOfTasks() + _pool.NumberOfWorkingThreads()
		));
	}

	if (_msgCamera) {
		// direction = { x = cos, y = sin }
		float3 dir3 = _camera.Direction();
		float3 dirXZ = Math::Normalize(_camera.DirectionXZ());
		// y - elevation
		f32 elevation = (dir3.y >= 0) ?
			Math::Rad2Deg(-Math::ArcCos(cos(dir3, dirXZ))):
			Math::Rad2Deg(Math::ArcCos(cos(dir3, dirXZ)));
		// xz azimuth (reversed - unit circle)
		f32 azimuth = dirXZ.z < 0.f ?
			Math::Rad2Deg(Math::ArcCos(dirXZ.x)):
			360.f - Math::Rad2Deg(Math::ArcCos(dirXZ.x));

		buffer.Append(StringLocal<256>::Format(
			"+--------------------+\n"
			"| C A M E R A        |\n"
			"+------+-------------+\n"
			"| LONG | {4:-11} |\n"
			"|  FPS | {0:-11, .2} |\n"
			"|    X | {1:-11, .2} |\n"
			"|    Y | {2:-11, .2} |\n"
			"|    Z | {3:-11, .2} |\n"
			"|    A | {5:-11, .2} |\n"
			"|    E | {6:-11, .2} |\n"
			"|  FoV | {7:-11, .2} |\n"
			"| Near | {8:-11, .2} |\n"
			"|  Far | {9:-11, .2} |\n"
			"+------+-------------+\n",
			/*0*/ _fps,
			/*1*/ _camera.Position().x,
			/*2*/ _camera.Position().y,
			/*3*/ _camera.Position().z,
			/*4*/ _fpsLongestRecord.Value,
			/*5*/ azimuth,
			/*6*/ elevation,
			/*7*/ Math::Rad2Deg(Math::ArcTg(_camera.TgFov().x)),
			/*8*/ _camera.Near(),
			/*9*/ _camera.Far()
		));
	}

	_messaging.Buffer.Append(buffer);

	processTextMessages();
}

void Engine::draw2D() {
	DD_PROFILE_SCOPE_STATS("Engine/Draw/Draw2D");

#if __ENABLE_D2D
	Concurrency::ThreadPool::TaskList<64> taskList(_pool);
	auto spriteProxy = *_sprites;
	// remove old spites
	if (_frameCount % 30 == 0) {
		auto removeProxy = *_spriteRemove;
		while (removeProxy->Size()) {
			ISprite * sprite = removeProxy->Last();
			size_t index = spriteProxy->IndexOf(
				[sprite](const SpriteEx & s) { return s.Sprite == sprite; }
			);
			sprite->OnUnregistration(sprite);
			spriteProxy->RemoveAt(index);
			removeProxy->RemoveLast();
		}
	}
	// process sprites
	for (SpriteEx & sprite : *spriteProxy) {
		taskList.Add([&sprite, this] (){
			if (sprite.Sprite->Enabled()) {
				sprite.Sprite->Update(_renderTimeGap.Cast<f32>(), _renderTime.Cast<f32>());
				sprite.Sprite->Draw(*_d2d);
			}
		});
	}

	// console
	if (_console.Enabled) {
		taskList.Add([this]() {
			DD_PROFILE_SCOPE_STATS("Engine/Draw/Draw2D/Console");
			DD_TODO_STAMP("Make console browser per instance (currently it is global).");
			// init console browser
			static EngineConsoleImpl _consoleBrowser(_consolePageSize);
			// init console browser input
			static Delegate<Key> onKeyDown = [this](Key k) {
				if (_console.Enabled)
					_consoleBrowser.ProcessKey(k, _keyboard.Modifiers());
			};
			// assign input between window and console
			static bool init = [this]() { _window->OnKeyDown += onKeyDown; return true; }();
			_consoleBrowser.FeedText(_console.Buffer);

			Gpu::D2D::Labels text;
			text.Box.Origins = 0.f;
			text.Box.Dimensions = 1.f;
			text.Box.Mode = Gpu::D2D::SSModes::Interval;
			text.Box.HAlign = _console.HAlign;
			text.Box.VAlign = _console.VAlign;
			text.String = _console.Buffer;
			text.THAlign = _console.TAlign;
			text.Scale = _console.Scale;
			Action32<const Gpu::D2D::Boxes &, Gpu::D2D &> callback = [this](const Gpu::D2D::Boxes & box, Gpu::D2D& d2d) {
				Gpu::D2D::Rectangles rectangle;
				rectangle.Box = box;
				// set padding
				rectangle.Box.Origins -= float2(10.f, 10.f);
				rectangle.Box.Dimensions += float2(20.f, 20.f);
				rectangle.Background = Color::Collection()[+_console.Background].SetA(_console.Alpha);
				d2d.DrawRectangle(rectangle);
			};
			text.Callbacks.SizeComputed = callback;

			_d2d->DrawLabel(text);
		});
	}
	// messaging
	if (_messaging.Enabled) {
		taskList.Add([this]() {
			DD_PROFILE_SCOPE_STATS("Engine/Draw/Draw2D/Messaging");
			processText();
			Gpu::D2D::Labels text;
			text.Box.Origins = 0.f;
			text.Box.Dimensions = 1.f;
			text.Box.Mode = Gpu::D2D::SSModes::Interval;
			text.Box.HAlign = _messaging.HAlign;
			text.Box.VAlign = _messaging.VAlign;
			text.String = _messaging.Buffer;
			text.THAlign = _messaging.TAlign;
			text.Scale = _messaging.Scale;
			_messaging.Buffer.Clear();

			Action32<const Gpu::D2D::Boxes &, Gpu::D2D &> callback = [this](const Gpu::D2D::Boxes & box, Gpu::D2D & d2d) {
				Gpu::D2D::Rectangles rectangle;
				rectangle.Box = box;
				// set padding
				rectangle.Box.Origins -= float2(10.f, 10.f);
				rectangle.Box.Dimensions += float2(20.f, 20.f);
				rectangle.Background = Color::Collection()[+_messaging.Background].SetA(_messaging.Alpha);
				_d2d->DrawRectangle(rectangle);
			};
			text.Callbacks.SizeComputed = callback;

			_d2d->DrawLabel(text);


			Color::Collection()[+_messaging.Background].SetA(_messaging.Alpha);
		});
	}

	if (_fpsStatistics.Mode != StatisticSprite::Modes::HIDDEN) {
		//static wchar_t unit[] = { L' ', 0xB5, 's', 0 };
		static StatisticSprite::Drawing drawer(L" FPS", L".2");
		taskList.Add([this] {
			DD_PROFILE_SCOPE_STATS("Engine/Draw/Draw2D/FPS");
			Gpu::D2D::Statistics statistic;
			statistic.Statistic.Samples.Swap(Array<f32>(_fpsHistory.ToView(), nullptr));
			statistic.Brush = Colors::BLUE;
			statistic.Box.Origins.Set(_fpsStatistics.OffsetX, _fpsStatistics.OffsetY);
			statistic.Box.Dimensions.Set(_fpsStatistics.SizeX, _fpsStatistics.SizeY);
			statistic.Box.Mode = Gpu::D2D::SSModes::Interval;
			if (_fpsStatistics.Mode == StatisticSprite::Modes::FLUENT)
				statistic.Statistic.IndexOffset = (u32)_frameCount;
			drawer.Draw(*_d2d, statistic, L"FPS");
		});
	}

	if (_frameStatistics.Mode != StatisticSprite::Modes::HIDDEN) {
		static wchar_t unit[] = { L' ', 0xB5, 's', 0 };
		static StatisticSprite::Drawing drawer(unit, L".0");
		taskList.Add([this] {
			DD_PROFILE_SCOPE_STATS("Engine/Draw/Draw2D/Frame");
			Gpu::D2D::Statistics statistic;
			statistic.Statistic.Samples.Swap(Array<f32>(_frameHistory.ToView(), nullptr));
			statistic.Brush = Colors::BLUE;
			statistic.Box.Origins.Set(_frameStatistics.OffsetX, _frameStatistics.OffsetY);
			statistic.Box.Dimensions.Set(_frameStatistics.SizeX, _frameStatistics.SizeY);
			statistic.Box.Mode = Gpu::D2D::SSModes::Interval;
			if (_frameStatistics.Mode == StatisticSprite::Modes::FLUENT)
				statistic.Statistic.IndexOffset = (u32)_frameCount;
			drawer.Draw(*_d2d, statistic, L"Frame duration");
		});
	}

	taskList.Wait();
	// draw texture
	{
		DD_PROFILE_SCOPE_STATS("Engine/Draw/Draw2D/Render");
		_d2d->Draw();
	}

	// cleanup
	if (_messaging.Enabled)
		_messaging.Buffer.Clear();
	if (_console.Enabled)
		_console.Buffer.Clear();
#endif
}

void Engine::cameraUpdate() {
	_camera->SceneDepthInv = 1.f / _camera.Far();
	_camera->CameraFlags = +_cameraMode;
	// check special camera visualizations
	if (_camera->CameraFlags & (_CAMERA_FLAG_HEIGHT | _CAMERA_FLAG_HEIGHT_TUNE)) {
		// detect settings automatically
		if (!_cameraHeightManual) {
			f32 min = Math::ConstantsF::Max;
			f32 max = Math::ConstantsF::Min;
			// locked recursively, should be cheap
			auto proxy = *_objects;
			// get min-max from the objects
			for (auto & object : *proxy) {
				AABox3F box = object.Object->BoundingAABox();
				if (box.Min.y < min)
					min = box.Min.y;
				if (max < box.Max.y)
					max = box.Max.y;
			}

			// update diag values
			_cameraHeightMin = min;
			_cameraHeightMax = max;
		}

		// update cbuffer values
		_camera->SceneHeightMin = _cameraHeightMin;
		_camera->SceneHeightRangeInv = 1.f / (_cameraHeightMax - _cameraHeightMin);
	}

	// update camera constant buffer
	_camera.Update();
	// bind to all shaders
	_camera.Bind();
}

void Engine::directDraw(bool effect) {
	if (effect) {
		if (_frustumCulling) {
			for (ObjectEx & object : **_objects) if (object.Object->Effect()) {
				DD_PROFILE_SCOPE_STATS("Engine/Draw3D/Object/DrawCulled");
				object.Object->OnObjectUpdate(object.Object);
				object.Object->DrawCulled(_frustum);
			}
		}
		else {
			for (ObjectEx & object : **_objects) if (object.Object->Effect()) {
				DD_PROFILE_SCOPE_STATS("Engine/Draw3D/Object/Draw");
				object.Object->Draw();
			}
		}
	}
	else {
		if (_frustumCulling) {
			for (ObjectEx & object : **_objects) {
				DD_PROFILE_SCOPE_STATS("Engine/Draw3D/Object/DrawCulled");
				object.Object->OnObjectUpdate(object.Object);
				object.Object->DrawCulled(_frustum);
			}
		}
		else {
			for (ObjectEx & object : **_objects) {
				DD_PROFILE_SCOPE_STATS("Engine/Draw3D/Object/Draw");
				object.Object->Draw();
			}
		}
	}
}

void Engine::effectDraw() {
	DD_TODO_STAMP("Move statics to properties.");
	DD_TODO_STAMP("Update textures when settings of RT is being changed.")
	// main render target
	static Gpu::Texture textureReal(Gpu::Texture::R8G8B8A8);
	// aura mask
	static Gpu::Texture textureMask(Gpu::Texture::R8);
	// constant buffer for aura effect control
	static Gpu::ConstantBuffer<EffectConstantsBuffer> effectBuffer;

	// initialize if this is first run
	if (!textureReal.IsInitialized()) {
		textureReal.Init(_d3d);
		textureMask.Init(_d3d);
		effectBuffer.Init(_d3d);
	}

	// reset textures by given background color
	float4 background = Color(_background->Value());
	textureReal.ClearRT((f32 *)&background);
	textureMask.ClearRT((f32 *)&background);

	// perform drawing to real texture
	effectBuffer->Effect = 0;
	effectBuffer.Update();
	effectBuffer.Bind();

	textureReal.Bind2RT();
	directDraw(false);

	effectBuffer->Effect = 1;
	effectBuffer.Update();
	effectBuffer.Bind();
	textureMask.Bind2RT();
	directDraw(true);
	// render to swap chain render target
	aura2RT(textureReal, textureMask);
}

void Engine::draw3D() {
	DD_PROFILE_SCOPE_STATS("Engine/Draw3D");

	// lock objects - must be first because in the called methods
	// this could recursively locked and changed
	auto proxy = *_objects;

	// update camera constant buffer
	cameraUpdate();

	// update frustum
	if (!_frustumCullingFix) {
		_frustum = _camera.CreateFrustum();
	}

	if (_numberOfHighlightedObjects)
		effectDraw();
	else
		directDraw(false);

	// register new objects
	auto proxyInit = *_objectsToInit;
	for (ObjectEx & toInit : *proxyInit) {
		toInit.Object->Model()->Init(_d3d);
		proxy->Add(toInit);
	}
	proxyInit->Free();

	// remove old objects
	if (_frameCount % 120 == 0) {
		auto removeProxy = *_objectsExpired;
		while (removeProxy->Size()) {
			Object object = Fwd(removeProxy->Last());
			UnregisterObject(object);
			removeProxy->RemoveLast();
		}
	}

	// async init of models
	auto proxyAsyncInit = *_modelsToAsyncInit;
	// microseconds
	Units::Time_MicroSecondULL timeForAsync = 5000;
	while (!proxyAsyncInit->IsEmpty() && 0ULL < timeForAsync) {
		if (proxyAsyncInit->Front().Interface->Tick(_d3d, timeForAsync)) {
			proxyAsyncInit->Front().Variable->WakeOne();
			proxyAsyncInit->PopFront();
		}
	}

	// perform bounding volume drawing
	switch (+_bboxVisual) {
	case BoundingVisualizations::NONE:
		ICullableModel::DebugTools.DrawBoxes = false;
		break;
	case BoundingVisualizations::SOLID:
		ICullableModel::DebugTools.DrawBoxes = true;
		ICullableModel::DebugTools.WireBoxes = false;
		break;
	case BoundingVisualizations::WIREFRAME:
		ICullableModel::DebugTools.DrawBoxes = true;
		ICullableModel::DebugTools.WireBoxes = true;
		break;
	}

	switch (+_bsphereVisual) {
	case BoundingVisualizations::NONE:
		ICullableModel::DebugTools.DrawSpheres = false;
		break;
	case BoundingVisualizations::SOLID:
		ICullableModel::DebugTools.DrawSpheres = true;
		ICullableModel::DebugTools.WireSpheres = false;
		break;
	case BoundingVisualizations::WIREFRAME:
		ICullableModel::DebugTools.DrawSpheres = true;
		ICullableModel::DebugTools.WireSpheres = true;
		break;
	}

	{
		DD_PROFILE_SCOPE_STATS("Engine/Draw3D/DebugTools")
		ICullableModel::DebugTools.Draw(_d3d);
	}
}

void Engine::gauss2RT(Gpu::Texture & toBeBlurred) {
	static Debug::i32Range gaussKX(2, 0, 15,        L"Effects", L"Kernel X",             L"Tooltip.");
	static Debug::i32Range gaussKY(2, 0, 15,        L"Effects", L"Kernel Y",             L"Tooltip.");
	static Debug::f32Range gaussX(1.f, 0.01f, 10.f, L"Effects", L"Standard Deviation X", L"Tooltip.");
	static Debug::f32Range gaussY(1.f, 0.01f, 10.f, L"Effects", L"Standard Deviation Y", L"Tooltip.");
	static Debug::f32Range gaussXY(0.f, -1.f, 1.f,  L"Effects", L"Covariance XY",        L"Tooltip.");

	static Gpu::ConstantBuffer<ConvulutionBuffer> effectBuffer;
	if (!effectBuffer.IsInitialized()) {
		effectBuffer.Init(_d3d);
	}

	toBeBlurred.Bind2PS();

	// create Gaussian kernel
	Math::GaussF gaussKernel(0.f, 0.f, gaussX, gaussY, gaussXY);
	f32 normalization = 0.f;
	for (i32 x = -gaussKX; x <= gaussKX; ++x) {
		for (i32 y = -gaussKY; y <= gaussKY; ++y) {
			f32 p = gaussKernel((f32)x, (f32)y);
			effectBuffer->Kernel[gaussKX + x][gaussKY + y] = p;
			normalization += p;
		}
	}
	normalization = 1.f / normalization;
	for (i32 x = -gaussKX; x <= gaussKX; ++x) {
		for (i32 y = -gaussKY; y <= gaussKY; ++y) {
			effectBuffer->Kernel[gaussKX + x][gaussKY + y] *= normalization;
		}
	}

	// fill the constants
	effectBuffer->Resolution = float2((f32)_window->Client().Width, (f32)_window->Client().Height);
	effectBuffer->ResolutionInv = float2(1.f / _window->Client().Width, 1.f / _window->Client().Height);
	effectBuffer->Samples = _d3d->MultiSampling();
	effectBuffer->KernelX = gaussKX;
	effectBuffer->KernelY = gaussKY;
	effectBuffer.Update();
	effectBuffer.Bind();

	// draw
	_d3d->GetVertexShader<EmptyVertex>(L"VSTextureEffect")->Set();
	_d3d->GetPixelShader(L"PSConvolution")->Set();
	_d3d->SetPrimitiveTopology(Gpu::Devices::PrimitiveTopologies::TriangleList);
	_d3d->Draw(6);
}

void Engine::aura2RT(Gpu::Texture & real, Gpu::Texture & mask) {
	_d3d->GetMainRenderTarget().Bind2RT();
	real.Bind2PS(0);
	mask.Bind2PS(1);

	static Gpu::ConstantBuffer<ConvulutionBuffer> effectBuffer;
	if (!effectBuffer.IsInitialized()) {
		effectBuffer.Init(_d3d);
	}

	// fill the constants
	effectBuffer->Resolution = float2((f32)_window->Client().Width, (f32)_window->Client().Height);
	effectBuffer->ResolutionInv = float2(1.f / _window->Client().Width, 1.f / _window->Client().Height);
	effectBuffer->Samples = _d3d->MultiSampling();
	effectBuffer->KernelX = 4;
	effectBuffer->KernelY = 4;
	effectBuffer.Update();
	effectBuffer.Bind();

	// draw
	_d3d->GetVertexShader<EmptyVertex>(L"VSTextureEffect")->Set();
	_d3d->GetPixelShader(L"PSAura")->Set();
	_d3d->SetPrimitiveTopology(Gpu::Devices::PrimitiveTopologies::TriangleList);
	_d3d->Draw(6);
}

bool Engine::processInput() {
	if (_focused && _enabled && _processInput) {
		_keyboard.Refresh();
		processKeyboard();
		processKeyboardCamera();
	}

	return _enabled;
}
#pragma endregion

#pragma region Engine public
IProgress * Engine::RegisterProgressBar(const Text::StringView16 & name) {
	ProgressBarSprite * loader = new ProgressBarSprite(name);
	ProgressBarFactory::Instance().Add(loader);
	RegisterSprite(loader);
	return loader;
}

void Engine::InitModelAsync(IModel * model, IProgress * progress) {
	Concurrency::CriticalSection section;
	Concurrency::ConditionVariable variable;
	IModel::AsyncInitInterface asyncInit(model, progress);
	section.Enter();
	{
		auto proxy = *_modelsToAsyncInit;
		proxy->PushBack();
		proxy->Back().Interface = &asyncInit;
		proxy->Back().Variable = &variable;
	}

	variable.SleepCriticalSection(section);
	section.Leave();
}

void Engine::EnableProfileScopeStatistics() {
	const wchar_t unitW[] = { L' ', 0xB5, 's', 0 };
	const Text::StringView16 category(L"PFStatistics");
	const Text::StringView16 offsetHelp(L"Set left-top origin of statistic chart relatively screen space");
	const Text::StringView16 sizeHelp(L"Set horizontal dimension of statistic chart relatively to screen space.");
	const Text::StringView16 modeHelp(L"Set mode of visualization of statistics");
	const Text::StringView16 scopeHelp(L"Set which scope should be rendered.");

	for (size_t x = 0; x < 4; ++x) for (size_t y = 0; y < 4; ++y) {
		// create names
		StringLocal<32> name, offsetXName, offsetYName, sizeXName, sizeYName, modeName, scopeName;
		name.AppendBatch("PF ", x, 'x', y);
		offsetXName.AppendBatch(name, " - Offset X");
		offsetYName.AppendBatch(name, " - Offset Y");
		sizeXName.AppendBatch(name, " - Size X");
		sizeYName.AppendBatch(name, " - Size Y");
		modeName.AppendBatch(name, " - Mode");
		scopeName.AppendBatch(name, " - Scope");
		// create statistic
		StatisticSprite * statistic = new StatisticSprite(
			name,
			unitW,
			/*format*/ L".0",
			Debug::f32Range(0.25f * x + 0.005f, 0.f, 1.f, category, offsetXName, offsetHelp),
			Debug::f32Range(0.25f * y + 0.005f, 0.f, 1.f, category, offsetYName, offsetHelp),
			Debug::f32Range(0.24f, 0.1f, 1.f, category, sizeXName, sizeHelp),
			Debug::f32Range(0.24f, 0.1f, 1.f, category, sizeYName, sizeHelp),
			Debug::Enum<StatisticSprite::Modes>(StatisticSprite::Modes::HIDDEN, category, modeName, modeHelp),
			Debug::Options(category, scopeName, scopeHelp)
		);
		// add to engine
		RegisterSprite(statistic);
	}
}

void Engine::Exit() {
	_enabled = false;
	OnEngineClosed();
	_d3d->Exit();
	Concurrency::Thread::Sleep(200);
	_pool.TerminateAll();
}

Gpu::Devices::Screenshot Engine::CaptureScreenshot(bool only3D) {
	Gpu::Devices::Screenshot screenshot;

	if (only3D)
		_screenshotsOnly3D.Add(std::addressof(screenshot));
	else
		_screenshots.Add(std::addressof(screenshot));

	while (screenshot.IsNull())
		Concurrency::Thread::Sleep(10);

	return screenshot;
}

Engine & Engine::MouseMode(MouseModes mode) {
	_mouseMode = mode;
	OnMouseModeChanged(*this, mode);
	return *this;
}

bool Engine::Draw() {
	DD_PROFILE_SCOPE_STATS("Engine/Draw");
	// check device settings
	if (_deviceSettings.AlphaBlending != _d3d->AlphaBlending()) {
		_d3d->AlphaBlending(_deviceSettings.AlphaBlending);
	}
	if (_deviceSettings.DepthBuffer != _d3d->DepthBuffer()) {
		_d3d->DepthBuffer(_deviceSettings.DepthBuffer);
	}
	if (_deviceSettings.VSync != _d3d->VSync()) {
		_d3d->VSync(_deviceSettings.VSync);
	}
	if (_deviceSettings.WireFrame != _d3d->Wireframe()) {
		_d3d->Wireframe(_deviceSettings.WireFrame);
	}
	if (_deviceSettings.MultiSampling != _d3d->MultiSampling()) {
		_d3d->MultiSampling(_deviceSettings.MultiSampling);
		establishD2D();
	}


	auto delayLock = *_delayedTasks;
	while (!delayLock->IsEmpty() && delayLock->Top().Tick <= _frameCount) {
		_asyncTasks.Add([action = delayLock->Top().Action, this] () mutable { action(*this); });
		delayLock->Pop();
	}




	DD::Units::Time_MicroSecondULL time = GetTime();
  _engineTime = Units::Time_SecondF(time.Cast<f32>()).Value;
	_renderTimeGap = time - _renderTime;
	_renderTime = time;
	_lastFrameFocused = _focused;
	_focused = _window->Focused();

	if (_focused || _forcedRendering) {
		Units::Time_MicroSecondULL frame = time - _frameTime;
		Units::Time_MicroSecondULL measure = time - _frameTimeMeasure;
		_fpsLongest = Math::Max(time - _frameTime, _fpsLongest);
		_frameTime = time;
		if (measure.Value > 500000 && _frameCountMeasure > 0U) {
			_fps = ((f32)(_frameCountMeasure * 1000000) / measure.Value);
			_fpsTime = time;
			_frameCountMeasure = 0U;
			_frameTimeMeasure = time;
			_fpsLongestRecord = _fpsLongest;
			_fpsLongest = 0U;
		}

		float4 background = Color::Collection()[+_background];
		_d3d->Clear(&background.x).ClearDepth(1.f);
		_camera.FrameUpdate();
		_camera.Bind();
		draw3D();

		while (_screenshotsOnly3D.IsNotEmpty()) {
			(*_screenshotsOnly3D.Last()) = _d3d->CaptureScreenshot();
			_screenshotsOnly3D.RemoveLast();
		}

#if DD_DEVICE_STATISTIC
		Gpu::Devices::Statistics statistic = _d3d->Statistic();
		_statistics.Bytes = statistic.Bytes;
		_statistics.CBBytes = statistic.CBBytes;
		_statistics.IBBytes = statistic.IBBytes;
		_statistics.VBBytes = statistic.VBBytes;
		_statistics.SBBytes = statistic.SBBytes;
		_statistics.RWSBBytes = statistic.RWSBBytes;
		_statistics.CBCount = statistic.CBCount;
		_statistics.IBCount = statistic.IBCount;
		_statistics.VBCount = statistic.VBCount;
		_statistics.SBCount = statistic.SBCount;
		_statistics.RWSBCount = statistic.RWSBCount;
		_statistics.CBInits = statistic.CBInits;
		_statistics.IBInits = statistic.IBInits;
		_statistics.VBInits = statistic.VBInits;
		_statistics.SBInits = statistic.SBInits;
		_statistics.RWSBInits = statistic.RWSBInits;
		_statistics.DrawCalls = statistic.DrawCalls;
		_statistics.Points = statistic.PointLists;
		_statistics.Lines = (statistic.LineLists / 2) + (statistic.LineStrips);
		_statistics.Triangles = (statistic.TriangleLists / 3) + (statistic.TriangleStrips);
#endif
		draw2D();
		{
			DD_PROFILE_SCOPE_STATS("Engine/Draw/Present");
			_d3d->Present();
		}
		++_frameCount, ++_frameCountMeasure;
		_frameHistory[_frameCount % _frameHistory.Length()] = _frameWatch.ElapsedMicroseconds().Cast<f32>().Value;
		_fpsHistory[_frameCount % _fpsHistory.Length()] = 1000000.f / _frameWatch.ElapsedMicroseconds().Cast<f32>().Value;
		if (_fpsHistory.Length() != _fpsStatistics.Samples)
			_fpsHistory.Reallocate((u32)_fpsStatistics.Samples).FillUniform(0.f);
		if (_frameHistory.Length() != _frameStatistics.Samples)
			_frameHistory.Reallocate((u32)_frameStatistics.Samples).FillUniform(0.f);
		_frameWatch.Restart();
	}

	while (_screenshots.IsNotEmpty()) {
		(*_screenshots.Last()) = _d3d->CaptureScreenshot();
		_screenshots.RemoveLast();
	}

	// process sync actions
	List<Action<Engine &>> syncActions = Fwd(**_syncActions);
	for (Action<Engine &> & syncAction : syncActions)
		syncAction(*this);


	return _enabled;
}

bool Engine::RegisterMessage(Message message) {
	message->OnRegisterTry();
	auto proxy = *_messages;
	if (!proxy->Contains(message)) {
		proxy->Add(message);
		message->OnEnabled();
		return true;
	}

	return false;
}

void Engine::ClearObjectRegister() {
	DD_TODO_STAMP("_objectsToInit should be clearead too and initializtion in progress should be cancelled.");
	_objects->Free();
	OnObjectRegisterClearing();
}

Array<Object> Engine::ObjectRegisterState() {
	auto proxy = _objects.GetProxy();
	size_t size = proxy->Size();
	Array<Object> copy(size);
	for (size_t i = 0; i < size; ++i)
		copy[i] = proxy->At(i).Object;

	return copy;
}

Reference<Engine::BasicMessage> Engine::RegisterMessage(
	const Gpu::D2D::Strings & str, Units::Time_MilliSecondULL time
) {
	Reference<BasicMessage> msg = new BasicMessage(str, time);
	RegisterMessage(msg);
	return msg;
}

Object Engine::RegisterObject(Model model, Text::StringView16 name, bool async, IProgress * progress) {
	ObjectEx item;
	item.Object = Object(_d3d, model, name);
	item.Object->OnObjectExpiration += _objectExpirationDelegate;
	item.Functions = Debug::Enum<ObjectFunctions>(
		ObjectFunctions::Select,
		StringLocal<128>::From(_name, " Objects"),
		name,
		L"Simple operations with objects.",
		Debug::Sources::ALL ^ Debug::Sources::SERIAL
	);

	if (async)
		InitModelAsync(model, progress);

	ObjectBase * base = item.Object;
	item.Functions->OnValueChanged += [base, this](
		Debug::EnumBase<ObjectFunctions> * var,
		Debug::Sources
	) {
		// methods for blinking
		static Delegate<ObjectBase *> _blink = [](ObjectBase * obj) {
			obj->Enabled(Stopwatch::TimeStampSeconds() % 2);
		};
		switch (var->Value()) {
			case ObjectFunctions::Select:
				break;
			case ObjectFunctions::Show:
				base->OnObjectUpdate -= _blink;
				base->OnObjectUpdate->Free();
				base->Enabled(true);
				var->Value(ObjectFunctions::Select, Debug::Sources::CODE);
				break;
			case ObjectFunctions::Hide:
				base->OnObjectUpdate -= _blink;
				base->OnObjectUpdate->Free();
				base->Enabled(false);
				var->Value(ObjectFunctions::Select, Debug::Sources::CODE);
				break;
			case ObjectFunctions::Release:
				UnregisterObject(base);
				var->Value(ObjectFunctions::Select, Debug::Sources::CODE);
				break;
			case ObjectFunctions::Blink:
				base->OnObjectUpdate -= _blink;
				base->OnObjectUpdate += _blink;
				var->Value(ObjectFunctions::Select, Debug::Sources::CODE);
				break;
			case ObjectFunctions::CameraNNP:
			case ObjectFunctions::CameraNNN:
			case ObjectFunctions::CameraNPP:
			case ObjectFunctions::CameraNPN:
			case ObjectFunctions::CameraPNP:
			case ObjectFunctions::CameraPNN:
			case ObjectFunctions::CameraPPP:
			case ObjectFunctions::CameraPPN: {
				const Sphere3F & bsphere = base->BoundingSphere();
				f32 ex = bsphere.Radius * 1.5f;
				float3 offset(
					(var->Value() & 4) ? ex : -ex,
					(var->Value() & 2) ? ex : -ex,
					(var->Value() & 1) ? ex : -ex
				);
				float3 position = bsphere.Center + offset;
				_camera.Position(position);
				_camera.Direction(-Math::Normalize(offset));
				var->Value(ObjectFunctions::Select, Debug::Sources::CODE);
			} break;
			case ObjectFunctions::Effect:
				base->Effect(!base->Effect());
				var->Value(ObjectFunctions::Select, Debug::Sources::CODE);
				break;
			default:
				DD_WARNING("Undefined case.");
				break;
		}
	};

	if (async)
		_objects->Add(item);
	else
		_objectsToInit->Add(item);

	item.Object->OnEffectChange += [this](ObjectBase * object, bool enabled) {
		if (enabled)
			_numberOfHighlightedObjects.AtomicInc();
		else
			_numberOfHighlightedObjects.AtomicDec();
	};

	OnObjectRegistration(item.Object);

	return item.Object;
}

void Engine::RegisterSprite(Sprite sprite) {
	// set not expiration behavior
	sprite->OnRegistration(sprite);
	sprite->OnExpiration += _spriteExpirationDelegate;
	// lock the scope
	auto spriteProxy = *_sprites;
	// add new object
	spriteProxy->Append();
	// set sprite
	spriteProxy->Last().Sprite = sprite;
	// set debug manipulation
	spriteProxy->Last().Functions = Debug::Enum<SpriteFunctions>(
		SpriteFunctions::Select,
		StringLocal<128>::From(_name, " Sprites"),
		sprite->Name(),
		L"Simple operations with sprites.",
		Debug::Sources::ALL ^ Debug::Sources::SERIAL
	);
	// attach the manipulator
	spriteProxy->Last().Functions->OnValueChanged += [sprite](
		Debug::EnumBase<SpriteFunctions> * var,
		Debug::Sources source
	) mutable {
		switch (var->Value()) {
			case SpriteFunctions::Select:  // do nothing
				break;
			case SpriteFunctions::Enable:  // enable rendering
				sprite->Enabled(true);
				var->Value(SpriteFunctions::Select, Debug::Sources::GUI);
				break;
			case SpriteFunctions::Disable: // disable rendering
				sprite->Enabled(false);
				var->Value(SpriteFunctions::Select, Debug::Sources::GUI);
				break;
			default:                       // handle undefined
				DD_WARNING("Undefined case.");
				break;
		}
	};
	// init sprite
	sprite->Init(*_d2d);
}

void Engine::UnregisterObject(Object o) {
	// create search pattern
	auto search = [&o](const ObjectEx & item) { return item.Object == o; };
	size_t index;

	for (auto * objectRepo : { &_objects, &_objectsToInit })
	{ // locking scope
		auto proxy = objectRepo->GetProxy();
		// is object in the repo
		if ((index = proxy->IndexOf(search)) < proxy->Size()) {
			// remove
			OnObjectUnregistration(o);
			proxy->RemoveAt(index);
			// if already found, return
			return;
		}
	}
}

void Engine::RegisterKeyboardAction(Text::StringView16 name, KeyShortcut key, Lambda<void(Engine&)> action) {
	KeyboardAction ka;
	ka.name = name;
	ka.action = action;
	ka.key = key;
	_keyboardActions.Add(ka);
	for (auto iterator = _keyboardActions.begin(); iterator < _keyboardActions.end(); ++iterator) {
		if (_keyboardActions.Last().key.ToHash32() > iterator->key.ToHash32()) {
			Swap(*iterator, _keyboardActions.Last());
		}
	}
}

Engine::Engine(const String & engineName, Controls::Window & window)
	: _window(window)
	, _name(engineName)
	, _adapters(engineName, L"[D3D] Adapter", L"Select the device adapter. To use it, save the value and restart the app.")
	, _msgCamera           (0,                            engineName, L"[D2D] Show camera position",          L"Show camera position in render window.")
	, _msgDevice           (0,                            engineName, L"[D2D] Show device statistics",        L"Show device statistics position in render window.")
	, _msgMT               (0,                            engineName, L"[D2D] Show multi-thread activity",    L"Show multi-thread activity in render window.")
	, _background          (Colors::BLACK,                engineName, L"[D3D] Background color",              L"Background of texture into which engine is rendering.")
	, _fps                 (0,                            engineName, L"[D3D] FPS",                           L"Rendered frames per second", Debug::Sources::CODE)
	, _engineTime          (0.f,                          engineName, L"[ENG] Engine time [s]",               L"Engine time in seconds.", Debug::Sources::CODE)
	, _clearObjectRegister (                              engineName, L"[ENG] !Clear objects",                L"Remove all the engine objects from engine.")
	, _bboxVisual          (BoundingVisualizations::NONE, engineName, L"[ENG] Bounding box visualization",    L"Modes of the visualization of bounding boxes of the engine objects.")
	, _bsphereVisual       (BoundingVisualizations::NONE, engineName, L"[ENG] Bounding sphere visualization", L"Modes of the visualization of bounding spheres of the engine objects.")
	, _frustumCullingFix   (0,                            engineName, L"[ENG] Fixed frustum culling",         L"By click engine freezes camera frustum on current camera position.\nFrustum culling will be done on the mentioned frozen frustum.")
	, _forcedRendering     (false,                        engineName, L"[ENG] Forced rendering",              L"Render even without focus.")
	, _frameCount          (1,                            engineName, L"[ENG] Frame count",                   L"Number of rendered frames since engine has started", Debug::Sources::CODE)
	, _frustumCulling      (1,                            engineName, L"[ENG] Frustum culling",               L"If enabled, engine renders just objects in camera frustum, otherwise engine renders all the engine objects per each frame.")
	, _mouseMode           (MouseModes::ATTACHED,         engineName, L"[ENG] Mouse mode")
	, _mouseSensitivity    (0.005f, 0.0001f, 0.01f,       engineName, L"[ENG] Mouse sensitivity",             L"Sensitivity of mouse move to camera orientation.")
	, _keyboardSensitivity (1.f, 0.001f, 1000.f,          engineName, L"[ENG] Keyboard sensitivity",          L"Sensitivity of keyboard press to camera position.")
	, _mouseX              (0,                            engineName, L"[ENG] Mouse X",                       L"Last position of mouse click in engine window.", Debug::Sources::CODE)
	, _mouseY              (0,                            engineName, L"[ENG] Mouse Y",                       L"Last position of mouse click in engine window.", Debug::Sources::CODE)
	, _objectFunctions     (ObjectFunctions::Select, StringLocal<128>::From(engineName, " Objects"),   L"#All", L"Do operation with all the objects.", Debug::Sources::ALL ^ Debug::Sources::SERIAL)
	, _cameraMode          (CameraModes::COLOR, engineName, L"[CAM] Camera mode",           L"Use different coloring for debugging.")
	, _cameraHeightMin     (0.f,                engineName, L"[CAM] Camera height min",     L"Camera height visualization settings.")
	, _cameraHeightMax     (0.f,                engineName, L"[CAM] Camera height max",     L"Camera height visualization settings.")
	, _cameraHeightManual  (false,              engineName, L"[CAM] Camera height manual",  L"Camera height visualization settings.")
	, _renderTimeGap(0)
	, _renderTime()
	, _camera()
	, _enabled(1)
	, _keyboardProcessor()
	, _keyboardCameraTime(0)
	, _mouseMoveEnabled(false)
	, _numberOfHighlightedObjects(0)
	, _pool(L"EnginePool")
	, _asyncTasks(_pool)
{
	_statistics.Bytes     = Debug::u32(0, L"Device", L"!Bytes",                         L"Total bytes allocated on device.",                         Debug::Sources::CODE);
	_statistics.DrawCalls = Debug::u32(0, L"Device", L"!Draw Calls",                    L"Number of draw calls while last frame drawing.",           Debug::Sources::CODE);
	_statistics.Lines     = Debug::u32(0, L"Device", L"!Rendered lines",                L"Number of lines rendered while last draw call.",           Debug::Sources::CODE);
	_statistics.Points    = Debug::u32(0, L"Device", L"!Rendered points",               L"Number of points rendered while last draw call.",          Debug::Sources::CODE);
	_statistics.Triangles = Debug::u32(0, L"Device", L"!Rendered triangles",            L"Number of triangles rendered while last draw call.",       Debug::Sources::CODE);
	_statistics.CBBytes   = Debug::u32(0, L"Device", L"Constant Buffers [bytes]",       L"Total bytes allocated on device by constant buffers.",     Debug::Sources::CODE);
	_statistics.CBCount   = Debug::u32(0, L"Device", L"Constant Buffers [count]",       L"Number of constants buffers allocated on device.",         Debug::Sources::CODE);
	_statistics.CBInits   = Debug::u32(0, L"Device", L"Constant Buffers [inits]",       L"Number of init calls of constants buffers.",               Debug::Sources::CODE);
	_statistics.IBBytes   = Debug::u32(0, L"Device", L"Index Buffers [bytes]",          L"Total bytes allocated on device by index buffers.",        Debug::Sources::CODE);
	_statistics.IBCount   = Debug::u32(0, L"Device", L"Index Buffers [count]",          L"Number of constants buffers allocated on device.",         Debug::Sources::CODE);
	_statistics.IBInits   = Debug::u32(0, L"Device", L"Index Buffers [inits]",          L"Number of init calls of index buffers.",                   Debug::Sources::CODE);
	_statistics.RWSBBytes = Debug::u32(0, L"Device", L"RW Structured Buffers [bytes]",  L"Total bytes allocated on device by RW structured buffers", Debug::Sources::CODE);
	_statistics.RWSBCount = Debug::u32(0, L"Device", L"RW Structured Buffers [count]",  L"Number of constants buffers allocated on device.",         Debug::Sources::CODE);
	_statistics.RWSBInits = Debug::u32(0, L"Device", L"RW Structured Buffers [inits]",  L"Number of init calls of RW structured buffers.",           Debug::Sources::CODE);
	_statistics.SBBytes   = Debug::u32(0, L"Device", L"Structured Buffers [bytes]",     L"Total bytes allocated on device by structured buffers",    Debug::Sources::CODE);
	_statistics.SBCount   = Debug::u32(0, L"Device", L"Structured Buffers [count]",     L"Number of constants buffers allocated on device.",         Debug::Sources::CODE);
	_statistics.SBInits   = Debug::u32(0, L"Device", L"Structured Buffers [inits]",     L"Number of init calls of structured buffers.",              Debug::Sources::CODE);
	_statistics.VBBytes   = Debug::u32(0, L"Device", L"Vertex Buffers [bytes]",         L"Total bytes allocated on device by vertex buffers.",       Debug::Sources::CODE);
	_statistics.VBCount   = Debug::u32(0, L"Device", L"Vertex Buffers [count]",         L"Number of constants buffers allocated on device.",         Debug::Sources::CODE);
	_statistics.VBInits   = Debug::u32(0, L"Device", L"Vertex Buffers [inits]",         L"Number of init calls of vertex buffers.",                  Debug::Sources::CODE);

	_messaging.Alpha      =           Debug::u8Range(64, 0, 255,         engineName,               L"[Messaging] Text background transparency", L"Transparency of text background in render window.");
	_messaging.Background =      Debug::Enum<Colors>(Colors::BLACK,      engineName,               L"[Messaging] Text background color",        L"Background color of the text area in render window.");
	_messaging.TAlign     = Debug::Enum<Gpu::D2D::THAligns>(Gpu::D2D::THAligns::TLeft, engineName, L"[Messaging] Text alignment",               L"Alignment of text in render window.");
	_messaging.HAlign     = Debug::Enum<Gpu::D2D::HAligns>(Gpu::D2D::HAligns::HLeft, engineName,   L"[Messaging] Text horizontal alignment",    L"Block alignment of text in render window.");
	_messaging.VAlign     = Debug::Enum<Gpu::D2D::VAligns>(Gpu::D2D::VAligns::VTop,  engineName,   L"[Messaging] Text vertical alignment",      L"Block alignment of text in render window.");
	_messaging.Scale      =          Debug::f32Range(1.f, 0.5f, 2.f,     engineName,               L"[Messaging] Text size",                    L"Size of text in render window.");
	_messaging.Enabled    =           Debug::Boolean(true,               engineName,               L"[Messaging] Enabled",                      L"Is messaging text block enabled.");

	_console.Alpha      =           Debug::u8Range(64, 0, 255,               engineName,           L"[Console] Text background transparency",  L"Transparency of text background in render window.");
	_console.Background =      Debug::Enum<Colors>(Colors::BLACK,            engineName,           L"[Console] Text background color",         L"Background color of the text area in render window.");
	_console.Enabled    =           Debug::Boolean(false,                    engineName,           L"[Console] Enabled",                       L"Is messaging text block enabled.");
	_console.TAlign     = Debug::Enum<Gpu::D2D::THAligns>(Gpu::D2D::THAligns::TLeft,   engineName, L"[Console] Text alignment",                L"Alignment of text in render window.");
	_console.HAlign     = Debug::Enum<Gpu::D2D::HAligns>(Gpu::D2D::HAligns::HRight,    engineName, L"[Console] Text horizontal alignment",     L"Block alignment of text in render window.");
	_console.VAlign     = Debug::Enum<Gpu::D2D::VAligns>(Gpu::D2D::VAligns::VBottom,   engineName, L"[Console] Text vertical alignment",       L"Block alignment of text in render window.");
	_console.Scale      =          Debug::f32Range(1.f, 0.5f, 2.f,           engineName,           L"[Console] Text size",                     L"Size of text in render window.");
	_consolePageSize    =          Debug::i32Range(10, 5, 40,                engineName,           L"[Console] Page size",                     L"Number of lines of console.");

	_fpsStatistics.Mode = Debug::Enum<StatisticSprite::Modes>(StatisticSprite::Modes::FLUENT, engineName, L"[Stats FPS] Mode",     L"Set mode of visualization of statistics");
	_fpsStatistics.SizeX =             Debug::f32Range(0.24f, 0.1f, 1.f,        engineName,               L"[Stats FPS] Size X",   L"Set horizontal dimension of statistic chart relatively to screen space.");
	_fpsStatistics.SizeY =             Debug::f32Range(0.24f, 0.1f, 1.f,        engineName,               L"[Stats FPS] Size Y",   L"Set vertical dimension of statistic chart relatively to screen space.");
	_fpsStatistics.OffsetX =           Debug::f32Range(0.505f, 0.f, 1.f,        engineName,               L"[Stats FPS] Offset X", L"Set left-top origin of statistic chart relatively screen space");
	_fpsStatistics.OffsetY =           Debug::f32Range(0.005f, 0.f, 1.f,        engineName,               L"[Stats FPS] Offset Y", L"Set left-top origin of statistic chart relatively screen space");
	_fpsStatistics.Samples =           Debug::u32Range(240, 30, 600,            engineName,               L"[Stats FPS] Samples",  L"Number of samples of the chart");

	_frameStatistics.Mode = Debug::Enum<StatisticSprite::Modes>(StatisticSprite::Modes::HIDDEN, engineName, L"[Stats Frame] Mode",     L"Set mode of visualization of statistics");
	_frameStatistics.SizeX =             Debug::f32Range(0.24f, 0.1f, 1.f,        engineName,               L"[Stats Frame] Size X",   L"Set horizontal dimension of statistic chart relatively to screen space.");
	_frameStatistics.SizeY =             Debug::f32Range(0.24f, 0.1f, 1.f,        engineName,               L"[Stats Frame] Size Y",   L"Set vertical dimension of statistic chart relatively to screen space.");
	_frameStatistics.OffsetX =           Debug::f32Range(0.505f, 0.f, 1.f,        engineName,               L"[Stats Frame] Offset X", L"Set left-top origin of statistic chart relatively screen space");
	_frameStatistics.OffsetY =           Debug::f32Range(0.255f, 0.f, 1.f,        engineName,               L"[Stats Frame] Offset Y", L"Set left-top origin of statistic chart relatively screen space");
	_frameStatistics.Samples =           Debug::u32Range(240, 30, 600,            engineName,               L"[Stats Frame] Samples",  L"Number of samples of the chart");

	_deviceSettings.AlphaBlending = Debug::Boolean(false,                              engineName,           L"[D3D] Alpha blending", L"Enable/Disable rendering of transparent colors.");
	_deviceSettings.DepthBuffer   = Debug::Boolean(true,                               engineName,           L"[D3D] Depth buffer",   L"Enable/Disable GPU z-ordering.");
	_deviceSettings.VSync         = Debug::Boolean(true,                               engineName,           L"[D3D] VSync",          L"Enable/Disable synchronization of frame rendering with monitor frequency.");
	_deviceSettings.WireFrame     = Debug::Boolean(false,                              engineName,           L"[D3D] Wireframe",      L"Enable/Disable rendering wireframe of solid triangle models.");
	_deviceSettings.MultiSampling = Debug::Enum<Gpu::Devices::MSAA>(Gpu::Devices::MSAA::MSAA_1x, engineName, L"[D3D] Multi sampling", L"Set number of samples for rendering.");

	_objectFunctions->OnValueChanged += [this](Debug::EnumBase<ObjectFunctions> * var, Debug::Sources) {
		switch (var->Value()) {
			case ObjectFunctions::Hide: {
				auto proxy = *_objects;
				for (auto & item : *proxy)
					item.Object->Enabled(false);
				RegisterMessage("All objects are hidden.\n");
				var->Value(ObjectFunctions::Select, Debug::Sources::CODE);
			} break;
				// Remove all
			case ObjectFunctions::Release: {
				ClearObjectRegister();
				RegisterMessage("All objects are released.\n");
				var->Value(ObjectFunctions::Select, Debug::Sources::CODE);
			} break;
				// Show all
			case ObjectFunctions::Show: {
				auto proxy = *_objects;
				for (auto & item : *proxy)
					item.Object->Enabled(true);
				RegisterMessage("All objects are shown.\n");
				var->Value(ObjectFunctions::Select, Debug::Sources::CODE);
			} break;
			case ObjectFunctions::Select: {
			} break;
			default: {
				RegisterMessage("Not implemented.\n");
			} break;
		}
	};

	_time.Start();
	establishD3D();
	establishD2D();
	CompassSprite * compass = new CompassSprite(_camera);
	compass->Enabled(0);
	RegisterSprite(compass);

	_camera.TgFov({ (f32)_window->Width() / _window->Height(), 1.f });

	// asynchronous reading of keyboard
	static Concurrency::ThreadPool::TaskLocal<16> task([this]() {
		while (processInput()) { Concurrency::Thread::Sleep(10); }
	}, 0, _pool);
	//_pool.RegisterAction(task);

	// create window delegates
	Delegate<> drawDelegate([=]() {
		Draw();
	});
	Delegate<> resizeDelegate([=]() {
		_d2d = nullptr;
		_d3d->Resize(
			(size_t)_window->Client().Width,
			(size_t)_window->Client().Height
		);
		establishD2D();
		_camera.TgFov({ (f32)(size_t)_window->Client().Width / (size_t)_window->Client().Height, 1.f });
	});
	Delegate<i32, i32> mouseMoveDelegate([this](i32 x, i32 y) mutable {
		switch (+_mouseMode) {
		case MouseModes::ATTACHED:
			_camera
				.RotateX((300 - (_mouseX = x)) * _mouseSensitivity)
				.RotateY(-(300 - (_mouseY = y)) * _mouseSensitivity);
			_window->Mouse(300, 300);
			break;
		case MouseModes::REVERSED_ATTACHED:
			_camera
				.RotateX(((_mouseX = x) - 300) * _mouseSensitivity)
				.RotateY(-((_mouseY = y) - 300) * _mouseSensitivity);
			_window->Mouse(300, 300);
			break;
		case MouseModes::CLICK:
			if (_mouseMoveEnabled) {
				_camera
					.RotateX((_mouseX - x) * _mouseSensitivity)
					.RotateY(-(_mouseY - y) * _mouseSensitivity);
				_mouseX = x;
				_mouseY = y;
			}
		case MouseModes::REVERSED_CLICK:
			if (_mouseMoveEnabled) {
				_camera
					.RotateX((x - _mouseX) * _mouseSensitivity)
					.RotateY(-(y - _mouseY) * _mouseSensitivity);
				_mouseX = x;
				_mouseY = y;
			}
			break;
		case MouseModes::NONE:
		default:
			break;
		}
	});
	Delegate<> mouseDownDelegate([this]() mutable {
		switch (+_mouseMode) {
		case MouseModes::REVERSED_CLICK:
		case MouseModes::CLICK:
			_mouseX = _window->MouseX();
			_mouseY = _window->MouseY();
			_mouseMoveEnabled = true;
			break;
		case MouseModes::ATTACHED:
		case MouseModes::NONE:
		default:
			break;
		}
	});
	Delegate<> mouseUpDelegate([this]() mutable {
		switch (+_mouseMode) {
			case MouseModes::REVERSED_CLICK:
			case MouseModes::CLICK:
			_mouseMoveEnabled = false;
			break;
		case MouseModes::ATTACHED:
		case MouseModes::NONE:
		default:
			break;
		}
	});
	Delegate<i32> mouseWheel([this](i32 i) {
		if (i > 0)
			_keyboardSensitivity *= 2.f;
		else
			_keyboardSensitivity /= 2.f;
	});
	Delegate<bool &> destroyDelegate = [=](bool &) {
		_window->OnIdle -= drawDelegate;
		_window->OnSizeChanged -= resizeDelegate;
		_window->OnMouseLeftUp -= mouseUpDelegate;
		_window->OnMouseLeftDown -= mouseDownDelegate;
		_window->OnMouseMove -= mouseMoveDelegate;
		_window->OnMouseWheel -= mouseWheel;
		_window->OnClose -= destroyDelegate;
		Concurrency::Thread::Sleep(1000);
	};
	// assign window delegates
	_window->OnMouseWheel += mouseWheel;
	_window->OnIdle += drawDelegate;
	_window->OnSizeChanged += resizeDelegate;
	_window->OnMouseLeftUp += mouseUpDelegate;
	_window->OnMouseLeftDown += mouseDownDelegate;
	_window->OnMouseMove += mouseMoveDelegate;
	_window->OnClose += destroyDelegate;
	// engine delegates
	OnMouseModeChanged += [](Engine & e, MouseModes mode) {
		switch (mode) {
			case MouseModes::ATTACHED:
			case MouseModes::REVERSED_ATTACHED:
				Controls::Window::HideCursor();
				break;
			case Engine::MouseModes::CLICK:
			case MouseModes::REVERSED_CLICK:
			case MouseModes::NONE:
				Controls::Window::ShowCursor();
				break;
			default:
				break;
		}
	};
	// mouse mode
	MouseMode(MouseModes::CLICK);

	// delegate for expiration sprites
	_spriteExpirationDelegate = [this](ISprite * sprite) {
		_spriteRemove->Add(sprite);
		sprite->OnExpiration -= _spriteExpirationDelegate;
	};
	// delegate for expiration of objects
	_objectExpirationDelegate = [this](ObjectBase * object) {
		_objectsExpired->Add(object);
		object->OnObjectExpiration -= _objectExpirationDelegate;
	};
}

bool Engine::RegisterDialog(Dialog dialog) {
	if (_keyboardProcessor.RegisterWriteEvent(dialog)) {
		dialog->Reset();
		RegisterMessage(dialog);
		return true;
	}

	return false;
}

bool Engine::RegisterDialogAndWait(Dialog dialog) {
	RegisterDialog(dialog);
	dialog->Wait();
	return dialog->Successfull();
}
#pragma endregion

/************************************************************************/
/* Engine Message                                                       */
/************************************************************************/
#pragma region EngineMessage
bool Engine::ITimeMessage::iMessageEnabled(Units::Time_MicroSecondULL delta) {
	if (Time.Value) {
		if (delta < Time) {
			Time -= delta;
			return true;
		}
		else {
			Time = 0;
		}
	}
	return false;
}

Engine::BasicMessage::BasicMessage(const Gpu::D2D::Strings & text, Units::Time_MilliSecondULL time)
	: ITimeMessage(time)
	, _text(text)
{}
#pragma endregion

/************************************************************************/
/* Engine Dialog                                                        */
/************************************************************************/
#pragma region EngineDialog
void Engine::IDialog::Wait() {
	while (!IsDisposable()) {
		Concurrency::Thread::Sleep(100);
	}
}

void Engine::DialogSimpleBase::fixCursor(int cursor) {
	auto proxy = *_items;
	if (proxy->Size() == 0)
		return;

	(*proxy)[_focus]->Focused(false);
	_focus += (size_t)cursor;

	if (_focus == proxy->Size())
		_focus = 0;
	else if (_focus > proxy->Size())
		_focus = proxy->Size() - 1;

	(*proxy)[_focus]->Focused(true);
}

void Engine::DialogSimpleBase::build() {
	auto copyProxy = *_copy;
	auto itemsProxy = *_items;
	copyProxy->Clear().AppendBatch(Gpu::D2D::Fonts::DEFAULT_YELLOW_GREATER, _question, '\n');

	size_t max = 0;
	for (Item & i : *itemsProxy) {
		const auto & labelProxy = *(i->_label);
		if (max < labelProxy->Length()) {
			max = labelProxy->Length();
		}
	}

	StringLocal<16> format;
	format.AppendBatch("{0:", max, "}   ");

	for (Item & i : *itemsProxy) {
		if (i->Enabled()) {
			copyProxy->AppendFormat(Gpu::D2D::Fonts::DEFAULT_YELLOW, format.begin(), **(i->_label));
			copyProxy->Append(**(i->_formattedInput));
			copyProxy->AppendFormat(Gpu::D2D::Fonts::DEFAULT_YELLOW_SMALL, "\n{0}\n", **(i->_tooltip));
		}
	}

	_msg->Swap(*copyProxy);
}

void Engine::DialogSimpleBase::exportXmlFile() {
	// select file
	exportXmlFile(::Dialog::SaveFile(::Dialog::Extension::XML).ToString());
}

void Engine::DialogSimpleBase::exportXmlFile(const String & file) {
	// if path is valid
	if (file.Length()) {
		// create xml document
		XML::Document document(L"MyDialog");
		// fill xml by content of my objects
		ToXml(document);
		// open file
		Array<i8> data = Array<i8>::FromSequence(DD::Sequencer::Drive(document->ToStringUtf8()).Cast<i8>());
		FileSystem::Path(file).AsFile().Write(
			ArrayView((const char *)data.begin(), data.Length())
		);
	}
}

void Engine::DialogSimpleBase::importXmlFile() {
	// select file
	importXmlFile(::Dialog::OpenFile(Arrayize<::Dialog::Extension>(::Dialog::Extension::XML)).ToString());
}

void Engine::DialogSimpleBase::importXmlFile(const String & file) {
#if 0
	// if path is valid
	if (file.Length()) {
		// open file
		std::wifstream stream(file.ToStdWString());
		// read char by char, even whitespace
		stream >> std::noskipws;
		// initialize xml reader
		XML::Reader reader;
		// read data from file to xml
		reader.ReadFromStream(stream);
		// parse data from xml to object
		FromXml(reader.GetDocument());
		// fix values of dialog items
		auto itemsProxy = *_items;
		for (Item & item : *itemsProxy)
			item->iItemBuild();
		// fix dialog string
		build();
	}
#endif
}

void Engine::DialogSimpleBase::iKeyboardEventProcess(Key key, KeyModifiers modifiers) {
	switch (key) {
	case Key::F9: importXmlFile(); break;
	case Key::F10: exportXmlFile(); break;
	case Key::Up: fixCursor(-1); break;
	case Key::Down: fixCursor(+1); break;
	case Key::Enter: _successful = true; _enabled = false; break;
	case Key::Esc: _successful = false; _enabled = false; break;
	default:
		auto proxy = *_items;
		if (proxy->Size())
			(*proxy)[_focus]->iItemProcess(key);
		break;
	}
	build();
}

inline void Engine::DialogSimpleBase::iDialogReset() {
	_enabled = true;
	_successful = false;

	auto itemsProxy = *_items;
	if (itemsProxy->Size())
		(*itemsProxy)[_focus]->Focused(true);
}

void Engine::DialogSimpleBase::Add(Item item) {
	auto proxy = *_items;
	proxy->Add(item);
	proxy->Last()->iItemBuild();
	build();
}

void Engine::DialogSimpleBase::Enable(size_t i) {
	auto proxy = *_items;
	if (0 <= 1 && i < proxy->Size())
		(*proxy)[i]->Enabled(true);
}

void Engine::DialogSimpleBase::Disable(size_t i) {
	auto proxy = *_items;
	if (0 <= i && i < proxy->Size()) {
		(*proxy)[i]->Enabled(false);
		(*proxy)[i]->iItemBuild();
		build();
	}
}

void Engine::DialogSimpleBase::RemoveAt(size_t i) {
	_items->RemoveAt(i);
}

Engine::DialogSimpleBase::DialogSimpleBase(const String & question, const Array<Item> & items)
	: _question(question)
	, _focus(0)
	, _items() {
	auto proxy = *_items;
	Reset();

	proxy->AddIterable(items);
	for (auto & i : *proxy)
		i->iItemBuild();

	build();
}
#pragma endregion

/************************************************************************/
/* EngineDialogSimple                                                   */
/************************************************************************/
#pragma region EngineDialogSimple
/************************************************************************/
/* Engine::DialogSimple::IItem                                          */
/************************************************************************/

Engine::DialogSimpleBase::IItem::IItem(const String & label, const String & tooltip, const String & input)
	: _focused(false)
	, _enabled(true)
	, _label(label)
	, _tooltip(tooltip)
	, _input(input)
{}

/************************************************************************/
/* Engine::DialogSimple::FloatBase                                      */
/************************************************************************/

void Engine::DialogSimpleBase::FloatBase::iItemBuild() {
	auto inputProxy = *_input;
	auto formattedInputProxy = *_formattedInput;
	_value = inputProxy->ToDouble();
	formattedInputProxy->Clear();

	StringLocal<128> left;
	for (size_t i = 0; i < _cursor; ++i) {
		left.Append((*inputProxy)[i]);
	}
	if (Focused()) {
		left.Append(CURSOR);
	}
	for (size_t i = _cursor; i < inputProxy->Length(); ++i) {
		left.Append((*inputProxy)[i]);
	}

	formattedInputProxy->AppendFormat(
		"{0:-24} {1:-16}",
		left,
		StringLocal<32>::From('[', _value, ']')
	);

	if (Focused()) {
		formattedInputProxy->Color(Gpu::D2D::Fonts::DEFAULT_GREEN);
	}
}

void Engine::DialogSimpleBase::FloatBase::iItemProcess(Key key) {
	if (!Enabled())
		return;

	auto proxyInput = *_input;
	switch (key) {
	case Key::Left:
		if (--_cursor > proxyInput->Length()) {
			_cursor = proxyInput->Length();
		}
		break;
	case Key::Right:
		if (++_cursor > proxyInput->Length()) {
			_cursor = 0;
		}
		break;
	case Key::Backspace:
		if (_cursor) {
			proxyInput->RemoveChar(--_cursor);
		}
		break;
	case Key::Delete:
		if (_cursor < proxyInput->Length()) {
			proxyInput->RemoveChar(_cursor);
		}
		break;
	case Key::Dot:
	case Key::NumPoint:
		if (!proxyInput->Contains('.')) {
			proxyInput->InsertChar(_cursor++, '.');
		}
		break;
	case Key::Minus:
		if (!proxyInput->Contains('-') && proxyInput->Length()) {
			proxyInput->Prepend('-');
		}
		else {
			proxyInput->RemoveFirst();
		}
		break;
	default:
		if (Key::Num0 <= key && key <= Key::Num9) {
			proxyInput->InsertChar(_cursor++, (wchar_t)(L'0' + (wchar_t)(key - Key::Num0)));
		}
		else if (Key::TK0 <= key && key <= Key::TK9) {
			proxyInput->InsertChar(_cursor++, (wchar_t)(L'0' + (wchar_t)(key - Key::TK0)));
		}
		break;
	}
	iItemBuild();
}

Engine::DialogSimpleBase::FloatBase::FloatBase(const String & label, const String & tooltip, const f64 & value)
	: IItem(label, tooltip, StringLocal<32>::From(value))
	, _value(value) {
	auto inputProxy = *_input;
	_cursor = inputProxy->Length();
}

/************************************************************************/
/* Engine::DialogSimple::SelectBase                                     */
/************************************************************************/

void Engine::DialogSimpleBase::SelectBase::iItemProcess(Key key) {
	if (!Enabled())
		return;

	auto inputProxy = *_input;
	inputProxy->TryCast(_selected);

	switch (key) {
	case Key::Left:
		if (--_selected > _options.Length()) _selected = _options.Length() - 1;
		break;
	case Key::Right:
		if (++_selected >= _options.Length()) _selected = 0;
		break;
	}

	_input->Clear().Append(_selected);
	iItemBuild();
}

void Engine::DialogSimpleBase::SelectBase::iItemBuild() {
	auto proxy = *_formattedInput;

	if (_options.Length()) {
		proxy->Clear();
		for (size_t i = 0; i < _selected; ++i) {
			proxy->Append(Gpu::D2D::Fonts::DEFAULT_RED, _options[i]);
			proxy->Append(_delimiter);
		}

		proxy->Append(Focused() ? Gpu::D2D::Fonts::DEFAULT_GREEN : Gpu::D2D::Fonts::DEFAULT_YELLOW, _options[_selected]);
		proxy->Append(_delimiter);

		for (size_t i = _selected + 1, m = _options.Length(); i < m; ++i) {
			proxy->Append(Gpu::D2D::Fonts::DEFAULT_RED, _options[i]);
			proxy->Append(_delimiter);
		}
	}
}

Engine::DialogSimpleBase::SelectBase::SelectBase(size_t selected, const String & label, const String & tooltip, options_t && options)
	: IItem(label, tooltip, String())
	, _selected(selected)
	, _options(options)
	, _delimiter("    ") {}

/************************************************************************/
/* Engine::DialogSimple::IntegerBase                                    */
/************************************************************************/

void Engine::DialogSimpleBase::IntegerBase::iItemBuild() {
	auto formattedInputProxy = *_formattedInput;
	auto inputProxy = *_input;

	_value = inputProxy->ToInteger();
	formattedInputProxy->Clear();

	StringLocal<128> left;

	for (size_t i = 0; i < _cursor; ++i) {
		left.Append((*inputProxy)[i]);
	}
	if (Focused()) {
		left.Append(CURSOR);
	}
	for (size_t i = _cursor; i < inputProxy->Length(); ++i) {
		left.Append((*inputProxy)[i]);
	}

	formattedInputProxy->AppendFormat(
		"{0:-24} {1:-16}",
		left,
		StringLocal<32>::From('[', _value, ']')
	);

	if (Focused()) {
		formattedInputProxy->Color(Gpu::D2D::Fonts::DEFAULT_GREEN);
	}
}

void Engine::DialogSimpleBase::IntegerBase::iItemProcess(Key key) {
	if (!Enabled())
		return;

	auto proxyInput = *_input;
	switch (key) {
	case Key::Left:
		if (--_cursor > proxyInput->Length()) {
			_cursor = proxyInput->Length();
		}
		break;
	case Key::Right:
		if (++_cursor > proxyInput->Length()) {
			_cursor = 0;
		}
		break;
	case Key::Backspace:
		if (_cursor) proxyInput->RemoveChar(--_cursor);
		break;
	case Key::Delete:
		if (_cursor < proxyInput->Length()) {
			proxyInput->RemoveChar(_cursor);
		}
		break;
	case Key::Minus:
		if (!proxyInput->Contains('-') && proxyInput->Length()) {
			proxyInput->Prepend('-');
		}
		else {
			proxyInput->RemoveFirst();
		}
		break;
	default:
		if (Key::Num0 <= key && key <= Key::Num9) {
			proxyInput->InsertChar(_cursor++, (wchar_t)(L'0' + (wchar_t)(key - Key::Num0)));

		}
		else if (Key::TK0 <= key && key <= Key::TK9) {
			proxyInput->InsertChar(_cursor++, (wchar_t)(L'0' + (wchar_t)(key - Key::TK0)));
		}
		break;
	}
	iItemBuild();
}

Engine::DialogSimpleBase::IntegerBase::IntegerBase(const String & label, const String & tooltip, const i64 & value)
	: IItem(label, tooltip, StringLocal<32>::From(value))
	, _cursor(0)
	, _value(value)
{}
#pragma endregion

