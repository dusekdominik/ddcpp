#include <DD/Dialog.h>
#include <DD/Debug.h>
#include <Windows.h>      // GetOpenFileName
#include <shlobj_core.h>  // SHBrowseForFolder

#pragma comment(lib, "Comdlg32.lib") // GetOpenFileName
#pragma comment(lib, "Shell32.lib")  // SHBrowseForFolder

using namespace DD;

const Dialog::Extension Dialog::Extension::LAS = { "Point Cloud LAS", "las" };
const Dialog::Extension Dialog::Extension::XML = { "Extensible Markup Language", "xml" };
const Dialog::Extension Dialog::Extension::PNG = { "Portable Network Graphics", "png" };
const Dialog::Extension Dialog::Extension::ANY = { "Any file", "*" };

void Dialog::parseExtension(const Extension & extension, wchar_t * extBuffer, size_t & pointer) {
	for (const wchar_t & letter : extension.Title) {
		extBuffer[pointer++] = letter;
	}
	extBuffer[pointer++] = L'\0';
	extBuffer[pointer++] = L'*';
	extBuffer[pointer++] = L'.';
	for (const wchar_t & letter : extension.Definition) {
		extBuffer[pointer++] = letter;
	}
	extBuffer[pointer++] = L'\0';
}

void Dialog::parseExtensions(const Array<Extension> & extensions, wchar_t * extBuffer) {
	size_t pointer = 0;
	for (auto & extension : extensions) {
		parseExtension(extension, extBuffer, pointer);
	}
	extBuffer[pointer++] = L'\0';
}

Array<FileSystem::Path> Dialog::OpenFiles(const Array<Extension> & extensions) {
	wchar_t buffer[8192] = { 0 };
	wchar_t extBuffer[512];
	parseExtensions(extensions, extBuffer);

	OPENFILENAME q = { 0 };
	q.lStructSize = sizeof(q);
	q.hwndOwner = NULL;
	q.lpstrFile = buffer;
	q.lpstrFile[0] = '\0';
	q.nMaxFile = 8192;
	q.lpstrFilter = extBuffer;
	q.nFilterIndex = 1;
	q.lpstrFileTitle = NULL;
	q.nMaxFileTitle = 0;
	q.lpstrInitialDir = NULL;
	q.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_NOCHANGEDIR | OFN_EXPLORER | OFN_ALLOWMULTISELECT;

	if (GetOpenFileName(&q)) {
		Text::StringView16 directory = Text::StringViewNull(buffer);
		StringLocal<256> path;

		// count number of files for file allocation
		size_t numberOfFiles = 0;
		for (size_t fileIndex = 1 + directory.Length(); buffer[fileIndex]; ++numberOfFiles) {
			while (buffer[fileIndex++]);
		}

		if (numberOfFiles) {
			// copy data into array
			Array<FileSystem::Path> result(numberOfFiles);
			for (size_t fileIndex = 1 + directory.Length(), i = 0; i < numberOfFiles; ++i) {
				result[i] = path.Clear().AppendBatch(directory, '\\', buffer + fileIndex).View();
				while (buffer[fileIndex++]);
			}

			return result;
		}
		else {
			Array<FileSystem::Path> result(1);
			result[0] = directory;
			return result;
		}
	}

	return Array<FileSystem::Path>();
}

Array<FileSystem::Path> Dialog::OpenFiles(const Extension & extension) {
	return OpenFiles(Arrayize<Extension>(extension));
}


FileSystem::Path Dialog::OpenFile(const Array<Extension> & extensions) {
	wchar_t buffer[512];
	wchar_t extBuffer[512];
	parseExtensions(extensions, extBuffer);

	OPENFILENAME q;
	ZeroMemory(&q, sizeof(q));
	q.lStructSize = sizeof(q);
	q.hwndOwner = NULL;
	q.lpstrFile = buffer;
	q.lpstrFile[0] = '\0';
	q.nMaxFile = 512;
	q.lpstrFilter = extBuffer;
	q.nFilterIndex = 1;
	q.lpstrFileTitle = NULL;
	q.nMaxFileTitle = 0;
	q.lpstrInitialDir = NULL;
	q.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_NOCHANGEDIR | OFN_EXPLORER;

	if (GetOpenFileName(&q))
		return buffer;
	else
		return ::DD::String();
}

FileSystem::Path DD::Dialog::OpenFile(const Extension & extension) {
	return OpenFile(Arrayize<Extension>(extension));
}


FileSystem::Path Dialog::SaveFile(const Extension & extension) {
	wchar_t buffer[512];
	wchar_t extBuffer[512];
	size_t p(0);
	parseExtension(extension, extBuffer, p);

	OPENFILENAME q;
	ZeroMemory(&q, sizeof(q));
	q.lStructSize = sizeof(q);
	q.hwndOwner = NULL;
	q.lpstrFile = buffer;
	q.lpstrFile[0] = '\0';
	q.nMaxFile = 512;
	q.lpstrFilter = extBuffer;
	q.nFilterIndex = 1;
	q.lpstrFileTitle = NULL;
	q.nMaxFileTitle = 0;
	q.lpstrInitialDir = NULL;
	q.lpstrDefExt = extension.Definition;

	q.Flags = OFN_NOCHANGEDIR | OFN_OVERWRITEPROMPT;

	if (GetSaveFileName(&q))
		return buffer;

	return FileSystem::Path();
}

FileSystem::Path DD::Dialog::OpenDir() {
	wchar_t path[MAX_PATH];
	BROWSEINFO q = {0};

	q.ulFlags = BIF_NEWDIALOGSTYLE;
	q.iImage = -1;

	LPITEMIDLIST list = SHBrowseForFolder(&q);
	if (list) {
		SHGetPathFromIDList(list, path);
		return Text::StringViewNull(path);
	}

	return FileSystem::Path();
}
