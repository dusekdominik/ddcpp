#include <DD/Svg.h>
#include <DD/Macros.h>
#include <DD/Vector.h>
#include <DD/Matrices.h>
#include <DD/Svg/Primitives.h>
#include <DD/Svg/MatrixTransformation.h>
#include <DD/Svg/Transformation.h>

using namespace DD;
using namespace DD::Svg;

/** Helper struct for argument and function values. */
struct Record {
	/** Argument value. */
	f64 TValue;
	/** Point at given TValue. */
	double2 Point;
};

/**
 * Helper for refining curves with respect to angles of discretized polylines.
 */
template<typename CURVE_COMPUTER>
struct CornerRefiner {
	/**
	 * Angle limit in cosine measure, cos of an angle.
	 * cos(1 deg) ~ 0.9999
	 */
	static constexpr f64 DefaultLimit = 0.9999;
	/** Computer of curve. */
	CURVE_COMPUTER Computer;
	f64 Limit = DefaultLimit;

	/** Check corner, either put them to buffer or recursively continue refining.  */
	void RefineCorner(const Record & a, const Record & b, const Record & c, List<double2> & dataTarget) {
		double2 ab = b.Point - a.Point;
		double2 bc = c.Point - b.Point;
		// refine if necessary, cos(1 deg) ~ 0.9999
		if (cos(ab, bc) < Limit) {
			RefineCorner(a, GetMiddlePoint(a, b), b, dataTarget);
			RefineCorner(b, GetMiddlePoint(b, c), c, dataTarget);
		}
		// otherwise put to the points
		else {
			if (dataTarget.Size() == 0 || dataTarget.Last() != a.Point)
				dataTarget.Add(a.Point);
			dataTarget.Add(b.Point);
		}
	}
	/** Get list of points. */
	void GetAngleLimitedDiscretization(List<double2> & dataTarget) {
		Record begin = Computer.GetPoint(0.0);
		Record end = Computer.GetPoint(1.0);
		RefineCorner(begin, GetMiddlePoint(begin, end), end, dataTarget);
		dataTarget.Add(end.Point);
	}
	/** Compute central point from two points */
	Record GetMiddlePoint(const Record & a, const Record & b) {
		return Computer.GetPoint((a.TValue + b.TValue) * 0.5);
	}
};

/** Compute Bezier quadric curve for t in 0..1. */
struct QuadricBezierComputer {
	/** Start point of the curve. */
	double2 StartPoint;
	/** Control point defining the curve. */
	double2 ControlPoint;
	/** Target point of the curve. */
	double2 TargetPoint;

	/** Get points of the curve for t in [0..1]. */
	Record GetPoint(f64 t) {
		DD::f64 t2 = t * t;
		DD::f64 s = (1 - t);
		DD::f64 s2 = s * s;
		return { t, (StartPoint * s2 + ControlPoint * (2 * s * t) + TargetPoint * t2) };
	}
};

using CornerRefinerQuadric = CornerRefiner<QuadricBezierComputer>;

/** Compute Bezier cubic curve for t in [0..1]. */
struct CubicBezierComputer {
	/** Start point of the curve. */
	double2 StartPoint;
	/** Control point defining the curve. */
	double2 ControlPointStart;
	/** Control point defining the curve. */
	double2 ControlPointTarget;
	/** Target point of the curve. */
	double2 TargetPoint;

	/** Get points of the curve for t in [0..1]. */
	Record GetPoint(f64 t) {
		DD::f64 t2 = t * t;
		DD::f64 t3 = t2 * t;
		DD::f64 s = (1 - t);
		DD::f64 s2 = s * s;
		DD::f64 s3 = s2 * s;
		return Record{
			t,
			StartPoint * s3 +
			ControlPointStart * (3 * s2 * t) +
			ControlPointTarget * (3 * s * t2) +
			TargetPoint * t3,
		};
	}
};

using CornerRefinerCubic = CornerRefiner<CubicBezierComputer>;

/** Computer for svg arcs. */
struct ArcComputer {                               // sweep = 1 arc goes from line p0->p1 CCW
	// sweep = 0 arc goes from line p0->p1 CW
public:                             // larc is unused if |da|=DD::Math::ConstantsD::Pi
	double2 rotatedCenter;
	/** Ellipse half-axes.  */
	double2 radius;
	/** */
	f64 a0, a1, da, ang;      // sx,sy rotated center by ang
	/** Safety check value. */
	f64 ACC_ZERO_ANG = Math::Deg2Rad(0.000001);
	/** */
	f64 MCos, MSin;
	/** Get points of the curve for t in [0..1]. */
	Record GetPoint(f64 t);

public: // constructor
	/** */
	ArcComputer(const double2 & startPoint, const DD::Svg::Path::PathCommand & cmd);
}
;
Record ArcComputer::GetPoint(f64 t) {
	f64 tt = a0 + (da * t);
	f64 xx = rotatedCenter.x + radius.x * Math::Cos(tt);
	f64 yy = rotatedCenter.y + radius.y * Math::Sin(tt);
	f64 x = xx * MCos - yy * MSin;
	f64 y = xx * MSin + yy * MCos;

	return { t, { x, y } };
}

ArcComputer::ArcComputer(const double2 & startPoint, const DD::Svg::Path::PathCommand & cmd) {
	f64 ax, ay, bx, by;            // body
	f64 vx, vy, l, db;
	i32 sweep = cmd.Arc.LargeArcFlag ? !cmd.Arc.SweepFlag : cmd.Arc.SweepFlag;
	radius = cmd.Arc.Radius;
	ang = DD::Math::ConstantsD::Pi - Math::Deg2Rad(cmd.Arc.Rotation);
	f64 e = cmd.Arc.Radius.x / cmd.Arc.Radius.y;
	f64 c = Math::Cos(ang);
	f64 s = Math::Sin(ang);
	MCos = Math::Cos(-ang);
	MSin = Math::Sin(-ang);
	ax = startPoint.x * c - startPoint.y * s;
	ay = startPoint.x * s + startPoint.y * c;
	bx = cmd.Arc.Target.x * c - cmd.Arc.Target.y * s;
	by = cmd.Arc.Target.x * s + cmd.Arc.Target.y * c;

	ay *= e;                  // transform to circle
	by *= e;

	rotatedCenter.x = 0.5 * (ax + bx);         // mid point between A,B
	rotatedCenter.y = 0.5 * (ay + by);
	vx = (ay - by);
	vy = (bx - ax);
	l = (cmd.Arc.Radius.x * cmd.Arc.Radius.x) / ((vx * vx) + (vy * vy)) - 0.25;
	if (l < 0) l = 0;
	l = Math::Sqrt(l);
	vx *= l;
	vy *= l;

	if (sweep) {
		rotatedCenter.x += vx;
		rotatedCenter.y += vy;
	}
	else {
		rotatedCenter.x -= vx;
		rotatedCenter.y -= vy;
	}

	a0 = Math::ArcTg2(ay - rotatedCenter.y, ax - rotatedCenter.x);
	a1 = Math::ArcTg2(by - rotatedCenter.y, bx - rotatedCenter.x);
	rotatedCenter.y = rotatedCenter.y / e;


	da = a1 - a0;
	if (Math::Abs(Math::Abs(da) - DD::Math::ConstantsD::Pi) <= ACC_ZERO_ANG)       // half arc is without larc and sweep is not working instead change a0,a1
	{
		db = (0.5 * (a0 + a1)) - Math::ArcTg2(by - ay, bx - ax);
		while (db < -DD::Math::ConstantsD::Pi)
			db += DD::Math::ConstantsD::Pi * 2;     // db<0 CCW ... sweep=1
		while (db > +DD::Math::ConstantsD::Pi)
			db -= DD::Math::ConstantsD::Pi * 2;     // db>0  CW ... sweep=0

		sweep = 0;
		if ((db < 0.0) && (!sweep))
			sweep = 1;

		if ((db > 0.0) && (sweep))
			sweep = 1;

		if (sweep)
		{
			if (da >= 0.0)
				a1 -= DD::Math::ConstantsD::Pi * 2;
			if (da < 0.0)
				a0 -= DD::Math::ConstantsD::Pi * 2;
		}
	}
	else if (cmd.Arc.LargeArcFlag)
	{
		if ((da < DD::Math::ConstantsD::Pi) && (da >= 0.0))
			a1 -= DD::Math::ConstantsD::Pi * 2;
		if ((da > -DD::Math::ConstantsD::Pi) && (da < 0.0))
			a0 -= DD::Math::ConstantsD::Pi * 2;
	}
	else {                       // small arc
		if (da > +DD::Math::ConstantsD::Pi)
			a1 -= DD::Math::ConstantsD::Pi * 2;
		if (da < -DD::Math::ConstantsD::Pi)
			a0 -= DD::Math::ConstantsD::Pi * 2;
	}

	da = a1 - a0;
}

using CornerRefinerArc = CornerRefiner<ArcComputer>;


Svg::Path::DecompositionSequencer Svg::Path::Decompose() const {
	return DecompositionSequencer(this);
}

void Svg::Path::Absolutize() {
	// initial point
	double2 currentPoint = 0.0;
	double2 closingPoint = 0.0;
	// convert all
	for (PathCommand & command : Commands) {
		if (command.Mode == PathCommand::Modes::Absolute) {
			switch (command.Type) {
			case PathCommand::Types::Arc:
				currentPoint = command.Arc.Target;
				break;
			case PathCommand::Types::Cubic:
				currentPoint = command.Cubic.Target;
				break;
			case PathCommand::Types::Quadric:
				currentPoint = command.Quadric.Target;
				break;
			case PathCommand::Types::Horizontal:
				currentPoint.x = command.Horizontal.TargetX;
				break;
			case PathCommand::Types::Vertical:
				currentPoint.y = command.Vertical.TargetY;
				break;
			case PathCommand::Types::Line:
				currentPoint = command.Line.Target;
				break;
			case PathCommand::Types::Move:
				currentPoint = command.Move.Target;
				closingPoint = command.Move.Target;
				break;
			case PathCommand::Types::Close:
				currentPoint = closingPoint;
				break;
			default:
				throw;
			}
		}
		else {
			// absolutize relative command
			command.Mode = PathCommand::Modes::Absolute;

			switch (command.Type) {
			case PathCommand::Types::Arc:
				command.Arc.Target = (currentPoint += command.Arc.Target);
				break;
			case PathCommand::Types::Cubic:
				command.Cubic.ControlPointStart += currentPoint;
				command.Cubic.ControlPointTarget += currentPoint;
				command.Cubic.Target = (currentPoint += command.Cubic.Target);
				break;
			case PathCommand::Types::Quadric:
				command.Quadric.ControlPoint += currentPoint;
				command.Quadric.Target = (currentPoint += command.Quadric.Target);
				break;
			case PathCommand::Types::Horizontal:
				command.Horizontal.TargetX = (currentPoint.x += command.Horizontal.TargetX);
				break;
			case PathCommand::Types::Vertical:
				command.Vertical.TargetY = (currentPoint.y += command.Vertical.TargetY);
				break;
			case PathCommand::Types::Line:
				command.Line.Target = (currentPoint += command.Line.Target);
				break;
			case PathCommand::Types::Move:
				command.Move.Target = (currentPoint += command.Move.Target);
				closingPoint = command.Move.Target;
				break;
			case PathCommand::Types::Close:
				currentPoint = closingPoint;
				break;
			default:
				throw;
			}
		}
	}
}

void Svg::Path::AddArcAbsolute(
	const double2 & point,
	const double2 & radius,
	f64 rotation,
	bool sweepFlag,
	bool largeArcFlag
) {
	// create cmd
	PathCommand cmd(PathCommand::Types::Arc, PathCommand::Modes::Absolute);
	// fill cmd params
	cmd.Arc.Target = point;
	cmd.Arc.Radius = radius;
	cmd.Arc.Rotation = rotation;
	cmd.Arc.SweepFlag = sweepFlag;
	cmd.Arc.LargeArcFlag = largeArcFlag;

	// add to collection
	Commands.Add(cmd);
}


void Svg::Path::AddArcRelative(
	const double2 & point,
	const double2 & radius,
	f64 rotation,
	bool sweepFlag,
	bool largeArcFlag
) {
	// create cmd
	PathCommand cmd(PathCommand::Types::Arc, PathCommand::Modes::Relative);
	// fill cmd params
	cmd.Arc.Target = point;
	cmd.Arc.Radius = radius;
	cmd.Arc.Rotation = rotation;
	cmd.Arc.SweepFlag = sweepFlag;
	cmd.Arc.LargeArcFlag = largeArcFlag;

	// add to collection
	Commands.Add(cmd);
}


void Svg::Path::AddCircleAbsolute(const double2 & centerPoint, f64 radius) {
	double2 L = centerPoint - double2(radius, 0);
	double2 R = centerPoint + double2(radius, 0);
	double2 r(radius, radius);
	AddMoveAbsolute(L);
	AddArcAbsolute(R, r, 0.0, 0, 0);
	AddArcAbsolute(L, r, 0.0, 0, 0);
}


void Svg::Path::AddCircleRelative(const double2 & centerPoint, f64 radius) {
	double2 r(radius, radius);
	AddMoveRelative(centerPoint - double2(radius, 0));
	AddArcRelative({ radius * 2.0, 0 }, r, 0.0, 0, 0);
	AddArcRelative({ radius * -2.0, 0 }, r, 0.0, 0, 0);
}


void Svg::Path::AddLineAbsolute(const double2 & target) {
	PathCommand cmd(PathCommand::Types::Line, PathCommand::Modes::Absolute);
	cmd.Line.Target = target;
	Commands.Add(cmd);
}


void Svg::Path::AddLineRelative(const double2 & target) {
	PathCommand cmd(PathCommand::Types::Line, PathCommand::Modes::Relative);
	cmd.Line.Target = target;
	Commands.Add(cmd);
}


void Svg::Path::AddHorizontalRelative(f64 x) {
	PathCommand cmd(PathCommand::Types::Horizontal, PathCommand::Modes::Relative);
	cmd.Horizontal.TargetX = x;
	Commands.Add(cmd);
}

void DD::Svg::Path::AddHorizontalAbsolute(f64 x) {
	PathCommand cmd(PathCommand::Types::Horizontal, PathCommand::Modes::Absolute);
	cmd.Horizontal.TargetX = x;
	Commands.Add(cmd);
}

void Svg::Path::AddVerticalRelative(f64 y) {
	PathCommand cmd(PathCommand::Types::Vertical, PathCommand::Modes::Relative);
	cmd.Vertical.TargetY = y;
	Commands.Add(cmd);
}

void DD::Svg::Path::AddVerticalAbsolute(f64 y) {
	PathCommand cmd(PathCommand::Types::Vertical, PathCommand::Modes::Absolute);
	cmd.Vertical.TargetY = y;
	Commands.Add(cmd);
}


void Svg::Path::AddMoveAbsolute(const double2 & target) {
	PathCommand cmd(PathCommand::Types::Move, PathCommand::Modes::Absolute);
	cmd.Move.Target = target;
	Commands.Add(cmd);
}


void Svg::Path::AddMoveRelative(const double2 & target) {
	PathCommand cmd(PathCommand::Types::Move, PathCommand::Modes::Relative);
	cmd.Move.Target = target;
	Commands.Add(cmd);
}


void Svg::Path::AddQuadricRelative(
	const double2 & controlPoint,
	const double2 & target
) {
	PathCommand cmd(PathCommand::Types::Quadric, PathCommand::Modes::Relative);
	cmd.Quadric.ControlPoint = controlPoint;
	cmd.Quadric.Target = target;
	Commands.Add(cmd);
}


void DD::Svg::Path::AddQuadricAbsolute(const double2 & controlPoint, const double2 & target) {
	PathCommand cmd(PathCommand::Types::Quadric, PathCommand::Modes::Absolute);
	cmd.Quadric.ControlPoint = controlPoint;
	cmd.Quadric.Target = target;
	Commands.Add(cmd);
}

void Svg::Path::AddRectangle(const Math::Interval2D & box) {
	AddMoveAbsolute(box.Min);
	AddLineAbsolute({ box.Min.x, box.Max.y });
	AddLineAbsolute(box.Max);
	AddLineAbsolute({ box.Max.x, box.Min.y });
	AddClose();
}

void DD::Svg::Path::AddRectangleStrip(const Math::Interval2D & box, const double2 & offset, i32 rectangles) {
	// internal copy
	Math::Interval2D rectangle = box;

	// generate
	for (i32 i = 0; i < rectangles; ++i) {
		AddRectangle(rectangle);
		rectangle += offset;
	}
}

DD::Sequencer::Driver<DD::Svg::Path::PolylineSequencer> Svg::Path::ReadPolylines() const {
	return PolylineSequencer(*this);
}

DD::Sequencer::Driver<DD::Svg::Path::PolylineSequencer> Svg::Path::ReadPolylines(
	f64 yCorrection, double2 offset, double2 scale, f64 angleLimit
) const {
	return PolylineSequencer(*this, yCorrection, offset, scale, angleLimit);
}

void Svg::Path::AddSquare(const double2 & center, f64 halfSize) {
	// left-top
	double2 point = center - halfSize;
	AddMoveAbsolute(point);
	// right-top
	point.x += 2 * halfSize;
	AddLineAbsolute(point);
	// right-bottom
	point.y += 2 * halfSize;
	AddLineAbsolute(point);
	// left-bottom
	point.x -= 2 * halfSize;
	AddLineAbsolute(point);
	// close the square
	AddClose();
}


void Svg::Path::AddCross(const double2 & point, f64 radius) {
	AddLine({ point - radius, point + radius });
	AddLine({ point + double2(-radius, radius), point + double2(radius, -radius) });
}


void Svg::Path::AddLine(const Math::LineSegment2D & line) {
	AddMoveAbsolute(line.A);
	AddLineAbsolute(line.B);
}


void DD::Svg::Path::AddLineStrip(const ArrayView1D<const double2> & lineStrip) {
	if (lineStrip.Length() == 0)
		return;

	AddMoveAbsolute(lineStrip.First());
	for (const double2 & point : lineStrip.SubView(1))
		AddLineAbsolute(point);
}


void DD::Svg::Path::AddPolygon(const ArrayView1D<const double2> & lineStrip) {
	if (lineStrip.Length() == 0)
		return;

	AddLineStrip(lineStrip);
	if (lineStrip.First() != lineStrip.Last())
		AddClose();
}


void Svg::Path::AddParabola(const Math::Parabola2D & parabola, const double2 & aabox) {
	// if parabola is degenerated, draw line
	if (parabola.IsDegenerated()) {
		AddLine({
			parabola.Focus - parabola.Directrix.Normal * 100,
			parabola.Focus + parabola.Directrix.Normal * 100 }
		);
		return;
	}

	// handle aabox values
	f64 q = parabola.FocusDistance();
	f64 w = Min(aabox.x * 0.5, sqrt(Math::Abs(2 * q * aabox.y - q * 2)));

	// project aabox (w) onto directrix
	double2 direction(parabola.Directrix.Normal.y, -parabola.Directrix.Normal.x);
	const double2 & zero = parabola.Zero();
	const double2 & larg = zero - direction * w;
	const double2 & rarg = zero + direction * w;

	// compute start and endpoint
	const double2 & start = parabola.PointAt(larg);
	const double2 & end = parabola.PointAt(rarg);

	// compute middle point as intersection of tangents
	double2 middle;
	Math::Line2D ltangent = parabola.TangentAt(larg);
	Math::Line2D rtangent = parabola.TangentAt(rarg);
	// is intersection valid
	if (Math::Line2D::PointIntersection({ ltangent, rtangent }, middle)) {
		// move to the start point
		AddMoveAbsolute(start);
		AddQuadricAbsolute(middle, end);
	}
}


void Svg::Path::AddPath(
	const Path & path,
	const double2x2 & transformation,
	const double2 & offset
) {
	size_t index = Commands.Size();
	Commands.AddIterable(path.Commands);
	if (offset != 0.0 || transformation != double2x2(DiagonalMatrix<f64, 2>(1.0)))
		Transform(transformation, offset, index);
}


void Svg::Path::AddPath(const Path & path, const double2 & offset) {
	AddPath(path, DiagonalMatrix<f64, 2>(1), offset);
}


void Svg::Path::AddClose() {
	Commands.Add(PathCommand(Svg::Path::PathCommand::Types::Close, Svg::Path::PathCommand::Absolute));
}


void Svg::Path::Mirror() {
	// mirror x - fill horizontal space
	AddPath(*this, DiagonalMatrix<f64, 2>(-1.0, 1.0));
	// mirror y - fill vertical space
	AddPath(*this, DiagonalMatrix<f64, 2>(1.0, -1.0));
}


void Svg::Path::Transform(
	const double2x2 & transformation,
	const double2 & offset,
	size_t index,
	size_t length
) {
	// clamp length to admissible value
	length = Min(index + length, Commands.Size());
	// for each command since the given index
	for (size_t i = index; i < length; ++i) {
		// get command reference
		PathCommand & command = Commands[i];
		// offset positioning with respect to command mode
		double2 commandOffset = (command.Mode == PathCommand::Modes::Absolute) ? offset : 0.0;

		// according to the command type, do the transformation
		switch (command.Type) {
		case PathCommand::Types::Arc:
			DD_TODO_STAMP("Svg::Transformation - Check this scope https://stackoverflow.com/questions/5149301/baking-transforms-into-svg-path-element-commands");
			command.Arc.Target = transformation * command.Arc.Target + commandOffset;
			command.Arc.Radius = transformation * command.Arc.Radius;
			break;
		case PathCommand::Types::Cubic:
			command.Cubic.ControlPointStart = transformation * command.Cubic.ControlPointStart + commandOffset;
			command.Cubic.ControlPointTarget = transformation * command.Cubic.ControlPointTarget + commandOffset;
			command.Cubic.Target = transformation * command.Cubic.Target + commandOffset;
			break;
		case PathCommand::Types::Quadric:
			command.Quadric.ControlPoint = transformation * command.Quadric.ControlPoint + commandOffset;
			command.Quadric.Target = transformation * command.Quadric.Target + commandOffset;
			break;
		case PathCommand::Types::Horizontal:
			command.Type = PathCommand::Types::Line;
			command.Line.Target = transformation * double2(command.Horizontal.TargetX, 0) + commandOffset;
			break;
		case PathCommand::Types::Vertical:
			DD_TODO_STAMP("this needs revision.");
			command.Type = PathCommand::Types::Line;
			command.Line.Target = transformation * double2(0, command.Vertical.TargetY) + commandOffset;
			break;
		case PathCommand::Types::Line:
			command.Line.Target = transformation * command.Line.Target + commandOffset;
			break;
		case PathCommand::Types::Move:
			command.Move.Target = transformation * command.Move.Target + commandOffset;
			break;
		case PathCommand::Types::Close: break;
		}
	}
}


void Svg::Path::ToString(String & string) const {
	// for each command in the path
	for (const PathCommand & command : Commands) {

		// stringize according to the command type
		switch (command.Type) {
		case PathCommand::Types::Arc:
			string.AppendFormat(
				"{0} {1} {2} {3} {4} {5} {6} {7}",
				/*0*/ command.Mode == PathCommand::Modes::Absolute ? 'A' : 'a',
				/*1*/ command.Arc.Radius.x,
				/*2*/ command.Arc.Radius.y,
				/*3*/ command.Arc.Rotation,
				/*4*/ command.Arc.LargeArcFlag,
				/*5*/ command.Arc.SweepFlag,
				/*6*/ command.Arc.Target.x,
				/*7*/ command.Arc.Target.y
			);
			break;
		case PathCommand::Types::Cubic:
			string.AppendFormat(
				"{0} {1},{2} {3},{4} {5},{6} ",
				/*0*/ command.Mode == PathCommand::Modes::Absolute ? 'C' : 'c',
				/*1*/ command.Cubic.ControlPointStart.x,
				/*2*/ command.Cubic.ControlPointStart.y,
				/*3*/ command.Cubic.ControlPointTarget.x,
				/*4*/ command.Cubic.ControlPointTarget.y,
				/*5*/ command.Cubic.Target.x,
				/*6*/ command.Cubic.Target.y
			);
			break;
		case PathCommand::Types::Quadric:
			string.AppendFormat(
				"{0} {1},{2} {3},{4} ",
				/*0*/ command.Mode == PathCommand::Modes::Absolute ? 'Q' : 'q',
				/*1*/ command.Quadric.ControlPoint.x,
				/*2*/ command.Quadric.ControlPoint.y,
				/*3*/ command.Quadric.Target.x,
				/*4*/ command.Quadric.Target.y
			);
			break;
		case PathCommand::Types::Horizontal:
			string.AppendFormat(
				"{0} {1} ",
				/*0*/ command.Mode == PathCommand::Modes::Absolute ? 'H' : 'h',
				/*1*/ command.Horizontal.TargetX
			);
			break;
		case PathCommand::Types::Vertical:
			string.AppendFormat(
				"{0} {1} ",
				/*0*/ command.Mode == PathCommand::Modes::Absolute ? 'V' : 'v',
				/*1*/ command.Vertical.TargetY
			);
			break;
		case PathCommand::Types::Line:
			string.AppendFormat(
				"{0} {1},{2} ",
				/*0*/ command.Mode == PathCommand::Modes::Absolute ? 'L' : 'l',
				/*1*/ command.Line.Target.x,
				/*2*/ command.Line.Target.y
			);
			break;
		case PathCommand::Types::Move:
			string.AppendFormat(
				"{0} {1},{2} ",
				/*0*/ command.Mode == PathCommand::Modes::Absolute ? 'M' : 'm',
				/*1*/ command.Move.Target.x,
				/*2*/ command.Move.Target.y
			);
			break;
		case PathCommand::Types::Close:
			string.AppendFormat("Z");
			break;

			// fail if undefined command type
		default:
			throw;
		}
	}
}


void Svg::DocumentBase::AddPath(
	const Path & path,
	Text::StringView16 fillColor,
	Text::StringView16 strokeColor
) {
	// stringbuilder
	StringLocal<4096> buffer;
	path.ToString(buffer);
	DD::XML::Element & element = XMLDocument->GetRoot().DefineElement(L"path");
	element.DefineAttribute(L"d", buffer);
	element.DefineAttribute(L"fill", fillColor);
	element.DefineAttribute(L"stroke", strokeColor);
	element.DefineAttribute(L"stroke-width", L"0.1");
}


void Svg::DocumentBase::AddEllipse(
	const Math::Interval2D & rect,
	Text::StringView16 fillColor /*= Stringize(L"none")*/,
	Text::StringView16 strokeColor /*= Stringize(L"none") */
) {
	// stringbuilder
	StringLocal<32> buffer;
	DD::XML::Element & element = XMLDocument->GetRoot().DefineElement(L"ellipse");

	double2 center = rect.Center();
	double2 eccentricity = rect.Size() * 0.5;
	element.DefineAttribute(L"cx", buffer.Append(center.x));
	element.DefineAttribute(L"cy", buffer.Clear().Append(center.y));
	element.DefineAttribute(L"rx", buffer.Clear().Append(eccentricity.x));
	element.DefineAttribute(L"ry", buffer.Clear().Append(eccentricity.y));
	element.DefineAttribute(L"fill", fillColor);
	element.DefineAttribute(L"stroke", strokeColor);
	element.DefineAttribute(L"stroke-width", L"0.1");
}


void Svg::DocumentBase::AddText(
	double2 position,
	Text::StringView16 text,
	Text::StringView16 fillColor,
	Text::StringView16 strokeColor
) {
	StringLocal<32> buffer;
	DD::XML::Element & element = XMLDocument->GetRoot().DefineElement(L"text");
	element.DefineAttribute(L"x", buffer.Clear().Append(position.x));
	element.DefineAttribute(L"y", buffer.Clear().Append(position.y));
	element.DefineAttribute(L"fill", fillColor);
	element.DefineAttribute(L"stroke", strokeColor);
	element.DefineAttribute(L"stroke-width", L"0.1");
	element.DefineAttribute(L"style", L"font: 4px serif;");
	element.DefineText(text);
}


void Svg::DocumentBase::AddRectangle(
	const Math::Interval2D & rect,
	Text::StringView16 fillColor,
	Text::StringView16 strokeColor
) {
	// stringbuilder
	double2 size = rect.Size();
	StringLocal<32> buffer;
	DD::XML::Element & element = XMLDocument->GetRoot().DefineElement(L"rect");

	element.DefineAttribute(L"x", buffer.Append(rect.Min.x));
	element.DefineAttribute(L"y", buffer.Clear().Append(rect.Min.y));
	element.DefineAttribute(L"width", buffer.Clear().Append(size.x));
	element.DefineAttribute(L"height", buffer.Clear().Append(size.y));
	element.DefineAttribute(L"fill", fillColor);
	element.DefineAttribute(L"stroke", strokeColor);
	element.DefineAttribute(L"stroke-width", L"0.1");
}


void Svg::DocumentBase::SaveUtf8(const FileSystem::Path & path) {
	// write
	Array<i8> data = Array<i8>::FromSequence(Sequencer::Drive(XMLDocument->ToStringUtf8()).Cast<i8>());
	path.AsFile().Write({data.begin(), data.Length()});
}


void Svg::DocumentBase::SaveUtf16(const FileSystem::Path & path) {
	DD::String xml = XMLDocument->ToStringUtf16();
	path.AsFile().Write((i8 *)xml.begin(), xml.Length() * 2);
}


Svg::DocumentBase::DocumentBase(f64 w, f64 h)
	: XMLDocument(L"svg") {
	XMLDocument->GetRoot().DefineAttribute(L"xmlns", L"http://www.w3.org/2000/svg");
	XMLDocument->GetRoot().DefineAttribute(L"width", StringLocal<64>::From(w, "mm"));
	XMLDocument->GetRoot().DefineAttribute(L"height", StringLocal<64>::From(h, "mm"));
	XMLDocument->GetRoot().DefineAttribute(L"viewBox", StringLocal<64>::From("0 0 ", w, " ", h));
}

DD::Svg::DocumentBase::DocumentBase(const FileSystem::Path & path) {
	// read file
	Load(path);

	// initialize id register
	for (XML::Element & e : XMLDocument->GetRoot().Search()) {
		// check if the attribute 'id' is present
		Text::StringView16 id = Const(e)[L"id"];
		if (id != nullptr)
			Id2Element.Insert(id, &e);
	}
}


bool Svg::Path::PathCommand::UpdatePosition(double2 & opening, double2 & position) const {
	switch (Type) {
	case Types::Arc:
	case Types::Cubic:
	case Types::Quadric:
	case Types::Line:
		position = Mode == Modes::Absolute ? Move.Target : position + Move.Target;
		return false;
	case Types::Horizontal:
		position.x = Mode == Modes::Absolute ? Horizontal.TargetX : position.x + Horizontal.TargetX;
		return false;
	case Types::Vertical:
		position.y = Mode == Modes::Absolute ? Vertical.TargetY : position.y + Vertical.TargetY;
		return false;
	case Types::Move:
		position = Mode == Modes::Absolute ? Move.Target : position + Move.Target;
		opening = position;
		return true;
	case Types::Close:
		position = opening;
		return false;
	default:
		throw;
	}
}


bool Path::DecompositionSequencer::Next() {
	// reset path
	_decomposedPath.Commands.Clear();
	// move to current cursor position
	_decomposedPath.AddMoveAbsolute(_cursor);

	while (_index < _sourcePath->Commands.Size()) {
		// currently processed command
		const Svg::Path::PathCommand & cmd = _sourcePath->Commands[_index++];
		// update position accumulators, check if command breaks process
		bool interrupted = cmd.UpdatePosition(_begin, _cursor);
		// handle interruption
		if (interrupted) {
			// skip all interruptions that happened before path has a valid command
			if (_decomposedPath.Commands.Size() == 1) {
				// update cursor position
				_decomposedPath.Commands.First().Move.Target = _cursor;
				continue;
			}
			// interruption means, that we have a path.
			return true;
		}
		else {
			// add to collection
			_decomposedPath.Commands.Add(cmd);
		}
	}

	// if the cycle ends and the sub-path is valid, the number of commands must be non-trivial
	return _decomposedPath.Commands.Size() != 1;
}


bool Path::DecompositionSequencer::Reset() {
	_begin = 0;
	_index = 0;
	_cursor = 0;

	return Next();
}


bool Svg::Path::PolylineSequencer::Reset() {
	Index = 0;
	Accumulator.Reserve(1024);
	return Next();
}


bool Svg::Path::PolylineSequencer::Next() {
	// reset accumulator of polyline
	Accumulator.Clear();
	// stop flag
	bool stop = false;

	// continue with enumerating source commands until we run out of them or
	// until we met a stop flag
	for (; Index < AbsolutePath.Commands.Size() && !stop; ++Index) {
		Path::PathCommand & command = AbsolutePath.Commands[Index];
		switch (command.Type) {
		case Path::PathCommand::Types::Vertical:
			if (Accumulator.Size() == 0)
				Accumulator.Add(Position);

			if (Position != command.Vertical.TargetY) {
				Position.y = command.Vertical.TargetY;
				Accumulator.Add(Position);
			}
			break;

		case Path::PathCommand::Types::Horizontal:
			if (Accumulator.Size() == 0)
				Accumulator.Add(Position);

			if (Position != command.Horizontal.TargetX) {
				Position.x = command.Horizontal.TargetX;
				Accumulator.Add(Position);
			}
			break;

		case Path::PathCommand::Types::Line:
			if (Accumulator.Size() == 0)
				Accumulator.Add(Position);

			if (Position != command.Line.Target) {
				Position = command.Line.Target;
				Accumulator.Add(Position);
			}
			break;

		case Path::PathCommand::Types::Move:
			Position = command.Move.Target;
			// stop only if anything is accumulated, e.g. multiple move commands could be grouped without stopping
			stop = Accumulator.Size() != 0;
			break;

		case Path::PathCommand::Types::Quadric: {
			CornerRefinerQuadric({ { Position, command.Quadric.ControlPoint, command.Quadric.Target }, AngleLimit })
				.GetAngleLimitedDiscretization(Accumulator);
			Position = command.Quadric.Target;
		} break;

		case Path::PathCommand::Types::Cubic: {
			CornerRefinerCubic({ { Position, command.Cubic.ControlPointStart, command.Cubic.ControlPointTarget, command.Cubic.Target }, AngleLimit })
				.GetAngleLimitedDiscretization(Accumulator);
			Position = command.Cubic.Target;
		} break;

		case Path::PathCommand::Types::Arc: {
			CornerRefinerArc{ ArcComputer(Position, command), AngleLimit }
				.GetAngleLimitedDiscretization(Accumulator);
			Position = command.Arc.Target;
		} break;

		case Path::PathCommand::Types::Close:
			if (Accumulator.Size() && Accumulator.Last() != Accumulator.First())
				Accumulator.Add(Position = Accumulator.First());
			break;

		default:
			throw "Not implemented";
		}
	}

	if (OutputCorrectionEnabled) {
		for (double2 & pt : Accumulator) {
			pt.y = YCorrection - pt.y;
			pt += Offset;
			pt *= Scale;
		}
	}

	return Accumulator.Size() != 0;
}


DD::Svg::Path::PolylineSequencer & DD::Svg::Path::PolylineSequencer::operator=(PolylineSequencer && rhs) {
	OutputCorrectionEnabled = (rhs.OutputCorrectionEnabled);
	YCorrection = (rhs.YCorrection);
	Offset = (rhs.Offset);
	Scale = (rhs.Scale);
	AbsolutePath = Fwd(rhs.AbsolutePath);
	AngleLimit = rhs.AngleLimit;

	return *this;
}


DD::Svg::Path::PolylineSequencer::PolylineSequencer(
	const Path & path, f64 yCorrection, double2 offset, double2 scale, f64 angleLimit
)
	: Accumulator()
	, AbsolutePath(path)
	, OutputCorrectionEnabled(yCorrection != 0.0 || offset != 0.0 || scale != 1.0)
	, YCorrection(yCorrection)
	, Offset(offset)
	, Scale(scale)
	, AngleLimit(angleLimit)
{
	AbsolutePath.Absolutize();
}


DD::Svg::Path::PolylineSequencer::PolylineSequencer(const PolylineSequencer & rhs)
	: AbsolutePath(rhs.AbsolutePath)
	, YCorrection(rhs.YCorrection)
	, OutputCorrectionEnabled(rhs.OutputCorrectionEnabled)
	, Offset(rhs.Offset)
	, Scale(rhs.Scale)
	, AngleLimit(rhs.AngleLimit)
{}


/************************************************************************/
/* Svg::Reader                                                          */
/************************************************************************/
#pragma region Svg::Reader

bool DD::Svg::Reader::Follow(Text::StringView16 text) {
	Primitives primitive = (Primitives)Svg::Hash(text);
	switch (primitive) {
	case Primitives::Svg:
		return true;
	case Primitives::Circle:
	case Primitives::Rectangle:
	case Primitives::Polygon:
	case Primitives::Polyline:
	case Primitives::Ellipse:
	case Primitives::Path:
	case Primitives::Use:
	case Primitives::Group:
	case Primitives::Script:
	case Primitives::Style:
	case Primitives::LinearGradient:
	case Primitives::RadialGradient:
	case Primitives::Defs:
	case Primitives::Text:
		return false;
	case Primitives::Link:
	case Primitives::Animate:
	case Primitives::AnimateMotion:
	case Primitives::AnimateTransform:
	case Primitives::ClipPath:
	case Primitives::Description:
	case Primitives::Filter:
	case Primitives::ForeignObject:
	case Primitives::Image:
	case Primitives::Line:
	case Primitives::Marker:
	case Primitives::Mask:
	case Primitives::Pattern:
	case Primitives::Switch:
	case Primitives::Symbol:
	case Primitives::TextPath:
	case Primitives::View:
	default:
		return false;
	}
}


void DD::Svg::Reader::ReadNumbers(Text::StringView16 view, ArrayView1D<f64 *> numbers) {
	auto splitter = view.Split<Text::SplitterFlags::SkipEmptyMatches>(
		[](wchar_t c) {return Text::IsWhiteSpace(c) || c == ','; }
	);

	bool valid = splitter.Reset();
	for (f64 * number : numbers) {
		if (!valid)
			break;

		valid = splitter.Get().TryCast(*number);
		valid = splitter.Next();
	}
}


void DD::Svg::Reader::AppendPath(const XmlRectangle & x) {
	// simple rectangles
	if (x.Rounded == 0.0) {
		Path.Commands.Reserve(5);
		Path.AddMoveAbsolute(x.Point);
		Path.AddVerticalRelative(x.Dimensions.y);
		Path.AddHorizontalRelative(x.Dimensions.x);
		Path.AddVerticalRelative(-x.Dimensions.y);
		Path.AddClose();
	}
	// rectangle with rounded corners
	else {
		// precomputed values
		double2 maxPoint = x.Point + x.Dimensions;
		double2 ltOffsets = x.Point + x.Rounded;
		double2 rbOffsets = maxPoint - x.Rounded;

		Path.Commands.Reserve(9);
		// 1 point above rounded lb corner
		Path.AddMoveAbsolute({ x.Point.x, rbOffsets.y });
		// 2 point next to rounded lb corner
		Path.AddQuadricAbsolute({ x.Point.x, maxPoint.y }, { ltOffsets.x, rbOffsets.y });
		// 3 point next to rounded rb corner
		Path.AddHorizontalAbsolute(rbOffsets.x);
		// 4 point above rounded rb corner
		Path.AddQuadricAbsolute(maxPoint, { maxPoint.x, rbOffsets.y });
		// 5 point below rounded rt corner
		Path.AddVerticalAbsolute(ltOffsets.y);
		// 6 point next to rounded rt corner
		Path.AddQuadricAbsolute({ maxPoint.x, x.Point.y }, { rbOffsets.x, ltOffsets.y });
		// 7 point next to rounded lt corner
		Path.AddVerticalAbsolute(ltOffsets.x);
		// 8 point below rounded lt corner
		Path.AddQuadricAbsolute(x.Point, { x.Point.x, ltOffsets.y });
		// 9 point above rounded lb corner
		Path.AddClose();
	}
}


void DD::Svg::Reader::AppendPath(const XmlEllipse & x) {
	Path.Commands.Reserve(3);
	Path.AddMoveAbsolute({ x.Center.x - x.Radii.x, x.Center.y });
	Path.AddArcAbsolute({ x.Center.x + x.Radii.x, x.Center.y }, x.Radii, 0.0, false, true);
	Path.AddArcAbsolute({ x.Center.x - x.Radii.x, x.Center.y }, x.Radii, 0.0, false, true);
}


void DD::Svg::Reader::AppendPath(const XmlCircle & x) {
	XmlEllipse e;
	e.Center = x.Center;
	e.Radii = x.Radius;
	AppendPath(e);
}


void DD::Svg::Reader::AppendPath(const XmlLine & x) {
	Path.Commands.Reserve(2);
	Path.AddMoveAbsolute(x.X);
	Path.AddLineAbsolute(x.Y);
}


void DD::Svg::Reader::AppendPath(const XmlPolyline & x) {
	ReadPoints(x.Points);
}


void DD::Svg::Reader::AppendPath(const XmlPolygon & x) {
	if (ReadPoints(x.Points))
		Path.AddClose();
}


void DD::Svg::Reader::AppendPath(const XmlPath & x) {
	// detection of split, e is excluded because of number in scientific notation
	static constexpr auto split = [](wchar_t c) {return Text::IsLetter(c) && c != 'e' && c != 'E'; };
	// sequencer is going through path elements, we need the "d" attribute
	DD_TODO_STAMP("Remove string-stringview conversion.");
	String s = x.Path;
	for (Text::StringView16 textCommand : Sequencer::Drive(s.Split<Text::SplitterFlags::SkipAndStickLeft>(split))) {
		DD_TODO_STAMP("Remove temporary check because split function is parsing zero terminator as command.");
		if (textCommand.Trim().Length()) {
			Path::PathCommand command;
			command.Mode = Text::IsUpperLetter(textCommand.First()) ? Path::PathCommand::Modes::Absolute : Path::PathCommand::Modes::Relative;

			switch (textCommand.First()) {
			case 'h':
			case 'H':
				command.Type = Path::PathCommand::Types::Horizontal;
				ReadNumbers(textCommand.RemoveFirst(), Arrayize<f64 *>(&command.Horizontal.TargetX));
				break;

			case 'v':
			case 'V':
				command.Type = Path::PathCommand::Types::Vertical;
				ReadNumbers(textCommand.RemoveFirst(), Arrayize<f64 *>(&command.Vertical.TargetY));
				break;

			case 'm':
			case 'M':
				command.Type = Path::PathCommand::Types::Move;
				ReadNumbers(textCommand.RemoveFirst(), Arrayize<f64 *>(&command.Move.Target.x, &command.Move.Target.y));
				break;

			case 'l':
			case 'L':
				command.Type = Path::PathCommand::Types::Line;
				ReadNumbers(textCommand.RemoveFirst(), Arrayize<f64 *>(&command.Line.Target.x, &command.Line.Target.y));
				break;

			case 'q':
			case 'Q':
				command.Type = Path::PathCommand::Types::Quadric;
				ReadNumbers(textCommand.RemoveFirst(), Arrayize<f64 *>(
					&command.Quadric.ControlPoint.x,
					&command.Quadric.ControlPoint.y,
					&command.Quadric.Target.x,
					&command.Quadric.Target.y
					));
				break;

			case 'c':
			case 'C':
				command.Type = Path::PathCommand::Types::Cubic;
				ReadNumbers(textCommand.RemoveFirst(), Arrayize<f64 *>(
					&command.Cubic.ControlPointStart.x,
					&command.Cubic.ControlPointStart.y,
					&command.Cubic.ControlPointTarget.x,
					&command.Cubic.ControlPointTarget.y,
					&command.Cubic.Target.x,
					&command.Cubic.Target.y
					));
				break;

			case 'A':
			case 'a': {
				command.Type = Path::PathCommand::Types::Arc;
				f64 large_arc_flag;
				f64 sweep_flag;
				ReadNumbers(textCommand.RemoveFirst(), Arrayize<f64 *>(
					&command.Arc.Radius.x,
					&command.Arc.Radius.y,
					&command.Arc.Rotation,
					&large_arc_flag,
					&sweep_flag,
					&command.Arc.Target.x,
					&command.Arc.Target.y
					));
				command.Arc.LargeArcFlag = (i32)large_arc_flag;
				command.Arc.SweepFlag = (i32)sweep_flag;
			} break;

			case 'z':
			case 'Z':
				command.Type = Path::PathCommand::Types::Close;
				break;

			case 0:
				continue;

			default:
				throw "not implemented";
			}

			Path.Commands.Add(command);
		}

	}
}


void DD::Svg::Reader::AppendPath(const XmlGroup & x) {
		for (XML::Element & child : Element.Children) {
			Svg::Path p;
			Svg::Reader{ Document, child, p }.Deserialize();
			Path.AddPath(p);
		}
}


void Svg::Reader::AppendPath(const XmlUse & x) {
	BTreePair<Text::StringView16, XML::Element *> * a = Document->Id2Element.GetItem(x.Link.SubView(1));
	if (a) {
		Svg::Path p;
		Svg::Reader{ Document, *a->Value, p }.Deserialize();
		Path.AddPath(p, x.Offset);
	}
}


bool DD::Svg::Reader::Deserialize() {
	Svg::Primitives hash = (Svg::Primitives)Hash(Element.GetName());
	switch (hash) {
		// process
	case Primitives::Rectangle: return AppendElement<Svg::XmlRectangle>();
	case Primitives::Circle:    return AppendElement<Svg::XmlCircle>();
	case Primitives::Ellipse:   return AppendElement<Svg::XmlEllipse>();
	case Primitives::Line:      return AppendElement<Svg::XmlLine>();
	case Primitives::Polyline:  return AppendElement<Svg::XmlPolyline>();
	case Primitives::Polygon:   return AppendElement<Svg::XmlPolygon>();
	case Primitives::Path:      return AppendElement<Svg::XmlPath>();
	case Primitives::Group:     return AppendElement<Svg::XmlGroup>();
	case Primitives::Use:       return AppendElement<Svg::XmlUse>();
		return true;
		// ignore
	case Svg::Primitives::Animate:
	case Svg::Primitives::AnimateMotion:
	case Svg::Primitives::AnimateTransform:
	case Svg::Primitives::Defs:
	case Svg::Primitives::Description:
	case Svg::Primitives::ForeignObject:
	case Svg::Primitives::Image:
	case Svg::Primitives::LinearGradient:
	case Svg::Primitives::Pattern:
	case Svg::Primitives::RadialGradient:
	case Svg::Primitives::Script:
	case Svg::Primitives::Style:
		// fix later
	case Svg::Primitives::Link:
	case Svg::Primitives::ClipPath:
	case Svg::Primitives::Filter:
	case Svg::Primitives::Marker:
	case Svg::Primitives::Mask:
	case Svg::Primitives::Switch:
	case Svg::Primitives::Svg:
	case Svg::Primitives::Symbol:
	case Svg::Primitives::Text:
	case Svg::Primitives::TextPath:
	case Svg::Primitives::View:
		DD_LOG("Unsupported element:", Element.GetName());
		return false;
	default:
		throw "Unsupported type of svg element.";
		return false;
	}
}


bool DD::Svg::Reader::ReadPoints(Text::StringView16 points) {
	// decode text to sequence of points
	//auto ptReader = points.Split<true>([](wchar_t c) {return !Text::IsNumeric(c); })
	auto ptReader = Sequencer::Drive(points.SplitSkipEmpty([](wchar_t c) {return !Text::IsNumeric(c); }))
		.Select([](Text::StringView16 number) { f64 n; number.TryCast(n); return n; })
		.Batch<2>()
		.Cast<double2>()
		;

	// if no points, fail the reading
	if (!ptReader.Reset())
		return false;

	// add first point as a move command
	Path.AddMoveAbsolute(ptReader.Get());
	// connect all the following points as line connections
	while (ptReader.Next())
		Path.AddLineAbsolute(ptReader.Get());

	return true;
}

#pragma endregion


/************************************************************************/
/* Svg::Document                                                        */
/************************************************************************/
#pragma region Svg::Document

Svg::Document::Document(const FileSystem::Path & path)
	: Reference(new DocumentBase(path))
{}

Svg::Document::Document(const double2 & dims)
	: Reference(new DocumentBase(dims.x, dims.y))
{}


Svg::Document::Document(f64 w, f64 h)
	: Reference(new DocumentBase(w, h))
{}

#pragma endregion


/************************************************************************/
/* Svg::Svg::MatrixTransfromation                                       */
/************************************************************************/
#pragma region Svg::MatrixTransfromation

size_t Svg::MatrixTransformation::TryParseArguments(
	const Text::StringView16 & text,
	Array<f64> & target
) {
	auto splitter = text.SplitSkipEmpty([](wchar_t c) { return c == ',' || Text::IsWhiteSpace(c); });
	bool ready = splitter.Reset();
	for (auto & targetItem : target) {
		// if no more input, return number of loaded items
		if (!ready)
			return &targetItem - target.begin();

		// if casting error, return 0 as incorrect loading
		if (!splitter.Get().TryCast(targetItem))
			return 0;

		// iterate to the next input
		ready = splitter.Next();
	}

	// if some resting input, return 0 as fail
	return ready ? 0 : target.Length();
}


bool DD::Svg::MatrixTransformation::TryParse(Text::StringView16 t) {
	// split name and arguments
	auto splitter = t.SplitSkipEmpty(L'(');
	if (!splitter.Reset())
		return false;

	// name of the transformation
	Text::StringView16 name = splitter.Get().Trim();

	if (!splitter.Next())
		return false;

	// array with parsed arguments
	Array<f64, 6> arguments;
	size_t argCount = TryParseArguments(splitter.Get(), arguments);

	// use name of the transformation
	switch ((Types)Hash(name))
	{
	case Types::Scale:
		if (argCount == 1)
			InitScale(arguments[0]);
		else if (argCount == 2)
			InitScale({ arguments[0], arguments[1] });
		else
			return false;
		return true;

	case Types::SkewX:
		if (argCount == 1)
			InitSkewX(arguments[0]);
		else
			return false;
		return true;

	case Types::SkewY:
		if (argCount == 1)
			InitSkewY(arguments[0]);
		else
			return false;
		return true;

	case Types::Translate:
		if (argCount == 2)
			InitTranslate({ arguments[0], arguments[1] });
		else
			return false;
		return true;

	case Types::Rotate:
		if (argCount == 1)
			InitRotate(arguments[0]);
		else if (argCount == 3)
			InitRotate(arguments[0]).Offset = { arguments[1], arguments[2] };
		else
			return false;
		return true;

	case Types::Matrix:
		if (argCount == 6)
			InitMatrix({ arguments[4], arguments[5] }, { arguments[0], arguments[1], arguments[2], arguments[3] });
		else
			return false;
		return true;

	default:
		throw;
		return false;
	}
}

#pragma endregion


#pragma region Svg::Transformation

double2 DD::Svg::Transformation::Apply(const double2 & input) {
	double2 output = input;
	for (const MatrixTransformation & t : Transformations) {
		output = t.Apply(output);
	}

	return output;
}


void Svg::Transformation::AppendString(String & target) const {
	// add all the transformations
	for (const MatrixTransformation & t : Transformations) {
		// separate transformations by space
		if (&t != Transformations.begin())
			target.Append(" ");

		t.AppendString(target);
	}
}


bool DD::Svg::Transformation::TryParse(const Text::StringView16 & text) {
	// for x ; {"translate(0, 1", "scale(4, 5", ... }
	for (const Text::StringView16 & t : Sequencer::Drive(text.SplitSkipEmpty(L')'))) {
		MatrixTransformation mt;
		if (mt.TryParse(t))
			Transformations.Add(mt);
		else
			return false;
	}

	return true;
}

#pragma endregion

