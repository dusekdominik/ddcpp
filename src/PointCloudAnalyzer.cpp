#include <DD/Concurrency/ThreadPool.h>
#include <DD/Math.h>
#include <DD/PointCloudAnalyzer.h>
#include <DD/Profiler.h>

#include <algorithm>

using namespace DD;
using namespace DD::Simulation;

DD_TODO_STAMP("Region class identification - create id (tree/building/ground) from ptcloud.");

template<typename T>
void convexHull(const ArrayView1D<V2<T>> & input, List<V2<T>> & output) {
	// safety check
	DD_ASSERT(input.Length() >= 3, "Number of input points to convex hull method is not valid.");
	// preallocate
	output.Resize(input.Length() * 2);
	// Sort points lexicographically
	std::sort(input.begin(), input.end(), [](const V2<T> & f1, const V2<T> & f2) {
		return f1.x < f2.x || (f1.x == f2.x && f1.y < f2.y);
	});

	i32 k = 0;
	// Build lower hull
	for (size_t i = 0; i < input.Length(); ++i) {
		while (k >= 2) {
			V2<T> & a = input[i];
			V2<T> & b = output[k - 1];
			V2<T> & c = output[k - 2];
			T cross = (b.x - c.x) * (a.y - c.y) - (b.y - c.y) * (a.x - c.x);

			if (cross > 0)
				break;
			else
				k--;
		}
		output[k++] = input[i];
	}

	// Build upper hull
	for (i32 i = (i32)input.Length() - 2, t = (k + 1); i >= 0; i--) {
		while (k >= t) {
			V2<T> & a = input[i];
			V2<T> & b = output[k - 1];
			V2<T> & c = output[k - 2];
			T cross = (b.x - c.x) * (a.y - c.y) - (b.y - c.y) * (a.x - c.x);

			if (cross > 0)
				break;
			else
				k--;
		}
		output[k++] = input[i];
	}


	// safety check
	DD_ASSERT(k > 3, "Number of output points from convex hull method is not valid.");
	// prevent invalid call (negative number)
	if (k > 3)
		output.Resize(k - 1);
}

void alphaHull(const List<float2> & points, List<PolygonF> & polygons, f32 alpha2) {
	// auxiliary structure
	struct Edge { float2 A, B; };
	// collection of edges
	List<Edge> edges(512);
	// squared diameter
	f32 diameter2 = 4 * alpha2;

	for (size_t i = 0; i < points.Size() - 1; ++i) {
		// alias of point under index i
		const float2 & x = points[i];

		for (size_t j = i + 1; j < points.Size(); ++j) {
			// alias of point under index j
			const float2 & y = points[j];
			// get distance
			f32 dist2 = dot(x - y);
			// diameter check
			if (diameter2 < dist2) {
				continue;
			}
			// get mid point of xy
			float2 midPoint((x + y) * 0.5f);
			// coefficient for deviation
			f32 coefficient = Math::Sqrt((alpha2 - dist2 * 0.25f) / dist2);
			// cross product
			float2 dev(coefficient * (x.y - y.y), coefficient * (y.x - x.x));
			// get two centers for discs that are containing given points on border
			const float2 & center0 = midPoint + dev;
			const float2 & center1 = midPoint - dev;
			// if anything found, it is not an edge
			enum { NONE = 0, C0 = 1, C1 = 2, DONE = 3 };
			i32 found = NONE;

			// check if there are any points captured by discs
			for (size_t k = 0; k < points.Size() && found != DONE; ++k) {
				// alias of point under index k
				const float2 & z = points[k];
				// do not check with given indices
				if (i == k || j == k) {
					continue;
				}
				// check only if not found
				switch (found) {
				case NONE: // check both
					if (dot(center0 - z) < alpha2) {
						found |= C0;
					}
					if (dot(center1 - z) < alpha2) {
						found |= C1;
					}
					break;
				case C0: // check just c1
					if (dot(center1 - z) < alpha2) {
						found |= C1;
					}
					break;
				case C1: // check just c0
					if (dot(center0 - z) < alpha2) {
						found |= C0;
					}
					break;
				}
			}

			// push edge, if not found
			if (found != DONE) {
				edges.Add(Edge{ x, y });
			}
		}
	}

	// create polygons from edges
	// reset collection
	polygons.Free();

	// create polygons until all polygons are not created
	while (edges.Size()) {
		// get reference to the new polygon
		PolygonF & polygon = polygons.Append();
		// add initial edge to the new polygon
		polygon.Points.Add(edges.Last().A, edges.Last().B);
		// remove initial edge from edges
		edges.RemoveLast();

		// iterate until polygon is closed
		for (i32 run = true; run;) {
			// reset state
			run = false;
			// iterate over all edges
			for (size_t i = 0; i < edges.Size(); ++i) {
				// alias for edge under index i
				const Edge & edge = edges[i];
				// check first point of the edge
				if (edge.A == polygon.Points.Last()) {
					if (edge.B != polygon.Points.First()) {
						// if anything found, confirm continue state
						run = true;
						polygon.Points.Add(edge.B);
					}
					edges.RemoveAt(i);
					break;
				}
				// check second point of the edge
				if (edge.B == polygon.Points.Last()) {
					if (edge.A != polygon.Points.First()) {
						// if anything found, confirm continue state
						run = true;
						polygon.Points.Add(edge.A);
					}
					edges.RemoveAt(i);
					break;
				}
			}
		}
	}
}


/**
 * Sample average color from a sequence of vertices.
 * @param SEQUENCER - derived from ISequencer<QuadVertex>.
 */
template<typename SEQUENCER>
Color sampleColor(SEQUENCER sequence) {
	// init vars
	size_t count = 0;
	float4 color = 0.f;

	// aggregate values
	for (const QuadVertex & vertex : sequence) {
		color.x += (f32)vertex.Color.r;
		color.y += (f32)vertex.Color.g;
		color.z += (f32)vertex.Color.b;
		color.w += (f32)vertex.Color.a;
		++count;
	}

	// parse values
	color /= (f32)count;
	color.x = Math::Saturate(color.x, 0.f, 255.f);
	color.y = Math::Saturate(color.y, 0.f, 255.f);
	color.z = Math::Saturate(color.z, 0.f, 255.f);
	color.w = Math::Saturate(color.w, 0.f, 255.f);

	// result
	return Color((u8)color.x, (u8)color.y, (u8)color.z, (u8)color.w);
};


/************************************************************************/
/* Helpers                                                              */
/************************************************************************/
#pragma region region growing

/** Pair of segment and point. */
struct PointSegmentRelation { Simulation::PointCloud::Cell * Cell; Simulation::QuadVertex * Point; };
/** Undefined region flag. */
static constexpr i32 UNDEFINED = 0xffffffff;


static List<Simulation::QuadVertex *> growRegion(
	ArrayView2D<Simulation::PointCloud::Cell> data,
	PointSegmentRelation & init,
	PointCloudAnalyzer::RegionGrowingSettings & settings
) {
	int id = init.Point->Backup;
	List<PointSegmentRelation, 5000> stack;
	List<Simulation::QuadVertex *> region(100000);

	stack.Add(init);
	while (stack.Size()) {
		float3 position = stack.Last().Point->Position;
		PointCloud::Cell * cell = stack.Last().Cell;
		region.Add(stack.Last().Point);
		stack.RemoveLast();
		for (Simulation::PointCloud::Cell & neighbor : Sequencer::Drive(data.Neighborhood(cell))) {
			for (Simulation::QuadVertex & vertex : neighbor) {
				if (vertex.Backup != UNDEFINED)
					continue;

				if (dot(position - vertex.Position) < settings.Distance2) {
					vertex.Backup = id;
					stack.Add(PointSegmentRelation{ &neighbor, &vertex });
				}
			}
		}
	}

	return region;
}


static List<PointCloudAnalyzer::Region> growOnSlice(
	ArrayView2D<Simulation::PointCloud::Cell> data,
	PointCloudAnalyzer::RegionGrowingSettings & settings
) {
	DD_PROFILE_SCOPE("PointCloudAnalyzer/GetSlice");
	List<PointCloudAnalyzer::Region> regions(200);
	// set all points to undefined
	for (Simulation::PointCloud::Cell & cell : data.PlainView()) for (QuadVertex & v : cell)
		v.Backup = UNDEFINED;
	// grow regions
	for (Simulation::PointCloud::Cell & cell : data.PlainView()) for (QuadVertex & v : cell) {
		if (v.Backup == UNDEFINED) {
			v.Backup = (int)regions.Size();
			PointSegmentRelation init = { &cell, &v };
			List<QuadVertex *> region = growRegion(data, init, settings);

			if (settings.MinimalPointCount > region.Size())
				continue;
			if (settings.MaximalPointCount < region.Size())
				continue;

			PointCloudAnalyzer::Region r;
			r.Cloud = PointCloud(region);
			// create convex hull
			PointCloudAnalyzer::ConvexHull2D::Input cIn;
			cIn.Cloud = &(r.Cloud);
			PointCloudAnalyzer::ConvexHull2D::Output cOut;
			cOut.ConvexHull = &r.Profile;
			PointCloudAnalyzer::ConvexHull2D::Evaluate(cIn, cOut);
			// check area of surface
			f32 area = r.Profile.Surface();
			if (settings.MinimalArea > area)
				continue;
			if (settings.MaximalArea < area)
				continue;

			regions.Add(r);
		}
	}

	return regions;
}
#pragma endregion


#pragma region public methods
void PointCloudAnalyzer::AlphaHull2D::Evaluate(Input & input, Output & output) {
	// assert input
	DD_ASSERT(input.Cloud, "Cloud must be non null.");
	DD_ASSERT(input.Alpha > 0.f, "Alpha param must be positive number.");

	// get number of points
	size_t numberOfPoints = (*input.Cloud)->Size();
	// reserve space for 2d points
	List<float2> points(numberOfPoints);
	// copy data
	for (const Simulation::QuadVertex & v : (*input.Cloud)->SelectPointsSeq()) {
		points.Add(v.Position.xz);
	}
	// compute hull
	alphaHull(points, output.AlphaShapes, input.Alpha * input.Alpha);
}


void PointCloudAnalyzer::ConvexHull2D::Evaluate(Input & input, Output & output) {
	// source
	Simulation::PointCloud & cloud = *(input.Cloud);
	// number of points
	size_t pointCount = cloud->Size();
	// index to output cvx hull
	size_t k = 0;
	// input points
	Array<float2> input2D(pointCount);
	// ptr for parsing points
	float2 * ptr = input2D.begin();

	// parse vertices into coords
	for (const Simulation::PointCloud::Cell & cell : cloud->Grid().PlainView()) {
		for (const Simulation::QuadVertex & vertex : cell) {
			*(ptr++) = vertex.Position.xz;
		}
	}

	// compute hull
	convexHull(input2D.ToView(), output.ConvexHull->Points);
}



#pragma endregion

/************************************************************************/
/* Slicing method                                                       */
/************************************************************************/
#pragma region SliceDecomposition
#define MULTI_THREAD 1
void PointCloudAnalyzer::SliceDecomposition::Evaluate(Input & settings, Output & output) {
	DD_PROFILE_SCOPE("PointCloudAnalyzer/GetSlices");

	output.RegionsPerSlices.Reserve(64);

	Simulation::PointCloud ptCloud = settings.Cloud->RegridArea(settings.GridSetting, settings.Callbacks.RegridProgress);
	size_t biggest = 0;
	output.RegionsPerSlices.Resize(ptCloud->Grid().Length());

	IProgress::ScopeCounter32A progressCounter(
		settings.Callbacks.SlicingProgress,
		(i32)ptCloud->Grid().Length()
	);

#if MULTI_THREAD
	Concurrency::ThreadPool::TaskList<256, 64> tasks;
#endif
	if (settings.Callbacks.NumberOfSlices)
		settings.Callbacks.NumberOfSlices->Run(ptCloud->Grid().Length());


	for (size_t i = 0; i < ptCloud->Grid().Length(); ++i) {
		auto task = [&, i]() {
			output.RegionsPerSlices[i] = growOnSlice((ptCloud->Grid())[i], settings.RegionGrowing);
			if (settings.Callbacks.OneSliceDone) {
				settings.Callbacks.OneSliceDone->Run();
			}
			++progressCounter;
		};
#if MULTI_THREAD
		tasks.Add(task);
#else
		task();
#endif
	}
}
#pragma endregion

#pragma region SliceComposition
/**
 * Aggregation of convex polygons in slices.
 * Works like throwing circles on sticks.
 *
 *   1. Add convex polygons to the aggregator. @see Add.
 *   2. Aggregate polygons and collect sticks. @see Aggregate
 */
struct Aggregator {
	/** Single information.*/
	struct Convex {
		/** Centroid of cvx. */
		float2 Centroid;
		/** Ref of cvx. */
		PointCloudAnalyzer::Region * Reference;
	};
public: // statics
	/** Property forwarder of dot centroid dot product. */
	static f32 Distance2(const Convex & cvx0, const Convex & cvx1) { return dot(cvx0.Centroid - cvx1.Centroid); }
	/** Property forwarder of dot centroid dot product. */
	static f32 Distance2(const Convex & cvx, const List<Convex> & stick) { return Distance2(cvx, stick.First()); }
private: // properties
	/** Collection of sticks. */
	List<List<Convex>> StickCollection;
	/** Squared distance limit .*/
	f32 D2;
public:
	/** Add a single cvx. */
	void Add(Convex cvx) {
		bool found = false;
		// search if any stick is reachable
		for (List<Convex> & stick : StickCollection) {
			// get distance between centroids
			f32 d2 = Distance2(cvx, stick);
			if (d2 < D2) {
				// and reference of polygon to the stick
				stick.Add(cvx);
				found = true;
			}
		}
		// if no stick found, add new stick
		if (!found) {
			List<Convex> cvxList;
			cvxList.Add(cvx);
			StickCollection.Add(Fwd(cvxList));
		}
	}
	/**
	 * Do the aggregation - multi-thread method.
	 * @param [in]  stickSize        - minimal number of polygons which must be located on valid stick.
	 * @param [out] outputCollection - collection of output polygons.
	 */
	void Aggregate(size_t stickSize, List<PointCloudAnalyzer::Stick> & outputCollection) {
		// prepare mt collections
		Concurrency::ThreadPool::TaskList<256, 48> tasks;
		//tasks.Reserve(StickCollection.Size());
		// prepare target collection
		outputCollection.Clear();
		outputCollection.Reserve(StickCollection.Size());

		// establish polygon aggregation
		for (List<Convex> & stick : StickCollection) {
			// if number of polygons on a stick is valid
			if (stick.Size() > stickSize) {
				// copy polygon to the output
				outputCollection.Add(PointCloudAnalyzer::Stick());
				// get references for asynchronous aggregation
				List<Convex> * stickReference = &stick;
				PointCloudAnalyzer::Stick * outputReference = &(outputCollection.Last());
				// register async task
				tasks.Add([stickReference, outputReference]() {
					// copy aggregator
					outputReference->MergedPolygon = stickReference->First().Reference->Profile;
					// copy regions
					outputReference->SourceRegions.Reserve(stickReference->Size());
					for (size_t i = 0, m = stickReference->Size(); i < m; ++i) {
						outputReference->SourceRegions.Add(*(*stickReference)[i].Reference);
					}
					// aggregate area of all the polygons on the stick
					// start from i=1 because i=0 is the mergedPolygon
					for (size_t i = 1, m = stickReference->Size(); i < m; ++i) {
						outputReference->MergedPolygon.Include((*stickReference)[i].Reference->Profile);
					}
					// aggregate point clouds of all the polygons on the stick
					List<const PointCloudBase *, 256> clouds;
					for (size_t i = 0, m = stickReference->Size(); i < m; ++i) {
						clouds.Add((*stickReference)[i].Reference->Cloud);
					}
					outputReference->MergedCloud = DD::Simulation::PointCloud(clouds);
					// create centroid of merged polygon
					outputReference->Centroid = outputReference->MergedPolygon.Centroid();
				});
			}
		}
		// wait until process finished
		tasks.Wait();
	}
	/**
	 * Constructor.
	 * @param d2 - squared distance between centroids to be accepted as single stick
	 */
	Aggregator(f32 d2) : D2(d2) {}
};

void PointCloudAnalyzer::SliceComposition::Evaluate(Input & input, Output & output) {
	// initialize aggregator by sq distance
	Aggregator agg(input.MaxDistance * input.MaxDistance);

	for (auto & slice : input.RegionsPerSlices) {
		for (auto & item : slice) {
			agg.Add({ item.Profile.Centroid(), &item });
		}
	}

	agg.Aggregate(input.MinimalStick, output.Collection);
}
#pragma endregion


#pragma region SliceBuildings
void PointCloudAnalyzer::SliceBuildings::Evaluate(Input & input, Output & output) {
	// safety checks
	DD_ASSERT(input.Cloud != nullptr, "Source cloud cannot be null.");
	DD_ASSERT(input.SliceStick != nullptr, "Source stick cannot be null.");
	DD_ASSERT(0.f <= input.Angle && input.Angle <= 180.f, "Invalid value for Angle param.");
	DD_ASSERT(0.f <= input.RatioDeviation && input.RatioDeviation <= 1.f, "Invalid value for RatioDeviation param.");
	DD_ASSERT(0.f <= input.Treshold && input.Treshold <= 100.f, "Invalid value for Similarity param.");
	DD_ASSERT(0.f <= input.RectangularityRatio && input.RectangularityRatio <= 1.f, "Invalid value for RectangularityRatio  param.");
	// convert floats to doubles
	static auto f2d = [](const float2 & p) { return double2((f64)p.x, (f64)p.y); };
	// convert doubles to floats
	static auto d2f = [](const double2 & p) { return float2((f32)p.x, (f32)p.y); };
	// saturate value in [-1.0...1.0]
	static auto sat = [](const float2 & p) {
		return float2(Math::Saturate(p.x, -1.f, 1.f), Math::Saturate(p.y, -1.f, 1.f));
	};
	// alias
	Building & building = output.EvaluatedBuilding;
	// regions
	const List<Region> & regions = input.SliceStick->SourceRegions;
	// simplified regions - regions are in doubles, because floating point
	// inaccuracy was causing many problems
	auto & listSimplifiedRegions = output.Simplified;
	// list with surfaces of regions
	List<PolygonLocalD<256>, 32> listRegions;
	// list with surfaces of simplified regions
	List<f32, 32> listSurfaces;


	/************************************************************************/
	/* Detect building type                                                 */
	/************************************************************************/
	#pragma region building type
	// parse simplification params
	DD::f32 angle = DD::Math::Deg2Rad<DD::f32>(input.Angle);
	DD::f32 length = input.Length;
	bool conservative = input.Conservative;

	for (const Region & region : regions) {
		// copy floats to doubles
		PolygonLocalD<256> tempPolygon;
		tempPolygon.Points.AddSequence(
			Sequencer::Drive(region.Profile.Points).Select(f2d)
		);
		listRegions.Add(tempPolygon);
		// simplify
		size_t count = 0;
		while (count != tempPolygon.Points.Size()) {
			count = tempPolygon.Points.Size();
			tempPolygon.Filter(angle, length, conservative);
		}

		new (&listSimplifiedRegions.Append()) PolygonLocalD<16>(tempPolygon);
		// compute surface
		listSurfaces.Add((f32)tempPolygon.Surface());
	}

	// starting index of roof regions
	size_t roofIndex = regions.Size() - 1;
	// distinct roof and wall slices
	for (size_t i = 0, m = regions.Size() - 1; i < m; ++i) {
		// get next region
		const DD::PointCloudAnalyzer::Region & region = regions[i];
		// compute surface of intersection
		const auto & is = DD::PolygonLocalD<256>::Intersection(
			listSimplifiedRegions.At(i),
			listSimplifiedRegions.At(i + 1)
		);
		// compute coefficient
		f32 intersectionSurface = (f32)is.Surface();
		f32 partA = listSurfaces.At(i);
		f32 partB = listSurfaces.At(i + 1);
		f32 similarity = 200.f * (intersectionSurface) / (partA + partB);
		// check
		if (similarity < input.Treshold) {
			output.left.Points.AddSequence(Sequencer::Drive(listSimplifiedRegions.At(i).Points).Select(d2f));
			output.right.Points.AddSequence(Sequencer::Drive(listSimplifiedRegions.At(i + 1).Points).Select(d2f));
			output.insec.Points.AddSequence(Sequencer::Drive(is.Points).Select(d2f));
			roofIndex = i;
			break;
		}
	}

	// check if most of polygons are rectangular
	bool isRectangular = true;
	switch (input.RectangularityCheck) {
		case Input::RectangularityChecks::AngleFilter:
			for (size_t i = roofIndex; i < listSimplifiedRegions.Size(); ++i) {
				isRectangular &= (3 <= listSimplifiedRegions[i].Points.Size() && listSimplifiedRegions[i].Points.Size() <= 5);
			}
			break;
		case Input::RectangularityChecks::OBoxRatio:
      for (size_t i = roofIndex; i < listRegions.Size(); ++i) {
        isRectangular &= input.RectangularityRatio <= listRegions[i].Surface() / listRegions[i].OBox().Surface();
      }
			break;
		default:
			DD_WARNING("Undefined RectangularityCheck.");
			break;
	}

	// compute ratio of longest/shortest lines
	List<f32, 32> listLongShortRatio;
	for (size_t i = roofIndex; i < listSimplifiedRegions.Size(); ++i) {
		// only rectangular regions could have valid ratio
		if (listSimplifiedRegions[i].Points.Size() == 4) {
			// formal param
			size_t dummy;
			// squared sizes of smallest and longest
			DD::f64 maximal2, minimal2;
			listSimplifiedRegions[i].Extremes(dummy, minimal2, dummy, maximal2);
			// compute ration
			listLongShortRatio.Add(Math::Sqrt((f32)minimal2) / Math::Sqrt((f32)maximal2));
		}
	}

	// get maximal number of the similar coefficients
	size_t similarityMax = 0;
	for (f32 left : listLongShortRatio) {
		size_t similarity = 0;
		for (f32 right : listLongShortRatio) {
			if (Math::Abs(left - right) < input.RatioDeviation) {
				++similarity;
			}
		}
		// override similarity if it is higher
		if (similarityMax < similarity) {
			similarityMax = similarity;
			// cannot be bigger
			if (similarityMax == listLongShortRatio.Size())
				break;
		}
	}

	// check if ratio of longest and shortest line is fixed or not
	bool ratioDecay = similarityMax != listLongShortRatio.Size();

	// recognition state machine
	DD::Building::Roofs::Types type;
	if (roofIndex == regions.Size() - 1)
		type = DD::Building::Roofs::Types::Flat;
	else if (!isRectangular)
		type = DD::Building::Roofs::Types::Dome;
	else if (!ratioDecay)
		type = DD::Building::Roofs::Types::Pyramidal;
	else
		type = DD::Building::Roofs::Types::Gabled;
	#pragma endregion
	/************************************************************************/
	/* Read wall properties                                                 */
	/************************************************************************/
	#pragma region building wall
	PolygonLocalD<256> aggregatedRegion;
	OBox2D box2;

	if (listSimplifiedRegions.Size()) {
		List<double2, 256> cvxHullInput;
		// select point
		for (size_t i = 1; i < roofIndex; ++i) {
			for (const double2 & point : listSimplifiedRegions[i].Points) {
				cvxHullInput.Add(point);
			}
		}
		if (cvxHullInput.Size()) {
			// set defined type
			building.Wall.Params.Type = Building::Walls::Types::Basic;
			// get aggregated region
			convexHull(cvxHullInput.ToView(), (List<double2> &)aggregatedRegion.Points);
			aggregatedRegion.Filter(angle, length, conservative);
			box2 = aggregatedRegion.OBox();
			f64 angle = box2.Angle();
			// prepare inversions
			// transform points from global-space to building-space
			auto transform = [&](const double2 & point) { return box2.LocalSpace(point); };
			// filter for obtaining vertices in the building region
			auto filter = [&aggregatedRegion](const QuadVertex & v) {
				return aggregatedRegion.IsIn(double2(v.Position.xz));
			};
			// sequence of points in the building region
			const AABox2F & area2 = aggregatedRegion.AABox().Cast<f32>();
			AABox3F area3;
			area3.Min.Set(area2.Min.x, Math::ConstantsF::Min, area2.Min.y);
			area3.Max.Set(area2.Max.x, Math::ConstantsF::Max, area2.Max.y);
			auto sequence = input.Cloud->SelectPointsSeq(area3).Filter(filter);
			// get y-extremes
			f32 miny = Math::ConstantsF::Max;
			f32 maxy = Math::ConstantsF::Min;
			for (const QuadVertex & v : sequence) {
				if (maxy < v.Position.y)
					maxy = v.Position.y;
				if (v.Position.y < miny)
					miny = v.Position.y;
			}
			// normalize profile
			// fill info into building descriptor
			building.Box.Position.xz = box2.Position;
			building.Box.Position.y = (miny + maxy) * 0.5f;
			building.Box.Radius.xz = box2.Radius;
			building.Box.Radius.y = (maxy - miny) * 0.5f;
			building.Box.Rotation = Matrices::RotationY((f32)angle);
			building.Profile.Points.AddSequence(
				Sequencer::Drive(aggregatedRegion.Points).Select(transform).Select(d2f).Select(sat)
			);
		}
	}
	#pragma endregion

	/************************************************************************/
	/* Read roof properties                                                 */
	/************************************************************************/
	#pragma region building roof
	switch (type) {
	case DD::Building::Roofs::Types::Flat: {
		building.Wall.Height = 2.f;
		building.Roof.Height = 0.f;
		building.Roof.Params.Type = DD::Building::Roofs::Types::Flat;
		building.Roof.Params.Flat.Color = sampleColor(regions.Last().Cloud->SelectPointsSeq());
	} break;
	case DD::Building::Roofs::Types::Dome: {
		// get center of dome
		double2 aggCentroidD = 0.0;
		for (size_t i = roofIndex; i < regions.Size(); ++i) {
			DD::PolygonLocalD<256> local;
			local.Points.AddSequence(Sequencer::Drive(regions[i].Profile.Points).Select(f2d));
			aggCentroidD += box2.LocalSpace(local.Centroid());
		}
		aggCentroidD /= (f32)(regions.Size() - roofIndex);

		f64 radius = 1.0/*regions[roofIndex].Profile.InscribedRadius()*/;

		// my dome
		const AABox3F & abox = regions[roofIndex].Cloud->Area();
		building.Roof.Height = 1.f - ((abox.Max.y - building.Box.Position.y) / building.Box.Radius.y);
		building.Wall.Height = 2.f - building.Roof.Height;
		building.Roof.Params.Type = DD::Building::Roofs::Types::Dome;
		building.Roof.Params.Dome.Center = aggCentroidD;
		building.Roof.Params.Dome.DomeColor = sampleColor(regions.Last().Cloud->SelectPointsSeq());
		building.Roof.Params.Dome.EdgeColor = sampleColor(
			regions[roofIndex].Cloud
			->SelectPointsSeq()
			.Filter([&regions, roofIndex](const QuadVertex & v) { return !regions[roofIndex + 1].Profile.IsIn(v.Position.xz); })
		);
		building.Roof.Params.Dome.Radius = Math::Saturate((f32)radius);

	} break;
	case DD::Building::Roofs::Types::Pyramidal: {
		double2 aggCentroidD = 0.0;
		for (size_t i = roofIndex; i < regions.Size(); ++i) {
			DD::PolygonLocalD<256> local;
			local.Points.AddSequence(Sequencer::Drive(regions[i].Profile.Points).Select(f2d));
			aggCentroidD += box2.LocalSpace(local.Centroid());
		}
		aggCentroidD /= (f32)(regions.Size() - roofIndex);
		const AABox3F & abox = regions[roofIndex].Cloud->Area();
		building.Roof.Height = 1.f - ((abox.Max.y - building.Box.Position.y) / building.Box.Radius.y);
		building.Wall.Height = 2.f - building.Roof.Height;
		building.Roof.Params.Type = DD::Building::Roofs::Types::Pyramidal;
		building.Roof.Params.Pyramidal.Center = aggCentroidD;
		building.Roof.Params.Pyramidal.Color = sampleColor(regions.Last().Cloud->SelectPointsSeq());
	} break;
	case DD::Building::Roofs::Types::Gabled:
		const AABox3F & abox = regions[roofIndex].Cloud->Area();
		building.Roof.Params.Type = DD::Building::Roofs::Types::Gabled;
		building.Roof.Height = 1.f - ((abox.Max.y - building.Box.Position.y) / building.Box.Radius.y);
		building.Wall.Height = 2.f - building.Roof.Height;
		building.Roof.Params.Gabled.Center;
		building.Roof.Params.Gabled.MainColor = sampleColor(regions.Last().Cloud->SelectPointsSeq());
		building.Roof.Params.Gabled.SideColor = Colors::GREEN;
		f64 angle = 0;
		size_t count = 0;
		for (size_t i = roofIndex + 1; i < listSimplifiedRegions.Size(); ++i) {
			const auto & region = listSimplifiedRegions[i];
			if (region.Points.Size() == 4) {
				++count;
				size_t lIndex, sIndex;
				f64 lSize2, sSize2;
				region.Extremes(sIndex, sSize2, lIndex, lSize2);
				double2 direction = Math::Normalize(region.Points[(lIndex + 1) % region.Points.Size()] - region.Points[lIndex]);
				f64 dirAngle = Math::ArcTg2(direction.y, direction.x);
				if (dirAngle < 0.0)
					dirAngle += Math::ConstantsD::Pi;

				//dirAngle = Math::ConstantsD::Pi - dirAngle;
				angle += dirAngle;
				DD_DEBUG_VARS(Math::Rad2Deg(dirAngle));
			}
		}
		angle = (angle / (f64)count) + (Math::ConstantsD::Pi * 0.5);
		DD_DEBUG_VARS(Math::Rad2Deg(angle));

		// all points from simplified regions
		auto sequence = Sequencer::Drive(listSimplifiedRegions)
			.Select([](const PolygonD & polygon) -> const List<double2> & { return polygon.Points; })
			.Unroll();
		// create box
		AABox2D box;
		const double2x2 & matrix = Matrices::Rotation(angle);
		for (const double2 & pt : sequence) {
			box.Include(pt * matrix);
		}
		// print to descriptor
		building.Box.Rotation = Matrices::RotationY((f32)angle);
		building.Box.Radius.xz = box.Size() * 0.5f;
		building.Box.Position.xz = matrix * box.Center();
		break;
	}
	#pragma endregion
}
#pragma endregion

void PointCloudAnalyzer::HistogramRemoval::Evaluate(Input & input, Output & output) {
	DD_PROFILE_SCOPE_SIMPLE("histogram");
	/************************************************************************/
	/* Ad hoc structs                                                       */
	/************************************************************************/
	struct HistogramTile {
		/** Histogram accumulator. */
		u32 * Histogram;
		/** Histogram accumulator. */
		u32 * HistogramAgg;
		/** Boolean filter for filtering histogram. */
		bool * Filter;
		/** histogram tile coordinates. */
		u32 X, Y;
		/** Number of vertices in selection. */
		u32 Size;
		/** Handle to task in which is tile processed. */
		Concurrency::ThreadPool::TaskLocal<64> Routine;
		/** AABB of @see Selection area. */
		AABox3F Area;
	};

	/************************************************************************/
	/* Init constants for computation                                       */
	/************************************************************************/
	AABox3F pcArea = input.Cloud->Area();
	float3 range = pcArea.Size();
	size_t histogramResolution = (size_t)Math::Ceil(range.y / input.Step);
	// check constants
	DD_ASSERT(range.x > 0.f, "PointCloud area must be positive in all axes.");
	DD_ASSERT(range.y > 0.f, "PointCloud area must be positive in all axes.");
	DD_ASSERT(range.z > 0.f, "PointCloud area must be positive in all axes.");
	DD_ASSERT(histogramResolution, "Resolution of histogram must be positive value.");
	DD_ASSERT(input.Step > 0.f, "Height step must be positive.");

	/************************************************************************/
	/* Allocation - partially data oriented design                          */
	/************************************************************************/
	// array for tiles
	f32 tilesX = Math::Max(1.f, Math::Ceil((range.x) / input.Area.x));
	f32 tilesY = Math::Max(1.f, Math::Ceil((range.z) / input.Area.y));
	DD_ASSERT(tilesX > 0.f, "Tile-dimension must be a positive number.");
	DD_ASSERT(tilesY > 0.f, "Tile-dimension must be a positive number.");
	Array2D<HistogramTile> tiles({ (size_t)tilesY, (size_t)tilesX });
	// array for all histograms of all tiles
	Collections::BinArray<size_t, 3> size(histogramResolution, tiles.Lengths()[0], tiles.Lengths()[1]);
	Array3D<u32> histograms         = Array3D<u32>::Uniform(size, 0);
	Array3D<u32> aggHistograms      = Array3D<u32>::Uniform(size, 0);
	Array3D<bool> histogramToFilter(size);
	// check dimensions of allocation
	DD_ASSERT(tiles.Lengths()[0], "Tile-dimension must be a positive number.");
	DD_ASSERT(tiles.Lengths()[1], "Tile-dimension must be a positive number.");
	DD_ASSERT(histograms.Length(), "Must be allocated at least one histogram.");

	// callback for size
	if (input.Callbacks.NumberOfTiles) {
		input.Callbacks.NumberOfTiles->Run(tiles.Length() * 2);
	}

	/************************************************************************/
	/* Compute histogram per tile                                           */
	/************************************************************************/
	// convert height into index in histogram
	auto y2i = [&](f32 height) {
		return Math::Min(
			(size_t)(((height - pcArea.Min.y) / range.y) * histogramResolution),
			histogramResolution
		);
	};

	// create an observer
	IProgress::ScopeCounter32A counter(input.Callbacks.Progress, (i32)tiles.PlainView().Length() * 2);

 	// create histogram for each tile
 	for (u32 y = 0; y < tiles.Lengths()[1]; ++y) for (u32 x = 0; x < tiles.Lengths()[0]; ++x) {
 		// convert x-y coords to linear index
 		HistogramTile & tile = tiles[y][x];
 		// bounds of a tile
		tile.X = x;
		tile.Y = y;
 		tile.Area.Min.Set(pcArea.Min.x + x * input.Area.x, pcArea.Min.y, pcArea.Min.z + y * input.Area.y);
 		tile.Area.Max.Set(tile.Area.Min.x + input.Area.x, pcArea.Max.y, tile.Area.Min.z + input.Area.y);
 		tile.Histogram = histograms[y][x].Ptr();
		tile.HistogramAgg = aggHistograms[y][x].Ptr();
		tile.Filter = histogramToFilter[y][x].Ptr();
 		// asynchronous routine
 		tile.Routine = [histogramResolution, &input, &tile, &y2i, &counter]() mutable {
 			// create histogram
 			tile.Size;
 			for (QuadVertex * vertex : input.Cloud->SelectPoints(tile.Area)) {
 				// height to histogram index
 				++tile.Histogram[y2i(vertex->Position.y)];
 				++tile.Size;
 			}
 			// convert histogram to filter
 			if (tile.Size) {
 				f32 averageCount = tile.Size / (f32)histogramResolution;
 			}
 			// callback that tile is done
 			if (input.Callbacks.OneTileDone) {
 				input.Callbacks.OneTileDone->Run();
 			}
			if (input.Callbacks.Progress) {
				++counter;
			}
 		};
 	}

	for (HistogramTile & tile : tiles.ToView().PlainView()) {
		tile.Routine.Wait();
	}

	/************************************************************************/
	/* Compute filters from histograms                                      */
	/************************************************************************/
	// init output clouds
	float3 tileSize(input.Area.x, range.y, input.Area.y);
	if (input.Mode & Input::Modes::Negative)
		output.Negative = Simulation::PointCloud(input.Cloud->Area(), tileSize);
	if (input.Mode & Input::Modes::Positive)
		output.Positive = Simulation::PointCloud(input.Cloud->Area(), tileSize);

	for (u32 y = 0; y < tiles.Lengths()[1]; ++y) for (u32 x = 0; x < tiles.Lengths()[0]; ++x) {
		HistogramTile & tile = tiles[y][x];
		// create histogram filter from (neighbor) tiles.
		tile.Routine = [&tile, &input, &output, &tiles, &y2i, histogramResolution, &counter]() {
			// indices of neighbor tiles
			u32 minX, maxX;
			if (tile.X <= input.Range.x) {
				minX = 0;
				maxX = Math::Min(minX + 2 * input.Range.x, (u32)tiles.Lengths()[0] - 1);
			}
			else if ((u32)tiles.Lengths()[0] <= tile.X + input.Range.x) {
				maxX = (u32)tiles.Lengths()[0] - 1;
				minX = (u32)Math::Min((i32)maxX - 2 * (i32)input.Range.x, 0);
			}
			else {
				minX = tile.X - input.Range.x;
				maxX = tile.X + input.Range.x;
			}
			u32 minY, maxY;
			if (tile.Y <= input.Range.y) {
				minY = 0;
				maxY = Math::Min(minY + 2 * input.Range.y, (u32)tiles.Lengths()[1] - 1);
			}
			else if ((u32)tiles.Lengths()[1] <= tile.Y + input.Range.y) {
				maxY = (u32)tiles.Lengths()[1] - 1;
				minY = (u32)Math::Min((i32)maxY - 2 * (i32)input.Range.y, 0);
			}
			else {
				minY = tile.Y - input.Range.y;
				maxY = tile.Y + input.Range.y;
			}

			u32 totalCount = 0;
			// for each neighbor tile, aggregate histogram
			for (u32 friendX = minX; friendX <= maxX; ++friendX) for (u32 friendY = minY; friendY <= maxY; ++friendY) {
				HistogramTile & friendTile = tiles[friendY][friendX];
				for (u32 histogramIndex = 0; histogramIndex < histogramResolution; ++histogramIndex) {
					tile.HistogramAgg[histogramIndex] += friendTile.Histogram[histogramIndex];
					totalCount += friendTile.Histogram[histogramIndex];
				}
			}
			// convert histogram to filter
			f32 averageCount = (f32)totalCount/ histogramResolution;
			f32 thresholdCount = averageCount * input.AverageDeviation;
			for (u32 histogramIndex = 0; histogramIndex < histogramResolution; ++histogramIndex) {
				// tile.Filter[histogramIndex] = (tile.HistogramAgg[histogramIndex] * input.AverageDeviation) < totalCount;
				tile.Filter[histogramIndex] = tile.HistogramAgg[histogramIndex] > thresholdCount;
			}

#if 0 // Debug for sample histograms
			DD::Debug::Log::PrintLine("Histogram:");
			for (u32 histogramIndex = 0; histogramIndex < histogramResolution; ++histogramIndex) {
				DD::Debug::Log::Print(tile.HistogramAgg[histogramIndex], ", ");
			}
			DD::Debug::Log::PrintLine();
#endif

			// fill output
			switch (input.Mode) {
				case Input::Modes::Negative: {
					auto & negative = output.Negative->Grid()[{0, tile.Y, tile.X}];
					for (const Simulation::QuadVertex & v : input.Cloud->SelectPointsSeq(tile.Area)) {
						if (!tile.Filter[y2i(v.Position.y)])
							negative.Add(v);
					}
				} break;
				case Input::Modes::Positive: {
					auto & positive = output.Positive->Grid()[{0, tile.Y, tile.X}];
					for (const Simulation::QuadVertex & v : input.Cloud->SelectPointsSeq(tile.Area)) {
						if (tile.Filter[y2i(v.Position.y)])
							positive.Add(v);
					}
				} break;
				case Input::Modes::Both: {
					auto & negative = output.Negative->Grid()[{0, tile.Y, tile.X}];
					auto & positive = output.Positive->Grid()[{0, tile.Y, tile.X}];
					for (const Simulation::QuadVertex & v : input.Cloud->SelectPointsSeq(tile.Area)) {
						(tile.Filter[y2i(v.Position.y)]) ? positive.Add(v) : negative.Add(v);
					}
				} break;
				default: break;
			}
			// callback that tile is done
			if (input.Callbacks.OneTileDone) {
				input.Callbacks.OneTileDone->Run();
			}
			if (input.Callbacks.Progress) {
				++counter;
			}
		};
	}

	for (HistogramTile & tile : tiles.ToView().PlainView()) {
		tile.Routine.Wait();
	}

	// fix number of points in point clouds
	if (output.Negative)
		output.Negative->RecountPoints();
	if (output.Positive)
		output.Positive->RecountPoints();
}
