#if defined _M_IX86
#pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
#pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif

#include <DD/Concurrency/Thread.h>
#include <DD/Concurrency/ThreadSafeObject.h>
#include <DD/List.h>
#include <DD/Debug.h>
#include <DD/Profiler.h>
#include <DD/Math.h>
#include <DD/BTree.h>

#include <DD/Controls/IControl.h>
#include <DD/Controls/ButtonControl.h>
#include <DD/Controls/CheckboxControl.h>
#include <DD/Controls/ListView.h>
#include <DD/Controls/Window.h>
#include <DD/Controls/PanelControl.h>
#include <DD/Controls/GridControl.h>
#include <DD/Controls/LabelControl.h>
#include <DD/Controls/SliderControl.h>
#include <DD/Controls/TextControl.h>
#include <DD/Controls/OptionControl.h>
#include <DD/Controls/TabViewControl.h>

#include <windows.h>
#include <windowsx.h>
#include <CommCtrl.h>
#include <ShellScalingApi.h>
#include <shellapi.h>

#pragma comment(lib, "Comctl32.lib")
#pragma comment(lib, "Shcore.lib")

#define CONTROLS_LOG 0
#define WM_TRAY 0x01d2

using namespace DD;
using namespace DD::Controls;

namespace Libraries {
	static BTree<Color, HBRUSH> Brushes;
	static BTree<String, HFONT> Fonts;
}

struct WindowBase::NativeView {
	HWND Window;
	//HINSTANCE Instance;
	//HANDLE Thread;
};

/************************************************************************/
/* Helpers                                                              */
/************************************************************************/
#pragma region helpers
static void compute(
	IControl::Rect parent,
	IControl::VAlign valignment,
	IControl::HAlign halignment,
	i32 margin,
	IControl::Rect design,
	IControl::Rect & actual
) {
	switch (valignment) {
		case IControl::VAlign::None:
			actual.Height = design.Height;
			actual.Top = design.Top + margin;
			break;
		case IControl::VAlign::Bottom:
			actual.Height = Math::Min(design.Height, parent.Height - 2 * margin);
			actual.Top = parent.Height - actual.Height;
			break;
		case IControl::VAlign::Center:
			actual.Height = Math::Min(design.Height, parent.Height - 2 * margin);
			actual.Top = (parent.Width - design.Width) / 2;
			break;
		case IControl::VAlign::Top:
			actual.Height = Math::Min(design.Height, parent.Height - 2 * margin);
			actual.Top = margin;
			break;
		case IControl::VAlign::Stretch:
			actual.Height = parent.Height - 2 * margin;
			actual.Top = margin + parent.Top;
			break;
	}

	switch (halignment) {
		case IControl::HAlign::None:
			actual.Width = design.Width;
			actual.Left = design.Left + margin;
			break;
		case IControl::HAlign::Left:
			actual.Width = Math::Min(design.Width, parent.Width - 2 * margin);
			actual.Left = margin + design.Left;
			break;
		case IControl::HAlign::Center:
			actual.Width = Math::Min(design.Width, parent.Width - 2 * margin);
			actual.Left = (parent.Width - actual.Width) / 2;
			break;
		case IControl::HAlign::Right:
			actual.Width = Math::Min(design.Width, parent.Width - 2 * margin);
			actual.Left = parent.Width - actual.Width;
			break;
		case IControl::HAlign::Stretch:
			actual.Width = parent.Width - 2 * margin;
			actual.Left = margin + parent.Left;
			break;
	}
}

static bool simpleClass(const wchar_t * className, WNDPROC proc) {
	WNDCLASSEX wcex;
	// The size, in bytes, of this structure.
	wcex.cbSize = sizeof(WNDCLASSEX);
	// The class style(s). This member can be any combination of the Class Styles.
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	// A pointer to the window procedure.
	wcex.lpfnWndProc = proc;
	// The number of extra bytes to allocate following the window-class structure.
	wcex.cbClsExtra = 0;
	// The number of extra bytes to allocate following the window instance.
	wcex.cbWndExtra = 0;
	// A handle to the instance that contains the window procedure for the class.
	wcex.hInstance = reinterpret_cast<HINSTANCE>(GetModuleHandleW(NULL));
	// [Resource] A handle to the class icon.
	wcex.hIcon = 0;
	// [Resource] A handle to the class cursor.
	wcex.hCursor = 0;
	// Pointer to a null-terminated character string that specifies the resource name of the class menu, as the name appears in the resource file.
	wcex.lpszMenuName = nullptr;
	// A handle to a small icon that is associated with the window class.
	wcex.hIconSm = 0;
	// A handle to the class background brush.
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW);
	// A pointer to a null-terminated string or is an atom.
	wcex.lpszClassName = className;

	// Register class.
	if (!RegisterClassEx(&wcex)) {
		DD_LOG("simpleClass registration failed:", className);
		throw;
	}

	return true;
}

/**
 * Register window class with init method [name]Init and [name]Proc.
 */
#define DD_REGISTER_SIMPLE_WINAPI_CLASS(name)\
static LRESULT name##Proc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);\
\
wchar_t * name##Init() {\
	static wchar_t className[] = L"DDClass" L###name;\
	static bool init = simpleClass(className, (WNDPROC)&name##Proc);\
	return className;\
}\
\
static LRESULT name##Proc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)\

static const float Scale(HWND hwnd = nullptr) {
	static HMONITOR monitor;
	static DEVICE_SCALE_FACTOR nativeFactor;
	static float factor = 1.f;
	static HWND _hwnd = 0;

	if (_hwnd != hwnd) {
		monitor = MonitorFromWindow(hwnd, MONITOR_DEFAULTTONEAREST);
		GetScaleFactorForMonitor(monitor, &nativeFactor);
		factor = ((int)nativeFactor) / 100.f;
		_hwnd = hwnd;
	}


	return factor;
}

template<typename T>
static T Scaled(void * hwnd, T t) { return (T)(t * Scale((HWND)hwnd)); }

#pragma endregion

 /************************************************************************/
 /* WindowProcess                                                        */
 /************************************************************************/
#pragma region window process

u32 GMainThreadId = Concurrency::Thread::Id();

struct WindowProcess {

	static WindowProcess& Instance() { static WindowProcess instance; return instance; }

private: // properties
	/** Commands which must be done in main thread. */
	List<Lambda<void()>> CommandBuffer;

public:  // properties
	/** Registered windows. */
	Concurrency::ThreadSafeObject<List<Window, 256>> Windows;
	/** Commands which must be done in main thread. */
	Concurrency::ThreadSafeObject<List<Lambda<void()>>> Commands;

public: // methods
	/** Loop for processing commands and window messages. */
	void Loop();
	/** Register window. */
	void Register(WindowBase * w) { Windows->Add(w); }
	/** Unregister window. */
	void Unregister(WindowBase * w) { Windows->TryRemove(w); }
	/** Remove registered objects from process holders. */
	void Destroy();

private: // constructors
	/** Singleton constructor. */
	WindowProcess() { Commands->Reserve(1024), CommandBuffer.Reserve(1024); }
	/** Destructor. */
	~WindowProcess() { Destroy(); }
};

#define GWindowProcess WindowProcess::Instance()
#define GCommand(...) {\
	auto __COMMAND = __VA_ARGS__;\
	/*if (Thread::Id() == GMainThreadId)*/\
	/*	__COMMAND();*/\
	/*else*/\
	 GWindowProcess.Commands->Add(__COMMAND);\
}

void WindowProcess::Loop() {
	// disable scaling issues
	SetProcessDpiAwareness(PROCESS_DPI_AWARENESS::PROCESS_PER_MONITOR_DPI_AWARE);

	MSG msg = { 0 };

	while (WM_QUIT != msg.message && WM_CLOSE != msg.message) {
		BOOL success = false;
		success = PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE);
		if (success) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else {
			Commands->Swap(CommandBuffer);
			for (int i = 0, m = (int)CommandBuffer.Size(); i < m;) {
				for (int limit = Math::Min(m, i + 2000); i < limit; ++i) {
					CommandBuffer[i]();
				}
				while (success = PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE)) {
					TranslateMessage(&msg);
					DispatchMessage(&msg);
					if (WM_QUIT == msg.message || WM_CLOSE == msg.message)
						return;
				}
			}
			CommandBuffer.Clear();
			{
				auto proxy = *Windows;
				for (WindowBase * w : *proxy)
					if (w->Ready())
						w->OnIdle();
			}

		}
	}
}

void WindowProcess::Destroy() {
	auto proxy = Windows.GetProxy();
	for (auto window : *proxy)
		if (window)
			window->Destroy();
	proxy->Free();
}
#pragma endregion

/************************************************************************/
/* IControl                                                             */
/************************************************************************/
#pragma region icontrol
/**
 * Init should not contain any delegate assignment.
 * Delegate assignments should be placed in iControlAdd
 *  and should be correctly unassigned in iControlRemove.
 */
void IControl::Init(IControl * parent) {
	if (GMainThreadId != Concurrency::Thread::Id()) {
		GCommand([this, parent]() { if (!Ready()) Init(parent); });
		while (!Ready()) MemoryBarrier();
		return;
	}

	if (_ready && _parent) {
		::SetParent((HWND)NativeHandle(), (HWND)parent->NativeHandle());
		_parent = parent;
	}
	else {
		_parent = parent;
		iControlInit();
	}

	recomputeRect();
	recomputeClient();
	OnSizeChanged();

	if (parent) {
		_tooltipHandle = parent->_tooltipHandle;
	}

	auto childrenProxy = *_children;
	for (auto & child : *childrenProxy) {
		//if (!child->Ready())
			child->Init(this);
	}

	if (_menuHandle) {
		SetMenu((HWND)NativeHandle(), (HMENU)_menuHandle);
	}

	_ready = true;

	if (!Tooltip().IsEmpty())
		Tooltip(Tooltip());

	Show(Show());
	Enabled(Enabled());
	Background(Background());
	Font(FontFamily(), FontSize());
	OnSizeChanged();
}

void IControl::Destroy() {
	if (_ready) {
 		Remove();
// 		for (auto & child : _children) {
// 			child->Destroy();
// 		}
// 		_children.Free();
		iControlDestroy();
	}

	_ready = false;
}

void IControl::iControlDestroy() {
	for (auto & handle : _handles) {
		if (handle) {
			SetWindowLongPtr((HWND)handle, GWLP_USERDATA, 0);
			DestroyWindow((HWND)handle);
			handle = 0;
		}
	}
	if (_menuHandle) {
		DestroyMenu((HMENU)_menuHandle);
	}
}

void IControl::Add(IControl * ctrl) {
	// holder because of of potential deallocation
	Reference<IReferential> holder = ctrl;
	// remove if the ctrl is currently bound to another parent
	ctrl->Remove();

	OnChildAdd(ctrl);
	ctrl->Scale(_scale);
	if (Ready())
		ctrl->Init(this);

	_children->Add(ctrl);
}

void IControl::Remove() {
	if (_parent)
		_parent->Remove(this);
}

void IControl::Remove(IControl * ctrl) {
	if (_children->TryRemove(ctrl)) {
		InvalidateRect((HWND)NativeHandle(), NULL, TRUE);
		OnChildRemove(ctrl);
	}
}

void IControl::recomputeClient() {
	RECT client;
	GetClientRect((HWND)NativeHandle(), &client);
	_client.Left = client.left;
	_client.Top = client.top;
	_client.Width = client.right - client.left;
	_client.Height = client.bottom - client.top;
}

void IControl::recomputeWindow() {
	RECT window;
	GetWindowRect((HWND)NativeHandle(), &window);
	_design.Left = window.left;
	_design.Top = window.top;
	_design.Width = window.right - window.left;
	_design.Height = window.bottom - window.top;
}

void IControl::recomputeRect() {
	if (_handles[0]) {
		if (_parent == nullptr) {
			_actual = _design;
			iControlRect(_actual);
		}
		else if(_dedicatedRect) {
			_actual = _design;
			Rect actual = _actual;
			actual.Left -= _parent->_hOffset;
			actual.Top -= _parent->_vOffset;
			iControlRect(actual);
			recomputeClient();
		}
		else {
			compute(_parent->_client, _valign, _halign, _margin, _design, _actual);
			Rect actual = _actual;
			actual.Left -= _parent->_hOffset;
			actual.Top -= _parent->_vOffset;
			iControlRect(actual);
			iControlRect(actual);
			recomputeClient();
		}
	}
}

void IControl::iControlRect(const Rect & actual) {
	BOOL succeed = SetWindowPos(
		// A handle to the window.
		(HWND)NativeHandle(),
		// A handle to the window to precede the positioned window in the Z order.
		HWND_TOP,
		// The new position of the left side of the window, in client coordinates.
		actual.Left,
		// The new position of the top of the window, in client coordinates.
		actual.Top,
		// The new width of the window, in pixels.
		actual.Width,
		// The new height of the window, in pixels.
		actual.Height,
		// The window sizing and positioning flags. [SWP_]
		0
	);
}

void IControl::Height(i32 height, bool scaled) {
	if (scaled) {
		height = Scaled(NativeHandle(), height);
	}
	if (_design.Height != height) {
		_design.Height = height;
		recomputeRect();
	}
}

void IControl::Width(i32 width, bool scaled) {
	if (scaled) {
		width = Scaled(NativeHandle(), width);
	}
	if (_design.Width != width) {
		_design.Width = width;
		recomputeRect();
	}
}

void IControl::Scale(f32 scale, bool autopropagation) {
	_scale = scale;
	OnScaleChanged();

	if (autopropagation) {
		auto childrenProxy = *_children;
		for (auto & child : *childrenProxy) {
			child->Scale(scale);
		}
	}
}

void IControl::Tooltip(const String & tip) {
	_tooltip = tip;
	if (_tooltipHandle) {
		TOOLINFO toolInfo = { 0 };
		toolInfo.cbSize = sizeof(toolInfo);
		toolInfo.uFlags = TTF_IDISHWND | TTF_SUBCLASS;
		toolInfo.uId = (UINT_PTR)_handles[0];
		toolInfo.lpszText = _tooltip.begin();

		SendMessage((HWND)_tooltipHandle, TTM_ADDTOOL, 0, (LPARAM)&toolInfo);

#if _DEBUG
		if (!Enabled())
			DD_WARNING("On disabled controls, tooltip will not be working");
#endif
	}
}

void IControl::Grid(const GridPosition & newGrid) {
	GridPosition oldGrid = _grid;
	_grid = newGrid;
	OnGridChanged(oldGrid, newGrid);
}

void IControl::GridCol(i32 col) {
	GridPosition grid = Grid();
	grid.Col = col;
	Grid(grid);
}

void IControl::GridColSpan(i32 colspan) {
	GridPosition grid = Grid();
	grid.ColSpan = colspan;
	Grid(grid);
}

void IControl::GridRow(i32 row) {
	GridPosition grid = Grid();
	grid.Row = row;
	Grid(grid);
}

void IControl::GridRowSpan(i32 rowspan) {
	GridPosition grid = Grid();
	grid.RowSpan = rowspan;
	Grid(grid);
}

void IControl::Design(const Rect & design) {
	if (
		_design.Left != design.Left ||
		_design.Top != design.Top ||
		_design.Width != design.Width ||
		_design.Height != design.Height
	) {
		_design = design;
		recomputeRect();
	}
}

void IControl::Left(i32 left) {
	if (_design.Left != left) {
		_design.Left = left;
		recomputeRect();
	}
}

void IControl::Top(i32 top) {
	if (_design.Top != top) {
		_design.Top = top;
		recomputeRect();
	}
}

void IControl::Background(Color c) {
	_background = c;
}

void IControl::Font(Text::StringView16 family, i32 size) {
	_fontName = family;
	_fontSize = size;


	if (Ready()) {
		StringLocal<32> fontId;
		fontId.AppendBatch(family, size);
		HFONT & font = Libraries::Fonts[fontId];
		if (font == NULL) {
			font = CreateFont(
				// The height, in logical units, of the font's character cell or character.
				(int)ceil(Scale() * size),
				// The average width, in logical units, of characters in the requested font. If this value is zero, the font mapper chooses a closest match value.
				0,
				// The angle, in tenths of degrees, between the escapement vector and the x-axis of the device.
				0,
				// The angle, in tenths of degrees, between each character's base line and the x-axis of the device.
				0,
				// The weight of the font in the range 0 through 1000.
				FW_DONTCARE,
				// Specifies an italic font if set to TRUE.
				FALSE,
				// Specifies an underlined font if set to TRUE.
				FALSE,
				// A strikeout font if set to TRUE.
				FALSE,
				// The character set.The following values are predefined :
				DEFAULT_CHARSET,
				// The output precision.
				OUT_TT_PRECIS,
				// The clipping precision.
				CLIP_DEFAULT_PRECIS,
				// The output quality.
				DEFAULT_QUALITY,
				// The pitch and family of the font.
				DEFAULT_PITCH | FF_DONTCARE,
				// A pointer to a null - terminated string that specifies the typeface name of the font.
				family
			);
		}

		if (_handles[1])
			SetWindowFont((HWND)_handles[1], font, TRUE);
	}
}

void IControl::Show(bool show) {
	_show = show;
	if (Ready()) {
		ShowWindow((HWND)NativeHandle(), _show ? SW_SHOW : SW_HIDE);
	}
}

void IControl::Enabled(bool enabled) {
	_enabled = enabled;
	if (Ready()) {
		EnableWindow((HWND)NativeHandle(), _enabled);
		OnSizeChanged();
	}
}

IControl::IControl()
	: _handles()
	, _menuHandle()
{
	_design.Height = Defaults::Height;
	_design.Width = Defaults::Width;
	_design.Left = Defaults::Left;
	_design.Top = Defaults::Top;
	_fontName = Defaults::FontFamily;
	_fontSize = Defaults::FontSize;
	_background = Defaults::Background;
	_halign = Defaults::HorizontalAlignment;
	_valign = Defaults::VerticalAlignment;
}

WindowBase::MenuProxy WindowBase::MenuProxy::AddItem(Text::StringView16 name) {
	HMENU hMenu = CreatePopupMenu();
	AppendMenuW((HMENU)_handle, MF_POPUP, (UINT_PTR)hMenu, name.begin());
	MenuProxy menu;
	menu._parent = _parent;
	menu._handle = hMenu;
	menu._routines = (List<Event<i32>> *)(&menu);

	return menu;
}

i32 WindowBase::MenuProxy::AddItem(Text::StringView16 name, Delegate<i32> routine) {
	i32 id = MENU_FLAG | (i32)_parent->_menu.Size();
	AppendMenuW((HMENU)_handle, MF_STRING, id, name.begin());
	_parent->_menu.Append() += routine;
	return id;
}

WindowBase::MenuProxy Window::MenuProxy::InsertItem(int index, Text::StringView16 name) {
	HMENU hMenu = CreatePopupMenu();
	InsertMenuW((HMENU)_handle, index,  MF_POPUP, (UINT_PTR)hMenu, name.begin());
	MenuProxy menu;
	menu._parent = _parent;
	menu._handle = hMenu;
	menu._routines = _routines;

	return menu;
}

i32 WindowBase::MenuProxy::InsertItem(i32 index, Text::StringView16 name, Delegate<i32> routine) {
	i32 id = MENU_FLAG | (i32)_parent->_menu.Size();
	InsertMenuW((HMENU)_handle, index, MF_STRING, id, name.begin());
	_routines->Append() += routine;
	return id;
}

void WindowBase::MenuProxy::CheckItem(i32 id, bool check) {
	DWORD result = CheckMenuItem((HMENU)_handle, id, check ? MF_CHECKED : MF_UNCHECKED);
	DD_LOG((int)result)
}

bool WindowBase::MenuProxy::IsChecked(i32 id) {
	MENUITEMINFO info;
	info.cbSize = sizeof(info);
	info.fMask = MIIM_STATE;
	if (GetMenuItemInfo((HMENU)_handle, id, FALSE, &info)) {
		return MFS_CHECKED & info.fState;
	}
	else {
		DD_WARNING((int)GetLastError());
		return false;
	}
}

Window::MenuProxy Window::MenuProxy::GetItem(int index) {
	MenuProxy menu;
	menu._parent = _parent;
	menu._handle = (void*)GetSubMenu((HMENU)_handle, index);
	menu._routines = _routines;

	return menu;
}

void Window::MenuProxy::RemoveItem(int index) {
	DeleteMenu((HMENU)_handle, index, MF_BYPOSITION);
}

void Window::MenuProxy::Destroy() {
	DestroyMenu((HMENU)_handle);
}

int Window::MenuProxy::Size() const {
	return GetMenuItemCount((HMENU)_handle);
}

#pragma endregion

/************************************************************************/
/* PanelControl                                                         */
/************************************************************************/
#pragma region panel
#if CONTROLS_LOG
#define PANEL_LOG(...) DD_LOG("PanelLog:", __VA_ARGS__)
#else
#define PANEL_LOG(...)
#endif

DD_REGISTER_SIMPLE_WINAPI_CLASS(panel) {
	static constexpr int SCROLL_LINE = 20;
	PanelControlBase * ddPanel = (PanelControlBase *)GetWindowLongPtr(hWnd, GWLP_USERDATA);
	if (ddPanel) switch (message) {
		case WM_HSCROLL: {
			SCROLLINFO sinfo;
			sinfo.cbSize = sizeof(sinfo);
			sinfo.fMask = SIF_ALL;
			GetScrollInfo((HWND)ddPanel->NativeHandle(), SB_HORZ, &sinfo);
			switch (LOWORD(wParam)) {
				case SB_LEFT: ddPanel->HorizontalOffset(sinfo.nMin); break;
				case SB_RIGHT: ddPanel->HorizontalOffset(sinfo.nMax); break;
				case SB_PAGELEFT: ddPanel->HorizontalOffset(ddPanel->HorizontalOffset() - sinfo.nPage); break;
				case SB_PAGERIGHT: ddPanel->HorizontalOffset(ddPanel->HorizontalOffset() + sinfo.nPage); break;
				case SB_LINELEFT: ddPanel->HorizontalOffset(ddPanel->HorizontalOffset() - SCROLL_LINE); break;
				case SB_LINERIGHT: ddPanel->HorizontalOffset(ddPanel->HorizontalOffset() + SCROLL_LINE); break;
				case SB_THUMBTRACK: ddPanel->HorizontalOffset(sinfo.nTrackPos); break;
				case SB_THUMBPOSITION: ddPanel->HorizontalOffset(sinfo.nTrackPos); break;
			}
		} return 0;
		case WM_VSCROLL: {
			SCROLLINFO sinfo;
			sinfo.cbSize = sizeof(sinfo);
			sinfo.fMask = SIF_ALL;
			GetScrollInfo((HWND)ddPanel->NativeHandle(), SB_VERT, &sinfo);
			switch (LOWORD(wParam)) {
				case SB_TOP: ddPanel->VerticalOffset(sinfo.nMin); break;
				case SB_BOTTOM: ddPanel->VerticalOffset(sinfo.nMax);; break;
				case SB_PAGEUP: ddPanel->VerticalOffset(ddPanel->VerticalOffset() - sinfo.nPage); break;
				case SB_PAGEDOWN: ddPanel->VerticalOffset(ddPanel->VerticalOffset() + sinfo.nPage); break;
				case SB_LINEUP: ddPanel->VerticalOffset(ddPanel->VerticalOffset() - SCROLL_LINE); break;
				case SB_LINEDOWN: ddPanel->VerticalOffset(ddPanel->VerticalOffset() + SCROLL_LINE); break;
				case SB_THUMBTRACK: ddPanel->VerticalOffset(sinfo.nTrackPos); break;
				case SB_THUMBPOSITION: ddPanel->VerticalOffset(sinfo.nTrackPos); break;
			}
		} return 0;
		case WM_SIZE: {
			ddPanel->OnSizeChanged();
		} break;
		case WM_KEYDOWN: {
			switch (wParam) {
				case VK_UP: SendMessage((HWND)ddPanel->NativeHandle(), WM_VSCROLL, MAKELONG(SB_LINEUP, 0), 0L); break;
				case VK_PRIOR: SendMessage((HWND)ddPanel->NativeHandle(), WM_VSCROLL, MAKELONG(SB_PAGEUP, 0), 0L); break;
				case VK_NEXT: SendMessage((HWND)ddPanel->NativeHandle(), WM_VSCROLL, MAKELONG(SB_PAGEDOWN, 0), 0L); break;
				case VK_DOWN: SendMessage((HWND)ddPanel->NativeHandle(), WM_VSCROLL, MAKELONG(SB_LINEDOWN, 0), 0L); break;
				case VK_HOME: SendMessage((HWND)ddPanel->NativeHandle(), WM_VSCROLL, MAKELONG(SB_TOP, 0), 0L); break;
				case VK_END: SendMessage((HWND)ddPanel->NativeHandle(), WM_VSCROLL, MAKELONG(SB_BOTTOM, 0), 0L); break;
				case VK_LEFT: SendMessage((HWND)ddPanel->NativeHandle(), WM_HSCROLL, MAKELONG(SB_LINELEFT, 0), 0L); break;
				case VK_RIGHT: SendMessage((HWND)ddPanel->NativeHandle(), WM_HSCROLL, MAKELONG(SB_LINERIGHT, 0), 0L); break;
			} return 0;
		}
		case WM_MOUSEWHEEL: {
			if (GET_WHEEL_DELTA_WPARAM(wParam) > 0)
				SendMessage((HWND)ddPanel->NativeHandle(), WM_VSCROLL, MAKELONG(SB_LINEUP, 0), 0L);
			else
				SendMessage((HWND)ddPanel->NativeHandle(), WM_VSCROLL, MAKELONG(SB_LINEDOWN, 0), 0L);
		} return 0;
		case WM_MOUSEHWHEEL: {
			if (GET_WHEEL_DELTA_WPARAM(wParam) < 0)
				SendMessage((HWND)ddPanel->NativeHandle(), WM_HSCROLL, MAKELONG(SB_LINELEFT, 0), 0L);
			else
				SendMessage((HWND)ddPanel->NativeHandle(), WM_HSCROLL, MAKELONG(SB_LINERIGHT, 0), 0L);
		} return 0;
		case WM_RBUTTONDOWN:
		case WM_LBUTTONDOWN: {
			SetFocus((HWND)ddPanel->NativeHandle());
		} return 0;
		case WM_ERASEBKGND: {
			Color bg = ddPanel->Background();
			if (bg.a == 255) {
				RECT rect;
				HBRUSH & brush = Libraries::Brushes[bg];
				if (brush == NULL) {
					brush = CreateSolidBrush(RGB(bg.r, bg.g, bg.b));
				}
				SelectObject((HDC)wParam, brush);
				GetClientRect((HWND)ddPanel->NativeHandle(), &rect);
				Rectangle((HDC)wParam, rect.left - 1, rect.top - 1, rect.right + 1, rect.bottom + 1);
			}
			else {
				break;
			}
		} return 0;
		case WM_PAINT: {
			PAINTSTRUCT ps;
			BeginPaint((HWND)ddPanel->NativeHandle(), &ps);
			EndPaint((HWND)ddPanel->NativeHandle(), &ps);
			return 0;
		}
		default:
			break;
	}
	return DefWindowProc(hWnd, message, wParam, lParam);
}

void PanelControlBase::iControlInit() {
	DWORD style = 0;
	if (_show)
		style |= WS_VISIBLE;

	_handles[0] = CreateWindowEx(
		0,                                        // no extended styles
		panelInit(),                                // global string containing name of window class
		L"",                                      // global string containing title bar text
		WS_CHILD | WS_HSCROLL | WS_VSCROLL | style, // window styles
		CW_USEDEFAULT,                            // default horizontal position
		CW_USEDEFAULT,                            // default vertical position
		CW_USEDEFAULT,                            // default width
		CW_USEDEFAULT,                            // default height
		(HWND)_parent->NativeHandle(),             // no parent for overlapped windows
		(HMENU)NULL,                              // use the window class menu
		GetModuleHandle(NULL),                    // global instance handle
		(PVOID)NULL                               // pointer not needed
	);

	SetWindowLongPtr((HWND)_handles[0], GWLP_USERDATA, (LONG_PTR)this);
	_ready = true;
}

void PanelControlBase::iControlRect(const Rect & actual) {
	BOOL succeed = SetWindowPos(
		// A handle to the window.
		(HWND)NativeHandle(),
		// A handle to the window to precede the positioned window in the Z order.
		HWND_TOP,
		// The new position of the left side of the window, in client coordinates.
		actual.Left,
		// The new position of the top of the window, in client coordinates.
		actual.Top,
		// The new width of the window, in pixels.
		actual.Width,
		// The new height of the window, in pixels.
		actual.Height,
		// The window sizing and positioning flags. [SWP_]
		0
	);
	recomputeScroll();
}

void PanelControlBase::updateOffset(int dx, int dy) {
	ScrollWindow((HWND)NativeHandle(), dx, dy, NULL, NULL);
	//UpdateWindow((HWND)NativeHandle());
}

void PanelControlBase::recomputeWorkspace() {
	auto childrenProxy = *_children;
	_minX = _minY = _maxX = _maxY = 0;
	for (auto & child : *childrenProxy) {
		const Rect & rect = child->Design();
		if (rect.Left < _minX)
			_minX = rect.Left;
		if ((rect.Left + rect.Width) > _maxX)
			_maxX = rect.Left + rect.Width;
		if (rect.Top < _minY)
			_minY = rect.Top;
		if ((rect.Top + rect.Height) > _maxY)
			_maxY = rect.Top + rect.Height;
	}

	_workspace.Left = _minX;
	_workspace.Width = _maxX - _minX;
	_workspace.Top = _minY;
	_workspace.Height = _maxY - _minY;

	PANEL_LOG("Workspace X ~ [", _minX, "-", _maxX, "] ~ ", _workspace.Width);
	PANEL_LOG("Workspace Y ~ [", _minY, "-", _maxY, "] ~ ", _workspace.Height);
}

void PanelControlBase::recomputeScroll() {
	recomputeClient();
	recomputeWorkspace();

	SCROLLINFO sinfo;
	// Set the horizontal scrolling range and page size.
	sinfo.cbSize = sizeof(sinfo);
	sinfo.fMask = SIF_RANGE | SIF_PAGE;
	switch (_hbar) {
		case IControl::Visibility::Visible:
			sinfo.nMin = Workspace().Left;
			sinfo.nMax = Math::Max(Workspace().Left + Workspace().Width, Client().Width);
			break;
		case IControl::Visibility::Invisible:
			sinfo.nMin = 0;
			sinfo.nMax = 0;
			break;
		default:
			throw;
	}
	sinfo.nPage = Client().Width;
	SetScrollInfo((HWND)NativeHandle(), SB_HORZ, &sinfo, TRUE);
	PANEL_LOG("Scroll X ~ [", sinfo.nMin, "-", sinfo.nMax, "] / ", sinfo.nPage);
	// Set the vertical scrolling range and page size
	sinfo.cbSize = sizeof(sinfo);
	sinfo.fMask = SIF_RANGE | SIF_PAGE;
	switch (_vbar) {
		case IControl::Visibility::Visible:
			sinfo.nMin = Workspace().Top;
			sinfo.nMax = Math::Max(Workspace().Top + Workspace().Height, Client().Height);
			break;
		case IControl::Visibility::Invisible:
			sinfo.nMin = 0;
			sinfo.nMax = 0;
			break;
		default:
			throw;
	}
	sinfo.nPage = Client().Height;
	SetScrollInfo((HWND)NativeHandle(), SB_VERT, &sinfo, TRUE);
	recomputeClient();
	PANEL_LOG("Scroll Y ~~ [", sinfo.nMin, "-", sinfo.nMax, "] / ", sinfo.nPage);
}

void PanelControlBase::HorizontalScrollBar(Visibility visibility) {
	_hbar = visibility;
	recomputeScroll();
}

void PanelControlBase::VerticalScrollBar(Visibility visibility) {
	_vbar = visibility;
	recomputeScroll();
}

void PanelControlBase::HorizontalOffset(int offset) {
	recomputeScroll();
	// recompute offset difference
	int newOffset = Math::Saturate(offset, _minX, Math::Max(_maxX - _client.Width, 0));
	// scroll window
	updateOffset(_hOffset - newOffset, 0);
	// scroll window
	SCROLLINFO info;
	info.nPos = newOffset;
	info.cbSize = sizeof(info);
	info.fMask = SIF_POS;
	SetScrollInfo((HWND)NativeHandle(), SB_HORZ, &info, TRUE);
	// update offset
	_hOffset = newOffset;
	PANEL_LOG("Scroll X Offset", _hOffset);
}

void PanelControlBase::VerticalOffset(int offset) {
	recomputeScroll();
	// recompute offset difference
	int newOffset = Math::Saturate(offset, _minY, Math::Max(_maxY - _client.Height, 0));
	// scroll window
	updateOffset(0, _vOffset - newOffset);
	// scroll window
	SCROLLINFO info;
	info.nPos = newOffset;
	info.cbSize = sizeof(info);
	info.fMask = SIF_POS;
	SetScrollInfo((HWND)NativeHandle(), SB_VERT, &info, TRUE);
	// update offset
	_vOffset = newOffset;

	PANEL_LOG("Scroll Y Offset", _vOffset);
}

PanelControlBase::PanelControlBase(PanelControlTypes types) {
	_halign = IControl::HAlign::Stretch;
	_valign = IControl::VAlign::Stretch;
	_hbar = IControl::Visibility::Visible;
	_vbar = IControl::Visibility::Visible;
	OnSizeChanged += [this]() {
		//GCommand([this]() {
			recomputeWorkspace();
			recomputeScroll();
			recomputeRect();
			HorizontalOffset(HorizontalOffset());
			VerticalOffset(VerticalOffset());
		//});
	};

	switch (types) {
		case PanelControlTypes::Basic:

			break;
		case PanelControlTypes::VerticalStack: {
			Delegate<> recomputeChildren = [this]() mutable {
				auto childrenProxy = *_children;
				int size = 0;
				for (auto & child : *childrenProxy) {
					size += child->Margin(); // top margin
					child->VerticalAlignment(IControl::VAlign::None);
					child->Top(size);
					size += child->Height(); // height of child
					size += child->Margin(); // bottom margin
				}
			};

			OnChildAdd += [this, recomputeChildren](IControl * child) mutable {
				child->OnSizeChanged += recomputeChildren;
				OnSizeChanged += child->OnSizeChanged;
				recomputeWorkspace();
				recomputeScroll();
				recomputeChildren();
			};
			OnChildRemove += [this, recomputeChildren](IControl * child) mutable {
				child->OnSizeChanged -= recomputeChildren;
				OnSizeChanged -= child->OnSizeChanged;
				recomputeWorkspace();
				recomputeScroll();
				recomputeChildren();
			};
		}	break;
		case PanelControlTypes::HorizontalStack: {
			Delegate<> recomputeChildren = [this]() mutable {
				auto childrenProxy = *_children;
				int size = 0;
				for (auto & child : *childrenProxy) {
					size += child->Margin(); // top margin
					child->HorizontalAlignment(IControl::HAlign::None);
					child->Left(size);
					size += child->Width(); // height of child
					size += child->Margin(); // bottom margin
				}
			};
			OnChildAdd += [this, recomputeChildren](IControl * child) mutable {
				child->OnSizeChanged += recomputeChildren;
				OnSizeChanged += child->OnSizeChanged;
				recomputeWorkspace();
				recomputeScroll();
				recomputeChildren();
			};
			OnChildRemove += [this, recomputeChildren](IControl * child) mutable {
				child->OnSizeChanged -= recomputeChildren;
				OnSizeChanged -= child->OnSizeChanged;
				recomputeWorkspace();
				recomputeScroll();
				recomputeChildren();
			};
		}	break;
		default:
			break;
	}

}

#pragma endregion

/************************************************************************/
/* GridControl                                                          */
/************************************************************************/
#pragma region grid
#if CONTROLS_LOG
#define GRID_LOG(...) DD_LOG("GridLog:", __VA_ARGS__)
#else
#define GRID_LOG(...)
#endif

void GridControlBase::recomputeChild(IControl * child) {
	Rect r;
	switch (child->VerticalAlignment()) {
		case VAlign::Stretch: {
			r.Top = ((int)_rows[child->Grid().Row].Top + child->Margin() - _vOffset);
			r.Height = ((int)_rows[child->Grid().Row].ActualHeight - 2 * child->Margin());
		} break;
		case VAlign::Bottom: {}
		case VAlign::Center: {}
		case VAlign::None: {}
		case VAlign::Top: {}
		default: throw;
			break;
	}
	switch (child->HorizontalAlignment()) {
		case HAlign::Stretch: {
			r.Left = ((int)_cols[child->Grid().Col].Left + child->Margin() - _hOffset);
			r.Width = -2 * child->Margin();
			for (int j = 0; j < child->Grid().ColSpan; ++j)
				r.Width += ((int)_cols[child->Grid().Col + j].ActualWidth);
		} break;
		case HAlign::Left: {
			r.Left = ((int)_cols[child->Grid().Col].Left + child->Margin() - _hOffset);
			r.Width = child->Design().Width;
		} break;
		case HAlign::Center: {}
		case HAlign::None: {}
		case HAlign::Right: {}
		default:
			throw;
			break;
	}
	child->Design(r);
}

void GridControlBase::recomputeGrid() {
	GRID_LOG("RecomputeGrid", StringLocal<32>::From(_cols.Size(), " x ", _rows.Size()));

	recomputeRect();
	recomputeClient();

	float lastLeft = 0.f;
	for (ColDescriptor & desc : _cols) {
		switch (desc.Width.Mode) {
			case IControl::SizeModes::Pixels: {
				desc.ActualWidth = desc.Width.Value;
			} break;
			case IControl::SizeModes::Points: {
				desc.ActualWidth = Scale() * desc.Width.Value;
			} break;
			case IControl::SizeModes::Percents: {
				desc.ActualWidth = desc.Width.Value * _client.Width * 0.01f;
			} break;
			case IControl::SizeModes::Adaptive: {
				DD_TODO_STAMP("Support for colspaned children.")
					desc.ActualWidth = 0.f;
				for (const IControl * child : desc.Children) {
					if (desc.ActualWidth < (float)child->Width())
						desc.ActualWidth = (float)child->Width();
				}
			} break;
			default: throw;
		}
		desc.Left = lastLeft;
		lastLeft += desc.ActualWidth;
	}

	float lastTop = 0.f;
	for (RowDescriptor & desc : _rows) {
		switch (desc.Height.Mode) {
			case IControl::SizeModes::Pixels: {
				desc.ActualHeight = desc.Height.Value;
			} break;
			case IControl::SizeModes::Points: {
				desc.ActualHeight = Scale() * desc.Height.Value;
			} break;
			case IControl::SizeModes::Percents: {
				desc.ActualHeight = desc.Height.Value * _client.Height * 0.01f;
			} break;
			case IControl::SizeModes::Adaptive: {
				DD_TODO_STAMP("Support for rowspaned children.")
					desc.ActualHeight = 0.f;
				for (const IControl * child : desc.Children) {
					if (desc.ActualHeight < (float)child->Height())
						desc.ActualHeight = (float)child->Height();
				}
			}
			default: throw;
		}
		desc.Top = lastTop;
		lastTop += desc.ActualHeight;
	}

	auto childrenProxy = *_children;
	for (auto & child : *childrenProxy) {
		recomputeChild(child);
	}
}

void GridControlBase::iControlInit() {
	DWORD style = 0;
	if (_show)
		style |= WS_VISIBLE;

	_handles[0] = CreateWindowEx(
		0,                                          // no extended styles
		panelInit(),                                // global string containing name of window class
		L"",                                        // global string containing title bar text
		WS_CHILD | WS_HSCROLL | WS_VSCROLL | style, // window styles
		CW_USEDEFAULT,                              // default horizontal position
		CW_USEDEFAULT,                              // default vertical position
		CW_USEDEFAULT,                              // default width
		CW_USEDEFAULT,                              // default height
		(HWND)_parent->NativeHandle(),              // no parent for overlapped windows
		(HMENU)NULL,                                // use the window class menu
		GetModuleHandle(NULL),                      // global instance handle
		(PVOID)NULL                                 // pointer not needed
	);

	SetWindowLongPtr((HWND)_handles[0], GWLP_USERDATA, (LONG_PTR)this);
}

void GridControlBase::AddRow(Size size) {
	RowDescriptor & desc = _rows.Append();
	desc.Height = size;
	GCommand([this]() { recomputeGrid(); });
}

void GridControlBase::AddCol(Size size) {
	ColDescriptor & desc = _cols.Append();
	desc.Width = size;
	GCommand([this]() { recomputeGrid(); });
}

GridControlBase::GridControlBase(GridControlTypes type)
	: PanelControlBase(PanelControlTypes::Basic)
{
	OnSizeChanged = Event<>();

	OnSizeChanged += [this]() {
		GCommand([this]() {
			recomputeWorkspace();
			recomputeScroll();
			recomputeRect();
			recomputeScroll();
			HorizontalOffset(HorizontalOffset());
			VerticalOffset(VerticalOffset());
			recomputeGrid();
		});
	};

	switch (type) {
		case GridControlTypes::Basic: {
			throw;
		} break;
		case GridControlTypes::Generic: {
			OnChildAdd += [this](IControl * child) {
				child->AutoRectRecalculation(false);

				int col = child->Grid().Col;
				int row = child->Grid().Row;
				while (_cols.Size() <= col)
					AddCol();
				while (_rows.Size() <= row)
					AddRow();

				recomputeChild(child);

				_rows[row].Children.Add(child);
				_cols[col].Children.Add(child);

				child->OnGridChanged += [this, child](GridPosition oldGrid, GridPosition newGrid) {
					int col = newGrid.Col;
					int row = newGrid.Row;
					while (_cols.Size() <= col)
						AddCol();
					while (_rows.Size() <= row)
						AddRow();

					if (oldGrid.Col != newGrid.Col) {
						_cols[oldGrid.Col].Children.TryRemove(child);
						_cols[newGrid.Col].Children.Add(child);
					}
					if (oldGrid.Row != newGrid.Row) {
						_rows[oldGrid.Row].Children.TryRemove(child);
						_rows[newGrid.Row].Children.Add(child);
					}
					if (_cols[newGrid.Col].Width.Mode == SizeModes::Adaptive || _rows[newGrid.Row].Height.Mode == SizeModes::Adaptive) {
						GCommand([this]() { recomputeGrid(); });
					}

					recomputeChild(child);
					recomputeScroll();
				};
 				child->OnSizeChanged += [this, child]() {
 					if (_cols[child->Grid().Col].Width.Mode == SizeModes::Adaptive || _rows[child->Grid().Row].Height.Mode == SizeModes::Adaptive) {
						GCommand([this]() { recomputeGrid(); });
 					}
 				};
			};
		} break;
		default:
			throw;
			break;
	}
}

#pragma endregion

/************************************************************************/
/* ButtonControl                                                        */
/************************************************************************/
#pragma region button
#if CONTROLS_LOG
#define BUTTON_LOG(...) DD_LOG("Button:", __VA_ARGS__)
#else
#define BUTTON_LOG(...)
#endif

DD_REGISTER_SIMPLE_WINAPI_CLASS(button) {
	ButtonControlBase * ddButton = (ButtonControlBase *)GetWindowLongPtr(hWnd, GWLP_USERDATA);
	if (ddButton) switch (message) {
		case WM_COMMAND: {
			WORD wmEvent = HIWORD(wParam);
			if (wmEvent == BS_PUSHBUTTON) {
				ddButton->OnClick();
			}
		}		break;
		case WM_SIZE:
			ddButton->OnSizeChanged();
			break;
	}

	return DefWindowProc(hWnd, message, wParam, lParam);
}

void ButtonControlBase::iControlInit() {
	DWORD style = 0;
	if (_show)
		style |= WS_VISIBLE;

	_handles[0] = CreateWindowW(
		// A null - terminated string or a class atom created by a previous call to the RegisterClass or RegisterClassEx function.
		buttonInit(),
		// The window name. If the window style specifies a title bar, the window title pointed to by lpWindowName is displayed in the title bar.
		_text,
		// The style of the window being created.
		WS_CHILD | style,
		// The initial horizontal position of the window.
		0,
		// The initial vertical position of the window.
		0,
		// The width, in device units, of the window. For overlapped windows, nWidth is either the window's width, in screen coordinates, or CW_USEDEFAULT.
		100,
		// The height, in device units, of the window.For overlapped windows, nHeight is the window's height, in screen coordinates. If nWidth is set to CW_USEDEFAULT, the system ignores nHeight.
		100,
		// A handle to the parent or owner window of the window being created.
		(HWND)_parent->NativeHandle(),
		// A handle to a menu, or specifies a child-window identifier depending on the window style.
		nullptr,
		// A handle to the instance of the module to be associated with the window.
		reinterpret_cast<HINSTANCE>(GetModuleHandleW(NULL)),
		// A pointer to a value to be passed to the window through the CREATESTRUCT structure (lpCreateParams member) pointed to by the lParam param of the WM_CREATE message.
		this
	);

	SetWindowLongPtr((HWND)NativeHandle(), GWLP_USERDATA, (LONG_PTR)this);

	DWORD align = 0;

	switch (_talign) {
		case IControl::TAlign::Left:   align |= BS_LEFT;   break;
		case IControl::TAlign::Center: align |= BS_CENTER; break;
		case IControl::TAlign::Right:  align |= BS_RIGHT;  break;
		default: throw;
	}

	_handles[1] = CreateWindowW(
		// A null - terminated string or a class atom created by a previous call to the RegisterClass or RegisterClassEx function.
		L"BUTTON",
		// The window name. If the window style specifies a title bar, the window title pointed to by lpWindowName is displayed in the title bar.
		_text,
		// The style of the window being created.
		WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON | align,
		// The initial horizontal position of the window.
		0,
		// The initial vertical position of the window.
		0,
		// The width, in device units, of the window. For overlapped windows, nWidth is either the window's width, in screen coordinates, or CW_USEDEFAULT.
		100,
		// The height, in device units, of the window.For overlapped windows, nHeight is the window's height, in screen coordinates. If nWidth is set to CW_USEDEFAULT, the system ignores nHeight.
		100,
		// A handle to the parent or owner window of the window being created.
		(HWND)_handles[0],
		// A handle to a menu, or specifies a child-window identifier depending on the window style.
		nullptr,
		// A handle to the instance of the module to be associated with the window.
		reinterpret_cast<HINSTANCE>(GetModuleHandleW(NULL)),
		// A pointer to a value to be passed to the window through the CREATESTRUCT structure (lpCreateParams member) pointed to by the lParam param of the WM_CREATE message.
		this
	);

	SetWindowLongPtr((HWND)NativeHandle(), GWLP_USERDATA, (LONG_PTR)this);
}

void ButtonControlBase::iControlRect(const Rect & actual) {
	BOOL succeed0 = SetWindowPos(
		// A handle to the window.
		(HWND)_handles[0],
		// A handle to the window to precede the positioned window in the Z order.
		HWND_TOP,
		// The new position of the left side of the window, in client coordinates.
		actual.Left,
		// The new position of the top of the window, in client coordinates.
		actual.Top,
		// The new width of the window, in pixels.
		actual.Width,
		// The new height of the window, in pixels.
		actual.Height,
		// The window sizing and positioning flags. [SWP_]
		0
	);
	BOOL succeed1 = SetWindowPos(
		// A handle to the window.
		(HWND)_handles[1],
		// A handle to the window to precede the positioned window in the Z order.
		HWND_TOP,
		// The new position of the left side of the window, in client coordinates.
		_padding.Left,
		// The new position of the top of the window, in client coordinates.
		_padding.Top,
		// The new width of the window, in pixels.
		actual.Width - (_padding.Left + _padding.Right),
		// The new height of the window, in pixels.
		actual.Height - (_padding.Top + _padding.Bottom),
		// The window sizing and positioning flags. [SWP_]
		0
	);
}

void ButtonControlBase::TextAlignment(TAlign align) {
	if (_talign != align) {
		_talign = align;

		if (Ready()) {
			static constexpr DWORD remove = ~(BS_LEFT | BS_RIGHT | BS_CENTER);
			DWORD style = GetWindowStyle((HWND)_handles[1]);
			style &= remove;

			switch (align) {
				case IControl::TAlign::Left:
					style |= BS_LEFT;
					break;
				case IControl::TAlign::Center:
					style |= BS_CENTER;
					break;
				case IControl::TAlign::Right:
					style |= BS_RIGHT;
					break;
				default:
					throw;
			}

			SetWindowLong((HWND)_handles[1], GWL_STYLE, style);
		}
	}
}

ButtonControlBase::ButtonControlBase(const Text::StringView16 & str)
	: _text(str)
	, IControl()
{
	OnSizeChanged += [this]() {
		BUTTON_LOG("OnSizeChanged b", _actual.Left, _actual.Top, _actual.Width, _actual.Height);
		recomputeRect();
		BUTTON_LOG("OnSizeChanged e", _actual.Left, _actual.Top, _actual.Width, _actual.Height);
	};
}
#pragma endregion

/************************************************************************/
/* CheckboxControl                                                      */
/************************************************************************/
#pragma region checkbox
struct CheckboxControlBaseHelper : public CheckboxControlBase {
	void Download() { downloadState(); }
};

DD_REGISTER_SIMPLE_WINAPI_CLASS(checkbox) {
	CheckboxControlBaseHelper * ddCheck = (CheckboxControlBaseHelper *)GetWindowLongPtr(hWnd, GWLP_USERDATA);

	if (ddCheck) switch (message) {
		case WM_COMMAND: {
			WORD wmEvent = HIWORD(wParam);
			if (wmEvent == BS_PUSHBUTTON) {
				ddCheck->Download();
			}
		} return 0;
		case WM_SIZE:
			ddCheck->OnSizeChanged();
			break;
	}
	return DefWindowProc(hWnd, message, wParam, lParam);
}

void CheckboxControlBase::iControlInit() {
	DWORD style = 0;
	if (_show)
		style |= WS_VISIBLE;

	_handles[0] = CreateWindowW(
		// A null - terminated string or a class atom created by a previous call to the RegisterClass or RegisterClassEx function.
		checkboxInit(),
		// The window name. If the window style specifies a title bar, the window title pointed to by lpWindowName is displayed in the title bar.
		_text,
		// The style of the window being created.
		style | WS_CHILD,
		// The initial horizontal position of the window.
		0,
		// The initial vertical position of the window.
		0,
		// The width, in device units, of the window. For overlapped windows, nWidth is either the window's width, in screen coordinates, or CW_USEDEFAULT.
		100,
		// The height, in device units, of the window.For overlapped windows, nHeight is the window's height, in screen coordinates. If nWidth is set to CW_USEDEFAULT, the system ignores nHeight.
		100,
		// A handle to the parent or owner window of the window being created.
		(HWND)_parent->NativeHandle(),
		// A handle to a menu, or specifies a child-window identifier depending on the window style.
		nullptr,
		// A handle to the instance of the module to be associated with the window.
		reinterpret_cast<HINSTANCE>(GetModuleHandleW(NULL)),
		// A pointer to a value to be passed to the window through the CREATESTRUCT structure (lpCreateParams member) pointed to by the lParam param of the WM_CREATE message.
		this
	);

	SetWindowLongPtr((HWND)NativeHandle(), GWLP_USERDATA, (LONG_PTR)this);

	DWORD align = 0;

	switch (_talign) {
		case IControl::TAlign::Left: break;
		case IControl::TAlign::Right: align |= BS_RIGHTBUTTON; break;
		default:
			DD_WARNING("BoxAlignment could be just left or right.");
			throw;
	}

	_handles[1] = CreateWindowW(
		// A null - terminated string or a class atom created by a previous call to the RegisterClass or RegisterClassEx function.
		L"BUTTON",
		// The window name. If the window style specifies a title bar, the window title pointed to by lpWindowName is displayed in the title bar.
		_text,
		// The style of the window being created.
		WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON | BS_AUTOCHECKBOX | align,
		// The initial horizontal position of the window.
		0,
		// The initial vertical position of the window.
		0,
		// The width, in device units, of the window. For overlapped windows, nWidth is either the window's width, in screen coordinates, or CW_USEDEFAULT.
		100,
		// The height, in device units, of the window.For overlapped windows, nHeight is the window's height, in screen coordinates. If nWidth is set to CW_USEDEFAULT, the system ignores nHeight.
		100,
		// A handle to the parent or owner window of the window being created.
		(HWND)_handles[0],
		// A handle to a menu, or specifies a child-window identifier depending on the window style.
		nullptr,
		// A handle to the instance of the module to be associated with the window.
		reinterpret_cast<HINSTANCE>(GetModuleHandleW(NULL)),
		// A pointer to a value to be passed to the window through the CREATESTRUCT structure (lpCreateParams member) pointed to by the lParam param of the WM_CREATE message.
		this
	);

	SetWindowLongPtr((HWND)NativeHandle(), GWLP_USERDATA, (LONG_PTR)this);

	if (_state) {
		Button_SetCheck((HWND)_handles[1], _state);
	}
}

void CheckboxControlBase::iControlRect(const Rect & actual) {
	BOOL succeed0 = SetWindowPos(
		// A handle to the window.
		(HWND)_handles[0],
		// A handle to the window to precede the positioned window in the Z order.
		HWND_TOP,
		// The new position of the left side of the window, in client coordinates.
		actual.Left,
		// The new position of the top of the window, in client coordinates.
		actual.Top,
		// The new width of the window, in pixels.
		actual.Width,
		// The new height of the window, in pixels.
		actual.Height,
		// The window sizing and positioning flags. [SWP_]
		0
	);
	BOOL succeed1 = SetWindowPos(
		// A handle to the window.
		(HWND)_handles[1],
		// A handle to the window to precede the positioned window in the Z order.
		HWND_TOP,
		// The new position of the left side of the window, in client coordinates.
		_padding.Left,
		// The new position of the top of the window, in client coordinates.
		_padding.Top,
		// The new width of the window, in pixels.
		actual.Width - (_padding.Left + _padding.Right),
		// The new height of the window, in pixels.
		actual.Height - (_padding.Top + _padding.Bottom),
		// The window sizing and positioning flags. [SWP_]
		0
	);
}

void CheckboxControlBase::downloadState() {
	bool isChecked = (bool)Button_GetCheck((HWND)_handles[1]);
	if (_state != isChecked) {
		_state = isChecked;
		OnStateDownload(_state);
		OnStateChanged(_state);
	}
}

void CheckboxControlBase::BoxAlignment(TAlign align) {
	if (_talign != align) {
		_talign = align;

		if (Ready()) {
			static constexpr DWORD remove = ~(BS_RIGHTBUTTON);
			DWORD style = GetWindowStyle((HWND)_handles[1]);
			style &= remove;

			switch (align) {
				case IControl::TAlign::Left:  break;
				case IControl::TAlign::Right: style |= BS_RIGHTBUTTON; break;
				default:
					DD_WARNING("BoxAlignment could be just left or right");
					throw;
			}

			SetWindowLong((HWND)_handles[1], GWL_STYLE, style);
		}
	}
}

void CheckboxControlBase::Checked(bool checked, bool silent) {
	if (_state != checked) {
		_state = checked;
		if (Ready()) {
			Button_SetCheck((HWND)_handles[1], _state);
		}
		if (!silent){
			OnStateUpload(_state);
			OnStateChanged(_state);
		}
	}
}

CheckboxControlBase::CheckboxControlBase(const String & str, bool value)
	: _text(str)
	, _state(value)
	, IControl()
{
	OnSizeChanged += [this]() { recomputeRect(); };
}


#pragma endregion

/************************************************************************/
/* LabelControl                                                         */
/************************************************************************/
#pragma region Label
DD_REGISTER_SIMPLE_WINAPI_CLASS(label) {
	LabelControlBase * ddLabel = (LabelControlBase *)GetWindowLongPtr(hWnd, GWLP_USERDATA);
	if (ddLabel) switch (message) {
		case WM_CTLCOLORSTATIC: {
			if (ddLabel->Background().a == 255) {
				HDC hdcStatic = (HDC)wParam;
				COLORREF color = RGB(ddLabel->Background().r, ddLabel->Background().g, ddLabel->Background().b);
				SetBkColor(hdcStatic, color);
				HBRUSH & brush = Libraries::Brushes[ddLabel->Background()];
				if (brush == NULL) {
					brush = CreateSolidBrush(color);
				}
				return (INT_PTR)brush;
			}
		} break;
		case WM_ERASEBKGND: {
			Color bg = ddLabel->Background();
			if (bg.a == 255) {
				RECT rect;
				HBRUSH & brush = Libraries::Brushes[bg];
				if (brush == NULL) {
					brush = CreateSolidBrush(RGB(bg.r, bg.g, bg.b));
				}
				SelectObject((HDC)wParam, brush);
				GetClientRect((HWND)ddLabel->NativeHandle(), &rect);
				Rectangle((HDC)wParam, rect.left - 1, rect.top - 1, rect.right + 1, rect.bottom + 1);
			}
			else {
				break;
			}
		} return 0;
		case WM_SIZE:
			ddLabel->OnSizeChanged();
			break;
	}
	return DefWindowProc(hWnd, message, wParam, lParam);
}

void LabelControlBase::uploadText() {
	SetWindowTextW((HWND)_handles[1], _text.begin());
}

void LabelControlBase::iControlRect(const Rect & actual) {
	BOOL succeed0 = SetWindowPos(
		// A handle to the window.
		(HWND)_handles[0],
		// A handle to the window to precede the positioned window in the Z order.
		HWND_TOP,
		// The new position of the left side of the window, in client coordinates.
		actual.Left,
		// The new position of the top of the window, in client coordinates.
		actual.Top,
		// The new width of the window, in pixels.
		actual.Width,
		// The new height of the window, in pixels.
		actual.Height,
		// The window sizing and positioning flags. [SWP_]
		0
	);
	BOOL succeed1 = SetWindowPos(
		// A handle to the window.
		(HWND)_handles[1],
		// A handle to the window to precede the positioned window in the Z order.
		HWND_TOP,
		// The new position of the left side of the window, in client coordinates.
		_padding.Left,
		// The new position of the top of the window, in client coordinates.
		_padding.Top,
		// The new width of the window, in pixels.
		actual.Width - (_padding.Left + _padding.Right),
		// The new height of the window, in pixels.
		actual.Height - (_padding.Top + _padding.Bottom),
		// The window sizing and positioning flags. [SWP_]
		0
	);
}

void LabelControlBase::iControlInit() {
	DWORD style = 0;
	if (_show)
		style |= WS_VISIBLE;

	_handles[0] = CreateWindowW(
		// A null - terminated string or a class atom created by a previous call to the RegisterClass or RegisterClassEx function.
		labelInit(),
		// The window name. If the window style specifies a title bar, the window title pointed to by lpWindowName is displayed in the title bar.
		_text,
		// The style of the window being created.
		style | WS_CHILD,
		// The initial horizontal position of the window.
		0,
		// The initial vertical position of the window.
		0,
		// The width, in device units, of the window. For overlapped windows, nWidth is either the window's width, in screen coordinates, or CW_USEDEFAULT.
		100,
		// The height, in device units, of the window.For overlapped windows, nHeight is the window's height, in screen coordinates. If nWidth is set to CW_USEDEFAULT, the system ignores nHeight.
		100,
		// A handle to the parent or owner window of the window being created.
		(HWND)_parent->NativeHandle(),
		// A handle to a menu, or specifies a child-window identifier depending on the window style.
		nullptr,
		// A handle to the instance of the module to be associated with the window.
		reinterpret_cast<HINSTANCE>(GetModuleHandleW(NULL)),
		// A pointer to a value to be passed to the window through the CREATESTRUCT structure (lpCreateParams member) pointed to by the lParam param of the WM_CREATE message.
		this
	);
	SetWindowLongPtr((HWND)_handles[0], GWLP_USERDATA, (LONG_PTR)this);

	DWORD alignment = 0;
	switch (_talign) {
		case IControl::TAlign::Left:   alignment |= SS_LEFT;   break;
		case IControl::TAlign::Center: alignment |= SS_CENTER; break;
		case IControl::TAlign::Right:  alignment |= SS_RIGHT;  break;
		default: throw;
	}

	_handles[1] = CreateWindowW(
		// A null - terminated string or a class atom created by a previous call to the RegisterClass or RegisterClassEx function.
		WC_STATIC,
		// The window name. If the window style specifies a title bar, the window title pointed to by lpWindowName is displayed in the title bar.
		_text,
		// The style of the window being created.
		WS_VISIBLE | WS_CHILD | alignment | SS_LEFTNOWORDWRAP,
		// The initial horizontal position of the window.
		0,
		// The initial vertical position of the window.
		0,
		// The width, in device units, of the window. For overlapped windows, nWidth is either the window's width, in screen coordinates, or CW_USEDEFAULT.
		100,
		// The height, in device units, of the window.For overlapped windows, nHeight is the window's height, in screen coordinates. If nWidth is set to CW_USEDEFAULT, the system ignores nHeight.
		100,
		// A handle to the parent or owner window of the window being created.
		(HWND)_handles[0],
		// A handle to a menu, or specifies a child-window identifier depending on the window style.
		nullptr,
		// A handle to the instance of the module to be associated with the window.
		reinterpret_cast<HINSTANCE>(GetModuleHandleW(NULL)),
		// A pointer to a value to be passed to the window through the CREATESTRUCT structure (lpCreateParams member) pointed to by the lParam param of the WM_CREATE message.
		this
	);

	SetWindowLongPtr((HWND)_handles[1], GWLP_USERDATA, (LONG_PTR)this);
}

void LabelControlBase::TextAlignment(TAlign align) {
	constexpr DWORD remove = ~(SS_LEFT | SS_CENTER | SS_RIGHT);
	if (align != _talign) {
		_talign = align;

		if (Ready()) {
			// download style
			DWORD style = GetWindowStyle((HWND)_handles[1]);
			// remove alignment from style
			style &= remove;
			// add current style
			switch (align) {
				case IControl::TAlign::Left:   style |= SS_LEFT;   break;
				case IControl::TAlign::Center: style |= SS_CENTER; break;
				case IControl::TAlign::Right:  style |= SS_RIGHT;  break;
				default: throw;
			}
			// update style
			SetWindowLong((HWND)_handles[1], GWL_STYLE, style);
		}
	}
}

LabelControlBase::LabelControlBase(const Text::StringView16 & text)
	: _text(text)
{
	OnSizeChanged += [this, text]() {
		recomputeRect();
	};
}

LabelControlBase::~LabelControlBase() {
	// DD_LOG("LabelControlBase::~LabelControlBase");
}
#pragma endregion

/************************************************************************/
/* SliderControl                                                        */
/************************************************************************/
#pragma region slider

DD_REGISTER_SIMPLE_WINAPI_CLASS(slider) {
	SliderControlBase * ddSlider = (SliderControlBase *)GetWindowLongPtr(hWnd, GWLP_USERDATA);
	if (ddSlider) switch (message) {
		case WM_HSCROLL:
			ddSlider->downloadValue();
			return 0;
		case WM_SIZE:
			ddSlider->OnSizeChanged();
			break;
	}

	return DefWindowProc(hWnd, message, wParam, lParam);
}

void SliderControlBase::iControlInit() {
	DWORD style = 0;
	if (_show)
		style |= WS_VISIBLE;

	_handles[0] = CreateWindowW(
		// A null - terminated string or a class atom created by a previous call to the RegisterClass or RegisterClassEx function.
		sliderInit(),
		// The window name. If the window style specifies a title bar, the window title pointed to by lpWindowName is displayed in the title bar.
		NULL,
		// The style of the window being created.
		style | WS_CHILD,
		// The initial horizontal position of the window.
		0,
		// The initial vertical position of the window.
		0,
		// The width, in device units, of the window. For overlapped windows, nWidth is either the window's width, in screen coordinates, or CW_USEDEFAULT.
		100,
		// The height, in device units, of the window.For overlapped windows, nHeight is the window's height, in screen coordinates. If nWidth is set to CW_USEDEFAULT, the system ignores nHeight.
		100,
		// A handle to the parent or owner window of the window being created.
		(HWND)_parent->NativeHandle(),
		// A handle to a menu, or specifies a child-window identifier depending on the window style.
		NULL,
		// A handle to the instance of the module to be associated with the window.
		reinterpret_cast<HINSTANCE>(GetModuleHandleW(NULL)),
		// A pointer to a value to be passed to the window through the CREATESTRUCT structure (lpCreateParams member) pointed to by the lParam param of the WM_CREATE message.
		this
	);

	SetWindowLongPtr((HWND)NativeHandle(), GWLP_USERDATA, (LONG_PTR)this);

	_handles[1] = CreateWindowW(
		// A null - terminated string or a class atom created by a previous call to the RegisterClass or RegisterClassEx function.
		TRACKBAR_CLASS,
		// The window name. If the window style specifies a title bar, the window title pointed to by lpWindowName is displayed in the title bar.
		NULL,
		// The style of the window being created.
		WS_VISIBLE | WS_CHILD | TBS_NOTICKS,
		// The initial horizontal position of the window.
		0,
		// The initial vertical position of the window.
		0,
		// The width, in device units, of the window. For overlapped windows, nWidth is either the window's width, in screen coordinates, or CW_USEDEFAULT.
		100,
		// The height, in device units, of the window.For overlapped windows, nHeight is the window's height, in screen coordinates. If nWidth is set to CW_USEDEFAULT, the system ignores nHeight.
		100,
		// A handle to the parent or owner window of the window being created.
		(HWND)_handles[0],
		// A handle to a menu, or specifies a child-window identifier depending on the window style.
		NULL,
		// A handle to the instance of the module to be associated with the window.
		reinterpret_cast<HINSTANCE>(GetModuleHandleW(NULL)),
		// A pointer to a value to be passed to the window through the CREATESTRUCT structure (lpCreateParams member) pointed to by the lParam param of the WM_CREATE message.
		this
	);

	uploadMin();
	uploadMax();
	uploadValue();

	SetWindowLongPtr((HWND)NativeHandle(), GWLP_USERDATA, (LONG_PTR)this);
}

void SliderControlBase::iControlRect(const Rect & actual) {
	BOOL succeed0 = SetWindowPos(
		// A handle to the window.
		(HWND)_handles[0],
		// A handle to the window to precede the positioned window in the Z order.
		HWND_TOP,
		// The new position of the left side of the window, in client coordinates.
		actual.Left,
		// The new position of the top of the window, in client coordinates.
		actual.Top,
		// The new width of the window, in pixels.
		actual.Width,
		// The new height of the window, in pixels.
		actual.Height,
		// The window sizing and positioning flags. [SWP_]
		0
	);
	BOOL succeed1 = SetWindowPos(
		// A handle to the window.
		(HWND)_handles[1],
		// A handle to the window to precede the positioned window in the Z order.
		HWND_TOP,
		// The new position of the left side of the window, in client coordinates.
		0,
		// The new position of the top of the window, in client coordinates.
		0,
		// The new width of the window, in pixels.
		actual.Width,
		// The new height of the window, in pixels.
		actual.Height,
		// The window sizing and positioning flags. [SWP_]
		0
	);
}

void SliderControlBase::uploadValue() {
	PostMessage((HWND)_handles[1], TBM_SETPOS, TRUE, _value);
}

void SliderControlBase::uploadMin() {
	PostMessage((HWND)_handles[1], TBM_SETRANGEMIN, TRUE, (LPARAM)_min);
}

void SliderControlBase::uploadMax() {
	PostMessage((HWND)_handles[1], TBM_SETRANGEMAX, TRUE, (LPARAM)_max);
}

void SliderControlBase::Min(i32 newValue) {
	if (_min != newValue) {
		_min = newValue;
		uploadMin();
	}
}

void SliderControlBase::Max(i32 newValue) {
	if (_max != newValue) {
		_max = newValue;
		uploadMax();
	}
}

void SliderControlBase::Value(i32 newValue, bool silent) {
	if (_value != newValue) {
		i32 oldValue = _value;
		_value = newValue;
		uploadValue();
		if (!silent) {
			OnValueUpload(oldValue, newValue);
			OnValueChanged(oldValue, newValue);
		}
	}
}

void SliderControlBase::downloadValue() {
	i32 oldValue = _value;
	i32 newValue = (i32)SendMessage((HWND)_handles[1], TBM_GETPOS, 0, 0);
	if (_value != newValue) {
		_value = newValue;
		OnValueDownload(oldValue, newValue);
		OnValueChanged(oldValue, newValue);
	}
}
#pragma endregion

/************************************************************************/
/* TextContol                                                           */
/************************************************************************/
#pragma region text
struct TextControlBaseHelper : public TextControlBase {
	void DownloadText() { downloadText(); }
	bool Silent() { return _silent; }
	void Unsilent() { _silent = false; }
};


DD_REGISTER_SIMPLE_WINAPI_CLASS(text) {
	TextControlBaseHelper * ddControl = (TextControlBaseHelper *)GetWindowLongPtr(hWnd, GWLP_USERDATA);
	if (ddControl) switch (message) {
		case WM_COMMAND:
			if (HIWORD(wParam) == EN_UPDATE)
				if (ddControl->Silent())
					ddControl->Unsilent();
				else
					ddControl->DownloadText();
			return 0;
		case WM_CTLCOLOREDIT: {
			if (ddControl->Background().a == 255) {
				HDC hdcStatic = (HDC)wParam;
				COLORREF color = RGB(ddControl->Background().r, ddControl->Background().g, ddControl->Background().b);
				SetBkColor(hdcStatic, color);
				HBRUSH & brush = Libraries::Brushes[ddControl->Background()];
				if (brush == NULL) {
					brush = CreateSolidBrush(color);
				}
				return (INT_PTR)brush;
			}
		} break;
		case WM_SIZE:
			ddControl->OnSizeChanged();
			break;
	}

	return DefWindowProc(hWnd, message, wParam, lParam);
}

void TextControlBase::iControlInit() {
	DWORD style = 0;
	if (_show)
		style |= WS_VISIBLE;

	_handles[0] = CreateWindowW(
		// A null - terminated string or a class atom created by a previous call to the RegisterClass or RegisterClassEx function.
		textInit(),
		// The window name. If the window style specifies a title bar, the window title pointed to by lpWindowName is displayed in the title bar.
		NULL,
		// The style of the window being created.
		style | WS_CHILD,
		// The initial horizontal position of the window.
		0,
		// The initial vertical position of the window.
		0,
		// The width, in device units, of the window. For overlapped windows, nWidth is either the window's width, in screen coordinates, or CW_USEDEFAULT.
		100,
		// The height, in device units, of the window.For overlapped windows, nHeight is the window's height, in screen coordinates. If nWidth is set to CW_USEDEFAULT, the system ignores nHeight.
		100,
		// A handle to the parent or owner window of the window being created.
		(HWND)_parent->NativeHandle(),
		// A handle to a menu, or specifies a child-window identifier depending on the window style.
		NULL,
		// A handle to the instance of the module to be associated with the window.
		reinterpret_cast<HINSTANCE>(GetModuleHandleW(NULL)),
		// A pointer to a value to be passed to the window through the CREATESTRUCT structure (lpCreateParams member) pointed to by the lParam param of the WM_CREATE message.
		this
	);

	SetWindowLongPtr((HWND)NativeHandle(), GWLP_USERDATA, (LONG_PTR)this);

	DWORD alignment = 0;
	switch (_talign) {
		case IControl::TAlign::Left: alignment |= ES_LEFT; break;
		case IControl::TAlign::Center: alignment |= ES_CENTER; break;
		case IControl::TAlign::Right: alignment |= ES_RIGHT; break;
		default: throw;
	}

	_handles[1] = CreateWindowW(
		// A null - terminated string or a class atom created by a previous call to the RegisterClass or RegisterClassEx function.
		WC_EDIT,
		// The window name. If the window style specifies a title bar, the window title pointed to by lpWindowName is displayed in the title bar.
		NULL,
		// The style of the window being created.
		WS_VISIBLE | WS_CHILD | alignment | ES_AUTOHSCROLL,
		// The initial horizontal position of the window.
		0,
		// The initial vertical position of the window.
		0,
		// The width, in device units, of the window. For overlapped windows, nWidth is either the window's width, in screen coordinates, or CW_USEDEFAULT.
		100,
		// The height, in device units, of the window.For overlapped windows, nHeight is the window's height, in screen coordinates. If nWidth is set to CW_USEDEFAULT, the system ignores nHeight.
		100,
		// A handle to the parent or owner window of the window being created.
		(HWND)_handles[0],
		// A handle to a menu, or specifies a child-window identifier depending on the window style.
		NULL,
		// A handle to the instance of the module to be associated with the window.
		reinterpret_cast<HINSTANCE>(GetModuleHandleW(NULL)),
		// A pointer to a value to be passed to the window through the CREATESTRUCT structure (lpCreateParams member) pointed to by the lParam param of the WM_CREATE message.
		this
	);

	SetWindowLongPtr((HWND)NativeHandle(), GWLP_USERDATA, (LONG_PTR)this);
}

void TextControlBase::iControlRect(const Rect & actual) {
	BOOL succeed0 = SetWindowPos(
		// A handle to the window.
		(HWND)_handles[0],
		// A handle to the window to precede the positioned window in the Z order.
		HWND_TOP,
		// The new position of the left side of the window, in client coordinates.
		actual.Left,
		// The new position of the top of the window, in client coordinates.
		actual.Top,
		// The new width of the window, in pixels.
		actual.Width,
		// The new height of the window, in pixels.
		actual.Height,
		// The window sizing and positioning flags. [SWP_]
		0
	);
	BOOL succeed1 = SetWindowPos(
		// A handle to the window.
		(HWND)_handles[1],
		// A handle to the window to precede the positioned window in the Z order.
		HWND_TOP,
		// The new position of the left side of the window, in client coordinates.
		_padding.Left,
		// The new position of the top of the window, in client coordinates.
		_padding.Top,
		// The new width of the window, in pixels.
		actual.Width - (_padding.Left + _padding.Right),
		// The new height of the window, in pixels.
		actual.Height - (_padding.Top + _padding.Bottom),
		// The window sizing and positioning flags. [SWP_]
		0
	);
}

void TextControlBase::downloadText() {
	_text.LoadFromBuffer(GetWindowTextW((HWND)_handles[1], _text.begin(), 256));
	OnTextDownload();
	OnTextChanged();
}

void TextControlBase::uploadText() {
	SetWindowTextW((HWND)_handles[1], _text.begin());
}

void TextControlBase::Text(const DD::Text::StringView16 & text, bool silent) {
	if (_text != text) {
		_text = text;
		if (Ready()) {
			_silent = silent;
			uploadText();
		}
		if (!silent) {
			OnTextUpload();
			OnTextChanged();
		}
	}
}

void TextControlBase::TextAlignment(TAlign align) {
	constexpr DWORD remove = ~(ES_LEFT | ES_CENTER | ES_RIGHT);
	if (align != _talign) {
		_talign = align;

		if (Ready()) {
			// download style
			DWORD style = GetWindowStyle((HWND)_handles[1]);
			// remove alignment from style
			style &= remove;
			// add current style
			switch (align) {
				case IControl::TAlign::Left:   style |= ES_LEFT;   break;
				case IControl::TAlign::Center: style |= ES_CENTER; break;
				case IControl::TAlign::Right:  style |= ES_RIGHT;  break;
				default: throw;
			}
			// update style
			SetWindowLong((HWND)_handles[1], GWL_STYLE, style);
		}
	}
}

#pragma endregion

/************************************************************************/
/* OptionsControl                                                       */
/************************************************************************/
#pragma region options
/** Helper for uncovering protected methods.  */
struct OptionControlBaseHelper : public OptionControlBase {
	void DownloadIndex() { downloadIndex(); }
};

DD_REGISTER_SIMPLE_WINAPI_CLASS(options) {
	OptionControlBaseHelper * ddControl = (OptionControlBaseHelper *)GetWindowLongPtr(hWnd, GWLP_USERDATA);
	if (ddControl) switch (message) {
		case WM_COMMAND:
			if (HIWORD(wParam) == CBN_SELCHANGE) {
				ddControl->DownloadIndex();
			}
			return 0;
		case WM_SIZE:
			ddControl->OnSizeChanged();
			break;
	}

	return DefWindowProc(hWnd, message, wParam, lParam);
}

void OptionControlBase::iControlInit() {
	DWORD style = 0;
	if (_show)
		style |= WS_VISIBLE;

	_handles[0] = CreateWindowW(
		// A null - terminated string or a class atom created by a previous call to the RegisterClass or RegisterClassEx function.
		optionsInit(),
		// The window name. If the window style specifies a title bar, the window title pointed to by lpWindowName is displayed in the title bar.
		NULL,
		// The style of the window being created.
		style | WS_CHILD | TBS_NOTICKS,
		// The initial horizontal position of the window.
		0,
		// The initial vertical position of the window.
		0,
		// The width, in device units, of the window. For overlapped windows, nWidth is either the window's width, in screen coordinates, or CW_USEDEFAULT.
		100,
		// The height, in device units, of the window.For overlapped windows, nHeight is the window's height, in screen coordinates. If nWidth is set to CW_USEDEFAULT, the system ignores nHeight.
		100,
		// A handle to the parent or owner window of the window being created.
		(HWND)_parent->NativeHandle(),
		// A handle to a menu, or specifies a child-window identifier depending on the window style.
		NULL,
		// A handle to the instance of the module to be associated with the window.
		reinterpret_cast<HINSTANCE>(GetModuleHandleW(NULL)),
		// A pointer to a value to be passed to the window through the CREATESTRUCT structure (lpCreateParams member) pointed to by the lParam param of the WM_CREATE message.
		this
	);

	SetWindowLongPtr((HWND)NativeHandle(), GWLP_USERDATA, (LONG_PTR)this);

	_handles[1] = CreateWindowW(
		// A null - terminated string or a class atom created by a previous call to the RegisterClass or RegisterClassEx function.
		WC_COMBOBOX,
		// The window name. If the window style specifies a title bar, the window title pointed to by lpWindowName is displayed in the title bar.
		NULL,
		// The style of the window being created.
		WS_VISIBLE | WS_CHILD | CBS_DROPDOWNLIST | CBS_DROPDOWN | CBS_HASSTRINGS | WS_VSCROLL,
		// The initial horizontal position of the window.
		0,
		// The initial vertical position of the window.
		0,
		// The width, in device units, of the window. For overlapped windows, nWidth is either the window's width, in screen coordinates, or CW_USEDEFAULT.
		100,
		// The height, in device units, of the window.For overlapped windows, nHeight is the window's height, in screen coordinates. If nWidth is set to CW_USEDEFAULT, the system ignores nHeight.
		100,
		// A handle to the parent or owner window of the window being created.
		(HWND)_handles[0],
		// A handle to a menu, or specifies a child-window identifier depending on the window style.
		NULL,
		// A handle to the instance of the module to be associated with the window.
		reinterpret_cast<HINSTANCE>(GetModuleHandleW(NULL)),
		// A pointer to a value to be passed to the window through the CREATESTRUCT structure (lpCreateParams member) pointed to by the lParam param of the WM_CREATE message.
		this
	);

	SetWindowLongPtr((HWND)NativeHandle(), GWLP_USERDATA, (LONG_PTR)this);

	for (const auto & option : _options) {
		SendMessage((HWND)_handles[1], CB_ADDSTRING, (WPARAM)0, (LPARAM)option.begin());
	}

	SendMessage((HWND)_handles[1], CB_SETCURSEL, Index(), 0);
}

void OptionControlBase::iControlRect(const Rect & actual) {
	BOOL succeed0 = SetWindowPos(
		// A handle to the window.
		(HWND)_handles[0],
		// A handle to the window to precede the positioned window in the Z order.
		HWND_TOP,
		// The new position of the left side of the window, in client coordinates.
		actual.Left,
		// The new position of the top of the window, in client coordinates.
		actual.Top,
		// The new width of the window, in pixels.
		actual.Width,
		// The new height of the window, in pixels.
		actual.Height,
		// The window sizing and positioning flags. [SWP_]
		0
	);
	BOOL succeed1 = SetWindowPos(
		// A handle to the window.
		(HWND)_handles[1],
		// A handle to the window to precede the positioned window in the Z order.
		HWND_TOP,
		// The new position of the left side of the window, in client coordinates.
		_padding.Left,
		// The new position of the top of the window, in client coordinates.
		_padding.Top,
		// The new width of the window, in pixels.
		actual.Width - (_padding.Left + _padding.Right),
		// The new height of the window, in pixels.
		actual.Height - (_padding.Top + _padding.Bottom),
		// The window sizing and positioning flags. [SWP_]
		0
	);
}

void OptionControlBase::downloadIndex() {
	int oldIndex = _selected;
	int newIndex = (int)SendMessage((HWND)_handles[1], CB_GETCURSEL, 0, 0);
	if (oldIndex != newIndex) {
		_selected = newIndex;
		OnSelectionDownload(oldIndex, newIndex);
		OnSelectionChanged(oldIndex, newIndex);
	}
}

void OptionControlBase::AddOption(const String & option) {
	_options.Add(option);
	if (Ready()) {
		GCommand([option, this]() { SendMessage((HWND)_handles[1], CB_ADDSTRING, (WPARAM)0, (LPARAM)option.begin()); });
	}
}

void OptionControlBase::Clear() {
	if (Ready()) {
		int count = (int)SendMessage((HWND)_handles[1], CB_GETCOUNT, 0, 0);
		while (count--) {
			PostMessage((HWND)_handles[1], CB_DELETESTRING, count, 0);
		}
	}

	_options.Clear();

	int oldIndex = _selected;
	int newIndex = -1;
	if (oldIndex != newIndex) {
		_selected = newIndex;
		OnSelectionChanged(oldIndex, newIndex);
	}
}

void OptionControlBase::Index(i32 newIndex, bool silent) {
	if (_selected != newIndex) {
		int oldIndex = _selected;
		_selected = newIndex;
		if (Ready()) {
			PostMessage((HWND)_handles[1], CB_SETCURSEL, _selected, 0);
		}
		if (!silent) {
			OnSelectionUpload(oldIndex, newIndex);
			OnSelectionChanged(oldIndex, newIndex);
		}
	}
}

void OptionControlBase::InsertOption(size_t index, const String & option) {
	_options.Insert(index, option);
	if (Ready()) {
		PostMessage((HWND)_handles[1], CB_ADDSTRING, (WPARAM)index, (LPARAM)option.begin());
	}
}

void OptionControlBase::RemoveOption(size_t index) {
	_options.RemoveAt(index);
	if (Ready()) {
		PostMessage((HWND)_handles[1], CB_DELETESTRING, (WPARAM)index, (LPARAM)0);
	}
}
#pragma endregion

/************************************************************************/
/* TabViewControl                                                       */
/************************************************************************/
#pragma region tabview

#if CONTROLS_LOG
#define TABVIEW_LOG(...) DD_LOG("TabView:", __VA_ARGS__);
#else
#define TABVIEW_LOG(...)
#endif

struct TabViewControlBaseHelper : public TabViewControlBase {
	HWND Handles(int i) { return (HWND)_handles[i]; }
	void UnsafeIndex(int index) { _index = index; }
	int Size() { return (int)_tabs.Size(); }
};

DD_REGISTER_SIMPLE_WINAPI_CLASS(tabview) {
	TabViewControlBaseHelper * control = (TabViewControlBaseHelper *)GetWindowLongPtr(hWnd, GWLP_USERDATA);
	if (control) switch (message) {
		case WM_NOTIFY:
			if (((LPNMHDR)lParam)->code == TCN_SELCHANGE) {
				int oldIndex = control->Index();
				int newIndex = TabCtrl_GetCurSel(control->Handles(1));
				if (oldIndex != newIndex) {
					if (0 <= oldIndex && oldIndex < control->Size())
						control->Tab(oldIndex)->Show(false);
					if (0 <= newIndex && newIndex < control->Size())
						control->Tab(newIndex)->Show(true);

					control->UnsafeIndex(newIndex);
					control->OnTabChanged(oldIndex, newIndex);
				}
				return 0;
			}
		case WM_SIZE:
			control->OnScaleChanged();
			break;
	}
	return DefWindowProc(hWnd, message, wParam, lParam);
}

void TabViewControlBase::iControlInit() {
	DWORD style = 0;
	if (_show)
		style |= WS_VISIBLE;

	_handles[0] = CreateWindowW(
		// A null - terminated string or a class atom created by a previous call to the RegisterClass or RegisterClassEx function.
		tabviewInit(),
		// The window name. If the window style specifies a title bar, the window title pointed to by lpWindowName is displayed in the title bar.
		NULL,
		// The style of the window being created.
		style | WS_CHILD,
		// The initial horizontal position of the window.
		0,
		// The initial vertical position of the window.
		0,
		// The width, in device units, of the window. For overlapped windows, nWidth is either the window's width, in screen coordinates, or CW_USEDEFAULT.
		100,
		// The height, in device units, of the window.For overlapped windows, nHeight is the window's height, in screen coordinates. If nWidth is set to CW_USEDEFAULT, the system ignores nHeight.
		100,
		// A handle to the parent or owner window of the window being created.
		(HWND)_parent->NativeHandle(),
		// A handle to a menu, or specifies a child-window identifier depending on the window style.
		NULL,
		// A handle to the instance of the module to be associated with the window.
		reinterpret_cast<HINSTANCE>(GetModuleHandleW(NULL)),
		// A pointer to a value to be passed to the window through the CREATESTRUCT structure (lpCreateParams member) pointed to by the lParam param of the WM_CREATE message.
		this
	);

	SetWindowLongPtr((HWND)NativeHandle(), GWLP_USERDATA, (LONG_PTR)this);

	_handles[1] = CreateWindowW(
		// A null - terminated string or a class atom created by a previous call to the RegisterClass or RegisterClassEx function.
		WC_TABCONTROL,
		// The window name. If the window style specifies a title bar, the window title pointed to by lpWindowName is displayed in the title bar.
		NULL,
		// The style of the window being created.
		WS_VISIBLE | WS_CHILD,
		// The initial horizontal position of the window.
		0,
		// The initial vertical position of the window.
		0,
		// The width, in device units, of the window. For overlapped windows, nWidth is either the window's width, in screen coordinates, or CW_USEDEFAULT.
		100,
		// The height, in device units, of the window.For overlapped windows, nHeight is the window's height, in screen coordinates. If nWidth is set to CW_USEDEFAULT, the system ignores nHeight.
		100,
		// A handle to the parent or owner window of the window being created.
		(HWND)_handles[0],
		// A handle to a menu, or specifies a child-window identifier depending on the window style.
		NULL,
		// A handle to the instance of the module to be associated with the window.
		reinterpret_cast<HINSTANCE>(GetModuleHandleW(NULL)),
		// A pointer to a value to be passed to the window through the CREATESTRUCT structure (lpCreateParams member) pointed to by the lParam param of the WM_CREATE message.
		this
	);

	SetWindowLongPtr((HWND)NativeHandle(), GWLP_USERDATA, (LONG_PTR)this);


	TCITEMW tie = { 0 };
	tie.mask = TCIF_TEXT;
	for (int i = 0; i < _tabs.Size(); ++i) {
		tie.pszText = _tabs[i].Name.begin();
		TabCtrl_InsertItem((HWND)_handles[1], i, &tie);
	}

	TabCtrl_SetCurSel((HWND)_handles[1], _index);
}

void TabViewControlBase::iControlRect(const Rect & actual) {
	BOOL succeed0 = SetWindowPos(
		// A handle to the window.
		(HWND)_handles[0],
		// A handle to the window to precede the positioned window in the Z order.
		HWND_TOP,
		// The new position of the left side of the window, in client coordinates.
		actual.Left,
		// The new position of the top of the window, in client coordinates.
		actual.Top,
		// The new width of the window, in pixels.
		actual.Width,
		// The new height of the window, in pixels.
		actual.Height,
		// The window sizing and positioning flags. [SWP_]
		0
	);
	BOOL succeed1 = SetWindowPos(
		// A handle to the window.
		(HWND)_handles[1],
		// A handle to the window to precede the positioned window in the Z order.
		HWND_TOP,
		// The new position of the left side of the window, in client coordinates.
		0,
		// The new position of the top of the window, in client coordinates.
		0,
		// The new width of the window, in pixels.
		actual.Width,
		// The new height of the window, in pixels.
		actual.Height,
		// The window sizing and positioning flags. [SWP_]
		0
	);
}

void TabViewControlBase::InsertTab(i32 index, const String & name, IControl * tab) {
	if (_tabs.Size()) {
		// added tab should not be shown
		tab->Show(false);
	}
	else {
		tab->Show(true);
	}

	// add tab as child
	Add(tab);
	// insert
	_tabs.Insert(index) = { name, tab };
	// insert into winapi
	if (Ready()) {
		TCITEMW tie = { 0 };
		tie.mask = TCIF_TEXT;
		tie.pszText = _tabs[index].Name.begin();
		TabCtrl_InsertItem((HWND)_handles[1], index, &tie);

		if (_tabs.Size() == 1) {
			OnSizeChanged();
		}
	}

	if (_tabs.Size() > 1) {
		if (index <= _index) {
			i32 nIndex = _index + 1;
			MemoryBarrier();
			Index(nIndex);
		}
	}
	else {
		Index(0);
	}
}

void TabViewControlBase::SetTab(i32 index, const String & name, IControl * tab) {
	if (index >= _tabs.Size()) {
		DD_WARNING("Index is out of range");
	}
	else {
		// remove old tab
		if (_tabs[index].Control)
			_tabs[index].Control->Remove();
		// set tab properties
		Add(tab);
		tab->Show(index == _index);
		_tabs[index].Control = tab;
		_tabs[index].Name = name;
		// update gui
		TCITEMW tie = { 0 };
		tie.mask = TCIF_TEXT;
		tie.pszText = _tabs[index].Name.begin();
		TabCtrl_SetItem((HWND)_handles[1], index, &tie);
	}
}

void TabViewControlBase::Index(i32 newIndex) {
	int oldIndex = _index;
	if (oldIndex != newIndex) {
		if (Ready()) {
			TabCtrl_SetCurSel((HWND)_handles[1], newIndex);
		}
		_index = newIndex;
		if (0 <= oldIndex && oldIndex < (int)_tabs.Size())
			Tab(oldIndex)->Show(false);
		if (0 <= newIndex && newIndex < (int)_tabs.Size())
			Tab(newIndex)->Show(true);

		OnSizeChanged();
		OnTabChanged(oldIndex, newIndex);
	}
}

i32 TabViewControlBase::Name2Index(const Text::StringView16 & name) const {
	for (i32 i = 0, m = (i32)_tabs.Size(); i < m; ++i) {
		if (name == _tabs[i].Name.View()) {
			return i;
		}
	}

	return -1;
}

TabViewControlBase::TabViewControlBase(TabViewControlTypes) {
	OnChildAdd += [this](IControl * child) {
		OnSizeChanged += child->OnSizeChanged;
	};

	OnChildRemove += [this](IControl * child) {
		OnSizeChanged -= child->OnSizeChanged;
		// remove child from tabs
		for (auto & tab : _tabs) {
			if (tab.Control.Ptr() == child) {
				tab.Control.Release();
				break;
			}
		}
	};

	OnSizeChanged += [this]() {
		//GWindowProcess.Commands->Add([this]() {
			recomputeRect();

			RECT client;
			GetClientRect((HWND)_handles[1], &client);
			RECT client2 = client;
			TabCtrl_AdjustRect((HWND)_handles[1], FALSE, &client);

			_client.Left = client.left;
			_client.Top = client.top;
			_client.Width = client.right - client.left;
			_client.Height = client.bottom - client.top;

			if (TabCount() && Tab())
				Tab()->OnSizeChanged();
		//});
	};
}



#pragma endregion

/************************************************************************/
/* ListView                                                             */
/************************************************************************/
#pragma region listview

#if CONTROLS_LOG
#define LISTVIEW_LOG(...) DD_LOG("ListView", __VA_ARGS__)
#else
#define LISTVIEW_LOG(...)
#endif

void ListViewBase::iControlInit() {
	DWORD style = 0;
	if (_show)
		style |= WS_VISIBLE;

	_handles[0] = CreateWindowW(
		// A null - terminated string or a class atom created by a previous call to the RegisterClass or RegisterClassEx function.
		WC_LISTVIEW,
		// The window name. If the window style specifies a title bar, the window title pointed to by lpWindowName is displayed in the title bar.
		L"",
		// The style of the window being created.
		style | WS_BORDER | WS_CHILD | LVS_REPORT | LVS_EX_DOUBLEBUFFER | WS_VSCROLL,
		// The initial horizontal position of the window.
		0,
		// The initial vertical position of the window.
		0,
		// The width, in device units, of the window. For overlapped windows, nWidth is either the window's width, in screen coordinates, or CW_USEDEFAULT.
		100,
		// The height, in device units, of the window.For overlapped windows, nHeight is the window's height, in screen coordinates. If nWidth is set to CW_USEDEFAULT, the system ignores nHeight.
		100,
		// A handle to the parent or owner window of the window being created.
		(HWND)_parent->NativeHandle(),
		// A handle to a menu, or specifies a child-window identifier depending on the window style.
		nullptr,
		// A handle to the instance of the module to be associated with the window.
		reinterpret_cast<HINSTANCE>(GetModuleHandleW(NULL)),
		// A pointer to a value to be passed to the window through the CREATESTRUCT structure (lpCreateParams member) pointed to by the lParam param of the WM_CREATE message.
		NULL
	);

	ShowScrollBar((HWND)NativeHandle(), SB_VERT, ESB_ENABLE_BOTH);

	ListView_SetExtendedListViewStyle((HWND)NativeHandle(), LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
}

void ListViewBase::setColWidth(int i, int pixels) {
	GWindowProcess.Commands->Add([this, i, pixels]() {
		LISTVIEW_LOG("SetColWidth", i, pixels);
		ListView_SetColumnWidth((HWND)NativeHandle(), i, pixels);
	});
}

void ListViewBase::recomputeCols() {
	static constexpr int MINIMAL_WIDTH = 25;
	recomputeRect();

	recomputeClient();

	int partitions = 0;
	int pixels = (int)_client.Width;
	int pixelRest = pixels;
	float percent = pixels / 100.f;

	for (int i = 0; i < _cols.Size(); ++i) {
		ColDescription & col = _cols[i];
		switch (col.Width.Mode) {
			case SizeModes::Pixels: {
				pixelRest -= (int)col.Width.Value;
				setColWidth(i, (int)col.Width.Value);
			} break;
			case SizeModes::Points: {
				int width = (int)Scaled(NativeHandle(), col.Width.Value);
				pixelRest -= width;
				setColWidth(i, width);
			} break;
			case SizeModes::Percents: {
				int width = Math::Max(MINIMAL_WIDTH, (int)(col.Width.Value * percent));
				pixelRest -= width;
				setColWidth(i, width);
			} break;
			case SizeModes::Partitions: {
				partitions += (int)col.Width.Value;
			} break;
			default: throw;
		}
	}

	float partionCoef = (float)pixelRest / partitions;

	for (int i = 0; i < _cols.Size(); ++i) {
		ColDescription & col = _cols[i];
		switch (col.Width.Mode) {
			case SizeModes::Partitions: {
				int width = Math::Max(MINIMAL_WIDTH, (int)(col.Width.Value * partionCoef));
				setColWidth(i, width);
			} break;
		}
	}
}

void ListViewBase::InsertCol(int col) {
	_cols.Insert(col, ColDescription());
	GWindowProcess.Commands->Add([col, this]() mutable {
		LVCOLUMN lvc = { 0 };
		lvc.mask = LVCF_TEXT;
		lvc.pszText = (LPWSTR)L"";
		LISTVIEW_LOG("InsertCol", col);
		ListView_InsertColumn((HWND)NativeHandle(), col, &lvc);
		recomputeCols();
	});
}

void ListViewBase::ResizeBuffer(int i) {
	GWindowProcess.Commands->Add([i, this]() mutable {
		LISTVIEW_LOG("ResizeBuffer", i);
		ListView_SetItemCount((HWND)NativeHandle(), i);
	});
}

void ListViewBase::SetColText(int col, const String & name) {
	GWindowProcess.Commands->Add([col, name, this]() mutable {
		LVCOLUMN lvc = { 0 };
		lvc.mask = LVCF_TEXT | LVCF_SUBITEM;
		lvc.pszText = (LPWSTR)name.begin();
		LISTVIEW_LOG("SetColText", col, name);
		ListView_SetColumn((HWND)NativeHandle(), col, &lvc);
	});
}

void ListViewBase::SetColAlign(int col, TAlign align) {
	int fmt;

	switch (align) {
		case TAlign::Left:
			fmt = LVCFMT_LEFT;
			break;
		case TAlign::Center:
			fmt = LVCFMT_CENTER;
			break;
		case TAlign::Right:
			fmt = LVCFMT_RIGHT;
			break;
	}

	GWindowProcess.Commands->Add([col, fmt, this]() {
		LVCOLUMN lvc;
		lvc.mask = LVCF_FMT;
		lvc.fmt = fmt;
		ListView_SetColumn((HWND)NativeHandle(), col, &lvc);
	});
}

void ListViewBase::ShowRow(int n) {
	GWindowProcess.Commands->Add([this, n]() {
		ListView_EnsureVisible((HWND)NativeHandle(), n, TRUE);
	});
}

ListViewBase::RowsProxy ListViewBase::Rows() {
	return { this };
}

ListViewBase::ColsProxy ListViewBase::Cols() {
	return ColsProxy{ this };
}

void ListViewBase::InsertRow(int i) {
	++_rows;
	GWindowProcess.Commands->Add([i, this]() {
		LISTVIEW_LOG("InsertRow", i);
		LVITEM item = { 0 };
		item.iItem = i;
		ListView_InsertItem((HWND)NativeHandle(), &item);
	});
}

void ListViewBase::SetItemText(int row, int col, const String & str) {
	GWindowProcess.Commands->Add([this, str, col, row]() mutable {
		LISTVIEW_LOG("SetItemText", row, col, str);
		ListView_SetItemText((HWND)NativeHandle(), row, col, (LPWSTR)str.begin());
	});
}

ListViewBase::RowProxy ListViewBase::RowsProxy::operator[](int row) {
	return { _proxy, row };
}

ListViewBase::RowProxy ListViewBase::RowsProxy::First() {
	return operator[](0);
}

ListViewBase::RowProxy ListViewBase::RowsProxy::Last() {
	return operator[](Size() - 1);
}

ListViewBase::RowProxy ListViewBase::RowsProxy::Insert(int i) {
	_proxy->InsertRow(i); return operator[](i);
}

ListViewBase::RowProxy ListViewBase::RowsProxy::Add() {
	_proxy->InsertRow(Size()); return Last();
}

ListViewBase::ColProxy ListViewBase::ColsProxy::operator[](int col) {
	return { _proxy, col };
}

ListViewBase::ColProxy ListViewBase::ColsProxy::First() {
	return operator[](0);
}

ListViewBase::ColProxy ListViewBase::ColsProxy::Last() {
	return operator[](Size() - 1);
}

ListViewBase::ColProxy ListViewBase::ColsProxy::Add() {
	_proxy->InsertCol(Size()); return Last();
}

ListViewBase::ColProxy ListViewBase::ColsProxy::Insert(int i) {
	_proxy->InsertCol(i); return operator[](i);
}

ListViewBase::CellProxy ListViewBase::RowProxy::operator[](int col) {
	return CellProxy{ _proxy, _row, col };
}

#pragma endregion

/************************************************************************/
/* Window                                                               */
/************************************************************************/
#pragma region window

struct WindowBaseHelper : public WindowBase {
	void RunMenu(int i, int id) { _menu[i](id); }
	HMENU TrayMenuHandle(int i) { return (HMENU)_trays[i].Menu; }
	void RunTrayDoubleClick(int i) { _trays[i].OnDoubleClick(); }
	auto & UnsafeChildren() { return _children; }
};

DD_REGISTER_SIMPLE_WINAPI_CLASS(window) {
	WindowBaseHelper * ddWindow = (WindowBaseHelper *)GetWindowLongPtr(hWnd, GWLP_USERDATA);
	if (ddWindow) {
		switch (message) {
			case WM_SIZING: return 0;
			// Sent to a window immediately before it loses the keyboard focus.
			case WM_KILLFOCUS:
				ddWindow->OnLostFocus();
				break;
				// Sent to a window after it has gained the keyboard focus.
			case WM_SETFOCUS:
				ddWindow->OnGotFocus();
				break;
				// Posted when the user double-clicks the left mouse button while the cursor is in the client area of a window.
			case WM_LBUTTONDBLCLK:
				ddWindow->OnMouseLeftDoubleClick();
				break;
				// Posted when the user presses the left mouse button while the cursor is in the client area of a window.
			case WM_LBUTTONDOWN:
				ddWindow->OnMouseLeftDown();
				break;
				// Posted when the user releases the left mouse button while the cursor is in the client area of a window.
			case WM_LBUTTONUP:
				ddWindow->OnMouseLeftUp();
				break;
				// Posted when the user double-clicks the right mouse button while the cursor is in the client area of a window.
			case WM_RBUTTONDBLCLK:
				ddWindow->OnMouseRightDoubleClick();
				break;
				// Posted when the user presses the right mouse button while the cursor is in the client area of a window.
			case WM_RBUTTONDOWN:
				ddWindow->OnMouseRightDown();
				break;
				// Posted when the user releases the right mouse button while the cursor is in the client area of a window.
			case WM_RBUTTONUP:
				ddWindow->OnMouseRightUp();
				break;
				// Posted when the user double-clicks the middle mouse button while the cursor is in the client area of a window.
			case WM_MBUTTONDBLCLK:
				ddWindow->OnMouseMiddleDoubleClick();
				break;
				// Posted when the user presses the middle mouse button while the cursor is in the client area of a window.
			case WM_MBUTTONDOWN:
				ddWindow->OnMouseMiddleDown();
				break;
				// Posted when the user releases the middle mouse button while the cursor is in the client area of a window.
			case WM_MBUTTONUP:
				ddWindow->OnMouseMiddleUp();
				break;
				// Sent to the focus window when the mouse wheel is rotated.
			case WM_MOUSEWHEEL:
				ddWindow->OnMouseWheel(GET_WHEEL_DELTA_WPARAM(wParam));
				break;
				// Posted to a window when the cursor moves.
			case WM_MOUSEMOVE:
				ddWindow->updateMouse(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
				break;
				// Posted to the window with the keyboard focus when a nonsystem key is released.
			case WM_KEYUP:
				ddWindow->OnKeyUp((Input::Key::Values)wParam);
				break;
				// Posted to the window with the keyboard focus when a nonsystem key is pressed.
			case WM_KEYDOWN:
				ddWindow->OnKeyDown((Input::Key::Values)wParam);
				break;
				// The message is sent when the system or another application makes a request to paint a portion of an application's window.
			case WM_PAINT:
			{ PAINTSTRUCT ps; HDC hdc; hdc = BeginPaint(hWnd, &ps); EndPaint(hWnd, &ps); }
			return 0;
			// Indicates a request to terminate an application, and is generated when the application calls the PostQuitMessage function.
			case WM_QUIT:
				// Sent as a signal that a window or an application should terminate.
			case WM_CLOSE:
				// Sent when a window is being destroyed. It is sent after the window is removed from the screen.
			case WM_DESTROY:
				ddWindow->Close();
				return 0;
				// Sent after a window has been moved.
			case WM_MOVE:
				ddWindow->updatePosition((i16)LOWORD(lParam), (i16)HIWORD(lParam));
				break;
			case WM_ENTERSIZEMOVE: {
				auto childrenProxy = *ddWindow->UnsafeChildren();
				for (auto & child : *childrenProxy) {
					child->Show(false);
				}
			} break;
			case WM_EXITSIZEMOVE:
				GCommand([=]() {
					auto childrenProxy = *ddWindow->UnsafeChildren();
					for (auto & child : *childrenProxy) {
						child->Show(true);
					}
				});
				break;
				// Sent to a window after its size has changed.
			case WM_SIZE:
				ddWindow->updateSize((i16)LOWORD(lParam), (i16)HIWORD(lParam));
				break;
			case WM_NOTIFY:
				ddWindow->OnNotify(((LPNMHDR)lParam)->idFrom, ((LPNMHDR)lParam)->code);
				break;
			case WM_TIMER:
				ddWindow->OnTimerTick((i32)wParam);
				return 0;
			case WM_COMMAND: {
				int commandID = LOWORD(wParam);
					if (commandID & Window::MenuProxy::MENU_FLAG) {
						ddWindow->RunMenu(commandID ^ Window::MenuProxy::MENU_FLAG, commandID);
					}
			} return 0;
			case WM_TRAY: {
				// int idOfTray = wParam;
				switch (lParam) {
					case WM_LBUTTONDBLCLK: {
						ddWindow->RunTrayDoubleClick((int)wParam);
					} return 0;
					case WM_RBUTTONDOWN: {
						if (ddWindow->TrayMenuHandle((int)wParam)) {
							POINT pt;
							GetCursorPos(&pt);
							SetForegroundWindow((HWND)ddWindow->NativeHandle());
							TrackPopupMenu(
								// handle of content menu
								ddWindow->TrayMenuHandle((int)wParam),
								// alignment flags
								TPM_LEFTALIGN | TPM_LEFTBUTTON | TPM_BOTTOMALIGN,
								// x position
								pt.x,
								// y position
								pt.y,
								// reserved space
								0,
								// handle of owner
								(HWND)ddWindow->NativeHandle(),
								// rect
								NULL
							);
						}
					} return 0;
				}
			} break;
		}
	}

	return DefWindowProc(hWnd, message, wParam, lParam);
}

void Window::ShowCursor() {
	::ShowCursor(TRUE);
}

void Window::HideCursor() {
	::ShowCursor(FALSE);
}

void Window::SetCursor(i32 x, i32 y) {
	::SetCursorPos(x, y);
}

void Window::Loop() {
	GWindowProcess.Loop();
}

WindowBase::NativeView * WindowBase::nativeView() const {
	//static_assert(sizeof(NativeView) == sizeof(_handles), "Invalid size.");
	return (NativeView *)_handles;
}
void WindowBase::updateTitle() {
	if (Ready()) {
		SetWindowText(nativeView()->Window, _title);
	}
}

void WindowBase::updateMouse(i32 x, i32 y) {
	if (_mouseX != x || _mouseY != y) {
		_mouseX = x, _mouseY = y;
		OnMouseMove(x, y);
	}
}

void WindowBase::updatePosition(i32 l, i32 t) {
	if (_client.Left != l || _client.Top != t) {
		_client.Left = l, _client.Top = t;
		recomputeWindow();
		OnPositionChanged();
	}
}

void WindowBase::updateSize(u32 w, u32 h) {
	recomputeWindow();
	recomputeClient();
	GCommand([this]() {
		OnSizeChanged();
	});
}

bool WindowBase::Ready() const {
	return nativeView()->Window != 0;
}

bool WindowBase::Focused() const {
	if (Ready())
		return GetFocus() == nativeView()->Window;
	else
		return false;
}

void WindowBase::iControlInit() {
	DWORD alwaysOnTop = _onTop ? WS_EX_TOPMOST : 0;

	nativeView()->Window = CreateWindowEx(
		// The extended window style of the window being created.
		alwaysOnTop,
		// A null - terminated string or a class atom created by a previous call to the RegisterClass or RegisterClassEx function.
		windowInit(),
		// The window name. If the window style specifies a title bar, the window title pointed to by lpWindowName is displayed in the title bar.
		_title,
		// The style of the window being created.
		WS_OVERLAPPEDWINDOW,
		// The initial horizontal position of the window.
		_design.Left,
		// The initial vertical position of the window.
		_design.Top,
		// The width, in device units, of the window. For overlapped windows, nWidth is either the window's width, in screen coordinates, or CW_USEDEFAULT.
		_design.Width,
		// The height, in device units, of the window.For overlapped windows, nHeight is the window's height, in screen coordinates. If nWidth is set to CW_USEDEFAULT, the system ignores nHeight.
		_design.Height,
		// A handle to the parent or owner window of the window being created.
		NULL,
		// A handle to a menu, or specifies a child-window identifier depending on the window style.
		NULL,
		// A handle to the instance of the module to be associated with the window.
		reinterpret_cast<HINSTANCE>(GetModuleHandleW(NULL)),
		// A pointer to a value to be passed to the window through the CREATESTRUCT structure (lpCreateParams member) pointed to by the lParam param of the WM_CREATE message.
		this
	);

	if (!nativeView()->Window) {
		throw;
	}

	recomputeClient();


	Scale(Scaled(NativeHandle(), 1.f));

	SetWindowLongPtr(nativeView()->Window, GWLP_USERDATA, (LONG_PTR)this);
	ShowWindow(nativeView()->Window, SW_SHOW);
	GWindowProcess.Register(this);

	_tooltipHandle = (void *)CreateWindowEx(
		WS_EX_TOOLWINDOW,
		TOOLTIPS_CLASS,
		NULL,
		WS_POPUP | TTS_ALWAYSTIP,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		(HWND)_handles[0],
		NULL,
		NULL,
		NULL
	);

	SendMessage((HWND)_tooltipHandle, TTM_SETMAXTIPWIDTH, 1, SHRT_MAX);

	OnLoad();
}

void WindowBase::iControlDestroy() {
	IControl::iControlDestroy();
	GWindowProcess.Unregister(this);
}

String WindowBase::Clipboard() const {
	// empty string if clipboard not found
	String result;
	if (OpenClipboard(nativeView()->Window)) {
		// get 16-bit string from cb
		wchar_t * clip = (wchar_t *)GetClipboardData(CF_UNICODETEXT);
		if (clip)
			result = clip;
		CloseClipboard();
	}
	return result;
}

WindowBase & WindowBase::Position(i32 x, i32 y) {
	_design.Left = x, _design.Top = y;
	recomputeRect();
	return *this;
}

WindowBase & WindowBase::Size(u32 w, u32 h) {
	_design.Width = w, _design.Height = h;
	recomputeRect();
	return *this;
}

WindowBase & WindowBase::Title(const String & v) {
	_title = v;
	updateTitle();
	return *this;
}

WindowBase & WindowBase::Style(WindowTypes s) {
	if (Ready())
		throw;
	_style = s;
	return *this;
}

WindowBase & WindowBase::Clipboard(const String & s) {
	if (OpenClipboard(nativeView()->Window)) {
		EmptyClipboard();
		SIZE_T bytes = (s.Length() + 1) * 2;
		HGLOBAL buffer = GlobalAlloc(GMEM_DDESHARE, bytes);
		LPVOID dest = GlobalLock(buffer);
		memcpy(dest, s.begin(), bytes);
		GlobalUnlock(buffer);
		SetClipboardData(CF_UNICODETEXT, buffer);
		CloseClipboard();
		return *this;
	}
	throw;
}

void WindowBase::AlwaysOnTop(bool onTop) {
	if (_onTop != onTop) {
		_onTop = onTop;
		if (Ready()) {
			HWND options = onTop ? HWND_TOPMOST : HWND_NOTOPMOST;
			SetWindowPos((HWND)NativeHandle(), options, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
		}
		OnAlwaysOnTop();
	}
}

void WindowBase::Mouse(i32 x, i32 y) {
	POINT p = { x, y };
	if (ClientToScreen(nativeView()->Window, &p))
		Window::SetCursor(p.x, p.y);
}

void WindowBase::Close() {
	bool close = true;
	OnClose(close);
	if (close) {
		GWindowProcess.Unregister(this);
		SetWindowLongPtr(nativeView()->Window, GWLP_USERDATA, (LONG_PTR)this);
		PostQuitMessage(0);
	}
	//if (nativeView()->Thread)
		//WaitForSingleObject(nativeView()->Thread, 200);
}

void WindowBase::Maximize() {
	SendMessage(nativeView()->Window, WM_SYSCOMMAND, SC_MAXIMIZE, 0);
}

void WindowBase::Minimize() {
	SendMessage(nativeView()->Window, WM_SYSCOMMAND, SC_MINIMIZE, 0);
}

void WindowBase::Timer(i32 timerId, u32 milliseconds) {
	SetTimer(nativeView()->Window, timerId, milliseconds, NULL);
}

void WindowBase::RemoveTimer(i32 timerId) {
	KillTimer(nativeView()->Window, timerId);
}

WindowBase::MenuProxy WindowBase::Menu() {
	MenuProxy menu;
	menu._parent = this;
	menu._handle = _menuHandle;
	if (menu._handle == 0) {
		_menuHandle = menu._handle = CreateMenu();
		if (Ready()) {
			SetMenu((HWND)NativeHandle(), (HMENU)_menuHandle);
			OnSizeChanged();
		}
	}

	return menu;
}

WindowBase::TrayProxy WindowBase::Tray(int index) {
	if (_trays.Size() <= index) {
		_trays.Resize(index + 1);
	}

	TrayProxy proxy;
	proxy._index = index;
	proxy._parent = this;
	proxy.OnDoubleClick = _trays[index].OnDoubleClick;

	return proxy;
}

WindowBase::WindowBase(WindowTypes type, const String & name)
	: _style(type)
	, _title(name)
{
	OnChildAdd += [this](IControl * child) { OnSizeChanged += child->OnSizeChanged; };
	OnChildRemove += [this](IControl * child) { OnSizeChanged -= child->OnSizeChanged; };
}

void WindowBase::TrayProxy::Enable() {
	NOTIFYICONDATA nidApp = { 0 };
	nidApp.cbSize = sizeof(NOTIFYICONDATA);
	nidApp.hWnd = (HWND)_parent->NativeHandle();
	nidApp.uID = _index;
	// set tray message
	nidApp.uFlags |= NIF_MESSAGE;
	nidApp.uCallbackMessage = WM_TRAY;
	// set the same icon as application has
	nidApp.uFlags |= NIF_ICON;
	nidApp.hIcon = LoadIconW(NULL, IDI_APPLICATION);

	GCommand([nidApp]() mutable { Shell_NotifyIcon(NIM_ADD, &nidApp); });
}

void WindowBase::TrayProxy::Disable() {
	NOTIFYICONDATA nidApp = { 0 };
	nidApp.cbSize = sizeof(NOTIFYICONDATA);
	nidApp.hWnd = (HWND)_parent->NativeHandle();
	nidApp.uID = _index;

	GCommand([nidApp]() mutable { Shell_NotifyIcon(NIM_DELETE, &nidApp); });
}


void WindowBase::TrayProxy::Tooltip(const String & tip) {
	NOTIFYICONDATA nidApp = { 0 };
	nidApp.cbSize = sizeof(NOTIFYICONDATA);
	nidApp.hWnd = (HWND)_parent->NativeHandle();
	nidApp.uID = _index;
	// set tooltip
	String tooltip;
	tooltip.Wrap(ArrayView(nidApp.szTip));
	nidApp.uFlags |= NIF_TIP;
	tooltip = tip;
	GCommand([nidApp]() mutable { Shell_NotifyIcon(NIM_MODIFY, &nidApp); });
}

WindowBase::MenuProxy WindowBase::TrayProxy::Menu() {
	MenuProxy menu;
	menu._parent = _parent;
	menu._handle = _parent->_trays[_index].Menu;
	menu._routines = &(_parent->_trays[_index].TrayMenuRoutines);
	if (menu._handle == 0) {
		_parent->_trays[_index].Menu = menu._handle = CreatePopupMenu();
	}

	return menu;
}
#pragma endregion
