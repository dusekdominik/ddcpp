#include <DD/Simulation/PointCloudModel.h>
#include <DD/Stopwatch.h>
#include <initializer_list>

using namespace DD;
using namespace DD::Simulation;

void PointCloudModel::initModels() {
	const Array3D<PointCloudBase::Cell> & grid = _pointCloud->Grid();
	_models.Reallocate(grid.Lengths());
	for (const auto & index : Sequencer::Drive(_models.ToView().Counter())) {
		const PointCloudBase::Cell & cell = grid[index];
		if (cell.Size()) {
			_models[index] = new QuadModel(const_cast<QuadVertex *>(cell.begin()), cell.Size(), _pointSize);
		}
	}
}


void PointCloudModel::initFrustumCullingHierarchy(bool initialized) {
	Array3D<Reference<ICullableModel>> model[2];
	// init same grid as for native models
	model[0].Reallocate(_models.Lengths()).CopyIn(_models.ToView());
	// iteration controls copying from even to odd model
	int iteration = 0;
	while (true) {
		// model already created
		Array3D<Reference<ICullableModel>> & source = model[iteration % 2];
		// model which will be created
		Array3D<Reference<ICullableModel>> & target = model[(iteration + 1) % 2];
		// model should be 4-times smaller
		target.Reallocate(source.Lengths().Convert([](size_t length) { return length / 2 + length % 2; }));

		// each 4 groups should be grouped into one
		for (const auto & tgtIndex : Sequencer::Drive(target.ToView().Counter())) {
			// declare new group
			Reference<IntersectableModelGroup> group = new IntersectableModelGroup();

			// fill group
			auto begin = tgtIndex.Convert([](size_t i) { return 2 * i; });
			auto end = tgtIndex.Convert([](size_t i) { return 2 * i + 1; });
			for (const auto & srcIndex : Sequencer::Drive(source.ToView().Counter()))
				if (source[srcIndex].IsNotNull())
					group->Add(source[srcIndex], initialized);

			// push to target
			if (!group->IsEmpty())
				target[tgtIndex] = group;

			// if all the data are grouped to just one group, end
			if (target.Lengths().UnaryAnd([](size_t l) { return l == 1; })) {
				_pointModel = target[{0, 0, 0}];
				break;
			}
			++iteration;
		}
	}
}


void PointCloudModel::init() {
	if (!_inited) {
		initModels();
		initFrustumCullingHierarchy();
		_boundingBox = _pointCloud->Area();
		_boundingSphere = _boundingBox.BoundingSphere();
		_inited = true;
	}
}


PointCloudModel::PointCloudModel(PointCloud & pointCloud, f32 pointSize)
	: ICullableModel()
	, _pointCloud(pointCloud)
	, _inited(false)
	, _pointSize(pointSize)
{
	_pointCloud->LockPoints();

	if (pointSize == 0.f)
		_pointSize = _pointCloud->RecommendedPointSize();
}


const char * DD::Simulation::PointCloudModel::iModelName() const {
	return "PointCloudModel";
}


void PointCloudModel::iModelInit(Gpu::Device & device) {
	init();
	if (_pointModel)
		_pointModel->Init(device);
}


void PointCloudModel::iModelDraw(DrawArgs & args) {
	if (_pointModel)
		_pointModel->Draw(args);
}


void PointCloudModel::iModelDrawCulled(const Frustum3F & f, const float4x4 & t, DrawArgs & args) {
	if (_pointModel)
		_pointModel->DrawFrustumCulled(f, t, args);
}


bool PointCloudModel::iModelAsyncTick(Gpu::Device & device, AsyncInitInterface * async, Units::Time_MicroSecondULL & limit) {
	// start time measurement
	Stopwatch stopwatch;
	stopwatch.Start();

	const Array3D<PointCloudBase::Cell> & grid = _pointCloud->Grid();
	if (_models.Length() == 0) {
		_models.Reallocate(grid.Lengths());
	}

	while (stopwatch.ElapsedMicroseconds() < limit) {
		if (async->Counter.Index < _models.Length()) {
			size_t index = async->Counter.Index++;
			const PointCloud::Cell * cell = grid.PlainView().begin() + index;
			if (cell->Size()) {
				Reference<QuadModel> & model = *(_models.PlainView().begin() + index);
				model = new QuadModel(const_cast<QuadVertex *>(cell->begin()), cell->Size(), _pointSize);
				model->Init(device);
			}
		}
		else {
			_boundingBox = _pointCloud->Area();
			_boundingSphere = _boundingBox.BoundingSphere();
			initFrustumCullingHierarchy(true);
			if (_pointModel)
				_pointModel->Init(device);
			_inited = true;
			limit -= stopwatch.ElapsedMicroseconds();
			return true;
		}
	}

	limit -= stopwatch.ElapsedMicroseconds();
	return false;
}


void PointCloudModel::PointSize(f32 pointSize) {
	_pointSize = pointSize;
	for (Reference<QuadModel> & model : _models.PlainView())
		model->PointSize(_pointSize);
}
