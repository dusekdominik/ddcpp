#include <DD/Debug.h>
#include <DD/Las.h>
#include <DD/Math.h>
#include <DD/Profiler.h>
#include <DD/FileSystem/FileReader.h>

#include <regex>
#include <fstream>

using namespace DD;
using namespace DD::Las;

/************************************************************************/
/* Las::Math                                                            */
/************************************************************************/

f64 Las::Math::ArcLengthOfMeridian(f64 phi) {
	f64 alpha, beta, gamma, delta, epsilon, n;

	/* Precalculate n */
	n = (MajorAxis - MinorAxis) / (MajorAxis + MinorAxis);
	/* Precalculate alpha */
	alpha = ((MajorAxis + MinorAxis) / 2.0) * (1.0 + (pow(n, 2.0) / 4.0) + (pow(n, 4.0) / 64.0));
	/* Precalculate beta */
	beta = (-3.0 * n / 2.0) + (9.0 * pow(n, 3.0) / 16.0) + (-3.0 * pow(n, 5.0) / 32.0);
	/* Precalculate gamma */
	gamma = (15.0 * pow(n, 2.0) / 16.0) + (-15.0 * pow(n, 4.0) / 32.0);
	/* Precalculate delta */
	delta = (-35.0 * pow(n, 3.0) / 48.0) + (105.0 * pow(n, 5.0) / 256.0);
	/* Precalculate epsilon */
	epsilon = (315.0 * pow(n, 4.0) / 512.0);

	/* Now calculate the sum of the series and return */
	f64 result = phi;
	result += (beta * sin(2.0 * phi));
	result += (gamma * sin(4.0 * phi));
	result += (delta * sin(6.0 * phi));
	result += (epsilon * sin(8.0 * phi));
	result *= alpha;
	return result;
}

f64 Las::Math::FootpointLatitude(f64 y) {
	f64 y_, alpha_, beta_, gamma_, delta_, epsilon_, n;
	n = (MajorAxis - MinorAxis) / (MajorAxis + MinorAxis);
	alpha_ = ((MajorAxis + MinorAxis) / 2.0) * (1 + (pow(n, 2.0) / 4) + (pow(n, 4.0) / 64));
	y_ = y / alpha_;
	beta_ = (3.0 * n / 2.0) + (-27.0 * pow(n, 3.0) / 32.0) + (269.0 * pow(n, 5.0) / 512.0);
	gamma_ = (21.0 * pow(n, 2.0) / 16.0) + (-55.0 * pow(n, 4.0) / 32.0);
	delta_ = (151.0 * pow(n, 3.0) / 96.0) + (-417.0 * pow(n, 5.0) / 128.0);
	epsilon_ = (1097.0 * pow(n, 4.0) / 512.0);
	return  y_ +
		(beta_ * sin(2.0 * y_)) +
		(gamma_ * sin(4.0 * y_)) +
		(delta_ * sin(6.0 * y_)) +
		(epsilon_ * sin(8.0 * y_));
}

void Las::Math::MapLatLonToXY(f64 phi, f64 lambda, f64 lambda0, f64 & x, f64 & y) {
	f64 N, nu2, ep2, t, t2, l;
	f64 l3coef, l4coef, l5coef, l6coef, l7coef, l8coef;
	f64 tmp;

	ep2 = (pow(MajorAxis, 2.0) - pow(MinorAxis, 2.0)) / pow(MinorAxis, 2.0);
	nu2 = ep2 * pow(::cos(phi), 2.0);
	N = pow(MajorAxis, 2.0) / (MinorAxis * sqrt(1 + nu2));
	t = tan(phi);
	t2 = t * t;
	tmp = (t2 * t2 * t2) - pow(t, 6.0);
	l = lambda - lambda0;

	/* Precalculate coefficients for l**n in the equations belowso a normal human being can read the expressions for easting and northing -- l**1 and l**2 have coefficients of 1.0 */
	l3coef = 1.0 - t2 + nu2;
	l4coef = 5.0 - t2 + 9 * nu2 + 4.0 * (nu2 * nu2);
	l5coef = 5.0 - 18.0 * t2 + (t2 * t2) + 14.0 * nu2 - 58.0 * t2 * nu2;
	l6coef = 61.0 - 58.0 * t2 + (t2 * t2) + 270.0 * nu2 - 330.0 * t2 * nu2;
	l7coef = 61.0 - 479.0 * t2 + 179.0 * (t2 * t2) - (t2 * t2 * t2);
	l8coef = 1385.0 - 3111.0 * t2 + 543.0 * (t2 * t2) - (t2 * t2 * t2);

	/* Calculate easting (x) */
	x = N * ::cos(phi) * l
		+ (N / 6.0 * pow(::cos(phi), 3.0) * l3coef * pow(l, 3.0))
		+ (N / 120.0 * pow(::cos(phi), 5.0) * l5coef * pow(l, 5.0))
		+ (N / 5040.0 * pow(::cos(phi), 7.0) * l7coef * pow(l, 7.0));

	/* Calculate northing (y) */
	y = ArcLengthOfMeridian(phi)
		+ (t / 2.0 * N * pow(::cos(phi), 2.0) * pow(l, 2.0))
		+ (t / 24.0 * N * pow(::cos(phi), 4.0) * l4coef * pow(l, 4.0))
		+ (t / 720.0 * N * pow(::cos(phi), 6.0) * l6coef * pow(l, 6.0))
		+ (t / 40320.0 * N * pow(::cos(phi), 8.0) * l8coef * pow(l, 8.0));
}

void Las::Math::MapXYToLatLon(f64 x, f64 y, f64 lambda0, f64 & lat, f64 & lon) {
	f64 phif, Nf, Nfpow, nuf2, ep2, tf, tf2, tf4, cf;
	f64 x1frac, x2frac, x3frac, x4frac, x5frac, x6frac, x7frac, x8frac;
	f64 x2poly, x3poly, x4poly, x5poly, x6poly, x7poly, x8poly;

	phif = FootpointLatitude(y);
	ep2 = (pow(MajorAxis, 2.0) - pow(MinorAxis, 2.0)) / pow(MinorAxis, 2.0);
	cf = ::cos(phif);
	nuf2 = ep2 * pow(cf, 2.0);
	Nf = pow(MajorAxis, 2.0) / (MinorAxis * sqrt(1 + nuf2));
	Nfpow = Nf;
	tf = tan(phif);
	tf2 = tf * tf;
	tf4 = tf2 * tf2;

	/* Precalculate fractional coefficients for x**n in the equations below to simplify the expressions for latitude and longitude. */
	x1frac = 1.0 / (Nfpow * cf);
	Nfpow *= Nf;   /* now equals Nf**2) */
	x2frac = tf / (2.0 * Nfpow);
	Nfpow *= Nf;   /* now equals Nf**3) */
	x3frac = 1.0 / (6.0 * Nfpow * cf);
	Nfpow *= Nf;   /* now equals Nf**4) */
	x4frac = tf / (24.0 * Nfpow);
	Nfpow *= Nf;   /* now equals Nf**5) */
	x5frac = 1.0 / (120.0 * Nfpow * cf);
	Nfpow *= Nf;   /* now equals Nf**6) */
	x6frac = tf / (720.0 * Nfpow);
	Nfpow *= Nf;   /* now equals Nf**7) */
	x7frac = 1.0 / (5040.0 * Nfpow * cf);
	Nfpow *= Nf;   /* now equals Nf**8) */
	x8frac = tf / (40320.0 * Nfpow);

	/* Precalculate polynomial coefficients for x**n. -- x**1 does not have a polynomial coefficient. */
	x2poly = -1.0 - nuf2;
	x3poly = -1.0 - 2 * tf2 - nuf2;
	x4poly = 5.0 + 3.0 * tf2 + 6.0 * nuf2 - 6.0 * tf2 * nuf2 - 3.0 * (nuf2 *nuf2) - 9.0 * tf2 * (nuf2 * nuf2);
	x5poly = 5.0 + 28.0 * tf2 + 24.0 * tf4 + 6.0 * nuf2 + 8.0 * tf2 * nuf2;
	x6poly = -61.0 - 90.0 * tf2 - 45.0 * tf4 - 107.0 * nuf2 + 162.0 * tf2 * nuf2;
	x7poly = -61.0 - 662.0 * tf2 - 1320.0 * tf4 - 720.0 * (tf4 * tf2);
	x8poly = 1385.0 + 3633.0 * tf2 + 4095.0 * tf4 + 1575 * (tf4 * tf2);

	/* Calculate latitude */
	lat = phif + x2frac * x2poly * (x * x)
		+ x4frac * x4poly * pow(x, 4.0)
		+ x6frac * x6poly * pow(x, 6.0)
		+ x8frac * x8poly * pow(x, 8.0);

	/* Calculate longitude */
	lon = lambda0 + x1frac * x
		+ x3frac * x3poly * pow(x, 3.0)
		+ x5frac * x5poly * pow(x, 5.0)
		+ x7frac * x7poly * pow(x, 7.0);
}

u32 Las::Math::LatLonToUTMXY(f64 lat, f64 lon, u32 zone, f64 & x, f64 & y) {
	MapLatLonToXY(lat, lon, UTMCentralMeridian(zone), x, y);
	/* Adjust easting and northing for UTM system. */
	x *= UTMScaleFactor + 500000.0;
	y *= y * UTMScaleFactor;
	if (y < 0.0) y += 10000000.0;
	return zone;
}

void Las::Math::UTMXYToLatLon(f64 x, f64 y, u32 zone, bool southhemi, f64 & lat, f64 & lon) {
	x -= 500000.0;
	x /= UTMScaleFactor;

	/* If in southern hemisphere, adjust y accordingly. */
	if (southhemi) y -= 10000000.0;
	y /= UTMScaleFactor;

	f64 cmeridian = UTMCentralMeridian(zone);
	MapXYToLatLon(x, y, cmeridian, lat, lon);
}

f64 Las::Math::DistanceAproximation(f64 xlat, f64 xlon, f64 ylat, f64 ylon) {
	// Constants
	const f64 RADIUS = 6371000; // m

	// Conversion
	xlat *= DD::Math::ConstantsD::Deg2Rad;
	xlon *= DD::Math::ConstantsD::Deg2Rad;
	ylat *= DD::Math::ConstantsD::Deg2Rad;
	ylon *= DD::Math::ConstantsD::Deg2Rad;

	// Difference
	f64 diffLat = (ylat - xlat);
	f64 diffLon = (ylon - xlon);

	// Distance
	f64 a = sin(diffLat / 2) * sin(diffLat / 2) + sin(diffLon / 2) * sin(diffLon / 2) * ::cos(xlat) * ::cos(ylat);
	f64 c = 2 * atan2(sqrt(a), sqrt(1 - a));
	return RADIUS * c;
}

f64 Las::Math::UTMCentralMeridian(u32 zone) {
	return DD::Math::Deg2Rad(-183.0 + (zone * 6.0));
}

/************************************************************************/
/* IIO                                                                  */
/************************************************************************/
#define ISTREAM (*reinterpret_cast<FileSystem::FileReader8 *>(_istream))
#define OSTREAM (*reinterpret_cast<std::ofstream *>(_stream))


template<size_t SIZE>
inline void IO<SIZE>::Load(istream_t & _istream) { ISTREAM.Read(_data, SIZE); }
template<size_t SIZE>
inline void IO<SIZE>::Save(ostream_t & _stream) const { OSTREAM.write((char*)_data, SIZE); }

/************************************************************************/
/* Las::Header                                                          */
/************************************************************************/
void Header::InitSignature() {
	FileSignature()[0] = 'L';
	FileSignature()[1] = 'A';
	FileSignature()[2] = 'S';
	FileSignature()[3] = 'F';
}

bool Header::CheckSignature() const {
	return
		FileSignature()[0] == 'L' &&
		FileSignature()[1] == 'A' &&
		FileSignature()[2] == 'S' &&
		FileSignature()[3] == 'F';
}

/************************************************************************/
/* Header                                                               */
/************************************************************************/

void Header::Load(istream_t & stream) { IO::Load(stream); }

void Header::Save(ostream_t & stream) const { IO::Save(stream); }


/************************************************************************/
/* VariableLengthRecord                                                 */
/************************************************************************/
void VariableLengthRecord::Load(istream_t & _istream) {
	IO::Load(_istream);
	_data.Reallocate(RecordLengthAfterHeader());
	ISTREAM.Read(Data(), RecordLengthAfterHeader());
}

void VariableLengthRecord::Save(ostream_t & _stream) const {
	IO::Save(_stream);
	OSTREAM.write((char*)Data(), RecordLengthAfterHeader());
}

/************************************************************************/
/* Point                                                                */
/************************************************************************/
#define POINT_LOAD(format) void Point<Formats::format>::Load(istream_t & stream) { IO::Load(stream); }
#define POINT_SAVE(format) void Point<Formats::format>::Save(ostream_t & stream) const { IO::Save(stream); }
#define POINT_IO(format) POINT_LOAD(format) POINT_SAVE(format)
DD_XLIST(POINT_IO, Format0, Format1, Format2, Format3, FormatDD)



/************************************************************************/
/* Las::File                                                            */
/************************************************************************/
bool File::checkFormat() {
	switch (_header.PointDataFormatID()) {
		//case Formats::Format0: _iPoint = new Point<Formats::Format0>; return true;
		case Formats::Format1: _iPoint = new Point<Formats::Format1>; return true;
		case Formats::Format2: _iPoint = new Point<Formats::Format2>; return true;
		case Formats::Format3: _iPoint = new Point<Formats::Format3>; return true;
		case Formats::FormatDD: _iPoint = new Point<Formats::FormatDD>; return true;
		default: return false;
	}
}

/************************************************************************/
/* Las::FileReader                                                      */
/************************************************************************/
DD::Simulation::PointCloud FileReader::LoadPointCloud(const float3 & cellDefinition) {
	DD_PROFILE_SCOPE_SIMPLE("Loading ptcloud");
	DD::Simulation::PointCloud ptCloud;
	switch (GetHeader().PointDataFormatID()) {
		case Formats::FormatDD: { // DD format optimized for fast loading
			const AABox3D & boxD = GetBBox();
			AABox3F boxF((float3)boxD.Min, (float3)boxD.Max);
			ptCloud = DD::Simulation::PointCloud(boxF, cellDefinition);
			Point<Formats::FormatDD> & handler = static_cast<Point<Formats::FormatDD> &>(*(GetPoint()));
			DD::Simulation::QuadVertex qv;
			while (ReadPoint()) {
				qv.Position = { handler.X(), handler.Y(), handler.Z() };
				qv.Color = handler.ColorAndClassification();
				ptCloud->AddPoint(qv);
			}
		} break;
		default: {
			ptCloud = DD::Simulation::PointCloud(
				AABox3F(float3(0.f), (float3)GetDimensions().xzy),
				cellDefinition
			);
			DD::Simulation::QuadVertex qv;
			auto gpsBBox = GetGPSBBox();
			auto meterScale = GetDimensions();
			double3 gpsScale = gpsBBox.Max - gpsBBox.Min;
			const double3 & scale = meterScale.xzy / gpsScale.xzy;
			while (ReadPoint()) {
				V3<u16> color(GetColor());
				const double3 & gps = GetGPS();
				qv.Position = scale * (gps - gpsBBox.Min).xzy;
				qv.Color = Color::FromSRGB(color.x / 65535.f, color.y / 65535.f, color.z / 65535.f);
				qv.Color.a = (u8)GetClassification();
				ptCloud->AddPoint(qv);
			}
		} break;
	}
	return ptCloud;
}

bool FileReader::ReadPoint() {
	if (_counter++ < _header.NumberOfPointRecords())
	{
		_iPoint->Load(_istream);
		return true;
	}
	return false;
}

bool FileReader::Open(const wchar_t * path) {
	enum { GeoAsciiParamsTag = 34737 };
	_istream = new FileSystem::FileReader8(path);
	//ISTREAM = FileSystem::FileReader(path); //.open(path, std::ios::binary);
	if (!ISTREAM.Reset())
		return false;

	_header.Load(_istream);

	if (!_header.CheckSignature())
		return false;

	if (!checkFormat())
		return false;

	InitRecords();

	for (u32 i = 0; i < _header.NumberOfVariableLengthRecords(); ++i)	{
		_records[i].Load(_istream);
		if (_records[i].RecordId() == GeoAsciiParamsTag) {
			std::string data((char*)_records[i].Data());
			std::regex rgx(".*UTM zone (\\d+)([NS]).*");
			std::smatch match;
			if (std::regex_search(data, match, rgx)) {
				_UTM.Mercator = (unsigned)stoi(match[1]);
				_UTM.South = match[1] == "S";
			}
		}
	}

	ISTREAM.Seek(_header.PointOffset());

	return true;
}

static void UTM2GPS(f64 utmX, f64 utmY, f64 & gpsX, f64 & gpsY, unsigned zone, bool south) {
	Las::Math::UTMXYToLatLon(utmX, utmY, zone, south, gpsX, gpsY);
	gpsX *= DD::Math::ConstantsD::Rad2Deg;
	gpsY *= DD::Math::ConstantsD::Rad2Deg;
}

double3 FileReader::GetGPS() const {
	auto X = ((f64)_iPoint->GetPosition().x * _header.ScaleX() + _header.OffsetX());
	auto Y = ((f64)_iPoint->GetPosition().y * _header.ScaleY() + _header.OffsetY());
	UTM2GPS(X, Y, X, Y, _UTM.Mercator, _UTM.South);
	auto Z = ((f64)_iPoint->GetPosition().z * _header.ScaleZ() + _header.OffsetZ());
	return double3(X, Y, Z);
}

AABox3D FileReader::GetBBox() const {
	AABox3D bbox;
	bbox.Min.x = _header.MinX();
	bbox.Min.y = _header.MinY();
	bbox.Min.z = _header.MinZ();
	bbox.Max.x = _header.MaxX();
	bbox.Max.y = _header.MaxY();
	bbox.Max.z = _header.MaxZ();
	return bbox;
}

AABox3D FileReader::GetGPSBBox() const {
	AABox3D box = GetBBox();
	switch (GetHeader().PointDataFormatID()) {
		case Formats::FormatDD:
			return box;
		default:
			UTM2GPS(box.Min.x, box.Min.y, box.Min.x, box.Min.y, _UTM.Mercator, _UTM.South);
			UTM2GPS(box.Max.x, box.Max.y, box.Max.x, box.Max.y, _UTM.Mercator, _UTM.South);
			return box;
	}
}

double3 FileReader::GetDimensions() const {
	switch (GetHeader().PointDataFormatID()) {
		case Formats::FormatDD: {
			AABox3D bbox = GetBBox();
			return bbox.Max - bbox.Min;
		}
		default: {
			AABox3D bbox = GetGPSBBox();
			return double3(
				Las::Math::DistanceAproximation(bbox.Min.x, bbox.Min.y, bbox.Max.x, bbox.Min.y),
				Las::Math::DistanceAproximation(bbox.Min.x, bbox.Min.y, bbox.Min.x, bbox.Max.y),
				DD::Math::Abs(bbox.Min.z - bbox.Max.z)
			);
		}
	}

}


DD::Las::FileReader::FileReader()
	: File()
	, _counter(0)
	, _istream(0)
{}

FileReader::~FileReader() {
	if (_istream)
		delete (FileSystem::FileReader8 *)_istream;
}


/************************************************************************/
/* Las::FileWriter                                                      */
/************************************************************************/
void FileWriter::SetPoint(f32 x, f32 y, f32 z, u32 color) {
	if (_iPoint) switch (_iPoint->GetFormat())
	{
		case Formats::FormatDD:
		{
			Point<Formats::FormatDD> & format = reinterpret_cast<Point<Formats::FormatDD> &>(*_iPoint);
			format.X() = x;
			format.Y() = y;
			format.Z() = z;
			format.ColorAndClassification() = color;
			break;
		}
		default:
			throw "Not supported.";
	}
}

void FileWriter::SetPoint(
	double3 position, V3<u16> color, Classification classification
) {
	_iPoint->SetColor(color);
	_iPoint->SetPosition(position);
	_iPoint->SetClassification(classification);
}

bool FileWriter::Open(const wchar_t * file) {
	OSTREAM.open(file, std::ios::binary);
	return OSTREAM.good();
}

void FileWriter::Close() {
	OSTREAM.close();
	_init = false;
}

void DD::Las::FileWriter::WritePointCloud(
	const Simulation::PointCloud & pointCloud, Formats format, IProgress * progress
) {
	_header.InitSignature();
	_header.PointDataFormatID() = format;
	_header.NumberOfVariableLengthRecords() = 0;
	_header.NumberOfPointRecords() = (u32)pointCloud->Size();
	_header.MinX() = pointCloud->Area().Min.x;
	_header.MinY() = pointCloud->Area().Min.y;
	_header.MinZ() = pointCloud->Area().Min.z;
	_header.MaxX() = pointCloud->Area().Max.x;
	_header.MaxY() = pointCloud->Area().Max.y;
	_header.MaxZ() = pointCloud->Area().Max.z;
	_header.ScaleX() = 1.0;
	_header.ScaleY() = 1.0;
	_header.ScaleZ() = 1.0;
	_header.OffsetX() = 0.0;
	_header.OffsetY() = 0.0;
	_header.OffsetZ() = 0.0;

	if (Init()) {
		IProgress::ScopeCounter32 counter(progress, (i32)pointCloud->Size());
		for (const DD::Simulation::PointCloud::Cell & cell : pointCloud->Grid().PlainView()) {
			for (const DD::Simulation::QuadVertex & point : cell) {
				SetPoint(point.Position.x, point.Position.y, point.Position.z, point.Color);
				WritePoint();
				++counter;
			}
		}
	}
}

bool FileWriter::Init() {
	if (_header.CheckSignature()) {
		if (checkFormat()) {
			OSTREAM.seekp(Header::IO_SIZE);
			for (size_t i = 0; i < _header.NumberOfVariableLengthRecords(); ++i)
				_records[i].Save(_stream);
			_header.PointOffset() = (u32)OSTREAM.tellp();
			OSTREAM.seekp(0);
			_header.Save(_stream);
			OSTREAM.seekp(_header.PointOffset());
			return _init = true;
		}
		return false;
	}
	return false;
}

FileWriter::FileWriter()
	: _counter(0)
	, _init(0)
	, _stream(new std::ofstream())
{}

FileWriter::~FileWriter() {
	delete _stream;
}

DD::Simulation::PointCloud DD::Las::LoadPointCloud(const String & path, const float3 & cell, IProgress * progress) {
	DD::Simulation::PointCloud result;
	Las::FileReader reader;

	// tracking
	IProgress::Scope scope(progress, [&reader](f32 & result) {
		result = (f32)reader.GetReadCount() / reader.GetTotalCount();
	});

	if (reader.Open(path)) {
		const double3 & implicit = reader.GetDimensions();
		float3 dimensions(
			cell.x == 0.f ? (f32)implicit.x : cell.x,
			cell.y == 0.f ? (f32)implicit.y : cell.y,
			cell.z == 0.f ? (f32)implicit.z : cell.z
		);

		result = reader.LoadPointCloud(dimensions);
	}
	// end measurement
	if (progress)
		progress->End();

	return result;
}

bool DD::Las::SavePointCloud(
	Text::StringView16 path,
	const DD::Simulation::PointCloud & cloud,
	Formats format,
	IProgress * progress
) {
	Las::FileWriter writer;
	if (writer.Open(path.begin())) {
		writer.WritePointCloud(cloud, format, progress);
		writer.Close();
		return true;
	}

	return false;
}
