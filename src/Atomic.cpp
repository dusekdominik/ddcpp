#include <DD/Concurrency/Atomic.h>
#include <DD/Macros.h>

#include <intrin.h>
#include <intrin0.h>
#include <Windows.h>

DD_TODO_STAMP(
	"Move functionality to standard types to be working in following way:\n"
	"  DD::i32 normalSomething;          // for std behavior \n"
	"  volatile DD::i32 atomicSomething; // for atomic behavior"
);

/************************************************************************/
/* i32                                                                  */
/************************************************************************/

DD::i32 DD::Concurrency::Atomic::i32::Add(volatile DD::i32 & base, DD::i32 value) {
	return _InlineInterlockedAdd((volatile long*)&base, value);
}

DD::i32 DD::Concurrency::Atomic::i32::And(volatile DD::i32 & base, DD::i32 value) {
	return _InterlockedAnd((volatile long*)&base, value);
}

DD::i32 DD::Concurrency::Atomic::i32::Or(volatile DD::i32 & base, DD::i32 value) {
	return _InterlockedOr((volatile long*)&base, value);
}

DD::i32 DD::Concurrency::Atomic::i32::Xor(volatile DD::i32 & base, DD::i32 value) {
	return _InterlockedXor((volatile long*)&base, value);
}

DD::i32 DD::Concurrency::Atomic::i32::Inc(volatile DD::i32 & base) {
	return _InterlockedIncrement((volatile long*)&base);
}

DD::i32 DD::Concurrency::Atomic::i32::Dec(volatile DD::i32 & base) {
	return _InterlockedDecrement((volatile long*)&base);
}

DD::i32 DD::Concurrency::Atomic::i32::Load(const volatile DD::i32 & base) {
	DD::i32 value = base;
	_ReadWriteBarrier();
	return value;
}

DD::i32 DD::Concurrency::Atomic::i32::Exchange(volatile DD::i32 & base, DD::i32 value) {
	return _InterlockedExchange((volatile long *)&base, (long)value);
}

DD::i32 DD::Concurrency::Atomic::i32::CompareExchange(volatile DD::i32 & base, DD::i32 value, DD::i32 compare) {
	return _InterlockedCompareExchange((volatile long *)&base, (long)value, (long)compare);
}


/************************************************************************/
/* u32                                                                  */
/************************************************************************/

DD::u32 DD::Concurrency::Atomic::u32::Add(volatile DD::u32 & base, DD::u32 value) {
	return (DD::u32)_InlineInterlockedAdd((volatile long *)& base, value);
}

DD::u32 DD::Concurrency::Atomic::u32::And(volatile DD::u32 & base, DD::u32 value) {
	return (DD::u32)_InterlockedAnd((volatile long *)& base, value);
}

DD::u32 DD::Concurrency::Atomic::u32::Or(volatile DD::u32 & base, DD::u32 value) {
	return (DD::u32)_InterlockedOr((volatile long *)& base, value);
}

DD::u32 DD::Concurrency::Atomic::u32::Xor(volatile DD::u32 & base, DD::u32 value) {
	return (DD::u32)_InterlockedXor((volatile long *)&base, value);
}

DD::u32 DD::Concurrency::Atomic::u32::Inc(volatile DD::u32 & base) {
	return (DD::u32)_InterlockedIncrement((volatile long *)&base);
}

DD::u32 DD::Concurrency::Atomic::u32::Dec(volatile DD::u32 & base) {
	return (DD::u32)_InterlockedDecrement((volatile long *)& base);
}

DD::u32 DD::Concurrency::Atomic::u32::Load(const volatile DD::u32 & base) {
	DD::u32 value = base;
	_ReadWriteBarrier();
	return value;
}

DD::u32 DD::Concurrency::Atomic::u32::Exchange(volatile DD::u32 & base, DD::u32 value) {
	return (DD::u32)_InterlockedExchange((volatile long *)& base, (long)value);
}

DD::u32 DD::Concurrency::Atomic::u32::CompareExchange(volatile DD::u32 & value, DD::u32 newValue, DD::u32 oldValue) {
	return (DD::u32)_InterlockedCompareExchange((volatile long *)&value, (long)newValue, (long)oldValue);
}



/************************************************************************/
/* i64                                                                  */
/************************************************************************/

DD::i64 DD::Concurrency::Atomic::i64::Add(volatile DD::i64 & base, DD::i64 value) {
	return _InlineInterlockedAdd64((volatile __int64*)&base, value);
}

DD::i64 DD::Concurrency::Atomic::i64::And(volatile DD::i64 & base, DD::i64 value) {
	return _InterlockedAnd64((volatile __int64*)&base, value);
}

DD::i64 DD::Concurrency::Atomic::i64::Or(volatile DD::i64 & base, DD::i64 value) {
	return _InterlockedOr64((volatile __int64*)&base, value);
}

DD::i64 DD::Concurrency::Atomic::i64::Xor(volatile DD::i64 & base, DD::i64 value) {
	return _InterlockedXor64((volatile __int64*)&base, value);
}

DD::i64 DD::Concurrency::Atomic::i64::Inc(volatile DD::i64 & base) {
	return _InterlockedIncrement64((volatile __int64*)&base);
}

DD::i64 DD::Concurrency::Atomic::i64::Dec(volatile DD::i64 & base) {
	return _InterlockedDecrement64((volatile __int64*)&base);
}

DD::i64 DD::Concurrency::Atomic::i64::Load(const volatile DD::i64 & base) {
	DD::i64 value = base;
	_ReadWriteBarrier();
	return value;
}

DD::i64 DD::Concurrency::Atomic::i64::Exchange(volatile DD::i64 & base, DD::i64 value) {
	return _InterlockedExchange64((__int64 volatile *)&base, value);
}

DD::i64 DD::Concurrency::Atomic::i64::CompareExchange(volatile DD::i64 & value, DD::i64 newValue, DD::i64 oldValue) {
	return _InterlockedCompareExchange64((__int64 volatile *)&value, newValue, oldValue);
}


/************************************************************************/
/* u64                                                                  */
/************************************************************************/

DD::u64 DD::Concurrency::Atomic::u64::Add(volatile DD::u64 & base, DD::u64 value) {
	return (DD::u64)_InlineInterlockedAdd64((volatile __int64*)&base, value);
}

DD::u64 DD::Concurrency::Atomic::u64::And(volatile DD::u64 & base, DD::u64 value) {
	return (DD::u64)_InterlockedAnd((volatile unsigned __int64*)&base, value);
}

DD::u64 DD::Concurrency::Atomic::u64::Or(volatile DD::u64 & base, DD::u64 value) {
	return (DD::u64)_InterlockedOr((volatile unsigned __int64*)&base, value);
}

DD::u64 DD::Concurrency::Atomic::u64::Xor(volatile DD::u64 & base, DD::u64 value) {
	return (DD::u64)_InterlockedXor((volatile unsigned __int64*)&base, value);
}

DD::u64 DD::Concurrency::Atomic::u64::Inc(volatile DD::u64 & base) {
	return (DD::u64)_InterlockedIncrement((volatile unsigned __int64*)&base);
}

DD::u64 DD::Concurrency::Atomic::u64::Dec(volatile DD::u64 & base) {
	return (DD::u64)_InterlockedDecrement((volatile unsigned __int64*)&base);
}

DD::u64 DD::Concurrency::Atomic::u64::Load(const volatile DD::u64 & base) {
	DD::u64 value = base;
	_ReadWriteBarrier();
	return value;
}

DD::u64 DD::Concurrency::Atomic::u64::Exchange(volatile DD::u64 & base, DD::u64 value) {
	return (DD::u64)_InterlockedExchange64((__int64 volatile *)&base, value);
}

DD::u64 DD::Concurrency::Atomic::u64::CompareExchange(volatile DD::u64 & value, DD::u64 newValue, DD::u64 oldValue) {
	return (DD::u64)_InterlockedCompareExchange64((__int64 volatile *)&value, newValue, oldValue);
}

