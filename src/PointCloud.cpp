#include <DD/Debug.h>
#include <DD/Profiler.h>
#include <DD/Sequencer/Driver.h>
#include <DD/Simulation/PointCloud.h>


DD_TODO_STAMP("Make selection methods const.")
DD_TODO_STAMP("Profile from ptclouds.")

using namespace DD;
using namespace DD::Simulation;

template<typename T>
inline static void Swap(T & l, T & r) { T tmp = l; l = r; r = tmp; }


void PointCloudBase::InitGrid() {
	_cellCount = _gridRange / _cellDimensions;
	_cellCount.x = _cellDimensions.x == 0 ? 1.f : Math::Ceil(_cellCount.x);
	_cellCount.y = _cellDimensions.y == 0 ? 1.f : Math::Ceil(_cellCount.y);
	_cellCount.z = _cellDimensions.z == 0 ? 1.f : Math::Ceil(_cellCount.z);
	_grid.Reallocate({ (size_t)_cellCount.x, (size_t)_cellCount.z, (size_t)_cellCount.y });
	_gridLock = true;
}


PointCloud PointCloudBase::RegridArea(
	const AABox3F & area,
	const float3 & cell,
	IProgress * progress /*= 0*/
) const {
	DD_PROFILE_SCOPE("PointCloud/RegridArea");
	// init point cloud
	PointCloud ref(area, cell);
	PointCloudBase & ptCloud = *ref;

	// register callback for progress observation
	IProgress::Scope scope(
		progress,
		[this, &ptCloud](f32 & value) { value = (f32)ptCloud._pointCount / _pointCount; }
	);


	ptCloud.InitGrid();
	// copy content
	for (const Cell & segment : _grid.ToView().PlainView()) {
		for (const QuadVertex & point : segment) {
			ptCloud.AddPoint(point);
		}
	}

	return ref;
}


Array<PointCloudBase::Cell*> PointCloudBase::SelectCells(const AABox3F & area) {
	// compute size of cell buffer
	float3 minimumXZ = positionToIndices(area.Min);
	float3 maximumXZ = positionToIndices(area.Max);
	i32 minimumX = (i32)Math::Saturate(floor(minimumXZ.x), 0.f, (_cellCount.x));
	i32 minimumY = (i32)Math::Saturate(floor(minimumXZ.y), 0.f, (_cellCount.z));
	i32 minimumZ = (i32)Math::Saturate(floor(minimumXZ.z), 0.f, (_cellCount.y));
	i32 maximumX = (i32)Math::Saturate(ceil(maximumXZ.x),  0.f, (_cellCount.x));
	i32 maximumY = (i32)Math::Saturate(ceil(maximumXZ.y),  0.f, (_cellCount.z));
	i32 maximumZ = (i32)Math::Saturate(ceil(maximumXZ.z),  0.f, (_cellCount.y));
	size_t surface = (size_t)((maximumX - minimumX) * (maximumY - minimumY) * (maximumZ - minimumZ));
	Array<Cell *> result(surface);

	// copy data
	for (size_t z = minimumZ, k = 0; z < maximumZ; ++z) {
		for (size_t y = minimumY; y < maximumY; ++y) {
			for (size_t x = minimumX; x < maximumX; ++x) {
				result[k++] = &_grid[{z, y, x}];
			}
		}
	}

	return result;
}


Array<QuadVertex*> PointCloudBase::SelectPoints(const AABox3F & area) {
	Array<Cell *> selection = SelectCells(area);
	size_t count = 0, i = 0;
	// count size for buffer
	for (Cell * segment : selection) {
		count += segment->Size();
	}
	// create buffer
	Array<QuadVertex *> buffer(count);
	// copy data
	for (Cell * segment : selection) {
		for (QuadVertex & v : *segment) {
			if (area.Contains(v.Position)) {
				buffer[i++] = &v;
			}
		}
	}
	// narrow buffer
	buffer.ResizeUnsafe(i);

	return buffer;
}


Array<QuadVertex*> PointCloudBase::SelectPoints(const Polygon<f32> & profile) {
	return SelectPoints(profile, _gridArea.Min.y, _gridArea.Max.y);
}


Array<QuadVertex*> PointCloudBase::SelectPoints(const Polygon<f32> & profile, f32 minHeight, f32 maxHeight) {
	// compute AABB selection
	const AABox2F & horizontalBox = profile.AABox();
	AABox3F area = {
		float3(horizontalBox.Min.x, minHeight, horizontalBox.Min.y),
		float3(horizontalBox.Max.x, maxHeight, horizontalBox.Max.y)
	};
	Array<QuadVertex*> aabbSelection = SelectPoints(area);
	Array<QuadVertex*> cvxSelection(aabbSelection.Length());
	size_t index = 0;
	// copy if in cvx
	for (QuadVertex * v : aabbSelection)
		if (profile.IsIn(v->Position.xz))
			cvxSelection[index++] = v;

	// narrow
	cvxSelection.ResizeUnsafe(index);

	return cvxSelection;
}


void PointCloudBase::RemovePoints(const Polygon<f32> & profile) {
	// compute AABB selection to select cells
	const AABox2F & horizontalBox = profile.AABox();
	AABox3F area = {
		float3(horizontalBox.Min.x, Area().Min.y, horizontalBox.Min.y),
		float3(horizontalBox.Max.x, Area().Max.y, horizontalBox.Max.y)
	};
	// select bounds for cells
	float3 minimumXZ = positionToIndices(area.Min);
	float3 maximumXZ = positionToIndices(area.Max);
	i32 minimumX = (i32)Math::Saturate(floor(minimumXZ.x), 0.f, (_cellCount.x));
	i32 minimumY = (i32)Math::Saturate(floor(minimumXZ.y), 0.f, (_cellCount.z));
	i32 minimumZ = (i32)Math::Saturate(floor(minimumXZ.z), 0.f, (_cellCount.y));
	i32 maximumX = (i32)Math::Saturate(ceil(maximumXZ.x),  0.f, (_cellCount.x));
	i32 maximumY = (i32)Math::Saturate(ceil(maximumXZ.y),  0.f, (_cellCount.z));
	i32 maximumZ = (i32)Math::Saturate(ceil(maximumXZ.z),  0.f, (_cellCount.y));

	size_t numberOfRemovedVertices = 0;
	// copy data
	List<QuadVertex> verticesBuffer;
	for (size_t x = minimumX; x < maximumX; ++x) {
		for (size_t z = minimumZ; z < maximumZ; ++z) {
			// declare xz boundaries
			f32 xbegin = Area().Min.x + x * CellDimensions().x;
			f32 xend = xbegin + CellDimensions().x;
			f32 zbegin = Area().Min.z + z * CellDimensions().y;
			f32 zend = zbegin + CellDimensions().y;

			// if y-profile is in the polygon, clear whole cells
			// otherwise check point by point
			if (
				profile.IsIn(float2(xbegin, zbegin)) &&
				profile.IsIn(float2(xend, zbegin))   &&
				profile.IsIn(float2(xend, zend))     &&
				profile.IsIn(float2(xbegin, zend))
			) {
				for (size_t y = minimumY; y < maximumY; ++y) {
					Cell & cell = _grid[{z, y, x}];
					numberOfRemovedVertices += cell.Size();
					cell.Clear();
				}
			}
			else {
				for (size_t y = minimumY; y < maximumY; ++y) {
					Cell & cell = _grid[{z, y, x}];
					// copy not eliminated vertices into the buffer
					for (const QuadVertex & qv : cell) {
						if (!profile.IsIn(qv.Position.xz)) {
							verticesBuffer.Add(qv);
						}
					}
					numberOfRemovedVertices += cell.Size() - verticesBuffer.Size();
					// swap buffers
					cell.Swap(verticesBuffer);
					verticesBuffer.Clear();
				}
			}
		}
	}

	// fix number of points
	_pointCount -= numberOfRemovedVertices;
}


Array2D<f32> PointCloudBase::CreateHeightMap(size_t w, size_t h) {
	Array2D<f32> heightMap = Array2D<f32>::Uniform({ w, h }, UNFILLED);
	float2 dims = _gridRange.xz / float2((f32)w, (f32)h);
	// fore ach cell of defined granularity
	for (size_t row = 0; row < h; ++row) {
		for (size_t col = 0; col < w; ++col) {
			// make selection of points
			float2 begin = _gridArea.Min.xz + dims * float2((f32)col, (f32)row);
			float2 end = _gridArea.Min.xz + dims * float2((f32)(col + 1), (f32)(row + 1));
			f32 & height = heightMap[row][col];
			AABox3F area(
				float3(begin.x, _gridArea.Min.y, begin.y),
				float3(end.x, _gridArea.Min.y, end.y)
			);
			Array<Cell *> selection = SelectCells(area);
			// parse points to height map
			for (Cell * cell : selection) {
				for (QuadVertex & v : *cell) {
					// skip irrelevant
					if (v.Position.x < begin.x)
						continue;
					if (v.Position.z < begin.y)
						continue;
					if (v.Position.x > end.x)
						continue;
					if (v.Position.z > end.y)
						continue;
					// parse relevant
					if (v.Position.y > height)
						height = v.Position.y;
				}
			}
		}
	}

	return heightMap;
}


bool PointCloudBase::AddPoint(const QuadVertex & vertex) {
	float3 shiftedPosition = vertex.Position + _offset;
	if (!IsPointInGrid(shiftedPosition))
		return false;

	float3 indices = positionToIndices(shiftedPosition);
	Cell & cell = _grid[{
		Math::Min((size_t)indices.z, _grid.Lengths()[0] - 1),
		Math::Min((size_t)indices.y, _grid.Lengths()[1] - 1),
		Math::Min((size_t)indices.x, _grid.Lengths()[2] - 1)
	}];
	cell.Add(vertex);
	cell.Last().Position = shiftedPosition;
	++_pointCount;
	return true;
}


void PointCloudBase::AddNoise(f32 x, f32 y, f32 z) {
	auto randomNoise = Sequencer::ParallelSequence(
		Random::SequencerF(_pointCount, x * -0.5f, x * 0.5f),
		Random::SequencerF(_pointCount, y * -0.5f, y * 0.5f),
		Random::SequencerF(_pointCount, z * -0.5f, z * 0.5f)
	).Select([](const Collections::Bin<f32, f32, f32> & pack) { return float3(pack[0], pack[1], pack[2]); });

	randomNoise.Reset();

	for (Cell & cell : _grid.PlainView()) {
		for (QuadVertex & vertex : cell) {
			vertex.Position += randomNoise.Get();
			randomNoise.Next();
		}
	}
}


void PointCloudBase::CreateHeightProfile(float2 begin, float2 end, f32 width, Array<f32> & store) {
	typedef Polygon<f32, 4> square_t;

	store.FillUniform(UNFILLED);

	size_t granularity = store.Length();
	float2 direction = end - begin;
	f32 stepSize = Math::Norm(direction);
	float2 crossDirection = float2(direction.y, -direction.x) * (0.5f * width / stepSize);
	float2 tBegin = begin + crossDirection;
	float2 bBegin = begin - crossDirection;


	for (size_t step = 0; step < granularity; ++step) {
		f32 & height = store[step];
		f32 begin = (f32)step / granularity;
		f32 end = (f32)(step + 1) / granularity;
		float2 lt = tBegin + direction * begin;
		float2 rt = tBegin + direction * end;
		float2 lb = bBegin + direction * begin;
		float2 rb = bBegin + direction * end;
		square_t square(lt, rt, rb, lb);
		Array<QuadVertex *> selection = SelectPoints(square);
		for (QuadVertex * qv : selection) {
			if (qv->Position.y > height) {
				height = qv->Position.y;
			}
		}
	}
}


Array<f32> PointCloudBase::CreateHeightProfile(float2 begin, float2 end, f32 width, size_t granularity) {
	Array<f32> store(granularity);
	CreateHeightProfile(begin, end, width, store);
	return store;
}


void PointCloudBase::CreateNormalizedHeightProfile(float2 begin, float2 end, f32 width, Array<f32> & store) {
	CreateHeightProfile(begin, end, width, store);
	f32 min = Math::ConstantsF::Max, max = Math::ConstantsF::Min, range;

	// get extremes
	for (f32 height : store) {
		if (height == Math::ConstantsF::Min)
			continue;
		if (min > height)
			min = height;
		if (max < height)
			max = height;
	}
	range = max - min;

	for (f32 & height : store) {
		// skip unfilled
		if (height == Math::ConstantsF::Min)
			continue;
		// normalize
		height = (height - min) / range;
	}
}


Array<f32> PointCloudBase::CreateNormalizedHeightProfile(float2 begin, float2 end, f32 width, size_t granularity) {
	Array<f32> store= Array<f32>::Uniform(granularity, Math::ConstantsF::Min);
	CreateNormalizedHeightProfile(begin, end, width, store);
	return store;
}


PointCloud PointCloudBase::CreateHeightSlice(const cvx_t & area, f32 heightBegin, f32 heightEnd) {
	// make xz selection
	Array<QuadVertex *> selection = SelectPoints(area, heightBegin, heightEnd);
	// create storage
	AABox2F bbox = area.AABox();
	// lhl
	AABox3F aabb(
		float3(bbox.Min.x, heightBegin, bbox.Min.y),
		float3(bbox.Max.x, heightEnd, bbox.Max.y)
	);
	PointCloud ptCloudRef(aabb);
	PointCloudBase & ptCloud = *ptCloudRef;
	Cell & cell = ptCloud._grid[{0, 0, 0}];
	cell.Reserve(selection.Length());
	// make y selection
	for (QuadVertex * qv : selection) {
		cell.AppendUnsafe(*qv);
	}
	// set size
	ptCloud._pointCount = cell.Size();

	return ptCloudRef;
}


PointCloud PointCloudBase::CreateHeightSlice(f32 heightBegin, f32 heightEnd) {
	Polygon<f32, 4> square(
		float2(_gridArea.Min.x, _gridArea.Min.z),
		float2(_gridArea.Min.x, _gridArea.Max.z),
		float2(_gridArea.Max.x, _gridArea.Max.z),
		float2(_gridArea.Max.x, _gridArea.Min.z)
	);
	return CreateHeightSlice(square, heightBegin, heightEnd);
}


#define SWAP(x) ::Swap(ptCloud.x, x);

void PointCloudBase::Swap(PointCloudBase & ptCloud) {
	_grid.Swap(ptCloud._grid);
	DD_XLIST(SWAP, _pointCount, _gridArea, _gridRange, _pointCount, _cellDimensions, _cellCount, _offset);
}


void PointCloudBase::RecountPoints() {
	_pointCount = 0;
	for (const Cell & cell : _grid.ToView().PlainView())
		_pointCount += cell.Size();
}


PointCloudBase::PointCloudBase()
	: _gridLock(0)
	, _pointCount(0)
	, _pointLock(0)
	, _offset(0.f)
{}


PointCloudBase::PointCloudBase(const AABox3F & area)
	: PointCloudBase(area, area.Size())
{}


PointCloudBase::PointCloudBase(const AABox3F & area, const float3 & cellSize)
	: PointCloudBase()
{
	Area(area);
	CellDimensions(cellSize);
	InitGrid();
}


float3 PointCloudBase::positionToIndices(const float3 & position) const {
	return (_cellCount * (position - _gridArea.Min) / _gridRange).xzy;
}


PointCloud PointCloudBase::RegridArea(
	const float3 & cell,
	IProgress * progress /*= 0*/
) const {
	return RegridArea(_gridArea, cell, progress);
}


Array2D<f32> PointCloudBase::CreateHeightMap() {
	return CreateHeightMap((size_t)_cellCount.x, (size_t)_cellCount.y);
}


PointCloudBase::PointCloudBase(const ArrayView1D<QuadVertex *> & vertices)
	: PointCloudBase()
{
	AABox3F aabb;
	List<QuadVertex> cell(vertices.Length());

	for (const QuadVertex * v : vertices) {
		cell.Add(*v);
		aabb.Include(cell.Last().Position);
	}

	Area(aabb);
	CellDimensions(aabb.Max - aabb.Min);
	InitGrid();
	Grid()[{0, 0, 0}].Swap(cell);
	_pointCount = vertices.Length();
}


PointCloudBase::PointCloudBase(const ArrayView1D<const PointCloudBase *> & clouds)
	: PointCloudBase()
{
	size_t count = 0;
	AABox3F aabb;
	// count points
	for (const PointCloudBase * cloud : clouds) {
		count += cloud->Size();
	}
	// create storage
	List<QuadVertex> newCell(count);
	// copy vertices to storage
	for (const PointCloudBase * cloud : clouds) {
		for (const Cell & cell : cloud->Grid().ToView().PlainView()) {
			for (const QuadVertex & v : cell) {
				newCell.Add(v);
				aabb.Include(v.Position);
			}
		}
	}
	// declare single-cell-grid properties
	Area(aabb);
	CellDimensions(aabb.Max - aabb.Min);
	InitGrid();
	Grid()[{0, 0, 0}].Swap(newCell);
	_pointCount = count;
}


PointCloud::PointCloud(const AABox3F & area)
	: Reference(new PointCloudBase(area))
{}


PointCloud::PointCloud(const AABox3F & area, const float3 & cell)
	: Reference(new PointCloudBase(area, cell))
{}


PointCloud::PointCloud(const ArrayView1D<QuadVertex *> & vertices)
	: Reference(new PointCloudBase(vertices))
{}


PointCloud::PointCloud(const ArrayView1D<const PointCloudBase *> & clouds)
	: Reference(new PointCloudBase(clouds))
{}


bool PointCloudBase::CellSequencerBase::next() {
	if (++X.Iterator < X.End) {
		return true;
	}
	else if (++Y.Iterator < Y.End) {
		X.Iterator = X.Begin;
		return true;
	}
	else if (++Z.Iterator < Z.End) {
		X.Iterator = X.Begin;
		Y.Iterator = Y.Begin;
		return true;
	}

	return false;
}


bool PointCloudBase::CellSequencerBase::reset() {
	if (Size) {
		X.Iterator = X.Begin;
		Y.Iterator = Y.Begin;
		Z.Iterator = Z.Begin;

		return Size > 0;
	}

	return false;
}


PointCloudBase::CellSequencerBase::CellSequencerBase(const PointCloudBase * cloud, const AABox3F & area) {
	float3 minimumXZ = cloud->positionToIndices(area.Min);
	float3 maximumXZ = cloud->positionToIndices(area.Max);
	X.Begin = (i32)Math::Saturate(Math::Floor(minimumXZ.x), 0.f, (cloud->_cellCount.x - 1.f));
	Y.Begin = (i32)Math::Saturate(Math::Floor(minimumXZ.y), 0.f, (cloud->_cellCount.z - 1.f));
	Z.Begin = (i32)Math::Saturate(Math::Floor(minimumXZ.z), 0.f, (cloud->_cellCount.y - 1.f));
	X.End = (i32)Math::Saturate(Math::Ceil(maximumXZ.x), 0.f, (cloud->_cellCount.x));
	Y.End = (i32)Math::Saturate(Math::Ceil(maximumXZ.y), 0.f, (cloud->_cellCount.z));
	Z.End = (i32)Math::Saturate(Math::Ceil(maximumXZ.z), 0.f, (cloud->_cellCount.y));
	Size = (X.End - X.Begin) * (Y.End - Y.Begin) * (Z.End - Z.Begin);
}


PointCloudBase::CellSequencerBase::CellSequencerBase(const PointCloudBase * source)
	: X{ size_t(0), size_t(0), (size_t)source->_cellCount.x }
	, Y{ size_t(0), size_t(0), (size_t)source->_cellCount.y }
	, Z{ size_t(0), size_t(0), (size_t)source->_cellCount.z }
{
	Size = (X.End - X.Begin) * (Y.End - Y.Begin) * (Z.End - Z.Begin);
}
