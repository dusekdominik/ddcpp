#include <DD/Simulation/MultiBillboardModel.h>

using namespace DD;
using namespace DD::Simulation;

void MultiBillboardModel::iModelInit(Gpu::Device & device) {
	_device = device;
	for (billbord_r model : _models) if (model->Size()) {
		model->iModelInit(device);
	}
}


void MultiBillboardModel::iModelDraw(DrawArgs & args) {
	Reinit(_device);
	for (billbord_r & model : _models) {
		if (model->Size()) {
			model->Draw(args);
		}
	}
}


void MultiBillboardModel::iModelDrawCulled(
	const Frustum3F & frustum, const float4x4 & transform, DrawArgs & args
) {
	Reinit(_device);
	for (billbord_r model : _models) if (model->Size()) {
		if (model->Intersects(frustum, transform)) {
			model->Draw(args);
		}
	}
}


size_t MultiBillboardModel::GetItem(size_t item) {
	return _icons[item].IconType;
}


void MultiBillboardModel::SetItem(size_t item, size_t iconType) {
	Item & i = _icons[item];
	if (i.IconType != iconType) {
		billbord_r oldModel = _models[i.IconType];
		billbord_r newModel = _models[iconType];

		oldModel->Remove(i.ModelIndex);
		for (Item & icon : _icons) {
			if (icon.IconType == i.IconType && icon.ModelIndex > i.ModelIndex) {
				--(icon.ModelIndex);
			}
		}
		newModel->Add(i.Position);

		i.ModelIndex = newModel->Size() - 1;
		i.IconType = iconType;

		_reinit = true;
	}
}


size_t MultiBillboardModel::AddItem(size_t icon, const float3 & position) {
	size_t index = _icons.Size();
	_icons.Add(Item{ icon, index, _models[icon]->Size(), position });
	_models[icon]->Add(position);
	_reinit = true;
	return index;
}


void MultiBillboardModel::AddIconClass(const Icon & icon) {
	_models.Add(new BillboardModel(icon));
}


void MultiBillboardModel::Clear() {
	for (Reference<BillboardModel> & model : _models) {
		model->Clear();
	}
	_reinit = true;
}
