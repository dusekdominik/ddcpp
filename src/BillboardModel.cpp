#include <DD/Gpu/Devices.h>
#include <DD/Simulation/BillboardModel.h>
#include <DD/Simulation/EmptyVertex.h>

#define QV_STRUCTURED_BUFFER 1

using namespace DD;
using namespace DD::Simulation;

Reference<Gpu::PixelShader> BillboardModel::_PIXEL_SHADER;
Reference<Gpu::VertexShader> BillboardModel::_VERTEX_SHADER;

void BillboardModel::iModelInit(Gpu::Device & device) {
	_device = device;
	if (_PIXEL_SHADER == nullptr) {
		_PIXEL_SHADER = device->GetPixelShader(L"PSTexture");
		_VERTEX_SHADER = device->GetVertexShader<EmptyVertex>(L"VSBillboard");
	}

	if  (_texture == nullptr)
		_texture = device->GetTexture(_icon.Path);

	_cb.Init(device);
	double3 center(0.0, 0.0, 0.0);
	double distance = 0.0;
	for (const vertex_t & q : _points) {
		center.x += q.Position.x;
		center.y += q.Position.y;
		center.z += q.Position.z;
	}
	center /= (double)_points.Size();
	for (const vertex_t & q : _points) {
		double x = center.x - q.Position.x;
		double y = center.y - q.Position.y;
		double z = center.z - q.Position.z;
		double radius2 = x*x + y*y + z*z;
		if (distance < radius2) {
			distance = radius2;
		}
	}
	distance = sqrt(distance);
	_boundingSphere.Center = center;
	_boundingSphere.Radius = (float)distance;

	_points.Init(device);
}

void BillboardModel::iModelDraw(DrawArgs & args) {
	_cb->BillboardSize = _icon.Dimensions;
	_cb.Bind2VS();
	_cb.Update();
	_PIXEL_SHADER->Set();
	_VERTEX_SHADER->Set();
	_texture->Bind2PS();
	_points.Bind2VS();
	_device->SetPrimitiveTopology(Gpu::Devices::PrimitiveTopologies::TriangleList);
	_device->Draw((u32)_points.Size() * 6);
}

BillboardModel::BillboardModel(const Icon & icon)
	: _icon(icon)
	, _points()
	, _texture()
{}

