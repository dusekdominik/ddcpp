#include <DD/Allocation.h>
#include <DD/Concurrency/Atomic.h>
#include <DD/Concurrency/ThreadPool.h>
#include <DD/Concurrency/ThreadSafeObject.h>
#include <DD/Debug.h>
#include <DD/Dialog.h>
#include <DD/Enum.h>
#include <DD/Environment.h>
#include <DD/FileSystem/Path.h>
#include <DD/Input/Keyboard.h>
#include <DD/List.h>
#include <DD/Properties.h>
#include <DD/Profiler.h>
#include <DD/Controls/Window.h>
#include <DD/Controls/ButtonControl.h>
#include <DD/Controls/CheckboxControl.h>
#include <DD/Controls/LabelControl.h>
#include <DD/Controls/TextControl.h>
#include <DD/Controls/OptionControl.h>
#include <DD/Controls/PanelControl.h>
#include <DD/Controls/GridControl.h>
#include <DD/Controls/SliderControl.h>
#include <DD/Controls/TabViewControl.h>
#include <DD/Unique.h>
#include <DD/XML.h>

#include <windows.h>
#include <typeinfo>
#undef CreateFile

#pragma warning(disable:4624) // destructor was implicitly defined as deleted
#pragma warning(disable:4060) // switch statement contains no 'case' or 'default' labels

#define DEFAULT_FONT L"Tahoma", 16
#define DEFAULT_PATH L"/DDCPP/DebugControls.xml"

using namespace DD;
using namespace DD::Units;

/** Override for references, extract key from reference as from referenced type. */
template<typename T>
struct BTreeTraitsKey<Reference<T>> {
	/** Type of key of stored item, automatically derived as return type from the KEY_TRAITS. */
	typedef typename BTreeTraitsKey<T>::key_t key_t;
	/** Standard trait, requires GetKey() method. */
	static key_t GetKey(const Reference<T> & item) { return BTreeTraitsKey<T>::GetKey(*item); }
};

/** Override for variables, default key is its name. */
template<>
struct BTreeTraitsKey<Debug::Variable> {
	/** Type of key of stored item, automatically derived as return type from the KEY_TRAITS. */
	using key_t = Text::StringView16;
	/** Standard trait, requires GetKey() method. */
	static Text::StringView16 GetKey(const Debug::Variable & item) { return item->Name(); }
};

/************************************************************************/
/* CommonVisitors                                                       */
/************************************************************************/
#pragma region visitors
/** Display to user what is content of variable. */
struct ToStringVisitor : public Debug::IVisitor<ToStringVisitor, DD::String&> {
	template<typename T>
	static void Visit(const T * var, DD::String &) { DD_WARNING("Undefined operation for typeName = {", typeid(T).name(), "}, varName = ", var->Category(), "/", var->Name(), "."); }
	static void Visit(const Debug::ButtonBase *, DD::String & buffer) { buffer.Append("Button"); }
	static void Visit(const Debug::PathBase * var, DD::String & buffer) { buffer.Append(var->Value()); }
	static void Visit(const Debug::AssertBase * var, DD::String & buffer) { buffer.AppendBatch(var->ValidTicks, " / ", (var->ValidTicks + var->InvalidTicks)); }
	static void Visit(const Debug::BooleanBase * var, DD::String & buffer) { buffer.Append(var->Value() ? "true" : "false"); }
	static void Visit(const Debug::IEnumBase * var, DD::String & buffer) { buffer.Append(var->Descriptor()->Value2String(var->NativeValue())); }
	static void Visit(const Debug::LogBase * var, DD::String & buffer) { buffer.Append(var->Ticks); }
	static void Visit(const Debug::OptionsBase * var, DD::String & buffer) { buffer.Append(var->Value()); }
	template<typename T>
	static void Visit(const Debug::RangeBase<T> * var, DD::String & buffer) { buffer.Append(var->Value()); }
	template<typename T>
	static void Visit(const Debug::TypeBase<T> * var, DD::String & buffer) { buffer.Append(var->Value()); }
};

/** Write to string content of variable. */
struct SerializeVisitor : public Debug::IVisitor<SerializeVisitor, DD::String&> {
	template<typename T>
	static void Visit(const T *, DD::String &) { DD_WARNING("Undefined operation for type:", typeid(T).name()); }
	static void Visit(const Debug::AssertBase * var, DD::String & buffer) { buffer.Append(var->Value()); }
	static void Visit(const Debug::BooleanBase * var, DD::String & buffer) { buffer.Append(var->Value() ? "true" : "false"); }
	static void Visit(const Debug::PathBase * var, DD::String & buffer) { buffer.Append(var->Value()); }
	static void Visit(const Debug::IEnumBase * var, DD::String & buffer) { buffer.Append(var->Descriptor()->Value2String(var->NativeValue())); }
	static void Visit(const Debug::LogBase * var, DD::String & buffer) { buffer.Append(var->Value()); }
	static void Visit(const Debug::OptionsBase * var, DD::String & buffer) { buffer.Append(var->Value()); }
	template<typename T>
	static void Visit(const Debug::RangeBase<T> * var, DD::String & buffer) { buffer.Append(var->Value()); }
	template<typename T>
	static void Visit(const Debug::TypeBase<T> * var, DD::String & buffer) { buffer.Append(var->Value()); }
};

/** Read content of variable from a string. */
struct DeserializeVisitor : public Debug::IVisitor<DeserializeVisitor, const DD::String &> {
	struct Option : public Debug::OptionsBase { void SavedValue(const DD::String & value) { _savedValue = value; } };
	static constexpr Debug::Sources SOURCE = Debug::Sources::SERIAL;
	template<typename T>
	static void Visit(T *, const DD::String &) { DD_WARNING("Undefined operation for type:", typeid(T).name()); }
	static void Visit(Debug::PathBase * var, const DD::String & buffer) { var->Value(buffer, SOURCE); }
	static void Visit(Debug::AssertBase * var, const DD::String & buffer) { u32 tmp; if (buffer.TryCast(tmp)) var->Value((Debug::Assert::Modes)tmp, SOURCE); }
	static void Visit(Debug::BooleanBase * var, const DD::String & buffer) { var->Value(buffer == "true", SOURCE); }
	static void Visit(Debug::IEnumBase * var, const DD::String & buffer) { var->NativeValue(var->Descriptor()->String2Value(buffer), SOURCE); }
	static void Visit(Debug::LogBase * var, const DD::String & buffer) { u32 tmp; if (buffer.TryCast(tmp)) var->Value((Debug::Log::Modes)tmp, SOURCE); }
	static void Visit(Debug::OptionsBase * var, const DD::String & buffer) { static_cast<Option *>(var)->SavedValue(buffer); }
	template<typename T>
	static void Visit(Debug::RangeBase<T> * var, const DD::String & buffer) { T tmp; if (buffer.TryCast(tmp)) var->Value(tmp, SOURCE); }
	template<typename T>
	static void Visit(Debug::TypeBase<T> * var, const DD::String & buffer) { T tmp; if (buffer.TryCast(tmp)) var->Value(tmp, SOURCE); }
};

#pragma endregion

/************************************************************************/
/* DebugCategoryImpl                                                    */
/************************************************************************/
#pragma region DebugCategoryImpl
Concurrency::CriticalSection GDebugVariablesLock;

struct DebugCategoryImpl
	: public Debug::ICategoryBase
{
	/** Name of the category. */
	StringLocal<32> _name;
	/** Storage of variables in the category. */
	BTree<Debug::Variable> _controls;

	/** Get all registered variables. */
	virtual void Enumerate(IAction<Debug::IVariableBase *> * lambda) override;
	/** Get name of the category. */
	virtual Text::StringView16 Name() const override { return _name; }
	/** Get control. */
	void Create(Debug::Variable);

	/** Constructor. */
	DebugCategoryImpl(const String & name) : _name(name) {}
};

inline void DebugCategoryImpl::Enumerate(IAction<Debug::IVariableBase *> * lambda) {
	Concurrency::CriticalSection::Scope section(GDebugVariablesLock);
	for (Debug::Variable & ctrl : _controls)
		lambda->Run(ctrl);
}

inline void DebugCategoryImpl::Create(Debug::Variable ctrl) {
	Concurrency::CriticalSection::Scope section(GDebugVariablesLock);
	Debug::Variable & var = _controls[ctrl->Name()];
	// temporary fix (reuse removed variables with count 1)
	DD_TODO_STAMP("Debug Variable - do a proper fix, probably remove the count=1 vars...");
	if (var == nullptr || var.Count() == 1) {
		var.Release();
		var = ctrl;
		OnVariableCreate(ctrl);
	}
	else {
		DD_WARNING("Variable already exists.");
	}
}
#pragma endregion

/************************************************************************/
/* DebugVariablesImpl                                                   */
/************************************************************************/
#pragma region DebugVariablesImpl
template<>
struct BTreeTraitsKey<Reference<DebugCategoryImpl>> {
	/** Standard trait, requires GetKey() method. */
	__forceinline static Text::StringView16 GetKey(const Reference<DebugCategoryImpl> & item) { return item->Name(); }
	/** Type of key of stored item, automatically derived as return type from the KEY_TRAITS. */
	typedef ::DD::String key_t;
};

struct DebugVariablesImpl : public Debug::IVariables {
	/** Singleton. */
	static DebugVariablesImpl * Instance() { static DebugVariablesImpl instance; return &instance; }
	/** Storage of categories. */
	BTree<Reference<DebugCategoryImpl>> _categories;
	/** Initialize gui variable manager. */
	virtual void InitGui() override;
	/** Get debug variable. */
	virtual void Create(Debug::IVariableBase *) override;
	/** Get variable if exists, nullptr otherwise. */
	virtual Debug::IVariableBase * Get(const Text::StringView16 & category, const Text::StringView16 & variable) override;
	/** Enumerate all categories of debug variables. */
	virtual void Enumerate(IAction<Debug::ICategoryBase *> * lambda) override;
	/** Enumerate all categories of debug variables. */
	virtual void Enumerate(IAction<Debug::IVariableBase *> * lambda) override;
	/** Acquire exclusive access to debug variables. */
	virtual void Lock() override { GDebugVariablesLock.Enter(); }
	/** Release exclusive access to debug variables. */
	virtual void UnLock() override { GDebugVariablesLock.Leave(); }
	/** Destructor. */
	~DebugVariablesImpl();
};

inline void DebugVariablesImpl::Create(Debug::IVariableBase * ctrl) {
	Concurrency::CriticalSection::Scope section(GDebugVariablesLock);
	Reference<DebugCategoryImpl> & category = _categories[ctrl->Category()];
	if (category.IsNull()) {
		category = new DebugCategoryImpl(ctrl->Category());
		category->OnVariableCreate += OnVariableCreate;
		OnCategoryCreate(category);
	}

	category->Create(ctrl);
}

inline void DebugVariablesImpl::Enumerate(IAction<Debug::ICategoryBase *> * lambda) {
	Concurrency::CriticalSection::Scope section(GDebugVariablesLock);
	for (auto & category : _categories)
		lambda->Run(category.Ptr());
}

inline void DebugVariablesImpl::Enumerate(IAction<Debug::IVariableBase *> * lambda) {
	Concurrency::CriticalSection::Scope section(GDebugVariablesLock);
	for (auto & category : _categories) {
		for (auto & variable : category->_controls) {
			lambda->Run(variable.Ptr());
		}
	}
}

inline DebugVariablesImpl::~DebugVariablesImpl() {
	// DD_DEBUG_LOG("-------------------Debug::Variables-------------------");
	// i32 globalIndex = -1, categoryIndex = -1;
	// for (auto & cat : _categories) {
	// 	i32 itemIndex = -1;
	// 	++categoryIndex;
	// 	for (auto & ctrl : cat->_controls) {
	// 		DD_DEBUG_LOGF("[{2,3}|{3,3}|{4,3}] {0}::{1}\n", cat->Name(), ctrl->Name(), ++globalIndex, categoryIndex, ++itemIndex);
	// 	}
	// }
	// DD_DEBUG_LOG("-------------------Debug::Variables-------------------");
}

inline DD::Debug::IVariableBase * DebugVariablesImpl::Get(const Text::StringView16 & category, const Text::StringView16 & variable) {
	// MT safe
	Concurrency::CriticalSection::Scope lock(GDebugVariablesLock);
	// is category present
	Reference<DebugCategoryImpl> * c = _categories.GetItem(category);
	if (c == nullptr)
		return nullptr;

	// is variable present
	Debug::Variable * v = (*c)->_controls.GetItem(variable);
	if (v == nullptr)
		return nullptr;

	// return variable
	return (*v).Ptr();
}
#pragma endregion

/************************************************************************/
/* Debug::Recorder                                                      */
/************************************************************************/
#if DD_DEBUG_CODE_ENABLED
#define RECORDER_LOG(description, ...) DD_LOGCF((__VA_ARGS__), PRINT, "DebugRecorder", description);
#else
#define RECORDER_LOG(...)
#endif

#pragma region CommandInfo
/** Description of command. */
struct CommandInfo {
	/** Category of variable. */
	DD::StringLocal<64> Category;
	/** Name of variable. */
	DD::StringLocal<64> Name;
	/** Value of variable. */
	DD::StringLocal<64> Value;
	/** Type of command. */
	DD::i32 Type;
	/** Time of command execution. */
	DD::u64 Time;
	/** Description of xml serialzation. */
	DD_XML_SERIALIZATION(
		ATTRIBUTE(Time),
		ATTRIBUTE(Type),
		ATTRIBUTE(Category),
		ATTRIBUTE(Name),
		ATTRIBUTE(Value)
	);
};
#pragma endregion

#pragma region Command
/** One command of Debug::Macro. */
struct CommandImpl {
	/** Is Command compiled. */
	bool Compiled = false;
	/** Information about command (serves for serialization). */
	CommandInfo Info;
	/** Action of command. */
	Action64<> Action = []() { DD_WARNING("Unset action."); };
	/**
	 * Compilation of command.
	 * Might be the variable is not available in time of creating command,
	 * so there is, additional compilation process.
	 */
	Action64<> Compile;
	/** Run command (and compile if necessary). */
	void Run();
	/** Serialization. */
	DD_XML_SERIALIZATION(ELEMENT(Info))
};

inline void CommandImpl::Run() {
	// compile command if necessary
	if (!Compiled) {
		Compile();
		Compiled = true;
	}
	// run command
	Action();
}

/** Container for parsing any data as function param. */
struct Container {
	/** Pointer to data. */
	const void * Ptr;
	/** Length of data. */
	size_t Length;
	/** Cast pointer to any data type. */
	template<typename T>
	const T & Cast() const { DD_DEBUG_ASSERT_SIMPLE(sizeof(T) == Length, "Invalid length of cast - ", sizeof(T), " != ", Length, "."); return *((T*)Ptr); }
	/** Constructor. */
	Container(const void * ptr, size_t length) : Ptr(ptr), Length(length) {}
	/** Generic constructor - parse any data type to the container. */
	template<typename T>
	explicit Container(const T & data) : Container(&data, sizeof(T)) {}
	/** Copy constructor. */
	Container(const Container & c) : Container(c.Ptr, c.Length) {}
	/** Zero constructor. */
	Container() {}
};

/** Container with an allocation. */
struct AllocatedContainer : public Container {
	/** Allocation for data storage. */
	DD::Allocation<8> Allocation;
	/** Load data to the allocation. */
	template<typename T>
	AllocatedContainer & operator=(const T & t) { Allocation = t; Length = sizeof(T); return *this; }
	/** Zero constructor - set container pointer to the Allocation. */
	AllocatedContainer() : Container(&Allocation, 0) {}
};

/** Data parsed to command creators. */
struct CommandVisitorInfo {
	/** Set into which command should be created. */
	DD::List<CommandImpl> & Commands;
	/** Index where new command should be placed. */
	size_t Index;
	/** Pointer to raw data. */
	Container Data;
};

/**
 * Creating commands is very expensive, so recorder is recording
 * just simple messages, and after recording messages are compiled
 * to commands.
 */
struct CommandMessage {
	/** Time stamp of message in nanoseconds (time since recording started). */
	DD::Units::Time_NanoSecondULL Time;
	/** Type of command. */
	DD::Debug::IRecorder::Commands CommandType;
	/** Variable which is recorded. */
	DD::Debug::IVariableBase * Variable;
	/** Allocation for storing value or additional info of command. */
	DD::Allocation<20> Value;
};
#pragma endregion

#pragma region Visitor
/** Convert value of variable to AllocatedContainer. */
struct ValueVisitor : public Debug::IVisitor<ValueVisitor, AllocatedContainer &> {
	template<typename T>
	static void Visit(const T *, AllocatedContainer &) { DD_WARNING("Undefined operation for type:", typeid(T).name()); }
	static void Visit(const Debug::AssertBase * source, AllocatedContainer & container) { container = source->Value(); }
	static void Visit(const Debug::LogBase * source, AllocatedContainer & container) { container = source->Value(); }
	static void Visit(const Debug::IEnumBase * source, AllocatedContainer & container) { container = source->NativeValue(); }
	static void Visit(const Debug::BooleanBase * source, AllocatedContainer & container) { container = (bool)source->Value(); }
	static void Visit(const Debug::ButtonBase *, AllocatedContainer &) {}
	template<typename T>
	static void Visit(const Debug::TypeBase<T> * source, AllocatedContainer & container) { container = source->Value(); }
	static void Visit(const Debug::TypeBase<DD::String> *, AllocatedContainer &) { DD_WARNING("Undefined operation"); }
	template<typename T>
	static void Visit(const Debug::RangeBase<T> * source, AllocatedContainer & container) { container = source->Value(); }
};

/** Helper method for decltype() derivation. */
template<typename T>
static constexpr T* ToPtr(const T &);
/** Data container for Value2StringVisitor.  */
struct Value2String { DD::String & String; void * Data; };
/** Visitor converting a value (@see Value2String::Data) to string (@see Value2String::String). */
struct Value2StringVisitor
	: public Debug::IVisitor<Value2StringVisitor, Value2String &>
{
	template<typename T>
	static void Visit(const T * var, Value2String & v2s) { typedef decltype(ToPtr(var->Value())) target_t;  v2s.String.Append(*(target_t)v2s.Data); }
	static void Visit(const Debug::IEnumBase * var, Value2String & v2s) { v2s.String.Append(*(DD::u64 *)v2s.Data); }
	static void Visit(const Debug::ButtonBase * var, Value2String & v2s) {  }
	static void Visit(const Debug::TypeBase<DD::String> * var, Value2String & v2s) { DD_WARNING("Problem"); }
};

/** Data container for String2ValueVisitor. */
struct String2Value { const DD::String & String; AllocatedContainer & Container; };
/** Visitor converting string (@see String2Value::String) to value (@see String2Value::Container).  */
struct String2ValueVisitor : public Debug::IVisitor<String2ValueVisitor, String2Value &> {
	template<typename T>
	static void Visit(const T *, String2Value &) { DD_WARNING("Undefined operation for type:", typeid(T).name()); }
	static void Visit(const Debug::AssertBase * source, String2Value & args) { DD::u64 tmp; args.String.TryCast(tmp); args.Container = (DD::u64)((DD::u32)tmp); }
	static void Visit(const Debug::LogBase * source, String2Value & args) { DD::u64 tmp; args.String.TryCast(tmp); args.Container = (DD::u64)((DD::u32)tmp); }
	static void Visit(const Debug::IEnumBase * source, String2Value & args) { args.Container = source->Descriptor()->String2Value(args.String); }
	static void Visit(const Debug::BooleanBase * source, String2Value & args) { args.Container = args.String == "true"; }
	static void Visit(const Debug::ButtonBase * source, String2Value & args) { }
	template<typename T>
	static void Visit(const Debug::TypeBase<T> * source, String2Value & args) { T tmp; args.String.TryCast(tmp); args.Container = tmp; }
	static void Visit(const Debug::TypeBase<DD::String> *, String2Value &) { DD_WARNING("Undefined operation"); }
	template<typename T>
	static void Visit(const Debug::RangeBase<T> * source, String2Value & args) { T tmp; args.String.TryCast(tmp); args.Container = tmp; }
};

/** Creating command from a variable. */
template<typename COMMAND>
struct CommandVisitor : public Debug::IVisitor<CommandVisitor<COMMAND>, const CommandVisitorInfo&> {
	template<typename T>
	static void Visit(T *, const CommandVisitorInfo &) { DD_WARNING("Undefined operation for type:", typeid(T).name()); }
	static void Visit(Debug::AssertBase * var, const CommandVisitorInfo & info) { COMMAND::Create(info, var, info.Data.Cast<Debug::Assert::Modes>()); }
	static void Visit(Debug::BooleanBase * var, const CommandVisitorInfo & info) { COMMAND::Create(info, var, info.Data.Cast<bool>()); }
	static void Visit(Debug::ButtonBase * var, const CommandVisitorInfo & info) { COMMAND::Create(info, var); }
	static void Visit(Debug::IEnumBase * var, const CommandVisitorInfo & info) { COMMAND::Create(info, var, info.Data.Cast<::DD::u64>()); }
	static void Visit(Debug::LogBase * var, const CommandVisitorInfo & info) { COMMAND::Create(info, var, info.Data.Cast<Debug::Log::Modes>()); }
	template<typename T>
	static void Visit(Debug::TypeBase<T> * var, const CommandVisitorInfo & info) { COMMAND::Create(info, var, info.Data.Cast<T>()); }
	template<typename T>
	static void Visit(Debug::RangeBase<T> * var, const CommandVisitorInfo & info) { COMMAND::Create(info, var, info.Data.Cast<T>()); }
};

/** Traits for CommandVisitor. */
struct CommandSET {
	template<typename VAR, typename VALUE>
	static void Create(const CommandVisitorInfo & info, VAR & var, VALUE & value) {
		//RECORDER_LOG("Log while debug recorder creating a command.", "Creating command Set(\"{0}/{1}\", \"{2}\")", var->Category(), var->Name(), value);
		info.Commands[info.Index].Action = [var, value]() {
			var->Value(value, Debug::Sources::GUI);
		};
	}
	static void Create(const CommandVisitorInfo & info, Debug::IEnumBase *& var, DD::u64 value) {
		//RECORDER_LOG("Log while debug recorder creating a command.", "Creating command Set(\"{0}/{1}\", \"{2}\")", var->Category(), var->Name(), value);
		info.Commands[info.Index].Action = [var, value]() {
			var->NativeValue(value, Debug::Sources::GUI);
		};
	}
	static void Create(const CommandVisitorInfo & info, Debug::ButtonBase *& var) {
		//RECORDER_LOG("Log while debug recorder creating a command.", "Creating command Set(\"{0}/{1}\", \"press\")", var->Category(), var->Name());
		info.Commands[info.Index].Action = [var]() {
			var->Press(Debug::Sources::GUI);
		};
	}
};

/** Traits for CommandVisitor. */
#define __DECLARE_COMMAND_FNC(name, operator)\
struct Command##name {\
	template<typename VAR, typename VALUE>\
	static void Create(const CommandVisitorInfo & info, VAR & var, VALUE & value) {\
		RECORDER_LOG("Log while debug recorder creating a command.", "Creating command " #name "(\"{0}/{1}\", \"{2}\")", var->Name(), var->Category(), value);\
		info.Commands[info.Index].Action = [var, value]() {\
			while(var->Value() operator value)\
				Concurrency::Thread::Sleep(10);\
		};\
	}\
	static void Create(const CommandVisitorInfo & info, Debug::IEnumBase *& var, DD::u64 value) {\
		RECORDER_LOG("Log while debug recorder creating a command.", "Creating command " #name "(\"{0}/{1}\", \"{2}\")", var->Name(), var->Category(), value);\
		info.Commands[info.Index].Action = [var, value]() {\
			var->NativeValue(value, Debug::Sources::GUI);\
		};\
	}\
	static void Create(const CommandVisitorInfo & info, Debug::ButtonBase *& var) {\
		RECORDER_LOG("Log while debug recorder creating a command.", "Creating command " #name "(\"{0}/{1}\", \"Press\")", var->Name(), var->Category());\
		info.Commands[info.Index].Action = [var] () {\
			bool value = false;\
			var->OnValueChanged += [&](Debug::IVariableBase *, Debug::Sources) { value = true; };\
			while (!value) {\
				Concurrency::Thread::Sleep(10);\
				MemoryBarrier();\
			}\
		};\
	}\
}
__DECLARE_COMMAND_FNC(EQUAL, == );
__DECLARE_COMMAND_FNC(NOT_EQUAL, != );
__DECLARE_COMMAND_FNC(LESS, < );
//__DECLARE_COMMAND_FNC(LESS_OR_EQUAL, <= );
//__DECLARE_COMMAND_FNC(GREATER, > );
//__DECLARE_COMMAND_FNC(GREATER_OR_EQUAL, >= );
#undef __DECLARE_COMMAND_FNC
#pragma endregion

#pragma region Macro
/** Implementation of Debug::IMacro */
struct RecordingImpl : public Debug::IRecorder::IRecording {
	/** List of commands. */
	DD::List<CommandImpl, 256> Commands;
protected: // IRecording
	virtual bool iMacroLoad(const DD::String & pathStr) override;
	virtual bool iMacroSave(const DD::String & pathStr) override;
public: // methods
	/** Compile commands which can precompiled. */
	void Precompile();
	/** Add command. */
	void AddVarCmd(Debug::IRecorder::Commands, Debug::Variable, DD::u64);
	/** Declare serialization with xml. */
	DD_XML_SERIALIZATION(ELEMENT(Commands));
};

bool RecordingImpl::iMacroLoad(const DD::String & pathStr) {
	FileSystem::Path path(pathStr);
	RECORDER_LOG("Macro::Load()", "Loading macro from {0}.", path.FullPath().ToString());
	if (path.IsFile()) {
		DD::Array<DD::i8> data = path.AsFile().Read();
		DD::XML::Reader reader;
		reader.ReadFormIterable(DD::ArrayView((wchar_t*)data.begin(), data.Length() / sizeof(wchar_t)));
		if (reader.Succeed()) {
			DD::XML::Deserialize(reader.GetDocument()->GetRoot(), *this);
			// set command actions
			Precompile();

			RECORDER_LOG("Macro::Load()", "Macro successfully loaded.", path.FullPath().ToString());
			return true;
		}
		else {
			DD_WARNING("Macro loaded from file ", path.FullPath().ToString(), " cannot be parsed as xml structure.");
			return false;
		}
	}
	else {
		DD_WARNING("Macro cannot be loaded from file ", path.FullPath().ToString(), ".");
		return false;
	}
}

bool RecordingImpl::iMacroSave(const DD::String & pathStr) {
	FileSystem::Path path(pathStr);
	RECORDER_LOG("Macro::Save()", "Saving macro to {0}.", (const String &)path.FullPath());
	if (path.IsFile() || path.CreateFile()) {
		DD::XML::Document document(L"DebugRecording");
		DD::XML::Serialize(*this, document->GetRoot());
		DD::String str = document->ToString();
		path.AsFile().Write(str.begin(), str.Length() * 2);
		return true;
	}
	else {
		DD_WARNING("Saving macro to ", path.FullPath().ToString(), " failed.");
		return false;
	}
}

#define __DECLARE_VAR_CMD_CASE(varCommand)\
case Debug::IRecorder::Commands::varCommand: {\
	size_t index = Commands.AddNaive();\
	Commands.Last().Info.Time = time;\
	CommandVisitor<Command##varCommand>::Run(var, { Commands, index, container });\
} break

void RecordingImpl::AddVarCmd(Debug::IRecorder::Commands type, Debug::Variable var, DD::u64 time) {
	AllocatedContainer container;
	ValueVisitor::Run(var, container);
	switch (type) {
		__DECLARE_VAR_CMD_CASE(SET);
		__DECLARE_VAR_CMD_CASE(EQUAL);
		__DECLARE_VAR_CMD_CASE(NOT_EQUAL);
		__DECLARE_VAR_CMD_CASE(LESS);
//		__DECLARE_VAR_CMD_CASE(LESS_OR_EQUAL);
//		__DECLARE_VAR_CMD_CASE(GREATER);
//		__DECLARE_VAR_CMD_CASE(GREATER_OR_EQUAL);
		default: {
			DD_WARNING("Undefined var command type.");
		} break;
	}
}
#undef __DECLARE_VAR_CMD_CASE

#define __DECLARE_VAR_CMD_CASE(varCommand)\
case Debug::IRecorder::Commands::varCommand: {\
	Debug::IVariableBase * variable = Debug::Variables()->Get(cmd.Info.Category, cmd.Info.Name);\
	if (variable) {\
		AllocatedContainer container;\
		String2Value sv = { cmd.Info.Value, container };\
		String2ValueVisitor::Run(variable, sv); \
		CommandVisitor<Command##varCommand>::Run(variable, { Commands, index, container });\
		cmd.Compiled = true;\
	}\
	else {\
		cmd.Compile = [index, this](){\
			Debug::IVariableBase * variable = 0;\
			for (size_t i = 0; i < 100; ++i) {\
				if (variable = Debug::Variables()->Get(Commands[index].Info.Category, Commands[index].Info.Name))\
					break;\
				Concurrency::Thread::Sleep(100);\
			}\
			if (variable) {\
				AllocatedContainer container;\
				String2Value sv = { Commands[index].Info.Value, container };\
				String2ValueVisitor::Run(variable, sv); \
				CommandVisitor<Command##varCommand>::Run(variable, { Commands, index, container });\
				Commands[index].Compiled = true;\
			}\
			else {\
				Commands[index].Action = [](){};\
			}\
		};\
	}\
} break;

void RecordingImpl::Precompile() {
	for (size_t index = 0; index < Commands.Size(); ++index) {
		CommandImpl & cmd = Commands[index];
		Debug::IRecorder::Commands type = (Debug::IRecorder::Commands) cmd.Info.Type;
		switch (type) {
			DD_XLIST(__DECLARE_VAR_CMD_CASE, SET, EQUAL, NOT_EQUAL, LESS/*, LESS_OR_EQUAL, GREATER, GREATER_OR_EQUAL*/);
			default: DD_WARNING("Undefined command (", (i32)type, ")."); break;
		}
	}
}
#undef __DECLARE_VAR_CMD_CASE

#pragma endregion

#pragma region Recorder
/** Implementatiorn of Debug::IRecorder. */
struct RecorderImpl : public Debug::IRecorder {
	/** State of recorder.  */
	enum States { Ready, Recording, Playing, Aborting } State;
	/** Settings of recorder. */
	union SettingsDesc {
		/** Settings for recording. */
		RecorderSettings Recorder;
		/** Settings for playing.  */
		PlayerSettings Player;
		/** Zero constructor. */
		SettingsDesc() {}
	} Settings;
	/** MT synchronization. */
	Concurrency::CriticalSection Section;
	/** Messages */
	List<CommandMessage> Messages;
	/** Timer for recording time-based records. */
	Stopwatch Watch;
	/** Delegate version of @see AddVariableLambda.  */
	Delegate<Debug::IVariableBase *> AddVariableDelegate;
	/** Recorder callback - if variable value changes and not filtered, save command. */
	Delegate<Debug::IVariableBase *, Debug::Sources> RecorderDelegates[32];
	/** Add recorder callback to debug variable if it is not filtered by settings. */
	Action<Debug::IVariableBase *> AddVariableLambda;
	/** Remove recorder callback from debug variable if it is not filtered by settings. */
	Action<Debug::IVariableBase *> RemoveVariableLambda;
public: // IRecorder
	virtual IRecording * Create() override { return new RecordingImpl(); }
	virtual bool Start(RecorderSettings &) override;
	virtual bool Stop() override;
	virtual bool Play(PlayerSettings &) override;
public: // lambda factory
	template<typename DEBUG_VAR> auto CreateRecorderLambda();
	template<> auto CreateRecorderLambda<Debug::ButtonBase>();
	template<> auto CreateRecorderLambda<Debug::IEnumBase>();
public: // constructors
	/** Zero constructor. */
	RecorderImpl();
};

template<typename DEBUG_VAR>
auto RecorderImpl::CreateRecorderLambda() {
	return [this](DEBUG_VAR * var, Debug::Sources source) {
		if (source & Settings.Recorder.Source) {
			RECORDER_LOG("Prints which debug variables are recorded.", "Recording set command: {2} \"{0}/{1}\"", var->Category(), var->Name(), Messages.Size());
			Concurrency::CriticalSection::Scope scope(Section);
			CommandMessage msg;
			msg.Time = (Settings.Recorder.SkipWaiting) ? 0_ns : Watch.ElapsedNanoseconds();
			msg.CommandType = Debug::IRecorder::Commands::SET;
			msg.Variable = var;
			msg.Value = var->Value();
			Messages.Add(msg);
		}
	};
}

template<>
auto RecorderImpl::CreateRecorderLambda<Debug::ButtonBase>() {
	return [this](Debug::ButtonBase * var, Debug::Sources source) {
		if (source & Settings.Recorder.Source) {
			RECORDER_LOG("Prints which debug variables are recorded.", "Recording set command: \"{0}/{1}\"", var->Category(), var->Name());
			Concurrency::CriticalSection::Scope scope(Section);
			CommandMessage msg;
			msg.Time = (Settings.Recorder.SkipWaiting) ? 0_ns : Watch.ElapsedNanoseconds();
			msg.CommandType = Debug::IRecorder::Commands::SET;
			msg.Variable = var;
			Messages.Add(msg);
		}
	};
}

template<>
auto RecorderImpl::CreateRecorderLambda<Debug::IEnumBase>() {
	return [this](Debug::IEnumBase * var, Debug::Sources source) {
		if (source & Settings.Recorder.Source) {
			RECORDER_LOG("Prints which debug variables are recorded.", "Recording set command: \"{0}/{1}\"", var->Category(), var->Name());
			Concurrency::CriticalSection::Scope scope(Section);
			CommandMessage msg;
			msg.Time = (Settings.Recorder.SkipWaiting) ? 0_ns : Watch.ElapsedNanoseconds();
			msg.CommandType = Debug::IRecorder::Commands::SET;
			msg.Variable = var;
			msg.Value = var->NativeValue();
			Messages.Add(msg);
		}
	};
}


bool RecorderImpl::Start(RecorderSettings & settings) {
	RECORDER_LOG("Recording of debug variables is trying to start.", "Recording started.");
	if (State == Ready) {
		Concurrency::CriticalSection::Scope scope(Section);
		State = Recording;
		Settings.Recorder = settings;
		// check settings
		DD_DEBUG_ASSERT(dynamic_cast<RecordingImpl*>(Settings.Recorder.Recording), "RecorderSettings check.");
		DD_DEBUG_ASSERT(Settings.Recorder.Filter, "RecorderSettings check.");
		// init recording
		Watch.Reset();
		Messages.Clear();
		Messages.Reserve(4096);
		Debug::Variables()->Lock();
		Debug::Variables()->OnVariableCreate += AddVariableDelegate;
		Debug::Variables()->Enumerate(AddVariableLambda);
		if (Settings.Recorder.LoadInitialState) {
			Action16<Debug::IVariableBase *> loadInit = [this](Debug::IVariableBase * var) {
				if ((var->Source() & Settings.Recorder.Source) && (Settings.Recorder.InitialFilter->Run(var))) {
					RecorderDelegates[(i32)var->Type()]->Run(var, Settings.Recorder.Source);
				}
			};
			Debug::Variables()->Enumerate(loadInit);
		}
		Watch.Start();
		Debug::Variables()->UnLock();
		// initialization succeeds
		return true;
	}
	DD_WARNING("Invalid state for call Debug::IRecorder::Start().");
	return false;
}

bool RecorderImpl::Stop() {
	RECORDER_LOG("Recording of debug variables starts.", "Recording finished.");
	switch (State) {
		case Recording: {
			Concurrency::CriticalSection::Scope scope(Section);
			State = Aborting;
			Debug::Variables()->Lock();
			Debug::Variables()->OnVariableCreate -= AddVariableDelegate;
			Debug::Variables()->Enumerate(RemoveVariableLambda);
			Watch.Stop();
			// convert msgs to cmds
			RecordingImpl * target = static_cast<RecordingImpl *>(Settings.Recorder.Recording);
			target->Commands.Clear();
			target->Commands.Reserve(Messages.Size());
			for (CommandMessage & msg : Messages) {
				auto & recording = target->Commands.Append();
				recording.Info.Category = msg.Variable->Category();
				recording.Info.Name = msg.Variable->Name();
				recording.Info.Time = msg.Time.Value;
				recording.Info.Type = (i32)msg.CommandType;
				Value2String v2s{ recording.Info.Value, &msg.Value };
				Value2StringVisitor::Run(msg.Variable, v2s);
			}
			Debug::Variables()->UnLock();
			State = Ready;
		} return true;
		case Playing: {
			State = Aborting;
			while (State == Aborting) {
				Concurrency::Thread::Sleep(10);
			}
		} return true;
	}

	DD_WARNING("Invalid state for call Debug::IRecorder::Stop().");
	return false;
}

bool RecorderImpl::Play(PlayerSettings & settings) {
	RECORDER_LOG("DD::Debug::Recorder()->Play();", "Playing of recorded macro started.");
	if (State == Ready) {
		State = Playing;
		Settings.Player = settings;
		// cast to playable type
		RecordingImpl * recording = dynamic_cast<RecordingImpl *>(Settings.Player.Recording);
		DD_ASSERT(recording);
		// iterate over commands
		Watch.Restart();
		for (auto & command : recording->Commands) {
			if (State == Aborting) {
				RECORDER_LOG("DD::Debug::Recorder()->Play();", "Playing was aborted.");
				State = Ready;
				return false;
			}
			while (!Settings.Player.SkipWaiting && Watch.ElapsedNanoseconds() < command.Info.Time);
			RECORDER_LOG("DD::Debug::Recorder()->Play();", "PlayTime = {1}, RecordTime = {0}, Diff = {2}.",
				/*0*/ command.Info.Time,
				/*1*/ Watch.ElapsedNanoseconds().Value,
				/*2*/ Watch.ElapsedNanoseconds().Value - command.Info.Time
			);
			command.Run();
		}

		RECORDER_LOG("DD::Debug::Recorder()->Play();", "Recording was successfully played.");
		State = Ready;
		return true;
	}

	DD_WARNING("Invalid state for call Debug::IRecorder::Play().");
	State = Ready;
	return false;
}

RecorderImpl::RecorderImpl() {
	RECORDER_LOG("Recording structure (for debug variables) has been initialized.", "Recording initialized.");

	AddVariableLambda = [this](Debug::IVariableBase * var) {
		if (var->Source() & Settings.Recorder.Source && Settings.Recorder.Filter->Run(var))
			var->OnValueChanged += RecorderDelegates[(i32)var->Type()];
	};
	RemoveVariableLambda = [this](Debug::IVariableBase * var) {
		if (var->Source() & Settings.Recorder.Source && Settings.Recorder.Filter->Run(var))
			var->OnValueChanged -= RecorderDelegates[(i32)var->Type()];
	};
	AddVariableDelegate = AddVariableLambda;
	RecorderDelegates[(i32)Debug::VariableTypes::f32] = CreateRecorderLambda<Debug::TypeBase<f32>>();
	RecorderDelegates[(i32)Debug::VariableTypes::f64] = CreateRecorderLambda<Debug::TypeBase<f64>>();
	RecorderDelegates[(i32)Debug::VariableTypes::i8] = CreateRecorderLambda<Debug::TypeBase<i8>>();
	RecorderDelegates[(i32)Debug::VariableTypes::i16] = CreateRecorderLambda<Debug::TypeBase<i16>>();
	RecorderDelegates[(i32)Debug::VariableTypes::i32] = CreateRecorderLambda<Debug::TypeBase<i32>>();
	RecorderDelegates[(i32)Debug::VariableTypes::i64] = CreateRecorderLambda<Debug::TypeBase<i64>>();
	RecorderDelegates[(i32)Debug::VariableTypes::u8]  = CreateRecorderLambda<Debug::TypeBase<u8>>();
	RecorderDelegates[(i32)Debug::VariableTypes::u16] = CreateRecorderLambda<Debug::TypeBase<u16>>();
	RecorderDelegates[(i32)Debug::VariableTypes::u32] = CreateRecorderLambda<Debug::TypeBase<u32>>();
	RecorderDelegates[(i32)Debug::VariableTypes::u64] = CreateRecorderLambda<Debug::TypeBase<u64>>();
	RecorderDelegates[(i32)Debug::VariableTypes::String] = [](DD::Debug::IVariableBase *, DD::Debug::Sources) {  };
	RecorderDelegates[(i32)Debug::VariableTypes::f64Range] = CreateRecorderLambda<Debug::RangeBase<f64>>();
	RecorderDelegates[(i32)Debug::VariableTypes::f32Range] = CreateRecorderLambda<Debug::RangeBase<f32>>();
	RecorderDelegates[(i32)Debug::VariableTypes:: i8Range] = CreateRecorderLambda<Debug::RangeBase< i8>>();
	RecorderDelegates[(i32)Debug::VariableTypes::i16Range] = CreateRecorderLambda<Debug::RangeBase<i16>>();
	RecorderDelegates[(i32)Debug::VariableTypes::i32Range] = CreateRecorderLambda<Debug::RangeBase<i32>>();
	RecorderDelegates[(i32)Debug::VariableTypes::i64Range] = CreateRecorderLambda<Debug::RangeBase<i64>>();
	RecorderDelegates[(i32)Debug::VariableTypes:: u8Range] = CreateRecorderLambda<Debug::RangeBase< u8>>();
	RecorderDelegates[(i32)Debug::VariableTypes::u16Range] = CreateRecorderLambda<Debug::RangeBase<u16>>();
	RecorderDelegates[(i32)Debug::VariableTypes::u32Range] = CreateRecorderLambda<Debug::RangeBase<u32>>();
	RecorderDelegates[(i32)Debug::VariableTypes::u64Range] = CreateRecorderLambda<Debug::RangeBase<u64>>();
	RecorderDelegates[(i32)Debug::VariableTypes::Boolean] = CreateRecorderLambda<Debug::BooleanBase>();
	RecorderDelegates[(i32)Debug::VariableTypes::Button] = CreateRecorderLambda<Debug::ButtonBase>();
	RecorderDelegates[(i32)Debug::VariableTypes::Enum] = CreateRecorderLambda<Debug::IEnumBase>();
	RecorderDelegates[(i32)Debug::VariableTypes::Assert] = CreateRecorderLambda<Debug::AssertBase>();
	RecorderDelegates[(i32)Debug::VariableTypes::Log] = CreateRecorderLambda<Debug::LogBase>();
}

Debug::IRecorder * Debug::Recorder() {
	static RecorderImpl instance;
	return &instance;
}
#pragma endregion

/************************************************************************/
/* DebugSerializerImpl                                                  */
/************************************************************************/
#if DD_DEBUG_CODE_ENABLED
#define SERIALIZER_LOG(...) DD_LOGCF((__VA_ARGS__), SILENT, "DebugSerializer")
#else
#define SERIALIZER_LOG(...)
#endif

#pragma region DebugSerializerImpl
/** Global variable serving for variable serialization. */
struct DebugSerializerImpl {
	/** Variable - value. */
	struct VariablePair {
		/** Is value tracked. */
		bool Tracked;
		/** Name of variable. */
		StringLocal<64> Name;
		/** Value of variable. */
		StringLocal<32> Value;
		/** Name of variable (BTree getter). */
		const DD::String & GetKey() const { return Name; }
		/** Serialization description. */
		DD_XML_SERIALIZATION(ATTRIBUTE(Name), ATTRIBUTE(Value), ATTRIBUTE(Tracked));
	};
	/** Category - variables */
	struct CategoryPair {
		/** Name of category. */
		StringLocal<64> Category;
		/** Variables of the category. */
		BTree<VariablePair> Variables;
		/** Name of category (BTree getter). */
		const DD::String & GetKey() const { return Category; }
	};
	/** Singleton. */
	static DebugSerializerImpl & Instance();
	/** Storage of categories. */
	BTree<CategoryPair> _categories;
	/** Tracker. */
	Delegate<Debug::IVariableBase *, Debug::Sources> _tracker;
	/**
	 * Event fired when a variable is (un)tracked.
	 * @param IVariableBase - variable which is (un)tracked.
	 * @param bool          - flag if variable is tracked or untracked.
	 */
	Event<Debug::IVariableBase *, bool> OnTrackerChanged;
	/**
	 * Event fired when a variable is (un)registered.
	 * @param IVariableBase - variable which is (un)registered.
	 * @param bool          - flag if variable is registerd or unregistered.
	 */
	Event<Debug::IVariableBase *, bool> OnSerializerChanged;
	/** Save into file. */
	void Save();
	/** Load from file */
	void Load();
	/** Remove serialization record if exits. */
	void Remove(Debug::Variable);
	/** Track variable value. */
	void Track(Debug::Variable);
	/** Untrack variable value. */
	void Untrack(Debug::Variable);
	/** Serialize control. */
	void Serialize(Debug::Variable);
	/** Deserialize control, if serialization record exists. */
	bool Deserialize(Debug::Variable);
	/** Check if given variable is registered. */
	bool IsRegistered(Debug::Variable);
	/** Is variable tracked. */
	bool IsTracked(Debug::Variable);
	/** Constructor. */
	DebugSerializerImpl(Debug::IVariables * variables);
};

inline DebugSerializerImpl & DebugSerializerImpl::Instance() {
	static DebugSerializerImpl serializer(DebugVariablesImpl::Instance());
	return serializer;
}

inline void DebugSerializerImpl::Save() {
	XML::Document document(L"DebugVariables");
	XML::Element & root = document->GetRoot();
	for (CategoryPair & category : _categories) {
		XML::Element & xmlCategory = root.DefineElement(L"Category");
		xmlCategory.DefineAttribute(L"Name", category.Category);
		for (VariablePair & variable : category.Variables) {
			XML::Serialize(variable, xmlCategory.DefineElement(L"Variable"));
		}
	}

	FileSystem::Path path(StringLocal<256>::From(FileSystem::Path::Documents().ToString(), "\\DDCPP\\debug.variables.xml"));
	if (!path.IsInvalid() && (path.IsFile() || path.CreateFile())) {
		DD::String str = document->ToString();
		path.AsFile().Write(str.begin(), str.Length() * 2);
	}
	else {
		DD_WARNING("Problem with saving file.");
	}
}

inline void DebugSerializerImpl::Load() {
	_categories.Clear();

	FileSystem::Path path(StringLocal<256>::From(FileSystem::Path::Documents().ToString(), "\\DDCPP\\debug.variables.xml"));
	if (path.IsFile()) {
		//SERIALIZER_LOG("Deserialization debug variables from {0}", path);
		Array<i8> data = path.AsFile().Read();
		ArrayView1D<wchar_t> data16 = {(wchar_t*)data.begin(), data.Length() / 2};
		XML::Reader reader;
		reader.ReadFormIterable(data16);
		for (const XML::Element& xmlCategory : reader.GetDocument()->GetRoot().Children) {
			CategoryPair & category = _categories[xmlCategory[L"Name"]];
			category.Category = xmlCategory[L"Name"];
			for (const XML::Element& xmlVariable : xmlCategory.Children) {
				XML::Deserialize(xmlVariable, category.Variables[xmlVariable[L"Name"]]);
			}
		}
	}
}

inline void DebugSerializerImpl::Remove(Debug::Variable ctrl) {
	// is category registered
	CategoryPair * category = _categories.GetItem(ctrl->Category());
	if (category == nullptr)
		return;

	// remove variable if exists
	category->Variables.Remove(ctrl->Name());
	OnSerializerChanged(ctrl, false);
	OnTrackerChanged(ctrl, false);

	SERIALIZER_LOG("Removing debug variable \"{0}\" from serialized variables", ctrl->Name());

	Save();
}

void DebugSerializerImpl::Track(Debug::Variable ctrl) {
	if (IsTracked(ctrl))
		return;

	CategoryPair & category = _categories[ctrl->Category()];
	if (category.Category.IsEmpty())
		category.Category = ctrl->Category();

	// create or find place in map for serialization
	VariablePair & variable = category.Variables[ctrl->Name()];
	// serialize var name
	variable.Name = ctrl->Name();
	// serialize var value
	SerializeVisitor::Run(ctrl, variable.Value.Clear());
	// set tracking for serialization
	variable.Tracked = true;
	SERIALIZER_LOG("Serialization & track of debug variable: {0} = {1}", variable.Name, variable.Value);
	// notify friends
	ctrl->OnValueChanged += _tracker;
	OnSerializerChanged(ctrl, true);
	OnTrackerChanged(ctrl, true);

	Save();
}

void DebugSerializerImpl::Untrack(Debug::Variable ctrl) {
	// is category registered
	CategoryPair * category = _categories.GetItem(ctrl->Category());
	if (category == nullptr)
		return;

	// remove variable if exists
	VariablePair * variable = category->Variables.GetItem(ctrl->Name());
	if (variable == nullptr)
		return;

	// stop tracking
	variable->Tracked = false;
	OnTrackerChanged(ctrl, false);
	SERIALIZER_LOG("Removing track debug variable \"{0}\" from serialized variables", ctrl->Name());

	Save();
}

inline void DebugSerializerImpl::Serialize(Debug::Variable ctrl) {
	CategoryPair & category = _categories[ctrl->Category()];
	if (category.Category.IsEmpty())
		category.Category = ctrl->Category();

	// create or find place in map for serialization
	VariablePair & variable = category.Variables[ctrl->Name()];
	// serialize var name
	variable.Name = ctrl->Name();
	// serialize var value
	SerializeVisitor::Run(ctrl, variable.Value.Clear());
	// notify friends
	if (ctrl->Source() & Debug::Sources::SERIAL)
		OnSerializerChanged(ctrl, true);

	SERIALIZER_LOG("Serialization of debug variable: {0} = {1}", variable.Name, variable.Value);

	Save();
}

inline bool DebugSerializerImpl::Deserialize(Debug::Variable ctrl) {
	// is category registered
	CategoryPair * category = _categories.GetItem(ctrl->Category());
	if (category == nullptr)
		return false;

	// is variable registered
	VariablePair * variable = category->Variables.GetItem(ctrl->Name());
	if (variable == nullptr)
		return false;

	//SERIALIZER_LOG("Deserialization of variable \"{0}\" = {1}", variable->Name, variable->Value);
	DeserializeVisitor::Run(ctrl, variable->Value);
	if (variable->Tracked) {
		ctrl->OnValueChanged += _tracker;
	}
	return true;
}

inline bool DebugSerializerImpl::IsRegistered(Debug::Variable ctrl) {
	// is category registered
	CategoryPair * category = _categories.GetItem(ctrl->Category());
	if (category == nullptr)
		return false;

	// is variable registered
	VariablePair * variable = category->Variables.GetItem(ctrl->Name());
	if (variable == nullptr)
		return false;

	return true;
}

bool DebugSerializerImpl::IsTracked(Debug::Variable ctrl) {
	// is category registered
	CategoryPair * category = _categories.GetItem(ctrl->Category());
	if (category == nullptr)
		return false;

	// is variable registered
	VariablePair * variable = category->Variables.GetItem(ctrl->Name());
	if (variable == nullptr)
		return false;

	return variable->Tracked;
}

inline DebugSerializerImpl::DebugSerializerImpl(Debug::IVariables * variables)
	: _tracker([](Debug::IVariableBase * v, Debug::Sources s) {
		if (Debug::Sources::SERIAL ^ s)
			DebugSerializerImpl::Instance().Serialize(v);
	})
{
	// deserialization lambda
	Action<Debug::IVariableBase *> deserialize([this](Debug::IVariableBase * c) { Deserialize(c); });
	// load saved variables from file
	Load();

	// lock interface to not be interrupted from another thread
	variables->Lock();
	// check while new var is created
	variables->OnVariableCreate += deserialize;
	// check already saved variables
	variables->Enumerate(deserialize);
	// release the lock
	variables->UnLock();
}
#pragma endregion

/************************************************************************/
/* DebugGuiVariable declaration                                         */
/************************************************************************/
#pragma region DebugGuiVariable declaration
/** Storage of Debug::Variable in  */
struct DebugGuiVariableBase : public IReferential {
	/** Gui controls for var representation. */
	union GuiVars {
		struct { Reference<Controls::IControl> _0, _1, _2, _3, _4; };
		struct IGuiVariable { Controls::CheckboxControl Serializable, CSerializable; Controls::LabelControl Label; } IVar;
		struct : public IGuiVariable { Controls::OptionControl Options; Controls::LabelControl Info; } Assert;
		struct : public IGuiVariable { Controls::OptionControl Options; Controls::LabelControl Info; } Log;
		struct : public IGuiVariable { Controls::CheckboxControl Checkbox; } Boolean;
		struct : public IGuiVariable { Controls::ButtonControl Button; } Button;
		struct : public IGuiVariable { Controls::OptionControl Options; } Enum;
		struct : public IGuiVariable { Controls::TextControl Text; } Type;
		struct : public IGuiVariable { Controls::SliderControl Slider; Controls::TextControl Text; } Range;
		struct : public IGuiVariable { Controls::TextControl Text; Controls::ButtonControl Button; } Path;
		struct : public IGuiVariable { Controls::OptionControl Options; } Options;
		GuiVars() : _0(), _1(), _2(), _3(), _4() {}
		~GuiVars() { _0.Release(), _1.Release(), _2.Release(), _3.Release(), _4.Release(); }
	} Gui;
	/** Debug variable. */
	Debug::Variable Variable;
	/** Delegate for auto-safe variables. */
	Delegate<Debug::IVariableBase *, Debug::Sources> AutoSafe;
	/** Flag if the variable has changed between gui updates. */
	bool Dirty = true;
	/** Check if gui console can modify the control. */
	bool IsEnabled() const { return Variable->Source() & Debug::Sources::GUI; }
	/** Methods updating gui controls. */
	void Clean();
	void Clean(Debug::AssertBase *);
	void Clean(Debug::BooleanBase *);
	void Clean(Debug::ButtonBase *) { Dirty = false; }
	void Clean(Debug::IEnumBase *);
	void Clean(Debug::PathBase *);
	void Clean(Debug::LogBase *);
	void Clean(Debug::OptionsBase *);
	template<typename T> void Clean(Debug::TypeBase<T> *);
	template<typename T> void Clean(Debug::RangeBase<T> *);
	/** Methods for initialization gui controls. */
	void Init();
	void Init(Debug::AssertBase *);
	void Init(Debug::BooleanBase *);
	void Init(Debug::ButtonBase *);
	void Init(Debug::IEnumBase *);
	void Init(Debug::PathBase *);
	void Init(Debug::LogBase *);
	void Init(Debug::OptionsBase *);
	template<typename T> void Init(Debug::TypeBase<T> *);
	template<typename T> void Init(Debug::RangeBase<T> *);
	/** BTree comparer methods. */
	Text::StringView16 GetKey() const { return Variable->Name(); }
	/** Constructor. */
	DebugGuiVariableBase(Debug::Variable control) : Variable(control) {}
};
/** Reference to GuiVariableBase. */
typedef Reference<DebugGuiVariableBase> DebugGuiVariable;
#pragma endregion

/************************************************************************/
/* DebugGuiCategory declaration                                         */
/************************************************************************/
#pragma region DebugGuiCategory declaration
/** Visual representation of Gui. */
struct DebugGuiCategoryBase {
	/** Winapi controls.  */
	struct GuiVars {
		struct {
			DD::Controls::Window::MenuProxy Main, Tools;
			i32 AlwaysOnTop;
		} Menu;
		DD::Controls::Window Window;
		DD::Controls::GridControl Grid;
	} Gui;
	/** Settings of gui of category. */
	struct {
		/** Window position and dimension. */
		Debug::i32Range Left, Top, Width, Height;
		/** Window states. */
		Debug::Boolean Independent, AlwaysOnTop;
	} Settings;
	/** Reference to the debug category. */
	Debug::Category Category;
	/** Timers. */
	Stopwatch Dirty, SinceLastRefresh;
	/** Gui variables. */
	BTree<DebugGuiVariable> Variables;
	/** Double buffered async initialization of variables. */
	List<Debug::Variable> Async, _Async;
	/** Category state. */
	bool Initialized = false;
	/** BTree getter of key. */
	Text::StringView16 GetKey() const { return Category->Name(); }
	/** Recalculate grid of variables. */
	void RecalculateGui();
	/** Initialize category. */
	void Init();
	/** */
	void MakeIndependent();
	/** Process one frame. */
	void ProcessFrame();
	/** Constructor. */
	DebugGuiCategoryBase(Debug::Category category);
};
/** Information of gui categories. */
typedef Reference<DebugGuiCategoryBase> DebugGuiCategory;
#pragma endregion

/************************************************************************/
/* DebugGui declaration                                                 */
/************************************************************************/
#pragma region DebugGui declaration
/** Singleton gui manager of Dbug::Variable's. */
struct DebugGui {
	/** Singleton. */
	static DebugGui & Instance() { static DebugGui i(Debug::Variables()); return i; }
	/** Parent window. */
	struct {
		/** Parent window. */
		DD::Controls::Window Window;
		/** Visual representation of cards. */
		DD::Controls::TabViewControl Tabs;
		/** Menu. */
		struct {
			/** Menu categories. */
			::DD::Controls::Window::MenuProxy Main, Tools;
			/** Menu final items. */
			::DD::i32 AlwaysOnTop, MakeIndependent, MakeAllIndependent, MakeAllDependent;
		} Menu;
	} Gui;
	/** Double buffered async initialization of categories. */
	List<Debug::Category> Async, _Async;
	/** Gui categories. */
	BTree<DebugGuiCategory> Categories;
	/** Watches for deferred processing. */
	Stopwatch DirtyXml, SinceLastRefresh;
	/** Debug values. */
	struct {
		/** Refreshing of window. */
		Debug::u64Range RefreshRate, SaveRate;
		/** Window position. */
		Debug::i32Range Left, Top, Width, Height;
		/** Selected tab. */
		Debug::String SelectedTab;
		/** Window stats. */
		Debug::Boolean AlwaysOnTop, Tray;
	} Settings;
	/** */
	void OnSerializerUpdate(Debug::IVariableBase *, bool);
	void OnTrackerUpdate(Debug::IVariableBase *, bool);
	/** Asynchronous init. */
	void AsyncInit();
	/** Save window gui data. */
	void Save();
	/** Process profile scopes. */
	void ProcessProfileScopes();
	/** Process single frame. */
	void ProcessFrame();
	/** Constructor. */
	DebugGui(Debug::IVariables *);
};
#pragma endregion

/************************************************************************/
/* DebugGuiVariable definition                                          */
/************************************************************************/
#pragma region DebugGuiVariable
struct DebugGuiVariableBaseCleanVisitor : Debug::IVisitor<DebugGuiVariableBaseCleanVisitor, DebugGuiVariableBase *> {
	template<typename T>
	static void Visit(T * variable, DebugGuiVariableBase * gui) { gui->Clean(variable); }
};
struct DebugGuiVariableBaseInitVisitor : Debug::IVisitor<DebugGuiVariableBaseInitVisitor, DebugGuiVariableBase *> {
	template<typename T>
	static void Visit(T * variable, DebugGuiVariableBase * gui) { gui->Init(variable); }
};

inline void DebugGuiVariableBase::Clean() {
	DebugGuiVariableBaseCleanVisitor::Run(Variable, this);
}

inline void DebugGuiVariableBase::Clean(Debug::AssertBase * c) {
	Gui.Assert.Info->Text(StringLocal<64>::From("[", c->ValidTicks, "/", (c->ValidTicks + c->InvalidTicks), "]"));
	Gui.Assert.Options->Index(c->Value().ToIndex());
	// read asserts continuously
	// Dirty = false;
}

inline void DebugGuiVariableBase::Clean(Debug::BooleanBase * c) {
	Gui.Boolean.Checkbox->Checked(c->Value());
	Dirty = false;
}

inline void DebugGuiVariableBase::Clean(Debug::IEnumBase * c) {
	Gui.Enum.Options->Index(c->Descriptor()->Value2Index(c->NativeValue()));
	Dirty = false;
}

void DebugGuiVariableBase::Clean(Debug::PathBase * path) {
	Gui.Path.Text->Text(path->Value());
	Dirty = false;
}

template<typename T>
inline void DebugGuiVariableBase::Clean(Debug::TypeBase<T> * c) {
	Gui.Type.Text->Text(StringLocal<256>::From(c->Value()));
	Dirty = false;
}

template<typename T>
inline void DebugGuiVariableBase::Clean(Debug::RangeBase<T> * c) {
	Gui.Range.Text->Text(StringLocal<256>::From(c->Value()));
	DD_TODO_STAMP("Slider");
	Dirty = false;
}

void DebugGuiVariableBase::Clean(Debug::LogBase * c) {
	Gui.Log.Info->Text(StringLocal<16>::From(c->Ticks));
	Gui.Log.Options->Index(c->Value().ToIndex());
	// read logs continuously
	// Dirty = false;
}

void DebugGuiVariableBase::Clean(Debug::OptionsBase * options) {
	Gui.Options.Options->Index((DD::i32)options->Index());
	Dirty = false;
}

inline void DebugGuiVariableBase::Init() {
	// dirty setter
	using Sources = Debug::Sources;
	static constexpr auto check = Sources::ALL ^ Sources::GUI;
	Variable->OnValueChanged += [this](Debug::IVariableBase *, Sources source) {
		if (source & check) {
			Dirty = true;
		}
	};
	// create label
	Gui.IVar.Label = Controls::LabelControl(Variable->Name());
	Gui.IVar.Label->Tooltip(Variable->Description());
	Gui.IVar.Label->Background(Colors::WHITE);
	Gui.IVar.Label->Left(10);
	Gui.IVar.Label->Margin(2);
	Gui.IVar.Label->Padding({ 5,2,2,2 });
	Gui.IVar.Label->Font(DEFAULT_FONT);

	static constexpr auto serializable = Sources::GUI | Sources::SERIAL;
	if ((Variable->Source() & serializable) == serializable) {
		// create serialization check-box
		Gui.IVar.Serializable = Controls::CheckboxControl(::DD::String());
		Gui.IVar.Serializable->Checked(DebugSerializerImpl::Instance().IsRegistered(Variable));
		Gui.IVar.Serializable->Margin(2);
		Gui.IVar.Serializable->Padding({ 2,2,2,2 });
		Gui.IVar.Serializable->Grid({ 1,1,4,1 });
		Gui.IVar.Serializable->OnStateDownload += [this](bool serialize) {
			if (serialize)
				DebugSerializerImpl::Instance().Serialize(Variable);
			else
				DebugSerializerImpl::Instance().Remove(Variable);
		};



		// create auto-serialization checkbox
		Gui.IVar.CSerializable = Controls::CheckboxControl(::DD::String());
		Gui.IVar.CSerializable->Checked(DebugSerializerImpl::Instance().IsTracked(Variable));
		Gui.IVar.CSerializable->Margin(2);
		Gui.IVar.CSerializable->Padding({ 2,2,2,2 });
		Gui.IVar.CSerializable->Grid({ 1,1,5,1 });
		Gui.IVar.CSerializable->OnStateDownload += [this](bool serialize) {
			if (serialize)
				DebugSerializerImpl::Instance().Track(Variable);
			else
				DebugSerializerImpl::Instance().Untrack(Variable);
		};

	}
	// type specific initialization
	DebugGuiVariableBaseInitVisitor::Run(Variable, this);
}

inline void DebugGuiVariableBase::Init(Debug::AssertBase * c) {
	// assert mode
	Gui.Assert.Options = Controls::OptionControl(Controls::OptionControl::Types::Basic);
	Gui.Assert.Options->Margin(2);
#if DD_DEBUG_CODE_ENABLED
	Gui.Assert.Options->Index(Debug::Assert::Modes::BREAK);
#else
	Gui.Assert.Options->Index(Debug::Assert::Modes::SILENT);
#endif
	//Gui.Assert.Options->Enabled(IsEnabled());
	Gui.Assert.Options->Tooltip(Variable->Description());

	Gui.Assert.Options->Font(L"Tahoma", 14);
	for (u32 i = 0; i < Debug::Assert::Modes::Size(); ++i)
		Gui.Assert.Options->AddOption(Debug::Assert::Modes::FromIndex(i).ToString());

	Gui.Assert.Options->OnSelectionDownload += [this](DD::i32, DD::i32 n) {
		reinterpret_cast<Debug::Assert &>(Variable)->Value(
			Debug::Assert::Modes::FromIndex(n),
			Debug::Sources::GUI
		);
	};
	// numbers of assert ticks
	Gui.Assert.Info = Controls::LabelControl(L"0 / 0");
	//Gui.Assert.Info->Tooltip(Stringize(L"[Valid ticks / Total ticks]."));
	Gui.Assert.Info->Background(Colors::WHITE);
	Gui.Assert.Info->Left(10);
	Gui.Assert.Info->Margin(2);
	Gui.Assert.Info->Padding({ 2,2,10,2 });
	Gui.Assert.Info->Font(DEFAULT_FONT);
	Gui.Assert.Info->TextAlignment(Controls::IControl::TAlign::Right);
	// position settings
	Gui.Assert.Label->Grid({ 1,1,1,1 });
	Gui.Assert.Options->Grid({ 1,1,2,1 });
	Gui.Assert.Info->Grid({ 1,1,3,1 });
}

inline void DebugGuiVariableBase::Init(Debug::BooleanBase * c) {
	Gui.Boolean.Checkbox = Controls::CheckboxControl(::DD::String(), c);
	Gui.Boolean.Checkbox->Margin(2);
	Gui.Boolean.Checkbox->Padding({ 2,2,2,2 });
	Gui.Boolean.Checkbox->BoxAlignment(Controls::IControl::TAlign::Right);
	Gui.Boolean.Checkbox->Enabled(c->Source() & Debug::Sources::GUI);
	if (IsEnabled())
		Gui.Boolean.Checkbox->Tooltip(c->Description());
	// changes from non gui source.
	Gui.Boolean.Checkbox->OnStateDownload += [this](bool v) mutable {
		static_cast<Debug::BooleanBase *>(Variable.Ptr())->Value(v, Debug::Sources::GUI);
	};
	// position settings
	Gui.Boolean.Label->Grid({ 1,1,1,2 });
	Gui.Boolean.Checkbox->Grid({ 1,1,3,1 });
}

inline void DebugGuiVariableBase::Init(Debug::IEnumBase * c) {
	// init options
	Gui.Enum.Options = Controls::OptionControl(Controls::OptionControl::Types::Basic);
	Gui.Enum.Options->Margin(2);
	Gui.Enum.Options->Enabled(IsEnabled());
	if (IsEnabled())
		Gui.Enum.Options->Tooltip(c->Description());
	Gui.Enum.Options->Font(L"Tahoma", 14);
	// fill names of option
	const DD::IEnum::Descriptor * descriptor = c->Descriptor();
	for (i32 i = 0, m = descriptor->Size(); i < m; ++i) {
		Gui.Enum.Options->AddOption(descriptor->Index2String(i));
	}
	// set index from debug
	Gui.Enum.Options->OnSelectionDownload += [this](DD::i32, DD::i32 n) {
		::DD::u64 value = reinterpret_cast<Debug::IEnum &>(Variable)->Descriptor()->Index2Value(n);
		reinterpret_cast<Debug::IEnum &>(Variable)->NativeValue(value, Debug::Sources::GUI);
	};
	// set index
	Gui.Enum.Options->Index(descriptor->Value2Index(c->NativeValue()));
	// position settings
	Gui.Enum.Label->Grid({ 1,1,1,1 });
	Gui.Enum.Options->Grid({ 1,1,2,2 });
}

void DebugGuiVariableBase::Init(Debug::PathBase * c) {
	// init button control
	Gui.Path.Button = Controls::ButtonControl(L"Find");
	Gui.Path.Button->TextAlignment(Controls::IControl::TAlign::Center);
	Gui.Path.Button->Margin(2);
	Gui.Path.Button->Tooltip(c->Description());
	Gui.Path.Button->Font(L"Tahoma", 14);
	Gui.Path.Button->OnClick += [this, c]() mutable {
		switch (c->Mode()) {
		case Debug::PathBase::Modes::OPEN_FILE: {
				FileSystem::Path path = Dialog::OpenFile(Array<Dialog::Extension>());
				Gui.Path.Text->Text(path);
				c->Value(path.ToString(), Debug::Sources::GUI);
			} break;
			case Debug::PathBase::Modes::OPEN_FILES: {
				Array<FileSystem::Path> files = Dialog::OpenFiles(Array<Dialog::Extension>());
				StringLocal<4096> fStrings;
				for (const FileSystem::Path & file : files)
					fStrings.AppendBatch(file.ToString(), ";");
				Gui.Path.Text->Text(fStrings);
				c->Value(fStrings, Debug::Sources::GUI);
			} break;
			case Debug::PathBase::Modes::SAVE_FILE: {
				FileSystem::Path path = Dialog::SaveFile(Dialog::Extension());
				Gui.Path.Text->Text(path);
				c->Value(path.ToString(), Debug::Sources::GUI);
			} break;
			case Debug::PathBase::Modes::SELECT_DIR: {
				FileSystem::Path path = Dialog::OpenDir();
				Gui.Path.Text->Text(path);
				c->Value(path.ToString(), Debug::Sources::GUI);
			} break;
			default: {
				DD_WARNING("Undefined");
			} break;
		}
	};


	Gui.Path.Text = Controls::TextControl(::DD::String());
	Gui.Path.Text->Background(Colors::WHITE);
	Gui.Path.Text->Margin(2);
	Gui.Path.Text->Padding({ 2,2,2,2 });
	Gui.Path.Text->TextAlignment(Controls::IControl::TAlign::Right);
	Gui.Path.Text->Enabled(IsEnabled());
	Gui.Path.Text->Font(DEFAULT_FONT);
	Gui.Path.Text->OnTextDownload += [this]() mutable {
		static_cast<Debug::PathBase*>(Variable.Ptr())->Value(
			Gui.Path.Text->Text(),
			DD::Debug::Sources::GUI
		);
	};

	// position settings
	Gui.Path.Label->Grid({ 1,1,1,1 });
	Gui.Path.Text->Grid({ 1,1,2,1 });
	Gui.Path.Button->Grid({ 1,1,3,1 });
}

inline void DebugGuiVariableBase::Init(Debug::ButtonBase * c) {
	// init button control
	Gui.Button.Button = Controls::ButtonControl(L"Submit");
	Gui.Button.Button->TextAlignment(Controls::IControl::TAlign::Center);
	Gui.Button.Button->Margin(2);
	Gui.Button.Button->Tooltip(c->Description());
	Gui.Button.Button->Font(L"Tahoma", 14);
	Gui.Button.Button->OnClick += [this]() mutable {
		static_cast<Debug::ButtonBase *>(Variable.Ptr())->Press(Debug::Sources::GUI);
	};
	// position settings
	Gui.Button.Label->Grid({ 1,1,1,2 });
	Gui.Button.Button->Grid({ 1,1,3,1 });
}

template<typename T>
inline void DebugGuiVariableBase::Init(Debug::TypeBase<T> * c) {
	// text
	Gui.Type.Text = Controls::TextControl(::DD::String());
	Gui.Type.Text->Background(Colors::WHITE);
	Gui.Type.Text->Margin(2);
	Gui.Type.Text->Padding({ 2,2,2,2 });
	Gui.Type.Text->TextAlignment(Controls::IControl::TAlign::Right);
	Gui.Type.Text->Enabled(IsEnabled());
	Gui.Type.Text->Font(DEFAULT_FONT);
	// position
	Gui.Type.Label->Grid({ 1,1,1,1 });
	Gui.Type.Text->Grid({ 1,1,2,2 });

	// changes from gui source
	Gui.Type.Text->OnTextDownload += [this]() mutable {
		T oldValue, newValue;
		if (Gui.Type.Text->Text().TryCast(newValue)) {
			if (newValue != reinterpret_cast<Debug::Type<T> &>(Variable)->Value()) {
				oldValue = reinterpret_cast<Debug::Type<T> &>(Variable)->Value();
				reinterpret_cast<Debug::Type<T> &>(Variable)->Value(newValue, Debug::Sources::GUI);
				Gui.Type.Text->Background(0xFFAAFFAA);
			}
		}
		else {
			Gui.Type.Text->Background(0xFFAAAAFF);
		}
	};
}

template<typename T>
inline void DebugGuiVariableBase::Init(Debug::RangeBase<T> * c) {
	static constexpr ::DD::i32 RESOLUTION = 1000;
	// text settings
	Gui.Range.Text = Controls::TextControl(::DD::String());
	Gui.Range.Text->Background(Colors::WHITE);
	Gui.Range.Text->Margin(2);
	Gui.Range.Text->TextAlignment(Controls::IControl::TAlign::Right);
	Gui.Range.Text->Padding({ 2,2,2,2 });
	if (IsEnabled())
		Gui.Range.Text->Tooltip(c->Description());
	Gui.Range.Text->Enabled(IsEnabled());
	Gui.Range.Text->Font(DEFAULT_FONT);
	// slider settings
	Gui.Range.Slider = Controls::SliderControl(0, RESOLUTION, 0);
	Gui.Range.Slider->Enabled(IsEnabled());
	Gui.Range.Slider->Margin(2);
	if (IsEnabled())
		Gui.Range.Slider->Tooltip(c->Description());
	// position settings
	Gui.Range.Label->Grid({ 1,1,1,1 });
	Gui.Range.Slider->Grid({ 1,1,2,1 });
	Gui.Range.Text->Grid({ 1,1,3,1 });
	// changes from gui slider
	Gui.Range.Slider->OnValueDownload += [this](::DD::i32 o, ::DD::i32 n) {
		T minimum = reinterpret_cast<Debug::Range<T> &>(Variable)->Minimum();
		T maximum = reinterpret_cast<Debug::Range<T> &>(Variable)->Maximum();
		T range = maximum - minimum;
		::DD::f64 factor = Gui.Range.Slider->Value() / (::DD::f64)RESOLUTION;
		T oldValue = reinterpret_cast<Debug::Range<T> &>(Variable)->Value();
		T newValue = (T)(factor * range + minimum);
		Gui.Range.Text->Text(StringLocal<32>::From(newValue), true);
		reinterpret_cast<Debug::Range<T> &>(Variable)->Value(newValue, Debug::Sources::GUI);
	};
	// changes from gui text
	Gui.Range.Text->OnTextDownload += [this]() {
		T minimum = reinterpret_cast<Debug::Range<T> &>(Variable)->Minimum();
		T maximum = reinterpret_cast<Debug::Range<T> &>(Variable)->Maximum();
		T range = maximum - minimum;
		T value;
		if (Gui.Range.Text->Text().TryCast(value) && minimum <= value && value <= maximum) {
			reinterpret_cast<Debug::Range<T> &>(Variable)->Value(value, Debug::Sources::GUI);
			Gui.Range.Slider->Value((::DD::i32)(RESOLUTION * (value - minimum) / range), true);
			Gui.Range.Text->Background(0xFFAAFFAA);
		}
		else {
			Gui.Range.Text->Background(0xFFAAAAFF);
		}
	};
}

void DebugGuiVariableBase::Init(Debug::LogBase * log) {
	// assert mode
	Gui.Log.Options = Controls::OptionControl(Controls::OptionControl::Types::Basic);
	Gui.Log.Options->Margin(2);
	Gui.Log.Options->Tooltip(Variable->Description());
	Gui.Log.Options->Font(L"Tahoma", 14);
	for (u32 i = 0; i < Debug::Log::Modes::Size(); ++i)
		Gui.Log.Options->AddOption(Debug::Log::Modes::FromIndex(i).ToString());
	Gui.Log.Options->Index(log->Value().ToIndex());
	Gui.Log.Options->OnSelectionDownload += [this](DD::i32, DD::i32 n) {
		reinterpret_cast<Debug::Log &>(Variable)->Value(
			Debug::Log::Modes::FromIndex(n),
			Debug::Sources::GUI
		);
	};
	// numbers of assert ticks
	Gui.Log.Info = Controls::LabelControl(L"0");
	Gui.Log.Info->Background(Colors::WHITE);
	Gui.Log.Info->Left(10);
	Gui.Log.Info->Margin(2);
	Gui.Log.Info->Padding({ 2,2,10,2 });
	Gui.Log.Info->Font(DEFAULT_FONT);
	Gui.Log.Info->TextAlignment(Controls::IControl::TAlign::Right);
	// position settings
	Gui.Log.Label->Grid({ 1,1,1,1 });
	Gui.Log.Options->Grid({ 1,1,2,1 });
	Gui.Log.Info->Grid({ 1,1,3,1 });
}

void DebugGuiVariableBase::Init(Debug::OptionsBase * options) {
	// init options
	Gui.Options.Options = Controls::OptionControl(Controls::OptionControl::Types::Basic);
	Gui.Options.Options->Margin(2);
	Gui.Options.Options->Enabled(IsEnabled());
	if (IsEnabled())
		Gui.Options.Options->Tooltip(options->Description());
	Gui.Options.Options->Font(L"Tahoma", 14);

	// stringlist changes
	options->OnCollectionChanged += [this](
		Debug::OptionsBase * base,
		const Debug::OptionsBase::CollectionChangedDescriptor & descriptor
	) {
		switch (descriptor.Change) {
		case Debug::OptionsBase::CollectionChangedDescriptor::Changes::ADD:
				Gui.Options.Options->AddOption(base->At(base->Size() - 1));
				break;
			case Debug::OptionsBase::CollectionChangedDescriptor::Changes::CLEAR:
				Gui.Options.Options->Clear();
				break;
			case Debug::OptionsBase::CollectionChangedDescriptor::Changes::INSERT:
				Gui.Options.Options->InsertOption(descriptor.Index, base->At(descriptor.Index));
				break;
			case Debug::OptionsBase::CollectionChangedDescriptor::Changes::REMOVE:
				Gui.Options.Options->RemoveOption(descriptor.Index);
				break;
			default:
				DD_WARNING("Undefined option");
				break;
		}
	};

	// fill names of option
	for (i32 i = 0, m = (DD::i32)options->Size(); i < m; ++i) {
		Gui.Options.Options->AddOption(options->At(i));
	}
	// set index from debug
	Gui.Options.Options->OnSelectionDownload += [this](DD::i32, DD::i32 n) {
		reinterpret_cast<Debug::OptionsBase *>(Variable.Ptr())->Index(n, Debug::Sources::GUI);
	};
	// set index
	Gui.Options.Options->Index((DD::i32)options->Index());
	// position settings
	Gui.Options.Label->Grid({ 1,1,1,1 });
	Gui.Options.Options->Grid({ 1,1,2,2 });
}

#pragma endregion

/************************************************************************/
/* DebugGuiCategory definition                                          */
/************************************************************************/
#pragma region DebugGuiCategory
inline void DebugGuiCategoryBase::RecalculateGui() {
	// index of variable in btree
	i32 index = 0;
	for (DebugGuiVariable & var : Variables) {
		++index;

		if (var->Gui._0)
			var->Gui._0->GridRow(index);
		if (var->Gui._1)
			var->Gui._1->GridRow(index);
		if (var->Gui._2)
			var->Gui._2->GridRow(index);
		if (var->Gui._3)
			var->Gui._3->GridRow(index);
		if (var->Gui._4)
			var->Gui._4->GridRow(index);
	}
}

inline void DebugGuiCategoryBase::Init() {
	using Sources = Debug::Sources;
	static constexpr Sources source = (Sources)(Sources::ALL ^ Sources::GUI);

	StringLocal<64> categoryStamp;
	categoryStamp.AppendFormat("[{0}] ", Category->Name());

	AABox2I desktop = Environment::VirtualDesktop::Box();
	Settings.Left = Debug::i32Range(500, desktop.Min.x - 10, desktop.Max.x + 10, L"Console", StringLocal<64>::From(categoryStamp, "Window Position Left"), L"", source);
	Settings.Top = Debug::i32Range(500, desktop.Min.y - 10, desktop.Max.y + 10, L"Console", StringLocal<64>::From(categoryStamp, "Window Position Top"), L"", source);
	Settings.Width = Debug::i32Range(600, 100, 8000, L"Console", StringLocal<64>::From(categoryStamp, "Window Dimension Width"), L"", source);
	Settings.Height = Debug::i32Range(800, 100, 8000, L"Console", StringLocal<64>::From(categoryStamp, "Window Dimension Height"), L"", source);
	Settings.Independent = Debug::Boolean(false, L"Console", StringLocal<64>::From(categoryStamp, "Window Independent"), L"", source);
	Settings.AlwaysOnTop = Debug::Boolean(false, L"Console", StringLocal<64>::From(categoryStamp, "Window Always On Top"), L"", source);

	Gui.Grid = Controls::GridControl(Controls::GridControl::Types::Generic);
	Gui.Grid->AddCol({ Controls::IControl::SizeModes::Percents, 5.f });
	Gui.Grid->AddCol({ Controls::IControl::SizeModes::Percents, 45.f });
	Gui.Grid->AddCol({ Controls::IControl::SizeModes::Percents, 20.f });
	Gui.Grid->AddCol({ Controls::IControl::SizeModes::Percents, 20.f });
	Gui.Grid->AddCol({ Controls::IControl::SizeModes::Points, 23.f });
	Gui.Grid->AddCol({ Controls::IControl::SizeModes::Points, 23.f });
	Gui.Grid->SetDefaultRow({ Controls::IControl::SizeModes::Points, 24.f });
	Gui.Grid->HorizontalScrollBar(Controls::IControl::Visibility::Invisible);
	Gui.Grid->Background(Color::Collection()[Category->Name().Hash<size_t, DD::Text::HashMethods::XorCS>()]);
	Initialized = true;
}

void DebugGuiCategoryBase::MakeIndependent() {
	if(Gui.Window.IsNotNull() && Settings.Independent == true)
		return;

	Settings.Independent = true;
	Dirty.Start();

	// Establish independent window
	Gui.Window = DD::Controls::Window(
		DD::Controls::Window::Types::Basic,
		StringLocal<128>::From("DD::Debug::", Category->Name())
	);
	Gui.Window->Left(Settings.Left);
	Gui.Window->Top(Settings.Top);
	Gui.Window->Width(Settings.Width);
	Gui.Window->Height(Settings.Height);
	Gui.Window->AlwaysOnTop(Settings.AlwaysOnTop);

	// move gui to the window
	Gui.Window->Add(Gui.Grid);
	// on close, move gui back to the manager
	Gui.Window->OnClose += [this](bool & close) mutable {
		i32 index = DebugGui::Instance().Gui.Tabs->Name2Index(Category->Name());
		DebugGui::Instance().Gui.Tabs->SetTab(index, Category->Name(), Gui.Grid);
		Gui.Window->Destroy();
		close = false;
		Settings.Independent = false;
		DebugSerializerImpl::Instance().Serialize(Settings.Independent);
	};
	// per frame update
	Gui.Window->OnIdle += [this]() {
		if (DebugGui::Instance().Settings.RefreshRate < SinceLastRefresh.ElapsedMilliseconds()) {
			ProcessFrame();
			SinceLastRefresh.Restart();
		}
	};
	// on position changed, serialize
	Gui.Window->OnPositionChanged += [this]() mutable {
		Dirty.Restart();
	};
	// on size changed, serialize
	Gui.Window->OnSizeChanged += [this]() mutable {
		Dirty.Restart();
	};
	// menu of the window
	Gui.Menu.Main = Gui.Window->Menu();
	Gui.Menu.Tools = Gui.Menu.Main.AddItem(L"&Tools");
	Gui.Menu.AlwaysOnTop = Gui.Menu.Tools.AddItem(
		L"Always on top",
		[this](i32 id) mutable {
			Gui.Window->AlwaysOnTop(!Gui.Window->AlwaysOnTop());
			Gui.Menu.Tools.CheckItem(id, Gui.Window->AlwaysOnTop());
		}
	);

	Gui.Window->Init();
	SinceLastRefresh.Start();
	// Establish temporary label
	Controls::LabelControl label(L"External control");
	label->Background(Colors::WHITE);
	label->TextAlignment(Controls::IControl::TAlign::Center);
	// Fix tab
	DebugGui::Instance().Gui.Tabs->SetTab(DebugGui::Instance().Gui.Tabs->Name2Index(Category->Name()), Category->Name(), label);
	Gui.Grid->Show(false);
	Gui.Grid->Show(true);
}

void DebugGuiCategoryBase::ProcessFrame() {
	DD_PROFILE_SCOPE_STATS("Debug/Gui/ProcessFrame");
	if (!Initialized)
		Init();

	// get actual Async buffer to _Async buffer
	Debug::Variables()->Lock();
	Async.Swap(_Async);
	Debug::Variables()->UnLock();
	// initialize variables - process _Async buffer
	for (Debug::Variable & variable : _Async) {
		DebugGuiVariable gVariable = new DebugGuiVariableBase(variable);
		Variables.Insert(gVariable);
		// initialize gui
		gVariable->Init();
		// place gui into category grid
		if (gVariable->Gui._0)
			Gui.Grid->Add(gVariable->Gui._0);
		if (gVariable->Gui._1)
			Gui.Grid->Add(gVariable->Gui._1);
		if (gVariable->Gui._2)
			Gui.Grid->Add(gVariable->Gui._2);
		if (gVariable->Gui._3)
			Gui.Grid->Add(gVariable->Gui._3);
		if (gVariable->Gui._4)
			Gui.Grid->Add(gVariable->Gui._4);
	}
	// variables to be removed
	List<DebugGuiVariable, 128> removedVariables;
	// enum variables
	{
		DD_PROFILE_SCOPE_STATS("Debug/Gui/ProcessFrame/CheckExpired");
		for (DebugGuiVariable & var : Variables) {
			// rest references are in DebugGui::Instance() and Debug::Variables()
			if (var->Variable.Count() == 2) {
				removedVariables.Add(var);
			}
		}
	}
	{
		DD_PROFILE_SCOPE_STATS("Debug/Gui/ProcessFrame/RemoveExpired");
		// remove variables
		for (DebugGuiVariable & var : removedVariables) {
			Variables.Remove(var->GetKey());
		}
	}
	// free buffer
	{
		DD_PROFILE_SCOPE_STATS("Debug/Gui/ProcessFrame/RecalcGui");
		if (_Async.Size() || removedVariables.Size()) {
			RecalculateGui();
			_Async.Free();
		}
	}
	// update values in initialized variables
	{
		DD_PROFILE_SCOPE_STATS("Debug/Gui/ProcessFrame/CleanVars");
		for (DebugGuiVariable & var : Variables) {
			if (var->Dirty) {
				var->Clean();
			}
		}
	}
	// safe properties
	{
		DD_PROFILE_SCOPE_STATS("Debug/Gui/ProcessFrame/Serialize");
		if (DebugGui::Instance().Settings.RefreshRate < Dirty.ElapsedMilliseconds()) {
			DebugSerializerImpl::Instance().Serialize(Settings.Left = Gui.Window->Left());
			DebugSerializerImpl::Instance().Serialize(Settings.Top = Gui.Window->Top());
			DebugSerializerImpl::Instance().Serialize(Settings.Width = Gui.Window->Width());
			DebugSerializerImpl::Instance().Serialize(Settings.Height = Gui.Window->Height());
			DebugSerializerImpl::Instance().Serialize(Settings.Independent);
			DebugSerializerImpl::Instance().Serialize(Settings.AlwaysOnTop = Gui.Window->AlwaysOnTop());
			Dirty.Reset();
		}
	}
}

inline DebugGuiCategoryBase::DebugGuiCategoryBase(Debug::Category category)
	: Category(category)
{
	// asynchronous registration of control
	Action<Debug::IVariableBase *> asyncReg = [this](Debug::IVariableBase * control) {
		this->Async.Add(control);
	};
	// register existing variables, if any
	Category->Enumerate(asyncReg);
	// register delegate, if new variable is being created
	Category->OnVariableCreate += asyncReg;
}
#pragma endregion

/************************************************************************/
/* DebugGui definition                                                  */
/************************************************************************/
#pragma region DebugGui
inline void DebugGui::ProcessProfileScopes() {
	DD_TODO_STAMP("Special DD::Debug::Variable for Profile Scope.");
	// parse profiler
	if (Profiler::ProcessScopes()) {
		// data storage
		static struct { Debug::u64 Count, Total, Min, Max, Last, Avg; } _profiler[1024];
		// check
		static Debug::Boolean enableProfilerProcessing(false, L"ProfileScopes", L"!Enabled", L"Is profiler enabled.");
		// if any changes re-parse data
		if (enableProfilerProcessing) Profiler::GetScopes([](Profiler::Scope & item) {
			// check if initialized
			if (_profiler[item.Index].Count.IsNull()) {
				// card name
				StringLocal<16> tab;
				tab.Append("ProfileScopes");
				// tooltip
				StringLocal<128> tooltip;
				tooltip.AppendFormat(
					"ProfileScope: {1} [{0,3}]\nFunction: {2}\nFile: {3}({4})",
					/* 0 */ item.Index,
					/* 1 */ item.ScopeName,
					/* 2 */ item.ScopeFuntion,
					/* 3 */ item.ScopeFileName,
					/* 4 */ item.ScopeFileLine
				);
				// name prefix
				StringLocal<32> name;
				name.AppendFormat(
					"{1} [{0,3}]",
					/* 0 */ item.Index,
					/* 1 */ item.ScopeName
				);
				// initialize
				_profiler[item.Index].Count = Debug::u64(0, tab, StringLocal<64>::From(name, "[0 - Count]"),   tooltip, Debug::Sources::CODE);
				_profiler[item.Index].Last =  Debug::u64(0, tab, StringLocal<64>::From(name, "[1 - Last]"),    tooltip, Debug::Sources::CODE);
				_profiler[item.Index].Avg =   Debug::u64(0, tab, StringLocal<64>::From(name, "[2 - Average]"), tooltip, Debug::Sources::CODE);
				_profiler[item.Index].Min =   Debug::u64(0, tab, StringLocal<64>::From(name, "[3 - Best]"),    tooltip, Debug::Sources::CODE);
				_profiler[item.Index].Max =   Debug::u64(0, tab, StringLocal<64>::From(name, "[4 - Worst]"),   tooltip, Debug::Sources::CODE);
				_profiler[item.Index].Total = Debug::u64(0, tab, StringLocal<64>::From(name, "[5 - Total]"),   tooltip, Debug::Sources::CODE);
			}
			// fill data
			_profiler[item.Index].Count = item.Count;
			_profiler[item.Index].Total = (item.TotalTime * 1000000000) / Stopwatch::Frequency();
			_profiler[item.Index].Min = (item.MinimalTime * 1000000000) / Stopwatch::Frequency();
			_profiler[item.Index].Max = (item.MaximalTime * 1000000000) / Stopwatch::Frequency();
			_profiler[item.Index].Last = (item.LastTime * 1000000000) / Stopwatch::Frequency();
			if ((u64)_profiler[item.Index].Count)
				_profiler[item.Index].Avg = _profiler[item.Index].Total / _profiler[item.Index].Count;
		});
	}
}

inline void DebugGui::ProcessFrame() {
	// profile scopes, read values
	ProcessProfileScopes();

	// initialize uninitialized categories - get list of them
	Debug::Variables()->Lock();
	Async.Swap(_Async);
	Debug::Variables()->UnLock();
	// initialize categories
	for (Debug::Category & category : _Async) {
		// create category
		DebugGuiCategory gCategory = new DebugGuiCategoryBase(category);
		// initialize category
		gCategory->Init();
		// insert into btree
		Categories[category->Name()] = gCategory;
		// select index of inserted value
		DD::i32 index = 0;
		for (DebugGuiCategory & gc : Categories) {
			if (gc == gCategory)
				break;
			++index;
		}
		// insert category as tab into gui
		Gui.Tabs->InsertTab(index, category->Name(), gCategory->Gui.Grid);
		if (gCategory->Settings.Independent)
			gCategory->MakeIndependent();
	}
	// clear buffer
	_Async.Free();

	// process current tab
	Categories.GetItemByIndex(Gui.Tabs->Index())->ProcessFrame();

	Settings.Left = Gui.Window->Left();
	Settings.Top = Gui.Window->Top();
	Settings.Width = Gui.Window->Width();
	Settings.Height = Gui.Window->Height();
	Settings.SelectedTab = Gui.Tabs->TabName();
	Settings.AlwaysOnTop = Gui.Window->AlwaysOnTop();

	// save state if dirty time is longer than following constant
	if (DirtyXml.IsRunning() && 2000ULL < DirtyXml.ElapsedMilliseconds() )
		Save();
}

void DebugGui::OnSerializerUpdate(Debug::IVariableBase * var, bool enabled) {
	DebugGuiCategory * category = Categories.GetItem(var->Category());
	if (category == nullptr)
		return;

	DebugGuiVariable * variable = (*category)->Variables.GetItem(var->Name());
	if (variable == nullptr)
		return;

	(*variable)->Gui.IVar.Serializable->Checked(enabled, true);
}

void DebugGui::OnTrackerUpdate(Debug::IVariableBase * var, bool enabled) {
	DebugGuiCategory * category = Categories.GetItem(var->Category());
	if (category == nullptr)
		return;

	DebugGuiVariable * variable = (*category)->Variables.GetItem(var->Name());
	if (variable == nullptr)
		return;

	(*variable)->Gui.IVar.CSerializable->Checked(enabled, true);
}

inline void DebugGui::AsyncInit() {
	Gui.Window = DD::Controls::Window(DD::Controls::Window::Types::Basic, L"DD::Debug");
	Gui.Window->Font(DEFAULT_FONT);
	Gui.Window->Left(Settings.Left);
	Gui.Window->Top(Settings.Top);
	Gui.Window->Width(Settings.Width);
	Gui.Window->Height(Settings.Height);
	Gui.Window->AlwaysOnTop(Settings.AlwaysOnTop);

	Gui.Tabs = Controls::TabViewControl(Controls::TabViewControl::Types::Basic);
	Gui.Tabs->Font(DEFAULT_FONT);
	Gui.Window->Add(Gui.Tabs);
	// on close hide to tray
	Gui.Window->OnClose += [this](bool & close) {
		close = false;
		Gui.Window->Tray().Enable();
		Gui.Window->Show(false);
		Settings.Tray = true;
		DirtyXml.Restart();
	};
	// on double click on tray, pop-up
	Gui.Window->Tray().OnDoubleClick += [this]() {
		Gui.Window->Show(true);
		Gui.Window->Tray().Disable();
		Settings.Tray = false;
		DirtyXml.Restart();
	};
	// on tab changed, serialize
	Gui.Tabs->OnTabChanged += [this](i32, i32) {
		DirtyXml.Restart();
	};
	// on idle, process frame
	Gui.Window->OnIdle += [this]() {
		if (Settings.RefreshRate < SinceLastRefresh.ElapsedMilliseconds()) {
			ProcessFrame();
			SinceLastRefresh.Restart();
		}
	};
	// on position changed, serialize
	Gui.Window->OnPositionChanged += [this]() {
		DirtyXml.Restart();
	};
	// on position changed, serialize
	Gui.Window->OnSizeChanged += [this]() {
		DirtyXml.Restart();
	};
	// set menu
	Gui.Menu.Main = Gui.Window->Menu();
	Gui.Menu.Tools = Gui.Menu.Main.AddItem(L"&Tools");
	// make one card independent
	Gui.Menu.MakeIndependent = Gui.Menu.Tools.AddItem(L"&Independent card", [this](i32) mutable {
		Categories.GetItemByIndex(Gui.Tabs->Index())->MakeIndependent();
		DirtyXml.Restart();
	});
	// make all cards independent
	Gui.Menu.MakeAllIndependent = Gui.Menu.Tools.AddItem(L"&Independent all cards", [this](i32) mutable {
		for (auto & category : Categories) {
			category->MakeIndependent();
		}
		DirtyXml.Restart();
	});
	// make all cards dependent
	Gui.Menu.MakeAllDependent = Gui.Menu.Tools.AddItem(L"&Dependent all cards", [this](i32) mutable {
		for (auto & category : Categories) {
			if (category->Gui.Window)
				category->Gui.Window->Close();
		}
		DirtyXml.Restart();
	});
	// make window always on top
	Gui.Menu.AlwaysOnTop = Gui.Menu.Tools.AddItem(L"&Always on top", [this](i32 id) mutable {
		Gui.Window->AlwaysOnTop(!Gui.Window->AlwaysOnTop());
		Gui.Menu.Tools.CheckItem(Gui.Menu.AlwaysOnTop, Gui.Window->AlwaysOnTop());
		DirtyXml.Restart();
	});
	Gui.Menu.Tools.CheckItem(Gui.Menu.AlwaysOnTop, Settings.AlwaysOnTop);
	// initialize
	StringLocal<64> selectedCategory = Settings.SelectedTab->Value();

	Gui.Window->Init();

	if (selectedCategory.Length()) {
		Debug::Button dummy(Settings.SelectedTab->Value(), L"Dummy", L"", Debug::Sources::NONE);
		ProcessFrame();
		Gui.Tabs->Index(Gui.Tabs->Name2Index(selectedCategory.View()));
	}

	SinceLastRefresh.Start();

	// tray
	if (Settings.Tray) {
		bool dummy;
		Gui.Window->OnClose(dummy);
	}
}

inline void DebugGui::Save() {
	DebugSerializerImpl::Instance().Serialize(Settings.Left);
	DebugSerializerImpl::Instance().Serialize(Settings.Top);
	DebugSerializerImpl::Instance().Serialize(Settings.Width);
	DebugSerializerImpl::Instance().Serialize(Settings.Height);
	DebugSerializerImpl::Instance().Serialize(Settings.SelectedTab);
	DebugSerializerImpl::Instance().Serialize(Settings.AlwaysOnTop);
	DebugSerializerImpl::Instance().Serialize(Settings.Tray);

	DirtyXml.Reset();
}

inline DebugGui::DebugGui(Debug::IVariables * variables) {
	static constexpr Debug::Sources notFromDebug = Debug::Sources::ALL ^ Debug::Sources::GUI;
	// initialize synchronized values
	AABox2I desktop = Environment::VirtualDesktop::Box();
	Settings.RefreshRate = Debug::u64Range(500, 5, 5000,   L"Console", L"Refresh rate of gui [ms]",           L"Time delay before winapi refresh.");
	Settings.SaveRate    = Debug::u64Range(500, 5, 5000,   L"Console", L"Refresh rate of serialization [ms]", L"");
	Settings.Left        = Debug::i32Range(400, 200, 8000, L"Console", L"Window Position Left",               L"", notFromDebug);
	Settings.Top         = Debug::i32Range(400, 200, 8000, L"Console", L"Window Position Top",                L"", notFromDebug);
	Settings.SelectedTab = Debug::String(String(),         L"Console", L"Selected Tab",                       L"", notFromDebug);
	Settings.AlwaysOnTop = Debug::Boolean(false,           L"Console", L"Always On Top",                      L"Should be debug window always on top.", notFromDebug);
	Settings.Tray        = Debug::Boolean(false,           L"Console", L"Tray",                               L"Should be debug window always on top.", notFromDebug);
	Settings.Width       = Debug::i32Range(600, desktop.Min.x, desktop.Max.x,  L"Console", L"Window Dimension Width",  L"", notFromDebug);
	Settings.Height      = Debug::i32Range(800, desktop.Min.y, desktop.Max.y,  L"Console", L"Window Dimension Height", L"", notFromDebug);

	// asynchronous registration of category
	Action<Debug::ICategoryBase *> registerCategory([this](Debug::ICategoryBase * c) {
		this->Async.Add(c);
	});

	variables->Lock();
	// register existing category, if any
	variables->Enumerate(registerCategory);
	// register delegate, if new category is being created
	variables->OnCategoryCreate += registerCategory;
	variables->UnLock();

	static DD::Concurrency::ThreadPool::TaskLocal<16> initTask([this]() {
		AsyncInit();
	});

	DebugSerializerImpl::Instance().OnSerializerChanged += [this](Debug::IVariableBase * v, bool b) {
		// if variable cannot be handled from debug, it does not have controls for serialization
		if (v->Source() & DD::Debug::Sources::GUI)
			OnSerializerUpdate(v, b);
	};
	DebugSerializerImpl::Instance().OnTrackerChanged += [this](Debug::IVariableBase * v, bool b) {
		// if variable cannot be handled from debug, it does not have controls for serialization
		if (v->Source() & DD::Debug::Sources::GUI)
			OnTrackerUpdate(v, b);
	};
}

inline void DebugVariablesImpl::InitGui() {
	if (DebugGui::Instance().Gui.Window != nullptr) {
		// push main window to front
		if (!DebugGui::Instance().Gui.Window->AlwaysOnTop()) {
			DebugGui::Instance().Gui.Window->AlwaysOnTop(true);
			DebugGui::Instance().Gui.Window->AlwaysOnTop(false);
		}

		// lock variables and copy them to be MT safe
		Debug::Variables()->Lock();
		List<DebugGuiCategory> categories;
		categories.Reserve(DebugGui::Instance().Categories.Size());
		for (DebugGuiCategory & category : DebugGui::Instance().Categories)
			categories.Add(category);
		Debug::Variables()->UnLock();

		// push all independent windows to front
		for (DebugGuiCategory & category : categories) {
			if (category->Gui.Window && !category->Gui.Window->AlwaysOnTop()) {
				category->Gui.Window->AlwaysOnTop(true);
				category->Gui.Window->AlwaysOnTop(false);
			}
		}
	}
}
#pragma endregion

/************************************************************************/
/* IVariableBase                                                        */
/************************************************************************/
#pragma region IVariableBase
void Debug::IVariableBase::ToString(DD::String & buffer) const {
	ToStringVisitor::Run(this, buffer);
}

void Debug::IVariableBase::Saved(bool save /* = true */) {
	if (Source() & Sources::SERIAL) {
		if (save)
			DebugSerializerImpl::Instance().Serialize(this);
		else
			DebugSerializerImpl::Instance().Remove(this);
	}
}

bool DD::Debug::IVariableBase::Saved() const {
	 return DebugSerializerImpl::Instance().IsRegistered(
		 const_cast<IVariableBase *>(this)
	 );
}

void DD::Debug::IVariableBase::Tracked(bool save) {
	if (Source() & Sources::SERIAL) {
		if (save)
			DebugSerializerImpl::Instance().Track(this);
		else
			DebugSerializerImpl::Instance().Untrack(this);
	}
}

bool DD::Debug::IVariableBase::Tracked() const {
	return DebugSerializerImpl::Instance().IsTracked(
		const_cast<IVariableBase *>(this)
	);
}

Debug::IVariableBase::IVariableBase(
	VariableTypes type,
	const Text::StringView16 & category,
	const Text::StringView16 & name,
	const Text::StringView16 & description,
	Sources source
) : _type(type)
	, _source(source)
	, _category(category)
	, _name(name)
	, _description(description)
	, OnValueChanged()
{}

#pragma endregion

/************************************************************************/
/* AssertBase                                                           */
/************************************************************************/
#pragma region Assert
template<Debug::Assert::Modes::Values>
static __forceinline void AssertPass(Debug::AssertBase *) { DD_WARNING("Not implemented assert mode."); }
template<Debug::Assert::Modes::Values>
static __forceinline void AssertFail(Debug::AssertBase *) { DD_WARNING("Not implemented assert mode."); }

#define __ASSERT_PASS(mode) AssertPass<Debug::Assert::Modes::mode>
#define __ASSERT_FAIL(mode) AssertFail<Debug::Assert::Modes::mode>
#define __DECLARE_ASSERT_PASS(mode) template<> static __forceinline void __ASSERT_PASS(mode)(Debug::AssertBase * assert)
#define __DECLARE_ASSERT_FAIL(mode) template<> static __forceinline void __ASSERT_FAIL(mode)(Debug::AssertBase * assert)

__DECLARE_ASSERT_PASS(DISABLED) { }
__DECLARE_ASSERT_FAIL(DISABLED) { }
__DECLARE_ASSERT_PASS(SILENT) { Concurrency::Atomic::i32::Inc(assert->ValidTicks); }
__DECLARE_ASSERT_FAIL(SILENT) { Concurrency::Atomic::i32::Inc(assert->InvalidTicks); }
__DECLARE_ASSERT_PASS(LOG) { __ASSERT_PASS(SILENT)(assert);  }
__DECLARE_ASSERT_FAIL(LOG) { __ASSERT_FAIL(SILENT)(assert); Debug::Log::PrintLine("ASSERT FAILED\n", assert->Description()); }
__DECLARE_ASSERT_PASS(BREAK) { __ASSERT_PASS(LOG)(assert); }
__DECLARE_ASSERT_FAIL(BREAK) { __ASSERT_FAIL(LOG)(assert); Debug::Break(); }
// __DECLARE_ASSERT_CASE_PASS(INTERACTIVE) { Atomic::i32::Inc(assert->InvalidTicks); }
// __DECLARE_ASSERT_CASE_FAIL(INTERACTIVE) { Atomic::i32::Inc(assert->ValidTicks); }
__DECLARE_ASSERT_PASS(FAIL) { __ASSERT_PASS(BREAK)(assert); }
__DECLARE_ASSERT_FAIL(FAIL) { __ASSERT_FAIL(BREAK)(assert); Debug::Fail(); }

#undef __ASSERT_PASS
#undef __ASSERT_FAIL
#undef __DECLARE_ASSERT_PASS
#undef __DECLARE_ASSERT_FAIL

#define __DECLARE_ASSERT_CASE(mode)\
case Debug::Assert::Modes::mode:\
	if (condition)\
		AssertPass<Debug::Assert::Modes::mode>(this);\
	else\
		AssertFail<Debug::Assert::Modes::mode>(this);\
  break;

void Debug::AssertBase::Check(bool condition) {
	switch (_mode) {
		DD_XLIST(__DECLARE_ASSERT_CASE, DISABLED, SILENT, LOG, BREAK, INTERACTIVE, FAIL);
		default: DD_WARNING("Invalid AssertMode"); Debug::Fail(); break;
	}
}
#undef __DECLARE_ASSERT_CASE

Debug::AssertBase::AssertBase(
	const char * condition,
	const char * function,
	const char * file,
	::DD::u32 line,
	const char * description,
	Modes mode,
	IVariables * vars
) : IVariableBase(
		VariableTypes::Assert,
		L"Asserts",
		StringLocal<128>::Format("{0}({1})", function, line),
		StringLocal<256>::Format(
			"{2}({3})\r\tAssert: {0}\r\tFunction: {1}\r\tFile: {2}({3})\r\t{4}",
			/*0*/ condition,
			/*1*/ function,
			/*2*/ file,
			/*3*/ line,
			/*4*/ description
		),
		Sources::ALL
	)
	, InvalidTicks(0)
	, ValidTicks(0)
	, _mode(mode)
{
	vars->Create(this);
}


Debug::Assert::Assert(
	const char * condition,
	const char * function,
	const char * file,
	::DD::u32    line,
	const char * description,
	Modes        mode,
	IVariables * vars
): Reference(
		new AssertBase(
			condition, function, file, line, description, mode, vars
		)
	)
{}

#pragma endregion

/************************************************************************/
/* BooleanBase                                                          */
/************************************************************************/
#pragma region Boolean
Debug::BooleanBase::BooleanBase(
	bool value,
	const Text::StringView16 & module,
	const Text::StringView16 & name,
	const Text::StringView16 & tooltip,
	Sources source,
	IVariables * vars
) : IVariableBase(VariableTypes::Boolean, module, name, tooltip, source)
	, _value(value)
{
	vars->Create(this);
}
#pragma endregion

/************************************************************************/
/* ButtonBase                                                           */
/************************************************************************/
#pragma region Button
bool Debug::ButtonBase::IsPressed(bool reset /*= true*/) {
	bool pressed = _buttonPressed;
	if (reset)
		_buttonPressed = false;

	return pressed;
}

void Debug::ButtonBase::Press(Sources s){
	if (_source & s) {
		_buttonPressed = true;
		OnValueChanged(this, s);
	}
}

Debug::ButtonBase::ButtonBase(
	const Text::StringView16 & module,
	const Text::StringView16 & name,
	const Text::StringView16 & tooltip,
	Sources source,
	IVariables * vars
) : IVariableBase(VariableTypes::Button, module, name, tooltip, source)
	, _buttonPressed(false)
{
	vars->Create(this);
}
#pragma endregion


/************************************************************************/
/* EnumBase                                                             */
/************************************************************************/
#pragma region Enum
Debug::IEnumBase::IEnumBase(
	const Text::StringView16 & module,
	const Text::StringView16 & name,
	const Text::StringView16 & tooltip,
	Sources source
)
	: IVariableBase(VariableTypes::Enum, module, name, tooltip, source)
{}
#pragma endregion

/************************************************************************/
/* LogBase                                                              */
/************************************************************************/
#pragma region Log
void Debug::LogBase::tick() {
	Concurrency::Atomic::i32::Inc(Ticks);
}

Debug::LogBase::LogBase(
	const char * name,
	const char * description,
	const char * function,
	const char * file,
	DD::i32 line,
	Modes mode,
	IVariables * vars
) : IVariableBase(
		VariableTypes::Log,
		L"Logs",
		StringLocal<256>::Format("{0} ({1}:{2})", name, function, line),
		StringLocal<256>::Format(
			"Log {0}\r  Description {1}\r  Funtion: {2}\r  File: {3}({4})\r\r{5}",
			/*0*/ name,
			/*1*/ description,
			/*2*/ function,
			/*3*/ file,
			/*4*/ line,
			/*5*/ Modes::Help()
		),
		Debug::Sources::ALL
	)
	, _mode(mode)
	, Stamp(StringLocal<128>::Format("{0}({1}):", file, line))
	, Ticks(0)
{
	vars->Create(this);
}

void Debug::LogBase::printEx(const DD::String & log) {
	DD_WARNING("Not implemented");
}

Debug::Log::Log(
	const char * name,
	const char * description,
	const char * function,
	const char * file,
	DD::i32 line,
	Modes mode,
	IVariables * vars
) : Reference(new LogBase(name, description, function, file, line, mode, vars))
{}

void Debug::Log::flush() {
	OutputDebugStringW(getBuffer().Terminate());
	getBuffer().Clear();
}
#pragma endregion

/************************************************************************/
/* PathBase                                                             */
/************************************************************************/
#pragma region Path
DD::Debug::PathBase::PathBase(
	Modes mode,
	const Text::StringView16 & path,
	const Text::StringView16 & module,
	const Text::StringView16 & name,
	const Text::StringView16 & tooltip,
	Sources source,
	IVariables * vars
) : IVariableBase(VariableTypes::Path, module, name, tooltip, source)
	, _path(path)
	, _mode(mode)
{
	vars->Create(this);
}
#pragma endregion

/************************************************************************/
/* Options                                                              */
/************************************************************************/
#pragma region Options
void DD::Debug::OptionsBase::Add(const Text::StringView16 & string, Sources source) {
	if (_source & source) {
		_items.Append() = string;
		CollectionChangedDescriptor change = {
			CollectionChangedDescriptor::Changes::ADD,
			(DD::i32)(_items.Size() - 1)
		};
		OnCollectionChanged(this, change);
		if (_savedValue == string)
			Index(_items.Size() - 1, Sources::SERIAL);
	}
}

void DD::Debug::OptionsBase::Insert(size_t index, const Text::StringView16 & string, Sources source) {
	if (_source & source) {
		_items.Insert(index) = string;
		CollectionChangedDescriptor change = {
			CollectionChangedDescriptor::Changes::INSERT,
			(DD::i32)index
		};
		OnCollectionChanged(this, change);
		Index(_index + 1, _source);
	}
}

void DD::Debug::OptionsBase::Free(Sources source) {
	if (_source & source) {
		_items.Free();
		_index = -1;
		CollectionChangedDescriptor change = {
			CollectionChangedDescriptor::Changes::CLEAR,
			0
		};
		OnCollectionChanged(this, change);
	}
}

void DD::Debug::OptionsBase::Index(size_t index, Sources source) {
	if (_source & source) {
		_index = (DD::i32)index;
		OnValueChanged(this, source);
	}
}

void DD::Debug::OptionsBase::RemoveAt(size_t index, Sources source) {
	if (_source & source) {
		_items.RemoveAt(index);
		CollectionChangedDescriptor change = {
			CollectionChangedDescriptor::Changes::REMOVE,
			(DD::i32)index
		};
		OnCollectionChanged(this, change);
		if (index <= _index)
			Index(_index - 1, _source);
	}
}

DD::Debug::OptionsBase::OptionsBase(
	const Text::StringView16 & module,
	const Text::StringView16 & name,
	const Text::StringView16 & tooltip,
	Sources source,
	IVariables * vars
) : IVariableBase(Debug::VariableTypes::Options, module, name, tooltip, source)
	, _index(0)
{
	vars->Create(this);
}

DD::Debug::Options::Options(
	const Text::StringView16 & module,
	const Text::StringView16 & name,
	const Text::StringView16 & tooltip,
	Sources source,
	IVariables * vars
) : Reference(new OptionsBase(module, name, tooltip, source, vars))
{}
#pragma endregion

Debug::IVariables * Debug::Variables() {
	// initialize serializer
	DebugSerializerImpl::Instance();
	return DebugVariablesImpl::Instance();
}

void Debug::Break() {
	if (IsDebuggerPresent())
		__debugbreak();
}

void Debug::Fail() {
	*(int*)nullptr = 0;
}

