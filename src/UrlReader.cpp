#include <DD/Net/UrlReader.h>
#include <DD/Unique.h>
#include <Urlmon.h>
#include <new.h>

#pragma comment( lib, "Urlmon.lib" )

using namespace DD;
using namespace DD::Net;

/************************************************************************/
/* Helpers                                                              */
/************************************************************************/

/** Helper for initialization of networking. */
struct Initializer {
public: // statics
	/** Singleton. */
	static Initializer & Instance() { static Initializer instance; return instance; }

private: // constructors
	/**
	 * Constructor.
	 * https://docs.microsoft.com/en-us/previous-versions/windows/embedded/ms886303(v=msdn.10)
	 */
	Initializer() { ::CoInitialize(NULL); }

public: // constructors
	/**
	 * Destructor.
	 * https://docs.microsoft.com/en-us/previous-versions/windows/embedded/ms886943(v=msdn.10)
	 */
	~Initializer() { ::CoUninitialize(); }
};

/** Traits for releasing MS pointers. */
struct MSUniqueTraits {
	/** Release pointer. */
	static void Delete(IUnknown * item) { item->Release(); }
};

/** Alias for unique pointer with MS traits. */
template<typename TYPE>
using MSUnique = Unique<TYPE, MSUniqueTraits>;

DD_TODO_STAMP("Url reader - POST method does not work.");
struct PostCallback : public IBindStatusCallback {
public: // properties
	/** */
	Text::StringView8 _postData;

public: // IUknown
	virtual HRESULT STDMETHODCALLTYPE QueryInterface(REFIID riid, void ** ppvObject) { return E_NOINTERFACE; }
	virtual ULONG STDMETHODCALLTYPE AddRef(void) { return 1; }
	virtual ULONG STDMETHODCALLTYPE Release(void) { return 1; }

public: // IBindStatusCallback
	/** https://docs.microsoft.com/en-us/previous-versions/windows/internet-explorer/ie-developer/platform-apis/ms775065(v=vs.85) */
	virtual HRESULT STDMETHODCALLTYPE OnStartBinding(DWORD dwReserved, __RPC__in_opt IBinding * pib) { return S_OK; }
	/** https://docs.microsoft.com/en-us/previous-versions/windows/internet-explorer/ie-developer/platform-apis/ms775059(v=vs.85) */
	virtual HRESULT STDMETHODCALLTYPE GetPriority(__RPC__out LONG * pnPriority) { return S_OK; }
	/** https://docs.microsoft.com/en-us/previous-versions/windows/internet-explorer/ie-developer/platform-apis/ms775062(v=vs.85) */
	virtual HRESULT STDMETHODCALLTYPE OnLowResource(DWORD reserved) { throw; }
	/** https://docs.microsoft.com/en-us/previous-versions/windows/internet-explorer/ie-developer/platform-apis/ms775064(v=vs.85) */
	virtual HRESULT STDMETHODCALLTYPE OnProgress(ULONG ulProgress, ULONG ulProgressMax, ULONG ulStatusCode, __RPC__in_opt LPCWSTR szStatusText) { return S_OK; }
	/** https://docs.microsoft.com/en-us/previous-versions/windows/internet-explorer/ie-developer/platform-apis/ms775066(v=vs.85) */
	virtual HRESULT STDMETHODCALLTYPE OnStopBinding(HRESULT hresult, __RPC__in_opt LPCWSTR szError) { return S_OK; }
	/** https://docs.microsoft.com/en-us/previous-versions/windows/internet-explorer/ie-developer/platform-apis/ms775061(v=vs.85) */
	virtual HRESULT STDMETHODCALLTYPE OnDataAvailable(DWORD grfBSCF, DWORD dwSize, FORMATETC * pformatetc, STGMEDIUM * pstgmed) { return S_OK; }
	/** https://docs.microsoft.com/en-us/previous-versions/windows/internet-explorer/ie-developer/platform-apis/ms775063(v=vs.85) */
	virtual HRESULT STDMETHODCALLTYPE OnObjectAvailable(__RPC__in REFIID riid, __RPC__in_opt IUnknown * punk) { return S_OK; }
	/** https://docs.microsoft.com/en-us/previous-versions/windows/internet-explorer/ie-developer/platform-apis/ms775058(v=vs.85) */
	virtual HRESULT STDMETHODCALLTYPE GetBindInfo(DWORD * grfBINDF, BINDINFO * pbindinfo) {
		//
		*grfBINDF |= BINDF_PULLDATA | BINDF_GETNEWESTVERSION | BINDF_NOWRITECACHE;

		// Set up the BINDINFO data structure
		// https://docs.microsoft.com/en-us/previous-versions/windows/internet-explorer/ie-developer/platform-apis/ms774966(v=vs.85)
		pbindinfo->cbSize = sizeof(BINDINFO);
		pbindinfo->dwBindVerb = BINDVERB_POST;

		pbindinfo->szExtraInfo = NULL;

		pbindinfo->grfBindInfoF = 0;
		pbindinfo->szCustomVerb = NULL;

		// https://docs.microsoft.com/en-us/windows/win32/api/objidl/ns-objidl-ustgmedium-r1
		pbindinfo->stgmedData.pUnkForRelease = (LPBINDSTATUSCALLBACK)this; // maintain control over the data.
		// this is the only medium urlmon supports right now
		// https://docs.microsoft.com/en-us/windows/win32/api/objidl/ne-objidl-tymed
 		memset(&pbindinfo->stgmedData, 0, sizeof(STGMEDIUM));
 		pbindinfo->stgmedData.tymed = TYMED_HGLOBAL;
 		//pbindinfo->stgmedData.hGlobal = (HGLOBAL)_postData.begin();
 		//pbindinfo->cbstgmedData = (DWORD)_postData.Length() + 1;

//
		return S_OK;
	}

public: // constructors
	/** */
	PostCallback(Text::StringView8 data) : _postData(data) {}
};


/************************************************************************/
/* UrlBlockReader                                                       */
/************************************************************************/

/** Native handles for Windows platform */
struct UrlBlockReader::NativeView {
	/** Stream handle. */
	MSUnique<IStream> StreamHandle = nullptr;
	/** Callback handle. */
	Unique<PostCallback> Callback = nullptr;
};


bool UrlBlockReader::Reset() {
	// prepare for networking
	Initializer::Instance();

	Native()->StreamHandle.Release();

	// https://docs.microsoft.com/en-us/previous-versions/windows/internet-explorer/ie-developer/platform-apis/ms775127(v=vs.85)
	HRESULT hr = URLOpenBlockingStreamW(NULL, _url.begin(), Native()->StreamHandle.Address(), 0, Native()->Callback.Ptr());
	return SUCCEEDED(hr) && Next();
}

bool UrlBlockReader::Next() {
	DWORD bytesRead = 0;
	HRESULT hr = Native()->StreamHandle->Read(_buffer.begin(), BUFFER_SIZE, &bytesRead);
	_buffer.ResizeUnsafe(bytesRead);

	return SUCCEEDED(hr) && bytesRead /*&& hr != S_FALSE*/;
}

UrlBlockReader::NativeView * UrlBlockReader::Native() {
	return reinterpret_cast<UrlBlockReader::NativeView *>(_native);
}

UrlBlockReader::UrlBlockReader() {
	static_assert(sizeof(_native) == sizeof(NativeView));
	new (_native) NativeView();
}

UrlBlockReader::UrlBlockReader(const String & url, Text::StringView8 post)
	: UrlBlockReader() {
	_url = url;
	if (post != nullptr)
		Native()->Callback = new PostCallback(post);
}

UrlBlockReader::~UrlBlockReader() {
	Native()->~NativeView();
}


/************************************************************************/
/* UrlReader                                                            */
/************************************************************************/

bool UrlReader::Next() {
	if (++_index < _blockReader.Get().Length()) {
		return true;
	}

	if (_blockReader.Next()) {
		_index = 0;
		return true;
	}

	return false;
}
