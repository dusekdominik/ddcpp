#include <DD/Object.h>
#include <DD/Matrices.h>
#include <DD/Debug.h>
#include <DD/Gpu/Devices.h>
#include <DD/Simulation/ICullableModel.h>

DD_TODO_STAMP("Fix camera update what about model view projection transform.");
DD_TODO_STAMP("Check transformation matrix to be not trasnapsed in shaders.");

using namespace DD;
using namespace DD::Simulation;


ObjectBase & ObjectBase::operator=(const ObjectBase & o) {
	_model = o._model;
	_position = o._position;
	_scale = o._scale;
	_rotation = o._rotation;
	return *this;
}


void ObjectBase::updateConstantBuffer() {
	if (_changed) {
		_changed = false;
		_cbuffer->ObjectTransformation = Transform();
		_cbuffer.Update();
	}
	_cbuffer.Bind2VS();
}


Sphere3F ObjectBase::BoundingSphere() const {
	const ICullableModel * model = static_cast<const ICullableModel*>(_model.Ptr());
	const Sphere3F & modelSphere =  model->GetBoundingSphere();
	f32 radius = Math::Max(_scale.x, _scale.y, _scale.z);

	return Sphere3F(modelSphere.Center + _position, radius * modelSphere.Radius);
}


AABox3F ObjectBase::BoundingAABox() const {
	const ICullableModel * model = static_cast<const ICullableModel *>(_model.Ptr());
	AABox3F modelBox =  model->GetBoundingBox();
	modelBox.Min += _position;
	modelBox.Max += _position;

	DD_TODO_STAMP("Compute transformation");

	return modelBox;
}


ObjectBase & ObjectBase::Expire() {
	// do not show expired objects
	_enabled = false;
	// set expiration
	_expired = true;
	// notify listeners
	OnObjectExpiration(this);

	return *this;
}


ObjectBase & ObjectBase::RotateX(f32 f) {
	_changed = true;
	_rotation *= Matrices::RotationX(f);

	return *this;
}


ObjectBase & ObjectBase::RotateY(f32 f) {
	_changed = true;
	_rotation *= Matrices::RotationY(f);

	return *this;
}


ObjectBase & ObjectBase::RotateZ(f32 f) {
	_changed = true;
	_rotation *= Matrices::RotationZ(f);;

	return *this;
}


ObjectBase & ObjectBase::RotateAxis(f32 f, const float3 & axis) {
	_changed = true;
	_rotation *= Matrices::RotationAxis(f, axis);

	return *this;
}


ObjectBase & ObjectBase::Translate(const float3 & v) {
	_changed = true;
	Position() += v;

	return *this;
}


ObjectBase & ObjectBase::Translate(f32 x, f32 y, f32 z) {
	return Translate(float3(x, y, z));
}


ObjectBase & ObjectBase::Draw() {
	if (Enabled()) {
		updateConstantBuffer();
		_model->Draw(_modelDrawArgs);
	}

	return *this;
}


ObjectBase & ObjectBase::DrawCulled(const Frustum3F & frustum) {
	if (Enabled()) {
		updateConstantBuffer();
		_model->DrawFrustumCulled(frustum, _cbuffer->ObjectTransformation, _modelDrawArgs);
	}

	return *this;
}


float4x4 ObjectBase::Transform() const {
	float4x4 m;

	m.Cols(0) = _rotation.Cols(0)[0] * _scale.x;
	m.Cols(0)[1] = _rotation.Cols(0)[1] * _scale.x;
	m.Cols(0)[2] = _rotation.Cols(0)[2] * _scale.x;
	m.Cols(0)[3] = 0.f;

	m.Cols(1)[0] = _rotation.Cols(1)[0] * _scale.y;
	m.Cols(1)[1] = _rotation.Cols(1)[1] * _scale.y;
	m.Cols(1)[2] = _rotation.Cols(1)[2] * _scale.y;
	m.Cols(1)[3] = 0.f;

	m.Cols(2)[0] = _rotation.Cols(2)[0] * _scale.z;
	m.Cols(2)[1] = _rotation.Cols(2)[1] * _scale.z;
	m.Cols(2)[2] = _rotation.Cols(2)[2] * _scale.z;
	m.Cols(2)[3] = 0.f;

	m.Cols(3)[0] = _position.x;
	m.Cols(3)[1] = _position.y;
	m.Cols(3)[2] = _position.z;
	m.Cols(3)[3] = 1.f;

	return  m;
}


ObjectBase & ObjectBase::Effect(bool enabled) {
	if (_effect != enabled) {
		_effect = enabled;
		OnEffectChange(this, enabled);
	}

	return *this;
}


ObjectBase::ObjectBase(Gpu::Device & device, Simulation::Model & model, const String & name)
	: _model(model)
	, _name(name)
	, _rotation(DiagonalMatrix<f32, 3>(1.f))
	, _position(0.f)
	, _scale(1.f, 1.f, 1.f)
	, _changed(true)
	, _enabled(false)
	, _effect(false)
{}


ObjectBase::~ObjectBase() {
	//DD_DEBUG_LOG("Object/Release", _name);
}


Object::Object(Gpu::Device & device, Simulation::Model & model, const String & name)
	: Reference(new ObjectBase(device, model, name))
{
	Object reference = *this;
	device->AddCommand([reference](Gpu::Device & device) mutable {
		reference->_cbuffer.Init(device);
		reference->_enabled = true;
	});
}
