#include <DD/Concurrency/ThreadPool.h>
#include <DD/GCode/Builder.h>
#include <DD/GCode/PolylineBuilder.h>
#include <DD/Debug.h>
#include <DD/QuickSort.h>
#include <DD/Color.h>

#include <algorithm>

using namespace DD;
using namespace DD::GCode;

struct Builder::MillingScope {
	/** Object where to push codes. */
	Builder * _builder;
	/** Millimeters/Inches. */
	MachineState::MeasurementModes _mmode;
	/** Absolute/Relative. */
	MachineState::PositionModes _pmode;
	/** Tool-head state. */
	MachineState::HeadModes _hmode;
	/** FeedRates. */
	i32 _fri, _frw;
	/** */
	f64 _safeHeight;
	/** Constructor. */
	MillingScope(Builder * builder, const MillingParams & params, const double3 & startPosition)
		: _builder(builder)
		, _mmode(_builder->_tracker.CurrentState.MeasureMode)
		, _pmode(_builder->_tracker.CurrentState.PositionMode)
		, _fri(_builder->_tracker.CurrentState.FeedRates[IDLE])
		, _frw(_builder->_tracker.CurrentState.FeedRates[WORK])
		, _hmode(builder->_tracker.CurrentState.HeadMode)
		, _safeHeight(params.SafeHeight)
	{
		_builder->AddCommand(Codes::Millimeters);
		_builder->AddCommand(Codes::Absolute);
		_builder->AddToolPower(params.ToolHeadPower);
		_builder->AddFeedRate(params.FeedRateIdle, IDLE);
		_builder->AddFeedRate(params.FeedRateWork, WORK);

		// go to start position
		// firstly go to the safe height
		builder->AddGoZ(params.SafeHeight, IDLE);
		// select xy position
		builder->AddGoX(startPosition.x, IDLE);
		builder->AddGoY(startPosition.y, IDLE);
		// start tool-head before going down
		if (!params.Laser)
			builder->AddTurnOn();
		// go down
		builder->AddGoZ(startPosition.z, IDLE);
		if (params.Laser)
			builder->AddTurnOn();
	}
	/** Destructor. */
	~MillingScope() {
		// restore state
		if (_mmode == MachineState::MeasurementModes::Inches)
			_builder->AddCommand(Codes::Inches);
		if (_pmode == MachineState::PositionModes::Relative)
			_builder->AddRelative();
		if (_frw)
			_builder->AddFeedRate(_frw, WORK);
		if (_fri)
			_builder->AddFeedRate(_fri, IDLE);
		if (_hmode != MachineState::HeadModes::On)
			_builder->AddTurnOff();

		_builder->AddGoZ(_safeHeight);
	}
};

void Command::Feed(String & buffer) const {
	switch (Code) {
	case Codes::Go0:         buffer.Append("G0");   break;
	case Codes::Go1:         buffer.Append("G1");   break;
	case Codes::Dwell:       buffer.Append("G4");   break;
	case Codes::Inches:      buffer.Append("G20");  break;
	case Codes::Millimeters: buffer.Append("G21");  break;
	case Codes::Absolute:    buffer.Append("G90");  break;
	case Codes::Relative:    buffer.Append("G91");  break;
	case Codes::HeadOn:      buffer.Append("M3");   break;
	case Codes::HeadOff:     buffer.Append("M5");   break;
	case Codes::FanOn:       buffer.Append("M106"); break;
	case Codes::FanOff:      buffer.Append("M107"); break;
	case Codes::_Comment:    buffer.Append(';');    break;
	default:
		DD_WARNING("Undefined command");
	}

	if (!(X != X))
		buffer.AppendBatch(" X", X);
	if (!(Y != Y))
		buffer.AppendBatch(" Y", Y);
	if (!(Z != Z))
		buffer.AppendBatch(" Z", Z);
	if (F)
		buffer.AppendBatch(" F", F);

	if (Comment.IsNotEmpty())
		buffer.Append(Comment);

	if (Code == Codes::FanOn || Code == Codes::HeadOn || Code == Codes::Dwell)
		if (S != -1)
			buffer.AppendBatch(" S", S);

	DD_TODO_STAMP("S, P must have indication if set or not.")

		buffer.Append('\n');
}


void Builder::AddGo(
	f64 x /*= 0.0*/,
	f64 y /*= 0.0*/,
	f64 z /*= 0.0*/,
	u8 index /*= 0*/,
	i32 f /*= 0*/) {
	Command cmd;
	// set code according to index
	switch (index) {
	case 0: cmd.Code = Codes::Go0; break;
	case 1: cmd.Code = Codes::Go1; break;
	default: DD_WARNING("Undefined index of Go GCode."); break;
	}

	cmd.X = x;
	cmd.Y = y;
	cmd.Z = z;
	cmd.F = f;

	AddCommand(cmd);
}


void Builder::AddHeader() {
	AddRelative();
	AddMillimeters();
	AddFanOn();
}


String Builder::ToString() const {
	// estimate at most 100 chars per line
	String string(_commands.Size() * 100);
	for (const Command & code : _commands) {
		code.Feed(string);
	}

	return string;
}


Svg::Document Builder::ToSvg() {
	bool draw = false;
	double2 cursor = 0;
	Math::Interval2D box;

	Svg::Path path;
	path.AddMoveRelative(0.0);
	for (const Command & code : _commands) {
		switch (code.Code) {
		case Codes::Go0:
			if (draw)
				path.AddLineRelative({ code.X, code.Y });
			else
				path.AddMoveRelative({ code.X, code.Y });
			cursor += double2{ code.X, code.Y };
			box.Include(cursor);
			break;
		case Codes::HeadOn:
			draw = true;
			break;
		case Codes::HeadOff:
			draw = false;
			break;
		default:
			break;
		}
	}
	path.Commands[0].Move.Target = -box.Min;

	Svg::Document document(box.Size());
	document->AddPath(path, L"none", L"black");

	return document;
}


void Builder::Save(const FileSystem::Path & path) {
	// write
	Array<i8> data = Array<i8>::FromSequence(DD::Sequencer::Drive(ToString()).Cast<i8>());
	path.AsFile().Write( ConstByteView{data.begin(), data.Length()});
}


void Builder::AddFanOn(i32 power) {
	Command fanOn(Codes::FanOn);
	fanOn.S = power;
	AddCommand(fanOn);
}


void DD::GCode::Builder::AddTurnOn(i32 power) {
	Command headOn(Codes::HeadOn);
	headOn.S = power;
	AddCommand(headOn);
}


void Builder::AddToolPower(i32 power /*= -1*/) {
	// value is set and power is has not been already set
	if (power != -1 && _tracker.CurrentState.HeadPower != power) {
		AddTurnOn(power);
		AddTurnOff();
	}
}


void Builder::DashedLineModel(f64 dash, f64 undash, const MillingParams & mpOutline, Sequencer::ISequencer2<Array<double2>> * polylines) {
	for (const Array<double2> & polyline : Sequencer::Drive(polylines)) {
		f64 dasha = dash;
		f64 undasha = undash;
		bool mode = true;
		List<double2> pointacc;
		for (Collections::Bin<double2, double2> segment : Sequencer::Drive(polyline).Revolve<2>()) {
		retarget:
			if (mode)
			pointacc.Add(segment.Head);
			double2 diff = segment.Tail.Head - segment.Head;
			f64 size = Math::Norm(diff);
			if (mode) { // dash
				if (size < dasha) {
					dasha -= size;
				}
				else {
					pointacc.Add(segment.Head = segment.Head + diff * (dasha / size));
					CutPolylineNative(mpOutline, pointacc);
					pointacc.Clear();
					dasha = dash;
					mode = !mode;
					goto retarget;
				}
			}
			else { // undash
				if (size < undasha) {
					undasha -= size;
				}
				else {
					segment.Head = segment.Head + diff * (undasha / size);
					undasha = undash;
					mode = !mode;
					goto retarget;
				}
			}
		}
	}
}


void Builder::CutAndFillLineModel(
	const MillingParams & mpOutline,
	const MillingParams & mpFill,
	PolylineInput polylines,
	bool invert/* = false*/
) {
	// set of naive lines
	List<Math::LineSegment2D> lines(4096);
	Math::Interval2D box;

	for (const Polyline & polyline : polylines) {
		// cut outline
		for (u32 i = 0; i < mpOutline.LaserPasses; ++i)
			CutPolylineNative(mpOutline, polyline);
		// convert to segments
		for (const Collections::Bin<double2, double2> & segment : Sequencer::Drive(polyline).Revolve<2>()) {
			box.Include(segment.Head);
			box.Include(segment.Tail.Head);
			lines.Add({ segment.Head, segment.Tail.Head});
		}
	}
	AddComment(L"GREEN");

	if (!box.IsValid()) {
		DD_WARNING("Input is not valid, it did not generate any bbox.");
			return;
	}

	if (mpFill.FillStep <= 1E-3) {
		//DD_WARNING("Parameters are not valid.")
		return;
	}

	// create helper for look-up the segments corresponding with the filing line
	using Segment = List<const Math::LineSegment2D *, 1024>;
	const f64 height = box.Max.y - box.Min.y;
	const size_t gridSize = (size_t)(height / mpFill.FillStep) + 1;
	List<Segment> grid;
	grid.Resize(gridSize);
	// for each line select appropriate intersection candidates
	for (const Math::LineSegment2D & edge : lines) {
		f64 min = Math::Min(edge.A.y, edge.B.y) - box.Min.y;
		f64 max = Math::Max(edge.A.y, edge.B.y) - box.Min.y;
		size_t begin = (size_t)Math::Max(0.0, Math::Round(min / mpFill.FillStep));
		size_t end = (size_t)Math::Min(gridSize - 1.0, Math::Round(max / mpFill.FillStep));
		for (size_t i = begin; i <= end; ++i)
			grid[i].Add(&edge);
	}

	// task list
	List<Concurrency::ThreadPool::Future<List<f64>>> futures(grid.Size());
	// switch for inverting direction, when true toolhead moves from left to right
	bool fwd = true;

	// create new tasks for each line
	for (f64 y = box.Min.y; y <= box.Max.y; y += mpFill.FillStep) {
		futures.Add([&box, &grid, &mpFill, y, fwd, invert]() {
			// set corresponding index to the grid
			const size_t index = (size_t)Math::Round((y - box.Min.y) / mpFill.FillStep);
			// buffer for intersections
			List<f64, 1024> points;
			points.Reserve(grid[index].Size());

			for (const Math::LineSegment2D * edge : grid[index]) {
				// skip non intersecting
				if (edge->A.y < y && edge->B.y < y)
					continue;

				// skip non intersecting
				if (edge->A.y > y && edge->B.y > y)
					continue;

				// points on the line
				if ((y == edge->A.y && y == edge->B.y)) {
					points.Add(edge->A.x);
					points.Add(edge->B.x);
				}
				// simple intersection
				else {
					DD::f64 x = Math::Lerp2Lerp(edge->A.y, edge->B.y, edge->A.x, edge->B.x, y);
					points.Add(x);
				}
			}

			if (invert)
				points.Add(box.Min.y, box.Max.y);

			if (fwd)
				std::sort(points.begin(), points.end());
			else
				std::sort(points.begin(), points.end(), std::greater<>());

			return points;
		});

		fwd = !fwd;
	}


	// do filling part
	double2 size = box.Size();
	fwd = true;
	// helper index for selecting precomputed lines
	size_t index = 0;
	// filling padding
	const f64 padding = mpFill.BitDiameter * 0.5;
	// add overlaps for each line to avoid toolhead to be slowing down during filling
	const	f64 laserPadding = 0;//mpFill.Laser ? (fwd ? 5.0 : -5.0) : 0.0;

	// convert precomputed values to gcode
	for (f64 y = box.Min.y; y <= box.Max.y; y += mpFill.FillStep) {
		// select points
		const List<f64> & points = futures[index++]->Result(1);
		if (!points.IsEmpty()) {
			AddGoY(y, 1, mpFill.FeedRateWork);
			AddGoX(points[0] - laserPadding, WORK);
			// each second point set toolhead on
			for (size_t pindex = fwd ? 1 : 2 - !(points.Size() % 2); pindex < points.Size(); pindex += 2) {
				f64 xA = points[pindex - 1];
				f64 xB = points[pindex];
				f64 xC = (xA + xB) * 0.5;

				if (!fwd)
					Swap(xA, xB);
				// padding is applicable
				if (padding == 0.0 || (xA + padding < xC)) {
					CutLineNative(mpFill, { { xA + padding, y }, { xB - padding, y } });
				}
				else {
					CutPointNative(mpFill, double2{ xC, y });
				}
			}

			AddGoX(_tracker.CurrentState.Position.x + laserPadding, WORK);
		}

		fwd = !fwd;
	}
}

void DD::GCode::Builder::CF2(const MillingParams & mpFill, PolylineInput polylines, bool perSlice) {

	// set of naive lines
	List<Math::LineSegment2D> lines(4096);
	Math::Interval2D box;

	for (const Polyline & polyline : polylines) {
		// convert to segments
		for (const Collections::Bin<double2, double2> & segment : Sequencer::Drive(polyline).Revolve<2>()) {
			box.Include(segment.Head);
			box.Include(segment.Tail.Head);
			lines.Add({ segment.Head, segment.Tail.Head });
		}
	}

	if (!box.IsValid()) {
		DD_WARNING("Input is not valid, it did not generate any bbox.");
		return;
	}

	if (mpFill.FillStep <= 1E-3) {
		//DD_WARNING("Parameters are not valid.")
		return;
	}

	// create helper for look-up the segments corresponding with the filing line
	using Segment = List<const Math::LineSegment2D *, 1024>;
	const f64 height = box.Max.y - box.Min.y;
	const size_t gridSize = (size_t)(height / mpFill.FillStep) + 2;
	List<Segment> grid;
	grid.Resize(gridSize);
	// for each line select appropriate intersection candidates
	for (const Math::LineSegment2D & edge : lines) {
		f64 min = Math::Min(edge.A.y, edge.B.y) - box.Min.y;
		f64 max = Math::Max(edge.A.y, edge.B.y) - box.Min.y;
		size_t begin = (size_t)Math::Max(0.0, Math::Round(min / mpFill.FillStep));
		size_t end = (size_t)Math::Min(gridSize - 1.0, Math::Round(max / mpFill.FillStep));
		for (size_t i = begin; i <= end; ++i)
			grid[i].Add(&edge);
	}

	// task list
	List<Concurrency::ThreadPool::Future<List<f64>>> futures(grid.Size());
	// switch for inverting direction, when true toolhead moves from left to right
	bool fwd = true;

	// create new tasks for each line
	for (f64 y = box.Min.y + mpFill.FillStep * 0.5; y <= box.Max.y; y += mpFill.FillStep) {
		futures.Add([&box, &grid, &mpFill, y, fwd/*, invert*/]() {
			// set corresponding index to the grid
			const size_t index = (size_t)Math::Round((y - box.Min.y) / mpFill.FillStep);
			// buffer for intersections
			List<f64, 1024> points;
			points.Reserve(grid[index].Size());

			for (const Math::LineSegment2D * edge : grid[index]) {
				// skip non intersecting
				if (edge->A.y < y && edge->B.y < y)
					continue;

				// skip non intersecting
				if (edge->A.y > y && edge->B.y > y)
					continue;

				// points on the line
				if ((y == edge->A.y && y == edge->B.y)) {
					points.Add(edge->A.x);
					points.Add(edge->B.x);
				}
				// simple intersection
				else {
					DD::f64 x = Math::Lerp2Lerp(edge->A.y, edge->B.y, edge->A.x, edge->B.x, y);
					points.Add(x);
				}
			}

			if (false)
				points.Add(box.Min.y, box.Max.y);

			if (fwd)
				std::sort(points.begin(), points.end());
			else
				std::sort(points.begin(), points.end(), std::greater<>());

			return points;
			});

		fwd = !fwd;
	}

	// filling padding
	const f64 padding = mpFill.BitDiameter * 0.5;
	// add overlaps for each line to avoid toolhead to be slowing down during filling
	const	f64 laserPadding = 0;//mpFill.Laser ? (fwd ? 5.0 : -5.0) : 0.0;

	// do filling part
	auto cut = [&](const MillingParams & mp) {
		AddComment(L"GREEN");
		// reset orientation
		fwd = true;
		// helper index for selecting precomputed lines
		size_t index = 0;
		// convert precomputed values to gcode
		for (f64 y = box.Min.y + mp.FillStep * 0.5; y <= box.Max.y; y += mp.FillStep) {
			// select points
			const List<f64> & points = futures[index++]->Result(1);
			if (!points.IsEmpty()) {
				AddGoY(y, 1, mp.FeedRateWork);
				AddGoX(points[0] - laserPadding, WORK);
				// each second point set toolhead on
				for (size_t pindex = fwd ? 1 : 2 - !(points.Size() % 2); pindex < points.Size(); pindex += 2) {
					f64 xA = points[pindex - 1];
					f64 xB = points[pindex];
					f64 xC = (xA + xB) * 0.5;

					if (!fwd)
						Swap(xA, xB);
					// padding is applicable
					if (padding == 0.0 || (xA + padding < xC)) {
						CutLineNative(mp, { { xA + padding, y }, { xB - padding, y } });
					}
					else {
						CutPointNative(mp, double2{ xC, y });
					}
				}

				AddGoX(_tracker.CurrentState.Position.x + laserPadding, WORK);
			}

			fwd = !fwd;
		}


		AddComment(L"RED");

		for (const Polyline & polyline : polylines)
			for (u32 i = 0; i < mp.LaserPasses; ++i)
				CutPolylineNative(mp, polyline);
	};


	if (perSlice) {
		MillingParams p = mpFill;
		for (f64 z : ZStepping(mpFill)) {
			p.Z = { z, z };
			cut(p);
		}
	}
	else {
		cut(mpFill);
	}
}

void Builder::FillPolygonNative(const MillingParams & mparams, const Array<Array<double2>> & polygons) {

	List<Math::LineSegment2D> segments(4096);
	Math::Interval2D box;
	for (auto & polygon : polygons) {
		CutPolylineNative(mparams, polygon);
		// get bounding box
		for (const double2 & point : polygon) {
			box.Include(point);
		}

		for (size_t pIndex = 1; pIndex < polygon.Length(); ++pIndex) {
			segments.Add(Math::LineSegment2D{ polygon[pIndex - 1], polygon[pIndex] });
		}
	}

	if (!box.IsValid())
		return;

	double2 size = box.Size();
	for (size_t i = 0, l = (size_t)size.y; i < l; i += 2) {
		Math::LineSegment2D mainLine{ double2{ box.Min.x, box.Min.y + i }, double2{ box.Max.x, box.Min.y + i } };
		List<f64, 64> points;
		for (const Math::LineSegment2D & polygonEdge : segments) {
			double2 intersection;
			if (Math::LineSegment2D::Intersection(polygonEdge, mainLine, intersection)) {
				points.Add(intersection.x);
			}
		}
		DD_TODO_STAMP("Fix middle sort, remove stl sort.");
		//QuickSort<size_t>::MiddleSort<f64, ComparerA>(points.begin(), points.Size());
		std::sort(points.begin(), points.end());
		f64 padding = mparams.BitDiameter * 0.5;
		for (size_t pindex = 1; pindex < points.Size(); pindex += 2) {
			f64 xA = points[pindex - 1];
			f64 xB = points[pindex];
			f64 xC = (xA + xB) * 0.5;
			f64 y = box.Min.y + i;
			// padding is applicable
			if (xA + padding < xC)
				CutLineNative(mparams, { { xA + padding, y }, { xB - padding, y } });
			else
				CutPointNative(mparams, double2{ xC, y });
		}
	}


}

void Builder::loadGCommand(Text::StringView16::CharSplitter<Text::SplitterFlags::SkipEmptyMatches> params) {
	Command cmd;
	// parse parameters
	for (Text::StringView16 param : Sequencer::Drive(params)) {
		// get view without the initial letter
		Text::StringView16 view = param.SubView(1);
		// according to the first char, select the source and the parser
		switch (param.First()) {
		case L'X':
			cmd.X = StringLocal<16>(view).ToDouble();
			break;

		case L'Y':
			cmd.Y = StringLocal<16>(view).ToDouble();
			break;

		case L'Z':
			cmd.Z = StringLocal<16>(view).ToDouble();
			break;

		case L'F':
			cmd.F = (i32)StringLocal<16>(view).ToInteger();
			break;

		case L'P':
			cmd.P = (u8)StringLocal<16>(view).ToInteger();
			break;

		case L'S':
			cmd.S = (i32)StringLocal<16>(view).ToInteger();
			break;

		case L'G':
			cmd.Code = (Codes)((u32)Codes::_MoveCode | (u32)DD::GCode::Hash(param));
			break;

		case L'M':
			cmd.Code = (Codes)((u32)Codes::_DeviceCode | (u32)DD::GCode::Hash(param));
			break;

		case ';':
			break;

		default:
			DD_WARNING("Not implemented gcode \"", param, "\"");
			break;
		}
	}

	AddCommand(cmd);
}


void Builder::loadLine(const String & line) {
	DD_TODO_STAMP("When StringView is ready, here it should be applied");
	// copy data to get it mutable (for trimming)
	StringLocal<256> lineBuffer = line;
	// if line is not empty
	if (lineBuffer.Trim().Length()) {
		// select command that should be used
		switch (lineBuffer.First()) {
			// comment
		case L';':
			AddCommand(Command(lineBuffer.SubView(1)));
			break;
			// GCode
		default:
			loadGCommand(lineBuffer.View().SplitSkipEmpty(L' '));
			break;
		}
	}
}


void Builder::goZero(const MillingParams & params, const float3 & zero) {
	// set speeds
	AddFeedRate(params.FeedRateWork, WORK);
	AddFeedRate(params.FeedRateIdle, IDLE);

	// go to start position
	// firstly go to the safe height
	AddGoZ(params.SafeHeight, IDLE);
	// select xy position
	AddGoX(zero.x, IDLE);
	AddGoY(zero.y, IDLE);
	AddGoZ(zero.z, IDLE);
}


bool Builder::Load(const FileSystem::Path & path) {
	// if file is valid
	if (path.Exists() && path.IsFile()) {
		// read file
		Array<i8> data = path.AsFile().Read();
		String text;
		text.Append(Text::StringView(data.begin(), data.Length()));

		// read lines
		for (const String & line : Sequencer::Drive(text.SplitSkipEmpty(L'\n'))) {
			loadLine(line);
		}

		return true;
	}

	return false;
}


void Builder::FillRectangleAbsolute(const MillingParams & mparams, const RectangleMillingParams & rfparams) {
	// precomputed values
	f64 bitRadius = 0.5 * mparams.BitDiameter;
	Math::Interval2D workArea = rfparams.Rectangle;
	workArea.Min += bitRadius;
	workArea.Max -= bitRadius;

	// start milling
	MillingScope mscope(this, mparams, { workArea.Min, mparams.Z.Max[0] });
	// are we going from up to down / left to right
	bool yasc = true, xasc = true;
	// until we reached the desired depth
	while (true) {
		// until we reach the end of the rectangle
		while (yasc ? CurrentPosition().y < workArea.Max.y : workArea.Min.y < CurrentPosition().y) {
			// go left to right or right to left
			AddGoX((xasc = !xasc) ? workArea.Min.x : workArea.Max.x, WORK);
			// step down or right
			AddGoY(
				yasc ?
				Math::Min(workArea.Max.y, CurrentPosition().y + mparams.FillStep) :
				Math::Max(workArea.Min.y, CurrentPosition().y - mparams.FillStep),
				WORK
			);
		}

		// go left to right or right to left
		AddGoX((xasc = !xasc) ? workArea.Min.x : workArea.Max.x, WORK);

		// invert the y-direction
		yasc = !yasc;

		// stop if desired depth has been reached
		if (CurrentPosition().z == mparams.Z.Min[0])
			break;

		// go down
		AddGoZ(Math::Max(mparams.Z.Min[0], CurrentPosition().z - mparams.DownStep), WORK);
	}
}


void Builder::CutPolyline(const MillingParams & mparams, const PolylineCuttingParam & params, const Polyline & polyline) {
	if (polyline.Length() < 2)
		return;

	List<double2, 1024> points;
	// inner cut of outer shape (true), outer cut of inner shape (false)
	bool inner = params.InnerCut;
	f64 radius = (inner ? -0.5 : 0.5) * mparams.BitDiameter;
	bool closedLoop = polyline.First() == polyline.Last();

	// create normals
	Array<double2> normals(polyline.Length() - 1);
	for (size_t i = 0, l = polyline.Length() - 1; i < l; ++i) {
		// safe normal
		const double2 & a = polyline[i];
		const double2 & b = polyline[i + 1];
		const double2 & ab = b - a;
		normals[i] = Math::Normalize(double2(ab.y, -ab.x));
	}

	// fix starting point
	if (closedLoop) {
		const double2 & point = polyline.Last();
		const double2 & nnext = normals[0];
		const double2 & nprev = normals[normals.Length() - 1];
		Math::Line2D line0(nnext, Math::Dot(point + nnext * radius, nnext));
		Math::Line2D line1(nprev, Math::Dot(point + nprev * radius, nprev));

		double2 shiftedPoint;
		// lines are identical, we do not care for distance, because lines shareing one point
		double2 diffs = line0.Normal - line1.Normal;
		double2 diffs2 = Math::Abs(diffs);
		if (diffs2 <= 1E-8)
			shiftedPoint = point;
		else
			Math::Line2D::PointIntersection({ line0, line1 }, shiftedPoint);

		points.Add(shiftedPoint);
		if (params.FixCorners) {
			bool cvx = Math::Dot(normals[normals.Length() - 1], polyline.Last(1) - polyline.At(1)) < 0.0;
			if (cvx != inner) {
				// go from shifted point directly to the original point
				// as far as drill bit touches the original corner
				const double2 & diff = (shiftedPoint - point);
				f64 dist = Math::Norm(diff);
				f64 rdist = dist - Math::Abs(radius);
				points.Add(shiftedPoint - (diff * (rdist / dist)));
				points.Add(shiftedPoint);
			}
		}
	}
	else {
		points.Add(polyline.First() + normals[0] * radius);
	}

	// fix inner points
	for (size_t i = 1, l = polyline.Length() - 1; i < l; ++i) {
		const double2 & point = polyline.At(i);
		const double2 & nprev = normals[i - 1];
		const double2 & nnext = normals[i];
		Math::Line2D line0(nnext, Math::Dot(point + nnext * radius, nnext));
		Math::Line2D line1(nprev, Math::Dot(point + nprev * radius, nprev));

		double2 shiftedPoint;
		// if no single-point-intersection, the point can be skipped
		// because it is situated on the straight line
		if (!Math::Line2D::PointIntersection({ line0, line1 }, shiftedPoint))
			continue;

		points.Add(shiftedPoint);
		// cornering is enabled and it is the inner corner
		if (params.FixCorners) {
			bool cvx = Math::Dot(nprev, polyline.At(i - 1) - polyline.At(i + 1)) < 0.0;
			if (cvx != inner) {
				// go from shifted point directly to the original point
				// as far as drill bit touches the original corner
				const double2 & diff = (shiftedPoint - point);
				f64 dist = Math::Norm(diff);
				f64 rdist = dist - Math::Abs(radius);
				points.Add(shiftedPoint - (diff * (rdist / dist)));
				points.Add(shiftedPoint);
			}
		}
	}

	// fix lastPoint
	if (closedLoop) {
		points.Add(points.First());
	}
	else {
		points.Add(polyline.Last() + normals[normals.Length() - 1] * radius);
	}


	switch (params.Strategy) {
	case PolylineCuttingParam::Strategies::LongestEdge: {
		// detect longest edge
		size_t longestBegin = 0;
		f64 maxLength2 = 0.0;
		for (size_t i = 0, l = points.Size() - 1; i < l; ++i) {
			f64 length2 = Math::Dot(points.At(i) - points.At(i + 1));
			if (maxLength2 < length2) {
				maxLength2 = length2;
				longestBegin = i;
			}
		}

		// cut part before longest edge
		AddComment(L"GREEN");
		if (longestBegin)
			CutPolylineNative(mparams, points.ToView().HeadView(longestBegin + 1));
		// cut part after longest edge
		AddComment(L"YELLOW");
		CutPolylineNative(mparams, points.ToView().SubView(longestBegin + 1));
		// cut longest edge
		AddComment(L"RED");
		CutPolylineNative(mparams, points.ToView().SubView(longestBegin, 2));
	} break;

	case PolylineCuttingParam::Strategies::Uniform:
		CutPolylineNative(mparams, points);
		break;

	default:
		DD_WARNING("Undefined cutting strategy.");
		break;
	}
}

void Builder::CutPolylineNative(const MillingParams & mparams, const Polyline & points) {
	// safety check
	if (points.Length() == 0) {
		return;
	}

	// are we going ascendantly trough the list
	f64 distToFront = Math::Dot(_tracker.CurrentState.Position.xy - points.First());
	f64 distToBack =  Math::Dot(_tracker.CurrentState.Position.xy - points.Last());
	bool fwd = distToFront < distToBack;
	// start milling
	MillingScope mscope(this, mparams, { fwd ? points.First() : points.Last(), mparams.Z.Max[0] });
	// break when the depth is reached
	while (true) {
		// go through the polyline
		for (size_t i = 0; i < points.Length(); ++i) {
			// get index of point in the list
			size_t index = fwd ? i : points.Length() - (i + 1);
			// polyline movement
			AddGoXY(points[index], WORK);
		}

		// stop if desired depth has been reached
		if (CurrentPosition().z == mparams.Z.Min[0])
			break;

		// z movement
		f64 nextZ = Math::Max(mparams.Z.Min[0], CurrentPosition().z - mparams.DownStep);
		if (mparams.CoolJump)
			AddGoZ(mparams.SafeHeight, WORK);

		AddGoZ(nextZ, WORK);

		fwd = !fwd;
	}
}


void DD::GCode::Builder::CutPolylinesNative(const MillingParams & mp, PolylineInput polylines, double2 partes, double2 overlaps) {
	// clip parts if required
	if (partes != 1.0) {
		Clipper clipper(polylines, partes, overlaps);
		for (bool valid = clipper.Reset(); valid; valid = clipper.Next()) {
			auto subsequence = clipper.Get();
			CutPolylinesNative(mp, subsequence, 1.0);
		}

		return;
	}

	// copy params for individual settings of layer
	MillingParams mpInstance = mp;

	if (partes == 1.0) {
		// fore each layer
		for (DD::f64 z : ZStepping(mp)) {
			// set layer
			mpInstance.Z = { z, z };
			// cut all polylines
			for (const Polyline & polyline : polylines)
				CutPolylineNative(mpInstance, polyline);
		}
	}
}


void Builder::CutRectangleAbsolute(const MillingParams & mparams, const RectangleCuttingParams & params) {
	static constexpr f64 sqrt2 = 1.4142135623730950488016887242097;
	static constexpr f64 normConstant = sqrt2 - 1.0;

	// precomputed values
	f64 bitRadius = 0.5 * mparams.BitDiameter;
	f64 bitShift = params.Inner ? bitRadius * normConstant : 0.0;
	Math::Interval2D workArea = params.Rectangle;
	workArea.Min += params.Inner ? bitRadius : -bitRadius;
	workArea.Max -= params.Inner ? bitRadius : -bitRadius;

	double2 A = { workArea.Min.x, workArea.Min.y };
	double2 AC = A - bitShift;
	double2 B = { workArea.Max.x, workArea.Min.y };
	double2 BC = B + double2(bitShift, -bitShift);
	double2 C = { workArea.Max.x, workArea.Max.y };
	double2 CC = C + bitShift;
	double2 D = { workArea.Min.x, workArea.Max.y };
	double2 DC = D + double2(-bitShift, bitShift);


	// parse strategy
	RectangleCuttingParams::Strategies strategy = params.Strategy;
	double2 size = params.Rectangle.Size();
	// convert short edge to a certain solution
	if (strategy == RectangleCuttingParams::Strategies::ShortEdge) {
		strategy = size.x < size.y ? RectangleCuttingParams::Strategies::Bottom : RectangleCuttingParams::Strategies::Left;
	}
	// convert long edge to a certain solution
	if (strategy == RectangleCuttingParams::Strategies::LongEdge) {
		strategy = size.x < size.y ? RectangleCuttingParams::Strategies::Left : RectangleCuttingParams::Strategies::Bottom;
	}

	List<double2, 16> points;
	// select path according to strategy
	switch (strategy) {
	case RectangleCuttingParams::Strategies::Uniform:
		points.Add(A, AC, A, B, BC, B, C, CC, C, D, DC, D, A);
		CutPolylineNative(mparams, points);
		break;
	case RectangleCuttingParams::Strategies::Top:
		points.Add(DC, D, A, AC, A, B, BC, B, C, CC);
		CutPolylineNative(mparams, points);
		points.Clear();
		points.Add(D, C);
		CutPolylineNative(mparams, points);
		break;
	case RectangleCuttingParams::Strategies::Bottom:
		points.Add(AC, A, D, DC, D, C, CC, C, B, BC);
		CutPolylineNative(mparams, points);
		points.Clear();
		points.Add(A, B);
		CutPolylineNative(mparams, points);
		break;
	case RectangleCuttingParams::Strategies::Left:
		points.Add(AC, A, B, BC, B, C, CC, C, D, DC);
		CutPolylineNative(mparams, points);
		points.Clear();
		points.Add(A, D);
		CutPolylineNative(mparams, points);
		break;
	case RectangleCuttingParams::Strategies::Right:
		points.Add(CC, C, D, DC, D, A, AC, A, B, BC);
		CutPolylineNative(mparams, points);
		points.Clear();
		points.Add(B, C);
		CutPolylineNative(mparams, points);
		break;
	default:
		DD_WARNING("Undefined cutting strategy.");
		break;
	}
}


void Builder::CutLineNative(const MillingParams & mparams, const Math::LineSegment2D & params) {
	List<double2, 2> points;
	points.Add(params.A, params.B);

	CutPolylineNative(mparams, points);
}


void Builder::CutPointNative(const MillingParams & mparams, const double2 & point) {
	// start milling
	MillingScope mscope(this, mparams, { point, mparams.Z.Max[0] });
	AddGoZ(mparams.Z.Min[0], WORK);
}


void Builder::AddCommand(const Command & c) {
	_tracker.ApplyCommand(c);

	Command cmd = c;
	bool append = true;
	// optimize code if possible
	switch (cmd.Code) {
	case Codes::HeadOn:
	case Codes::HeadOff:
		// no change applied
		append = _tracker.CurrentState.HeadPower != _tracker.PreviousState.HeadPower;
		append |= _tracker.CurrentState.HeadMode != _tracker.PreviousState.HeadMode;
		break;

	case Codes::Inches:
	case Codes::Millimeters:
		// no change applied
		append = _tracker.CurrentState.MeasureMode != _tracker.PreviousState.MeasureMode;
		break;

	case Codes::Relative:
	case Codes::Absolute:
		// no change applied
		append = _tracker.CurrentState.PositionMode != _tracker.PreviousState.PositionMode;
		break;

	case Codes::Go0:
	case Codes::Go1:
		if (_tracker.CurrentState.Position.x == _tracker.PreviousState.Position.x)
			cmd.X = NAN;
		if (_tracker.CurrentState.Position.y == _tracker.PreviousState.Position.y)
			cmd.Y = NAN;
		if (_tracker.CurrentState.Position.z == _tracker.PreviousState.Position.z)
			cmd.Z = NAN;
		if (_tracker.CurrentState.FeedRates[cmd.Code == Codes::Go1] == _tracker.PreviousState.FeedRates[cmd.Code == Codes::Go1])
			cmd.F = 0;
		// no change applied
		if (!(append = !(cmd.X != cmd.X && cmd.Y != cmd.Y && cmd.Z != cmd.Z && cmd.F == 0)))
			break;

		// merge commands if possible
		if (cmd.Code == _commands.Last().Code && Math::Abs((_tracker.CurrentState.Direction - _tracker.PreviousState.Direction)) < 1E-6) {
			if (!(cmd.X != cmd.X))
				_commands.Last().X = _tracker.CurrentState.PositionMode == MachineState::PositionModes::Absolute ? cmd.X : _commands.Last().X + cmd.X;
			if (!(cmd.Y != cmd.Y))
				_commands.Last().Y = _tracker.CurrentState.PositionMode == MachineState::PositionModes::Absolute ? cmd.Y : _commands.Last().Y + cmd.Y;
			if (!(cmd.Z != cmd.Z))
				_commands.Last().Z = _tracker.CurrentState.PositionMode == MachineState::PositionModes::Absolute ? cmd.Z : _commands.Last().Z + cmd.Z;

			append = false;
		}
		break;
	}

	if (append)
		_commands.Add(cmd);
}


void Builder::AddGCode(
	const Builder & gcode,
	const double2 & offset /*= 0.0*/,
	bool flipAxes /*= false*/,
	bool flipX /*= false*/,
	bool flipY /*= false*/
) {
	DD_TODO_STAMP("Builder removes some directives because assumes that basic position is zero. While adding a gcode position could be different, these removed directives could cause a fail.")

	for (Command cmd : gcode._commands) {
		if (flipAxes)
			Swap(cmd.X, cmd.Y);

		if (!(cmd.X != cmd.X)) {
			// flip
			if (flipX)
				cmd.X = -cmd.X;
			// apply offset
			cmd.X += offset.x;
		}

		if (!(cmd.Y != cmd.Y)) {
			// flip
			if (flipY)
				cmd.Y = -cmd.Y;
			// apply offset
			cmd.Y += offset.y;
		}

		AddCommand(cmd);
	}
}


bool Tracker::ApplyCommand(const Command & cmd) {
	DD_TODO_STAMP("state update is buggy!!!")
	switch (cmd.Code) {
	case Codes::Go0:
	case Codes::Go1:
		// update
		PreviousState = CurrentState;
		// update position
		switch (CurrentState.PositionMode) {
		case MachineState::PositionModes::Relative:
			CurrentState.Position.x += cmd.X != cmd.X ? 0.0 : cmd.X;
			CurrentState.Position.y += cmd.Y != cmd.Y ? 0.0 : cmd.Y;
			CurrentState.Position.z += cmd.Z != cmd.Z ? 0.0 : cmd.Z;
			break;
		case MachineState::PositionModes::Absolute:
			CurrentState.Position.x = cmd.X != cmd.X ? CurrentState.Position.x : cmd.X;
			CurrentState.Position.y = cmd.Y != cmd.Y ? CurrentState.Position.y : cmd.Y;
			CurrentState.Position.z = cmd.Z != cmd.Z ? CurrentState.Position.z : cmd.Z;
			break;
		default:
			DD_WARNING("Positioning has not been set yet, firstly set positioning mode (Relative/Absolute).");
		}

		// update ranges
		Extrema.Position.Include(CurrentState.Position);
		// update feed-rate if necessary
		if (cmd.F) {
			size_t fIndex = cmd.Code == Codes::Go1;
			PreviousState.FeedRates[fIndex] = CurrentState.FeedRates[fIndex];
			CurrentState.FeedRates[fIndex] = cmd.F;
			Extrema.FeedRate.Include(int1{ FeedRate });
		}
		// update feed rate
		FeedRate = CurrentState.FeedRates[cmd.Code == Codes::Go1];
		// update last track length
		CurrentState.Direction = PreviousState.Position - CurrentState.Position;
		LastTrack = Math::Norm(CurrentState.Direction);
		CurrentState.Direction *= 1.0 / LastTrack;
		// update track and time estimates
		Track += LastTrack;
		Time += LastTrack / FeedRate;
		if (CurrentState.HeadMode == MachineState::HeadModes::Off)
			VoidTime += LastTrack / FeedRate;
		// at least one position command is set
		return !(cmd.X != cmd.X && cmd.Y != cmd.Y && cmd.Z != cmd.Z);

	case Codes::Millimeters:
		PreviousState.MeasureMode = CurrentState.MeasureMode;
		CurrentState.MeasureMode = MachineState::MeasurementModes::Millimeters;
		return false;

	case Codes::Absolute:
		PreviousState.PositionMode = CurrentState.PositionMode;
		CurrentState.PositionMode = MachineState::PositionModes::Absolute;
		return false;

	case Codes::Relative:
		PreviousState.PositionMode = CurrentState.PositionMode;
		CurrentState.PositionMode = MachineState::PositionModes::Relative;
		return false;

	case Codes::HeadOn:
		PreviousState.HeadMode = CurrentState.HeadMode;
		PreviousState.HeadPower = CurrentState.HeadPower;
		CurrentState.HeadMode = MachineState::HeadModes::On;
		if (cmd.S != -1)
			CurrentState.HeadPower = cmd.S;
		return false;

	case Codes::HeadOff:
		PreviousState.HeadMode = CurrentState.HeadMode;
		CurrentState.HeadMode = MachineState::HeadModes::Off;
		return false;

	case Codes::FanOn:
		PreviousState.FanMode = CurrentState.FanMode;
		CurrentState.FanMode = MachineState::FanModes::On;
		return false;

	case Codes::FanOff:
		PreviousState.FanMode = CurrentState.FanMode;
		CurrentState.FanMode = MachineState::FanModes::Off;
		return false;

	case Codes::_Comment:
		return false;

	default:
		DD_WARNING("Undefined command.");
		return false;
	}
}

DD::GCode::PolylineBuilder PolylineBuilder::RoundedRectangle(f64 radius, double2 size, size_t discreteFactor /*= 16*/, f64 precision /*= 0.001*/) {
	// center for roundation
	double2 centerLB = { radius, radius };
	double2 centerRB = { size.x - radius, radius };
	double2 centerRT = { size.x - radius, size.y - radius };
	double2 centerLT = { radius, size.y - radius };

	double2 beginB = { radius, 0.0 };
	double2 endB = { size.x - radius, 0.0 };
	double2 beginR = { size.x, radius };
	double2 endR = { size.x, size.y - radius };
	double2 beginT = { size.x - radius, size.y };
	double2 endT = { radius, size.y };
	double2 beginL = { 0.0, size.y - radius };
	double2 endL = { 0.0, radius };

	discreteFactor += 1;
	if (size.y == 240)
		return PolylineBuilder()
		.AddAbsolute(beginB)
		.AddAbsolute(endB)
		.AddPolygon(centerRB, { 0.0, -radius }, discreteFactor * 4, discreteFactor, false, precision)
		.AddAbsolute(beginR)
		.AddAbsolute(endR)
		.AddPolygon(centerRT, { radius, 0.0 }, discreteFactor * 4, discreteFactor, false, precision)
		.AddAbsolute(beginT)
		.AddAbsolute(endT)
		.AddPolygon(centerLT, { 0.0, radius }, discreteFactor * 4, discreteFactor, false, precision)
		.AddAbsolute(beginL)
		.AddAbsolute(endL)
		.AddPolygon(centerLB, { -radius, 0.0 }, discreteFactor * 4, discreteFactor, false, precision)
		.AddAbsolute(endB);


	return PolylineBuilder()
		.AddAbsolute(beginL)
		.AddAbsolute(endL)
		.AddPolygon(centerLB, { -radius, 0.0 }, discreteFactor * 4, discreteFactor, false, precision)
		.AddAbsolute(endB)
		.AddAbsolute(beginB)
		.AddAbsolute(endB)
		.AddPolygon(centerRB, { 0.0, -radius }, discreteFactor * 4, discreteFactor, false, precision)
		.AddAbsolute(beginR)
		.AddAbsolute(endR)
		//.AddPolygon(centerRT, { radius, 0.0 }, discreteFactor * 4, discreteFactor, false, precision)
		//.AddAbsolute(beginT)
		//.AddAbsolute(endT)
		//.AddPolygon(centerLT, { 0.0, radius }, discreteFactor * 4, discreteFactor, false, precision)
		;
}

DD::GCode::PolylineBuilder & PolylineBuilder::AddPolygon(double2 center, double2 radius, size_t ngon, size_t n, bool clockwise, f64 precision) {
	// conversion of index to the [0.0...1.0] ratio
	f64 factor = (clockwise ? -1.0 : 1.0) / ngon;
	// generate all points
	for (size_t i = 1; i <= n; ++i) {
		// generate point
		double2 point = center + Matrices::Rotation(Math::Rate2Rad(factor * i)) * radius;
		// apply precision
		point.x = Math::Round(point.x, precision);
		point.y = Math::Round(point.y, precision);
		// add to the chain
		AddAbsolute(point);
	}

	return *this;
}

DD::GCode::PolylineBuilder & PolylineBuilder::Expand(f64 size, bool closed, bool removeLoops) {
	LineSequencer sequence{ *this };
	List<double2> normals(Points.Size());
	for (bool valid = sequence.Reset(); valid; valid = sequence.Next()) {
		ProxyLine line = sequence.Get();
		normals.Add(ncross(line.X, line.Y));

	}
	normals.Add(!closed ? double2(0.0) : ncross(Points.First(), Points.Last()) * size);
	for (size_t index = 0, limit = normals.Size(); index < limit; ++index) {
		Points[index] += normals[index] * size;
		Points[(index + 1) % limit] += normals[index] * size;
	}

	if (removeLoops) {
		List<size_t, 128> indices;
		for (size_t nindex = normals.Size() - 2; nindex--;) {
			double2 n = ncross(Points[nindex], Points[(nindex + 1)]);
			if (!(Math::Abs(n - normals[nindex]) < 0.25)) {
				indices.Add(nindex);
			}
		}

		for (size_t candidate : indices) {
			//Points.RemoveAt(candidate);
			bool found = false;
			for (size_t left = 0; !found && left < 10; ++left) for (size_t right = 1; !found && right < 10; ++right) {
				Math::LineSegment2D l0{ Points[candidate - left - 1], Points[candidate - left] };
				Math::LineSegment2D l1{ Points[candidate + right], Points[candidate + right + 1] };
				double2 pt;
				f64 l, r;
				if (Math::LineSegment2D::Intersection(l0, l1, l, r, pt)) {
					found = true;
					for (size_t i = 0; i < left + right /*- 1*/; ++i) {
						Points.RemoveAt(candidate - left);
					}
					Points[candidate - left] = pt;
				}
			}
		}

	}




	return *this;
}

DD::GCode::PolylineBuilder & PolylineBuilder::AddSpiral(double2 startPoint, f64 perCycleGrowth, u32 cycles, u32 ngon) {
	Points.Add(startPoint);
	f64 startSize = Math::Norm(startPoint);
	double2 nPoint = startPoint / startSize;

	for (u32 cycle = 0; cycle < cycles; ++cycle) {
		for (u32 n = 0; n < ngon; ++n) {
			f64 rate = (f64)n / ngon;
			f64 norm = startSize + (rate + cycle) * perCycleGrowth;
			Points.Add(nPoint * Matrices::Rotation(Math::Rate2Rad(rate)) * norm);
		}
	}

	return *this;
}

DD::GCode::PolylineBuilder & PolylineBuilder::Reverse() {
	for (size_t i = 0, halfSize = Points.Size() / 2; i < halfSize; ++i)
		Swap(Points.First(i), Points.Last(i));

	return *this;
}


Sequencer::Driver<GCode::Builder::ZSteppingSequencer> GCode::Builder::ZStepping(const MillingParams & mp) { return { { mp } }; }

