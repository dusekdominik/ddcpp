#include <DD/Gpu/IVertexBuffer.h>
#include <DD/Gpu/Devices.h>

using namespace DD;
using namespace DD::Gpu;

void IVertexBuffer::Init(Device & device, u32 bytes, u32 stride, const void * source) {
	_device = device;
	_device->VertexBufferInit(*this, bytes, stride, source);
}

void IVertexBuffer::Bind(u32 stride, u32 offset) {
	_device->VertexBufferBind(*this, stride, offset);
}

void IVertexBuffer::Release() {
	if (_device) {
		_device->VertexBufferRelease(*this);
		_device = nullptr;
	}
}
