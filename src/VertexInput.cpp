#include <DD/Simulation/ColorVertex.h>
#include <DD/Simulation/EmptyVertex.h>
#include <DD/Simulation/QuadVertex.h>
#include <DD/Simulation/QuadVertexInstance.h>
#include <DD/Simulation/SimpleInstance.h>
#include <DD/Simulation/SimpleVertex.h>
#include <DD/Simulation/TextureVertex.h>

#include <d3d11.h>

using namespace DD;
using namespace DD::Gpu;
using namespace DD::Simulation;

const unsigned int EmptyVertex::SIZE = 0;
const ShaderInput EmptyVertex::LAYOUT[] = { ShaderInput() };
const size_t EmptyVertex::LAYOUT_SIZE = 0;

DEFINE_VERTEX_SHADER_INPUT(ColorVertex)
DEFINE_VERTEX_SHADER_INPUT(QuadVertex)
DEFINE_VERTEX_SHADER_INPUT(QuadVertexInstance)
DEFINE_VERTEX_SHADER_INPUT(SimpleInstance)
DEFINE_VERTEX_SHADER_INPUT(SimpleInstanceVertex)
DEFINE_VERTEX_SHADER_INPUT(SimpleVertex)
DEFINE_VERTEX_SHADER_INPUT(TextureVertex)
