#include <DD/Concurrency/CriticalSection.h>
#include <DD/Concurrency/Thread.h>
#include <DD/Concurrency/ThreadPool.h>
#include <DD/Concurrency/ThreadSafeObject.h>
#include <DD/Debug.h>
#include <DD/Math.h>
#include <DD/Profiler.h>
#include <DD/Queue.h>
#include <DD/FileSystem/Directory.h>
#include <DD/FileSystem/DirectoryBrowser.h>
#include <DD/FileSystem/Drive.h>
#include <DD/FileSystem/File.h>
#include <DD/FileSystem/FileChunkReader.h>
#include <DD/FileSystem/FileReader.h>
#include <DD/FileSystem/Functions.h>
#include <DD/FileSystem/Item.h>
#include <DD/FileSystem/Path.h>
#include <DD/FileSystem/Provider.h>
#include <DD/FileSystem/Watchdog.h>
#include <DD/FileSystem/WatchdogAsync.h>
#include <DD/Memory/NonCopyable.h>

#include <windows.h>
#include <shlobj.h>
#include <shlwapi.h>
#include <io.h>

#pragma comment(lib, "shell32.lib")
#pragma comment(lib, "Shlwapi.lib")

#undef CreateFile
#undef CreateDirectory
#undef RemoveDirectory
#undef CopyFile

#if _DEBUG
#define DD_FS_DEBUG_LOG(...) DD_LOG_STAMP("FileSystemLog: ", __VA_ARGS__)
#else
#define DD_FS_DEBUG_LOG(...)
#endif

using namespace DD;
using namespace DD::FileSystem;

/************************************************************************/
/* Winapi helpers                                                       */
/************************************************************************/
#pragma region helpers
/** Unique pointer for winapi handles, calls CloseHande automatically.  */
struct WinHandle
	: Memory::NonCopyable
{
	/** Native winapi type. */
	HANDLE Handle = nullptr;

	/** Release store data. */
	WinHandle & Release() { if (Handle) ::CloseHandle(Handle), Handle = nullptr; return *this; }
	/** Is handle properly initialized. */
	bool IsValid() const { return Handle != NULL && Handle != INVALID_HANDLE_VALUE; }

	/** Auto conversion to native type. */
	operator HANDLE() const { return Handle; }
	/** Move assignment. */
	WinHandle & operator=(WinHandle && rhs) { DD::Swap(Handle, rhs.Handle); return *this; }
	/** Release stored data and save handle. */
	WinHandle & operator=(HANDLE handle) { Release().Handle = handle; return *this; }

	/** Default constructor. */
	WinHandle() = default;
	/** Save handle/ */
	WinHandle(HANDLE handle) : Handle(handle) {}
	/** Move constructor. */
	WinHandle(WinHandle && rhs) : WinHandle() { DD::Swap(Handle, rhs.Handle); }
	/** Destructor. */
	~WinHandle() { Release(); }
};


static WatchdogRecordCollection FromWinapiBuffer(u8* buffer) {
	FILE_NOTIFY_INFORMATION * pNotify;
	DWORD offset = 0;
	size_t count = 0;
	// count list items
	do {
		pNotify = (FILE_NOTIFY_INFORMATION *)(buffer + offset);
		offset += pNotify->NextEntryOffset;
		++count;
	} while (pNotify->NextEntryOffset != 0);

	// allocate storage
	WatchdogRecordCollection data(count);

	// fill storage
	offset = 0; count = 0;
	do {
		pNotify = (FILE_NOTIFY_INFORMATION *)((char *)buffer + offset);
		offset += pNotify->NextEntryOffset;
		data[count++] = WatchdogRecord{
			(WatchdogRecord::Types)pNotify->Action,
			Text::StringView16(pNotify->FileName, pNotify->FileNameLength >> 1)
		};
	} while (pNotify->NextEntryOffset != 0);


	return data;
}


static bool SingleWait(HANDLE wait) {
	switch (WaitForSingleObject(wait, INFINITE)) {
	case WAIT_OBJECT_0:
		return true;

	default:
		DD_WARNING("Undefined problem with waiting object");
		return false;
	}
}


static bool IsDelimiter(wchar_t c) {
	return c == '\\' || c == '/';
}

static bool HasDelimiter(const Text::StringView16 & path) {
	for (wchar_t c : path)
		if (IsDelimiter(c))
			return true;

	return false;
}

/** Returns a data to a file.  */
static WIN32_FILE_ATTRIBUTE_DATA FileInfo(const Text::StringView16 & path) {
	WIN32_FILE_ATTRIBUTE_DATA data;
	GetFileAttributesExW(path.begin(), GetFileExInfoStandard, &data);
	return data;
}

/** Check if given data belongs to a file. */
static bool IsDirectory(const WIN32_FILE_ATTRIBUTE_DATA & data) {
	return (data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0;
}

/** Check if given data belongs to a file. */
static bool IsFile(const WIN32_FILE_ATTRIBUTE_DATA & data) {
	return (data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == 0;
}

/** File or dir access. */
static bool Exists(const  Text::StringView16 & path) {
	return _waccess(path.begin(), 0) != -1;
}

/** File access. */
static bool IsReadable(const  Text::StringView16 & path) {
	return (_waccess(path.begin(), 0) & 04) != 0;
}

/** File access. */
static bool IsWritable(const  Text::StringView16 & path) {
	return (_waccess(path.begin(), 0) & 02) != 0;
}

/** Returns full form path of a path. Need to check if file exists before. */
static String GetFullPath(const Text::StringView16 & path) {
	String fullpath(1024);
	GetFullPathName(path.begin(), 1024, fullpath.begin(), NULL);
	fullpath.LoadFromBuffer();
	return fullpath;
}

static String ParentPath(const Text::StringView16 & path) {
	String parent(GetFullPath(path));
	if (parent.Length() == 3)
		return parent;

	if (IsDelimiter(parent.Last()))
		parent.RemoveLast();

	while (!IsDelimiter(parent.Last()))
		parent.RemoveLast();

	return parent;
}

static String FileName(const Text::StringView16 & path) {
	if (HasDelimiter(path)) {
		const wchar_t * end = path.end() - 2;
		while (!IsDelimiter(*end))
			--end;
		String filename(++end);
		if (IsDelimiter(filename.Last()))
			filename.RemoveLast();
		return filename;
	}
	else {
		return path;
	}
}

static bool CreateFile(const Text::StringView16 & path) {
	HANDLE handle = CreateFileW(
		// The name of the file or device to be created or opened.
		path.begin(),
		// The requested access to the file or device, which can be summarized as read, write, both or neither zero).
		(GENERIC_READ | GENERIC_WRITE),
		// The requested sharing mode of the file or device, which can be read, write, both, delete, all of these, or none.
		0,
		// A pointer to a SECURITY_ATTRIBUTES structure that contains two separate but related data members : an optional security descriptor, and a Boolean value that determines whether the returned handle can be inherited by child processes.
		0,
		// An action to take on a file or device that exists or does not exist. CREATE_ALWAYS, CREATE_NEW, OPEN_EXISTING, OPEN_ALWAYS, TRUNCATE_EXISTING, CREATE_NEW
		CREATE_NEW,
		// The file or device attributes and flags, FILE_ATTRIBUTE_NORMAL being the most common default value for files.
		FILE_ATTRIBUTE_NORMAL,
		// A valid handle to a template file with the GENERIC_READ access right.
		0
	);

	if (handle == INVALID_HANDLE_VALUE)
		return false;

	CloseHandle(handle);
	return true;
}

static bool CreateDirectory(const String & path) {
	BOOL result = CreateDirectoryW(path, NULL);
	return result != FALSE;
}

static bool RemoveFile(const String & path) {
	BOOL result = DeleteFileW(path);
	return result != FALSE;
}

static bool RemoveDirectory(const String & path) {
	BOOL result = RemoveDirectoryW(path);
	return result != FALSE;
}

static bool CopyFile(const String & from, const String & to, bool failIfExists) {
	return CopyFileW(from, to, failIfExists) != FALSE;
}

static bool Move(const String & from, const String & to, int flags) {
	return MoveFileExW(from, to, flags) != FALSE;
}
#pragma endregion

/************************************************************************/
/* Watchdog                                                             */
/************************************************************************/
#pragma region Watchdog
struct Watchdog::InternalData {
	/** System handle to the observed directory. */
	WinHandle Handle;
};


bool Watchdog::Start() {
	_internal->Handle = ::CreateFileW(
		_path.ToString().begin(),                               // path to directory
		FILE_LIST_DIRECTORY,                                    // access (read/write) mode
		FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE, // share mode
		NULL,                                                   // security descriptor
		OPEN_EXISTING,                                          // how to create
		FILE_FLAG_BACKUP_SEMANTICS,                             // file attributes
		NULL                                                    // file with attributes to copy
	);

	return _internal->Handle.IsValid();
}


void Watchdog::Stop() {
	_internal->Handle.Release();
}


WatchdogRecordCollection Watchdog::GetRecords() {
	// number of returned bytes
	DWORD bytes = 0;

	// read changes
	bool anyData = ReadDirectoryChangesW(
		/* handle to directory */    &_internal->Handle,
		/* read results buffer */    _buffer,
		/* length of buffer */       sizeof(_buffer),
		/* monitor subdirectories */ TRUE,
		/* filter conditions */ FILE_NOTIFY_CHANGE_FILE_NAME | FILE_NOTIFY_CHANGE_DIR_NAME |
		FILE_NOTIFY_CHANGE_ATTRIBUTES | FILE_NOTIFY_CHANGE_SIZE | FILE_NOTIFY_CHANGE_LAST_WRITE |
		FILE_NOTIFY_CHANGE_LAST_ACCESS | FILE_NOTIFY_CHANGE_CREATION | FILE_NOTIFY_CHANGE_SECURITY,
		/* bytes returned */     &bytes,
		/* overlapped buffer */  NULL,
		/* completion routine */ NULL
	);

	return anyData ? FromWinapiBuffer(_buffer) : WatchdogRecordCollection();
}


Watchdog::Watchdog(const Path & path) : _path(path) {}


Watchdog::~Watchdog() = default;
#pragma endregion Watchdog


#pragma region WatchdogAsync
/** Listener data. */
struct WatchdogAsync::IListener {
	/** A routine that will be called when changes are detected. */
	Event<const Path &, const WatchdogRecord &> Handler;
	/** Listener settings. */
	WatchdogAsync::Settings Settings;

	/** Buffer for reading changes, needs to be a quite large to not miss the changes. */
	alignas(sizeof(DWORD)) u8 ReadBuffer[65536] = {0};
	/** Handle to the observed folder. */
	WinHandle FolderHandle = nullptr;
	/**
	 * A handle to signaling event.
	 * https://learn.microsoft.com/en-us/windows/win32/api/synchapi/nf-synchapi-createeventw
	 */
	WinHandle EventHandle = ::CreateEventW(
		/*lpEventAttributes*/ NULL,
		/*bManualReset*/      FALSE,
		/*bInitialState*/     FALSE,
		/*lpName*/            NULL
	);
	/** The winapi structure for obtaining information about changes asynchronously. */
	OVERLAPPED Overlapped = { 0 };
	/**
	 * Probably dispensable value, returns how many bytes can were returned by the last call.
	 * Serves for fail detection, when it returns 0, it should mean that buffer overflew and we missed some data.
	 */
	DWORD Bytes = 0;

	/** Initialize listener by given path and event handler. */
	bool Init(const WatchdogAsync::Settings & path, EventHandler handler);
	/** Made a request for asynchronous reading. */
	bool BeginReading();
	/** When event occurs, read data and process @see Handler. */
	Array<u8> FinishReading();


	/** Constructor. */
	IListener() { Overlapped.hEvent = EventHandle; }
	/** Destructor. */
	~IListener() { if (EventHandle.IsValid()) ::CancelIo(EventHandle); }
};


/** Implementation of message for add/remove handlers. */
template<WatchdogAsync::IMessage::Types TYPE>
struct HandlerMessage
	: public WatchdogAsync::IMessage
{
	/** What to remove */
	WatchdogAsync::Settings Settigns;
	/** */
	WatchdogAsync::EventHandler Handler;
	/** Event for sync. */
	WinHandle DoneEvent = ::CreateEventW(NULL, TRUE, FALSE, L"WatchdogAsync::MessageDoneEvent");

	/** Constructor. */
	HandlerMessage(const WatchdogAsync::Settings& settings, const WatchdogAsync::EventHandler& handler)
		: IMessage(TYPE), Settigns(settings), Handler(handler)
	{}
};

/** Message for adding handlers. */
using AddHandlerMessage = HandlerMessage<WatchdogAsync::IMessage::Types::Add>;
/** Message for removing handlers */
using RemoveHandlerMessage = HandlerMessage<WatchdogAsync::IMessage::Types::Remove>;


/** Message for processing callbacks on a listener. */
struct CallbackMessage
	: public WatchdogAsync::IMessage
{
	/** Listener that received the data. */
	WatchdogAsync::Listener Sender;
	/** Native winapi data. */
	Array<u8> Data;

	/** Constructor. */
	CallbackMessage(WatchdogAsync::Listener sender) : IMessage(Types::Callback), Sender(sender), Data(sender->FinishReading()) {}
};


struct WatchdogAsync::Implementation {
	/** Releases the @see ReadThread when destructor called. */
	WinHandle ExitEvent = ::CreateEventW(NULL, TRUE, FALSE, L"WatchdogAsync::ExitEvent");
	/** When @see Listeners collection changed, rebuild the set of waiting events in @see ReadThread. */
	WinHandle ResetEvent = ::CreateEventW(NULL, FALSE, FALSE, L"WatchdogAsync::ResetWaiting");

	/** Collection oof listeners. */
	List<Listener> Listeners;
	/** Synchronization primitive for @see Listeners. */
	Concurrency::Lock ListenersLock;

	/** Thread for @see WatchdogAsync::tickMessageQueue. */
	Concurrency::Thread MessageProcessingThread;
	/** Synchronization primitive for Consumer-Producer pattern. */
	Concurrency::ConditionVariable MessageQueueCV;
	/** Threading safety, */
	Concurrency::CriticalSection MessageQueueLock;
	/** Queue of results. */
	Queue<Message> MessageQueue;

	/** Thread dedicated for handling async events. */
	Concurrency::Thread ReadThread;
};


bool WatchdogAsync::IListener::Init(const WatchdogAsync::Settings & settings, EventHandler handler) {
	// save path
	Settings = settings;
	// remove handlers if there were any
	Handler->Free();
	// add user specified handler
	Handler += handler;

	// https://learn.microsoft.com/en-us/windows/win32/api/fileapi/nf-fileapi-createfilew
	FolderHandle = ::CreateFileW(
		/* lpFileName */ Settings.Path,
		/* dwDesiredAccess */       FILE_LIST_DIRECTORY,
		/* dwShareMode */           FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE,
		/* lpSecurityAttributes */  NULL,
		/* dwCreationDisposition */ OPEN_EXISTING,
		/* dwFlagsAndAttributes */  FILE_FLAG_BACKUP_SEMANTICS | FILE_FLAG_OVERLAPPED,
		/* hTemplateFile */         NULL
	);

	return FolderHandle.IsValid();
}


bool WatchdogAsync::IListener::BeginReading() {
	// https://learn.microsoft.com/en-us/windows/win32/api/winbase/nf-winbase-readdirectorychangesw
	BOOL result = ReadDirectoryChangesW(
		/* hDirectory */          FolderHandle,
		/* lpBuffer */            ReadBuffer,
		/* nBufferLength */       sizeof(ReadBuffer),
		/* bWatchSubtree */       Settings.Recursive,
		/* dwNotifyFilter */      (DWORD)Settings.Flags,
		/* lpBytesReturned */     &Bytes,
		/* lpOverlapped */        &Overlapped,
		/* lpCompletionRoutine */ NULL
	);

	return result;
}


Array<u8> WatchdogAsync::IListener::FinishReading() {
	// https://learn.microsoft.com/en-us/windows/win32/api/ioapiset/nf-ioapiset-getoverlappedresult
	BOOL result = GetOverlappedResult(
		/* hFile */                      FolderHandle,
		/* lpOverlapped */               &Overlapped,
		/* lpNumberOfBytesTransferred */ &Bytes,
		/* bWait */                      TRUE
	);

	return result ? Array<u8>::Copy(ArrayView(ReadBuffer, (size_t)Bytes)) : Array<u8>();
}


bool WatchdogAsync::AddListenerAsync(const WatchdogAsync::Settings & settings, EventHandler handler, bool wait) {
	Reference<AddHandlerMessage> message = new AddHandlerMessage(settings, handler);
	sendMessage(message);

	if (wait && SingleWait(message->DoneEvent)) {
		DD_FS_DEBUG_LOG("Registration attempt finished ", message->Result ? "successfully." : "unsuccessfully.");
		return message->Result;
	}

	return false;
}


bool WatchdogAsync::RemoveListenerAsync(const WatchdogAsync::Settings & settings, EventHandler handler, bool wait) {
	Reference<RemoveHandlerMessage> message = new RemoveHandlerMessage(settings, handler);
	sendMessage(message);

	if (wait && SingleWait(message->DoneEvent)) {
		DD_FS_DEBUG_LOG("Unregistration attempt finished ", message->Result ? "successfully." : "unsuccessfully.");
		return message->Result;
	}

	return false;
}


void WatchdogAsync::addListener(Message imessage) {
	// lock listeners container
	Concurrency::Lock::ExclusiveScope scope(_impl->ListenersLock);
	// cast message to read data
	AddHandlerMessage* message = static_cast<AddHandlerMessage *>(imessage.Ptr());

	// fail if no available space
	if (_impl->Listeners.Size() == ListenerCount) {
		DD_WARNING("DD::FileSystem::WatchdogAsync - Maximal number of listers has been exceeded");
		message->Result = false;
		::SetEvent(message->DoneEvent);

		return;
	}

	// add new listener
	Listener listener = new IListener;
	message->Result = listener->Init(message->Settigns, message->Handler);
	// if init succeeded, save to register
	if (message->Result) {
		_impl->Listeners.Append(listener);
		listener->BeginReading();
		::SetEvent(_impl->ResetEvent);
	}
	else {
		DD_WARNING("DD::FileSystem::WatchdogAsync - Cannot initialize listener.");
	}


	// trigger event to recalculate waiting events
	::SetEvent(message->DoneEvent);
}


void WatchdogAsync::removeListener(Message imessage) {
	// lock listeners container
	Concurrency::Lock::ExclusiveScope scope(_impl->ListenersLock);
	// cast message to read data
	RemoveHandlerMessage * message = static_cast<RemoveHandlerMessage *>(imessage.Ptr());
	message->Result = false;

	for (size_t i = 0; i < _impl->Listeners.Size(); ++i) {
		// settings are correct
		if (_impl->Listeners[i]->Settings == message->Settigns) {
			// handler was present and removed
			if (_impl->Listeners[i]->Handler->Remove(message->Handler)) {
				// if all delegates were removed, remove listener
				if (_impl->Listeners[i]->Handler->IsEmpty())
					_impl->Listeners.RemoveAtFast(i);

				message->Result = true;
				::SetEvent(_impl->ResetEvent);
				break;
			}
		}
	}

	::SetEvent(message->DoneEvent);
}


void WatchdogAsync::callbacks(Message imessage) {
	// cast message
	CallbackMessage * message = static_cast<CallbackMessage *>(imessage.Ptr());

	// no data to process
	if (message->Data.Length() == 0) {
		DD_WARNING("Buffer overflew");
		return;
	}

	// helper for iteration over the collection
	auto cast = [offset = 0, message](DWORD nextOffset) mutable {
		return (const FILE_NOTIFY_INFORMATION *)(message->Data.begin() + (offset += nextOffset));
	};

	DD_FS_DEBUG_LOG("Processing callbacks");
	// count list items
	for (const FILE_NOTIFY_INFORMATION * pNotify = cast(0); ; pNotify = cast(pNotify->NextEntryOffset)) {
		// crate record
		WatchdogRecord record{
			(WatchdogRecord::Types)pNotify->Action,
			Text::StringView16(pNotify->FileName, pNotify->FileNameLength >> 1)
		};

		// callback
		message->Sender->Handler(message->Sender->Settings.Path, record);

		// no next entry
		if (pNotify->NextEntryOffset == 0)
			break;
	}
}


void WatchdogAsync::sendMessage(Message message) {
	_impl->MessageQueueLock.Enter();
	_impl->MessageQueue.PushBack(message);
	_impl->MessageQueueLock.Leave();
	_impl->MessageQueueCV.WakeOne();
}


WatchdogAsync::Message WatchdogAsync::receiveMessage() {
	Message message;

	_impl->MessageQueueLock.Enter();
	while (!_impl->MessageQueue.TryPopFront(message))
		_impl->MessageQueueLock.Sleep(_impl->MessageQueueCV);
	_impl->MessageQueueLock.Leave();

	return message;
}


bool WatchdogAsync::tickReadingThread() {
	// setup event array
	List<HANDLE, 64> handles;
	{
		Concurrency::Lock::SharedScope scope(_impl->ListenersLock);
		handles.Add(_impl->ExitEvent);
		handles.Add(_impl->ResetEvent);
		for (Listener & listener : _impl->Listeners)
			handles.Add(listener->EventHandle);
	}

	DWORD result = WaitForMultipleObjects((DWORD)handles.Size(), handles.begin(), FALSE, INFINITE);
	switch (result) {
		case WAIT_FAILED: // something went wrong, close the loop
			DD_WARNING("Waiting failed.");
			return false;

		case WAIT_OBJECT_0: // exit event
			DD_FS_DEBUG_LOG("WatchdogAsync::ExitEvent");
			return false;

		case WAIT_OBJECT_0 + 1: // reset event
			DD_FS_DEBUG_LOG("WatchdogAsync::ResetEvent");
			break;

		// process listener with given index
		default: {
			size_t index = result - (WAIT_OBJECT_0);
			if (handles.Size() <= index)
				return false;

			// select the listener, offset one because of closing event, @see constructor/destructor
			Listener listener;
			{
				Concurrency::Lock::SharedScope scope(_impl->ListenersLock);
				for (Listener & l : _impl->Listeners) {
					if (l->EventHandle == handles[index]) {
						listener = l;
						break;
					}
				}
			}

			if (listener) {
				// send message to be processed
				sendMessage(new CallbackMessage(listener));
				// start new reading
				listener->BeginReading();
			}
		} break;
	}

	return true;
}


bool WatchdogAsync::tickMessageThread() {
	// get message
	Message message = receiveMessage();

	// process message
	switch (message->Type) {
	case IMessage::Types::Add:
		addListener(message);
		break;

	case IMessage::Types::Remove:
		removeListener(message);
		break;

	case IMessage::Types::Callback:
		callbacks(message);
		break;

	case IMessage::Types::Exit:
		DD_FS_DEBUG_LOG("Exit message.");
		return false;

	default:
		DD_WARNING("Undefined message.");
		break;
	}

	return true;
}


WatchdogAsync::WatchdogAsync() {
	// initialize thread for reading
	_impl->ReadThread = Concurrency::Thread(
		[this]() {
			DD_FS_DEBUG_LOG("WatchdogAsync::ReadThread has started.");
			while (tickReadingThread());
			DD_FS_DEBUG_LOG("WatchdogAsync::ReadThread has teminated.");
		},
		L"DD::FileSystem::WatchdogAsync"
	);

	// initialize thread for processing
	_impl->MessageProcessingThread = Concurrency::Thread(
		[this]() {
			DD_FS_DEBUG_LOG("WatchdogAsync::MessageProcessingThread has started.");
			while (tickMessageThread());
			DD_FS_DEBUG_LOG("WatchdogAsync::MessageProcessingThread has teminated.");
		}
	);
}


WatchdogAsync::~WatchdogAsync() {
	// set close event to break the reading thread
	::SetEvent(_impl->ExitEvent);
	// send close message to the callback thread
	sendMessage(new IMessage{ IMessage::Types::Exit });

	// wait for threads
	_impl->ReadThread->Wait();
	_impl->MessageProcessingThread->Wait();
};

#pragma endregion WatchdogAsync


/************************************************************************/
/* FileSystem                                                           */
/************************************************************************/
bool FileSystem::Exists(const Text::StringView16 & path) {
	return ::Exists(path);
}

bool FileSystem::IsDirectory(const Text::StringView16 & path) {
	return ::IsDirectory(FileInfo(path));
}

bool FileSystem::IsFile(const Text::StringView16 & path) {
	return ::IsFile(FileInfo(path));
}

bool FileSystem::IsReadable(const Text::StringView16 & path) {
	return ::IsReadable(path);
}

bool FileSystem::IsWritable(const Text::StringView16 & path) {
	return ::IsWritable(path);
}

bool FileSystem::CreateFile(const Text::StringView16 & path, CreateFileMode mode) {
	return ::CreateFile(path);
}

bool FileSystem::CreateDirectory(const Text::StringView16 & path, CreateDirectoryMode mode) {
	return ::CreateDirectory(path);
}

bool FileSystem::RemoveFile(const Text::StringView16 & path, RemoveFileMode mode) {
	return ::RemoveFile(path);
}

bool FileSystem::RemoveDirectory(const Text::StringView16 & path, RemoveDirectoryModes mode) {
	return ::RemoveDirectory(path);
}

bool FileSystem::CopyFile(const Text::StringView16 & from, const Text::StringView16 & to, CopyFileModes mode) {
	return ::CopyFile(from, to, mode == CopyFileModes::FailIfExists);
}

bool FileSystem::Move(const Text::StringView16 & from, const Text::StringView16 & to, MoveModes mode) {
	return ::Move(from, to, (int)mode);
}

bool FileSystem::Cd(const Text::StringView16 & string) {
	String fullPath = GetFullPath(string);
	BOOL result = SetCurrentDirectoryW(fullPath);
	if (result)
		return true;

	DD_WARNING("Changing directory failed (Error:", (unsigned)::GetLastError(), ").");
	return false;
}

List<::DD::FileSystem::Drive> FileSystem::Drives() {
	List<::DD::FileSystem::Drive> result;
	wchar_t drives[MAX_PATH] = { 0 };
	const wchar_t * drive = drives;
	DWORD dwResult = ::GetLogicalDriveStringsW(MAX_PATH, drives);
	if (dwResult && dwResult <= MAX_PATH) {
		while (*drive) {
			result.Add(::DD::FileSystem::Drive(String(drive)));
			while (*(drive++));
		}
	}
	return result;
}

/************************************************************************/
/* FileSystem::Item                                                     */
/************************************************************************/
void FileSystem::Item::loadInstant(const Text::StringView16 & path) {
	static constexpr auto conv = [](ULONGLONG low, ULONGLONG hi) { return low | hi << 32; };

	if (::DD::FileSystem::Exists(path)) {
		WIN32_FILE_ATTRIBUTE_DATA data = FileInfo(path);
		_data = new Data();
		_data->Path = ParentPath(path);
		_data->Name = FileName(path);
		_data->Attributes = data.dwFileAttributes;
		_data->CreateTime = reinterpret_cast<i64&>(data.ftCreationTime);
		_data->AccessTime = reinterpret_cast<i64&>(data.ftLastWriteTime);
		_data->WriteTime = reinterpret_cast<i64&>(data.ftLastWriteTime);
		_data->Access = _waccess(path.begin(), 0);
		_data->Size = conv(data.nFileSizeLow, data.nFileSizeHigh);
	}
}


bool FileSystem::Item::Exists() const {
	return _data != nullptr;
}

bool FileSystem::Item::IsReadable() const {
	return Exists() && (_data->Access & 04);
}

bool FileSystem::Item::IsWritable() const {
	return Exists() && (_data->Access & 02);
}

bool FileSystem::Item::IsFile() const {
	return Exists() && (_data->Attributes & FILE_ATTRIBUTE_DIRECTORY) == 0;
}

bool FileSystem::Item::IsDirectory() const {
	return Exists() && (_data->Attributes & FILE_ATTRIBUTE_DIRECTORY);
}

bool FileSystem::Item::IsSystem() const {
	return Exists() && (_data->Attributes & FILE_ATTRIBUTE_SYSTEM);
}

bool FileSystem::Item::IsHidden() const {
	return Exists() && (_data->Attributes & FILE_ATTRIBUTE_HIDDEN) != 0;
}

Text::StringView16 FileSystem::Item::Path() const {
	return Exists() ? _data->Path.View() : nullptr;
}

Text::StringView16 FileSystem::Item::Name() const {
	return Exists() ? _data->Name.View() : nullptr;
}


/************************************************************************/
/* FileSystem::Directory                                                */
/************************************************************************/
#pragma region Directory
FileSystem::DirectoryBrowser FileSystem::Directory::Open(
	const Text::StringView16 & filter
) const {
	return DirectoryBrowser(*this, filter);
}


DD::FileSystem::Directory::Directory()
	: Directory(Path::WorkingDirectory())
{}


FileSystem::Directory::Directory(const Text::StringView16 & path) {
	loadInstant(path);
	if (!IsDirectory()) {
		_data = nullptr;
	}
}
#pragma endregion


/************************************************************************/
/* FileSystem::File                                                     */
/************************************************************************/

Array<i8> File::Read() {
	Array<i8> data;
	HANDLE out = ::CreateFileW(StringLocal<MAX_PATH>::From(Path(), Name()), FILE_READ_DATA, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (out) {
		data.Reallocate(GetFileSize(out, NULL));
		::ReadFile(out, data.begin(), (DWORD)data.Length(), NULL, NULL);
		::CloseHandle(out);
	}

	return data;
}


bool File::Read(ByteView target) {
	WinHandle out = ::CreateFileW(StringLocal<MAX_PATH>::From(Path(), Name()), FILE_READ_DATA, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (!out.IsValid())
		return false;

	DWORD filesize = GetFileSize(out, NULL);
	if (target.Length() != static_cast<size_t>(filesize))
		return false;

	::ReadFile(out, target.begin(), (DWORD)target.Length(), NULL, NULL);
	return true;
}


bool File::Write(ConstByteView data) {
	BOOL result = false;
	HANDLE out = ::CreateFileW(StringLocal<MAX_PATH>::From(Path(), Name()), FILE_WRITE_DATA, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	if (out) {
		result = ::WriteFile(out, data.begin(), (DWORD)data.Length(), NULL, NULL);
		::CloseHandle(out);
	}

	return (bool)result;
}


size_t DD::FileSystem::File::Size() { return _data->Size; }


DD::FileSystem::File::File() : Item() {}


DD::FileSystem::File::File(Item item) : Item(item) {}


File::File(const String & path) {
	loadInstant(path);
	if (!IsFile()) {
		_data = nullptr;
	}
}

/************************************************************************/
/* FileSystem::DirectoryBrowser                                         */
/************************************************************************/
#pragma region Browser


ArrayView1D<Directory> DirectoryBrowser::Directories() { return _data->Directories; }


ArrayView1D<File> DirectoryBrowser::Files() { return _data->Files; }


Text::StringView16 DirectoryBrowser::Filter() const { return _data->Filter; }


void DirectoryBrowser::Reload(const Text::StringView16 & filter) {
	_data->Filter = filter;
	_data->Directories.Clear();
	_data->Files.Clear();

	if (_data->Dir.Exists()) {
		String path = String::From(_data->Dir.Path(), _data->Dir.Name(), "\\");
		String find = String::From(path, filter);

		WIN32_FIND_DATAW winData;
		HANDLE handle = ::FindFirstFileW(&find[0], &winData);

		if (handle == INVALID_HANDLE_VALUE)
			return;

		do {
			const wchar_t * const name = winData.cFileName;
			if (name[0] == L'.' && (name[1] == 0 || (name[1] == '.' && name[2] == 0)))
				continue;

			Item item;
			item._data = new Item::Data();
			item._data->Access = _waccess(StringLocal<MAX_PATH>::From(path, name), 0);
			item._data->AccessTime = reinterpret_cast<i64&>(winData.ftLastAccessTime);
			item._data->CreateTime = reinterpret_cast<i64&>(winData.ftCreationTime);
			item._data->WriteTime = reinterpret_cast<i64&>(winData.ftLastWriteTime);
			item._data->Attributes = winData.dwFileAttributes;
			item._data->Name = winData.cFileName;
			item._data->Path = path;
			item._data->Size = winData.nFileSizeLow | (((u64)winData.nFileSizeHigh) << 32);

			if (item.IsDirectory())
				_data->Directories.Add(Directory(item));
			else if (item.IsFile())
				_data->Files.Add(File(item));

		} while (::FindNextFileW(handle, &winData));


		FindClose(handle);
	}
}


DirectoryBrowser::DirectoryBrowser(Directory dir, const Text::StringView16 & filter)
	:_data(new Data)
{
	_data->Dir = dir;
	Reload(filter);
}
#pragma endregion

/************************************************************************/
/* FileSystem::Drive                                                    */
/************************************************************************/
#pragma region Drive
bool FileSystem::Drive::Exists() const {
	return _data != nullptr;
}


Text::StringView16 FileSystem::Drive::FileSystem() const {
	return Exists() ? _data->FileSystem.View() : Text::StringView16(nullptr, 0ULL);
}


Text::StringView16 FileSystem::Drive::Path() const {
	return Exists() ? _data->Path.View() : Text::StringView16(nullptr, 0ULL);
}


Text::StringView16 FileSystem::Drive::Name() const {
	return Exists() ? _data->Name.View() : Text::StringView16(nullptr, 0ULL);
}


FileSystem::Directory FileSystem::Drive::Root() const {
	return Exists() ? Directory(_data->Path) : Directory();
}


FileSystem::Drive::Drive(const String & path) {
	String name(MAX_PATH);
	String fs(MAX_PATH);

	DWORD fsFlags;
	BOOL exists = GetVolumeInformationW(
		// A pointer to a string that contains the root directory of the volume to be described.
		path,
		// A pointer to a buffer that receives the name of a specified volume.
		name.begin(),
		// The length of a volume name buffer, in TCHARs.
		MAX_PATH,
		// A pointer to a variable that receives the volume serial number.
		NULL,
		// A pointer to a variable that receives the maximum length, in TCHARs, of a file name component that a specified file system supports.
		NULL,
		// A pointer to a variable that receives flags associated with the specified file system.
		&fsFlags,
		// A pointer to a buffer that receives the name of the file system, for example, the FAT file system or the NTFS file system.
		fs.begin(),
		// The length of the file system name buffer, in TCHARs. The maximum buffer size is MAX_PATH+1.
		MAX_PATH
	);

	if (!exists)
		return;

	name.LoadFromBuffer();
	fs.LoadFromBuffer();

	_data = new Data;
	_data->Name = name;
	_data->Path = path;
	_data->FileSystem = fs;
}
#pragma endregion

/************************************************************************/
/* Path                                                                 */
/************************************************************************/
#pragma region Path
Path Path::Exe() {
	Path path;
	path._path.LoadFromBuffer(GetModuleFileNameW(NULL, path._path.begin(), 260));
	if (path.IsInvalid())
		DD_WARNING((unsigned)::GetLastError());

	return path;
}


Path Path::WorkingDirectory() {
	Path path;
	path._path.LoadFromBuffer(GetCurrentDirectoryW(260, path._path.begin()));
	if (path.IsInvalid())
		DD_WARNING((unsigned)::GetLastError());

	return path;
}


Path Path::Documents() {
	Path path;
	if (SUCCEEDED(SHGetFolderPathW(NULL, CSIDL_MYDOCUMENTS, NULL, SHGFP_TYPE_CURRENT, path._path.begin())))
		path._path.LoadFromBuffer();
	else
		DD_WARNING((unsigned)::GetLastError());

	return path;
}


Directory DD::FileSystem::Path::AsDirectory() const {
	if (IsDirectory() || CreateDirectory())
		return Directory(_path);

	throw;
}


File DD::FileSystem::Path::AsFile() const {
	if (IsFile() || CreateFile())
		return File(_path);

	throw;
}


Path Path::FullPath() const {
	Path path;
	path._path.LoadFromBuffer(GetFullPathNameW(_path, 260, path._path.begin(), NULL));

	return path;
}


bool Path::IsRelative() const {
	// https://learn.microsoft.com/cs-cz/windows/win32/api/shlwapi/nf-shlwapi-pathisrelativea?redirectedfrom=MSDN
	return ::PathIsRelativeW(_path);
}

Path Path::Parent() const {
	Path path = FullPath();

	path._path.RemoveLast();
	while (path._path.Length() && !IsDelimiter(path._path.Last()))
		path._path.RemoveLast();

	path._path.TerminateUnsafe();

	return path;
}


Path Path::PatternSimple(Text::StringView16 prefix, Text::StringView16 suffix, size_t limit /*= 9999*/) {
	static constexpr wchar_t SuffixDelimiter = L'.';
	static constexpr wchar_t DefaultPattern[] = L"{0, 4}";

	StringLocal<MAX_PATH> pattern;
	pattern.AppendBatch(prefix, DefaultPattern, SuffixDelimiter, suffix);

	return Pattern(pattern, limit);
}


Path Path::Pattern(Text::StringView16 pattern, size_t limit /*= 9999*/) {
	// path
	Path target;

	// scan for first non-existing path
	for (size_t i = 0; i < limit; ++i) {
		target._path.Clear();
		target._path.AppendFormat(pattern, i);
		target._path.Terminate();
		if (!target.Exists())
			break;
	}

	return target;
}


Path Item::FullPath() const {
	return FileSystem::Path(StringLocal<MAX_PATH>::From(Path(), Name()));
}
#pragma endregion

/************************************************************************/
/* Auxiliary types for file readers.                                    */
/************************************************************************/

/** Thread pool for reading files. */
static Concurrency::ThreadPool & fileReader() {
	// just one instance for processing files
	static Concurrency::ThreadPool instance(L"FileReaderPool", 1);
	return instance;
}

struct FileReaderAction
	: public IAction<>
{
	/** Native system handle to a file opened for reading. */
	WinHandle Handle;
	/** Pointer to piece of memory, where read data should be stored. */
	LPVOID Target;
	/** Offset from beginning of file (in bytes). */
	size_t Begin;
	/** Number of bytes to be read. */
	size_t Size;

protected:// ILambda
	virtual void iLambdaRun() override {
		DD_PROFILE_SCOPE_STATS("File/ReadChunk");
		::SetFilePointer(Handle, (LONG)Begin, 0, FILE_BEGIN);
		::ReadFile(Handle, Target, (DWORD)Size, NULL, NULL);
	}

public: // constructors
	/** Zero constructor. */
	FileReaderAction() = default;
	/** Constructor. */
	FileReaderAction(HANDLE handle, LPVOID begin, size_t chunk, size_t index, size_t total)
		: Target(begin)
		, Handle(handle)
		, Size(total <= chunk * index ? 0 : Math::Min(total - chunk * index, chunk))
		, Begin(chunk * index)
	{}
};


/************************************************************************/
/* FileChunkReader                                                      */
/************************************************************************/
#pragma region FileChunkReader
struct FileChunkReader::NativeView {
	/** Front buffer - buffer from which is being read. */
	Array<u8> Front;
	/** Back buffer - buffer which is being filled. */
	Array<u8> Back;
	/** Async task to fill Back buffer. */
	Concurrency::ThreadPool::ITask BackTask;
	/** Routine to fill back buffer. */
	FileReaderAction BackRoutine;
	/** Handle to a file. */
	HANDLE Handle;
	/** Index of currently filled buffer (back one). */
	size_t Index;
	/** Number of bytes of the file. */
	size_t ByteSize;
	/** Number of bytes in buffer. */
	size_t Chunk;
	/** String path to the file. */
	StringLocal<260> StringPath;
};


const Array<u8> & FileChunkReader::Get() {
	return nativeView()->Front;
}


bool FileChunkReader::Next() {
	// push back buffer to the front
	nativeView()->BackTask.Wait();
	nativeView()->Back.ResizeUnsafe(nativeView()->BackRoutine.Size);
	nativeView()->Front.Swap(nativeView()->Back);
	// prepare task for filling back buffer
	nativeView()->BackRoutine = FileReaderAction(
		nativeView()->Handle,
		nativeView()->Back.begin(),
		nativeView()->Chunk,
		nativeView()->Index++,
		nativeView()->ByteSize
	);
	if (nativeView()->BackRoutine.Size)
		nativeView()->BackTask.Reset(nativeView()->BackRoutine, 1, fileReader());
	// is anything in front buffer
	return nativeView()->Front.Length() != 0;
}


bool FileChunkReader::Reset() {
	if (nativeView()->Handle) {
		::CloseHandle(nativeView()->Handle);
	}

	// open file handle
	nativeView()->Handle = ::CreateFileW(
		nativeView()->StringPath,     // lpFileName
		FILE_READ_DATA,         // dwDesiredAccess
		0,                      // dwShareMode FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE
		NULL,                   // lpSecurityAttributes
		OPEN_EXISTING,          // dwCreationDisposition CREATE_ALWAYS | CREATE_NEW | OPEN_ALWAYS | OPEN_EXISTING | TRUNCATE_EXISTING
		FILE_ATTRIBUTE_NORMAL,  // dwFlagsAndAttributes
		NULL                    // hTemplateFile
	);

	// file exists
	if (nativeView()->Handle != INVALID_HANDLE_VALUE) {
		// get size
		nativeView()->ByteSize = GetFileSize(nativeView()->Handle, NULL);
		// first chunk
		FileReaderAction routine(
			nativeView()->Handle,
			nativeView()->Front.begin(),
			nativeView()->Chunk,
			0,
			nativeView()->ByteSize
		);
		Concurrency::ThreadPool::ITask firstChunkTask(routine, 0, fileReader());
		// second chunk
		nativeView()->BackRoutine = FileReaderAction(
			nativeView()->Handle,
			nativeView()->Back.begin(),
			nativeView()->Chunk,
			1,
			nativeView()->ByteSize
		);
		if (nativeView()->BackRoutine.Size) {
			nativeView()->BackTask.Reset(nativeView()->BackRoutine, 0, fileReader());
		}
		// wait for first chunk
		firstChunkTask.Wait();
		nativeView()->Index = 2;

		return true;
	}

	return false;
}


FileChunkReader::FileChunkReader(const String & path, size_t chunk) {
	//static constexpr size_t test = sizeof(NativeView);
	static_assert(sizeof(NativeView) == STORAGE_SIZE, "Size of storage is not matching NativeView struct.");
	new (_storage) NativeView();
	nativeView()->Front.Reallocate(chunk);
	nativeView()->Back.Reallocate(chunk);
	nativeView()->StringPath = path;
	nativeView()->Chunk = chunk;
	nativeView()->Handle = 0;
}


FileChunkReader::FileChunkReader(const FileChunkReader & copy)
	: FileChunkReader(copy.nativeView()->StringPath, copy.nativeView()->Chunk)
{
	new (nativeView()) NativeView();
}


FileChunkReader::~FileChunkReader() {
	nativeView()->~NativeView();
}


const String & FileChunkReader::Path() const {
	return nativeView()->StringPath;
}


size_t FileChunkReader::Bytes() const {
	return nativeView()->ByteSize;
}


size_t FileChunkReader::Chunk() const {
	return nativeView()->Chunk;
}


bool FileChunkReader::Seek(size_t chunkIndex) {
	// data are in front buffer
	if (chunkIndex == nativeView()->Index - 2) {
		// check validity of front buffer
		return nativeView()->Front.Length() != 0;
	}

	// data are in back buffer
	if (chunkIndex == nativeView()->Index - 1) {
		// move back buffer to front and check it
		return Next();
	}

	// data are somewhere else
	nativeView()->Index = chunkIndex + 2;
	FileReaderAction frontBufferAction(
		nativeView()->Handle,
		nativeView()->Front.begin(),
		nativeView()->Chunk,
		chunkIndex++,
		nativeView()->ByteSize
	);

	// buffer will be empty, so invalid
	if (frontBufferAction.Size == 0) {
		return false;
	}

	// task in for pool
	Concurrency::ThreadPool::ITask frontBufferTask(frontBufferAction, 1, fileReader());

	// prepare back buffer reading
	nativeView()->BackRoutine = FileReaderAction(
		nativeView()->Handle,
		nativeView()->Front.begin(),
		nativeView()->Chunk,
		chunkIndex++,
		nativeView()->ByteSize
	);

	// read back buffer
	if (nativeView()->BackRoutine.Size) {
		nativeView()->BackTask.Reset(
			nativeView()->BackRoutine,
			0,
			fileReader()
		);
	}

	// wait for data in front buffer
	frontBufferTask.Wait();
	return true;
}
#pragma endregion


struct FileHandle
	: Com::Handle
{
	WinHandle _handle;
	Path _path;

	size_t RequestId(ByteView data) {
		Text::StringView16 fullpath = _path.ToString();
		if (data.Length() == fullpath.Length()) {
			data.CopyIn(ArrayView(fullpath.begin(), fullpath.Length()));
		}

		return fullpath.Length();
	}

	size_t RequestData(ByteView data) {
		size_t length = GetFileSize(_handle, NULL);
		if (data.Length() == length) {
			::ReadFile(_handle, data.begin(), (DWORD)data.Length(), NULL, NULL);
		}

		return length;
	}

	size_t Request(Text::StringView8 request, ByteView data) override {

		switch (request.Hash()) {
		case DD::Text::Hash(""):
			return RequestData(data);
			break;
		case DD::Text::Hash("id"):
			return RequestId(data);
		default:
			throw;
		}

		return 0;
	}

	FileHandle(const Path & fullpath)
		: _path(fullpath)
	{
		_handle = ::CreateFileW(_path, FILE_READ_DATA, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	}

	~FileHandle() override = default;
};


DD::Com::Handle * Provider::Produce(Text::StringView8 request) {
	Path p = Root.Infer(request).FullPath();
	if (!p.IsFile())
		return nullptr;

	return new FileHandle(p);
}

void Provider::Dispose(Com::Handle * target) {
	delete target;
}

