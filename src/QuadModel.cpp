#include <DD/Gpu/Devices.h>
#include <DD/Debug.h>
#include <DD/Simulation/EmptyVertex.h>
#include <DD/Simulation/QuadModel.h>

using namespace DD;
using namespace DD::Simulation;

static i32 LoDReductionCoef() {
	static Debug::i32Range _value(20, 0, 30, L"Models", L"[Quad] Detail", L"Coefficient for reduction of points by distance.");
	return _value;
}

static f32 PointSizeCoef() {
 static Debug::f32Range _value(1.f, 0.001f, 1000.f, L"Models", L"[Quad] Size", L"Quad size scaling.");
 return _value;
}

DD_TODO_STAMP("Remove dependency on engine camera.");
Camera * QuadModel::_CAMERA = nullptr;

Gpu::ConstantBuffer<QuadConstantBuffer>& QuadModel::QBuffer() {
	static Gpu::ConstantBuffer<QuadConstantBuffer> _buffer;
	return _buffer;
}

void QuadModel::InitStatic(Gpu::Device & device) {
	static bool firstRun = true;
	if (firstRun) {
		QBuffer()->PointSize = 1.f;
		QBuffer()->QuadColorTone = Color::Collection::GREEN;
		QBuffer().Init(device);
		firstRun = false;
	}
}

void QuadModel::setLoDReduction() {
	f32 dist = dot(_CAMERA->Position() - _boundingSphere.Center);
	_lodReduction = Math::Saturate((dist / (1 << LoDReductionCoef())) + 0.5f, 1, 32);
}

void QuadModel::iModelInit(Gpu::Device & device) {
	_device = device;
	InitStatic(_device);
	_ps = _device->GetPixelShader(L"PSSimple");
	_vs = _device->GetVertexShader<EmptyVertex>(L"VSQuad");

	_boundingBox.Reset();
	for (const QuadVertex & q : _points)
		_boundingBox.Include(q.Position);
	_boundingSphere = _boundingBox.BoundingSphere();

	_points.Randomize();
	_points.Init(device);
}

void QuadModel::iModelReinit(Gpu::Device & device) {
	_points.Reinit(device);
}

void QuadModel::iModelDraw(DrawArgs & args) {
	_ps->Set();
	_vs->Set();
	QBuffer()->PointSize = PointSizeCoef() * _pointSize * Math::Sqrt(_lodReduction);
	QBuffer().Bind2VS();
	QBuffer().Update();
	_points.Bind2VS();
	_device->SetPrimitiveTopology(Gpu::Devices::PrimitiveTopologies::TriangleList);
	setLoDReduction();
	_device->Draw(((u32)(_points.Size() / _lodReduction)) * 6);
}

QuadModel::QuadModel(f32 pointSize)
	: _points()
	, _pointSize(pointSize)
{}

QuadModel::QuadModel(QuadVertex * data, size_t count, f32 pointSize)
	: _points()
	, _pointSize(pointSize)
{
	_points.CatchConst({ data, count }, count);
}
