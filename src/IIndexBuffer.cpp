#include <DD/Gpu/Devices.h>
#include <DD/Gpu/IIndexBuffer.h>

using namespace DD;
using namespace DD::Gpu;

void IIndexBuffer::Init(Device & device, u32 bytes, const void * source) {
	_device = device;
	_device->IndexBufferInit(*this, bytes, source);
}

void IIndexBuffer::Bind(IIndexBufferIndices indexType, u32 offset) {
	_device->IndexBufferBind(*this, indexType, offset);
}

void IIndexBuffer::Release() {
	if (_device) {
		_device->IndexBufferRelease(*this);
		_device = nullptr;
	}
}
