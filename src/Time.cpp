#include <DD/Time.h>

#include <Windows.h>

using namespace DD;


/** Conversion between two types. */
static i64 FileTime2Integer(const FILETIME & filetime) {
	LARGE_INTEGER linteger;
	linteger.HighPart = filetime.dwHighDateTime;
	linteger.LowPart = filetime.dwLowDateTime;

	return linteger.QuadPart;
}


/** Conversion between two types. */
static FILETIME Integer2FileTime(i64 integer) {
	LARGE_INTEGER linteger;
	linteger.QuadPart = integer;

	FILETIME filetime;
	filetime.dwHighDateTime = linteger.HighPart;
	filetime.dwLowDateTime = linteger.LowPart;

	return filetime;
}


/** Convert given milliseconds to winapi timestamp. */
static SYSTEMTIME ExtractEpochMilliseconds(i64 unixMilliseconds) {
	FILETIME filetime = Integer2FileTime(Time::Unix2Win(unixMilliseconds));
	SYSTEMTIME systemtime;
	FileTimeToSystemTime(&filetime, &systemtime);

	return systemtime;
}



i64 Time::EpochTimeSeconds() {
	return EpochTimeMilliseconds() / 1000;
}


i64 Time::EpochTimeMilliseconds() {
	FILETIME filetime;
	GetSystemTimeAsFileTime(&filetime);

	return Win2Unix(FileTime2Integer(filetime)) ;
}


DD::Time::Stamp Time::GetStamp() const {
	const SYSTEMTIME & native = ExtractEpochMilliseconds(_epochMilliseconds);
	return {
		native.wYear,
		native.wMonth,
		native.wDay,
		native.wDayOfWeek,
		native.wHour,
		native.wMinute,
		native.wSecond,
		native.wMilliseconds
	};
}

