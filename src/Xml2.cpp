#include <DD/xml/Attribute.h>
#include <DD/xml/Builder.h>
#include <DD/Xml/Reader.h>
#include <DD/Xml/Tools.h>

using namespace DD;
using namespace DD::Xml;

#define DD_XML_READER_ERROR_FWD(...) {\
  ErrorCodes code = __VA_ARGS__;      \
  if (code != ErrorCodes::NoError)    \
    return code;                      \
}


/** Unescape quoted string. */
struct UnescapeQuotes {
	/** Source string. */
	Text::StringView16 Source;
	/** Cursor index. */
	size_t Index;
	/** Letter. */
	wchar_t Cursor;

	/** Sequencer method. */
	bool Next() {
		switch (Cursor = Source[++Index]) {
		case '&':
			if (Source.SubViewSafe(Index).StartsWith(Xml::Tools::EscapeAmpersand))
				Index += Xml::Tools::EscapeAmpersand.Length();
			break;
		case '\\':
			Cursor = Source[++Index];
			break;
		}

		return Index < Source.Length();
	}
	/** Sequencer method. */
	bool Reset() { Index = 0; return Source.Length() != 0; }
	/** Sequencer method. */
	wchar_t Get() { return Cursor; }
};

#pragma region Xml::Attribute
String Attribute::EscapedValue() const {
	return
		String(Value.Length() + 1)
		.AppendSequenceUnsafe(UnescapeQuotes{ Value.RemoveFirstLast(1, 1) })
		.TerminateUnsafe();
}
#pragma endregion


#pragma region Xml::Reader
Reader::ErrorCodes Reader::parseOpenTag(Text::StringView16 text) {
	if (text.Length() < 3)
		return InvalidTagName;

	bool shortTag = Tools::HasShortTagBrackets(text);

	DD::Text::StringReader16 reader(text.RemoveFirstLast(1, 1 + shortTag));
	reader.SkipWhiteSpace();
	if (!reader.CursorIsLetterOrUnderscore())
		return InvalidTagName;

	if (_topElement->Type == Element::DocumentType && _document.Root != nullptr)
			return MultipleRoots;

	DD::Text::StringView16 name = reader.ReadWordEx();
	Xml::Element & element = appendElement(!shortTag);
	element.Name = name;

	if (_document.Root == nullptr)
		_document.Root = &element;

	// parse attributes
	while (reader.Any()) {
		reader.SkipWhiteSpace();
		if (reader.CursorIsLetterOrUnderscore()) {
			DD_XML_READER_ERROR_FWD(parseAttribute(*new (element.Attributes.AppendAllocation()) Xml::Attribute, reader));
		}
	}

	if (!shortTag)
		_topElement = &element;

	element.Type = Element::Types::TagType;
	return NoError;
}


Reader::ErrorCodes Reader::parseCloseTag(Text::StringView16 text) {
	if (text.Length() < 4)
		return InvalidCloseTag;

	// pair elements does not match
	if (_topElement->Name != text.RemoveFirstLast(2, 1).Trim())
		return InvalidCloseTag;

	// pop from stack
	_topElement = _topElement->Parent;

	return NoError;
}


Xml::Element & Reader::appendElement(bool stack) {
	Xml::Element & element = _document.Elements.Append();

	if (element.Parent = _topElement)
		_topElement->AppendChild(&element);

	if (stack)
		_topElement = &element;

	return element;
}


Reader::ErrorCodes Reader::parseAttribute(
	Xml::Attribute & attribute, Text::StringReader16 & reader
) {
	if (!reader.CursorIsLetter())
		return InvalidAttributeName;

	attribute.Name = reader.ReadWord();
	reader.SkipWhiteSpace();

	if (reader.CursorIsNot('='))
		return NoError;

	reader.SkipOne();
	reader.SkipWhiteSpace();

	if (!reader.CursorIsQuote())
		return InvalidAttributeValue;

	attribute.Value = reader.ReadQuotes();
	if (attribute.Value.Length() < 2)
		return InvalidAttributeValue;

	if (attribute.Value.Last() != '\'' && attribute.Value.Last() != '"')
		return InvalidAttributeValue;

	return NoError;
}


Reader::ErrorCodes Reader::parseHeader(Text::StringView16 text) {
	if (_document.Header)
		return MultipleHeaders;

	if (!Tools::HasHeaderBrackets(text))
		return InvalidHeader;

	Text::StringReader16 reader(text.RemoveFirstLast(2, 2));
	reader.SkipWhiteSpace();

	if (!reader.CursorIsLetterOrUnderscore())
		return InvalidHeader;

	// header name
	Element & element = appendElement(false);
	element.Name = reader.ReadWordEx();

	while (reader.Any()) {
		reader.SkipWhiteSpace();
		// parse attributes
		if (reader.CursorIsLetterOrUnderscore()) {
			DD_XML_READER_ERROR_FWD(parseAttribute(*new (element.Attributes.AppendAllocation()) Xml::Attribute, reader))
		}
	}

	element.Type = Element::Types::HeaderType;
	return NoError;
}


Reader::ErrorCodes Reader::parseComment(Text::StringView16 text) {
	if (!Tools::HasCommentBrackets(text))
		return InvalidComment;

	Xml::Element & element = appendElement(false);
	element.Name = text.RemoveFirstLast(4, 4).Trim();
	element.Type = Element::Types::CommentType;
	return NoError;
}


Reader::ErrorCodes Reader::parseText(Text::StringView16 text) {
	Xml::Element & element = appendElement(false);
	element.Name = text;
	element.Type = Element::Types::TextType;

	return NoError;
}


Reader::ErrorCodes Reader::parseXml(Text::StringView16 xmlText) {
	// init super root
	_document.SuperRoot = &appendElement(true);
	_document.SuperRoot->Type = Xml::Element::Types::DocumentType;

	Text::StringReader16 reader(xmlText);

	while (reader.Any()) {
		// read text or tag
		Text::StringView16 item = reader.SkipWhiteSpace().CursorIs('<') ?
			reader.ReadAngleBrackets() : reader.ReadUntil('<').Trim();

		// if dummy string, do not save
		if (item.Length() == 0)
			continue;

		// next element is a tag
		if (2 < item.Length() && item[0] == '<') {
			switch (item[1]) {
			case '!': DD_XML_READER_ERROR_FWD(parseComment(item));  break;
			case '?': DD_XML_READER_ERROR_FWD(parseHeader(item));   break;
			case '/': DD_XML_READER_ERROR_FWD(parseCloseTag(item)); break;
			default:  DD_XML_READER_ERROR_FWD(parseOpenTag(item));  break;
			}
		}
		// next element is a text
		else {
			DD_XML_READER_ERROR_FWD(parseText(item));
		}
	}

	if (_topElement != _document.SuperRoot)
		return UnexpectedEnd;

	return NoError;
}
#pragma endregion


#pragma region Xml::Builder
Builder::ElementProxy & Builder::ElementProxy::AppendAttribute(
	Text::StringView16 name, Text::StringView16 value
) {
	Xml::Attribute * attribute = new (_element->Attributes.AppendAllocation()) Xml::Attribute();
	_parent->appendProxy(name, attribute->Name, false);
	_parent->appendProxy(value, attribute->Value, true);

	return *this;
}


Builder::ElementProxy Builder::ElementProxy::AppendChild(
	Text::StringView16 name, Xml::Element::Types type
) {
	Element & element = _parent->Target.Elements.Append();
	element.Type = type;
	_element->Parent = _element;
	_element->AppendChild(&element);
	_parent->appendProxy(name, element.Name, false);

	return { &element, _parent };
}


void Builder::appendText(Text::StringView16 value) {
	for (wchar_t letter : value) {
		switch (letter) {
		case '\'': Buffer.AppendEx(Tools::EscapeApostrophe);     break;
		case '"':  Buffer.AppendEx(Tools::EscapeQuote);          break;
		case '<':  Buffer.AppendEx(Tools::EscapeOpenBracket);    break;
		case '>':  Buffer.AppendEx(Tools::EscapeCloseBracket);   break;
		case '&':  Buffer.AppendEx(Tools::EscapeAmpersand); break;
		default:   Buffer.AppendEx(letter); break;
		}
	}
}

void Builder::appendQuotes(Text::StringView16 value) {
	Buffer.AppendEx('"');
	for (wchar_t letter : value) {
		switch (letter) {
		case '\\': Buffer.AppendEx(Text::StringView(L"\\\\"));                    break;
		case '"':  Buffer.AppendEx(Text::StringView(L"\\\""));                     break;
		case '&':  Buffer.AppendEx(Tools::EscapeAmpersand); break;
		default:   Buffer.AppendEx(letter); break;
		}
	}
	Buffer.AppendEx('"');
}


void Builder::appendProxy(Text::StringView16 text, Text::StringView16 & proxy, bool quoteEscape) {
	wchar_t * ptr = Buffer.begin();
	size_t oldLen = Buffer.Length();

	if (quoteEscape)
		appendQuotes(text);
	else
		appendText(text);

	proxy = Buffer.TailView(Buffer.Length() - oldLen);

	if (ptr != Buffer.begin()) {
		ptrdiff_t shift = ptr - Buffer.begin();
		for (StringViewProxy & proxy : Proxies)
			proxy._target = { proxy._target.begin() + shift, proxy._target.Length() };
	}

	new (Proxies.AppendAllocation()) StringViewProxy{ proxy };
}


void Builder::appendHeader(
	Text::StringView16 enconding /* = L"UTF-8" */, Text::StringView16 version /* = L"1.0" */
) {
	ElementProxy proxy = GetSuperRoot().AppendChild(L"xml", Xml::Element::Types::HeaderType);
	proxy.AppendAttribute(L"encoding", enconding);
	proxy.AppendAttribute(L"version", version);
	Target.Header = proxy.Native();
}


void Builder::appendRoot(Text::StringView16 name) {
	ElementProxy proxy = GetSuperRoot().AppendChild(name);
	Target.Root = proxy.Native();
}


Builder::ElementProxy Builder::GetSuperRoot() {
	if (Target.SuperRoot == nullptr) {
		Target.SuperRoot = &Target.Elements.Append();
		Target.SuperRoot->Type = Xml::Element::Types::DocumentType;
	}

	return { Target.SuperRoot, this };
}


Builder::ElementProxy Builder::Init(
	Text::StringView16 rootName,
	Text::StringView16 enconding /* = L"UTF-8" */,
	Text::StringView16 version /* = L"1.0" */
) {
	appendHeader(enconding, version);
	appendRoot(rootName);

	return GetRoot();
}
#pragma endregion
