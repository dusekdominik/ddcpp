#include <DD/Simulation/Camera.h>
#include <DD/Debug.h>
#include <DD/Matrices.h>
#include <DD/Stopwatch.h>

using namespace DD;
using namespace DD::Simulation;

Frustum3F Camera::CreateFrustum() const {
	const State state = *_state;

	float3 upVector = state.UpVector == 0.0 ? Constants::Up : state.UpVector;
	// x-axis (width) of camera coordinate system
	const float3 & Z = state.Direction;
	// x-axis (width) of camera coordinate system
	const float3 X = Math::Normalize(Math::Cross(upVector, Z));
	// y-axis (height) of camera coordinate system
#if DD_MATRICES_LEFT_HANDED
	const float3 Y = Math::Normalize(Math::Cross(Z, X));
#else
	const float3 Y = Math::Normalize(Math::Cross(X, Z));
#endif

	Frustum3F frustum;
	// near
	frustum.Planes[0] = { state.Direction, state.Position + (state.Direction * state.Near) };
	// far
	frustum.Planes[1] = { -state.Direction, state.Position + (state.Direction * state.Far) };



	switch (state.Projection) {
		case Camera::Projections::Orthographic: {
			// left
			frustum.Planes[2] = { -X, state.Position + X * (state.TgFov.x * 0.5f) };
			// right
			frustum.Planes[3] = {  X, state.Position - X * (state.TgFov.x * 0.5f) };
			// top
			frustum.Planes[4] = { -Y, state.Position + Y * (state.TgFov.y * 0.5f) };
			// bottom
			frustum.Planes[5] = {  Y, state.Position - Y * (state.TgFov.y * 0.5f) };
		} break;

		case Camera::Projections::Perspective: {
			// auxiliary calculations
			const float3 horizontalShift(X * state.TgFov.x * 0.5);
			const float3 verticalShift(Y * state.TgFov.y * 0.5f);
			const float3 l(state.Direction - horizontalShift);
			const float3 r(state.Direction + horizontalShift);
			const float3 t(state.Direction + verticalShift);
			const float3 b(state.Direction - verticalShift);

			// left
			frustum.Planes[2] = { cross(Y, l), state.Position }; // FromDirsAndPoint(l, Y, s.Position);
			// right
			frustum.Planes[3] = { cross(r, Y), state.Position }; // FromDirsAndPoint(Y, r, s.Position);
			// top
			frustum.Planes[4] = { cross(X, t), state.Position }; // FromDirsAndPoint(t, X, s.Position);
			// bottom
			frustum.Planes[5] = { cross(b, X), state.Position }; // FromDirsAndPoint(X, b, s.Position);
		} break;

	default:
		break;
	}





	return frustum;
}


void Camera::FrameUpdate() {
	// lock camera state
	State state = *_state;

	// set up matrices
	float3 upVector = state.UpVector == 0.0 ? Constants::Up : state.UpVector;
	switch (state.Projection) {
		case Projections::Orthographic:
			_projection = Math::OrthographicProjection(state.TgFov.x, state.TgFov.y, state.Near, state.Far);
			break;

		case Projections::Perspective:
			_projection = Math::PerspectiveProjection(state.TgFov.x, state.TgFov.y, state.Near, state.Far);
			break;

		default:
			throw;
	}

	_view = Matrices::LookTo(state.Position, state.Direction, upVector);
	_viewProjection = _projection * _view;
	// timestamp
	u32 time = (u32)Stopwatch::TimeStampMilliseconds();
	// fill constant buffer data
	(*this)->ViewProjectionTransformation = _viewProjection.Transposition();
	(*this)->Delta = (u32)(time - (*this)->Time);
	(*this)->Time = time;
	(*this)->CameraX.xyz = _view.Rows(0).xyz;
	(*this)->CameraY.xyz = _view.Rows(1).xyz;
	(*this)->CameraZ.xyz = _view.Rows(2).xyz;
	(*this)->CameraO.xyz = state.Position;
	(*this)->CameraHTgFovY = state.TgFov.y * 0.5f;
	(*this)->CameraHTgFovYInv = 1.f / (*this)->CameraHTgFovY;
	(*this)->CameraHTgFovX = state.TgFov.x * 0.5f;
	(*this)->CameraHTgFovXInv = 1.f / (*this)->CameraHTgFovX;
}


Camera & Camera::RotateX(Units::PlaneAngle_RadianF gain) {
	// check gain
	if (gain == 0.f)
		return *this;
	// lock state
	auto proxy = *_state;
	// rotate camera
#if DD_MATRICES_LEFT_HANDED
	proxy->Direction = Matrices::RotationY(gain) * proxy->Direction;
	proxy->UpVector = Matrices::RotationY(gain) * proxy->UpVector;
#else
	proxy->Direction = Matrices::RotationY(-gain) * proxy->Direction;
#endif
	return *this;
}


Camera & Camera::RotateY(Units::PlaneAngle_RadianF gain) {
	// check gain
	if (gain == 0.f)
		return *this;
	// lock state
	auto proxy = *_state;
	// rotate camera
	const f32 sgn = gain > 0.f ? 1.f : -1.f;
	// cos(Dir, Up) = Dir.y
	f32 angle = Math::ArcCos(proxy->Direction.y) + gain;
	if (angle < Math::ConstantsF::Zero || Math::ConstantsF::Pi < angle)
		return *this;
	// axis = cross(up, dirXZ)
	PositionF axis = proxy->UpVector == 0.0f ?
		PositionF{proxy->Direction.z, 0.f, -proxy->Direction.x} :
		Math::Cross(proxy->Direction, -proxy->UpVector).Cast<Units::Length_MeterF>();
	proxy->Direction = Matrices::RotationAxis(gain.Value, axis.Cast<f32>()) * proxy->Direction;
	proxy->UpVector  = Matrices::RotationAxis(gain.Value, axis.Cast<f32>()) * proxy->UpVector;

	return *this;
}


PositionF Camera::DirectionXZ() const {
	auto proxy = *_state;
	return { proxy->Direction.x, 0.f, proxy->Direction.z };
}


Camera & Camera::Move(const float3 & v) {
	_state->Position += v;
	return *this;
}


Camera & Camera::ForwardXZ(Units::Length_MeterF gain) {
	auto proxy = *_state;
	const f32 coef = gain / Math::Norm(proxy->Direction.xz);
	proxy->Position.x += proxy->Direction.x * coef;
	proxy->Position.z += proxy->Direction.z * coef;

	return *this;
}


Camera & Camera::RightXZ(Units::Length_MeterF gain) {
	auto proxy = *_state;
	// direction = cross(Up, DirXZ)
	const f32 coef = gain / Math::Norm(proxy->Direction.xz);
	// add cross product to position
#if DD_MATRICES_LEFT_HANDED
	proxy->Position.x += proxy->Direction.z * coef;
	proxy->Position.z -= proxy->Direction.x * coef;
#else
	proxy->Position.x -= proxy->Direction.z * coef;
	proxy->Position.z += proxy->Direction.x * coef;
#endif
	return *this;
}

Camera & Camera::TopXZ(Units::Length_MeterF gain) {
	_state->Position.y += gain;
	return *this;
}

Camera::Camera()
	: ConstantBuffer()
{
	Position(Constants::Position);
	Direction(Constants::Direction);
	UpVector(0.0f);
	Near(Constants::Near);
	Far(Constants::Far);
	TgFov(Constants::TgFov);
	//Projection(Constants::Projection);
}
