#include <DD/Input/KeyShortcut.h>

using namespace DD;
using namespace DD::Input;

String KeyShortcut::ToString() const {
	// preallocate
	String result(20);

	// stringize
	if (Mode.First != KeyShortcut::KeyModes::Undefined)
		result.AppendBatch(Keys.First.ToString());
	if (Mode.Second != KeyShortcut::KeyModes::Undefined)
		result.AppendBatch(Keys.Second.ToString());
	if (Mode.Second != KeyShortcut::KeyModes::Undefined)
		result.AppendBatch(Keys.Third.ToString());

	return result;
}

KeyShortcut KeyShortcut::operator&(Key key) const {
	return KeyShortcut(*this) &= key;
}

KeyShortcut KeyShortcut::operator|(Key key) const {
	return KeyShortcut(*this) |= key;
}

#define _SET_SHORTCUT(nkey, setMode)                   \
if (Mode.nkey == KeyShortcut::KeyModes::Undefined) {   \
  Mode.nkey = KeyShortcut::KeyModes::setMode;          \
  Keys.nkey = key;                                     \
  return *this;                                        \
}

KeyShortcut & KeyShortcut::operator|=(Key key) {
	_SET_SHORTCUT(First,  Admissible);
	_SET_SHORTCUT(Second, Admissible);
	_SET_SHORTCUT(Third,  Admissible);

	// all keys were already used
	throw;
	return *this;
}

KeyShortcut & KeyShortcut::operator&=(Key key) {
	_SET_SHORTCUT(First,  Mandatory);
	_SET_SHORTCUT(Second, Mandatory);
	_SET_SHORTCUT(Third,  Mandatory);

	// all keys were already used
	throw;
	return *this;
}

KeyShortcut::KeyShortcut()
	: Hash(0)
{}

KeyShortcut::KeyShortcut(Key first)
	: Hash(0)
{
	Mode.Shortcut = ShortcutModes::Strict;
	Keys.First = first;
	Mode.First = KeyModes::Mandatory;
}

KeyShortcut::KeyShortcut(Key first, Key second)
	: Hash(0)
{
	Mode.Shortcut = ShortcutModes::Strict;
	Keys.First = first;
	Mode.First = KeyModes::Mandatory;
	Keys.Second = second;
	Mode.Second = KeyModes::Mandatory;
}

KeyShortcut::KeyShortcut(Key first, Key second, Key third)
	: Hash(0)
{
	Mode.Shortcut = ShortcutModes::Strict;
	Keys.First = first;
	Mode.First = KeyModes::Mandatory;
	Keys.Second = second;
	Mode.Second = KeyModes::Mandatory;
	Keys.Third = third;
	Mode.Third = KeyModes::Mandatory;
}
