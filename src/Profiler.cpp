#include <DD/Concurrency/Atomic.h>
#include <DD/Debug.h>
#include <DD/Math.h>
#include <DD/Profiler.h>

using namespace DD;

/************************************************************************/
/* Profiler::WatchTraits                                                */
/************************************************************************/
#pragma region WatchTraits
void Profiler::WatchTraits::Log::Postprocess(Profiler::Scope & item, Profiler::Record record) {
	// nanoseconds
	u64 time = (1000000000 * (u64)record.Time) / Stopwatch::Frequency();
	u32 ns = time % 1000;
	u32 us = (time /= 1000) % 1000;
	u32 ms = (time /= 1000) % 1000;
	u32 s = (time /= 1000) % 1000;

	static auto format = [](String & s, u32 n) {
		if (n == 0)
			s.Append("000 ");
		else if (n < 10)
			s.AppendBatch("00", n, " ");
		else if (n < 100)
			s.AppendBatch("0", n, " ");
		else
			s.AppendBatch(n, " ");
	};

	StringLocal<32> timeStamp;
	timeStamp.AppendBatch(s, ".");
	format(timeStamp, ms);
	format(timeStamp, us);
	format(timeStamp, ns);
	timeStamp.Append('s');

	StringLocal<64> fileStamp;
	fileStamp.AppendFormat("{0}({1}):", item.ScopeFileName, item.ScopeFileLine);

	Debug::Log::PrintFormat(
		"{0:80} {3:64} {2:32} {1:-20}\n",
		/* 0 */ fileStamp,
		/* 1 */ timeStamp,
		/* 2 */ item.ScopeName,
		/* 3 */ item.ScopeFuntion
	);
}

void Profiler::WatchTraits::Limit::Postprocess(Profiler::Scope & item, Profiler::Record record) {
	Profiler::WatchTraits::RecLog::Postprocess(item, record);
	if (item.MaximalTime < record.Time) {
		Debug::Break();
	}
}

void Profiler::WatchTraits::Stats::Postprocess(Scope & scope, Record record) {
	static f32 conversion = 1000000.f  / (Stopwatch::Frequency());
	scope.Buffer.Data[Concurrency::Atomic::i32::Inc(scope.Buffer.Index) % 256] = record.Time * conversion;
}
#pragma endregion


/************************************************************************/
/* Profiler                                                             */
/************************************************************************/
#pragma region Profiler
Profiler::Buffer Profiler::_recordingBuffers[2];
Profiler::Scope Profiler::_profileScopes[1024];
i32 Profiler::_currentRecordingBuffer = 0;
i32 Profiler::_profileScopeTop = -1;

i32 Profiler::ProcessScopes() {
	Buffer& buffer = _recordingBuffers[_currentRecordingBuffer];
	_currentRecordingBuffer = !_currentRecordingBuffer;
	// memory barrier
	for (i32 i = 0; i <= buffer.Top; ++i) {
		Record & record = buffer.Records[i];
		Scope & item = _profileScopes[record.Index];
		item.TotalTime += record.Time;
		item.MinimalTime = Math::Min(item.MinimalTime, record.Time);
		item.MaximalTime = Math::Max(item.MaximalTime, record.Time);
		item.LastTime = record.Time;
		item.Count++;
	}
	i32 bufferSize = buffer.Top + 1;
	buffer.Top = -1;
	return bufferSize;
}

Profiler::Scope & Profiler::RegisterScope(const char * name, const char * place, const char * file, i32 line, i32 limit) {
	i32 index = Concurrency::Atomic::i32::Inc(_profileScopeTop);
	_profileScopes[index].ScopeName = name;
	_profileScopes[index].ScopeFuntion = place;
	_profileScopes[index].ScopeFileName = file;
	_profileScopes[index].ScopeFileLine = line;
	_profileScopes[index].Index = index;
	// ms to ticks
	_profileScopes[index].LimitTime = (limit * Stopwatch::Frequency()) / 1000;

	return _profileScopes[index];
}
#pragma endregion

