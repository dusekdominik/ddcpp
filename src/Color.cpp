#include <DD/Color.h>
#include <DD/Vector.h>
#include <math.h>

using namespace DD;

f32 Color::saturate(f32 f) {
	if (f < 0.f) return 0.f;
	if (f > 1.f) return 1.f;
	return f;
}

f32 Color::srgb(f32 f) {
	if (f > (0.0031308f * 12.92f))
		return static_cast<f32>(pow((f + 0.055f) * (1.0f / 1.055f), 1.6f));
	else
		return f * (1.0f / 12.92f);
}

Color Color::FromSRGB(f32 r, f32 g, f32 b) {
	return Color(srgb(r), srgb(g), srgb(b));
}

ColorHSV Color::ToHSV() const {
	ColorHSV c;
	u8 max = 0;
	u8 min = 255;

	if (max < r) max = r;
	if (max < g) max = g;
	if (max < b) max = b;

	if (min > r) min = r;
	if (min > g) min = g;
	if (min > b) min = b;

	u8 delta = max - min;

	if (delta == 0) c.h = 0;
	else if (max == r) c.h = 60 * (((g - b) / delta) % 6);
	else if (max == g) c.h = 60 * (((b - r) / delta) + 2);
	else c.h = 60 * (((r - g) / delta) + 4);

	if (max == 0) c.s = 0;
	else c.s = delta / max;

	if (c.h > 1024) c.h += 360;

	c.v = max;

	return c;
}

Color::Color(u8 r, u8 g, u8 b, u8 a)
	: r(r)
	, g(g)
	, b(b)
	, a(a)
{}

Color::Color(const float3 & color)
	: Color(color.x, color.y, color.z)
{}

Color::Color(const float4 & color)
	: Color(color.x, color.y, color.z, color.w)
{}

Color::Color(Colors::Values c)
	: Color(Color::Collection::COLORS[c])
{}

DD::Color Color::FromScale(f32 scale) {
	static const float3 DD_COLOR_BLUE   = float3(0.f, 0.f, 1.f);
	static const float3 DD_COLOR_GREEN  = float3(0.f, 1.f, 0.f);
	static const float3 DD_COLOR_YELLOW = float3(1.f, 1.f, 0.f);
	static const float3 DD_COLOR_RED    = float3(1.f, 0.f, 0.f);
	static auto Scale2Color = [](f32 iscale, const float3 & lColor, const float3 & hColor) {
		return hColor * iscale + lColor * (1.0f - iscale);
	};

	// extend scale according to number of colors
	float scaleEx = scale * 3;
	// integral part
	int scaleI = (int)scaleEx;
	// fractional part
	float part = scaleEx - scaleI;
	// select color
	switch (scaleI) {
	case 0: return Color(Scale2Color(part, DD_COLOR_BLUE, DD_COLOR_GREEN));
	case 1: return Color(Scale2Color(part, DD_COLOR_GREEN, DD_COLOR_YELLOW));
	case 2: return Color(Scale2Color(part, DD_COLOR_YELLOW, DD_COLOR_RED));
	case 3: return DD_COLOR_RED;
		// out of bounds - detectable by alpha channel
	default: return Color(0x00000000);
	}
}

DD::Color::Color(Colors c)
	: Color(Color::Collection::COLORS[c])
{}

Color::Color(u8 c)
	: r(c)
	, g(c)
	, b(c)
	, a(255)
{}

Color::Color(f32 c)
	: a(255)
{
	r = g = b = static_cast<u8>(saturate(c) * 255.f);
}

Color::Color(f32 r, f32 g, f32 b)
	: r(static_cast<u8>(saturate(r) * 255.f))
	, g(static_cast<u8>(saturate(g) * 255.f))
	, b(static_cast<u8>(saturate(b) * 255.f))
	, a(255)
{}

Color::Color(f32 r, f32 g, f32 b, f32 a)
	: r(static_cast<u8>(saturate(r) * 255.f))
	, g(static_cast<u8>(saturate(g) * 255.f))
	, b(static_cast<u8>(saturate(b) * 255.f))
	, a(static_cast<u8>(saturate(a) * 255.f))
{}


bool ColorHSV::IsGreen() const {
	return Math::Abs(HUE_GREEN - h) < HUE_TOLERANCE;
}
