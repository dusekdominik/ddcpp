#include <DD/Chain.h>
#include <DD/Random.h>
#include <DD/Concurrency/ThreadPool.h>
#include <DD/Net/Server.h>

#if 1
#include <DD/Debug.h>
#define NOTICE(...) DD::Debug::Log::PrintLine(__FUNCTION__ " - ", __VA_ARGS__)
#else
#include <iostream>
#define NOTICE_ONE(arg) << arg
#define NOTICE(...) std::cout << __FUNCTION__ << " - " DD_XLIST(NOTICE_ONE, __VA_ARGS__) << std::endl;
#define DD_WARNING(...) std::cout << __FUNCTION__ << " - Warning: " DD_XLIST(NOTICE_ONE, __VA_ARGS__) << std::endl;
#endif

using namespace DD;
using namespace DD::Net;

/************************************************************************/
/* HELPER DECLARATIONS                                                  */
/************************************************************************/
#pragma region Helpers declaration
struct MessageInput {
	Event<Server::Message> OnMessageCollected;
	Server::Message Message;
	u32 CollectedPackets;
	u32 TotalPackets;
	u64 Bytes;
	bool Check() { return (TotalPackets == CollectedPackets); }
	void Feed(const Server::Packet & packet);
	MessageInput(const Server::Packet & packet);
	MessageInput() {}
};

struct MessagePacketizer {
	const Server::Message & Message;
	Server::Packet OutputPacket;
	u32 PacketCount;
	u32 GetTotalPacketCount() const { return OutputPacket.Header.TotalPackets; }
	bool IsLastPacket(u32 packetNumber) const { return packetNumber + 1 >= OutputPacket.Header.TotalPackets; }
	const u8 * GetPacketBegin(u32 packetNumber) const { return Message.Data.begin() + packetNumber * Server::Packet::DATA_SIZE; }
	u16 GetLastPacketSize() const { return Message.Data.Length() ? (Message.Data.Length() % Server::Packet::DATA_SIZE ? (u16)Message.Data.Length() % Server::Packet::DATA_SIZE : Server::Packet::DATA_SIZE) : 0; }
	u16 GetNormalPacketSize() const { return Server::Packet::DATA_SIZE; }
	u16 GetPacketSize(u32 packetNumber) const { return IsLastPacket(packetNumber) ? GetLastPacketSize() : GetNormalPacketSize(); }
	const Server::Packet & GetPacket(u32 packetNumber);
	MessagePacketizer(const Server::Message & message);
};

struct MessageCollector {
	Chain<MessageInput> Collection;
	bool Contains(const Server::Packet & packet) const {
		for (const MessageInput & input : Collection) {
			if (input.Message.Header.MessageId == packet.Header.Message.MessageId)
				return true;
		}
		return false;
	}
	void AddPacket(const Server::Packet & packet) {

	}
};
#pragma endregion
/************************************************************************/
/* HELPER DEFINITIONS                                                   */
/************************************************************************/
#pragma region Helpers definition
inline void MessageInput::Feed(const Server::Packet & packet) {
	CollectedPackets += 1;
	Bytes += packet.Header.Size;
	u64 begin = packet.Header.Index * Server::Packet::DATA_SIZE;
	Message.Data.ToView().SubView(begin, packet.Header.Size).CopyIn(
		ArrayView(packet.IData, packet.Header.Size)
	);

	if (Check()) {
		Message.Data.ResizeUnsafe(Bytes);
		OnMessageCollected(Message);
	}
}

inline MessageInput::MessageInput(const Server::Packet & packet)
	: CollectedPackets(0)
	, Bytes(0)
{
	TotalPackets = packet.Header.TotalPackets;
	Message.Header = packet.Header.Message;
	if (packet.Header.TotalPackets == 1)
		Message.Data.Reallocate(packet.Header.Size);
	else
		Message.Data.Reallocate(packet.Header.TotalPackets * Server::Packet::DATA_SIZE);

	Feed(packet);
}

inline const Server::Packet & MessagePacketizer::GetPacket(u32 packetNumber) {
	OutputPacket.Header.Index = packetNumber;
	OutputPacket.Header.Size = GetPacketSize(packetNumber);
	ArrayView1D<u8> target(OutputPacket.UData, OutputPacket.Header.Size);
	ArrayView1D<const u8> source(GetPacketBegin(packetNumber), OutputPacket.Header.Size);
	if (OutputPacket.Header.Size) {
		target.CopyIn(source);
	};

	return OutputPacket;
}

inline MessagePacketizer::MessagePacketizer(const Server::Message & message)
	: Message(message)
{
	OutputPacket.Header.Message = Message.Header;
	OutputPacket.Header.TotalPackets = (u32)(Message.Data.Length() / Server::Packet::DATA_SIZE);
	if (Message.Data.Length() % Server::Packet::DATA_SIZE)
		++OutputPacket.Header.TotalPackets;
	if (OutputPacket.Header.TotalPackets == 0)
		++OutputPacket.Header.TotalPackets;
}
#pragma endregion

/************************************************************************/
/* IClient                                                              */
/************************************************************************/
#pragma region IClient
template<typename T>
void Server::IClient<T>::ReceivePacket(Server::Packet & packet, IPv4 & from) {
	int size = Packet::SIZE;
	Base::Receive((char*)&packet, size, from);
	if (Base::IsValid()) {
		if (size < (int)Packet::HEADER_SIZE) {
			DD_WARNING("Ilegal packet size.");
			packet.Header.Message.MessageType = MESSAGE_INVALID;
		}
		else if (packet.Header.Message.ServerSignature != SERVER_SIGNATURE) {
			DD_WARNING("Ilegal packet signature.");
			packet.Header.Message.MessageType = MESSAGE_INVALID;
		}
	}
}

template<typename T>
bool Server::IClient<T>::ReceiveMessage(Server::Message & message, Packet & packet) {
	if (Base::IsValid()) {
		ReceivePacket(packet, message.SenderIp);
		if (packet.Header.Message.MessageType == MESSAGE_INVALID) {
			return false;
		}
		if (packet.Header.TotalPackets > 1) {
			DD_WARNING("Multipacket messages are not handled.");
			return false;
		}
		message.Header = packet.Header.Message;
		message.Data.Swap(Array<u8>({ packet.UData, packet.Header.Size }, nullptr));
		return true;
	}
	return false;
}

template<typename T>
void Server::IClient<T>::SendMessage(Server::Message & msg) {
	ParentServer->adoptMessage(msg);
	MessagePacketizer packetizer(msg);
	for (u32 i = 0, m = packetizer.GetTotalPacketCount(); i < m; ++i) {
		const Packet & packet = packetizer.GetPacket(i);
		if (!Base::IsValid()) {
			DD_WARNING("Sending failed.");
			return;
		}
		Base::Send((char*)&packet, packet.Size());
	}
}

#pragma endregion

void Server::messageId(const Message & msg) {
	// has not been initialized yet
	if (_serverId == 0) {
		if (!idAlreadySeen(msg.Header.MessageSender)) {
			_servers->Add(msg.Header.MessageSender);
		}
	}
	else {
		// reserved id
		if (msg.Header.MessageSender == 0 || msg.Header.MessageSender == _serverId) {
			return;
		}
		// seen
		{
			auto proxy = _clients.GetProxy();
			for (const Tcp & server : proxy.Object()) {
				if (server.ClientId == msg.Header.MessageSender) {
					return;
				}
			}
		}
		// not seen
		Tcp client;
		client.ParentServer = this;
		client.ClientId = msg.Header.MessageSender;
		client.Type = Tcp::Out;
		client.Ip._ip = msg.SenderIp._ip;
		client.Ip._port = *((u16*)msg.Data.begin());

		NOTICE("Trying to connect(", client.Ip.ToString(), ").");
		client._client = TCPClient(client.Ip);
		if (!client._client->IsValid()) {
			NOTICE("Cannot establish connection(", client.Ip.ToString(), "). ", client._client->State().ToString());
			return;
		}

		Message outHello(MESSAGE_HELLO);
		Message inHello;
		Packet cache;
		NOTICE("Sending Hello message (to: ", client.Ip.ToString(), ").");
		client.SendMessage(outHello);
		NOTICE("Receiving Hello message (from: ", client.Ip.ToString(), ").");
		if (!client.ReceiveMessage(inHello, cache)) {
			DD_WARNING("Client failed.");
			return;
		}
		if (inHello.Header.MessageType != MESSAGE_HELLO) {
			DD_WARNING("Respond was not hello message.");
			return;
		}
		if (inHello.Header.MessageSender != msg.Header.MessageSender) {
			DD_WARNING("Incompatible id's.");
			return;
		}
		NOTICE("Registering valid client.");
		addClient(client);
	}
}

void Server::directClientLoop(Tcp client) {
	Server::Message message;
	Server::Packet packet;

	while (client.IsValid()) {
		client.ReceiveMessage(message, packet);

		if (message.Header.MessageType & MESSAGE_SYSTEM) {
			systemMessageProcedure(message);
			if (_suppressSystemMessages)
				continue;
		}

		OnMessageReceived(message, client);
	}


	NOTICE("Removing client ", client.ClientId, ".");
	auto proxy = *_clients;
	for (size_t i = 0, m = proxy->Size(); i < m; ++i) {
		if (client.ClientId == (*proxy)[i].ClientId) {
			proxy->RemoveAt(i);
			return;
		}
	}
}

void Server::broadcastLoop() {
	Server::Packet packet;
	Server::Message message;
	while (_broadcaster.IsValid()) {
		_broadcaster.ReceiveMessage(message, packet);
		if (_suppressMyMessages && message.Header.MessageSender == _serverId) {
			continue;
		}

		if (message.Header.MessageType & MESSAGE_SYSTEM) {
			systemMessageProcedure(message);
			if (_suppressSystemMessages)
				continue;
		}

		NOTICE("UDP message received.");
		OnBroadcastReceived(message);
	}
	DD_WARNING("Broadcast failed.");
}

void Server::broadcastIdLoop() {
	Server::Message idMessage(MESSAGE_ID);
	ArrayView1D<u8> data((u8 *)&(_tcp->IPAddress()._port), 2);
	idMessage.Data.Reallocate(2).CopyIn(data);
	while (1) {
		//NOTICE("Broadcasting.");
		_broadcaster.SendMessage(idMessage);
		Concurrency::Thread::Sleep(5000);
	}
}

void Server::acceptProcedure(TCPClient tcp) {
	NOTICE("Accepting client ", tcp->IPAddress().ToString(), ".");

	Tcp client;
	client._client = tcp;
	client.ParentServer = this;
	client.Type = Tcp::In;
	NOTICE("Accepting client - waiting for hello message.");
	Server::Message inHello;
	Server::Message outHello(MESSAGE_HELLO);
	Server::Packet cache;

	NOTICE("Receiving Hello message.");
	client.ReceiveMessage(inHello, cache);
	NOTICE("Sending Hello message.");
	client.SendMessage(outHello);

	if (inHello.Header.MessageType != MESSAGE_HELLO) {
		DD_WARNING("Accepted client is not sending hello message. Message type was ", inHello.Header.MessageType, ".");
		return;
	}
	else {
		client.ClientId = inHello.Header.MessageSender;
		NOTICE("Accepting client - hello message received. Id is ", client.ClientId, ".");
	}

	addClient(client);
}

void Server::systemMessageProcedure(Server::Message & message) {
	switch (message.Header.MessageType) {
	case MESSAGE_ID:
		messageId(message);
		break;
	}
}

void Server::addClient(Tcp & client) {
	if (client.ClientId == _serverId) {
		NOTICE("Add client failed - Server is trying to connect on itself.");
		return;
	}

	auto proxy = _clients.GetProxy();
	for (Tcp & c : *proxy) {
		if (c.ClientId == client.ClientId) {
			NOTICE("Add client failed - client already exists.");
			return;
		}
	}

	proxy->Add(client);

	_threads->Add([this, client]() mutable {directClientLoop(client);});
}

bool Server::idAlreadySeen(u16 id) const {
	// reserved
	if (id == 0)
		return true;
	// seen
	auto proxy = _servers.GetProxy();
	for (u16 serverId : proxy.Object())
		if (serverId == id)
			return true;
	// not seen
	return false;
}

void Server::init() {
	NOTICE("Init of broadcaster sockets.");
	_broadcaster.ParentServer = this;
	_broadcaster._emitter = UDPEmitter(BROADCAST_PORT);
	_broadcaster._receiver = UDPReceiver(BROADCAST_PORT);

	NOTICE("Starting receiving thread.");
	_threads->Add([this]() {broadcastLoop();});

	NOTICE("Waiting for another servers.");
	Concurrency::Thread::Sleep(STARTUP_LISTENING);

	NOTICE("Searching for free server id.");
	while (idAlreadySeen(_serverId = Random::Generate<u32>()));

	NOTICE("Searching for free TCP socket.");
	for (u16 i = 0; i < ATTEMPT_COUNT; ++i) {
		_tcp = TCPServer(DIRECT_PORT + i);
		if (_tcp->IsValid())
			break;
	}

	if (_tcp->IsValid()) {
		NOTICE("Searched TCP server ", _tcp->IPAddress().ToString(), ".");
	}
	else {
		DD_WARNING("No free tcp port.");
	}

	NOTICE("Setting listening routine of tcp server.");
	_tcp->OnClientAccepted += [this](TCPClient tcp) {acceptProcedure(tcp);};
	_tcp->StartAccepting();

	NOTICE("Establish heartbeet.");
	_threads->Add([this]() mutable {broadcastIdLoop();});
}

void Server::adoptMessage(Message & msg) {
	msg.Header.MessageId = GetMessageId();
	msg.Header.MessageSender = _serverId;
	msg.Header.ServerSignature = SERVER_SIGNATURE;
}

void Server::SendBroadcast(Message & msg) {
	_broadcaster.SendMessage(msg);
}

void Server::SendDirect(Message & msg) {
	auto proxy = *_clients;
	for (Tcp & tcp : *proxy) {
		tcp.SendMessage(msg);
	}
}

void Server::SendDirect(Message & msg, u16 id) {
	auto proxy = *_clients;
	for (Tcp & tcp : *proxy) {
		if (tcp.ClientId == id) {
			tcp.SendMessage(msg);
		}
	}
}

/************************************************************************/
/* Server                                                               */
/************************************************************************/
Server::Server()
	: _suppressSystemMessages(true)
	, _suppressMyMessages(true)
{
	_threads->Add([this]() {init();});
}