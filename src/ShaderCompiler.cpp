#include <DD/ShaderCompiler.h>
#include <DD/Debug.h>
#include <d3dcompiler.h>
#include <fstream>
#include <d3d11.h>
using namespace DD;
#if 0
PixelShader ShaderCompiler::PS(const String & name) {
	PixelShader result;
	ID3D11PixelShader * shader;
	auto file = (*this)(name, _psmodel);
	_device.Device()->CreatePixelShader(file->GetBufferPointer(), file->GetBufferSize(), 0, &shader);
	file->Release();
	return PixelShader(_device, name);
}

PixelShader DD::ShaderCompiler::CreatePixelShader(Devices & device, const String & name) {
	return PixelShader(device, name);
}

VertexShader DD::ShaderCompiler::CreateVertexShader(Devices & device, const String & name, const layout_t & layout) {
	String cso;
	cso
		.Reserve(name.Length() + 5)
		.Append(name)
		.Append(".cso")
		;
	ID3DBlob * file = nullptr;
	D3DReadFileToBlob(cso.begin(), &file);


	std::ifstream stream(cso.ToCWString(), std::ios::binary);
	stream.seekg(0, std::ios::end);
	size_t size = stream.tellg();
	char * data = new char[size];
	stream.seekg(0, std::ios::beg);
	stream.read(data, size);

	ID3D11VertexShader * shader = nullptr;
	HRESULT hr = device.Device()->CreateVertexShader(data, size, 0, &shader);
	if (hr != S_OK) throw;

	ID3D11InputLayout * l;
	device.Device()->CreateInputLayout( layout.data(),
		(unsigned int)layout.size(),
		data,
		size,
		&l
		);


	VertexShader result;
//	result = shader;
//	result = l;
	return result;
}

VertexShader DD::ShaderCompiler::CreateVertexShader(Devices & device, const String & name, layoutRaw_t layout, size_t layoutSize)
{
	String cso;
	cso
		.Reserve(name.Length() + 5)
		.Append(name)
		.Append(".cso")
		;
	ID3DBlob * file = nullptr;
	D3DReadFileToBlob(cso.begin(), &file);


	std::ifstream stream(cso.begin(), std::ios::binary);
	stream.seekg(0, std::ios::end);
	size_t size = stream.tellg();
	char * data = new char[size];
	stream.seekg(0, std::ios::beg);
	stream.read(data, size);

	ID3D11VertexShader * shader = nullptr;
	HRESULT hr = device.Device()->CreateVertexShader(data, size, 0, &shader);
	if (hr != S_OK) throw;

	ID3D11InputLayout * l;
	device.Device()->CreateInputLayout(layout,
		(unsigned int)layoutSize,
		data,
		size,
		&l
		);


	VertexShader result;
//	result = shader;
//	result = l;
	return result;
}

VertexShader ShaderCompiler::VS(const String & name, const layout_t & layout) {
	VertexShader result;
	ID3D11VertexShader* s;
	ID3D11InputLayout* l;
	auto file = (*this)(name, _vsmodel);
	//ID3DBlob * file;
	/*		if (name.find(".cso") != std::string::npos) file = (*this)(name);
	else*/
	//			file = (*this)(name, _vsmodel);
	_device.Device()->CreateVertexShader(file->GetBufferPointer(), file->GetBufferSize(), 0, &s);
	_device.Device()->CreateInputLayout(
		layout.data(),
		(unsigned int)layout.size(),
		file->GetBufferPointer(),
		file->GetBufferSize(),
		&l
		);
	file->Release();
//	result = s;
//	result = l;
	return result;
}

ID3DBlob * ShaderCompiler::operator()(const String & name, const String & model) {
	_error.Clear();
	ID3DBlob* ppError = nullptr;
	ID3DBlob* ppOut = nullptr;
	D3DCompileFromFile(_path.ToCWString(), 0, 0, name.ToString().c_str(), model.ToString().c_str(), _flags, 0, &ppOut, &ppError);
	if (ppError) {
		_error = reinterpret_cast<const char*>(ppError->GetBufferPointer());
		Debug::PrintLine("ShaderCompiler ERROR : ", _error);
		ppError->Release();
	}
	return ppOut;
}

ID3DBlob * ShaderCompiler::operator()(const String & cso) {
	ID3DBlob * blob;
	D3DReadFileToBlob(cso.ToCWString(), &blob);
	return blob;
}

ID3DBlob * DD::ShaderCompiler::GetPSFromCSO(const String & name)
{
	auto file = (*this)(_pspath);

	PixelShader result;
	ID3D11PixelShader * shader;
	_device.Device()->CreatePixelShader(file->GetBufferPointer(), file->GetBufferSize(), 0, &shader);
	file->Release();

	return nullptr;
}



ShaderCompiler::ShaderCompiler(Gpu::Devices & device, const String & vspath, const String & pspath)
	: _device(device)
	, _vspath(vspath)
	, _pspath(pspath)
{

}
#endif

ShaderCompiler::ShaderCompiler(Gpu::Devices & device, Text::StringView16 path, Text::StringView16 vsmodel, Text::StringView16 psmodel) :
	_device(device),
	_path(path),
	_vsmodel(vsmodel),
	_psmodel(psmodel),
	_flags(D3DCOMPILE_ENABLE_STRICTNESS
	) {
#ifdef _DEBUG
	_flags |= D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION;
#endif
}