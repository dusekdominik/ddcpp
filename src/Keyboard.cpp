#include <DD/Input/Keyboard.h>
#include <DD/Debug.h>
#include <windows.h>

using namespace DD;
using namespace DD::Input;

bool Keyboard::Refresh() {
	// return GetKeyboardState(_data) != 0;
	_pressed = 0;
	for (u32 i = 0; i < Key::Size(); ++i) {
		Key key = Key::FromIndex(i);
		_data[key] = GetAsyncKeyState(key);
		if (Check(key)) {
			switch (key) {
				// following keys make duplicities, so ignore it
				case Key::LeftAlt:
				case Key::LeftCtrl:
				case Key::LeftShift:
				case Key::LeftWin:
				case Key::RightAlt:
				case Key::RightCtrl:
				case Key::RightShift:
				case Key::RightWin:
					break;
				default:
					++_pressed;
					break;
			}
		}
	}

	return true;
}

#define _CHECK_SHORTCUT(mode, key)                                      \
	switch (mode)                                                         \
	{                                                                     \
	case KeyShortcut::KeyModes::Admissible:                               \
		if (Check(key))                                                     \
			++admissible;                                                     \
		break;                                                              \
	case KeyShortcut::KeyModes::Mandatory:                                \
		if (Check(key))                                                     \
			++mandatory;                                                      \
		else                                                                \
			return false;                                                     \
		break;                                                              \
	case KeyShortcut::KeyModes::Undefined:                                \
		return                                                              \
			/* either benevolent or sum-up must be strict  */                 \
			s.Mode.Shortcut == KeyShortcut::ShortcutModes::Benevolent ||      \
			(admissible + mandatory) == _pressed;                             \
	}                                                                     \

bool Keyboard::Check(KeyShortcut s) const {
	u32 admissible = 0;
	u32 mandatory = 0;
	// go through the checks
	_CHECK_SHORTCUT(s.Mode.First, s.Keys.First);
	_CHECK_SHORTCUT(s.Mode.Second, s.Keys.Second);
	_CHECK_SHORTCUT(s.Mode.Third, s.Keys.Third);

	return
		s.Mode.Shortcut == KeyShortcut::ShortcutModes::Benevolent ||
		(admissible + mandatory) == _pressed;
}

KeyModifiers Keyboard::Modifiers() const {
	return KeyModifiers(
		Check(Key:: LeftCtrl),
		Check(Key::RightCtrl),
		Check(Key:: LeftAlt),
		Check(Key::RightAlt),
		Check(Key:: LeftShift),
		Check(Key::RightShift)
	);
}
