#include <DD/Environment.h>

#include <Windows.h>
#include <WinUser.h>

#pragma comment(lib, "User32.lib")

using namespace DD;

/************************************************************************/
/* Helpers                                                              */
/************************************************************************/
static struct _VirtualDesktop {
	/** Properties. */
	i32 XMin, XMax, XSize, YMin, YMax, YSize, DisplayCount;
	/** Load values from system.*/
	void refresh();
	/** Constructor. */
	_VirtualDesktop() { refresh(); }
} & virtualDesktop() { static _VirtualDesktop instance; return instance; };

inline void _VirtualDesktop::refresh() {
	XMin = GetSystemMetrics(SM_XVIRTUALSCREEN);
	XMax = GetSystemMetrics(SM_CXVIRTUALSCREEN);
	XSize = XMax - XMin;
	YMin = GetSystemMetrics(SM_YVIRTUALSCREEN);
	YMax = GetSystemMetrics(SM_CYVIRTUALSCREEN);
	YSize = YMax - YMin;
	DisplayCount = GetSystemMetrics(SM_CMONITORS);
}

/************************************************************************/
/* Implementation                                                       */
/************************************************************************/

AABox2I Environment::VirtualDesktop::Box() {
	return AABox2I(Begin(), End());
}

int2 Environment::VirtualDesktop::Begin() {
	return int2(virtualDesktop().XMin, virtualDesktop().YMin);
}

int2 Environment::VirtualDesktop::End() {
	return int2(virtualDesktop().XMax, virtualDesktop().YMax);
}

int2 Environment::VirtualDesktop::Size() {
	return int2(virtualDesktop().XSize, virtualDesktop().YSize);
}

i32 Environment::VirtualDesktop::Displays() {
	return virtualDesktop().DisplayCount;
}

