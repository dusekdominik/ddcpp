#include <DD/Gpu/Texture.h>
#include <DD/Gpu/Devices.h>
#include <DD/Debug.h>

#include <d3d11.h>

using namespace DD;
using namespace DD::Gpu;

void Texture::Bind2RT() {
	DD_DEBUG_ASSERT(_device, "Texture is not initialized.");

	_device->TextureBind2RT(*this);
}

void Texture::Bind2PS(u32 slot/* = 0*/) {
	DD_DEBUG_ASSERT(_device, "Texture is not initialized.");

	_device->TextureBind2PS(*this, slot);
}

void Texture::Bind2VS() {
	DD_DEBUG_ASSERT(_device, "Texture is not initialized.");

	_device->TextureBind2VS(*this);
}

void Texture::Bind2CS() {
	DD_DEBUG_ASSERT(_device, "Texture is not initialized.");

	_device->TextureBind2CS(*this);
}

void Texture::ClearRT(const float * color) {
	DD_DEBUG_ASSERT(_device, "Texture is not initialized.");

	_device->TextureClearRT(*this, color);
}

void Texture::Init(Device & device) {
	DD_DEBUG_ASSERT(!_device, "Texture has been already initialized.");

	_device = device;
	_device->TextureInit(*this);
}

