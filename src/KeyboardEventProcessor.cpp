#include <DD/Input/KeyboardEventProcessor.h>
#include <DD/Stopwatch.h>

using namespace DD;
using namespace DD::Input;

void KeyboardEventProcessor::processKeyboardAction(i64 time, i32 key, i32 pressed, KeyModifiers modifiers) {
	if (pressed) {
		_times[key] = time + 150;
		switch (_states[key]) {
		case KeyState::Up:
			_states[key] = KeyState::Down;
			processActions(_onWrite, (Key::Values)key, modifiers);
			processActions(_onKeyDown, (Key::Values)key, modifiers);
			break;
		case KeyState::Down:
			processActions(_onWrite, (Key::Values)key, modifiers);
			break;
		}
	}
	else if (_states[key] == KeyState::Down) {
		_states[key] = KeyState::Up;
		_times[key] = time;
		processActions(_onKeyUp, (Key::Values)key, modifiers);
	}
}

void KeyboardEventProcessor::processActions(List<KeyboardEvent> & actions, Key key, KeyModifiers modifiers) {
	for (auto & on : actions) {
		if (on->IsEnabled()) {
			on->ProcessKey(key, modifiers);
		}
	}
}

void KeyboardEventProcessor::processDisposal(List<KeyboardEvent> & actions) {
	size_t index = actions.Size();

	while (index--) {
		if (actions[index]->IsDisposable()) {
			actions[index]->Dipose();
			actions.RemoveAt(index);
		}
	}
}

void KeyboardEventProcessor::processDisposalAll() {
	processDisposal(_onWrite);
	processDisposal(_onKeyUp);
	processDisposal(_onKeyDown);
}

bool KeyboardEventProcessor::registerEvent(List<KeyboardEvent> & target, KeyboardEvent event, Events type) {
	if (!target.Contains(event)) {
		event->OnRegister(type);
		target.Add(event);

		return true;
	}

	return false;
}

void KeyboardEventProcessor::processKeyboard(const Keyboard & keyboard) {
	i64 time = Stopwatch::TimeStampMilliseconds();
	KeyModifiers modifiers = keyboard.Modifiers();
	for (u32 i = 0, m = Key::Size(); i < m; ++i) {
		Key key = Key::FromIndex(i);
		if (_times[key] < time) {
			processKeyboardAction(time, key, (i32)keyboard[key], modifiers);
		}
	}

	processDisposalAll();
}

KeyboardEventProcessor::KeyboardEventProcessor() {
	for (size_t i = 0; i < 256; ++i) {
		_times[i] = 0;
		_states[i] = KeyState::Up;
	}
}
