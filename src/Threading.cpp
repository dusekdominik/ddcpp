#include <DD/Concurrency/CriticalSection.h>
#include <DD/Concurrency/ConditionVariable.h>
#include <DD/Concurrency/Lock.h>

#include <Windows.h>

#pragma warning(disable:4065) // switch statement contains 'default' but no 'case' labels

using namespace DD;
using namespace DD::Concurrency;

/************************************************************************/
/* CriticalSection                                                      */
/************************************************************************/

struct CriticalSection::NativeView
	: public CRITICAL_SECTION
{};


void CriticalSection::Enter() const {
	EnterCriticalSection(&_criticalSection);
}


void CriticalSection::Leave() const {
	LeaveCriticalSection(&_criticalSection);
}


bool CriticalSection::TryEnter() const {
	return (bool)TryEnterCriticalSection(&_criticalSection);
}


SleepingResult CriticalSection::Sleep(const ConditionVariable & cv) {
	return cv.SleepCriticalSection(*this);
}

SleepingResult CriticalSection::Sleep(const ConditionVariable & cv, int milliseconds) {
	return cv.SleepCriticalSection(*this, milliseconds);
}


CriticalSection::CriticalSection() {
	InitializeCriticalSection(&_criticalSection);
}

CriticalSection::CriticalSection(u32 spinCount) {
	InitializeCriticalSectionAndSpinCount(&_criticalSection, (DWORD)spinCount);
}

CriticalSection::~CriticalSection() {
	DeleteCriticalSection(&_criticalSection);
}

/************************************************************************/
/* Lock                                                                 */
/************************************************************************/

struct Lock::NativeView : public SRWLOCK {};

void Lock::AcquireExclusiveAccess() const {
	AcquireSRWLockExclusive(nativeView());
}

void Lock::AcquireSharedAccess() const {
	AcquireSRWLockShared(nativeView());
}

void Lock::ReleaseExclusiveAccess() const {
	ReleaseSRWLockExclusive(nativeView());
}

void Lock::ReleaseSharedAccess() const {
	ReleaseSRWLockShared(nativeView());
}

bool Lock::TryAcquireExclusiveAccess() const {
	return TryAcquireSRWLockExclusive(nativeView());
}

bool Lock::TryAcquireSharedAccess() const {
	return TryAcquireSRWLockShared(nativeView());
}

SleepingResult Lock::SleepShared(const ConditionVariable & cv) {
	return cv.SleepLockShared(*this);
}

SleepingResult Lock::SleepShared(const ConditionVariable & cv, int milliseconds) {
	return cv.SleepLockShared(*this, milliseconds);
}

SleepingResult Lock::SleepExclusive(const ConditionVariable & cv) {
	return cv.SleepLockExclusive(*this);
}

SleepingResult Lock::SleepExclusive(const ConditionVariable & cv, int milliseconds) {
	return cv.SleepLockExclusive(*this, milliseconds);
}

Lock::Lock() {
	InitializeSRWLock(nativeView());
}

Lock::~Lock() {}

/************************************************************************/
/* ConditionVariable                                                    */
/************************************************************************/

struct ConditionVariable::NativeView
	: public CONDITION_VARIABLE
{};


SleepingResult ConditionVariable::SleepCriticalSection(const CriticalSection & cs) const {
	BOOL result = SleepConditionVariableCS(nativeView(), cs.nativeView(), INFINITE);
	if (result)
		return SleepingResult::Succeed;

	switch (GetLastError()) {
	default:
		return SleepingResult::Error;
	}
}

SleepingResult ConditionVariable::SleepCriticalSection(const CriticalSection & cs, int milliseconds) const {
	BOOL result = SleepConditionVariableCS(nativeView(), cs.nativeView(), milliseconds);
	if (result)
		return SleepingResult::Succeed;

	switch (GetLastError()) {
	case ERROR_TIMEOUT:
		return SleepingResult::Timeout;
	default:
		return SleepingResult::Error;
	}
}

SleepingResult ConditionVariable::SleepLockExclusive(const Lock & lock) const {
	BOOL result = SleepConditionVariableSRW(nativeView(), lock.nativeView(), INFINITE, 0);
	if (result)
		return SleepingResult::Succeed;

	switch (GetLastError()) {
	default:
		return SleepingResult::Error;
	}
}

SleepingResult ConditionVariable::SleepLockExclusive(const Lock & lock, int milliseconds) const {
	BOOL result = SleepConditionVariableSRW(nativeView(), lock.nativeView(), milliseconds, 0);
	if (result)
		return SleepingResult::Succeed;

	switch (GetLastError()) {
	case ERROR_TIMEOUT:
		return SleepingResult::Timeout;
	default:
		return SleepingResult::Error;
	}
}

SleepingResult ConditionVariable::SleepLockShared(const Lock & lock) const {
	BOOL result = SleepConditionVariableSRW(nativeView(), lock.nativeView(), INFINITE, CONDITION_VARIABLE_LOCKMODE_SHARED);
	if (result)
		return SleepingResult::Succeed;

	switch (GetLastError()) {
	default:
		return SleepingResult::Error;
	}
}

SleepingResult ConditionVariable::SleepLockShared(const Lock & lock, int milliseconds) const {
	BOOL result = SleepConditionVariableSRW(nativeView(), lock.nativeView(), milliseconds, CONDITION_VARIABLE_LOCKMODE_SHARED);
	if (result)
		return SleepingResult::Succeed;

	switch (GetLastError()) {
	case ERROR_TIMEOUT:
		return SleepingResult::Timeout;
	default:
		return SleepingResult::Error;
	}
}


void ConditionVariable::WakeOne() const {
	WakeConditionVariable(nativeView());
}

void ConditionVariable::WakeAll() const {
	WakeAllConditionVariable(nativeView());
}

ConditionVariable::ConditionVariable() {
	InitializeConditionVariable(nativeView());
}

ConditionVariable::~ConditionVariable() {}
