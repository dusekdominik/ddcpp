#include <DD/Gpu/Devices.h>
#include <DD/Gpu/VertexShader.h>
#include <d3d11.h>

using namespace DD;
using namespace DD::Gpu;

void VertexShader::Set() {
	_device->VertexShaderSet(*this);
}

void VertexShader::Load(Device & device, const Text::StringView16 & path, const void * layout, u32 size) {
	_device = device;
	_device->VertexShaderLoad(*this, path, layout, size);
}

