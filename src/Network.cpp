#include <WinSock2.h>
#include <ws2tcpip.h> // addrinfo
#include <windows.h>
#include <mutex>
#undef MemoryBarrier

#include <DD/Concurrency/ThreadPool.h>
#include <DD/Debug.h>
#include <DD/Net/IPv4.h>
#include <DD/Net/SocketBase.h>
#include <DD/Net/TCPClient.h>
#include <DD/Net/TCPServer.h>
#include <DD/Net/UDPEmitter.h>
#include <DD/Net/UDPReceiver.h>

#pragma comment(lib, "Ws2_32.lib")

#define DEBUG_SOCKET(...) Debug::Log::PrintLine("NET::Socket: ", __VA_ARGS__)
#define DEBUG_WINSOCKS(...) Debug::Log::PrintLine("NET: ", __VA_ARGS__)
#define VALIDITY_CHECK \
if (_api->State != SocketStates::VALID) {\
	DEBUG_SOCKET("Warning, calling " __FUNCTION__ " on invalid socket.");\
	return;\
}

#define ERROR_DEBUG_CASE(name, value) case name: Debug::Log::PrintLine(__FUNCTION__ " - Error code: ", (u32) name, ";\n\t" value); break;
#define ERROR_LIST(X)\
X(WSA_INVALID_HANDLE, "Specified event object handle is invalid. An application attempts to use an event object, but the specified handle is not valid. Note that this error is returned by the operating system, so the error number may change in future releases of Windows.")\
X(WSA_NOT_ENOUGH_MEMORY, "Insufficient memory available. An application used a Windows Sockets function that directly maps to a Windows function.The Windows function is indicating a lack of required memory resources.Note that this error is returned by the operating system, so the error number may change in future releases of Windows.")\
X(WSA_INVALID_PARAMETER, "One or more parameters are invalid. An application used a Windows Sockets function which directly maps to a Windows function.The Windows function is indicating a problem with one or more parameters.Note that this error is returned by the operating system, so the error number may change in future releases of Windows.")\
X(WSA_OPERATION_ABORTED, "Overlapped operation aborted. An overlapped operation was canceled due to the closure of the socket, or the execution of the SIO_FLUSH command in WSAIoctl.Note that this error is returned by the operating system, so the error number may change in future releases of Windows.")\
X(WSA_IO_INCOMPLETE, "Overlapped I / O event object not in signaled state. The application has tried to determine the status of an overlapped operation which is not yet completed.Applications that use WSAGetOverlappedResult(with the fWait flag set to FALSE) in a polling mode to determine when an overlapped operation has completed, get this error code until the operation is complete.Note that this error is returned by the operating system, so the error number may change in future releases of Windows.")\
X(WSA_IO_PENDING, "Overlapped operations will complete later. The application has initiated an overlapped operation that cannot be completed immediately.A completion indication will be given later when the operation has been completed.Note that this error is returned by the operating system, so the error number may change in future releases of Windows.")\
X(WSAEINTR, "Interrupted function call. A blocking operation was interrupted by a call to WSACancelBlockingCall.")\
X(WSAEBADF, "File handle is not valid. The file handle supplied is not valid.")\
X(WSAEACCES, "Permission denied. An attempt was made to access a socket in a way forbidden by its access permissions.An example is using a broadcast address for sendto without broadcast permission being set using setsockopt(SO_BROADCAST). Another possible reason for the WSAEACCES error is that when the bind function is called(on Windows NT 4.0 with SP4 and later) another application, service, or kernel mode driver is bound to the same address with exclusive access.Such exclusive access is a new feature of Windows NT 4.0 with SP4 and later, and is implemented by using the SO_EXCLUSIVEADDRUSE option.")\
X(WSAEFAULT, "Bad address. The system detected an invalid pointer address in attempting to use a pointer argument of a call.This error occurs if an application passes an invalid pointer value, or if the length of the buffer is too small.For instance, if the length of an argument, which is a sockaddr structure, is smaller than the sizeof(sockaddr).")\
X(WSAEINVAL, "Invalid argument.	Some invalid argument was supplied(for example, specifying an invalid level to the setsockopt function).In some instances, it also refers to the current state of the socket�for instance, calling accept on a socket that is not listening.")\
X(WSAEMFILE, "Too many open files. Too many open sockets.Each implementation may have a maximum number of socket handles available, either globally, per process, or per thread.")\
X(WSAEWOULDBLOCK, "Resource temporarily unavailable. This error is returned from operations on nonblocking sockets that cannot be completed immediately, for example recv when no data is queued to be read from the socket.It is a nonfatal error, and the operation should be retried later.It is normal for WSAEWOULDBLOCK to be reported as the result from calling connect on a nonblocking SOCK_STREAM socket, since some time must elapse for the connection to be established.")\
X(WSAEINPROGRESS, "Operation now in progress.A blocking operation is currently executing.Windows Sockets only allows a single blocking operation�per - task or thread�to be outstanding, and if any other function call is made(whether or not it references that or any other socket) the function fails with the WSAEINPROGRESS error.")\
X(WSAEALREADY, "Operation already in progress. An operation was attempted on a nonblocking socket with an operation already in progress�that is, calling connect a second time on a nonblocking socket that is already connecting, or canceling an asynchronous request(WSAAsyncGetXbyY) that has already been canceled or completed.")\
X(WSAENOTSOCK, "Socket operation on nonsocket. An operation was attempted on something that is not a socket.Either the socket handle parameter did not reference a valid socket, or for select, a member of an fd_set was not valid.")\
X(WSAEDESTADDRREQ, "Destination address required. A required address was omitted from an operation on a socket.For example, this error is returned if sendto is called with the remote address of ADDR_ANY.")\
X(WSAEMSGSIZE, "Message too long. A message sent on a datagram socket was larger than the internal message buffer or some other network limit, or the buffer used to receive a datagram was smaller than the datagram itself.")\
X(WSAEPROTOTYPE, "Protocol wrong type for socket. A protocol was specified in the socket function call that does not support the semantics of the socket type requested.For example, the ARPA Internet UDP protocol cannot be specified with a socket type of SOCK_STREAM.")\
X(WSAENOPROTOOPT, "Bad protocol option. An unknown, invalid or unsupported option or level was specified in a getsockopt or setsockopt call.")\
X(WSAEPROTONOSUPPORT, "Protocol not supported. The requested protocol has not been configured into the system, or no implementation for it exists.For example, a socket call requests a SOCK_DGRAM socket, but specifies a stream protocol.")\
X(WSAESOCKTNOSUPPORT, "Socket type not supported. The support for the specified socket type does not exist in this address family.For example, the optional type SOCK_RAW might be selected in a socket call, and the implementation does not support SOCK_RAW sockets at all.")\
X(WSAEOPNOTSUPP, "Operation not supported. The attempted operation is not supported for the type of object referenced.Usually this occurs when a socket descriptor to a socket that cannot support this operation is trying to accept a connection on a datagram socket.")\
X(WSAEPFNOSUPPORT, "Protocol family not supported. The protocol family has not been configured into the system or no implementation for it exists.This message has a slightly different meaning from WSAEAFNOSUPPORT.However, it is interchangeable in most cases, and all Windows Sockets functions that return one of these messages also specify WSAEAFNOSUPPORT.")\
X(WSAEAFNOSUPPORT, "Address family not supported by protocol family. An address incompatible with the requested protocol was used.All sockets are created with an associated address family(that is, AF_INET for Internet Protocols) and a generic protocol type(that is, SOCK_STREAM).This error is returned if an incorrect protocol is explicitly requested in the socket call, or if an address of the wrong family is used for a socket, for example, in sendto.")\
X(WSAEADDRINUSE, "Address already in use. Typically, only one usage of each socket address(protocol / IP address / port) is permitted.This error occurs if an application attempts to bind a socket to an IP address / port that has already been used for an existing socket, or a socket that was not closed properly, or one that is still in the process of closing.For server applications that need to bind multiple sockets to the same port number, consider using setsockopt(SO_REUSEADDR).Client applications usually need not call bind at all�connect chooses an unused port automatically.When bind is called with a wildcard address(involving ADDR_ANY) a WSAEADDRINUSE error could be delayed until the specific address is committed.This could happen with a call to another function later, including connect, listen, WSAConnect, or WSAJoinLeaf.")\
X(WSAEADDRNOTAVAIL, "Cannot assign requested address. The requested address is not valid in its context.This normally results from an attempt to bind to an address that is not valid for the local computer.This can also result from connect, sendto, WSAConnect, WSAJoinLeaf, or WSASendTo when the remote address or port is not valid for a remote computer(for example, address or port 0).")\
X(WSAENETDOWN, "Network is down. A socket operation encountered a dead network.This could indicate a serious failure of the network system(that is, the protocol stack that the Windows Sockets DLL runs over), the network interface, or the local network itself.")\
X(WSAENETUNREACH, "Network is unreachable. A socket operation was attempted to an unreachable network.This usually means the local software knows no route to reach the remote host.")\
X(WSAENETRESET, "Network dropped connection on reset. The connection has been broken due to keep - alive activity detecting a failure while the operation was in progress.It can also be returned by setsockopt if an attempt is made to set SO_KEEPALIVE on a connection that has already failed.")\
X(WSAECONNABORTED, "Software caused connection abort. An established connection was aborted by the software in your host computer, possibly due to a data transmission time - out or protocol error.")\
X(WSAECONNRESET, "Connection reset by peer. An existing connection was forcibly closed by the remote host.This normally results if the peer application on the remote host is suddenly stopped, the host is rebooted, the host or remote network interface is disabled, or the remote host uses a hard close(see setsockopt for more information on the SO_LINGER option on the remote socket).This error may also result if a connection was broken due to keep - alive activity detecting a failure while one or more operations are in progress.Operations that were in progress fail with WSAENETRESET.Subsequent operations fail with WSAECONNRESET.")\
X(WSAENOBUFS, "No buffer space available. An operation on a socket could not be performed because the system lacked sufficient buffer space or because a queue was full.")\
X(WSAEISCONN, "Socket is already connected. A connect request was made on an already - connected socket.Some implementations also return this error if sendto is called on a connected SOCK_DGRAM socket(for SOCK_STREAM sockets, the to parameter in sendto is ignored) although other implementations treat this as a legal occurrence.")\
X(WSAENOTCONN, "Socket is not connected. A request to send or receive data was disallowed because the socket is not connected and (when sending on a datagram socket using sendto) no address was supplied.Any other type of operation might also return this error�for example, setsockopt setting SO_KEEPALIVE if the connection has been reset.")\
X(WSAESHUTDOWN, "Cannot send after socket shutdown. A request to send or receive data was disallowed because the socket had already been shut down in that direction with a previous shutdown call.By calling shutdown a partial close of a socket is requested, which is a signal that sending or receiving, or both have been discontinued.")\
X(WSAETOOMANYREFS, "Too many references. Too many references to some kernel object.")\
X(WSAETIMEDOUT, "Connection timed out. A connection attempt failed because the connected party did not properly respond after a period of time, or the established connection failed because the connected host has failed to respond.")\
X(WSAECONNREFUSED, "Connection refused. No connection could be made because the target computer actively refused it.This usually results from trying to connect to a service that is inactive on the foreign host�that is, one with no server application running.")\
X(WSAELOOP, "Cannot translate name. Cannot translate a name.")\
X(WSAENAMETOOLONG, "Name too long. A name component or a name was too long.")\
X(WSAEHOSTDOWN, "Host is down. A socket operation failed because the destination host is down.A socket operation encountered a dead host.Networking activity on the local host has not been initiated.These conditions are more likely to be indicated by the error WSAETIMEDOUT.")\
X(WSAEHOSTUNREACH, "No route to host. A socket operation was attempted to an unreachable host.See WSAENETUNREACH.")\
X(WSAENOTEMPTY, "Directory not empty. Cannot remove a directory that is not empty.")\
X(WSAEPROCLIM, "Too many processes. A Windows Sockets implementation may have a limit on the number of applications that can use it simultaneously.WSAStartup may fail with this error if the limit has been reached.")\
X(WSAEUSERS, "User quota exceeded. Ran out of user quota.")\
X(WSAEDQUOT, "Disk quota exceeded. Ran out of disk quota.")\
X(WSAESTALE, "Stale file handle reference. The file handle reference is no longer available.")\
X(WSAEREMOTE, "Item is remote. The item is not available locally.")\
X(WSASYSNOTREADY, "Network subsystem is unavailable. This error is returned by WSAStartup if the Windows Sockets implementation cannot function at this time because the underlying system it uses to provide network services is currently unavailable.Users should check : That the appropriate Windows Sockets DLL file is in the current path. That they are not trying to use more than one Windows Sockets implementation simultaneously.If there is more than one Winsock DLL on your system, be sure the first one in the path is appropriate for the network subsystem currently loaded. The Windows Sockets implementation documentation to be sure all necessary components are currently installed and configured correctly.")\
X(WSAVERNOTSUPPORTED, "Winsock.dll version out of range. The current Windows Sockets implementation does not support the Windows Sockets specification version requested by the application.Check that no old Windows Sockets DLL files are being accessed.")\
X(WSANOTINITIALISED, "Successful WSAStartup not yet performed. Either the application has not called WSAStartup or WSAStartup failed.The application may be accessing a socket that the current active task does not own(that is, trying to share a socket between tasks), or WSACleanup has been called too many times.")\
X(WSAEDISCON, "Graceful shutdown in progress. Returned by WSARecv and WSARecvFrom to indicate that the remote party has initiated a graceful shutdown sequence.")\
X(WSAENOMORE, "No more results. No more results can be returned by the WSALookupServiceNext function.")\
X(WSAECANCELLED, "Call has been canceled. A call to the WSALookupServiceEnd function was made while this call was still processing.The call has been canceled.")\
X(WSAEINVALIDPROCTABLE, "Procedure call table is invalid. The service provider procedure call table is invalid.A service provider returned a bogus procedure table to Ws2_32.dll.This is usually caused by one or more of the function pointers being NULL.")\
X(WSAEINVALIDPROVIDER, "Service provider is invalid. The requested service provider is invalid.This error is returned by the WSCGetProviderInfo and WSCGetProviderInfo32 functions if the protocol entry specified could not be found.This error is also returned if the service provider returned a version number other than 2.0.")\
X(WSAEPROVIDERFAILEDINIT, "Service provider failed to initialize. The requested service provider could not be loaded or initialized.This error is returned if either a service provider's DLL could not be loaded (LoadLibrary failed) or the provider's WSPStartup or NSPStartup function failed.")\
X(WSASYSCALLFAILURE, "System call failure. A system call that should never fail has failed.This is a generic error code, returned under various conditions. Returned when a system call that should never fail does fail.For example, if a call to WaitForMultipleEvents fails or one of the registry functions fails trying to manipulate the protocol / namespace catalogs. Returned when a provider does not return SUCCESS and does not provide an extended error code.Can indicate a service provider implementation error.")\
X(WSASERVICE_NOT_FOUND, "Service not found. No such service is known.The service cannot be found in the specified name space.")\
X(WSATYPE_NOT_FOUND, "Class type not found. The specified class was not found.")\
X(WSA_E_NO_MORE, "No more results. No more results can be returned by the WSALookupServiceNext function. ")\
X(WSA_E_CANCELLED, "Call was canceled. A call to the WSALookupServiceEnd function was made while this call was still processing.The call has been canceled.")\
X(WSAEREFUSED, "Database query was refused. A database query failed because it was actively refused.")\
X(WSAHOST_NOT_FOUND, "Host not found. No such host is known.The name is not an official host name or alias, or it cannot be found in the database(s) being queried.This error may also be returned for protocol and service queries, and means that the specified name could not be found in the relevant database.")\
X(WSATRY_AGAIN, "Nonauthoritative host not found. This is usually a temporary error during host name resolution and means that the local server did not receive a response from an authoritative server.A retry at some time later may be successful.")\
X(WSANO_RECOVERY, "This is a nonrecoverable error. This indicates that some sort of nonrecoverable error occurred during a database lookup.This may be because the database files(for example, BSD - compatible HOSTS, SERVICES, or PROTOCOLS files) could not be found, or a DNS request was returned by the server with a severe error.")\
X(WSANO_DATA, "Valid name, no data record of requested type. The requested name is valid and was found in the database, but it does not have the correct associated data being resolved for.The usual example for this is a host name - to - address translation attempt(using gethostbyname or WSAAsyncGetHostByName) which uses the DNS(Domain Name Server).An MX record is returned but no A record�indicating the host itself exists, but is not directly reachable.")\
X(WSA_QOS_RECEIVERS, "QoS receivers. At least one QoS reserve has arrived.")\
X(WSA_QOS_SENDERS, "QoS senders. At least one QoS send path has arrived.")\
X(WSA_QOS_NO_SENDERS, "No QoS senders.There are no QoS senders.")\
X(WSA_QOS_NO_RECEIVERS, "QoS no receivers. There are no QoS receivers.")\
X(WSA_QOS_REQUEST_CONFIRMED, "QoS request confirmed. The QoS reserve request has been confirmed.")\
X(WSA_QOS_ADMISSION_FAILURE, "QoS admission error. A QoS error occurred due to lack of resources.")\
X(WSA_QOS_POLICY_FAILURE, "QoS policy failure. The QoS request was rejected because the policy system couldn't allocate the requested resource within the existing policy.")\
X(WSA_QOS_BAD_STYLE, "QoS bad style. An unknown or conflicting QoS style was encountered.")\
X(WSA_QOS_BAD_OBJECT, "QoS bad object. A problem was encountered with some part of the filterspec or the provider - specific buffer in general.")\
X(WSA_QOS_TRAFFIC_CTRL_ERROR, "QoS traffic control error. An error with the underlying traffic control(TC) API as the generic QoS request was converted for local enforcement by the TC API.This could be due to an out of memory error or to an internal QoS provider error.")\
X(WSA_QOS_GENERIC_ERROR, "QoS generic error. A general QoS error.")\
X(WSA_QOS_ESERVICETYPE, "QoS service type error. An invalid or unrecognized service type was found in the QoS flowspec.")\
X(WSA_QOS_EFLOWSPEC, "QoS flowspec error. An invalid or inconsistent flowspec was found in the QOS structure.")\
X(WSA_QOS_EPROVSPECBUF, "Invalid QoS provider buffer. An invalid QoS provider - specific buffer.")\
X(WSA_QOS_EFILTERSTYLE, "Invalid QoS filter style. An invalid QoS filter style was used.")\
X(WSA_QOS_EFILTERTYPE, "Invalid QoS filter type. An invalid QoS filter type was used.")\
X(WSA_QOS_EFILTERCOUNT, "Incorrect QoS filter count. An incorrect number of QoS FILTERSPECs were specified in the FLOWDESCRIPTOR.")\
X(WSA_QOS_EOBJLENGTH, "Invalid QoS object length. An object with an invalid ObjectLength field was specified in the QoS provider - specific buffer.")\
X(WSA_QOS_EFLOWCOUNT, "Incorrect QoS flow count. An incorrect number of flow descriptors was specified in the QoS structure.")\
X(WSA_QOS_EUNKOWNPSOBJ, "Unrecognized QoS object. An unrecognized object was found in the QoS provider - specific buffer.")\
X(WSA_QOS_EPOLICYOBJ, "Invalid QoS policy object. An invalid policy object was found in the QoS provider - specific buffer.")\
X(WSA_QOS_EFLOWDESC, "Invalid QoS flow descriptor. An invalid QoS flow descriptor was found in the flow descriptor list.")\
X(WSA_QOS_EPSFLOWSPEC, "Invalid QoS provider - specific flowspec. An invalid or inconsistent flowspec was found in the QoS provider - specific buffer.")\
X(WSA_QOS_EPSFILTERSPEC, "Invalid QoS provider - specific filterspec. An invalid FILTERSPEC was found in the QoS provider - specific buffer.")\
X(WSA_QOS_ESDMODEOBJ, "Invalid QoS shape discard mode object. An invalid shape discard mode object was found in the QoS provider - specific buffer.")\
X(WSA_QOS_ESHAPERATEOBJ, "Invalid QoS shaping rate object. An invalid shaping rate object was found in the QoS provider - specific buffer.")\
X(WSA_QOS_RESERVED_PETYPE, "Reserved policy QoS element type.A reserved policy element was found in the QoS provider - specific buffer.")

#define ERROR_DEBUG()\
switch(::WSAGetLastError()){\
	ERROR_LIST(ERROR_DEBUG_CASE);\
	default:\
		Debug::Log::PrintLine(__FUNCTION__ " - Undefined error.");\
		break;\
}\
Debug::Break();\

using namespace DD;
using namespace Net;

/************************************************************************/
/* Winsock api                                                          */
/************************************************************************/
struct SocketBase::API {
	SocketStates State;
	String Port;
	String Host;
	addrinfoW Hints;
	addrinfoW * AddressInfo = 0;
	IPv4 Address;
	SOCKET Socket;

	API() : State(SocketStates::UNHANDLED), Port(6), Host(16) { ZeroMemory(&Hints, sizeof(Hints)); }
	~API() { if (AddressInfo) FreeAddrInfoW(AddressInfo); }
};

static void InitNetworking(bool forced = false) {
	static WSADATA data;
	static int result = ::WSAStartup(MAKEWORD(2, 2), &data);
	if (forced) {
		result = ::WSAStartup(MAKEWORD(2, 2), &data);
	}
	if (result != 0) {
		DEBUG_WINSOCKS("WSAStartup failed with error: ", result);
	}
}

/************************************************************************/
/* IPv4                                                                 */
/************************************************************************/
String IPv4::ToString() const {
	return String(32).AppendBatch(_bytes[2], '.', _bytes[3], '.', _bytes[4], '.', _bytes[5], ':', Port());
}

String IPv4::IP2String() const {
	return String(16).AppendBatch(_bytes[2], '.', _bytes[3], '.', _bytes[4], '.', _bytes[5]);
}

String IPv4::Port2String() const {
	return String(6).Append(Port());
}

u16 IPv4::Port() const {
	return Endian(_port);
}

u32 IPv4::IP() const {
	return Endian(_ip);
}

static IPv4 IPv4Make(addrinfoW * ptr) {
	return *((IPv4*)ptr->ai_addr->sa_data);
}

/************************************************************************/
/* NET::Socket                                                      */
/************************************************************************/
IPv4 SocketBase::GetLocalIp() {
	InitNetworking();
	static IPv4 ip;

	if (ip._ip == 0) {
		UDPEmitter sender(L"255.255.255.255", L"33333");
		UDPReceiver receiver(L"0.0.0.0", L"33333");

		int dummy = 0;
		Concurrency::ThreadPool::TaskLocal<24> t = [sender]() mutable {
			for (size_t i = 0; i < 10; ++i) {
				sender->Send(0, 0);
				Concurrency::Thread::Sleep(10);
			}
		};
		receiver->ReceiveFrom(0, dummy, ip);

		t.Wait();
	}
	return ip;
}

const IPv4 & SocketBase::IPAddress() const {
	return _api->Address;
}

SocketStates SocketBase::State() const {
	return _api->State;
}

void SocketBase::InitAddress() {
	i32 result = GetAddrInfoW(_api->Host, _api->Port, &_api->Hints, &_api->AddressInfo);
	_api->State = 0 == result ? SocketStates::VALID : SocketStates::UNHANDLED;
	if (_api->State != SocketStates::VALID) {
		ERROR_DEBUG();
		DEBUG_SOCKET("Initialize failed on ", _api->Host, ":", _api->Port, ". ", result);
		return;
	}
	_api->Address = IPv4Make(_api->AddressInfo);
}

void SocketBase::Accept(SocketBase & accepted) {
	VALIDITY_CHECK;
	union { sockaddr sender; sockaddr_in sender4; };
	int size = sizeof(sender);
	// https://msdn.microsoft.com/en-us/library/windows/desktop/ms737526(v=vs.85).aspx
	accepted._api->Socket = ::accept(_api->Socket, &sender, &size);
	accepted._api->State = accepted._api->Socket != INVALID_SOCKET ? SocketStates::VALID : SocketStates::UNHANDLED;
	accepted._api->Address = *((IPv4*)sender.sa_data);
	InetNtopW(sender4.sin_family, &sender4.sin_addr, accepted._api->Host.begin(), 16);
	accepted._api->Host.LoadFromBuffer();
	accepted._api->Port = _api->Port;

	if (accepted._api->State != SocketStates::VALID) {
		ERROR_DEBUG();
	}
}

void SocketBase::Close() {
	VALIDITY_CHECK;
	// https://msdn.microsoft.com/en-us/library/windows/desktop/ms737582(v=vs.85).aspx
	::closesocket(_api->Socket);
}

void SocketBase::Connect() {
	VALIDITY_CHECK;
	// https://msdn.microsoft.com/en-us/library/windows/desktop/ms740506(v=vs.85).aspx
	_api->Socket = ::socket(
		_api->AddressInfo->ai_family,
		_api->AddressInfo->ai_socktype,
		_api->AddressInfo->ai_protocol
	);

	_api->State = _api->Socket != INVALID_SOCKET ? SocketStates::VALID : SocketStates::UNHANDLED;

	if (_api->State != SocketStates::VALID) {
		ERROR_DEBUG();
		return;
	}

	if (_api->Host == "255.255.255.255") {
		INT bAllow = 1;
		INT err1 = setsockopt(_api->Socket, SOL_SOCKET, SO_BROADCAST, (char *)&bAllow, sizeof(bAllow));
	}

	// https://msdn.microsoft.com/en-us/library/windows/desktop/ms737625(v=vs.85).aspx
	i32 socketReport = ::connect(
		_api->Socket,
		_api->AddressInfo->ai_addr,
		(i32)_api->AddressInfo->ai_addrlen
	);

	_api->State = socketReport != SOCKET_ERROR ? SocketStates::VALID : SocketStates::UNHANDLED;
	if (_api->State != SocketStates::VALID) {
		if (::WSAGetLastError() == WSAECONNREFUSED) {
			_api->State = SocketStates::REFUSED;
			return;
		}
		ERROR_DEBUG()
	}
}

void SocketBase::Bind() {
	VALIDITY_CHECK;
	// https://msdn.microsoft.com/en-us/library/windows/desktop/ms740506(v=vs.85).aspx
	_api->Socket = ::socket(
		_api->AddressInfo->ai_family,
		_api->AddressInfo->ai_socktype,
		_api->AddressInfo->ai_protocol
	);

	_api->State = _api->Socket != SOCKET_ERROR ? SocketStates::VALID : SocketStates::UNHANDLED;

	if (!IsValid()) {
		ERROR_DEBUG();
		return;
	}

	if (_api->Host == "0.0.0.0") {
		INT bAllow = 1;
		if (_api->Hints.ai_protocol == IPPROTO_UDP) {
			INT err1 = setsockopt(_api->Socket, SOL_SOCKET, SO_REUSEADDR, (char *)&bAllow, sizeof(bAllow));
		}
	}

	// https://msdn.microsoft.com/en-us/library/windows/desktop/ms737550(v=vs.85).aspx
	_api->State = SOCKET_ERROR != ::bind(
		_api->Socket,
		_api->AddressInfo->ai_addr,
		(i32)_api->AddressInfo->ai_addrlen
	) ? SocketStates::VALID : SocketStates::UNHANDLED;

	if (_api->State != SocketStates::VALID) {
		if (::WSAGetLastError() == WSAEADDRINUSE) {
			_api->State = SocketStates::ALREADY_USED;
			return;
		}
		ERROR_DEBUG();
	}
}

void SocketBase::Listen() {
	VALIDITY_CHECK;
	// https://msdn.microsoft.com/en-us/library/windows/desktop/ms739168(v=vs.85).aspx
	_api->State = ::listen(_api->Socket, SOMAXCONN) != SOCKET_ERROR ? SocketStates::VALID : SocketStates::UNHANDLED;
	if (_api->State != SocketStates::VALID) {
		ERROR_DEBUG();
	}
}

void SocketBase::Receive(char * buffer, int & bufferLength) {
	VALIDITY_CHECK;
	// https://msdn.microsoft.com/en-us/library/windows/desktop/ms740121(v=vs.85).aspx
	bufferLength = ::recv(_api->Socket, buffer, bufferLength, 0);

	if (bufferLength == SOCKET_ERROR) {
		switch (::WSAGetLastError()) {
			case WSAECONNRESET:
				_api->State = SocketStates::RESET_BY_PEER;
				return;
			case WSAENETRESET:
				_api->State = SocketStates::NETWORK_RESET;
				return;
			default:
				_api->State = SocketStates::UNHANDLED;
				break;
		}

		ERROR_DEBUG();
	}
}

void SocketBase::ReceiveFrom(char * buffer, int & bufferLength, IPv4 & ip) {
	VALIDITY_CHECK;
	// https://msdn.microsoft.com/en-us/library/windows/desktop/ms740121(v=vs.85).aspx
	union { sockaddr sender; sockaddr_in sender4; };
	int senderlen = sizeof(sender);
	bufferLength = ::recvfrom(_api->Socket, buffer, bufferLength, 0, &sender, &senderlen);
	memcpy(&ip, sender.sa_data, 6);
	if (bufferLength == SOCKET_ERROR) {
		switch (::WSAGetLastError()) {
		case WSAECONNRESET:
			_api->State = SocketStates::RESET_BY_PEER;
			return;
		case WSAENETRESET:
			_api->State = SocketStates::NETWORK_RESET;
			return;
		default:
			_api->State = SocketStates::UNHANDLED;
			break;
		}
		ERROR_DEBUG();
	}
}

void SocketBase::Send(const char * buffer, int length) {
	VALIDITY_CHECK;
	// https://msdn.microsoft.com/en-us/library/windows/desktop/ms740149(v=vs.85).aspx
	i32 result = ::send(_api->Socket, buffer, length, 0);
	if (result == SOCKET_ERROR) {
		switch (::WSAGetLastError()) {
		case WSAECONNRESET:
			_api->State = SocketStates::RESET_BY_PEER;
			return;
		case WSAENETRESET:
			_api->State = SocketStates::NETWORK_RESET;
			return;
		default:
			_api->State = SocketStates::UNHANDLED;
			break;
		}

		ERROR_DEBUG();
	}
}

void SocketBase::Shutdown() {
	if (IsValid()) {
		// https://msdn.microsoft.com/en-us/library/windows/desktop/ms740481(v=vs.85).aspx
		i32 result = ::shutdown(_api->Socket, SD_BOTH);
		if (result == SOCKET_ERROR) {
			ERROR_DEBUG();
			Close();
		}
	}
}

SocketBase::SocketBase()
	: _api(new API)
{
	InitNetworking();
}

SocketBase::~SocketBase() {
	Shutdown();
}

void SocketBase::ReceiveSize(unsigned long & size) {
	if (::ioctlsocket(_api->Socket, FIONREAD, &size) == SOCKET_ERROR) {
		ERROR_DEBUG();
	}
}

/************************************************************************/
/* TCPClient                                                            */
/************************************************************************/
TCPClientBase::TCPClientBase(const IPv4 & ip)
	: TCPClientBase(ip.IP2String(), ip.Port2String())
{}

TCPClientBase::TCPClientBase(const String & host, const String & port)
	: SocketBase()
{
	_api->Host = host;
	_api->Port = port;
	_api->Hints.ai_family = AF_INET;
	_api->Hints.ai_socktype = SOCK_STREAM;
	_api->Hints.ai_protocol = IPPROTO_TCP;
	_api->Hints.ai_flags = AI_PASSIVE;
	InitAddress();
	Connect();
}

void TCPClientBase::StartReceiving() {
	_worker = [this]() mutable {
		constexpr int SIZE = 1 << 16;
		Array<u8, SIZE> data;
		while (IsValid()) {
			int size = SIZE;
			Receive((char *)data.begin(), size);
			data.ResizeUnsafe(size);
			OnMessageReceived(data);
		}
	};
}


/************************************************************************/
/* TCPServer                                                            */
/************************************************************************/
TCPServerBase::TCPServerBase(Text::StringView16 host, Text::StringView16 port)
	: SocketBase()
{
	_api->Host = host;
	_api->Port = port;
	_api->Hints.ai_family = AF_INET;
	_api->Hints.ai_socktype = SOCK_STREAM;
	_api->Hints.ai_protocol = IPPROTO_TCP;
	_api->Hints.ai_flags = 0;
	InitAddress();
	Bind();
	if (IsValid())
		Listen();
}

void TCPServerBase::StartAccepting() {
	_worker = [this]() mutable {
		while (_api->State == SocketStates::VALID) {
			TCPClient client;
			client.Catch(new TCPClientBase);
			Accept(reinterpret_cast<SocketBase&>(*client));
			Clients->Add(client);
			OnClientAccepted(client);
		}
	};
}

/************************************************************************/
/* UDEmitter                                                            */
/************************************************************************/
UDPEmitterBase::UDPEmitterBase(Text::StringView16 host, Text::StringView16 port)
	: SocketBase()
{
	_api->Host = host;
	_api->Port = port;
	_api->Hints.ai_family = AF_INET;
	_api->Hints.ai_socktype = SOCK_DGRAM;
	_api->Hints.ai_protocol = IPPROTO_UDP;
	_api->Hints.ai_flags = 0;
	InitAddress();
	Connect();
}

/************************************************************************/
/* UDPReceiver                                                          */
/************************************************************************/
void UDPReceiverBase::StartReceiving() {
	static constexpr size_t SIZE = 1 << 16;
	_worker = [this]() mutable {
		Array<u8, SIZE> data;
		IPv4 ip;
		while (_api->State == SocketStates::VALID) {
			int size = (int)SIZE;
			ReceiveFrom((char*)data.begin(), size, ip);
			data.ResizeUnsafe(size);
			OnMessageReceived(data, ip);
		}
	};
}

UDPReceiverBase::UDPReceiverBase(Text::StringView16 host, Text::StringView16 port)
	: SocketBase()
{
	_api->Host = host;
	_api->Port = port;
	_api->Hints.ai_family = AF_INET;
	_api->Hints.ai_socktype = SOCK_DGRAM;
	_api->Hints.ai_protocol = IPPROTO_UDP;
	_api->Hints.ai_flags = AI_PASSIVE;
	InitAddress();
	Bind();
}