#include <DD/Gpu/ComputeShader.h>
#include <DD/Gpu/Devices.h>
#include <d3d11.h>

using namespace DD;
using namespace DD::Gpu;


void ComputeShader::Load(Device & device, const Text::StringView16 & name) {
	_device = device;
	_device->ComputeShaderLoad(*this, name);
}


void ComputeShader::Reload(Device & device) {
	this->~ComputeShader();
	Load(device, _name);
}


void ComputeShader::Run(u32 x, u32 y, u32 z) {
	_device->ComputeShaderSet(*this);
	_device->ComputeShaderDispatch(x, y, z);
}

