#include <DD/Concurrency/Awaitable.h>
#include <windows.h>

using namespace DD;
using namespace DD::Concurrency;

void AwaitableBase::Sleep(u32 ms) {
	::Sleep(ms);
}
