#include <DD/Input/Clipboard.h>
#include <DD/Input/KeyboardConsole.h>
#include <DD/Input/Line.h>
#include <DD/Input/LineConsole.h>

#include <Windows.h>

using namespace DD;
using namespace DD::Input;

#pragma region KeyboardConsole
/** Lock for global memory. */
template<typename CAST_TYPE>
struct GlobLock {
	/** Handle to be locked. */
	HGLOBAL Handle;
	/** Unlocked char. */
	CAST_TYPE * Text;

	/** https://learn.microsoft.com/en-us/windows/win32/api/winbase/nf-winbase-globallock */
	GlobLock(HGLOBAL handle) : Handle(handle), Text(static_cast<CAST_TYPE *>(GlobalLock(handle))) {}
	/** https://learn.microsoft.com/en-us/windows/win32/api/winbase/nf-winbase-globalunlock */
	~GlobLock() { if (Handle) GlobalUnlock(Handle); }
};


/** Scope allocated global memory. */
struct GlobMem {
	/** Pointer to global memory. */
	HGLOBAL Handle = nullptr;

	/** Constructor. */
	GlobMem(SIZE_T size) : Handle(GlobalAlloc(GMEM_FIXED, size)) {}
	/** Destructor. */
	~GlobMem() { if (Handle) GlobalFree(Handle); }
};


/** Try to open clipboard for a scope, check @see Ready property, if opening was successfull.*/
struct ClipLock {
	/** Was clipboard opening successfully. */
	BOOL Ready = false;
	/** Handle to clipboard data.  */
	HANDLE Handle = nullptr;

	/**
	 * Constructor.
	 * https://learn.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-openclipboard
	 * https://learn.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-getclipboarddata
	 */
	ClipLock(UINT datatype = CF_UNICODETEXT) : Ready(OpenClipboard(nullptr)) { if (Ready) Handle = GetClipboardData(datatype); }
	/** https://learn.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-closeclipboard.*/
	~ClipLock() { if (Ready) CloseClipboard(); }
};


String Clipboard::Get(u32 n) {
	ClipLock clip;
	if (!clip.Ready)
		return String();

	GlobLock<wchar_t> datalock(clip.Handle);
	return String(datalock.Text);
}


bool Clipboard::Set(const Text::StringView16 & text) {
	ClipLock clip;
	if (!clip.Ready)
		return false;

	GlobMem memory ((text.Length() + 1) * sizeof(wchar_t));
	if (memory.Handle == nullptr)
		return false;

	GlobLock<void *> datalock(memory.Handle);
	if (datalock.Text == nullptr)
		return false;

	memcpy(datalock.Text, (const void *)text.begin(), text.Length() * sizeof(wchar_t));
	// https://learn.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-emptyclipboard
	EmptyClipboard();
	// https://learn.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-setclipboarddata
	if (SetClipboardData(CF_UNICODETEXT, memory.Handle) == nullptr)
		return false;

	// when SetClipboardData succeeds it takes care of deallocation
	memory.Handle = NULL;

	return true;
}
#pragma endregion


#pragma region KeyboardConsole
void KeyboardConsole::Update(Key key, State state) {
	// if state has been changed
	if (state != States[(size_t)key]) {
		switch (States[(size_t)key] = state) {
		case State::Released: OnKeyReleased(*this, key); break;
		case State::Pressed:  OnKeyPressed(*this, key);  break;
		default: throw;
		}
	}

	// Update whatever came
	OnKeyUpdate(*this, key);

	// if letter key, update char
	char letter = Transcript[key].Chars[!UpperLetter()];
	if (letter && state == State::Pressed && !Ctrl())
		OnLetter(*this, letter);
}


KeyboardConsole::KeyboardConsole() {
	Transcript[Key::Num0] = { '0', '0' };
	Transcript[Key::Num1] = { '1', '1' };
	Transcript[Key::Num2] = { '2', '2' };
	Transcript[Key::Num3] = { '3', '3' };
	Transcript[Key::Num4] = { '4', '4' };
	Transcript[Key::Num5] = { '5', '5' };
	Transcript[Key::Num6] = { '6', '6' };
	Transcript[Key::Num7] = { '7', '7' };
	Transcript[Key::Num8] = { '8', '8' };
	Transcript[Key::Num9] = { '9', '9' };
	Transcript[Key::NumPoint] = { '.', '.' };
	Transcript[Key::NumSlash] = { '/', '/' };
	Transcript[Key::NumStar] = { '*', '*' };
	Transcript[Key::NumPlus] = { '+', '+' };
	Transcript[Key::NumMinus] = { '-', '-' };
	Transcript[Key::Space] = { ' ', ' ' };
	Transcript[Key::Tab] = { '\t' };

	Transcript[Key::TK0] = { ')', '0' };
	Transcript[Key::TK1] = { '!', '1' };
	Transcript[Key::TK2] = { '@', '2' };
	Transcript[Key::TK3] = { '#', '3' };
	Transcript[Key::TK4] = { '$', '4' };
	Transcript[Key::TK5] = { '%', '5' };
	Transcript[Key::TK6] = { '^', '6' };
	Transcript[Key::TK7] = { '&', '7' };
	Transcript[Key::TK8] = { '*', '8' };
	Transcript[Key::TK9] = { '(', '9' };
	Transcript[Key::A] = { 'A', 'a' };
	Transcript[Key::B] = { 'B', 'b' };
	Transcript[Key::C] = { 'C', 'c' };
	Transcript[Key::D] = { 'D', 'd' };
	Transcript[Key::E] = { 'E', 'e' };
	Transcript[Key::F] = { 'F', 'f' };
	Transcript[Key::G] = { 'G', 'g' };
	Transcript[Key::H] = { 'H', 'h' };
	Transcript[Key::I] = { 'I', 'i' };
	Transcript[Key::J] = { 'J', 'j' };
	Transcript[Key::K] = { 'K', 'k' };
	Transcript[Key::L] = { 'L', 'l' };
	Transcript[Key::M] = { 'M', 'm' };
	Transcript[Key::N] = { 'N', 'n' };
	Transcript[Key::O] = { 'O', 'o' };
	Transcript[Key::P] = { 'P', 'p' };
	Transcript[Key::Q] = { 'Q', 'q' };
	Transcript[Key::R] = { 'R', 'r' };
	Transcript[Key::S] = { 'S', 's' };
	Transcript[Key::T] = { 'T', 't' };
	Transcript[Key::U] = { 'U', 'u' };
	Transcript[Key::V] = { 'V', 'v' };
	Transcript[Key::W] = { 'W', 'w' };
	Transcript[Key::X] = { 'X', 'x' };
	Transcript[Key::Y] = { 'Y', 'y' };
	Transcript[Key::Z] = { 'Z', 'z' };
	Transcript[Key::Comma] = { '<', ',' };
	Transcript[Key::Dot] = { '>', '.' };
	Transcript[Key::BackSlash] = { '|', '\\' };
	Transcript[Key::Plus] = { '+', '=' };
	Transcript[Key::Minus] = { '_', '-' };
	Transcript[Key::Slash] = { '?', '/' };
	Transcript[Key::Semicolon] = { ':', ';' };
	Transcript[Key::Quote] = { '"', '\'' };
	Transcript[Key::LeftBracket] = { '{', '[' };
	Transcript[Key::RightBracket] = { '}', ']' };
	Transcript[Key::Wave] = { '~', '`' };
}
#pragma endregion


#pragma region KeyboardConsole

Text::StringView16 Line::Copy() const {
	if (!IsSelectionValid())
		return nullptr;

	Math::Interval1S i = Math::Interval1S::From(Selection);
	return Text.SubView(i.Min[0], i.Size()[0]);
}

void Line::Set(const Text::StringView16 & value, CursorType cursor) {
	Text = value;
	Cursor = cursor == INVALID ? Text.Length() : cursor;
	Selection = INVALID;
}


void Line::BackspaceCursor() {
	if (Cursor != 0) {
		Text.RemoveChar(Cursor - 1);
		MoveCursor(Cursor - 1, false);
	}
}


void Line::DeleteSelection() {
	if (Selection.y < Selection.x)
		Swap(Selection.x, Selection.y);

	for (CursorType c = Selection.x; c < Selection.y; ++c)
		Text.RemoveChar(Selection.x);

	MoveCursor(Selection.x, false);
}


void Line::MoveCursor(CursorType whereTo, bool shift) {
	if (whereTo == INVALID) {
		Cursor = Math::Min(Cursor, Text.Length());
		return;
	}

	if (Selection.x == INVALID && shift)
		Selection.x = Cursor;

	Cursor = Math::Min(whereTo, Text.Length());

	if (shift)
		Selection.y = Cursor;
	else
		Selection = INVALID;
}


void Line::SelectText(Text::StringView16 selectedText) {
	// nothing to select
	if (Text.Length() == 0)
		return;

	if (selectedText == nullptr)
		return;

	Selection.x = selectedText.begin() - Text.begin();
	Selection.y = Selection.x + selectedText.Length();
	Cursor = Selection.x;
}


void Line::NextWord(bool backward) {
	if (!backward && IsCursorNaive())
		return;
	// nothing to select
	if (Text.Length() == 0)
		return;

	if (Selection != INVALID)
		Cursor = backward ? Math::Min(Selection.x, Selection.y) : Math::Max(Selection.x, Selection.y);

	Text::StringReader16 reader(Text, Cursor);
	// firstly skip, then read - otherwise if cursor is inside a word, it selects just part of it and not entire word
	Text::StringView16 nextWord = backward ? reader.RSkipNextNonWS().ReadNextNonWS() : reader.SkipNextNonWS().RReadNextNonWS();
	SelectText(nextWord);
	Cursor = Selection.x;
}
#pragma endregion


#pragma region LineConsole
void LineConsole::commit() {
	History.Append('\n').Append(Line.Text);
	OnCommand(Line.Text);
	Text::StringReader16 reader{ History };
	History = reader.SeekEnd().RSkipLine(5).CursorEx();
	CommandHistory.Append(Line.Text);

	Line.Clear();
}


void LineConsole::fillHint(Key key) {
	size_t hintIndex = (size_t)key - (size_t)Key::Num1;
	if (Hints.Size() <= hintIndex)
		return;

	// todo - do not replace whole line, just its end
	Line.Set(Hints.First(hintIndex), 0);
	Line.NextWord();
	Line.NextWord();
}


void LineConsole::listHistory(bool up) {
	if (up) {
		if (historyIndex == INVALID)
			historyIndex = CommandHistory.Size();
		else
			--historyIndex;

		if (Line.Text.Length())
			CommandHistory.Append(Line.Text);

		if (historyIndex != 0)
			--historyIndex;
	}
	else {
		++historyIndex;
	}

	if (historyIndex < CommandHistory.Size())
		Line.Set(CommandHistory[historyIndex]);
}


void LineConsole::onNavigationUpdate(Key key, bool ctrl, bool shift) {
	Concurrency::CriticalSection::Scope scope(SectionLock);

	switch (key) {
	case Key::Esc:   Line.Clear(); break;
	case Key::Up:    listHistory(true); break;
	case Key::Down:  listHistory(false); break;
	case Key::Tab:   Line.IsCursorNaive() && !shift ? fillHint() : Line.NextWord(shift); break;
	case Key::Left:  Line.PressLeft(shift);			break;
	case Key::Right: Line.PressRight(shift);			break;
	case Key::Home:  Line.PressHome(shift);							break;
	case Key::End:   Line.PressEnd(shift); break;
	case Key::Enter: /*if (KeyboardConsole::Instance().Ctrl())*/ commit(); break;
	case Key::Backspace: Line.PressBackSpace(); break;
	case Key::Delete:    Line.PressDelete(); break;
	case Key::Insert: if (shift) Line.Paste(Clipboard::Instance().Get()); break;
	case Key::V: if (ctrl) Line.Paste(Clipboard::Instance().Get()); break;
	case Key::C: if (ctrl) Clipboard::Instance().Set(Line.Copy()); break;
	case Key::X: if (ctrl) { Clipboard::Instance().Set(Line.Copy()); Line.PressDelete(); } break;
	case Key::A: if (ctrl) Line.SelectAll();
	case Key::Num1:
	case Key::Num2:
	case Key::Num3:
	case Key::Num4:
	case Key::Num5:
		// do nothing if ctrl isn't pressed
		if (!ctrl)
			return;

		fillHint(key);
		break;
		// if nothing happened, skip LineUpdate event
	default: return;
	}

	OnLineUpdate(Line);
}


void LineConsole::onLetterUpdate(char letter) {
	Concurrency::CriticalSection::Scope scope(SectionLock);
	switch ((Text::Ascii)letter) {
		// ignored keys
	case Text::Ascii::HorizontalTab:
	case Text::Ascii::LineFeed:
		return;

	default:
		Line.PressLetter(letter);
		break;
	}
	OnLineUpdate(Line);
}


void LineConsole::Activate() {
	Concurrency::CriticalSection::Scope scope(SectionLock);
	OnLineUpdate(Line);

	if (!KeyboardConsole::Instance().OnLetter->Contains(_letterUpdateDelegate))
		KeyboardConsole::Instance().OnLetter += _letterUpdateDelegate;

	if(!KeyboardConsole::Instance().OnKeyPressed->Contains(_navigationUpdateDelegate))
		KeyboardConsole::Instance().OnKeyPressed += _navigationUpdateDelegate;

	_active = true;
}


void LineConsole::Deactivate() {
	Concurrency::CriticalSection::Scope scope(SectionLock);
	KeyboardConsole::Instance().OnLetter -= _letterUpdateDelegate;
	KeyboardConsole::Instance().OnKeyPressed -= _navigationUpdateDelegate;
	_active = false;
}


LineConsole::LineConsole()
	: _letterUpdateDelegate([this](const KeyboardConsole &, char letter) mutable { onLetterUpdate(letter); })
	, _navigationUpdateDelegate([this](const KeyboardConsole & console, Key key) mutable { onNavigationUpdate(key, console.Ctrl(), console.Shift()); })
{
}
#pragma endregion