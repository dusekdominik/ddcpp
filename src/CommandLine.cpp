#include <DD/CommandLine.h>
#include <DD/Sequencer/Driver.h>
#include <DD/Text/StringReader.h>

using namespace DD;
using namespace DD::CommandLine;


Program * Program::LastInstance(Program * program) {
	// storage for the reference
	static Program * instance = nullptr;
	// if reference is provided, override value
	if (program)
		instance = program;

	// return stored value
	return instance;
}


void Program::Help(::DD::String & help) const {
	const char indent[] = "    ";
	// print general help of the program
	help.AppendBatch("Program:\n", indent, _name, "\n");
	help.AppendBatch("Brief:\n", indent, _brief, "\n");
	help.AppendBatch("Commands:\n");

	// for each command
	for (const Command * cmd : _commands) {
		// print brief description "name - arg0 arg1 ..."
		help.Append(indent);
		bool keyword = true;
		for (const IArgument * arg : cmd->Args()) {
			help.AppendBatch(arg->Descriptor().Name, keyword ? '\t' : ' ');
			keyword = false;
		}
		help.Append("\n");

		// command is not parsed properly, print detail
		if (!cmd->IsReady() && cmd->KeywordFound()) {
			help.AppendBatch(cmd->Brief(), "\n");
			for (const IArgument * arg : cmd->Args()) {
				help.AppendBatch(indent, indent, arg->Descriptor().Name, indent, arg->Descriptor().Brief, '\n');
			}
		}
	}

	if (_unparsed.Size()) {
		help.AppendBatch('\n');
		for (const char * arg : _unparsed) {
			help.AppendBatch("WARNING - unparsed arg: ", arg, '\n');
		}
	}
}


bool Program::TryParse(size_t argc, const char ** argv, Text::LogVirtual8 & log) {
	// circulate all args
	for (size_t argIndex = 0; argIndex < argc; ++argIndex) {
		// number of args parsed in the pass
		size_t parsedArgs = 0;

		// against all commands
		for (size_t cmdIndex = 0; cmdIndex < _commands.Size(); ++cmdIndex) {
			// get command
			Command * command = _commands[cmdIndex];
			// skip all already parsed commands
			if (*command) {
				continue;
			}
			// if command has been parsed, update index, stop inner loop
			parsedArgs = command->TryParse(argc - argIndex, argv + argIndex);
			if (parsedArgs) {
				argIndex += parsedArgs - 1;
				break;
			}
			// if command was not recognized, reset its state
			else {
				command->Reset();
			}
		}

		// if no command accepted command, report as unparsed
		if (parsedArgs == 0)
			_unparsed.Add(argv[argIndex]);
	}

	// Run
	bool anyRequest = false;
	for (RequestHandler * handler : _requestHandlers)
		if (handler->TryCall(log))
			anyRequest = true;

	// signal possible problems
	// I   arguments must be provided
	// II  all arguments must be parsed
	// III all must be parsed correctly or unparsed
	// IV  a request must called
	return
		argc &&
		_unparsed.Size() == 0 &&
		!Sequencer::Drive(_commands).Filter(&Command::Invalid).Any() &&
		anyRequest;
}


bool Program::TryParse(Text::StringView16 cmd, Text::LogVirtual8 & log) {
	DD::Text::StringReader16 reader(cmd);

	DD::List<char, 1024> buffer;
	DD::List<const char *, 16> words;
	while (reader.Any()) {
		DD::Text::StringView16 word = reader.ReadNextNonWS();
		words.Append(buffer.begin() + buffer.Size());
		for (wchar_t c : word)
			buffer.Append((char)c);
		buffer.Append(0);
	}

	return TryParse(words.Size(), words.begin(), log);
}


void Program::Reset() {
	// each command block should be reseted
	for (Command * cmd : _commands) {
		cmd->Reset();
	}

	_unparsed.Clear();
}


::DD::String Program::Line() const {
	::DD::String result;
	for (Command * c : _commands) {
		if (c->IsReady()) {
			for (const IArgument * const a : c->Args()) {
				result.AppendEx(' ').AppendEx(a->Value());
			}
		}
	}

	return result;
}

void Program::Deserialize(Text::StringView8 source) {
	auto set = [this](Text::StringView8 command, Text::StringView8 argument, Text::StringView8 value) {
		for (Command * cmd : _commands) {
			if (cmd->Name() == command) {
				for (IArgument * arg : cmd->_arguments) {
					if (arg->Descriptor().Name == argument) {
						arg->TryParse(value);
						return true;
					}
				}

				return false;
			}
		}

		return false;
	};

	Text::StringReader8 reader(source);
	while (reader.Any()) {
		Text::StringView8 command = reader.ReadNextNonWS();
		Text::StringView8 argument = reader.ReadNextNonWS();
		Text::StringView8 value = reader.Skip().ReadLine();

		set(command, argument, value);
	}
}

void Program::Serialize(Text::StringWriter8 & target) const {
	for (const Command * cmd : Commands())
		for (const IArgument * arg : cmd->Args())
			if (arg->Value() != arg->Descriptor().DefaultValue)
				target.WriteTN(cmd->Name(), arg->Descriptor().Name, arg->Value());
}


/************************************************************************/
/* Command                                                              */
/************************************************************************/
void Command::Reset() {
	// each argument of command must be reset
	for (IArgument * arg : _arguments) {
		arg->Reset();
		break;
	}
}


size_t Command::TryParse(size_t argc, const char ** argv) {
	// check if the first argument is the keyword
	if (argc == 0)
		return 0;

	// if keyword is found and argument fails, print help
	for (IArgument * argument : _arguments) {
		// if we are out of args, return fail
		if (argc-- == 0)
			return 0;
		// argument fails
		if (!argument->TryParse(*argv++)) {
			return 0;
		}
	}

	return _arguments.Size();
}


Command::Command(
	Text::StringView8 name,
	Text::StringView8 brief,
	Program * manager /*= Instance()*/
) : _name(name)
, _brief(brief) {
	// register command in the CommandLine object
	if (manager == nullptr)
		manager = Program::LastInstance();

	if (manager)
		manager->Register(this);

	// delayed init
	new (_keywordData) Keyword(name, "keyword", this);
}


/************************************************************************/
/* Argument                                                             */
/************************************************************************/
Command * IArgument::CommandCast(Command * command) { return command ? command : Program::LastInstance()->LastCommand(); }

IArgument::IArgument(ArgumentDescriptor && descriptor, Command * command/* = nullptr*/)
	: _descriptor(Fwd(descriptor)) {
	_value = _descriptor.DefaultValue;
	Command * targetCommand = CommandCast(command);
	targetCommand->_arguments.Add(this);
}


/************************************************************************/
/* RequestHandler                                                       */
/************************************************************************/
bool RequestHandler::Valid() const {
	switch (_behavior) {
	case Behaviors::NoCommandValid:
		return !Sequencer::Drive(_commands).Any(&Command::Valid);
	case Behaviors::AnyCommandValid:
		return Sequencer::Drive(_commands).Any(&Command::Valid);
	case Behaviors::AllCommandsValid:
		return Sequencer::Drive(_commands).All(&Command::Valid);
	case Behaviors::SingleCommandValid:
		return Sequencer::Drive(_commands).AtMost(&Command::Valid, 1);
	case Behaviors::NotSingleCommandValid:
		return Sequencer::Drive(_commands).Filter(&Command::Valid).Count();
	default:
		throw;
	}
}


bool RequestHandler::TryCall(Text::LogVirtual8 & log) {
	if (Valid()) {
		bool result = _callback(log);

		log.Write("Processing");
		for (const Command * cmd : _commands)
			log.Write(" ", cmd->Name());
			log.Critical(result ? "[SUCCEEDED]" : "[FAILED]");


		return result;
	}

	return false;
}


RequestHandler::RequestHandler(callback_t callback, Behaviors behavior, Program * program)
	: _callback(callback)
	, _behavior(behavior) {
	program->Register(this);
}
