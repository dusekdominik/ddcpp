#include <DD/String.h>
#include <DD/Debug.h>
#include <DD/Math.h>
#include <DD/List.h>
#include <DD/Text/Caster.h>
#include <string>
#include <Windows.h>

using namespace DD;

DD_TODO_STAMP("Stringize vectors (could be used in dbg log).")
DD_TODO_STAMP("Stringize matrices (could be used in dbg log).")
DD_TODO_STAMP("Append batch should have precomputed value.")
DD_TODO_STAMP("Create StringView, an object operating over const data but enabling offset manipulation (splits, trims etc...)")

/************************************************************************/
/* Helper classes                                                       */
/************************************************************************/
#pragma region helper classes
inline static size_t StringLength(const String & string) { return string.Length(); }
inline static size_t StringLength(const wchar_t * string) { return wcslen(string); }
inline static size_t StringLength(const char * string) { return strlen(string); }

struct StringFormat {
	int Alignment, Index;
	bool AlignmentLeft, Maximal;
	String Format;
	StringFormat & operator()(const String & fs) {
		Maximal = false;
		AlignmentLeft = false;
		Alignment = 0;
		Index = 0;

		enum { INDEX, ALIGNMENT, FORMAT } state = INDEX;

		for (size_t i = 0, m = fs.Length(); i < m; ++i) {
			wchar_t c = fs[i];
			switch (c) {
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
					switch (state) {
						case INDEX: (Index *= 10) += c - '0'; break;
						case ALIGNMENT: (Alignment *= 10) += c - '0'; break;
					}
					break;
				case '!': if (state == ALIGNMENT) Maximal = true; break;
				case '-': if (state == ALIGNMENT) AlignmentLeft = true; break;
				case ':': state = ALIGNMENT; break;
				case ',':
					state = FORMAT;
					wchar_t * begin = (wchar_t *)fs.begin() + (i + 1);
					wchar_t * end = (wchar_t *)fs.end();
					Format.Wrap({begin, end});
					Format.LoadFullBuffer();
					Format.Trim();
					return *this;
			}
		}

		return *this;
	}
};

struct IntegerFormat {
	/** Digit buffer. */
	wchar_t Digits[20];
	/** Number of digits. */
	char Length;
	/** True if minus should be shown. */
	bool Sign;
	/** Zero constructor. */
	IntegerFormat()
		: Length(0)
		, Sign(false)
	{}
	/** Signed integer. */
	template<typename T>
	static IntegerFormat MakeIntegerFormat(SignedT<T> i)
	{
		IntegerFormat f;
		// check minus sign
		if (i < 0) {
			f.Sign = true;
			i = -i;
		}
		// special case for INT_MAX values
		if (i < 0) {
			f.Digits[f.Length++] = L'0' + ((-(i + 10)) % 10);
			i /= 10;
			i = -i;
		}
		// fill numbers
		do {
			f.Digits[f.Length++] = L'0' + (i % 10);
		} while (i /= 10);

		return f;
	}
	/** Unsigned integer number. */
	template<typename T>
	static IntegerFormat MakeIntegerFormat(UnsignedT<T> i)
	{
		IntegerFormat f;
		// fill numbers
		do {
			f.Digits[f.Length++] = L'0' + (i % 10);
		} while (i /= 10);

		return f;
	}
};


template<typename STRING>
inline static void Format(const STRING & formatter, String & target, const Array<const void *> & refs, const Array<const String::IFormatter *> & formatters) {
	// locally allocated storage for partial results
	Array<StringLocal<64>, 32> pieces;
	Array<StringFormat, 32> formats;
	// last char was escape char
	bool escape = false;
	// loading escape sequence
	bool symbol = false;
	// loaded sequence
	StringLocal<32> symbolSequence;

	size_t charCount = 2;
	size_t refCount = 0;
	auto iterator = &(formatter[0]);
	// compute size
	for (wchar_t c = (wchar_t)*iterator; c; c = (wchar_t)*(++iterator)) {
		switch (c) {
			// open sequence char
			case '{':
				if (escape) ++charCount;
				else symbol = true;
				escape = false;
				break;
				// close sequence char
			case '}':
				if (symbol) {
					symbol = false;
					StringFormat & format = formats[refCount](symbolSequence);
					formatters[format.Index]->Format(pieces[refCount], refs[format.Index], format.Format);
					charCount += format.Maximal ? format.Alignment : Math::Max(format.Alignment, (int)pieces[refCount].Length());
					symbolSequence.Clear();
					++refCount;
				}
				else {
					++charCount;
				}
				break;
				// escape sequence char
			case '\\':
				if (escape) ++charCount, escape = false;
				else escape = true;
				break;
				// non-special chars
			default:
				if (symbol) {
					symbolSequence.Append(c);
				}
				else {
					if (escape) escape = false;
					++charCount;
				}
				break;
		}
	}

	target.Reserve(charCount);

	refCount = 0;
	iterator = &(formatter[0]);
	// fill string
	for (wchar_t c = (wchar_t)*iterator; c; c = (wchar_t)*(++iterator)) {
		switch (c) {
			// open sequence char
			case '{':
				if (escape) target.Append('{');
				else symbol = true;
				escape = false;
				break;
				// close sequence char
			case '}':
				if (symbol) {
					symbol = false;
					StringFormat & format = formats[refCount];
					if (format.Alignment) {
						int fill = Math::Max(0, format.Alignment - (int)pieces[refCount].Length());
						if (format.Maximal) {
							if (format.AlignmentLeft)
								target.AppendFill(fill).Append(pieces[refCount]);
							else
								target.Append(pieces[refCount]).AppendFill(fill);
						}
						else {
							if (format.AlignmentLeft)
								target.AppendFill(fill).Append(pieces[refCount]);
							else
								target.Append(pieces[refCount]).AppendFill(fill);
						}
					}
					else {
						target.Append(pieces[refCount]);
					}
					++refCount;
					symbolSequence.Clear();
				}
				else {
					target.Append('}');
				}
				break;
				// escape sequence char
			case '\\':
				if (escape) target.Append(c), escape = false;
				else escape = true;
				break;
				// non-special chars
			default:
				if (symbol) {
					symbolSequence.Append(c);
				}
				else {
					if (escape) target.Append('\\'), escape = false;
					target.Append(c);
				}
				break;
		}
	}
}

template<typename INT>
inline static void FormatIntegerType(const INT & i, String & target, const Text::StringView16 & format) {
	wchar_t buffer[64];
	wchar_t * writer = buffer;
	char leadingZeros = 0;
	bool triples = false;

	IntegerFormat input = IntegerFormat::MakeIntegerFormat<INT>(i);
	char allocation = input.Sign + input.Length;
	target.Reserve(allocation + 1);

	if (format.Length()) {
		for (const wchar_t & f : format) {
			// leading zeros
			switch (f) {
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
					leadingZeros = f - '0';
					break;
					// format divided into triples
				case 'N':
					triples = true;
					break;
			}
		}
	}

	// push leading zeros into parser
	if (leadingZeros && leadingZeros > input.Length) {
		for (int i = 0, m = leadingZeros - input.Length; i < m; ++i)
			input.Digits[input.Length++] = '0';
	}

	// signum of number
	if (input.Sign)
		*(writer++) = L'-';

	// triple format
	if (triples) {
		while (1) {
			// last triple
			if (input.Length <= 3) {
				while (input.Length)
					*(writer++) = input.Digits[--input.Length];

				break;
			}
			// not last triple
			else {
				switch (input.Length % 3) {
					case 0: *(writer++) = input.Digits[--input.Length];
					case 2: *(writer++) = input.Digits[--input.Length];
					case 1: *(writer++) = input.Digits[--input.Length];
					default: *(writer++) = ' ';
				}
			}
		}
	}
	// native format
	else {
		while (input.Length--)
			*(writer++) = input.Digits[input.Length];
	}

	target.Append(Text::StringView(buffer, writer - buffer));
}

template<typename C, typename S>
inline static void FormatStringType(
	const Text::StringViewT<C, S> & s,
	String & target,
	const Text::StringView16 & format
) {
	typedef wchar_t(*functor_t)(wchar_t);
	enum { NONE, LOWER = 'L', UPPER = 'U' };
	static auto nativeTranscriptor = [](wchar_t c) {return c; };
	static auto lowerTranscriptor = [](wchar_t c) {return (wchar_t)tolower(c); };
	static auto upperTranscriptor = [](wchar_t c) {return (wchar_t)toupper(c); };
	static auto selectTranscriptor = [&](int method) -> functor_t {
		switch (method) {
			case NONE:  return nativeTranscriptor;
			case LOWER: return lowerTranscriptor;
			case UPPER: return upperTranscriptor;
		}
		throw;
	};

	char spacing = 0;
	char method = NONE;

	for (wchar_t c : format) {
		switch (c) {
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				spacing = c - '0';
				break;
			case 'L':
				method = LOWER;
				break;
			case 'U':
				method = UPPER;
				break;
		}
	}

	size_t length = s.Length();
	if (length) {
		size_t allocation = 1 + length + (length - 1) * spacing;
		auto transcriptor = selectTranscriptor(method);
		target.Reserve(allocation).AppendUnsafe(transcriptor(s[0]));
		for (size_t i = 1; i < length; ++i) {
			for (int i = 0; i < spacing; ++i) {
				target.AppendUnsafe(' ');
			}
			target.AppendUnsafe(transcriptor(s[i]));
		}
		*target.end() = 0;
	}
}
#pragma endregion

/************************************************************************/
/* String - accession                                                   */
/************************************************************************/
#pragma region accession
wchar_t * String::begin() {
	return StringViewT::_begin;
}

wchar_t * String::end() {
	return StringViewT::_begin + StringViewT::_length;
}

wchar_t & String::operator[](size_t i) {
	return StringViewT::_begin[i];
}

const wchar_t * String::begin() const {
	return StringViewT::_begin;
}

const wchar_t * String::end() const {
	return StringViewT::_begin + StringViewT::_length;
}

const wchar_t & String::operator[](size_t i) const {
	return StringViewT::_begin[i];
}

#pragma endregion

/************************************************************************/
/* String - loading                                                     */
/************************************************************************/
#pragma region load
void String::LoadFromBuffer() {
	StringViewT::_begin = _data.begin();
	StringViewT::_length = Math::Min(wcslen(begin()));
}

void String::LoadFullBuffer() {
	StringViewT::_begin = _data.begin();
	StringViewT::_length = _data.Length();
}
#pragma endregion

/************************************************************************/
/* String - prepending                                                  */
/************************************************************************/
#pragma region prepend

#pragma endregion

/************************************************************************/
/* String - appending                                                   */
/************************************************************************/
#pragma region append
String & String::appendFormat(const Text::StringView16 & formatter, const Array<const void *> & refs, const Array<const IFormatter *> & formatters) {
	::Format(formatter, *this, refs, formatters);
	return *this;
}

String & String::appendFormat(const char * formatter, const Array<const void*> & refs, const Array<const IFormatter*> & formatters) {
	::Format(formatter, *this, refs, formatters);
	return *this;
}

String & String::appendFormat(const wchar_t * formatter, const Array<const void*> & refs, const Array<const IFormatter*> & formatters) {
	::Format(formatter, *this, refs, formatters);
	return *this;
}

String & String::AppendCol(const String & str) {
	// reserve
	Reserve(str.Length() + 1);

	// move to back
	size_t bufferSize = _data.Length();
	size_t stringSize = Length();
	for (size_t i = 1; i <= stringSize; ++i) {
		_data[bufferSize - i] = _data[stringSize - i];
	}

	// merge
	wchar_t * obegin = _data.begin();
	const wchar_t * lbegin = _data.begin() + (bufferSize - stringSize);
	const wchar_t * lend = _data.end();
	const wchar_t * rbegin = str.begin();
	const wchar_t * rend = str.end();

	while (lbegin < lend || rbegin < rend) {
		// write left part
		while (lbegin < lend) {
			const wchar_t c = *(lbegin++);
			if (c == '\n' || c == '\0')
				break;
			else
				*(obegin++) = c;
		}
		// writ right part
		while (rbegin < rend) {
			const wchar_t c = *(rbegin++);
			if (c == '\n' || c == '\0')
				break;
			else
				*(obegin++) = c;
		}
		// write line break
		*(obegin++) = '\n';
	}

	// fix string boundaries
	StringViewT::_begin = _data.begin();
	StringViewT::_length = obegin - _data.begin();
	*end() = 0;

	return *this;
}

String & String::Append(f32 f, const Text::StringView16 & format) {
	return Append((f64)f, format);
}

String & String::Append(f64 f, const Text::StringView16 & format) {
	static constexpr size_t BUFFER_SIZE = 256;
	wchar_t buffer[BUFFER_SIZE];
	wchar_t wformat[32];
	int size = 0;

	if (format.Length() == 1) {
		switch (format.First()) {
			case 'e': // semilogarithmic, e.g. 1e-5
				size = swprintf_s(buffer, BUFFER_SIZE, L"%e", f);
				return Append(Text::StringView(buffer, size));
			case 'E': // semilogarithmic, e.g. 1E-5
				size = swprintf_s(buffer, BUFFER_SIZE, L"%E", f);
				return Append(Text::StringView(buffer, size));
		}
	}

	const size_t m = format.Length();

	for (size_t i = 0; i < m; ++i)
		wformat[i + 1] = format[i];

	wformat[0] = '%';
	wformat[m + 1] = 'f';
	wformat[m + 2] = 0;


	size = swprintf_s(buffer, BUFFER_SIZE, wformat, f);
	return Append(Text::StringView(buffer, size));
}

String & String::Append(i32 i, const Text::StringView16 & format) {
	::FormatIntegerType(i, *this, format);
	return *this;
}

String & String::Append(u32 u, const Text::StringView16 & format) {
	::FormatIntegerType(u, *this, format);
	return *this;
}

String & String::Append(i64 i, const Text::StringView16 & format) {
	::FormatIntegerType(i, *this, format);
	return *this;
}

String & String::Append(u64 u, const Text::StringView16 & format) {
	::FormatIntegerType(u, *this, format);
	return *this;
}

String & String::Append(f32 f) {
	int appendLength = _scwprintf(L"%f", f);
	Reserve(appendLength + 1);
	swprintf_s(end(), appendLength + 1, L"%f", f);
	StringViewT::_length += appendLength;
	*end() = 0;
	return *this;
}

String & String::Append(f64 f) {
	int appendLength = _scwprintf(L"%f", f);
	Reserve(appendLength + 1);
	swprintf_s(end(), appendLength + 1, L"%f", f);
	StringViewT::_length += appendLength;
	*end() = 0;
	return *this;
}

String & String::Append(char c, const Text::StringView16 & format) {
	if (format.Length()) {
		throw;
		//size_t fill = (size_t)format.ToInteger();
		//return AppendFill(c, fill);
		return *this;
	}
	else {
		return Append(c);
	}
}

String & String::Append(wchar_t c, const Text::StringView16 & format) {
	if (format.Length()) {
		//size_t fill = (size_t)format.ToInteger();
		//return AppendFill(c, fill);
		throw;
		return *this;
	}
	else {
		return Append(c);
	}
}

String & String::Append(const char * str, const Text::StringView16 & format) {
	::FormatStringType(Text::StringView(str), *this, format);
	return *this;
}

String & String::Append(const Text::StringView16 & str, const Text::StringView16 & format) {
	::FormatStringType(str, *this, format);
	return *this;
}

String & DD::String::AppendTimestamp() {
  SYSTEMTIME time;
  ::GetSystemTime(&time);

  AppendFormat(
    "{0,4}-{1,2}-{2,2} {3,2}-{4,2}-{5,2}-{6,3}",
    /*0*/time.wYear,
    /*1*/time.wMonth,
    /*2*/time.wDay,
    /*3*/time.wHour,
    /*4*/time.wMinute,
    /*5*/time.wSecond,
    /*6*/time.wMilliseconds
  );

	return *this;
}

void String::Wrap(ArrayView1D<wchar_t> array, bool load /*= true*/) {
	_data.Swap(Array<wchar_t>{ array, nullptr });
	_begin = _data.begin();
	_length = 0;

	if (load)
		LoadFromBuffer();
}

#pragma endregion

/************************************************************************/
/* String - buffer manipulation                                         */
/************************************************************************/
#pragma region buffer
size_t String::freeLeft() const {
	return _data.begin() && StringViewT::_begin ? StringViewT::_begin - _data.begin() : 0;
}

size_t String::freeRight() const {
	return _data.begin() && StringViewT::_begin ? _data.end() - StringViewT::end() : 0;
}

String & String::Align() {
	BufferType ndata(Length());

	for (size_t i = 0; i < Length(); ++i)
		ndata[i] = (*this)[i];

	StringViewT::_length = Length();
	StringViewT::_begin = _data.begin();
	_data.Swap(ndata);

	return *this;
}

String & String::Reserve(size_t size) {
	return Reserve(0, size);
}

String & String::Reserve(size_t prepend, size_t append) {
	DD_STRING_ASSERT(ValidityCheck());
	// prepare constants
	size_t leftAlloc = 0;
	size_t rightAlloc = 0;
	size_t left = freeLeft();
	size_t right = freeRight();
	// compute left allocation
	if (left < prepend) {
		leftAlloc = prepend - left;
	}
	// compute right allocation
	if (right < append) {
		rightAlloc = append - right;
	}
	// allocation exists
	if (leftAlloc || rightAlloc) {
		size_t originalAlloc = _data.Length();
		size_t alloc = leftAlloc + rightAlloc + originalAlloc;
		// offset and must be recounted (not used "prepend") for the case that "left" is bigger than "prepend"
		size_t offset = leftAlloc + left;
		DD_TODO_STAMP("Following code will be broken by prepending, it should be copied with an offset.");
		{
			BufferType newData(alloc);
			newData.ToView().SubView(offset).CopyIn(ArrayView(StringViewT::_begin, StringViewT::_length));
			newData.Swap(_data);
		}

		// correct pointer to the new container
		StringViewT::_begin = _data.begin() + offset;

		// set null terminator if space is available
		if (end() < _data.end())
			*end() = 0;

		DD_STRING_ASSERT(ValidityCheck());
	}

	return *this;
}
#pragma endregion

/************************************************************************/
/* String - removing                                                    */
/************************************************************************/
#pragma region remove
String & String::RemoveChar(size_t i) {
	// char is in the string scope
	if (i < Length()) {
		// shift chars
		for (size_t j = i + 1; j < Length(); ++j) {
			(*this)[j - 1] = (*this)[j];
		}
		// correct the length
		--StringViewT::_length;
	}

	// set null-terminator
	*end() = '\0';

	return *this;
}
#pragma endregion

/************************************************************************/
/* String - insertion                                                   */
/************************************************************************/
#pragma region insertion
void String::insertAndShiftRight(size_t position, wchar_t * what, size_t size) {
	DD_STRING_ASSERT(ValidityCheck());
	// shift right
	for (size_t i = Length() + size; i >= position + size; --i) {
		(*this)[i] = (*this)[i - size];
	}
	// insert
	for (size_t i = 0; i < size; ++i) {
		(*this)[i + position] = what[i];
	}
	// correct length
	StringViewT::_length += size;
	// set null-terminator
	*end() = '\0';
	DD_STRING_ASSERT(ValidityCheck());

}

void String::insertAndShiftLeft(size_t position, wchar_t * what, size_t size) {
	DD_STRING_ASSERT(ValidityCheck());

	// correct properties
	StringViewT::_begin -= size;
	StringViewT::_length += size;
	// shift left
	for (size_t j = 0; j < position; ++j) {
		(*this)[j] = (*this)[j + size];
	}
	// insert
	for (size_t i = 0; i < size; ++i) {
		(*this)[i + position] = what[i];
	}
	// set null terminator
	*end() = '\0';
	DD_STRING_ASSERT(ValidityCheck());

}

void String::insertAndNoShift(size_t position, wchar_t * what, size_t size) {
	DD_STRING_ASSERT(ValidityCheck());

	// allocate new data space
	{
		auto leftPart = _data.ToView().HeadView(position);
		auto insertion = ArrayView(what, size);
		auto rightPart = _data.ToView().SubView(position);

		// copy parts to new allocation
		BufferType newData(_data.Length() + size + 1);
		newData.ToView().HeadView(position).CopyIn(leftPart);
		newData.ToView().SubView(position, size).CopyIn(insertion);
		newData.ToView().SubView(position + size).CopyIn(rightPart);

		// replace
		newData.Swap(_data);
		StringViewT::_begin = _data.begin();
	}

	// extend string
	StringViewT::_length += size;

	for (size_t i = 0; i < size; ++i) {
		(*this)[i + position] = what[i];
	}

	*end() = '\0';
	DD_STRING_ASSERT(ValidityCheck());
}

void String::insert(size_t position, wchar_t * what, size_t size) {
	const size_t leftSize = position;
	const size_t rightSize = Length() - position;

	if (leftSize < rightSize) {
		if (size < freeLeft()) insertAndShiftLeft(position, what, size);
		else if (size + 1 <= freeRight()) insertAndShiftRight(position, what, size);
		else insertAndNoShift(position, what, size);
	}
	else {
		if (size + 1 < freeRight()) insertAndShiftRight(position, what, size);
		else if (size <= freeLeft()) insertAndShiftLeft(position, what, size);
		else insertAndNoShift(position, what, size);
	}
}

String & String::InsertChar(size_t i, wchar_t c) {
	insert(i, &c, 1);
	return *this;
}

String & String::RemoveAndInsertChar(size_t remove, size_t insert, wchar_t c) {
	if (remove < insert) {
		for (size_t i = remove; i < insert; ++i) {
			(*this)[i] = (*this)[i + 1];
		}
	}
	else if (insert < remove) {
		for (size_t i = insert; i < remove; ++i) {
			(*this)[i + 1] = (*this)[i];
		}
	}

	(*this)[insert] = c;

	return *this;
}
#pragma endregion

/************************************************************************/
/* String - comparison methods                                          */
/************************************************************************/
#pragma region comparison
bool String::Contains(wchar_t letter) const {
	for (const auto & l : *this)
		if (l == letter)
			return true;

	return false;
}

Text::StringView16 String::Match(const wchar_t * pattern, size_t & iBegin, size_t & iEnd) const {
	static constexpr wchar_t WILDCARD = '*';
	static constexpr wchar_t END = 0;

	Text::StringView16  result;
	// wild-card in use
	bool wildcard = false;
	// index in pattern
	int index = 0;
	// initial check of wild-card
	if (pattern[index] == WILDCARD) {
		wildcard = true;
		// eat all wild-cards
		while (pattern[++index] == WILDCARD);
		// if pattern is over result is whole string
		if (pattern[index] == 0) {
			result = *this;
			iBegin = 0;
			iEnd = Length();
		}
	}

	const wchar_t * matchBegin = begin();
	for (const wchar_t & c : *this) {
		if ((c) == (pattern[index])) {
			// record beginning of the match
			if (index == 0) {
				matchBegin = &c;
			}
			// check if pattern is over
			switch (pattern[++index]) {
			case END:
				result = Text::StringView16{ matchBegin, (size_t)(&c - matchBegin) + 1 };
				iBegin = matchBegin - begin();
				iEnd = iBegin + result.Length();
				return result;
			case WILDCARD: {
				wildcard = true;
				// eat all wild-cards
				while (pattern[index] == WILDCARD)
					++index;
				// wildcard is last symbol in pattern
				if (pattern[index] == END) {
					result = Text::StringView16{ matchBegin, (size_t)(end() - matchBegin) + 1 };
					iBegin = matchBegin - begin();
					iEnd = iBegin + result.Length();
					return result;
				}
			} break;
				default: {
					wildcard = false;
				} break;
			}
		}
		else if (!wildcard) {
			index = 0;
		}
	}

	return result;
}


Text::StringView16 String::Match(const wchar_t * pattern) const {
	size_t _0, _1;
	return Match(pattern, _0, _1);
}

Text::StringView16 String::MatchI(const wchar_t * pattern, size_t & iBegin, size_t & iEnd) const {
	static constexpr wchar_t WILDCARD = '*';
	static constexpr wchar_t END = 0;

	Text::StringView16 result = nullptr;
	// wild-card in use
	bool wildcard = false;
	// index in pattern
	int index = 0;
	// initial check of wild-card
	if (pattern[index] == WILDCARD) {
		wildcard = true;
		// eat all wild-cards
		while (pattern[++index] == WILDCARD);
		// if pattern is over result is whole string
		if (pattern[index] == 0) {
			result = *this;
			iBegin = 0;
			iEnd = Length();
		}
	}

	const wchar_t * matchBegin = begin();
	for (const wchar_t & c : *this) {
		if (tolower(c) == tolower(pattern[index])) {
			// record beginning of the match
			if (index == 0) {
				matchBegin = &c;
			}
			// check if pattern is over
			switch (pattern[++index]) {
				case END: {
					result = Text::StringView16{ matchBegin, (size_t)(&c - matchBegin) + 1 };
					iBegin = matchBegin - begin();
					iEnd = iBegin + result.Length();
				} return result;
				case WILDCARD: {
					wildcard = true;
					// eat all wild-cards
					while (pattern[index] == WILDCARD)
						++index;
					// wildcard is last symbol in pattern
					if (pattern[index] == END) {
						result = Text::StringView16{ matchBegin, (size_t)(end() - matchBegin) + 1 };
						iBegin = matchBegin - begin();
						iEnd = iBegin + result.Length();
						return result;
					}
				} break;
				default: {
					wildcard = false;
				} break;
			}
		}
		else if (!wildcard) {
			index = 0;
		}
	}

	return result;
}

Text::StringView16 String::MatchI(const wchar_t * pattern) const {
	size_t _0, _1;
	return MatchI(pattern, _0, _1);
}

size_t String::CountChars(wchar_t c) const {
	size_t size = 0;
	for (auto i : *this)
		if (i == c)
			++size;

	return size;
}

//bool String::operator<(const String & str) const {
//	const size_t l = Length(), r = str.Length();
//	const size_t min = l < r ? l : r;
//
//	for (size_t i = 0; i < min; ++i) {
//		if ((*this)[i] > str[i])
//			return false;
//		else if ((*this)[i] < str[i])
//			return true;
//	}
//
//	if (l == r)
//		return false;
//	else if (l > r)
//		return false;
//	else
//		return true;
//}
#pragma endregion

/************************************************************************/
/* String - transcription                                               */
/************************************************************************/
#pragma region transcription

String & String::Replace(const ArrayView1D<const Text::StringView16> & patterns, const ArrayView1D<const Text::StringView16> & replacements) {
	DD_STRING_ASSERT(ValidityCheck());
	DD_TODO_STAMP("Rewrite as Aho-Corrasic algo.");
	if (patterns.Length() == replacements.Length()) {
		const Text::StringView16 * pattern = patterns.begin();
		const Text::StringView16 * replacement = replacements.begin();
		for (size_t i = 0, m = patterns.Length(); i < m; ++i) {
			Replace(*(pattern++), *(replacement++));
		}
	}

	DD_STRING_ASSERT(ValidityCheck());

	return *this;
}

String & String::Replace(const Text::StringView16 & pattern, const Text::StringView16 & replacement) {
	if (pattern.Length()) {
		int indication = 0;
		List<size_t, 128> occurences;

		wchar_t * b = begin();
		for (size_t i = 0, m = Length(); i < m; ++i) {
			// char of pattern found
			if (pattern[indication] == b[i]) {
				// full pattern found
				if (++indication == pattern.Length()) {
					occurences.Add(i - (indication - 1));
					indication = 0;
				}
			}
			// pattern missed
			else {
				indication = 0;
			}
		}

		if (occurences.Size()) {
			// if replacement is bigger, start replacing from the end
			if (pattern.Length() < replacement.Length()) {
				size_t shift = (replacement.Length() - pattern.Length()) * occurences.Size();
				Reserve(shift + 1);
				wchar_t * source = end();
				StringViewT::_length += shift;
				*end() = 0;
				wchar_t * target = end();
				for (size_t o = occurences.Size(); o > 0; ) {
					// get letter just after an occurence
					wchar_t* occurence = begin() + occurences[--o] + pattern.Length();
					// shift letters right to make space for occurence
					while (occurence < source) {
						*(--target) = *(--source);
					}
					for (size_t i = replacement.Length(); i > 0;) {
						*(--target) = replacement[--i];
					}
					source -= pattern.Length();
				}
			}
			// if pattern is bigger, start replacing from the beginning
			else if (pattern.Length() > replacement.Length()) {
				size_t diff = pattern.Length() - replacement.Length();
				wchar_t * target = b + occurences.First();
				wchar_t * source = b + occurences.First();

				for (size_t o = 0, m = occurences.Size(); o < m; ++o) {
					wchar_t * occurence = b + occurences[o];
					// copy text between occurences
					while (source < occurence) {
						*(target++) = *(source++);
					}
					// replace occurence
					for (wchar_t r : replacement) {
						*(target++) = r;
					}
					source += pattern.Length();
				}
				// fix the rest (space between end and last occurence)
				wchar_t* e = end();
				while (source < e) {
					*(target++) = *(source++);
				}
				StringViewT::_length -= (occurences.Size() * diff);
				*end() = 0;
			}
			// simple replacement
			else {
				for (size_t occurence : occurences) {
					wchar_t * o = b + occurence;
					for (wchar_t c : replacement) {
						*(o++) = c;
					}
				}
			}
		}
	}
	return *this;
}

String & String::Transcript(wchar_t(*transcriptor)(wchar_t)) {
	for (wchar_t & source : *this) {
		source = transcriptor(source);
	}
	return *this;
}

String & String::Transcript(int(*transcriptor)(int)) {
	for (wchar_t & source : *this) {
		source = (wchar_t)transcriptor(source);
	}
	return *this;
}

String & String::Transcript(char(*transcriptor)(char)) {
	for (wchar_t & source : *this) {
		source = (wchar_t)transcriptor((char)source);
	}
	return *this;
}

String & String::ToLower() {
	return Transcript(tolower);
}

String & String::ToUpper() {
	return Transcript(toupper);
}

//String & String::SubstringMe(size_t i, size_t length) {
//	StringViewT::_begin += i;
//	StringViewT::_length = length;
//	return *this;
//}
#pragma endregion

/************************************************************************/
/* String - splitting method                                            */
/************************************************************************/
#pragma region split
//String::Splitter String::Split(ILambda<i32(wchar_t)> * isDelimiter) const {
//	return Splitter(*this, isDelimiter);
//}

//String::SplitterLocal<32> String::Split(wchar_t letter) const {
//	return String::SplitterLocal<32>(*this, [letter](wchar_t c) -> i32 { return c == letter; }, DelimiterModes::Remove);
//}

//const String String::Substring(size_t i, size_t length) const {
//	String string;
//	string._data.Wrap(const_cast<Array<wchar_t>&>(_data));
//	string.StringViewT::_begin = StringViewT::_begin + i;
//	string.StringViewT::_length = string.StringViewT::_length + length;
//
//	return string;
//}
//
//const String String::Substring(size_t i) const {
//	return Substring(i, Length() - i);
//}
#pragma endregion

/************************************************************************/
/* String - converters                                                  */
/************************************************************************/
#pragma region converters

i64 String::ToInteger() const {
	return _wtoi(begin());
}

f64 String::ToDouble() const {
	return wcstod(begin(), NULL);
}

//bool String::TryCast(bool & target) const { return true; }
#pragma endregion

/************************************************************************/
/* String - assignment and construction                                 */
/************************************************************************/
#pragma region constructions
String& String::Swap(String & s) {
	_data.Swap(s._data);
	DD::Swap(StringViewT::_begin, s.StringViewT::_begin);
	DD::Swap(StringViewT::_length, s.StringViewT::_length);

	return *this;
}


#pragma endregion

/************************************************************************/
/* String - compatibility with STL formats                              */
/************************************************************************/
#pragma region stl
#if DD_STRING_STL_COMPATIBILITY
String & String::Load(std::wifstream & stream, size_t length) {
	if (_data.Size() < length)
		_data.Resize(length);

	StringViewT::_begin = _data.begin();
	StringViewT::_length = length;
	stream.read(begin(), length);

	return *this;
}

String & String::Load(std::wifstream & stream) {
	size_t position = stream.tellg();
	stream.seekg(0, std::ios::end);
	size_t end = stream.tellg();
	stream.seekg(position);

	return Load(stream, end - position);
}

String & String::Load(std::wifstream & stream, wchar_t delimiter) {
	size_t position = stream.tellg();
	size_t count = 0;
	wchar_t temp[1];

	while (stream.read(temp, 1) && !stream.eof()) {
		++count;
		if (delimiter == *temp)
			break;
	}

	stream.seekg(position);

	return Load(stream, count);
}

String::String(const std::string & str)
	: String(str.c_str(), str.size()) {}

String::String(const std::wstring & str)
	: String(str.c_str(), str.size()) {}
#endif
#pragma endregion
