#include <DD/Gpu/Devices.h>
#include <DD/Gpu/IConstantBuffer.h>

using namespace DD::Gpu;

void IConstantBuffer::Bind(u32 slot) {
	Bind2PS(slot);
	Bind2VS(slot);
	Bind2CS(slot);
}

void IConstantBuffer::Bind2CS(u32 slot) {
	_device->ConstantBufferBind2CS(*this, slot);
}

void IConstantBuffer::Bind2VS(u32 slot) {
	_device->ConstantBufferBind2VS(*this, slot);
}

void IConstantBuffer::Bind2PS(u32 slot) {
	_device->ConstantBufferBind2PS(*this, slot);
}

void IConstantBuffer::Update(const void * source) {
	_device->ConstantBufferUpdate(*this, source);
}

void IConstantBuffer::Init(Device & device, u32 bytes) {
	_device = device;
	_device->ConstantBufferInit(*this, bytes);
}

void IConstantBuffer::Release() {
	if (_device) {
		_device->ConstantBufferRelease(*this);
	}
}