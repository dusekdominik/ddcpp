#include <DD/DXMath.h>
#include <DD/Color.h>
#include <DD/List.h>
#include <DD/Perceptron.h>
#include <DD/Reference.h>
#include <DD/Simulation/EmptyVertex.h>
#include <DD/Simulation/LasModel.h>
#include <DD/XML.h>
#include <DD/Vector.h>

#include <stack>
#include <algorithm>

using namespace DD;
using namespace DD::Simulation;

enum Icons { Question, Biome, Building };

#if DDCGAL
#define PREFIX_CGAL "S:/Libraries/CGAL/build64/lib/"
#define PREFIX_GMP "s:/Libraries/CGAL/auxiliary/gmp/lib/"
#define LIB(prefix, library) comment(lib, prefix##library)
#define LIB_CGAL(lib) LIB(PREFIX_CGAL, lib)
#define LIB_GMP(lib)  LIB(PREFIX_GMP, lib)


#pragma LIB_GMP("libmpfr-4.lib")
#pragma LIB_GMP("libgmp-10.lib")
#pragma LIB_CGAL("CGAL_Core-vc140-mt-4.9.lib")
#pragma LIB_CGAL("CGAL_ImageIO-vc140-mt-4.9.lib")
#pragma LIB_CGAL("CGAL-vc140-mt-4.9.lib")
#pragma LIB_CGAL("CGAL_ImageIO-vc140-mt-gd-4.9.lib")
#pragma LIB_CGAL("CGAL_Core-vc140-mt-gd-4.9.lib")
#pragma LIB_CGAL("CGAL-vc140-mt-gd-4.9.lib")

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/point_generators_3.h>
#include <CGAL/algorithm.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/convex_hull_3.h>
#include <vector>
#endif


#if DDCGAL
using namespace DD;

typedef CGAL::Exact_predicates_inexact_constructions_kernel  K;
typedef CGAL::Polyhedron_3<K>                     Polyhedron_3;
typedef K::Segment_3                              Segment_3;
// define point creator
typedef K::Point_3                                Point_3;
typedef CGAL::Creator_uniform_3<float, Point_3>  PointCreator;
//a functor computing the plane containing a triangular facet
struct Plane_from_facet {
	Polyhedron_3::Plane_3 operator()(Polyhedron_3::Facet& f) {
		Polyhedron_3::Halfedge_handle h = f.halfedge();
		return Polyhedron_3::Plane_3(h->vertex()->point(),
			h->next()->vertex()->point(),
			h->opposite()->vertex()->point());
	}
};

struct Convertor {
	Point_3 operator()(const QuadVertex & q) {
		return Point_3(
			q.Position.x,
			q.Position.y,
			q.Position.z
		);
	}
};

int cvxTest(Engine & e, LasModel::MapInfo & buffer) {
	if (!buffer.HasModel()) return -1;
	CGAL::Random_points_in_sphere_3<Point_3, PointCreator> gen(100.0);
	// generate 250 points randomly on a sphere of radius 100.0
	// and copy them to a vector
	std::vector<Point_3> points(buffer.Count());
	std::transform(buffer.begin(), buffer.end(), points.begin(), Convertor());
	// define polyhedron to hold convex hull
	Polyhedron_3 poly;
	// compute convex hull of non-collinear points
	CGAL::convex_hull_3(points.begin(), points.end(), poly);
	DD_LOG("The convex hull contains ", poly.size_of_vertices(), " vertices");
	Reference<TriangleModel<SimpleVertex>> m = new TriangleModel<SimpleVertex>();
	int i = 0;
	DD_LOG("The convex hull contains ", poly.size_of_vertices(), poly.size_of_facets());
	for (auto b = poly.facets_begin(), e = poly.facets_end(); b != e; ++b) {
		//	Polyhedron_3::Halfedge_handle handle = e->halfedge();
		//	poly.create_center_vertex(handle);
		if (b->is_triangle()) {
			ColorVertex r, s, t;
			t.Color = s.Color = r.Color = Color::Collection[i++];
			r.Position.x = (float)b->halfedge()->next()->vertex()->point().x();
			r.Position.y = (float)b->halfedge()->next()->vertex()->point().y();
			r.Position.z = (float)b->halfedge()->next()->vertex()->point().z();
			s.Position.x = (float)b->halfedge()->vertex()->point().x();
			s.Position.y = (float)b->halfedge()->vertex()->point().y();
			s.Position.z = (float)b->halfedge()->vertex()->point().z();
			t.Position.x = (float)b->halfedge()->prev()->vertex()->point().x();
			t.Position.y = (float)b->halfedge()->prev()->vertex()->point().y();
			t.Position.z = (float)b->halfedge()->prev()->vertex()->point().z();
			unsigned short l = (unsigned short)m->Vertices().size();
			Simulation::TriangleModel<SimpleVertex>::Triangle t1(l, l + 2, l + 1);
			m->Vertices().add(r).add(s).add(t);
			m->Indices().add(t1);
		}
		else DD_LOG("Fail");
	}
	DD_LOG("The convex hull contains ", poly.size_of_vertices(), poly.size_of_facets());

	e.RegisterObject(m);
	// assign a plane equation to each polyhedron facet using functor Plane_from_facet
#if _DEBUG
	throw;
#else
	std::transform(
		poly.facets_begin(),
		poly.facets_end(),
		poly.planes_begin(),
		Plane_from_facet()
	);
#endif
	return 0;
}
#endif

using namespace DD;

static float3 Min(const float3 & a, const float3 & b) {
	return float3((Math::Min)(a.x, b.x), (Math::Min)(a.y, b.y), (Math::Min)(a.z, b.z));
}

static float3 Max(const float3 & a, const float3 & b) {
	return float3((Math::Max)(a.x, b.x), (Math::Max)(a.y, b.y), (Math::Max)(a.z, b.z));
}

static int Saturate(int x, int min, int max) { return (Math::Min)((Math::Max)(min, x), max); }


struct SegmentLine {
	float2 a, b;
	bool Intersects(const SegmentLine & o) {
		float2 intersection;
		float enumerator, denominator;
		denominator = ((a.x - b.x) * (o.a.y - o.b.y)) - ((a.y - b.y)*(o.a.x - o.b.x));
		if (denominator == 0) return false;
		enumerator = (((a.x * b.y) - (a.y * b.x)) * (o.a.x - o.b.x)) - ((a.x - b.x) * ((o.a.x * o.b.y) - (o.a.y * o.b.x)));
		intersection.x = enumerator / denominator;
		if (intersection.x < (Math::Min)(a.x, b.x)) return false;
		if (intersection.x > (Math::Max)(a.x, b.x)) return false;
		enumerator = (((a.x * b.y) - (a.y * b.x)) * (o.a.y - o.b.y)) - ((a.y - b.y) * ((o.a.x * o.b.y) - (o.a.y * o.b.x)));
		intersection.y = enumerator / denominator;
		if (intersection.y < (Math::Min)(a.y, b.y)) return false;
		if (intersection.y > (Math::Max)(a.y, b.y)) return false;
		return true;
	}
	float Size() const { return Math::Norm(b - a); }
};



#define LOCK volatile std::lock_guard<std::mutex> _(_queueMutex); //while (!_queueMutex.try_lock());

size_t LasModel::MapInfo::PREALLOCATION_CONSTANT = 0;

#pragma region LasModel::MapInfo

void LasModel::MapInfo::Region(size_t region) {
	_region = region;
	if (HasModel()) {
		//Model()->ToneColor() = Color::Collection[region];
	}
}

void LasModel::MapInfo::AddPoint(const QuadVertex & v) {
	if (_model == nullptr) {
		_model = new QuadModel();
		_model->Reserve(PREALLOCATION_CONSTANT);
		_model->Add(v);
	}
	else {
		_model->Add(v);
	}
}

float LasModel::MapInfo::RangeCheck(float minHeight, float maxHeight) {
	float number = 0;
	for (QuadVertex & point : *_model) {
		if (minHeight > point.Position.y) continue;
		if (maxHeight < point.Position.y) continue;
		number += 1.f;
	}
	return number / (float)Count();
}
#pragma endregion

#pragma region LasModel basic methods (selections, draw switching, buffer reinit)

void LasModel::ModelSwitch(Models s) {
	_triangleModel.SetEnabled((s & EnabledTriangle) != 0);
	_pointModel.SetEnabled((s & EnabledPoint) != 0);
	_boundingBoxModel.SetEnabled((s & EnabledLine) != 0);
}

LasModel::Models LasModel::ModelSwitch() {
	int m = EnabledNone;
	if (_triangleModel.Enabled()) m |= EnabledTriangle;
	if (_pointModel.Enabled()) m |= EnabledPoint;
	if (_boundingBoxModel.Enabled()) m |= EnabledLine;
	return (Models)m;
}

Array<LasModel::MapInfo *> LasModel::GetAABBSelectionSegements(const float2 & min, const float2 & max) {
	float2 gridMin = _min.xz;
	float2 gridMax = _max.xz;
	float2 scale = gridMax - gridMin;
	float2 minScaled = min - gridMin;
	minScaled.x /= scale.x;
	minScaled.y /= scale.y;
	float2 maxScaled = max - gridMin;
	maxScaled.x /= scale.x;
	maxScaled.y /= scale.y;
	int minimumX = (int)floor(minScaled.x * GRID_HEIGHT);
	int minimumZ = (int)floor(minScaled.y * GRID_WIDTH);
	int maximumX = (int)ceil(maxScaled.x * GRID_HEIGHT);
	int maximumZ = (int)ceil(maxScaled.y * GRID_WIDTH);

	minimumX = Math::Saturate(minimumX, 0, (int)(GRID_HEIGHT - 1));
	minimumZ = Math::Saturate(minimumZ, 0, (int)(GRID_WIDTH - 1));
	maximumX = Math::Saturate(maximumX, 0, (int)(GRID_HEIGHT));
	maximumZ = Math::Saturate(maximumZ, 0, (int)(GRID_WIDTH));

	size_t surface = (size_t)((maximumX - minimumX) * (maximumZ - minimumZ));

	Array<MapInfo *> result(surface);
	for (size_t z = minimumZ, k = 0; z < maximumZ; ++z) {
		for (size_t x = minimumX; x < maximumX; ++x) {
			result[k++] = &_map[x][z];
		}
	}
	return result;
}

Array<QuadVertex *> LasModel::GetAABBSelectionPoints(const float2 & min, const float2 & max) {
	Array<MapInfo *> selection = GetAABBSelectionSegements(min, max);
	size_t count = 0, i = 0;
	for (MapInfo * segment : selection) {
		count += segment->Count();
	}
	Array<QuadVertex *> buffer(count);
	for (MapInfo * segment : selection) {
		for (QuadVertex & v : *segment) {
			if (v.Position.x < min.x) continue;
			if (v.Position.z < min.y) continue;
			if (v.Position.x > max.x) continue;
			if (v.Position.z > max.y) continue;
			buffer[i++] = &v;
		}
	}

	Array<QuadVertex *> result(i);
	result.CopyIn(buffer.ToView());
	return result;
}

Array<QuadVertex*> LasModel::GetCVXSelection(const Polygon<float> & cvx) {
	float2 minimum(+FLT_MAX, +FLT_MAX), maximum(-FLT_MAX, -FLT_MAX);
	for (const float2 & p : cvx.Points) {
		if (minimum.x > p.x) minimum.x = p.x;
		if (minimum.y > p.y) minimum.y = p.y;
		if (maximum.x < p.x) maximum.x = p.x;
		if (maximum.y < p.y) maximum.y = p.y;
	}
	Array<QuadVertex*> aabbSelection = GetAABBSelectionPoints(minimum, maximum);
	Array<QuadVertex*> cvxSelection(aabbSelection.Length());
	size_t index = 0;

	for (QuadVertex * v : aabbSelection)
		if (cvx.IsIn(v->Position.xz))
			cvxSelection[index++] = v;

	if (index != cvxSelection.Length())
		cvxSelection.Reform(index);

	return cvxSelection;
}
#pragma endregion

#pragma region LasModel IModel implementation

void LasModel::iModelInit(Gpu::Device & device) {
	LOCK
	_pointModel.Init(device);
	_boundingBoxModel.Init(device);
	_triangleModel.Init(device);
}

void LasModel::iModelDrawCulled(
	const Frustum3F & f, const float4x4 & transform, DrawArgs & args
) {
	LOCK;
	if (_pointModel.Enabled()) {
		_pointModel.DrawFrustumCulled(f, transform, args);
	}
	if (_boundingBoxModel.Enabled()) {
		_boundingBoxModel.DrawFrustumCulled(f, transform, args);
	}
	if (_triangleModel.Enabled()) {
		_triangleModel.DrawFrustumCulled(f, transform, args);
	}
	if (_iconModel.Enabled()) {
		_iconModel.DrawFrustumCulled(f, transform, args);
	}
}

void LasModel::iModelDraw(DrawArgs & args) {
	LOCK;
	if (_pointModel.Enabled()) {
		_pointModel.Draw(args);
	}
	if (_boundingBoxModel.Enabled()) {
		_boundingBoxModel.Draw(args);
	}
	if (_triangleModel.Enabled()) {
		_triangleModel.Draw(args);
	}
	if (_iconModel.Enabled()) {
		_iconModel.Draw(args);
	}
}

#pragma endregion

#pragma region LasModel IMessage implementation

Gpu::D2D::Strings LasModel::iMessageGetText() const {
	LOCK
// 		auto parseNum = [](size_t number) {
// 		std::stack<unsigned char> digits;
// 		std::wstringstream stream;
// 		do { digits.push(number % 10); number /= 10; } while (number);
// 		for (size_t i = digits.size(); digits.size(); --i, digits.pop()) {
// 			if (i % 3 == 0) stream << L' ';
// 			stream << digits.top();
// 		}
// 		return stream.str();
// 	};
	String builder(1024);
	int save = (int)(this->GetLoadRatio() * 100);
	int points = this->GetTotalCount();
	builder
		.Append("Las object:")
		.Append("\n\t")
		.Append(this->_path)
		.Append("\n\tNumber of points: ")
		//.Append(parseNum(points))
		;
	if (save < 100 || this->_loadingIntoVRAM) {
		int VRAM = (this->_loadingVRAMCount * 100) / this->GetTotalCount();
		builder
			.Append("\n\tLoading into RAM: ")
			.Append(save)
			.Append("%")
			.Append("\n\tLoading into VRAM: ")
			.Append(VRAM)
			.Append("%")
			;
	}
	if (this->_writer) {
		int load = (int)(this->GetSaveRatio() * 100);
		builder
			.Append("\n\tSaving in progress: ")
			.Append(load)
			.Append("%")
			;
	}

	switch (_sharedInformation.Method) {
	case MethodShared::RegionGrowingOnPoint:
		builder.AppendBatch("\n\t");

		switch (_sharedInformation.RGOP.State) {
		case MSRegionGrowingOnPoints::ShowRegioningInfo:
			builder.AppendBatch(
				"Creating region number: ", _sharedInformation.RGOP.RegionNumber,
				"\n\tIncluded Points:    ", _sharedInformation.RGOP.EvaluatedPoints
			);
		}
	case LasModel::MethodShared::RegionGrowingBasedOnHistogram:
		builder.AppendBatch("\n\tRegion Growing based on Histogram Heuristic");
		switch (_sharedInformation.RGBH.State) {
		case MSRegionGrowingOnHistogram::CreatingHistogram:
			builder.AppendBatch(
				"\n\t\tCreating histogram - ",
				(_sharedInformation.RGBH.TileProcessed * 100) / _sharedInformation.RGBH.TileCount,
				" %"
			);
			break;
		case LasModel::MSRegionGrowingOnHistogram::Growing:
			builder.AppendBatch(
				"\n\t\tGrowing regions"
				"\n\t\t\tregions accepted: ", _sharedInformation.RGBH.RegionsAccepted,
				"\n\t\t\tregions created:  ", _sharedInformation.RGBH.RegionsCreated,
				"\n\t\t\tevaluated points: ", _sharedInformation.RGBH.EvaluatedPoints
			);
			break;
		case LasModel::MSRegionGrowingOnHistogram::CreatingConvexHull:
			builder.AppendBatch("\n\t\tCreating convex hulls.");
			break;
		case LasModel::MSRegionGrowingOnHistogram::CreatingConcaveHull:
			builder.AppendBatch("\n\t\tTransforming convex hulls into concave hulls.");
			break;
		default:
			break;
		}
	}

	builder.AppendBatch("\n", _appendInfo);
	return Gpu::D2D::Strings(builder.View());
}
#pragma endregion

#pragma region LasModel IO

void LasModel::buildfrustumCullingHierarchy() {
	Array2D<Reference<ICullableModel>> model[2];
	model[0].Reallocate(_map.Lengths());
	for (const auto & index : Sequencer::Drive(_map.ToView().Counter()))
		model[0].ToView()[index] = _map.ToView()[index].Model();

	//for (size_t row = 0; row < _map.Height(); ++row) for (size_t col = 0; col < _map.Width(); ++col) {
	//	model[0][row][col] = _map[row][col].Model();
	//}
	int iteration = 0;

	while (true) {
		Array2D<Reference<ICullableModel>> & source = model[iteration % 2];
		Array2D<Reference<ICullableModel>> & target = model[(iteration + 1) % 2];
		target = Array2D<Reference<ICullableModel>>(
			source.Lengths().Convert([](size_t length) { return length / 2 + length % 2; })
		);
		int decay = 1 << ++iteration;
		//for (size_t y = 0; y < target.Height(); ++y) for (size_t x = 0; x < target.Width(); ++x) {
		for (const auto & index : Sequencer::Drive(target.ToView().Counter())) {
			Reference<IntersectableModelGroup> group = new IntersectableModelGroup();

			size_t xm = index[0] * 2;
			size_t xM = xm + 1;
			size_t ym = index[1] * 2;
			size_t yM = ym + 1;

			group->Add(source[ym][xm]);

			if (xM < source.Lengths()[0])
				group->Add(source[ym][xM]);

			if (yM < source.Lengths()[1])
				group->Add(source[yM][xm]);

			if (yM < source.Lengths()[1] && xM < source.Lengths()[0])
				group->Add(source[yM][xM]);

			if (!group->IsEmpty())
				target.ToView()[index] = group;
		}

		if (target.Lengths()[0] == 1 && target.Lengths()[1] == 1)
			break;
	}
	{LOCK
		_pointModel.Add(model[iteration % 2][0][0]);
	}
}

void LasModel::Load() {
	const double GRID_UNIT = 25.0;
	Las::Formats format = _reader->GetHeader().PointDataFormatID();
	// scaling
	auto gpsBBox = _reader->GetGPSBBox();
	float3 gpsScale = gpsBBox.Max - gpsBBox.Min;
	auto meterScale = _max - _min;
	// dimensions of point cloud
	const auto dimensions = _reader->GetDimensions();
	// dimension of single point
	const float excentricity = 0.25f * (float)sqrt((dimensions.x * dimensions.y) / _reader->GetTotalCount());
	// const double qq = 500.0;
	const size_t width = (size_t)(format == Las::Formats::FormatDD ? dimensions.z : dimensions.y);
	GRID_WIDTH = Math::Max<size_t, size_t>(static_cast<size_t>(width / GRID_UNIT), 1);
	GRID_HEIGHT = Math::Max<size_t, size_t>(static_cast<size_t>(dimensions.x / GRID_UNIT), 1);
	// size of common point cloud segment
	MapInfo::PREALLOCATION_CONSTANT = 2 * _reader->GetTotalCount() / (GRID_WIDTH * GRID_HEIGHT);
	// create storing grid
	_map.Reallocate({ GRID_WIDTH, GRID_HEIGHT });
	const float GRID_X_CONSTANT = GRID_HEIGHT / meterScale.x;
	const float GRID_Z_CONSTANT = GRID_WIDTH / meterScale.z;
	// load point cloud into RAM
	switch (format) {
		// DD format optimized for fast loading
	case Las::Formats::FormatDD:
	{
		Las::Point<Las::Formats::FormatDD> & handler = reinterpret_cast<Las::Point<Las::Formats::FormatDD> &>(*(_reader->GetPoint()));
		Las::Header & h = _reader->GetHeader();
		QuadVertex qv;
		while (_reader->ReadPoint()) {
			qv.Position = { handler.X(), handler.Y(), handler.Z() };
			qv.Color = handler.ColorAndClassification();
			unsigned infoX = (unsigned)(GRID_X_CONSTANT * (qv.Position.x - _min.x));
			unsigned infoZ = (unsigned)(GRID_Z_CONSTANT * (qv.Position.z - _min.z));
			if (infoX < GRID_HEIGHT && infoZ < GRID_WIDTH) {
				_map[infoX][infoZ].AddPoint(qv);
			}
		}
	}
	break;
	default:
	{
		QuadVertex qv;
		const float3 scale = meterScale.xyz / gpsScale.xzy;
		while (_reader->ReadPoint()) {
			V3<u16> color(_reader->GetColor());
			qv.Position = _min + scale * (_reader->GetGPS().xzy - gpsBBox.Min.xzy);
			qv.Color = Color::FromSRGB(color.x / 65535.f, color.y / 65535.f, color.z / 65535.f);
			qv.Color.a = (unsigned char)_reader->GetClassification();
			unsigned infoX = (unsigned)(GRID_X_CONSTANT * (qv.Position.x - _min.x));
			unsigned infoZ = (unsigned)(GRID_Z_CONSTANT * (qv.Position.z - _min.z));
			if (infoX < GRID_HEIGHT && infoZ < GRID_WIDTH) {
				_map[infoX][infoZ].AddPoint(qv);
			}
		}
	}
	break;
	}

	for (MapInfo & info : _map.ToView().PlainView())
		if (info.HasModel())
			info.Model()->PointSize(_pointSize);

	buildfrustumCullingHierarchy();
	Loaded(*this);
}

void LasModel::Save(const String & path) {
	if (path.IsEmpty()) {
		return;
	}

	size_t points = 0;
	for (MapInfo & info : _map.ToView().PlainView()) {
		if (info.Model().IsNotNull()) {
			points += info.Model()->Count();
		}
	}
	_writer = new Las::FileWriter;
	Las::FileWriter & writer = *_writer;
	if (writer.Open(path)) {
		Las::Header & h = writer.GetHeader();
		h.InitSignature();
		h.PointDataFormatID() = Las::Formats::FormatDD;
		h.NumberOfVariableLengthRecords() = 0;
		h.NumberOfPointRecords() = (u32)points;
		h.MinX() = _min.x;
		h.MinY() = _min.y;
		h.MinZ() = _min.z;
		h.MaxX() = _max.x;
		h.MaxY() = _max.y;
		h.MaxZ() = _max.z;
		Debug::Log::PrintTabLine(h.MinX(), h.MinY(), h.MinZ());
		Debug::Log::PrintTabLine(h.MaxX(), h.MaxY(), h.MaxZ());
		h.ScaleX() = 1.0;
		h.ScaleY() = 1.0;
		h.ScaleZ() = 1.0;
		h.OffsetX() = 0.0;
		h.OffsetY() = 0.0;
		h.OffsetZ() = 0.0;
		if (writer.Init()) {
			for (MapInfo & segment : _map.ToView().PlainView())  for (QuadVertex & point : segment) {
				writer.SetPoint(point.Position.x, point.Position.y, point.Position.z, point.Color);
				writer.WritePoint();
			}
		}
	}
	writer.Close();
	_writer.Release();
}

void LasModel::ExportToXML(const String & path) {
	static XML::Document document(L"Contours");
	static auto contour = document->DeclareElement(L"Contour");
	static auto _pts = contour->GetAttributeDecl(L"Points");
	static auto point = document->DeclareElement(L"Point");
	static auto _lat = point->GetAttributeDecl(L"Latitude");
	static auto _lon = point->GetAttributeDecl(L"Longitude");
	static auto _ll = point->GetAttributeDecl(L"LatLon");

	if (path.IsEmpty()) {
		return;
	}
#if 0
	std::wofstream stream((const wchar_t *)path);

	if (stream.good()) {
		auto & gpsBox = _reader->GetGPSBBox();
		auto gpsDiff = gpsBox.Max - gpsBox.Min;
		DD_LOG("gpsDiff");
		double3 minimum, maximum;
		minimum = _min;
		maximum = _max;
		double3 diff = maximum - minimum;

		for (Region & region : _regions) {
			XML::Element & c = document->GetRoot().DefineElement(contour, _pts(String::From(region.Hull.Hull.Size())));
			for (float2 & pt : region.Hull.Hull) {
				double x = gpsBox.Min.x + (gpsDiff.x * (pt.x - minimum.x) / diff.x);
				double y = gpsBox.Min.y + (gpsDiff.y * (pt.y - minimum.z) / diff.z);
				String & lat = String::From(x);
				String & lon = String::From(y);
				String & latlon = String(2 + lat.Length() + lon.Length()).AppendBatch(lat, ' ', lon);
				c.DefineElement(point,  _lat(lat), _lon(lon), _ll(latlon));
			}
		}
		stream << document->ToString();
		stream.close();
	}
#endif
}

LasModel::LasModel(const String & file)
	: _valid(0)
	, _enabled(1)
	, _path(file)
	, _loadingIntoVRAM(false)
	, _iconModel(ICON_QUESTION, ICON_BIOME, ICON_BUILDING)
{
	LOCK;

	_reader = new Las::FileReader();
	if (_reader->Open(file)) {
		switch (_reader->GetHeader().PointDataFormatID()) {
			case Las::Formats::FormatDD: {
				auto box = _reader->GetBBox();
				double3 dimensions = _reader->GetDimensions();
				_pointSize = 0.25f * (float)sqrt((dimensions.x * dimensions.z) / _reader->GetTotalCount());
				_min = box.Min.xyz, _max = box.Max.xyz;
			} break;
			default:
				double3 dimensions = _reader->GetDimensions();
				if (dimensions.xyz == 0.0)
					dimensions.Set(500, 500, 100);
				_min = 0.f, _max = dimensions.xzy;
				_pointSize = 0.25f * (float)sqrt((dimensions.x * dimensions.y) / _reader->GetTotalCount());
				break;
		}
		_valid = true;
	}
}
#pragma endregion

#pragma region LasModel RegionGrowingOnBBox

#define NO_REGION 0
#define INIT_REGION 1;

void LasModel::RegionGrowingOnBBox(const SettingRegionGrowingOnBBoxes & settings) {
	size_t region = INIT_REGION;

	// Reset evaluation
	ArrayView1D<MapInfo> view = _map.ToView().PlainView();
	for (MapInfo & cell : view) {
		cell.Region(NO_REGION);
	}

	// Evaluate regions
	for (MapInfo & cell : view) {
		if (cell.Model().IsNotNull() && cell.Region() == NO_REGION) {
			cell.Region(region++);
			growRegionOnBBox(&cell, settings);
		}
	}

	Debug::Log::PrintLine("Regions: ", region);


	// Create region statics
	struct Helper {
		unsigned region, count;
		bool operator<(const Helper & h) { return h.count < count; }
	};
	Array<Helper> regions = Array<Helper>::Uniform(region, { 0,0 });
	for (MapInfo & cell : view) {
		regions[cell.Region()].region = (unsigned)cell.Region();
		regions[cell.Region()].count++;
	}

	/*std::sort(regions.begin(), regions.end());
	for (size_t i = 0, max = regions.Size(); i < max; ++i) {
	Debug::PrintLine("Region[", i, "] = { ", regions[i].region, ", ", regions[i].count,"}");
	}*/

	// Recolor points on RAM
	for (MapInfo & cell : view) {
		if (cell.HasModel()) for (QuadVertex & vertex : cell) {
			auto & count = regions[cell.Region()].count;
			if (count > 300) {
				reinterpret_cast<Color &>(vertex.Color).a = 1;
			}
			else if (count > 50) {
				reinterpret_cast<Color &>(vertex.Color).a = 2;
			}
			else if (count == 1) {
				reinterpret_cast<Color &>(vertex.Color).a = 1;
			}
			else {
				reinterpret_cast<Color &>(vertex.Color).a = 4;
			}
		}
	}
	// Recolor points on VRAM
	_pointModel.Reinit();
}

void LasModel::growRegionOnBBox(MapInfo * init, const SettingRegionGrowingOnBBoxes & settings) {
	if (init->MaxHeight() - init->MinHeight() > 5.f)
		return;

	size_t region = init->Region();
	List<MapInfo *> fringe;
	fringe.Add(init);
	while (fringe.IsNotEmpty()) {
		MapInfo * cell = fringe.Last();
		fringe.RemoveLast();
		for (MapInfo & neighbor : Sequencer::Drive(_map.ToView().Neighborhood(cell))) {
			// syntax check
			if (/*neighbor && */neighbor.Model().IsNotNull() && neighbor.Region() == NO_REGION) {
				// bbox check
				bool minOk = Math::Abs(neighbor.MinHeight() - cell->MinHeight()) < settings.MIN_HEIGHT_TOLERANCE;
				bool maxOk = Math::Abs(neighbor.MaxHeight() - cell->MaxHeight()) < settings.MAX_HEIGHT_TOLERANCE;
				bool ok = minOk && maxOk;
				// point check
				if (ok ||
					neighbor.RangeCheck(cell->MinHeight() - settings.SLOPE_DEVIATION, cell->MinHeight() + settings.SLOPE_DEVIATION) > settings.PARTITIONING ||
					neighbor.RangeCheck(cell->MaxHeight() - settings.SLOPE_DEVIATION, cell->MaxHeight() + settings.SLOPE_DEVIATION) > settings.PARTITIONING
				) {
					neighbor.Region(region);
					fringe.Add(&neighbor);
				}
			}
		}
	}
}
#pragma endregion

#pragma region Morphological filtering

void LasModel::MorphologicalFiltering(const SettingsMorphologicalFiltering & settings) {
	float CELL_SIZE = GRID_UNIT;
	static const unsigned char CLASS_ROOF = (unsigned char)Las::Classification::Unclassified;
	static const unsigned char CLASS_NONE = (unsigned char)Las::Classification::Ground;

	// reset point settings
	ArrayView1D<MapInfo> mapView = _map.ToView().PlainView();
	for (MapInfo & info : mapView) for (QuadVertex & qv : info) {
		reinterpret_cast<f32 &>(qv.Backup) = qv.Position.y;
		qv.Color.a = CLASS_NONE;
	}

	for (int run = 1; run <= settings.ITERATIONS; ++run) {
		// info for each iteration
		_appendInfo = String(128).AppendBatch(
			"\tMorphological filtering in progress ", ((100 * (run - 1)) / settings.ITERATIONS), "%\n",
			"\tParameters:\n",
			"\t\tNumber of iterations ", settings.ITERATIONS, "\n",
			"\t\tSquared radius       ", settings.RADIUS2, "\n",
			"\t\tSlope                ", settings.SLOPE, "\n",
			"\t\tMethod               ", settings.METHOD, "\n"
		);

		unsigned long long windowFactor = 0;
		switch (settings.METHOD) {
		case Linear: windowFactor = run; break;
		case Quadratic: windowFactor = run * run;
		case Exponential:
			windowFactor = 1;
			windowFactor <<= static_cast<unsigned long long>(run - 1);
		}

		const float windowRadius2 = settings.RADIUS2 * windowFactor;
		const float windowRadius = sqrt(windowRadius2);
		const float elevationDifference = settings.SLOPE * CELL_SIZE * windowRadius;

		for (MapInfo & info : mapView) for (QuadVertex & ptX : info) {
			if (ptX.Color.a == CLASS_ROOF) continue;

			float min = +FLT_MAX, max = -FLT_MAX;
			for (MapInfo & neighbor : Sequencer::Drive(_map.ToView().Neighborhood(&info, (int)ceil(windowRadius / 5.f)))) {
				/*if (neighbor)*/  for (QuadVertex & ptY : neighbor) {
					if (ptY.Color.a != CLASS_ROOF) {
						float3 difference = ptX.Position - ptY.Position;
						float distance2 = dot(difference);
						if (distance2 < windowRadius2) {
							//if (ptY.Backup < min) min = ptY.Backup;
							//if (ptY.Backup > max) max = ptY.Backup;
							if (ptY.Position.y < min) min = ptY.Position.y;
							if (ptY.Position.y > max) max = ptY.Position.y;
						}
					}
				}
			}
			if (run % 2 == 0) {
				//ptX.Backup = min;
				ptX.Position.y = min;
			}
			else {
				//ptX.Backup = max;
				ptX.Position.y = max;
				//if (ptX.Position.y - ptX.Backup > elevationDifference) {
				if (ptX.Backup - ptX.Position.y > elevationDifference) {
					ptX.Color.a = CLASS_ROOF;
				}
				else {
					ptX.Color.a = CLASS_NONE;
				}
			}
		}
	}

	_pointModel.Reinit();
	_appendInfo.Clear();
}
#pragma endregion

#pragma region LasModel RegionGrowingOnPoints

#define REGION(v) reinterpret_cast<unsigned&>(v.Backup)

const unsigned SQ_NO_REGION = 0;
const unsigned SQ_AE_REGION = 1;
const unsigned SQ_YO_REGION = 2;
void LasModel::RegionGrowingOnPoints(const SettingsRegionGrowingOnPoints & settings) {
	const unsigned char NO_COLOR = 0;
	_sharedInformation.Method = LasModel::MethodShared::RegionGrowingOnPoint;
	_appendInfo = "RegionGrowingOnPoints";

	// reset colors
	ArrayView1D<MapInfo> mapView = _map.ToView().PlainView();
	for (MapInfo & segment : mapView) for (QuadVertex & v : segment) {
		v.Color.a = NO_COLOR;
		REGION(v) = SQ_NO_REGION;
	}

	// grow on RAM


	size_t region = NO_COLOR;
	_sharedInformation.RGOP.State = LasModel::MSRegionGrowingOnPoints::ShowRegioningInfo;
	_sharedInformation.RGOP.RegionNumber = 0;
	for (MapInfo & segment : mapView) for (QuadVertex & v : segment) {
		if (REGION(v) == SQ_NO_REGION) {
			++_sharedInformation.RGOP.RegionNumber;
			growRegionOnPoints(settings, { &segment, &v }, v.Position.y);
		}
	}


	// create hulls
	List<ConvexHull2d> hulls;
	for (Region & r : _regions) {
		r.RecalculateHull();
		r.SplitLines(1.f);
		//if (r.IsGreen()) r.Clear();
	}


	// remove some convex hulls
	//for (size_t q = 0; q < 3; ++q) {
	//	for (size_t i = 0, l = _regions.Size(); i < l; ++i) {
	//		Region & r0 = _regions[i];
	//		if (r0.IsEmpty()) continue;
	//		for (size_t j = 0; j < l; ++j) {
	//			if (j == i) continue;
	//			Region & r1 = _regions[j];
	//			if (r1.IsEmpty()) continue;

	//			if (r0.PercentageIn(r1) > 0.0f) {
	//				r0.Merge(r1);
	//				r0.SplitLines(1.f);
	//				r1.Clear();
	//			}
	//		}
	//	}
	//}

	// print hulls
	_triangleModel.Clear();
	for (Region & r : _regions) {
		LOCK
			_triangleModel.Add(r.CreateHullModel());
	}


	//for (MapInfo & segment : _map) if(segment.HasModel()) {
	//	for (QuadVertex & v : segment) {
	//		ColorHSV hsv = v.Color.ToHSV();
	//		// if (60 <= hsv.h && hsv.h <= 180) v.Color.a = 3;
	//		if (hsv.IsGreen()) v.Color.a = 3;
	//		else if (180 <= hsv.h && hsv.h <= 270) v.Color.a = 4;
	//		else v.Color.a = 2;
	//	}
	//}


	// recolor VRAM
	_pointModel.Reinit();
	_sharedInformation.Method = LasModel::MethodShared::None;
}

void LasModel::growRegionOnPoints(const SettingsRegionGrowingOnPoints & settings, PointSegmentRelation qs, float y) {

	_sharedInformation.RGOP.EvaluatedPoints = 0;
	List<PointSegmentRelation> stack(1024 * 1024);
	List<QuadVertex *> outOfHeight(1024 * 4);
	stack.Add(qs);
	while (_sharedInformation.RGOP.EvaluatedPoints < stack.Size()) {
		PointSegmentRelation qs = stack[_sharedInformation.RGOP.EvaluatedPoints++];
		QuadVertex & point = *(qs.point);
		y = point.Position.y;
		for (MapInfo & neighbor : Sequencer::Drive(_map.ToView().Neighborhood(qs.segment))) /*if (neighbor)*/ {
			for (QuadVertex & point2 : neighbor) {
				if (REGION(point2) == SQ_NO_REGION) {
					if (Math::Abs(point2.Position.y - y) < settings.DISTANCE_Y) {
						if (Math::Dot(point2.Position.xz - point2.Position.xz) < settings.DISTANCE_XZ) {
							REGION(point2) = SQ_AE_REGION;
							stack.Add(PointSegmentRelation{ &neighbor, &point2 });
						}
					}
					else {
						REGION(point2) = SQ_YO_REGION;
						outOfHeight.Add(&point2);
					}
				}
			}
		}
	}

	while (outOfHeight.IsNotEmpty()) {
		REGION((*(outOfHeight.Last()))) = SQ_NO_REGION;
		outOfHeight.RemoveLast();
	}

	if (_sharedInformation.RGOP.EvaluatedPoints > settings.MINIMUM_POINTS) {
		if (_sharedInformation.RGOP.EvaluatedPoints < settings.MAXIMUM_POINTS) {
			size_t region = _regions.Size();
			_regions.Add(Region(stack.Size()));
			Region & r = _regions.Last();
			r.Number = region;
			r.Points.Resize(stack.Size());
			for (size_t i = 0, l = stack.Size(); i < l; ++i) {
				r.Points[i] = stack[i].point;
			}
			r.Classify();
			r.Icon = _iconModel[_iconModel.AddItem(Question, r.IconPosition())];
			RegionAccepted(r);
		}
	}
	else {
		for (PointSegmentRelation & q : stack) {
			q.point->Color.a = 1;
		}
	}
}

#pragma endregion;

#pragma region LasModel RegionGrowingHistogram
void LasModel::RegionGrowingBasedOnHeightHistogram(const SettingsRegionGrowingOnHistogram & settings, Engine & e) {
	_sharedInformation.RGBH.State = LasModel::MSRegionGrowingOnHistogram::Growing;
	_sharedInformation.RGBH.EvaluatedPoints = 0;
	_sharedInformation.RGBH.RegionsAccepted = 0;
	_sharedInformation.RGBH.RegionsCreated = 0;

	// create bounding boxes
	_regions.Clear();
	for (MapInfo & segment : _map.ToView().PlainView()) {
		if (segment.HasModel() && segment.Region() != 9) {
			growRegionHistogram(settings, &segment);
		}
	}

	_sharedInformation.RGBH.State = LasModel::MSRegionGrowingOnHistogram::CreatingConvexHull;

	// surface check
	for (Region & region : _regions) {
		if (region.Hull.Surface < settings.MINIMUM_SURFACE || region.Hull.Surface > settings.MAXIMUM_SURFACE) {
			region.Hull = ConvexHull2d();
			--_sharedInformation.RGBH.RegionsAccepted;
		}
	}

	// remove some convex hulls
	for (size_t i = 0, l = _regions.Size(); i < l; ++i) {
		if (_regions[i].Hull.Hull.Size() == 0) continue;
		for (size_t j = 0; j < l; ++j) {
			if (j == i) continue;
			if (_regions[j].Hull.Hull.Size() == 0) continue;
			size_t count = 0;
			for (float2 & v : _regions[i].Hull.Hull) {
				if (_regions[j].Hull.IsIn(v)) {
					++count;
				}
			}
			if (((float)count / _regions[i].Hull.Hull.Size()) > settings.CONVEX_HULL_MERGE) {
				auto & from = _regions[i].Points;
				auto & to = _regions[j].Points;
				to.AddIterable(from);
				_regions[i].Hull.Hull.Clear();
				_regions[j].RecalculateHull();
			}
		}
	}


	_sharedInformation.RGBH.State = LasModel::MSRegionGrowingOnHistogram::CreatingConcaveHull;

	/*for (size_t i = 0; i < hulls.Size(); ++i) {
		repairHull(settings, hulls[i], _regions[i].Points);
	}*/


	// Convert convex hulls into concave hulls
	for (Region & region : _regions) {
		region.ConcaveHull(
			settings.FITTING_CONCAVE_HULL_SEARCH_AREA,
			settings.FITTING_CONCAVE_HULL_SHIFT_SIZE,
			settings.MAXIMAL_POLYGON_EDGE_STATIC,
			settings.MAXIMAL_POLYGON_EDGE_DYNAMIC,
			settings.FITTING_CONCAVE_HULL_DE_CASTELJAU_ITERATIONS
		);
	}



	// display convex hulls
	{LOCK
		_triangleModel.Clear();
	for (Region & region : _regions) {
		if (region.Hull.Hull.Size()) {
			_triangleModel.Add(region.CreateHullModel());
		}
	}
	}

	// Recolor points on VRAM
	_pointModel.Reinit();
	_sharedInformation.Method = LasModel::MethodShared::None;
}

void LasModel::RegionGrowingBasedOnHeightHistogramHeuristic(const SettingsRegionGrowingOnHistogram & settings) {
	_sharedInformation.Method = LasModel::MethodShared::RegionGrowingBasedOnHistogram;
	for (MapInfo & segment : _map.ToView().PlainView())
		for (QuadVertex & v : segment)
			v.Color.a = 0;

	_sharedInformation.RGBH.State = MSRegionGrowingOnHistogram::CreatingHistogram;
	_sharedInformation.RGBH.TileProcessed = 0;
	_sharedInformation.RGBH.TileCount = (size_t)(
		ceil((_max.x - settings.HISTOGRAM_AREA.x) / settings.HISTOGRAM_SHIFT.x) *
		ceil((_max.z - settings.HISTOGRAM_AREA.y) / settings.HISTOGRAM_SHIFT.y)
		);

	float2 beg = settings.HISTOGRAM_SHIFT - settings.HISTOGRAM_AREA;
	for (float minX = 0, maxX = _max.x - settings.HISTOGRAM_AREA.x; minX < maxX; minX += settings.HISTOGRAM_SHIFT.x)
		for (float minZ = 0, maxZ = _max.z - settings.HISTOGRAM_AREA.y; minZ < maxZ; minZ += settings.HISTOGRAM_SHIFT.y) {
			++_sharedInformation.RGBH.TileProcessed;

			float2 minf(minX, minZ);
			float2 maxf = minf + settings.HISTOGRAM_AREA;
			Array<MapInfo *> segments = GetAABBSelectionSegements(minf, maxf);
			size_t count = 0;

			size_t emptySegments = 0;
			for (MapInfo * segment : segments) {
				size_t segmentCount = segment->Count();
				if (segmentCount)
					count += segmentCount;
				else
					++emptySegments;
			}

			if (emptySegments * 20 > segments.Length()) {
				//DD_LOG(_sharedInformation.RGBH.TileProcessed, "Empty region ", emptySegments, " : ", segments.Size());
				continue;
			}

			float scale = 1 / settings.HISTOGRAM_GRANULARITY;
			size_t max = (size_t)(scale * (_max.y - _min.y)) + 1;
			Array<unsigned> heightStatitics(max, 0);
			Array<double> heightPercentage(max, 0);

			// Create statistic
			for (MapInfo * segment : segments)  for (QuadVertex & v : *segment) {
				size_t height = (size_t)(v.Position.y * scale);
				if (height < max) {
					++heightStatitics[height];
				}
			}

			size_t min = 0;
			while (!heightStatitics[min]) ++min;
			while (!heightStatitics[--max]);

			// Create percentage
			for (size_t i = min; i < max; ++i) {
				heightPercentage[i] = 100.0 * heightStatitics[i] / count;
			}


			const size_t range = (max - min);
			const size_t skip = (size_t)(range * settings.HISTOGRAM_HEIGHT_SKIP);
			const size_t ctr = min + skip;

			// Color points on RAM
			const double average = 100.0 / (max - min);
			const double abnormality = average * settings.ABOVE_AVERAGE_HISTOGRAM;
			for (MapInfo * segment : segments)  for (QuadVertex & v : *segment) {
				if (v.Color.a == 2) continue;
				size_t height = (size_t)(v.Position.y * scale);
				if (height < max && skip < height) {
					const double & percentage = heightPercentage[height];
					if (percentage < abnormality) {
						v.Color.a = 2;
					}
				}
			}
		}
	_pointModel.Reinit();
}


void LasModel::growRegionHistogram(const SettingsRegionGrowingOnHistogram & settings, MapInfo * init) {
	static size_t REGION_NUMBER = 0;
	unsigned char color = 3 + (REGION_NUMBER++ % 3);

	Region reg;
	List<PointSegmentRelation> pointStack(4096);

	for (QuadVertex & v : *init) {
		if (v.Color.a == 2) {
			pointStack.Add(PointSegmentRelation({ init, &v }));
			break;
		}
	}

	while (pointStack.Size()) {
		PointSegmentRelation item = pointStack.Last();
		pointStack.RemoveLast();
		reg.Points.Add(item.point);
		QuadVertex & point = *(item.point);
		for (MapInfo & neighbor : Sequencer::Drive(_map.ToView().Neighborhood(item.segment))) {
			for (QuadVertex & v : neighbor) {
				if (v.Color.a == 2) {
					//float2 diff = float2(point.Position.x, point.Position.z) - float2(v.Position.x, v.Position.z);
					float3 diff = point.Position - v.Position;
					float size2 = dot(diff, diff);
					//float size2 = diff.y;
					if (size2 < settings.NEAREST_NEIGHBOR_MAXIMAL_DISATNCE) {
						++_sharedInformation.RGBH.EvaluatedPoints;
						v.Color.a = color;
						pointStack.Add(PointSegmentRelation({ &neighbor, &v }));
					}
				}
			}
		}
	}

	if (reg.Points.Size() > settings.MINIMUM_POINTS && reg.Points.Size() < settings.MAXIMUM_POINTS) {
		reg.RecalculateHull();
		reg.Icon = _iconModel[_iconModel.AddItem(Question, reg.IconPosition())];
		_regions.Add(reg);
		RegionAccepted(reg);
		++_sharedInformation.RGBH.RegionsAccepted;
	}
	++_sharedInformation.RGBH.RegionsCreated;
}

void LasModel::regionTracker(Engine & e, tracker_t track) {
	// foreach region, focus camera and do the action
	for (Region & region : _regions) {
		//e.GetCamera().Direction({ region.Hull.Center.x + 50, region.Hull.Height + 200, region.Hull.Center.y + 50 });
		e.GetCamera().Position({ region.Hull.Center.x, 0.f, region.Hull.Center.y });
		track(region);
	}
}
#pragma endregion








#pragma region Region
void Region::RecalculateHistogram() {
	Histogram = 0.f;
	for (QuadVertex * point : Points) {
		auto & c = point->Color;
		Histogram[0 + (c.r < 128)]++;
		Histogram[2 + (c.g < 128)]++;
		Histogram[4 + (c.b < 128)]++;
	}
	Histogram /= (float)Points.Size();
}

bool Region::IsEmpty() const {
	return Points.Size() == 0;
}

void Region::Merge(const Region & r) {
	Points.AddIterable(r.Points);
	Hull = ConvexHull2d(Points);
	Classify();
}

float Region::PercentageIn(const Region & r) {
	size_t count = 0;
	for (float2 f2 : r.Hull.Hull) {
		if (Hull.IsIn(f2)) {
			++count;
		}
	}
	return (float)count / r.Hull.Hull.Size();
}

void Region::Clear() {
	Points.Clear();
	Hull.Hull.Clear();
}

void Region::RecalculateHull() {
	RecalculateHistogram();
	Hull = ConvexHull2d(Points);
}

void Region::Classify(unsigned char c) {
	for (QuadVertex * q : Points) {
		q->Color.a = c;
	}
}

void Region::SplitLines(float longLine) {
	Hull.SplitLongLines(longLine);
}

void Region::ConcaveHull(const float2 & checkArea, float shiftSize, float staticSize, float dynamicSize, size_t deCasetljau) {
	// break long lines
	SplitLines(staticSize);
	// Iteration limit - not more than 250 meters.
	const size_t LIMIT = (size_t)(250 / shiftSize);

	// stick existing nodes
	for (size_t i = 0; i < Hull.Hull.Size(); ++i) {
		SegmentLine l = { Hull.Hull[i], Hull.Hull[(i + 1) % Hull.Hull.Size()] };
		float2 norm0 = Math::Normalize(l.b - l.a) * shiftSize;
		float2 norm(-norm0.y, norm0.x);
		float2 stick = l.a;
		size_t q = 0;
		--q;
		while (!Select(stick - checkArea, stick + checkArea) && ++q < LIMIT) {
			stick += norm;
		}
		if (q < LIMIT) {
			Hull.Hull[i] = stick;
		}
	}

	// add and stick nodes
	for (size_t i = 0; i < Hull.Hull.Size(); ++i) {
		SegmentLine l = { Hull.Hull[i], Hull.Hull[(i + 1) % Hull.Hull.Size()] };
		if (l.Size() > dynamicSize) {
			float2 norm0 = Math::Normalize(l.b - l.a);
			float2 center = l.a + (norm0 * 0.5 * dynamicSize);
			float2 norm(-norm0.y, norm0.x);
			norm *= shiftSize;
			size_t stickIteration = static_cast<size_t>(-1);
			while (!Select(center - checkArea, center + checkArea) && ++stickIteration < LIMIT) {
				center += norm;
			}
			if (stickIteration < LIMIT) {
				Hull.Hull.Insert(i + 1) = center;
			}
			else {
				Hull.Hull.Insert(i + 1) = l.a + norm0;
			}
			--i;
		}
		if (i > 50000) break;
		if (Hull.Hull.Size() > 100000) break;
	}

	const size_t l = Hull.Hull.Size();
	size_t splinec = 0;
	List<float2> spline[2];
	spline[0] = spline[1] = Hull.Hull;

	while (splinec < deCasetljau) {
		List<float2> & reader = spline[splinec % 2];
		List<float2> & store = spline[++splinec % 2];
		for (size_t i = 0; i < l; ++i) {
			store[i] = (reader[i] + reader[(i + 1) % l]) * 0.5f;
		}
	}
	Hull.Hull = spline[splinec % 2];
}

bool Region::Select(const float2 & min, const float2 & max) const {
	for (QuadVertex * v : Points) {
		if (v->Position.x < min.x) continue;
		if (v->Position.z < min.y) continue;
		if (v->Position.x > max.x) continue;
		if (v->Position.z > max.y) continue;
		return true;
	}
	return false;
}

Reference<TriangleModel<Simulation::ColorVertex>> Region::CreateHullModel() const {
	auto tm = new TriangleModel<Simulation::ColorVertex>();
	tm->AddPolyhedron(0.f, Hull.Height, Hull.Hull);
	return tm;
}

bool Region::IsGreen() const {
	size_t count = 0;
	for (QuadVertex * v : Points) {
		if (v->Color.ToHSV().IsGreen()) {
			++count;
		}
	}
	float ratio = (float)count / Points.Size();
	return ratio > 0.5f;
}
#pragma endregion

#pragma region ConvexHull2d

ConvexHull2d::ConvexHull2d(List<QuadVertex*> & input) {
	if (input.Size() == 0) return;

	size_t n = input.Size(), k = 0;
	Hull.Resize(2 * n);

	// Sort points lexicographically
	std::sort(input.begin(), input.end(), Float2ComparerLexi());

	// Build lower hull
	for (int i = 0; i < n; ++i) {
		float2 f2 = toFloat2(input[i]->Position);
		while (k >= 2 && cross(Hull[k - 2], Hull[k - 1], f2) <= 0) k--;
		Hull[k++] = f2;
	}

	// Build upper hull
	for (int i = (int)(n - 2), t = (int)(k + 1); i >= 0; i--) {
		float2 f2 = toFloat2(input[i]->Position);
		while (k >= t && cross(Hull[k - 2], Hull[k - 1], f2) <= 0) k--;
		Hull[k++] = f2;
	}

	// compute helpers
	std::sort(input.begin(), input.end(), Float2ComparerHeight());
	Height = input[input.Size() / 2]->Position.y;

	Hull.Resize(k - 1);

	Min.x = FLT_MAX;
	Min.y = FLT_MAX;
	Max.x = -FLT_MAX;
	Max.y = -FLT_MAX;

	for (float2 & f : Hull) {
		if (f.x < Min.x) Min.x = f.x;
		if (f.y < Min.y) Min.y = f.y;
		if (f.x > Max.x) Max.x = f.x;
		if (f.y > Max.y) Max.y = f.y;
	}
	Surface = (Max.x - Min.x) * (Max.y - Min.y);
	Center = (Min + Max) * 0.5f;
}

inline bool ConvexHull2d::IsIn(const float2 & a) {
	for (size_t i = 0, l = Hull.Size() - 1; i < l; ++i) {
		if (check(Hull[i + 0], a, Hull[i + 1])) {
			return false;
		}
	}
	if (check(Hull.Last(), a, Hull.First())) {
		return false;
	}
	return true;
}

inline void ConvexHull2d::SplitLongLines(float longLineDefinition) {
	for (size_t i = 0; i < Hull.Size(); ++i) {
		SegmentLine l = { Hull[i], Hull[(i + 1) % Hull.Size()] };
		if (l.Size() > longLineDefinition) {
			Hull.Insert(i + 1) = (l.a + l.b) * 0.5f;
			--i;
		}
	}
}

inline bool ConvexHull2d::check(const float2 & a, const float2 & b, const float2 & c) {
	float2 d1 = b - a;
	float2 d2 = c - b;
	float q = (d1.x * d2.y) - (d1.y * d2.x);
	return q >= 0.f;
}
#pragma endregion