#include <DD/Simulation/ModelGroup.h>

using namespace DD;
using namespace DD::Simulation;

void ModelGroup::iModelInit(Gpu::Device & device) {
	_device = device;
	for (auto & m : _models)
		m->Init(_device);
}


void ModelGroup::iModelDraw(DrawArgs & args) {
	for (size_t i = 0, m = _models.Size(); i < m; ++i) {
		_models[i]->Draw(args);
	}
}


void ModelGroup::iModelDrawCulled(
	const Frustum3F & f, const float4x4 & transform, DrawArgs & args
) {
	for (size_t i = 0, m = _models.Size(); i < m; ++i) {
		if (_models[i]->Intersects(f, transform)) {
			_models[i]->Draw(args);
		}
	}
}


void ModelGroup::Add(model_r model) {
	if (_device)
		model->Init(_device);

	_models.Add(model);
}


ModelGroup::ModelGroup()
	: _enabled(true)
	, _models()
	, _device()
{}


void ModelGroup::Clear() {
	for (auto & model : _models)
		model.Release();

	_models.Clear();
}


void IntersectableModelGroup::iModelInit(Gpu::Device & device) {
	// reset box
	_boundingBox = AABox3F();

	// parse box from initialized models
	for (model_r & init : _models) {
		_boundingBox.Include(init->GetBoundingBox());
	}

	// init models and parse box
	for (model_r & init : _init) {
		init->Init(device);
		_boundingBox.Include(init->GetBoundingBox());
		_models.Add(init);
	}

	// compute sphere from box
	_boundingSphere = _boundingBox.BoundingSphere();
	_init.Clear();
}


void IntersectableModelGroup::iModelDraw(DrawArgs & args) {
	if (_reinit) {
		for (model_r & m : _models)
			m->Reinit(_device);
		_reinit = false;
	}
	if (_init.IsNotEmpty())
		iModelInit(_device);

	for (model_r & model : _models) {
		model->Draw(args);
	}
}


void IntersectableModelGroup::iModelDrawCulled(
	const Frustum3F & f, const float4x4 & t, DrawArgs & args
) {
	if (_reinit) {
		for (model_r & m : _models)
			m->Reinit(_device);
		_reinit = false;
	}

	if (_init.IsNotEmpty())
		iModelInit(_device);

	switch (Intersects(f, t)) {
	case ALL_IN:
		for (model_r & model : _models) {
			model->Draw(args);
		}
		break;
	case PART_IN:
		for (model_r & model : _models) {
			model->DrawFrustumCulled(f, t, args);
		}
		break;
	case NONE:
		break;
	}
}


void IntersectableModelGroup::Add(model_r model, bool initialized) {
	if (model == nullptr)
		return;

	if (initialized) {
		// add directly to models
		_models.Add(model);
		// update volumes
		_boundingBox.Include(model->GetBoundingBox());
		_boundingSphere = _boundingBox.BoundingSphere();
	}
	else {
		_init.Add(model);
	}
}


void IntersectableModelGroup::iModelReinit(Gpu::Device &) {
	_reinit = true;
}


bool IntersectableModelGroup::IsEmpty() const {
	return _init.IsEmpty() && _models.IsEmpty();
}


void IntersectableModelGroup::Reinit() {
	_reinit = true;
}


void IntersectableModelGroup::SetEnabled(bool value) {
	_enabled = value;
}


IntersectableModelGroup::IntersectableModelGroup()
	: _enabled(1)
	, _reinit(false)
{
	_boundingSphere.Radius = 0;
}
