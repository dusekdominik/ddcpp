#include <DD/Controls/Console.h>

using namespace DD;
using namespace DD::Controls;


void ConsoleBase::WriteLine(const String & str) {
	_section.Enter();
	Units::Time_MilliSecondULL ms = _watch.ElapsedMilliseconds();
	//_consoleTable->Rows().Insert(0).Text(
	_consoleTable->Rows().Add(
		StringLocal<8>::From(_counter++),
		StringLocal<8>::Format("{0}.{1,3}", ms.Value / 1000, ms.Value % 1000),
		str
	);
	//_consoleTable->Rows().Last().Show();
	_section.Leave();
}


ConsoleBase::ConsoleBase(const String & name, i32 alloc)
	: _consoleWindow(WindowTypes::Basic, name)
	, _consoleTable(ListViewTypes::Basic)
	, _counter(0)
{
	_consoleWindow->Add(_consoleTable);
	_consoleWindow->Init();
	_consoleTable->Cols().Add(Text::StringView16(L"Id"), Text::StringView16(L"TimeStamp"), Text::StringView16(L"Message"));
	_consoleTable->Cols()[0].WidthPixel(50);
	_consoleTable->Cols()[1].WidthPixel(60);
	_consoleTable->Cols()[2].WidthPartition(1);
	_consoleTable->Cols()[0].Align(IControl::TAlign::Right);;
	_consoleTable->Cols()[1].Align(IControl::TAlign::Right);;
	_consoleTable->Cols()[2].Align(IControl::TAlign::Left);;
	_consoleTable->ResizeBuffer(alloc);
	_watch.Start();
}
