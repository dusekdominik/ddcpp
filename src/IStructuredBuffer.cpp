#include <DD/Gpu/Devices.h>
#include <DD/Gpu/IStructerdBuffer.h>

using namespace DD;
using namespace DD::Gpu;

void IStructuredBuffer::Init(Device & device, u32 bytes, u32 stride, const void * source) {
	_device = device;
	_device->StructuredBufferInit(*this, bytes, stride, source);
}

void IStructuredBuffer::Bind2VS(u32 slot) {
	_device->StructuredBufferBind2VS(*this, slot);
}

void IStructuredBuffer::Release() {
	if (_device) {
		_device->StructuredBufferRelease(*this);
		_device = nullptr;
	}
}

void IStructuredBuffer::Bind2CS(u32 slot) {
	_device->StructuredBufferBind2CS(*this, slot);
}

void IRWStructuredBuffer::Init(Device & device, u32 bytes, u32 stride, const void * source) {
	_device = device;
	_device->RWStructuredBufferInit(*this, bytes, stride, source);
}

void IRWStructuredBuffer::Bind2CS(u32 slot) {
	_device->RWStructuredBufferBind2CS(*this, slot);
}

void IRWStructuredBuffer::Read(void * data, u32 size) {
	_device->RWStructuredBufferRead(*this, data, size);
}

void IRWStructuredBuffer::Release() {

}
