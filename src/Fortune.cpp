#include <DD/Algo/Fortune.h>
#include <DD/HeapLib.h>

using namespace DD;
using namespace DD::Algo;
using namespace DD::Collections;

#define CIRCLE_CENTER_EPSILON 1.0e-7
#define POINT_EPSILON 1.0e-6
#define BREAKPOINTS_EPSILON 1.0e-5
#define INIFINITY 1E30


#pragma region AVLNode
FortuneAVLNode * FortuneAVLNode::leftRotation() {
	if (RightChild == nullptr)
		return this;

	FortuneAVLNode * r = RightChild;
	if (!IsRoot())
		Parent->SetChild(IsLeft(), r);

	r->Parent = Parent;
	RightChild = r->LeftChild;
	if (r->LeftChild != nullptr)
		r->LeftChild->Parent = this;

	r->LeftChild = this;
	Parent = r;

	updateHeight();
	r->updateHeight();
	if (r->Parent)
		r->Parent->updateHeight();

	return r;
}


FortuneAVLNode * FortuneAVLNode::rightRotation() {
	if (LeftChild == nullptr)
		return this;

	FortuneAVLNode * l = LeftChild;
	if (!IsRoot())
			Parent->SetChild(IsLeft(), l);

	l->Parent = Parent;
	LeftChild = l->RightChild;
	if (l->RightChild != nullptr)
		l->RightChild->Parent = this;

	l->RightChild = this;
	Parent = l;

	updateHeight();
	l->updateHeight();
	if (l->Parent)
		l->Parent->updateHeight();

	return l;
}

FortuneAVLNode * FortuneAVLNode::BalanceNode() {
	updateHeight();
	int balance = getBalance();
	if (balance > 1) {
		if (LeftChild != nullptr && !LeftChild->IsLeaf() && LeftChild->getBalance() < 0)
			LeftChild = LeftChild->leftRotation();

		return rightRotation();
	}

	if (balance < -1) {
		if (RightChild != nullptr && !RightChild->IsLeaf() && RightChild->getBalance() > 0)
			RightChild = RightChild->rightRotation();

		return leftRotation();
	}

	return this;
}

FortuneAVLNode * FortuneAVLNode::BalanceTree() {
	return 0;
}


FortuneAVLNode * FortuneAVLNode::RemoveLeaf() {
	DD_FORTUNE_ASSERT(IsLeaf());
	FortuneAVLNode * p = Parent;
	DD_FORTUNE_ASSERT(p != nullptr);
	FortuneAVLNode * gp = p->Parent;
	DD_FORTUNE_ASSERT(gp != nullptr);

	Collections::Bin<i32, i32> leftBP(PrevArc->PointIndices[0], PointIndices[0]);
	Collections::Bin<i32, i32> rightBP(PointIndices[0], NextArc->PointIndices[0]);
	Collections::Bin<i32, i32> otherBP = p->PointIndices == leftBP ? rightBP : leftBP;
	DD_FORTUNE_ASSERT(p->PointIndices == leftBP || p->PointIndices == rightBP);

	FortuneAVLNode * sibling = GetSibling();
	sibling->Parent = gp;
	gp->SetChild(p->IsLeft(), sibling);

	FortuneAVLNode * newRoot = gp;
	for (FortuneAVLNode * node = gp; node != nullptr; node = node->Parent) {
		// replace breakpoint with new one, two breakpoints coincided, should be represented by one
		if (node->PointIndices == otherBP)
			node->PointIndices = { PrevArc->PointIndices[0], NextArc->PointIndices[0] };

		newRoot = node = node->BalanceNode();
	}

	FortuneAVLNode::ConnectLeaves(PrevArc, NextArc);

	return newRoot;
}


void FortuneAVLNode::updateHeight() {
	TreeHeight = Max(LeftChild ? LeftChild->TreeHeight : 0, RightChild ? RightChild->TreeHeight : 0) + 1;
}


int FortuneAVLNode::getBalance() const {
	return (LeftChild ? LeftChild->TreeHeight : 0) - (RightChild ? RightChild->TreeHeight : 0);
}
#pragma endregion


#pragma region Event
FortuneEvent::Breakpoints FortuneEvent::ComputeBreakpoints() {
	if (ArcNode == nullptr || ArcNode->NextArc == nullptr || ArcNode->PrevArc == nullptr)
		return { nullptr, nullptr };

	Collections::Bin<i32, i32> leftBP(ArcNode->PrevArc->PointIndices[0], ArcNode->PointIndices[0]);
	Collections::Bin<i32, i32> rightBP(ArcNode->PointIndices[0], ArcNode->NextArc->PointIndices[0]);
	Collections::Bin<i32, i32> otherBP;

	bool left_is_missing = true;

	if (ArcNode->Parent->PointIndices == leftBP) {
		otherBP = rightBP;
		left_is_missing = false;
	}
	else if (ArcNode->Parent->PointIndices == rightBP) {
		otherBP = leftBP;
		left_is_missing = true;
	}

	// Go up and rebalance the whole tree
	FortuneAVLNode * gparent = ArcNode->Parent;
	while (gparent != nullptr && gparent->PointIndices != otherBP)
		gparent = gparent->Parent;

	return left_is_missing ? Breakpoints(gparent, ArcNode->Parent) : Breakpoints(ArcNode->Parent, gparent);
}

#pragma endregion


#pragma region Fortune
FortuneAVLNode * Fortune::findLeaf(FortuneAVLNode * root, f64 x) {
	if (root == nullptr)
		return nullptr;

	FortuneAVLNode * node = root;
	while (!node->IsLeaf())
		node = (computeNodeXValue(node) < x) ? node->RightChild : node->LeftChild;

	return node;
}


FortuneAVLNode * Fortune::replaceNode(FortuneAVLNode * oldNode, FortuneAVLNode * newNode) {
	if (oldNode == nullptr)
		return newNode;

	f64 x = computeNodeXValue(newNode);
	FortuneAVLNode * parent = oldNode->Parent;

	newNode->Parent = parent;
	if (parent != nullptr)
		parent->SetChild(x <= computeNodeXValue(parent), newNode);

	// balance
	FortuneAVLNode* newRoot = newNode;
	for (FortuneAVLNode * node = parent; node != nullptr; node = node->Parent)
		newRoot = node = node->BalanceNode();

	return newRoot;
}


void Fortune::createTwoNodeSubtree(i32 index1, i32 index0, FortuneAVLNode *& rnode, FortuneAVLNode *& lleaf, FortuneAVLNode *& rleaf) {
	rnode = new (_nodeAllocator.AppendAllocation()) FortuneAVLNode{ .PointIndices = { index0, index1 }, .TreeHeight = 3};
	FortuneAVLNode * node2 = new (_nodeAllocator.AppendAllocation()) FortuneAVLNode{ .PointIndices = { index1, index0 }, .TreeHeight = 2};
	lleaf = new (_nodeAllocator.AppendAllocation()) FortuneAVLNode{ .PointIndices = { index0, index0 }, .Parent = rnode };
	FortuneAVLNode * leaf2 = new (_nodeAllocator.AppendAllocation()) FortuneAVLNode{ .PointIndices = { index1, index1 }, .Parent = node2 };
	rleaf = new (_nodeAllocator.AppendAllocation()) FortuneAVLNode{ .PointIndices = { index0, index0 }, .Parent = node2 };

	rnode->RightChild = node2;
	rnode->LeftChild = lleaf;
	node2->Parent = rnode;
	node2->LeftChild = leaf2;
	node2->RightChild = rleaf;

	createEdgePair(index0, index1).Unpack(rnode->VoronoiEdge, node2->VoronoiEdge);
	FortuneAVLNode::ConnectLeaves(lleaf, leaf2);
	FortuneAVLNode::ConnectLeaves(leaf2, rleaf);
}


void Fortune::createOneNodeSubtree(i32 index0, i32 index1, FortuneAVLNode *& rnode, FortuneAVLNode *& lleaf, FortuneAVLNode *& rleaf) {
	// order wrt x
	if (Points[index0].x < Points[index1].x)
		Swap(index0, index1);

	FortuneEdge::Pair edgePair = createEdgePair(index0, index1);
	rnode = new (_nodeAllocator.AppendAllocation()) FortuneAVLNode{ .PointIndices = { index0, index1 }, .TreeHeight = 2, .VoronoiEdge = edgePair[0], };
	lleaf = rnode->LeftChild = new (_nodeAllocator.AppendAllocation()) FortuneAVLNode{ .PointIndices = { index0, index0 }, .Parent = rnode };
	rleaf = rnode->RightChild = new (_nodeAllocator.AppendAllocation()) FortuneAVLNode{ .PointIndices = { index1, index1 }, .Parent = rnode };

	FortuneAVLNode::ConnectLeaves(rnode->LeftChild, rnode->RightChild);
}


f64 Fortune::computeNodeXValue(FortuneAVLNode * node) {
	if (node->IsLeaf())
		return Points[node->PointIndices[0]].x;

	// foci
	const double2 & f1 = Points[node->PointIndices[0]];
	const double2 & f2 = Points[node->PointIndices[1]];
	const double2 d = f1 - f2;

	if (Math::Abs(d.x) < POINT_EPSILON) {
		f64 D = Math::Sqrt(SweeplineY * SweeplineY - SweeplineY * (f1.y + f2.y) + f1.y * f2.y);
		return f1.y < f2.y ? f1.x - D : f1.x + D;
	}

	if (Math::Abs(d.y) < POINT_EPSILON)
		return  0.5 * (f1.x + f2.x);

	const double2 sum = f1 + f2;
	const double2 d2 = d * d;
	const f64 dot = (d2.x + d2.y);
	const f64 x1_2 = f1.x * f1.x;
	const f64 x2_2 = f2.x * f2.x;
	const f64 D = Math::Sqrt(d2.x * (SweeplineY - f1.y) * (SweeplineY - f2.y) * dot);
	const f64 T = 0.5 * sum.y * dot - SweeplineY * d2.x;
	const f64 y1 = (T - D) / d2.y;
	const f64 y2 = (T + D) / d2.y;
	f64 x1 = 0.5 * (x1_2 - x2_2 - (2 * y1 - sum.y) * d.y) / d.x;
	f64 x2 = 0.5 * (x1_2 - x2_2 - (2 * y2 - sum.y) * d.y) / d.x;

	if (x2 < x1)
		Swap(x1, x2);

	return f1.y < f2.y ? x1 : x2;
}


FortuneEdge::Pair Fortune::createEdgePair(int leftIndex, int rightIndex) {
	FortuneEdge * a = new (_edgeAllocator.AppendAllocation()) FortuneEdge{ leftIndex, rightIndex };
	FortuneEdge * b = new (_edgeAllocator.AppendAllocation()) FortuneEdge{ rightIndex, leftIndex };
	a->Mirror = b;
	b->Mirror = a;

	return { a, b };
}


void Fortune::checkCircleEvent(FortuneAVLNode * n1, FortuneAVLNode * n2, FortuneAVLNode * n3) {
	if (n1 == nullptr || n2 == nullptr || n3 == nullptr)
		return;

	const double2 & p1 = Points[n1->PointIndices[0]];
	const double2 & p2 = Points[n2->PointIndices[0]];
	const double2 & p3 = Points[n3->PointIndices[0]];
	if (p2.y > p1.y && p2.y > p3.y)
		return;

	Math::Circle2D circle;
	if (!Math::Circle2D::Circumscribed(p1, p2, p3, circle))
		return;

	// check circle event
	const f64 y = circle.Center.y + circle.Radius;
	if (Math::Abs(y - SweeplineY) < POINT_EPSILON || SweeplineY < y) {
		FortuneEvent * e = new (_eventAllocator.AppendAllocation()) FortuneEvent{
			.EventType = FortuneEvent::EventTypes::CircleEvent,
			.Point = { circle.Center.x, y },
			.Circle = circle,
			.ArcNode = n2,
			.Index = -1,
		};

		n2->CircleEvent = e;
		_eventHeap.Push(e);
	}
}


void Fortune::processSiteEvent(FortuneEvent * event) {
	DD_FORTUNE_ASSERT(event->EventType == FortuneEvent::EventTypes::SiteEvent);

	if (Root == nullptr) {
		Root = new (_nodeAllocator.AppendAllocation()) FortuneAVLNode{ .PointIndices = { event->Index, event->Index }, };
		return;
	}

	FortuneAVLNode * arcNode = findLeaf(Root, event->Point.x);
	if (arcNode->CircleEvent != nullptr)
		arcNode->CircleEvent->EventType = FortuneEvent::EventTypes::SkipEvent;

	FortuneAVLNode * subtree;
	FortuneAVLNode * lLeaf;
	FortuneAVLNode * rLeaf;
	const double2 & f1 = Points[arcNode->PointIndices[0]];
	const double2 & f2 = event->Point;
	if (Math::Abs(f1.y - f2.y) < POINT_EPSILON) {
		// no intersection point
		if (Math::Abs(f1.x - f2.x) < POINT_EPSILON)
			return;

		// single intersection point
		createOneNodeSubtree(event->Index, arcNode->PointIndices[0], subtree, lLeaf, rLeaf);
	}
	// two intersection points
	else {
		createTwoNodeSubtree(event->Index, arcNode->PointIndices[0], subtree, lLeaf, rLeaf);
	}

	if (arcNode->PrevArc != nullptr)
		FortuneAVLNode::ConnectLeaves(arcNode->PrevArc, lLeaf);

	if (arcNode->NextArc != nullptr)
		FortuneAVLNode::ConnectLeaves(rLeaf, arcNode->NextArc);

	// Replace old leaf with a subtree and rebalance it
	Root = replaceNode(arcNode, subtree);

	// Check circle events
	checkCircleEvent(lLeaf->PrevArc, lLeaf, lLeaf->NextArc);
	checkCircleEvent(rLeaf->PrevArc, rLeaf, rLeaf->NextArc);
}


void Fortune::processCircleEvent(FortuneEvent * event) {
	DD_FORTUNE_ASSERT(event->EventType == FortuneEvent::EventTypes::CircleEvent);

	// get breakpoint nodes
	FortuneAVLNode * breakPoint0;
	FortuneAVLNode * breakPoint1;
	event->ComputeBreakpoints().Unpack(breakPoint0, breakPoint1);

	if (breakPoint0 == nullptr || breakPoint1 == nullptr)
		return;

	// recheck if it's a false alarm 2
	f64 v1 = computeNodeXValue(breakPoint0);
	f64 v2 = computeNodeXValue(breakPoint1);
	if (BREAKPOINTS_EPSILON < Math::Abs(v1 - v2))
		return;

	// create a new vertex and insert into doubly-connected edge list
	FortuneVertex * vertex = new (_vertexAllocator.AppendAllocation()) FortuneVertex(event->Circle.Center);
	FortuneEdge * first = breakPoint0->VoronoiEdge;
	FortuneEdge * second = breakPoint1->VoronoiEdge;

	// remove circle event corresponding to next leaf
	if (event->ArcNode->PrevArc != nullptr && event->ArcNode->PrevArc->CircleEvent != nullptr)
		event->ArcNode->PrevArc->CircleEvent->EventType = FortuneEvent::EventTypes::SkipEvent;

	// remove circle event corresponding to prev leaf
	if (event->ArcNode->NextArc != nullptr && event->ArcNode->NextArc->CircleEvent != nullptr)
		event->ArcNode->NextArc->CircleEvent->EventType = FortuneEvent::EventTypes::SkipEvent;

	// store pointers to the next and previous leaves
	FortuneAVLNode * prevLeaf = event->ArcNode->PrevArc;
	FortuneAVLNode * nextLeaf = event->ArcNode->NextArc;

	// They should not be null
	DD_FORTUNE_ASSERT(event->ArcNode->PrevArc != nullptr);
	DD_FORTUNE_ASSERT(event->ArcNode->NextArc != nullptr);

	// get node associated with a new edge
	FortuneAVLNode * newEdgeNode = event->ArcNode->Parent == breakPoint0 ? breakPoint1 : breakPoint0;
	// remove circle event's arc
	Root = event->ArcNode->RemoveLeaf();

	FortuneEdge * twin_nodes0;
	FortuneEdge * twin_nodes1;
	createEdgePair(prevLeaf->PointIndices[0], nextLeaf->PointIndices[0]).Unpack(twin_nodes0, twin_nodes1);
	newEdgeNode->VoronoiEdge = twin_nodes0;

	FortuneEdge::Connect(second, first->Mirror);
	FortuneEdge::Connect(first, twin_nodes0);
	FortuneEdge::Connect(twin_nodes1, second->Mirror);

	// edges pointing to the vertex, counterclockwise
	first->VoronoiVertex = vertex;
	second->VoronoiVertex = vertex;
	twin_nodes1->VoronoiVertex = vertex;
	vertex->VoronoiEdge = second;

	// check new circle events
	if (prevLeaf != nullptr && nextLeaf != nullptr) {
		checkCircleEvent(prevLeaf->PrevArc, prevLeaf, nextLeaf);
		checkCircleEvent(prevLeaf, nextLeaf, nextLeaf->NextArc);
	}
}


void Fortune::Process() {
	// initialize point events
	Faces.Resize(Points.Length()).Fill(nullptr);

	// put all events to heap list
	_eventHeap.Storage.Reserve(Points.Length() * 2);
	for (size_t i = 0; i < Points.Length(); ++i)
		_eventHeap.Storage.Append() = new (_eventAllocator.AppendAllocation()) FortuneEvent{ .EventType = FortuneEvent::EventTypes::SiteEvent, .Point = Points[i], .Index = (int)i };

	// sort
	_eventHeap.Heapify();

	// process
	for (FortuneEvent * event = nullptr; _eventHeap.TryPop(event);) {
		// set position of a sweepline
		SweeplineY = event->Point.y;

		switch (event->EventType) {
		case FortuneEvent::EventTypes::SiteEvent:
			processSiteEvent(event);
			break;
		case FortuneEvent::EventTypes::CircleEvent:
			processCircleEvent(event);
			break;
		case FortuneEvent::EventTypes::SkipEvent:
			break;
		default:
			DD_FORTUNE_WARNING("Undefined case");
			break;
		}
	}

	// init faces
	for (FortuneEdge & halfEdge : _edgeAllocator)
		if (halfEdge.PrevEdge == nullptr || Faces[halfEdge.PointIndex0] == nullptr)
			Faces[halfEdge.PointIndex0] = &halfEdge;
}


Array<Math::LineSegment2D> Fortune::GetLines() const {
	Array<Math::LineSegment2D> result(_edgeAllocator.Size());

	for (size_t i = 0; i < _edgeAllocator.Size(); ++i) {
		// source
		const FortuneEdge * edge = _edgeAllocator.At(i);
		const double2 & p1 = Points[edge->PointIndex0];
		const double2 & p2 = Points[edge->PointIndex1];
		// target
		Math::LineSegment2D & line = result[i];

		switch (edge->VertexMode()) {
		case FortuneEdge::VertexModes::None: {
				const double2 dir = Normal2Dir(p1 - p2, DefaultScale);
				const double2 center = (p1 + p2) * 0.5;
				line = { center + dir, center - dir };
			} break;
			case FortuneEdge::VertexModes::Vertex0:
				line = { edge->VoronoiVertex0(), edge->VoronoiVertex0() + Normal2Dir(p1 - p2, DefaultScale) };
				break;
			case FortuneEdge::VertexModes::Vertex1:
				line = { edge->VoronoiVertex1(), edge->VoronoiVertex1() + Normal2Dir(p2 - p1, DefaultScale) };
				break;
			case FortuneEdge::VertexModes::Vertex01:
				line = { edge->VoronoiVertex0(), edge->VoronoiVertex1() };
				break;
		}
	}

	return result;
}


/** BBox Clip, */
struct Clipper {
	/** Do we clip by minimal value or maximal value. */
	enum Modes { Minimum, Maximum };
	/** What coord is being clipped. */
	enum Coords { X, Y };
	/** Clip hash. */
	enum ClipFlags { None = 0, XMinimum = 1, XMaximum = 2, YMinimum = 4, YMaximum = 8 };

	template<Modes MODE, Coords COORD>
	static bool IsValid(const double2 & point, const double2 & ref)  {
		if constexpr (MODE == Modes::Minimum)
			return ref[COORD] <= point[COORD];
		else
			return point[COORD] <= ref[COORD];
	}
	template<Modes MODE, Coords COORD>
	static void PClip(LazyList<double3, 16>& polygon, const double2 & ref) {
		size_t clipBegin = 0;
		size_t clipEnd = 0;
		for (size_t i = 0; i < polygon.Count(); ++i) {
			size_t j = (i + 1) % polygon.Count();
			double3 & p = polygon[i];
			double3 & q = polygon[j];

			if (IsValid<MODE, COORD>(p, ref) && !IsValid<MODE, COORD>(q, ref))
				clipBegin = i;

			if (!IsValid<MODE, COORD>(p, ref) && IsValid<MODE, COORD>(q, ref))
				clipEnd = j;
		}

		if (clipEnd != clipBegin) {
			if (clipEnd != 0) {
				polygon.RotateLeft(clipEnd);
				clipBegin = (clipBegin - clipEnd + polygon.Count()) % polygon.Count();
				clipEnd = 0;
			}

			double2 beginPoint = Math::LineSegment2D(polygon[clipBegin], polygon[clipBegin + 1]).ToRay().AtCoord(ref[COORD], COORD);
			double2 endPoint = Math::LineSegment2D(polygon.Last(), polygon.First()).ToRay().AtCoord(ref[COORD], COORD);
			polygon.RemoveSince(clipBegin + 1);
			polygon.Append() = beginPoint;
			polygon.Append() = endPoint;
		}
	}
	static void PolygonClip(LazyList<double3, 16> & polygon, const Math::Interval2D & frame) {
		ClipFlags hash = None;
		for (const double3 & point : polygon)
			hash = (ClipFlags)(hash | frame.ClipHash<ClipFlags>(point.xy));

		if (hash) {
			if (hash & ClipFlags::XMinimum)
				PClip<Modes::Minimum, Coords::X>(polygon, frame.Min);
			if (hash & ClipFlags::XMaximum)
				PClip<Modes::Maximum, Coords::X>(polygon, frame.Max);
			if (hash & ClipFlags::YMinimum)
				PClip<Modes::Minimum, Coords::Y>(polygon, frame.Min);
			if (hash & ClipFlags::YMaximum)
				PClip<Modes::Maximum, Coords::Y>(polygon, frame.Max);
		}
	}
};


Array<Array<double2>> Fortune::GetPolygons(Math::Interval2D frame) const {
	// accumulators
	Array<LazyList<double3, 16>> polygons(Points.Length());
	Array<Array<double2>> result(Points.Length());

	// drop all vertices
	for (const FortuneEdge & e : _edgeAllocator) {
		const double2 & p1 = Points[e.PointIndex0];
		const double2 & p2 = Points[e.PointIndex1];
		switch (e.VertexMode()) {
		case FortuneEdge::VertexModes::Vertex0:
			polygons[e.PointIndex0].Append().xy = e.VoronoiVertex0();
			polygons[e.PointIndex0].Append().xy = e.VoronoiVertex0() - Normal2Dir(p2 - p1, DefaultScale);
			break;
		case FortuneEdge::VertexModes::Vertex01:
			polygons[e.PointIndex0].Append().xy = e.VoronoiVertex0();
			break;
		case FortuneEdge::VertexModes::Vertex1:
			polygons[e.PointIndex0].Append().xy = e.VoronoiVertex1();
			polygons[e.PointIndex0].Append().xy = e.VoronoiVertex1() + Normal2Dir(p2 - p1, DefaultScale);
			break;
		case FortuneEdge::VertexModes::None:
			polygons[e.PointIndex0].Append().xy = (p1 + p2) * 0.5 + Normal2Dir(p2 - p1, DefaultScale);
			break;
		}
	}


	for (size_t i = 0; i < Points.Length(); ++i) {
		//if (i != 7)
		//	continue;
		// generating point of face
		const double2 & center = Points[i];
		auto & polygon = polygons[i];
		// compute angles
		for (double3 & point : polygon)
			point.z = Math::ArcTg2(point.y - center.y, point.x - center.x);


		// sort vertices wrt angle
		HeapLib::Sort(polygon.ToArray(), ZComparer());
		// clip within frame
		Clipper::PolygonClip(polygon, frame);

		// produce output
		auto & output = result[i].Reallocate(polygon.Count());
		for (size_t p = 0; p < output.Length(); ++p)
			output[p] = polygon[p].xy;

	}

	return result;
}
#pragma endregion
