#include <DD/Concurrency/ThreadPool.h>
#include <DD/Debug.h>

#include <Windows.h>
#include <thread>

#undef MemoryBarrier

using namespace DD;
using namespace DD::Concurrency;

u32 ThreadPool::CocurrentThreads() {
	return std::thread::hardware_concurrency();
}

ThreadPool & ThreadPool::Instance() {
	static ThreadPool _pool(L"GlobalPool");
	return _pool;
}

void ThreadPool::loop() {
	ITask * action;
	while (1) {
		// wait for work
		_section.Enter();
		while (!_queue.TryPop(action)) {
			_section.Sleep(_cvar);
		}
		_section.Leave();
		// work
		InterlockedIncrement(&_working);
		action->Run();
		InterlockedDecrement(&_working);
	}
}

void ThreadPool::WaitAll() {
	ITask * action;
	while (true) {
		if (_queue.TryPop(action)) {
			action->Run();
		}
		else if (_working) {
			Sleep(25);
		}
		else {
			return;
		}
	}
}

void ThreadPool::DetachAll() {
	_queue.Clear();
	for (auto & worker : _workers) {
		worker->Detach();
	}
	_working = 0;
}

void ThreadPool::TerminateAll() {
	for (auto & worker : _workers) {
		worker->Terminate();
	}
	DetachAll();
}

ThreadPool::ThreadPool(Text::StringView16 name, u32 threads)
	: _sleep(1)
	, _workers(threads ? threads : CocurrentThreads())
	, _working(0)
{
	size_t index = 0;
	for (auto & worker : _workers) {
		worker = Thread(
			[this]() { loop(); },
			StringLocal<256>::From(name, ' ', index++)
		);
	}
}

ThreadPool::~ThreadPool() {
	WaitAll();
	TerminateAll();
}

void ThreadPool::RegisterAction(ITask * task) {
	//DD_DEBUG_ASSERT(task);
	//DD_DEBUG_ASSERT(task->_routine);
	//DD_DEBUG_ASSERT(!task->HasFinished());
	_section.Enter();
	_queue.Push(task);
	_section.Leave();
	_cvar.WakeOne();
}

/************************************************************************/
/* ITask                                                                */
/************************************************************************/
void ThreadPool::ITask::Run() {
	if (Atomic::i32::Dec(_access) == 0) {
		_routine->Run();
		_priority = EXPIRED;
	}
}

void ThreadPool::ITask::Reset(IAction<> * routine, i32 priority, ThreadPool & pool) {
	_routine = routine;
	_priority = priority;
	_access = 1;
	Thread::MemoryBarrier();
	pool.RegisterAction(this);
}

void ThreadPool::ITask::Wait(u32 milliseconds) const {
	while (_priority != EXPIRED) {
		if (milliseconds)
			Thread::Sleep(milliseconds);
		Thread::MemoryBarrier();
	}
}

ThreadPool::ITask::ITask(IAction<> * routine, i32 priority, ThreadPool & pool)
	: ITask(routine, priority, 1)
{
	pool.RegisterAction(this);
}

ThreadPool::ITask::~ITask() {
	//DD_DEBUG_ASSERT(HasFinished());
}

ThreadPool::ITask::ITask(IAction<> * routine, i32 priority, i32 access)
	: _routine(routine)
	, _priority(priority)
	, _access(access)
{}

/************************************************************************/
/* AsyncBase                                                            */
/************************************************************************/
void ThreadPool::AsyncBase::addContinuation(AsyncBase * task) {
	auto proxy = _continuation.GetProxy();
	if (HasFinished())
		_pool.RegisterAction(task);
	else
		proxy->Add(task);
}

void ThreadPool::AsyncBase::registerContinuations() {
	auto proxy = _continuation.GetProxy();
	for (AsyncBase * async : *proxy) {
		_pool.RegisterAction(async);
	}
}

ThreadPool::AsyncBase::AsyncBase(IAction<>* routine, ThreadPool & pool, i32 priority, i32 access)
	: ITask(routine, priority, access)
	, _pool(pool)
	, _continuation()
{
}

