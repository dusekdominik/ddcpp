#include <DD/Gpu/PixelShader.h>
#include <DD/Gpu/Devices.h>
#include <d3d11.h>

using namespace DD;
using namespace DD::Gpu;

void PixelShader::Load(Device & device, const Text::StringView16 & name) {
	_device = device;
	_device->PixelShaderLoad(*this, name);
}

void PixelShader::Reload(Device & device) {
	this->~PixelShader();
	Load(device, _name);
}

void PixelShader::Set() {
	_device->PixelShaderSet(*this);
}

