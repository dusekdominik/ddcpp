#include <DD/Gpu/Device.h>
#include <DD/Gpu/Devices.h>
#include <DD/FileSystem/Path.h>
#include <DD/Gpu/Texture.h>
#include <DD/Debug.h>
#include <DD/Unique.h>

#include <d3d11.h>
#include <d3dcompiler.h>
#include <dxgi1_2.h>
#include <comdef.h>
#include "c:\Program Files (x86)\Microsoft DirectX SDK (June 2010)\Include\D3DX11tex.h"
#undef CreateDirectory

// Experimental implementation of dds textures and png screenshots
#define DD_TEXTURE_EXPERIMENT 0
#define DD_DEVICE_DEPTHBUFFER_TYPE DXGI_FORMAT_R32_TYPELESS

#if DD_TEXTURE_EXPERIMENT
#include "S:\C++\DDSTextureLoader.cpp"
#endif

#pragma comment(lib, "c:\\Program Files (x86)\\Microsoft DirectX SDK (June 2010)\\Lib\\x64\\D3DX11.lib")
#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "D3D11.lib")
#pragma comment(lib, "D3DCompiler.lib")

DD_TODO_STAMP("Implement Loading of textures independent on dx.");
DD_TODO_STAMP("Separate renderer into special lib.")
DD_TODO_STAMP("Separate context from device; draw adn bind is matter of ctx, buffer creation of device.")
DD_TODO_STAMP("DXDevice is leaking while window resizing.")

/** Winapi HRESULT validation call. */
#define HR_WARNING(hr,  whatFailed)                                                              \
	if (FAILED(hr))                                                                                \
		DD_WARNING(whatFailed " failed with ErrorCode = ", (u64)hr, ". ", _com_error(hr).ErrorMessage());
/** Winapi HRESULT validation call. */
#define HR(call, ...) {             \
	HRESULT hr = call(__VA_ARGS__); \
	HR_WARNING(hr, #call);          \
}

using namespace DD;
using namespace DD::Gpu;


template<typename T>
struct DXDeallocator {
	/** Deallocation of DX pointers. */
	static void Delete(T *& t) { t->Release(); }
};


/** Unique pointer for dx objects. */
template<typename DX>
struct DXUnique
	: public Unique<DX, DXDeallocator<DX>>
{
	using Base = Unique<DX, DXDeallocator<DX>>;
	DX ** operator&() { return Base::Address(); }
	DXUnique & operator=(const DXUnique & rhs) {
		Base::Release();
		if (Base::_ptr = rhs._ptr)
			Base::_ptr->AddRef();

		return *this;
	}
	DXUnique(DXUnique && rhs) : Base(rhs.Unlock()) { }
	DXUnique(DX * dxPtr = nullptr) : Base(dxPtr) {};
};


struct Devices::DXDevice {
	/** https://learn.microsoft.com/en-us/windows/win32/api/d3d11/nn-d3d11-id3d11device */
	DXUnique<ID3D11Device> _device;
	/** https://learn.microsoft.com/en-us/windows/win32/api/d3d11/nn-d3d11-id3d11devicecontext */
	DXUnique<ID3D11DeviceContext> _context;
	/** https://learn.microsoft.com/en-us/windows/win32/api/dxgi/nn-dxgi-idxgiswapchain */
	DXUnique<IDXGISwapChain> _swapChain;
  /**https://learn.microsoft.com/en-us/windows/win32/api/d3d11/nn-d3d11-id3d11texture2d */
	DXUnique<ID3D11Texture2D> _depthBufferTexture;
	DXUnique<ID3D11Texture2D> _rtvTexture;
	/** https://learn.microsoft.com/en-us/windows/win32/api/d3d11/nn-d3d11-id3d11depthstencilview */
	DXUnique<ID3D11DepthStencilView> _depthStencilView;
	/** https://learn.microsoft.com/en-us/windows/win32/api/d3d11/nn-d3d11-id3d11depthstencilstate */
	DXUnique<ID3D11DepthStencilState> _depthStencilState;
	DXUnique<ID3D11DepthStencilView> _effectDepthStencilView;
	/** https://learn.microsoft.com/en-us/windows/win32/api/d3d11/nn-d3d11-id3d11rendertargetview */
	DXUnique<ID3D11RenderTargetView> _rtv;
	DXUnique<ID3D11RenderTargetView> _effectRtv;
	/** https://learn.microsoft.com/en-us/windows/win32/api/d3d11/nn-d3d11-id3d11rasterizerstate */
	DXUnique<ID3D11RasterizerState> _rasterizer;
	DXUnique<ID3D11RasterizerState> _wireframeEnabled;
	DXUnique<ID3D11RasterizerState> _wireframeDisabled;
	/** https://learn.microsoft.com/en-us/windows/win32/api/d3d11/nn-d3d11-id3d11blendstate */
	DXUnique<ID3D11BlendState> _blendStateABEnabled;
	DXUnique<ID3D11BlendState> _blendStateABDisabled;
	/** https://learn.microsoft.com/en-us/previous-versions/windows/desktop/legacy/bb173064(v=vs.85) */
	Array<DXGI_MODE_DESC> _displayModeList;
	/** https://learn.microsoft.com/en-us/windows/win32/api/d3d11/ns-d3d11-d3d11_depth_stencil_desc */
	D3D11_DEPTH_STENCIL_DESC _depthStencilDesc;
	D3D11_TEXTURE2D_DESC _depthBufferDesc;
	D3D11_DEPTH_STENCIL_VIEW_DESC _depthStencilViewDesc;
	/** */
	Texture RenderTarget;

	void initDepthBuffer(UINT w, UINT h, UINT msaa);
};


struct Texture::DXModel {
	/** Texture. */
	DXUnique<ID3D11Texture2D> _resource;
	/** Handle for shaders. */
	DXUnique<ID3D11ShaderResourceView> _shaderResourceView;
	/** Handel for shaders */
	DXUnique<ID3D11RenderTargetView> _renderTargetView;
	/** Sampler settings. */
	DXUnique<ID3D11SamplerState> _samplerState;
};


struct IBuffer::DXModel {
	/** Buffer holder. */
	DXUnique<ID3D11Buffer> Buffer;
};


struct IStructuredBuffer::DXModelEx {
	/** Buffer view. */
	DXUnique<ID3D11ShaderResourceView> ShaderResourceView;
};


struct IRWStructuredBuffer::DXModelEx {
	/** Buffer view. */
	DXUnique<ID3D11ShaderResourceView> ShaderResourceView;
	/** Buffer view. */
	DXUnique<ID3D11UnorderedAccessView> UnorderedAccessView;
};


struct VertexShader::DXModel {
	/** */
	DXUnique<ID3D11VertexShader> Shader;
	/** */
	DXUnique<ID3D11InputLayout> Layout;
};


struct PixelShader::DXModel {
	/** */
	DXUnique<ID3D11PixelShader> Shader;
};


struct ComputeShader::DXModel {
	/** */
	DXUnique<ID3D11ComputeShader> Shader;
};


Texture::Texture() = default;
Texture::~Texture() = default;


IBuffer::IBuffer() = default;
IBuffer::~IBuffer() = default;


IStructuredBuffer::IStructuredBuffer() = default;
IStructuredBuffer::~IStructuredBuffer() { Release(); }


IRWStructuredBuffer::IRWStructuredBuffer() = default;
IRWStructuredBuffer::~IRWStructuredBuffer() { Release(); }


VertexShader::VertexShader() = default;
VertexShader::~VertexShader() = default;


PixelShader::PixelShader() = default;
PixelShader::~PixelShader() = default;


ComputeShader::ComputeShader() = default;
ComputeShader::~ComputeShader() = default;


Texture::Texture(const String & filename)
	: _filename(filename)
{}


Texture::Texture(Modes mode)
	: _filename()
	, _mode(mode)
{}


void Devices::DXDevice::initDepthBuffer(UINT w, UINT h, UINT msaa) {
	// Depth buffer.
	ZeroMemory(&_depthBufferDesc, sizeof(_depthBufferDesc));
	_depthBufferDesc.Width = w;
	_depthBufferDesc.Height = h;
	_depthBufferDesc.MipLevels = 1;
	_depthBufferDesc.ArraySize = 1;
	_depthBufferDesc.Format = DD_DEVICE_DEPTHBUFFER_TYPE;
	_depthBufferDesc.SampleDesc.Count = msaa;
	_depthBufferDesc.SampleDesc.Quality = 0;
	_depthBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	_depthBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	_depthBufferDesc.CPUAccessFlags = 0;
	_depthBufferDesc.MiscFlags = 0;
	// https://learn.microsoft.com/en-us/windows/win32/api/d3d11/nf-d3d11-id3d11device-createtexture2d
	HR(_device->CreateTexture2D, &_depthBufferDesc, NULL, _depthBufferTexture.Init());

	// Depth stencil state.
	ZeroMemory(&_depthStencilDesc, sizeof(_depthStencilDesc));
	_depthStencilDesc.DepthEnable = false;
	_depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	_depthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS_EQUAL;
	_depthStencilDesc.StencilEnable = true;
	_depthStencilDesc.StencilReadMask = 0xFF;
	_depthStencilDesc.StencilWriteMask = 0xFF;
	// Stencil operations if pixel is front-facing.
	_depthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	_depthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	_depthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	_depthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	// Stencil operations if pixel is back-facing.
	_depthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	_depthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	_depthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	_depthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	// Create the depth stencil state.
	HR(_device->CreateDepthStencilState, &_depthStencilDesc, _depthStencilState.Init());

	// Set the depth stencil state to the context.
	// https://learn.microsoft.com/en-us/windows/win32/api/d3d11/nf-d3d11-id3d11devicecontext-omsetdepthstencilstate
	_context->OMSetDepthStencilState(_depthStencilState, 1);

	// Create the depth stencil view.
	ZeroMemory(&_depthStencilViewDesc, sizeof(_depthStencilViewDesc));
	_depthStencilViewDesc.Format = DXGI_FORMAT_D32_FLOAT;//DD_DEVICE_DEPTHBUFFER_TYPE;
	_depthStencilViewDesc.ViewDimension = msaa == 1 ? D3D11_DSV_DIMENSION_TEXTURE2D : D3D11_DSV_DIMENSION_TEXTURE2DMS;
	_depthStencilViewDesc.Texture2D.MipSlice = 0;
	// https://learn.microsoft.com/en-us/windows/win32/api/d3d12/nf-d3d12-id3d12device-createdepthstencilview
	HR(_device->CreateDepthStencilView, _depthBufferTexture, &_depthStencilViewDesc, _depthStencilView.Init());

	// Bind the render target view and depth stencil buffer to the output render pipeline.
	// https://learn.microsoft.com/en-us/windows/win32/api/d3d11/nf-d3d11-id3d11devicecontext-omsetrendertargets
	_context->OMSetRenderTargets(1, &_rtv, _depthStencilView);
}


Devices & Devices::Clear(const float * color) {
#if DD_DEVICE_STATISTIC
	_statistics.DrawCalls = 0;
	_statistics.LineLists = 0;
	_statistics.LineStrips = 0;
	_statistics.PointLists = 0;
	_statistics.TriangleLists = 0;
	_statistics.TriangleStrips = 0;
#endif

	_dxmodel->_context->ClearRenderTargetView(_dxmodel->_rtv, color);
	return *this;
}


Devices & Devices::ClearDepth(const float depth) {
	if (_dxmodel->_depthStencilView)
		_dxmodel->_context->ClearDepthStencilView(_dxmodel->_depthStencilView, D3D11_CLEAR_DEPTH, depth, 0);

	return *this;
}


Reference<Texture> Devices::GetDepthTexture() {
	Texture * texture = new Texture(String());

	// create copy of depth buffer
	D3D11_TEXTURE2D_DESC depthBufferDesc;
	ZeroMemory(&depthBufferDesc, sizeof(depthBufferDesc));
	depthBufferDesc.Width = (UINT)_window->Client().Width;
	depthBufferDesc.Height = (UINT)_window->Client().Height;
	depthBufferDesc.MipLevels = 1;
	depthBufferDesc.ArraySize = 1;
	depthBufferDesc.Format = DD_DEVICE_DEPTHBUFFER_TYPE;
	depthBufferDesc.SampleDesc.Count = _msaa;
	depthBufferDesc.SampleDesc.Quality = 0;
	depthBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	depthBufferDesc.CPUAccessFlags = 0;
	depthBufferDesc.MiscFlags = 0;
	depthBufferDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	// https://learn.microsoft.com/en-us/windows/win32/api/d3d11/nf-d3d11-id3d11device-createtexture2d
	HR(_dxmodel->_device->CreateTexture2D, &depthBufferDesc, NULL, texture->_model->_resource.Init());

	_dxmodel->_context->OMSetDepthStencilState(0, 1);
	_dxmodel->_context->OMSetRenderTargets(1, _dxmodel->_rtv.Address(), 0);
	_dxmodel->_context->CopyResource(texture->_model->_resource, _dxmodel->_depthBufferTexture);
	_dxmodel->_context->OMSetDepthStencilState(_dxmodel->_depthStencilState, 1);
	_dxmodel->_context->OMSetRenderTargets(1, _dxmodel->_rtv.Address(), _dxmodel->_depthStencilView);

	// create sampler
	D3D11_SAMPLER_DESC sampDesc;
	ZeroMemory(&sampDesc, sizeof(sampDesc));
	sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	sampDesc.MinLOD = 0;
	sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
	HR(_dxmodel->_device->CreateSamplerState, &sampDesc, texture->_model->_samplerState.Init());

	// resource view
	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
	ZeroMemory(&srvDesc, sizeof(srvDesc));
	srvDesc.Format = DXGI_FORMAT_R32_FLOAT;//DD_DEVICE_DEPTHBUFFER_TYPE;
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DMS;
	//srvDesc.Texture2D.MipLevels = 1;
	//srvDesc.Texture2DMS.
	HR(_dxmodel->_device->CreateShaderResourceView, texture->_model->_resource, &srvDesc, &texture->_model->_shaderResourceView);

	texture->_device = this;
	return texture;
}


Texture & Devices::GetMainRenderTarget() {
	// TODO: insert return statement here
	return _dxmodel->RenderTarget;
}


ShaderCompiler Devices::Compiler(const Text::StringView16 & path) {
	return ShaderCompiler(*this, path);
}


Devices::Devices(InitParams & params)
	: _window(params.Window)
	, _references(0)
	, _vsync(1)
	, _dxmodel()
	, _this(this)
#if DD_DEVICE_STATISTIC
	, _statistics()
#endif
	, _topology(PrimitiveTopologies::Undefined)
	, _msaa(1)
{
	init(params);
}


struct DXScreenshot
	: public Devices::IScreenshot
{
#if DD_TEXTURE_EXPERIMENT || 1
	static bool TryParse(Formats in, D3DX11_IMAGE_FILE_FORMAT & out) {
		switch (in) {
			case Formats::BMP:  out = D3DX11_IFF_BMP; return true;
			case Formats::JPEG: out = D3DX11_IFF_JPG; return true;
			case Formats::PNG:  out = D3DX11_IFF_PNG; return true;
			default: DD_WARNING("Format problem."); return false;
		}
	}
#endif
	/** Data. */
	DXUnique<ID3D10Blob> _blob;

	// Inherited via ScreenShot
	void Save(const FileSystem::Path & filename) override {
#if DD_TEXTURE_EXPERIMENT || 1
		DD::FileSystem::Path file(filename);
		file.Parent().CreateDirectory();
		file.AsFile().Write(Get());
#else
		DD_WARNING("Technology not enabled.")
#endif
	}

	ConstByteView Get() override {
#if DD_TEXTURE_EXPERIMENT || 1
		return ArrayView((const char *)_blob->GetBufferPointer(), _blob->GetBufferSize());
#else
		DD_WARNING("Technology not enabled.")
#endif

		return nullptr;
	}

	/** */
	DXScreenshot(ID3D10Blob * blob) : _blob(blob) {}
	/** */
	virtual ~DXScreenshot() override { }
};


Reference<Devices::IScreenshot> Devices::CaptureScreenshot() {
  // prepare container for output
  DXUnique<ID3D11Texture2D> screenshotTexture;

  // get back buffer
  DXUnique<ID3D11Texture2D> backBuffer;
  D3D11_TEXTURE2D_DESC backbufferDesc;
  _dxmodel->_swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID *)backBuffer.Init());
  backBuffer->GetDesc(&backbufferDesc);
  // prepare texture storage
  D3D11_TEXTURE2D_DESC textureCopyDesc(backbufferDesc);
  // create description for buffer copy
  DXGI_SAMPLE_DESC noMultiSampling = { 1, 0 };
  textureCopyDesc.SampleDesc = noMultiSampling;
  textureCopyDesc.MipLevels = 1;
  textureCopyDesc.Usage = D3D11_USAGE_STAGING;
  textureCopyDesc.MiscFlags = 0;
  textureCopyDesc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
  textureCopyDesc.BindFlags = 0;
  textureCopyDesc.ArraySize = 1;
  _dxmodel->_device->CreateTexture2D(&textureCopyDesc, NULL, screenshotTexture.Init());

  // check if back buffer is multi-sampled, if so texture must be resolved before saving
  if (backbufferDesc.SampleDesc.Count > 1) {
    // create intermediate copy because resolution could be done only on usage=default texture
    DXUnique<ID3D11Texture2D> intermediateCopy;
    D3D11_TEXTURE2D_DESC intermediateCopyDesc(backbufferDesc);
    intermediateCopyDesc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
    intermediateCopyDesc.MipLevels = 1;
    intermediateCopyDesc.Usage = D3D11_USAGE_DEFAULT;
    intermediateCopyDesc.CPUAccessFlags = 0;
    intermediateCopyDesc.BindFlags = 0;
    intermediateCopyDesc.SampleDesc = noMultiSampling;
    _dxmodel->_device->CreateTexture2D(&intermediateCopyDesc, NULL, intermediateCopy.Init());
    // resolve multi-sampling
    _dxmodel->_context->ResolveSubresource(intermediateCopy, 0, backBuffer, 0, backbufferDesc.Format);
    // copy to STAGING texture
    _dxmodel->_context->CopyResource(screenshotTexture, intermediateCopy);
  }
  else {
    // just copy back/buffer if no multi-sampling applied
    _dxmodel->_context->CopyResource(screenshotTexture, backBuffer);
  }

	ID3D10Blob * blob;
	HR(D3DX11SaveTextureToMemory, _dxmodel->_context, screenshotTexture, D3DX11_IFF_PNG, &blob, 0);
	// automatically wrapped into smart pointer
	return new DXScreenshot(blob);
}


void Devices::CaptureScreenshot(const FileSystem::Path & filename) {
  // constants
  static const Text::StringView16 DEFAULT_PREFIX = L"Screenshot/";
  static const Text::StringView16 DEFAULT_SUFFIX = L".png";

  // prepare filename
  StringLocal<256> file;

  if (filename.IsAbsolute()) {
    file.AppendBatch(filename, DEFAULT_SUFFIX);
  }
  else {
    file.AppendBatch(DEFAULT_PREFIX, filename, DEFAULT_SUFFIX);
    if (!FileSystem::Exists(DEFAULT_PREFIX)) {
			FileSystem::CreateDirectory(DEFAULT_PREFIX);
    }
  }

  // get texture and save it
  CaptureScreenshot()->Save(file);
}

#if _DEBUG
void Devices::ReportLiveObjects() {
	ID3D11Debug * pDebug;
	HRESULT hr = _dxmodel->_device->QueryInterface(IID_PPV_ARGS(&pDebug));
	if (S_OK == hr) {
		DD_LOG("Reporting live objects:");
		pDebug->ReportLiveDeviceObjects(D3D11_RLDO_DETAIL | D3D11_RLDO_IGNORE_INTERNAL);
		pDebug->Release();
	}
}
#endif


void Devices::Exit() {
	_computeShaders.Free();
	_pixelShaders.Free();
	_vertexShaders.Free();
	_textures.Free();
}


Devices & Devices::init(InitParams & params) {
	unsigned int numerator = 0, denominator = 1;

	DXUnique<IDXGIFactory1> factory;
	// Create a DirectX graphics interface factory.
	// https://learn.microsoft.com/en-us/windows/win32/api/dxgi/nf-dxgi-createdxgifactory1
	HR(CreateDXGIFactory1, __uuidof(IDXGIFactory2), (void**)factory.Init());

	// Use the factory to create an adapter for the primary graphics interface (video card).
	List<Description, 8> descriptions;
	List<DXUnique<IDXGIAdapter1>, 8> adapters;
	IDXGIAdapter1 * adapterTemp;
	for (UINT i = 0; SUCCEEDED(factory->EnumAdapters1(i, &adapterTemp)); ++i) {
		adapters.Add(adapterTemp);
		// get info about adapters
		DXGI_ADAPTER_DESC1 adapterDesc;
		adapters[i]->GetDesc1(&adapterDesc);
 		DD_DEBUG_LOGF(
 			"Adapter description\n"
 			"\tLocal id:  {0}\n"
 			"\tSystem id: {1}\n"
 			"\tVendor id: {2}\n"
 			"\tDevice id: {3}\n"
 			"\tRevision:  {4}\n"
 			"\tDedicated system memory: {5} MB\n"
 			"\tDedicated video memory:  {6} MB\n"
			"\tShared system memory:    {7} MB\n"
			"\tDescription: {8}\n",
 			/*0*/ reinterpret_cast<i64&>(adapterDesc.AdapterLuid),
 			/*1*/ adapterDesc.SubSysId,
 			/*2*/ adapterDesc.VendorId,
 			/*3*/ adapterDesc.DeviceId,
 			/*4*/ adapterDesc.Revision,
			/*5*/ adapterDesc.DedicatedSystemMemory / (1024 * 1024),
			/*6*/ adapterDesc.DedicatedVideoMemory / (1024 * 1024),
			/*7*/ adapterDesc.SharedSystemMemory / (1024 * 1024),
 			/*8*/ adapterDesc.Description
 		);
		auto & desc = descriptions.Append();
		desc.DedicatedSystemMemory = adapterDesc.DedicatedSystemMemory;
		desc.DedicatedVideoMemory = adapterDesc.DedicatedVideoMemory;
		desc.SharedSystemMemory = adapterDesc.SharedSystemMemory;
		desc.Name = adapterDesc.Description;
	}

	// default selected index zero
	UINT selectedAdapter = 0;
	// get adapter index from callback
	if (params.SelectDevice)
		selectedAdapter = params.SelectDevice->Run(descriptions);
	// check index validity
	if (adapters.Size() <= selectedAdapter)
		selectedAdapter = 0;

	// Enumerate the primary adapter output (monitor).
	DXUnique<IDXGIOutput> adapterOutput;
	HRESULT enumOutputsResult = adapters[selectedAdapter]->EnumOutputs(0, adapterOutput.Init());
	if (SUCCEEDED(enumOutputsResult)) {
		// number of available modes
		UINT nModes = 0;
		// Get the number of modes that fit the DXGI_FORMAT_R8G8B8A8_UNORM display format for the adapter output (monitor).
		HRESULT result = adapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &nModes, NULL);
		// check for remote sessions (GetDisplayModeList is not available on remote sessions)
		if (result != DXGI_ERROR_NOT_CURRENTLY_AVAILABLE) {
			if (FAILED(result))
				throw result;

			// Create a list to hold all the possible display modes for this monitor/video card combination.
			_dxmodel->_displayModeList.Reallocate(nModes);

			// Now fill the display mode list structures.
			result = adapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &nModes, _dxmodel->_displayModeList.Ptr());
			if (FAILED(result))
				throw result;
		}
		// Now go through all the display modes and find the one that matches the screen width and height.
		// When a match is found store the numerator and denominator of the refresh rate for that monitor.
		for (size_t i = 0; i < nModes; i++) {
			if (_dxmodel->_displayModeList[i].Width == _window->Width()) {
				if (_dxmodel->_displayModeList[i].Height == _window->Height()) {
					numerator = _dxmodel->_displayModeList[i].RefreshRate.Numerator;
					denominator = _dxmodel->_displayModeList[i].RefreshRate.Denominator;
				}
			}
		}
	}
	// ignore DXGI_ERROR_NOT_FOUND, some adapters does not have output
	else if (DXGI_ERROR_NOT_FOUND != enumOutputsResult) {
		HR_WARNING(enumOutputsResult, "IDXGIAdapter::EnumOutputs");
	}

	_videoCard = descriptions[selectedAdapter].Name;

	// Initialize the swap chain description.
	DXGI_SWAP_CHAIN_DESC swapChainDesc;
	ZeroMemory(&swapChainDesc, sizeof(swapChainDesc));
	swapChainDesc.BufferCount = 1;                                // single back buffer
	swapChainDesc.BufferDesc.Width = _window->Width();            // dims
	swapChainDesc.BufferDesc.Height = _window->Height();          // dims
	swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM; // 32-bit surface for the back buffers
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;  // usage
	swapChainDesc.OutputWindow = (HWND)_window->NativeHandle();   // where to render
	swapChainDesc.SampleDesc.Count = _msaa;                       // sampling
	swapChainDesc.SampleDesc.Quality = 0;                         // sampling
	swapChainDesc.Windowed = true;                                // window/full-screen
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;          // discard after present
	swapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED; // scanline
	swapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;                 // scanline
	swapChainDesc.Flags = 0;
	swapChainDesc.BufferDesc.RefreshRate.Numerator = numerator;     // refesh rate
	swapChainDesc.BufferDesc.RefreshRate.Denominator = denominator; // refresh rate

	UINT deviceFlags = D3D11_CREATE_DEVICE_BGRA_SUPPORT;
#ifdef _DEBUG
	deviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	// Set the feature level to DirectX 11.
	D3D_FEATURE_LEVEL featureLevel = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL featureLevelOut;
	// Create the swap chain, Direct3D device, and Direct3D device context.
	HR(
		D3D11CreateDeviceAndSwapChain, // https://learn.microsoft.com/en-us/windows/win32/api/d3d11/nf-d3d11-d3d11createdeviceandswapchain
		adapters[selectedAdapter],     // IDXGIAdapter* pAdapter
		D3D_DRIVER_TYPE_UNKNOWN,       // D3D_DRIVER_TYPE DriverType
		NULL,                          // HMODULE Software
		deviceFlags,                   // UINT Flags
		&featureLevel,                 // D3D_FEATURE_LEVEL* pFeatureLevels
		1,                             // UINT FeatureLevels
		D3D11_SDK_VERSION,             // UINT SDKVersion
		&swapChainDesc,                // DXGI_SWAP_CHAIN_DESC* pSwapChainDesc
		_dxmodel->_swapChain.Init(),   // IDXGISwapChain** ppSwapChain
		_dxmodel->_device.Init(),      // ID3D11Device** ppDevice
		&featureLevelOut,              // D3D_FEATURE_LEVEL* pFeatureLevel
		_dxmodel->_context.Init()      // ID3D11DeviceContext** ppImmediateContext
	);

	D3D11_FORMAT_SUPPORT formatSupport;
	_dxmodel->_device->CheckFormatSupport(DXGI_FORMAT_R1_UNORM, (UINT *)&formatSupport);
	DD_LOG("DXGI_FORMAT_R1_UNORM", formatSupport);
	_dxmodel->_device->CheckFormatSupport(DXGI_FORMAT_R8_UNORM, (UINT *)&formatSupport);
	DD_LOG("DXGI_FORMAT_R8_UNORM", formatSupport);

	// Get the pointer to the back buffer.
	DXUnique<ID3D11Texture2D> backBufferPtr;
	HR(_dxmodel->_swapChain->GetBuffer, NULL, __uuidof(ID3D11Texture2D), (LPVOID*)backBufferPtr.Init());
	// Create the render target view with the back buffer pointer.
	HR(_dxmodel->_device->CreateRenderTargetView, backBufferPtr, NULL, _dxmodel->_rtv.Init());
	_dxmodel->initDepthBuffer((UINT)_window->Width(), (UINT)_window->Height(), _msaa);

	// Setup the raster description which will determine how and what polygons will be drawn.
	D3D11_RASTERIZER_DESC rasterDesc;
	rasterDesc.AntialiasedLineEnable = 1;
	rasterDesc.CullMode = D3D11_CULL_BACK;
	rasterDesc.DepthBias = 0;
	rasterDesc.DepthBiasClamp = 0.0f;
	rasterDesc.DepthClipEnable = true;
	rasterDesc.FrontCounterClockwise = false;
	rasterDesc.MultisampleEnable = 1;
	rasterDesc.ScissorEnable = false;
	rasterDesc.SlopeScaledDepthBias = 0.0f;
	// Wireframe enabled
	rasterDesc.FillMode = D3D11_FILL_WIREFRAME;
	HR(_dxmodel->_device->CreateRasterizerState, &rasterDesc, _dxmodel->_wireframeEnabled.Init());
	// Wireframe disabled
	rasterDesc.FillMode = D3D11_FILL_SOLID;
	HR(_dxmodel->_device->CreateRasterizerState, &rasterDesc, _dxmodel->_wireframeDisabled.Init());

	// Now set the rasterizer state.
	_dxmodel->_context->RSSetState(_dxmodel->_wireframeDisabled);

	// Setup the viewport for rendering.
	D3D11_VIEWPORT viewport;
	viewport.Width = (float)_window->Width();
	viewport.Height = (float)_window->Height();
	viewport.MinDepth = 0.0f;
	viewport.MaxDepth = 1.0f;
	viewport.TopLeftX = 0.0f;
	viewport.TopLeftY = 0.0f;
	_dxmodel->_context->RSSetViewports(1, &viewport);

	D3D11_BLEND_DESC ABDisabled;
	ZeroMemory(&ABDisabled, sizeof(D3D11_BLEND_DESC));
	ABDisabled.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	ABDisabled.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	ABDisabled.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	ABDisabled.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	ABDisabled.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
	ABDisabled.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	ABDisabled.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
	HR(_dxmodel->_device->CreateBlendState, &ABDisabled, _dxmodel->_blendStateABDisabled.Init());

	D3D11_BLEND_DESC ABEnabled;
	ZeroMemory(&ABEnabled, sizeof(D3D11_BLEND_DESC));
	ABEnabled.AlphaToCoverageEnable = true;
	ABEnabled.RenderTarget[0].BlendEnable = true;
	ABEnabled.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	ABEnabled.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	ABEnabled.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	ABEnabled.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	ABEnabled.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
	ABEnabled.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	ABEnabled.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
	HR(_dxmodel->_device->CreateBlendState, &ABEnabled, _dxmodel->_blendStateABEnabled.Init());

	return *this;
}


i32 Devices::releaseBuffer(IBuffer & buffer) {
	if (buffer._model->Buffer) {
#if DD_DEVICE_STATISTIC
		D3D11_BUFFER_DESC desc;
		buffer._model->Buffer->GetDesc(&desc);
#endif
		DD_TODO_STAMP("Seems outdated.");
		buffer._model->Buffer = nullptr;
#if DD_DEVICE_STATISTIC
		return desc.ByteWidth;
#endif
	}
	return 0;
}


/************************************************************************/
/* ConstantBuffer                                                       */
/************************************************************************/
void Devices::ConstantBufferInit(IConstantBuffer & cb, u32 bytes) {
	DD_ASSERT(bytes != 0);

	cb._device = this;

	D3D11_BUFFER_DESC description;
	memset(&description, 0, sizeof(description));
	description.Usage = D3D11_USAGE_DEFAULT;
	description.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	description.ByteWidth = bytes;
	description.CPUAccessFlags = 0;

#if DD_DEVICE_STATISTIC
	_statistics.Bytes += bytes;
	_statistics.CBBytes += bytes;
	_statistics.CBCount += 1;
	_statistics.CBInits += 1;
#endif

	_dxmodel->_device->CreateBuffer(&description, 0, cb._model->Buffer.Init());
}


void Devices::ConstantBufferBind2CS(IConstantBuffer & cb, u32 slot) {
	_dxmodel->_context->CSSetConstantBuffers(slot, 1, &cb._model->Buffer);
}


void Devices::ConstantBufferBind2VS(IConstantBuffer & cb, u32 slot) {
	_dxmodel->_context->VSSetConstantBuffers(slot, 1, &cb._model->Buffer);
}


void Devices::ConstantBufferBind2PS(IConstantBuffer & cb, u32 slot) {
	_dxmodel->_context->PSSetConstantBuffers(slot, 1, &cb._model->Buffer);
}


void Devices::ConstantBufferUpdate(IConstantBuffer & cb, const void * source) {
	if (cb._model->Buffer)
		_dxmodel->_context->UpdateSubresource(cb._model->Buffer, 0, 0, source, 0, 0);
}


void Devices::ConstantBufferRelease(IConstantBuffer & cb) {
	i32 bytes = releaseBuffer(cb);
#if DD_DEVICE_STATISTIC
	AddCommand([bytes](Device & d) {
		d->_statistics.Bytes -= bytes;
		d->_statistics.CBBytes -= bytes;
		d->_statistics.CBCount -= 1;
	});
#endif
}


/************************************************************************/
/* IndexBuffer                                                          */
/************************************************************************/
void Devices::IndexBufferInit(IIndexBuffer & ib, u32 bytes, const void * source) {
	DD_ASSERT(bytes != 0);

	ib._device = this;

	D3D11_BUFFER_DESC description;
	memset(&description, 0, sizeof(description));
	description.Usage = D3D11_USAGE_DEFAULT;
	description.BindFlags = D3D11_BIND_INDEX_BUFFER;
	description.ByteWidth = bytes;
	description.CPUAccessFlags = 0;

#if DD_DEVICE_STATISTIC
	_statistics.Bytes += bytes;
	_statistics.IBBytes += bytes;
	_statistics.IBCount += 1;
	_statistics.IBInits += 1;
#endif

	D3D11_SUBRESOURCE_DATA init;
	memset(&init, 0, sizeof(init));
	init.pSysMem = source;

	_dxmodel->_device->CreateBuffer(&description, &init, ib._model->Buffer.Init());
}


void Devices::IndexBufferBind(IIndexBuffer & ib, IIndexBufferIndices indexType, u32 offset) {
	_dxmodel->_context->IASetIndexBuffer(ib._model->Buffer, static_cast<DXGI_FORMAT>(indexType), offset);
}


void Devices::IndexBufferRelease(IIndexBuffer & ib) {
	i32 bytes = releaseBuffer(ib);
#if DD_DEVICE_STATISTIC
	AddCommand([bytes](Device & d) {
		d->_statistics.Bytes -= bytes;
		d->_statistics.IBBytes -= bytes;
		d->_statistics.IBCount -= 1;
	});
#endif
}

/************************************************************************/
/* ComputeShader                                                        */
/************************************************************************/
void Devices::ComputeShaderLoad(ComputeShader & cs, const Text::StringView16 & name) {
	cs._name = name;

	String path = String::From(name, ".cso");
	DXUnique<ID3DBlob> file;
	HR(D3DReadFileToBlob, path.begin(), file.Init());
	HR(_dxmodel->_device->CreateComputeShader, file->GetBufferPointer(), file->GetBufferSize(), NULL, &cs._model->Shader);
}


void Devices::ComputeShaderSet(ComputeShader & cs) {
	_dxmodel->_context->CSSetShader(cs._model->Shader, 0, 0);
}


void Devices::ComputeShaderDispatch(u32 x, u32 y, u32 z) {
	_dxmodel->_context->Dispatch(x, y, z);
}


/************************************************************************/
/* PixelShader                                                          */
/************************************************************************/
void Devices::PixelShaderLoad(PixelShader & ps, const Text::StringView16 & name) {
	ps._name = name;

	String path = String::From(name, ".cso");
	DXUnique<ID3DBlob> file;
	HR(D3DReadFileToBlob, path.begin(), file.Init());
	HR(_dxmodel->_device->CreatePixelShader, file->GetBufferPointer(), file->GetBufferSize(), 0, &ps._model->Shader);
}


void Devices::PixelShaderSet(PixelShader & ps) {
	_dxmodel->_context->PSSetShader(ps._model->Shader, 0, 0);
}


/************************************************************************/
/* VertexShader                                                         */
/************************************************************************/
void Devices::VertexShaderLoad(VertexShader & vs, const Text::StringView16 & name, const void * layout, u32 layoutSize) {
	vs._name = name;

	String path = String::From(vs._name, ".cso");
	DXUnique<ID3DBlob> file;
	DD_LITE_DEBUG_ASSERT(layout, "Layout must be set");
	HR(D3DReadFileToBlob, path.begin(), file.Init());
	HR(_dxmodel->_device->CreateVertexShader, file->GetBufferPointer(), file->GetBufferSize(), NULL, &vs._model->Shader);
	HR(
		_dxmodel->_device->CreateInputLayout,
		reinterpret_cast<const D3D11_INPUT_ELEMENT_DESC*&>(layout),
		layoutSize,
		file->GetBufferPointer(),
		file->GetBufferSize(),
		&vs._model->Layout
	);
}


void Devices::VertexShaderSet(VertexShader & vs) {
	_dxmodel->_context->IASetInputLayout(vs._model->Layout);
	_dxmodel->_context->VSSetShader(vs._model->Shader, 0, 0);
}


/************************************************************************/
/* VertexBuffer                                                         */
/************************************************************************/
void Devices::VertexBufferInit(IVertexBuffer & vb, u32 bytes, u32 stride, const void * source) {
	DD_ASSERT(bytes != 0);

	vb._device = this;

	D3D11_BUFFER_DESC description;
	memset(&description, 0, sizeof(description));
	description.Usage = D3D11_USAGE_DEFAULT;
	description.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	description.ByteWidth = bytes;
	description.CPUAccessFlags = 0;

#if DD_DEVICE_STATISTIC
	_statistics.Bytes += bytes;
	_statistics.VBBytes += bytes;
	_statistics.VBCount += 1;
	_statistics.VBInits += 1;
#endif

	D3D11_SUBRESOURCE_DATA init;
	memset(&init, 0, sizeof(init));
	init.pSysMem = source;

	_dxmodel->_device->CreateBuffer(&description, &init, vb._model->Buffer.Init());
}


void Devices::VertexBufferBind(IVertexBuffer & vb, u32 stride, u32 offset) {
	_dxmodel->_context->IASetVertexBuffers(0, 1, vb._model->Buffer.Address(), &stride, &offset);
}


void Devices::VertexBufferBind(IVertexBuffer & vb1, u32 stride1, u32 offset1, IVertexBuffer & vb2, u32 stride2, u32 offset2) {
	ID3D11Buffer * buffers[2] = { vb1._model->Buffer, vb2._model->Buffer };
	u32 strides[2] = { stride1, stride2 };
	u32 offsets[2] = { offset1, offset2 };

	_dxmodel->_context->IASetVertexBuffers(0, 2, buffers, strides, offsets);
}


void Devices::VertexBufferRelease(IVertexBuffer & vb) {
	i32 bytes = releaseBuffer(vb);
#if DD_DEVICE_STATISTIC
	AddCommand([bytes](Device & d) {
		d->_statistics.Bytes -= bytes;
		d->_statistics.VBBytes -= bytes;
		d->_statistics.VBCount -= 1;
	});
#endif
}


/************************************************************************/
/* StructuredBuffer                                                     */
/************************************************************************/
void Devices::StructuredBufferInit(IStructuredBuffer & sb, u32 bytes, u32 stride, const void * source) {
	DD_ASSERT(bytes != 0);

	sb._device = this;

	D3D11_BUFFER_DESC description;
	memset(&description, 0, sizeof(description));
	description.Usage = D3D11_USAGE_DEFAULT;
	description.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	description.ByteWidth = bytes;
	description.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
	description.StructureByteStride = stride;

#if DD_DEVICE_STATISTIC
	_statistics.Bytes += bytes;
	_statistics.SBBytes += bytes;
	_statistics.SBCount += 1;
	_statistics.SBInits += 1;
#endif

	D3D11_SUBRESOURCE_DATA init;
	memset(&init, 0, sizeof(init));
	init.pSysMem = source;

	_dxmodel->_device->CreateBuffer(&description, &init, &sb._model->Buffer);

	D3D11_SHADER_RESOURCE_VIEW_DESC srvDescription;
	memset(&srvDescription, 0, sizeof(srvDescription));
	srvDescription.Format = DXGI_FORMAT_UNKNOWN;
	srvDescription.ViewDimension = D3D11_SRV_DIMENSION_BUFFER;
	srvDescription.Buffer.NumElements = bytes / stride;
	_dxmodel->_device->CreateShaderResourceView(
		sb._model->Buffer,
		&srvDescription,
		&sb.ModelEx->ShaderResourceView
	);
}


void Devices::RWStructuredBufferInit(IRWStructuredBuffer & sb, u32 bytes, u32 stride, const void * source) {
	DD_ASSERT(bytes != 0);

	sb._device = this;

	D3D11_BUFFER_DESC description;
	memset(&description, 0, sizeof(description));
	description.Usage = D3D11_USAGE_DEFAULT;
	description.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS;
	description.ByteWidth = bytes;
	description.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
	description.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE | D3D11_CPU_ACCESS_READ;
	description.StructureByteStride = stride;

#if DD_DEVICE_STATISTIC
	_statistics.Bytes += bytes;
	_statistics.RWSBBytes += bytes;
	_statistics.RWSBCount += 1;
	_statistics.RWSBInits += 1;
#endif

	_dxmodel->_device->CreateBuffer(&description, 0, &sb._model->Buffer);

	D3D11_SHADER_RESOURCE_VIEW_DESC srvDescription;
	memset(&srvDescription, 0, sizeof(srvDescription));
	srvDescription.Format = DXGI_FORMAT_UNKNOWN;
	srvDescription.ViewDimension = D3D11_SRV_DIMENSION_BUFFER;
	srvDescription.Buffer.NumElements = bytes / stride;
	_dxmodel->_device->CreateShaderResourceView(
		sb._model->Buffer,
		&srvDescription,
		&sb.ModelEx->ShaderResourceView
	);

	D3D11_UNORDERED_ACCESS_VIEW_DESC uavDescription;
	ZeroMemory(&uavDescription, sizeof(uavDescription));
	uavDescription.Format = DXGI_FORMAT_UNKNOWN;
	uavDescription.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
	uavDescription.Buffer.NumElements = bytes / stride;
	_dxmodel->_device->CreateUnorderedAccessView(
		sb._model->Buffer,
		&uavDescription,
		&sb.ModelEx->UnorderedAccessView
	);
}


void Devices::StructuredBufferBind2VS(IStructuredBuffer & sb, u32 slot) {
	_dxmodel->_context->VSSetShaderResources(slot, 1, &sb.ModelEx->ShaderResourceView);
}


void Devices::StructuredBufferBind2CS(IStructuredBuffer & sb, u32 slot) {
	_dxmodel->_context->CSSetShaderResources(slot, 1, &sb.ModelEx->ShaderResourceView);
}


void Devices::StructuredBufferRelease(IStructuredBuffer & sb) {
	i32 bytes = releaseBuffer(sb);
#if DD_DEVICE_STATISTIC
	AddCommand([bytes](Device & d) {
		d->_statistics.Bytes -= bytes;
		d->_statistics.SBBytes -= bytes;
		d->_statistics.SBCount -= 1;
	});
#endif

	sb.ModelEx->ShaderResourceView = nullptr;
}

void Devices::RWStructuredBufferBind2CS(IRWStructuredBuffer & sb, u32 slot) {
	_dxmodel->_context->CSSetUnorderedAccessViews(slot, 1, &sb.ModelEx->UnorderedAccessView, 0);
}

void Devices::RWStructuredBufferRead(IRWStructuredBuffer & sb, void * data, u32 size) {
	D3D11_MAPPED_SUBRESOURCE MappedResource = { 0 };
	if (SUCCEEDED(_dxmodel->_context->Map(sb._model->Buffer, 0, D3D11_MAP_READ, 0, &MappedResource))) {
		memcpy(data, MappedResource.pData, size);
		_dxmodel->_context->Unmap(sb._model->Buffer, 0);
	}
	else {
		throw;
	}
}


void Devices::RWStructuredBufferRelease(IRWStructuredBuffer & sb) {
	i32 bytes = releaseBuffer(sb);
#if DD_DEVICE_STATISTIC
	AddCommand([bytes](Device & d) {
		d->_statistics.Bytes -= bytes;
		d->_statistics.RWSBBytes -= bytes;
		d->_statistics.RWSBCount -= 1;
	});
#endif

	sb.ModelEx->UnorderedAccessView = nullptr;
	sb.ModelEx->ShaderResourceView = nullptr;
}


/************************************************************************/
/* Textures                                                             */
/************************************************************************/
void Devices::TextureInit(Texture & t) {
	if (t._filename.Length()) {
#if DD_TEXTURE_EXPERIMENT
		ID3D11Resource * resource;
		ID3D11SamplerState * samplerState;
		ID3D11ShaderResourceView * shaderResourceView;
		D3D11_SAMPLER_DESC sampDesc;
		ZeroMemory(&sampDesc, sizeof(sampDesc));
		sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
		sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
		sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
		sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
		sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
		sampDesc.MinLOD = 0;
		sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
		HRESULT hr;

		// resource, resource view
		hr = DirectX::CreateDDSTextureFromFile(_dxmodel->_device, _dxmodel->_context, t._filename, &resource, &shaderResourceView);
		// sampler
		hr = _dxmodel->_device->CreateSamplerState(&sampDesc, &samplerState);
		// texture init
		t._model->_resource = resource;
		t._model->_samplerState = samplerState;
		t._model->_shaderResourceView = shaderResourceView;
#else
		DD_WARNING("Technology not enabled");
#endif
	}
	// initialize as RTV
	else {
		D3D11_TEXTURE2D_DESC txtDesc;
		txtDesc.Format = (DXGI_FORMAT)t._mode; // DXGI_FORMAT_R8G8B8A8_UNORM;
		txtDesc.MipLevels = 1;
		txtDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET;
		txtDesc.ArraySize = 1;
		txtDesc.CPUAccessFlags = 0;
		txtDesc.Width = _window->Client().Width;
		txtDesc.Height = _window->Client().Height;
		txtDesc.MiscFlags = 0;
		txtDesc.Usage = D3D11_USAGE_DEFAULT;
		txtDesc.SampleDesc.Count = _msaa;
		txtDesc.SampleDesc.Quality = 0;

		HR(_dxmodel->_device->CreateTexture2D, &txtDesc, NULL, t._model->_resource.Init());
		HR(_dxmodel->_device->CreateRenderTargetView, t._model->_resource, NULL, t._model->_renderTargetView.Init());
		HR(_dxmodel->_device->CreateShaderResourceView, t._model->_resource, NULL, t._model->_shaderResourceView.Init());

		t._device = this;
	}
}


void Devices::TextureBind2CS(Texture & t) {
	DD_DEBUG_ASSERT(t._model->_renderTargetView, "Texture does not have shader-resource-view initialized.");

	_dxmodel->_context->CSSetShaderResources(0, 1, t._model->_shaderResourceView.Address());
	_dxmodel->_context->CSSetSamplers(0, 1, t._model->_samplerState.Address());
}


void Devices::TextureBind2PS(Texture & t, u32 slot) {
	DD_DEBUG_ASSERT(t._model->_renderTargetView, "Texture does not have shader-resource-view initialized.");

	_dxmodel->_context->PSSetShaderResources(slot, 1, t._model->_shaderResourceView.Address());
	_dxmodel->_context->PSSetSamplers(slot, 1, t._model->_samplerState.Address());
}


void Devices::TextureBind2VS(Texture & t) {
	DD_DEBUG_ASSERT(t._model->_renderTargetView, "Texture does not have shader-resource-view initialized.");

	_dxmodel->_context->VSSetShaderResources(0, 1, t._model->_shaderResourceView.Address());
	_dxmodel->_context->VSSetSamplers(0, 1, t._model->_samplerState.Address());
}


void Devices::TextureBind2RT(Texture & t) {
	DD_DEBUG_ASSERT(t._model->_renderTargetView, "Texture does not have render-target-view initialized.");
	// resources must be unset
	ID3D11ShaderResourceView * const nullResource[8] = { NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, };
	_dxmodel->_context->VSSetShaderResources(0, 8, nullResource);
	_dxmodel->_context->PSSetShaderResources(0, 8, nullResource);
	_dxmodel->_context->CSSetShaderResources(0, 8, nullResource);
	// set new rt
	_dxmodel->_context->OMSetRenderTargets(1, t._model->_renderTargetView.Address(), _dxmodel->_depthStencilView);
}


void Devices::TextureClearRT(Texture & t, const float * color) {
	DD_DEBUG_ASSERT(t._model->_renderTargetView, "Texture does not have render-target-view initialized.");

	_dxmodel->_context->ClearRenderTargetView(t._model->_renderTargetView, color);
}


void Devices::SetPrimitiveTopology(PrimitiveTopologies topology) {
	if (topology != _topology) {
#if DD_DEVICE_STATISTIC
		switch (topology) {
		case Devices::PrimitiveTopologies::PointList:
			_statistics.Primitive = &_statistics.PointLists;
			break;
		case Devices::PrimitiveTopologies::LineList:
			_statistics.Primitive = &_statistics.LineLists;
			break;
		case Devices::PrimitiveTopologies::LineStrip:
			_statistics.Primitive = &_statistics.LineStrips;
			break;
		case Devices::PrimitiveTopologies::TriangleList:
			_statistics.Primitive = &_statistics.TriangleLists;
			break;
		case Devices::PrimitiveTopologies::TriangleStrip:
			_statistics.Primitive = &_statistics.TriangleStrips;
			break;
		default:
			throw;
		}
#endif

		_dxmodel->_context->IASetPrimitiveTopology(reinterpret_cast<D3D11_PRIMITIVE_TOPOLOGY&>(topology));
	}
}


void Devices::Flush() {
	_dxmodel->_context->Flush();
}


void Devices::DrawIndexed(u32 indexCount) {
#if DD_DEVICE_STATISTIC
	++_statistics.DrawCalls;
	*_statistics.Primitive += indexCount;
#endif

	_dxmodel->_context->DrawIndexed(indexCount, 0, 0);
}


void Devices::DrawIndexedInstanced(u32 indexCount, u32 instanceCount) {
#if DD_DEVICE_STATISTIC
	++_statistics.DrawCalls;
	*_statistics.Primitive += indexCount * instanceCount;
#endif

	_dxmodel->_context->DrawIndexedInstanced(indexCount, instanceCount, 0, 0, 0);
}


void Devices::Draw(u32 vertexCount) {
#if DD_DEVICE_STATISTIC
	++_statistics.DrawCalls;
	*_statistics.Primitive += vertexCount;
#endif

	_dxmodel->_context->Draw(vertexCount, 0);
}


void Devices::Present() {
	_dxmodel->_swapChain->Present(_vsync ? 1 : 0, 0);

	Device device(*this);
	auto proxy = _cmds.GetProxy();
	while (!proxy->IsEmpty()) {
		proxy->Front()->Run(device);
		proxy->PopFront();
	}
}


void Devices::AlphaBlending(Boolean::Value set) {
	_alpha = set;

	if (_alpha)
		_dxmodel->_context->OMSetBlendState(_dxmodel->_blendStateABEnabled, 0, 0xffffffff);
	else
		_dxmodel->_context->OMSetBlendState(_dxmodel->_blendStateABDisabled, 0, 0xffffffff);
}


bool Devices::AlphaBlending() {
	return _alpha;
}


void Devices::DepthBuffer(Boolean::Value set) {
	_depth = set;

	if (_depth) {
		_dxmodel->_depthStencilDesc.DepthEnable = true;
		_dxmodel->_depthStencilDesc.StencilEnable = true;
		_dxmodel->_device->CreateDepthStencilState(&_dxmodel->_depthStencilDesc, _dxmodel->_depthStencilState.Init());
		_dxmodel->_context->OMSetDepthStencilState(_dxmodel->_depthStencilState, 1);
	}
	else {
		_dxmodel->_depthStencilDesc.DepthEnable = false;
		_dxmodel->_depthStencilDesc.StencilEnable = false;
		_dxmodel->_device->CreateDepthStencilState(&_dxmodel->_depthStencilDesc, _dxmodel->_depthStencilState.Init());
		_dxmodel->_context->OMSetDepthStencilState(_dxmodel->_depthStencilState, 1);
	}
}


bool Devices::DepthBuffer() const {
	return _dxmodel->_depthStencilDesc.DepthEnable == TRUE;
}


void Devices::MultiSampling(MSAA msaa) {
	_msaa = msaa;
	Resize(_window->Client().Width, _window->Client().Height);
}


Devices::MSAA Devices::MultiSampling() const {
	return _msaa;
}


void Devices::Resize(size_t w, size_t h) {
	// check validity of arguments
	DD_ASSERT(
		h != 0 && w != 0,
		"The texture of render target must have non-zero dimensions. "
		"Function will not be processed.",
		LOG
	);

	if (h == 0 || w == 0)
		return;

	// unbind render target
	_dxmodel->_context->OMSetRenderTargets(0, 0, 0);

	// Release all outstanding references to the swap chain's buffers.
	_dxmodel->_rtv = nullptr;

	// Create a DirectX graphics interface factory.
	DXUnique<IDXGIFactory1> factory;
	HR(CreateDXGIFactory1, __uuidof(IDXGIFactory1), (void **)factory.Init());

	DXGI_SWAP_CHAIN_DESC swapChainDesc;
	swapChainDesc.BufferCount = 1;
	swapChainDesc.BufferDesc.Width = _window->Width();
	swapChainDesc.BufferDesc.Height = _window->Height();
	swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	swapChainDesc.BufferDesc.RefreshRate.Numerator = 0;
	swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.OutputWindow = (HWND)_window->NativeHandle();
	swapChainDesc.SampleDesc.Count = _msaa;
	swapChainDesc.SampleDesc.Quality = 0;
	swapChainDesc.Windowed = true;
	swapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	swapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	swapChainDesc.Flags = 0;
	HR(factory->CreateSwapChain, _dxmodel->_device, &swapChainDesc, _dxmodel->_swapChain.Init());

	// Preserve the existing buffer count and format.
	// Automatically choose the width and height to match the client rect for HWNDs.
	UINT items[]{ 0, 1, 2, 3 };
	for (UINT i : items) {
		HR(_dxmodel->_swapChain->ResizeBuffers, i, (UINT)w, (UINT)h, DXGI_FORMAT_R8G8B8A8_UNORM, 0);
	}

	// Get buffer and create a render-target-view.
	HR(_dxmodel->_swapChain->GetBuffer, 0, __uuidof(ID3D11Texture2D), (void**)&_dxmodel->_rtvTexture);
	// Perform error handling here!
	HR(_dxmodel->_device->CreateRenderTargetView, _dxmodel->_rtvTexture, NULL, _dxmodel->_rtv.Init());

	_dxmodel->RenderTarget._model->_renderTargetView = _dxmodel->_rtv;
	_dxmodel->RenderTarget._model->_resource = _dxmodel->_rtvTexture;
	_dxmodel->RenderTarget._device = this;
	_dxmodel->_context->OMSetRenderTargets(1, _dxmodel->_rtv.Address(), NULL);

	// Set up the viewport.
	D3D11_VIEWPORT vp;
	vp.Width = (FLOAT)w;
	vp.Height = (FLOAT)h;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	_dxmodel->_context->RSSetViewports(1, &vp);

	_dxmodel->initDepthBuffer((UINT)w, (UINT)h, _msaa);
}


bool Devices::Wireframe() const {
	ID3D11RasterizerState * rasterizer;
	_dxmodel->_context->RSGetState(&rasterizer);
	return rasterizer == _dxmodel->_wireframeEnabled;
}


void Devices::Wireframe(Boolean::Value set) {
	_wire = set;
	_dxmodel->_context->RSSetState(_wire ? _dxmodel->_wireframeEnabled : _dxmodel->_wireframeDisabled);
}


void Devices::VSync(Boolean::Value set) {
	_vsync = set;
}


Reference<ComputeShader> Devices::GetComputeShader(const Text::StringView16 & name) {
	Concurrency::CriticalSection::Scope _scope(_shaderLock);
	Reference<ComputeShader> & computeShader = _computeShaders[name];
	if (computeShader == nullptr) {
		computeShader = new ComputeShader(*this, name);
	}
	return computeShader;
}


Reference<PixelShader> Devices::GetPixelShader(const Text::StringView16 & name) {
	Concurrency::CriticalSection::Scope _scope(_shaderLock);
	Reference<PixelShader> & pixelShader = _pixelShaders[name];
	if (pixelShader == nullptr) {
		pixelShader = new PixelShader(*this, name);
	}
	return pixelShader;
}


Reference<Texture> Devices::GetTexture(const Text::StringView16 & file) {
	Concurrency::CriticalSection::Scope _scope(_shaderLock);
	Reference<Texture> texture = _textures[file];
	if (texture == nullptr) {
		texture = new Texture(file);
		texture->Init(*this);
	}
	return texture;
}


/************************************************************************/
/* ASYNC INTERFACE                                                      */
/************************************************************************/
Concurrency::Awaitable<Reference<Devices::IScreenshot>> Devices::CaptureScreenshotAsync() {
	Concurrency::Awaitable<Reference<Devices::IScreenshot>> result;
	AddCommand([=](Device & d) mutable { result = d->CaptureScreenshot(); });
	return result;
}


void Devices::CaptureScreenshotAsync(const FileSystem::Path & path) {
	Reference<FileSystem::Path> pathR = new FileSystem::Path(path);
  AddCommand([pathR](Device & d) mutable { d->CaptureScreenshot(*pathR); });
}


Concurrency::AwaitBool Devices::AlphaBlendingAsync() {
	Concurrency::AwaitBool result;
	AddCommand([=](Device & d) mutable { result = d->AlphaBlending(); });
	return result;
}


Concurrency::AwaitBool Devices::DepthBufferAsync() {
	Concurrency::AwaitBool result;
	AddCommand([=](Device & d) mutable { result = d->DepthBuffer(); });
	return result;
}


Concurrency::AwaitBool Devices::VSyncAsync() {
	Concurrency::AwaitBool result;
	AddCommand([=](Device & d) mutable { result = d->VSync(); });
	return result;
}


Concurrency::AwaitBool Devices::WireframeAsync() {
	Concurrency::AwaitBool result;
	AddCommand([=](Device & d) mutable { result = d->Wireframe(); });
	return result;
}


void Devices::ResizeAsync(size_t w, size_t h) {
	AddCommand([=](Device & d) { d->Resize(w, h); });
}


void Devices::AlphaBlendingAsync(Boolean::Value set) {
	AddCommand([=](Device & d) { d->AlphaBlending(set); });
}


void Devices::DepthBufferAsync(Boolean::Value set) {
	AddCommand([=](Device & d) { d->DepthBuffer(set); });
}


void Devices::VSyncAsync(Boolean::Value set) {
	AddCommand([=](Device & d) {d->VSync(set); });
}


void Devices::WireframeAsync(Boolean::Value set) {
	AddCommand([=](Device & d) { d->Wireframe(set); });
}


Devices::AsyncInterface Devices::Async() {
	return AsyncInterface(*this);
}


Devices::~Devices() {
#if _DEBUG
	ReportLiveObjects();
#endif
}


/************************************************************************/
/* Device                                                               */
/************************************************************************/
Device & Device::operator=(const Device & lvalue) {
	*this = nullptr;
	_device = lvalue._device;
	_device->_references.AtomicInc();

	return *this;
}


Device & Device::operator=(Device && rvalue) {
	Devices * swp;
	swp = _device;
	_device = rvalue._device;
	rvalue._device = swp;

	return *this;
}


Device & Device::operator=(Devices * ptr) {
	*this = nullptr;
	if (ptr) {
		ptr->_references.AtomicInc();
		_device = ptr;
	}

	return *this;
}


Device & Device::operator=(nullptr_t) {
	if (_device) {
		i32 i = _device->_references.AtomicDec();
		if (i == 0) delete _device;
		_device = 0;
	}

	return *this;
}


Device::Device(Devices * device)
	: Device()
{
	*this = device;
}


Device::Device(const Device & device)
	: Device()
{
	*this = device;
}


Device::Device(Device && device)
	: Device()
{
	*this = device;
}


Device::~Device() {
	*this = nullptr;
}
