#include <DD/Net/JSON.h>

using namespace DD::Net::JSON;

template<>
void Reader::ReadLetter<Reader::Stages::_Autoselect>(char letter) {
	switch (_stages.Last()) {
	case Stages::_Object_0_Begin:
		ReadLetter<Reader::Stages::_Object_0_Begin>(letter);
		break;
	case Stages::_Object_1_WhitespaceBeforeName:
		ReadLetter <Reader::Stages::_Object_1_WhitespaceBeforeName>(letter);
		break;
	case Stages::_Object_2_Name:
		ReadLetter <Reader::Stages::_Object_2_Name>(letter);
		break;
	case Stages::_Object_3_WhitespaceAfterName:
		ReadLetter <Reader::Stages::_Object_3_WhitespaceAfterName>(letter);
		break;
	case Stages::_Object_4_WhitespaceBeforeValue:
		ReadLetter <Reader::Stages::_Object_4_WhitespaceBeforeValue>(letter);
		break;
	case Stages::_Object_5_WhitespaceAfterValue:
		ReadLetter <Reader::Stages::_Object_5_WhitespaceAfterValue>(letter);
		break;
	case Stages::_Array_0_Begin:
		ReadLetter <Reader::Stages::_Array_0_Begin>(letter);
		break;
	case Stages::_Array_1_WhitespaceAfterValue:
		ReadLetter <Reader::Stages::_Array_1_WhitespaceAfterValue>(letter);
		break;
	case Stages::_String_0_Letters:
		ReadLetter <Reader::Stages::_String_0_Letters>(letter);
		break;
	case Stages::_Value_0_Whitespace:
		ReadLetter <Reader::Stages::_Value_0_Whitespace>(letter);
		break;
	case Stages::_Number_0_Numbers:
		ReadLetter <Reader::Stages::_Number_0_Numbers>(letter);
		break;
	case Stages::_Literal_0_Letters:
		ReadLetter <Reader::Stages::_Literal_0_Letters>(letter);
		break;
	}
}


template<>
void Reader::ReadLetter<Reader::Stages::_Object_0_Begin>(char letter) {
	switch (letter) {
	case ' ': case '\r': case '\n': case '\t': break;
	case '"': _stages.Last() = Stages::_Object_2_Name; break;
	case '}': _stages.RemoveLast(); break;
	default: _stages.Add(Stages::_Error); break;
	}
}

template<>
void Reader::ReadLetter<Reader::Stages::_Object_1_WhitespaceBeforeName>(char letter) {
	switch (letter) {
	case ' ': case '\r': case '\n': case '\t': break;
	case '"': _stages.Last() = Stages::_Object_2_Name; break;
	default: _stages.Add(Stages::_Error); break;
	}
}

template<>
void Reader::ReadLetter<Reader::Stages::_Object_2_Name>(char letter) {
	switch (letter) {
	case '"':
		_stages.Last() = Stages::_Object_3_WhitespaceAfterName;
		_target = (Any **)_current->AsMap()->Value.Append().Address();
		Create(new Member);
		_current->AsMember()->Name = DD::Text::StringView8(_stringBuffer.begin(), _stringBuffer.Size());
		_target = _current->AsMember()->Value.Address();
		_stringBuffer.Clear();
		break;
	default:
		_stringBuffer.Add(letter);
		break;
	}
}

template<>
void Reader::ReadLetter<Reader::Stages::_Object_3_WhitespaceAfterName>(char letter) {
	switch (letter) {
	case ' ': case '\r': case '\n': case '\t': break;
	case ':': _stages.Last() = Stages::_Object_4_WhitespaceBeforeValue; break;
	default: _stages.Add(Stages::_Error); break;
	}
}

template<>
void Reader::ReadLetter<Reader::Stages::_Object_4_WhitespaceBeforeValue>(char letter) {
	switch (letter)
	{
	case ' ': case '\r': case '\n': case '\t': break;
	default:
		_stages.Last() = Stages::_Object_5_WhitespaceAfterValue;
		_stages.Add(Stages::_Value_0_Whitespace);
		ReadLetter<Reader::Stages::_Value_0_Whitespace>(letter);
		break;
	}
}

template<>
void Reader::ReadLetter<Reader::Stages::_Object_5_WhitespaceAfterValue>(char letter) {
	switch (letter) {
	case ' ': case '\r': case '\n': case '\t': break;
	case ',':
		_stages.Last() = Stages::_Object_1_WhitespaceBeforeName;
		_current = _current->Parent;
		break;
	case '}':
		_stages.RemoveLast();
		_current = _current->Parent;
		_current = _current->Parent;
		break;
	default: _stages.Add(Stages::_Error); break;
	}
}

template<>
void Reader::ReadLetter<Reader::Stages::_Value_0_Whitespace>(char letter) {
	switch (letter) {
	case ' ': case '\r': case '\n': case '\t': break;
	case '[':
		_stages.Last() = Stages::_Array_0_Begin;
		Create(new Array);
		break;
	case '{':
		_stages.Last() = Stages::_Object_0_Begin;
		Create(new Map);
		break;
	case '"':
		_stages.Last() = Stages::_String_0_Letters;
		Create(new String);
		break;
	case '-': case '0': case '1': case '2': case '3':
	case '4': case '5': case '6': case '7': case '8':
	case '9':
		_stages.Last() = Stages::_Number_0_Numbers;
		Create(new Number);
		ReadLetter<Reader::Stages::_Number_0_Numbers>(letter);
		break;
	case 't': case 'f': case 'n':
		_stages.Last() = Stages::_Literal_0_Letters;
		Create(new Literal);
		ReadLetter<Reader::Stages::_Literal_0_Letters>(letter);
		break;
	default:
		_stages.Add(Reader::Stages::_Error);
		break;
	}
}

template<>
void Reader::ReadLetter<Reader::Stages::_String_0_Letters>(char letter) {
	switch (letter) {
	case '"':
		_current->AsString()->Value = DD::Text::StringView8(_stringBuffer.begin(), _stringBuffer.Size());
		_stringBuffer.Clear();
		_stages.RemoveLast();
		_current = _current->Parent;
		break;
	default:
		_stringBuffer.Add(letter);
		break;
	}
}

template<>
void Reader::ReadLetter<Reader::Stages::_Array_0_Begin>(char letter) {
	switch (letter) {
	case ' ': case '\r': case '\n': case '\t': break;
	case ']':
		_current = _current->Parent;
		_stages.RemoveLast();
		break;
	default:
		_target = _current->AsArray()->Value.Append().Address();
		_stages.Last() = Stages::_Array_1_WhitespaceAfterValue;
		_stages.Add(Stages::_Value_0_Whitespace);
		ReadLetter<Reader::Stages::_Value_0_Whitespace>(letter);
		break;
	}
}

template<>
void Reader::ReadLetter<Reader::Stages::_Array_1_WhitespaceAfterValue>(char letter) {
	switch (letter) {
	case ' ': case '\r': case '\n': case '\t': break;
	case ']':
		_current = _current->Parent;
		_stages.RemoveLast();
		break;
	case ',':
		_target = _current->AsArray()->Value.Append().Address();
		_stages.Add(Stages::_Value_0_Whitespace);
		break;
	default:
		_stages.Add(Stages::_Error);
		break;
	}
}

template<>
void Reader::ReadLetter<Reader::Stages::_Number_0_Numbers>(char letter) {
	switch (letter) {
	case '-': case '0': case '1': case '2': case '3':
	case '4': case '5': case '6': case '7': case '8':
	case '9': case '.':
		_stringBuffer.Add(letter);
		break;
	default:
		if (!DD::Text::StringView8(_stringBuffer.begin(), _stringBuffer.Size()).TryCast(_current->AsNumber()->Value)) {
			_stages.Add(Stages::_Error);
		}
		else {
			_stringBuffer.Clear();
			_stages.RemoveLast();
			_current = _current->Parent;
			ReadLetter(letter);
		}
		break;
	}
}

template<>
void Reader::ReadLetter<Reader::Stages::_Literal_0_Letters>(char letter) {
	// letters of true, false or null
	switch (letter) {
	case 'a': case 'e': case 'f': case 'l':
	case 'n': case 'r': case 's': case 't':
	case 'u':
		_stringBuffer.Add(letter);
		break;
	default:
		_stages.RemoveLast();
		DD::Text::StringView8 text(_stringBuffer.begin(), _stringBuffer.Size());
		if (text == "true")
			_current->AsLiteral()->Value = Literals::True;
		else if (text == "false")
			_current->AsLiteral()->Value = Literals::False;
		else if (text == "null")
			_current->AsLiteral()->Value = Literals::Null;
		else
			_stages.Add(Reader::Stages::_Error);

		_stringBuffer.Clear();

		_current = _current->Parent;
		ReadLetter(letter);
		break;
	}
}

void Reader::ReadLetter(char letter) {
	ReadLetter<Stages::_Autoselect>(letter);
}

void Reader::Create(Any * abstract) {
	*_target = abstract;
	abstract->Parent = _current;
	_current = abstract;
}

Any * Any::GetMember(const DD::Text::StringView8 & name) {
	if (Type == Types::Map)
		return static_cast<Map *>(this)->Get(name);

	return nullptr;
}

template<typename JSON_TYPE>
auto & SafeIndex(JSON_TYPE * json, size_t index) {
	return index < json->Value.Size() ? json->Value[index] : nullptr;
}

Any * Any::GetIndex(size_t index) {
	switch (Type) {
	case Types::Array:
		return index < AsArray()->Value.Size() ? AsArray()->Value[index].Ptr() : nullptr;
	case Types::Map:
		return index < AsMap()->Value.Size() ? AsMap()->Value[index].Ptr() : nullptr;
	default:
		return nullptr;
	}
}

size_t Any::GetSize() {
	switch (Type) {
	case Types::Array:
		return AsArray()->Value.Size();
	case Types::Map:
		return AsMap()->Value.Size();
	default:
		return 0;
	}
}

DD::f64 Any::GetNumber() {
	switch (Type)
	{
	case DD::Net::JSON::Types::Number:
		return AsNumber()->Value;
	default:
		return 0.0;
	}
}

DD::Text::StringView16 Any::GetString() {
	switch (Type) {
	case Types::String:
		return AsString()->Value;
	default:
		return nullptr;
	}
}

DD::Net::JSON::Any * Any::GetLast() {
	switch (Type) {
	case Types::Array:
		return AsArray()->Value.Size() ? AsArray()->Value.Last().Ptr() : nullptr;
	default:
		return 0;
	}
}

DD::Net::JSON::Any * Any::GetLast(size_t nth) {
	switch (Type) {
	case Types::Array:
		return nth < AsArray()->Value.Size() ? AsArray()->Value.Last(nth).Ptr() : nullptr;
	default:
		return 0;
	}
}

DD::Net::JSON::Any * Any::GetFirst() {
	return AsArray()->Value.Size() ? AsArray()->Value[0].Ptr() : nullptr;
}

DD::Net::JSON::Array * Any::AsArray() {
	return static_cast<Array *>(this);
}

Number * DD::Net::JSON::Any::AsNumber() {
	return static_cast<Number *>(this);
}

Map * DD::Net::JSON::Any::AsMap() {
	return static_cast<Map *>(this);
}

Member * DD::Net::JSON::Any::AsMember() {
	return static_cast<Member *>(this);
}

String * DD::Net::JSON::Any::AsString() {
	return static_cast<String *>(this);
}

Literal * DD::Net::JSON::Any::AsLiteral() {
	return static_cast<Literal *>(this);
}

bool Any::IsTrue() {
	return Type == Types::Literal && AsLiteral()->Value == Literals::True;
}

bool Any::IsFalse() {
	return Type == Types::Literal && AsLiteral()->Value == Literals::False;
}

bool Any::IsNull() {
	return Type == Types::Literal && AsLiteral()->Value == Literals::Null;
}

DD::Net::JSON::Any * Map::Get(const DD::Text::StringView8 & name) {
	for (Unique<Member> & member : Value)
		if (member->Name == name)
			return member->Value;

	return nullptr;
}
