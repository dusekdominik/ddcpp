#pragma comment(lib, "Dwrite")
#pragma comment(lib, "d2d1")
#pragma comment(lib, "Shcore.lib")

#include <DD/Gpu/D2D.h>
#include <DD/Debug.h>
#include <DD/Primitives.h>
#include <DD/Unique.h>

#include <ShellScalingApi.h>
#include <dxgi.h>
#include <dwrite.h>
#include <d2d1.h>
#include <d2d1_1.h>

// D2D macro
#undef DrawText

/** Winapi HRESULT validation call. */
#define HR(call, ...) {                                      \
	HRESULT hr = call(__VA_ARGS__);                            \
  if (FAILED(hr))                                            \
    DD_WARNING(#call " failed with ErrorCode = ", (int)hr); \
}


using namespace DD;
using namespace DD::Gpu;

/************************************************************************/
/* Helpers                                                              */
/************************************************************************/
__forceinline static void parseSize(
	/*IN*/ const D2D::Sizes & source,
	/*OUT*/ f32 & target
) {
	switch (source.Mode) {
		case D2D::SSModes::Pixel:
			target = source.Value;
			break;
		case D2D::SSModes::Interval:
			DD_WARNING("Not implemented.");
			break;
		case D2D::SSModes::Density:
			DD_WARNING("Not implemented.");
			break;
		default:
			DD_WARNING("Undefined SSMode.");
			break;
	}
}

__forceinline static void parsePoint(
	/*IN*/ const D2D::HAligns & hAlign,
	/*IN*/ const D2D::VAligns & vAlign,
	/*IN*/ const float2 & point,
	/*IN*/ const D2D1_SIZE_F & size,
	/*OUT*/ D2D1_POINT_2F & target
) {
	// parse x
	switch (hAlign) {
		case D2D::HAligns::HLeft:   target.x = point.x; break;
		case D2D::HAligns::HCenter: target.x = point.x + size.width * 0.5f; break;
		case D2D::HAligns::HRight:  target.x = point.x + size.width; break;
		default: DD_WARNING("D2D Invalid HAlign value."); break;
	}
	// parse y
	switch (vAlign) {
		case D2D::VAligns::VTop:    target.y = point.y; break;
		case D2D::VAligns::VCenter: target.y = point.y + size.height * 0.5f; break;
		case D2D::VAligns::VBottom: target.y = point.y + size.height; break;
		default: DD_WARNING("D2D Invalid VAlign value."); break;
	}
}

__forceinline static void parseRect(
	/*IN*/ const D2D::Boxes & box,
	/*IN*/ const D2D1_SIZE_F & size,
	/*OUT*/ D2D1_RECT_F & target
) {
	f32 l, r, b, t;
	switch (box.Mode) {
		case D2D::SSModes::Pixel:
			l = box.Origins.x;
			r = box.Origins.x + box.Dimensions.x;
			t = box.Origins.y;
			b = box.Origins.y + box.Dimensions.y;
			break;
		case D2D::SSModes::Interval:
			l = box.Origins.x * size.width;
			r = (box.Origins.x + box.Dimensions.x) * size.width;
			t = box.Origins.y * size.height;
			b = (box.Origins.y + box.Dimensions.y) * size.height;
			break;
		case D2D::SSModes::Density:
			DD_WARNING("Not implemented.");
			break;
		default:
			DD_WARNING("Undefined SSMode.");
			break;
	}

	switch (box.HAlign) {
		case D2D::HAligns::HLeft: {
			target.left = l;
			target.right = r;
		} break;
		case D2D::HAligns::HCenter: {
			f32 offset = (size.width - (r - l)) * 0.5f;
			target.left = l + offset;
			target.right = r + offset;
		} break;
		case D2D::HAligns::HRight: {
			f32 offset = size.width - (r - l);
			target.left = l + offset;
			target.right = r + offset;
		} break;
		default: {
			DD_WARNING("Undefined HAlign.");
		} break;
	}

	f32 y = b - t;
	switch (box.VAlign) {
		case D2D::VAligns::VTop: {
			target.top = t;
			target.bottom = b;
		} break;
		case D2D::VAligns::VCenter: {
			f32 offset = (size.height - (b - t)) * 0.5f;
			target.top = t + offset;
			target.bottom = b + offset;
		} break;
		case D2D::VAligns::VBottom: {
			f32 offset = (size.height - (b - t));
			target.top = t + offset;
			target.bottom = b + offset;
		} break;
		default: {
			DD_WARNING("Undefined VAlign.");
		} break;
	}
}

__forceinline static DWRITE_TEXT_ALIGNMENT parseTHAligns(D2D::THAligns align) {
	switch(align) {
		case D2D::THAligns::TLeft:
			return DWRITE_TEXT_ALIGNMENT::DWRITE_TEXT_ALIGNMENT_LEADING;
		case D2D::THAligns::TCenter:
			 return DWRITE_TEXT_ALIGNMENT::DWRITE_TEXT_ALIGNMENT_CENTER;
		case D2D::THAligns::TRight:
			return DWRITE_TEXT_ALIGNMENT::DWRITE_TEXT_ALIGNMENT_TRAILING;
		case D2D::THAligns::TJustify:
			return DWRITE_TEXT_ALIGNMENT::DWRITE_TEXT_ALIGNMENT_JUSTIFIED;
		default:
			DD_WARNING("Undefined case.");
			return DWRITE_TEXT_ALIGNMENT();
	}
}

__forceinline DWRITE_PARAGRAPH_ALIGNMENT parseTVAligns(D2D::TVAligns align) {
	switch (align) {
		case D2D::TVAligns::TTop:
			return DWRITE_PARAGRAPH_ALIGNMENT::DWRITE_PARAGRAPH_ALIGNMENT_NEAR;
		case D2D::TVAligns::TBottom:
			return DWRITE_PARAGRAPH_ALIGNMENT::DWRITE_PARAGRAPH_ALIGNMENT_FAR;
		case D2D::TVAligns::TCenter:
			return DWRITE_PARAGRAPH_ALIGNMENT::DWRITE_PARAGRAPH_ALIGNMENT_CENTER;
		default:
			DD_WARNING("Undefined case.");
			return DWRITE_PARAGRAPH_ALIGNMENT();
	}
}
/************************************************************************/
/* DXModel                                                              */
/************************************************************************/

/** Release deallocation for DX objects. */
struct DXDeallocator {
	template<typename T>
	__forceinline static void Delete(T *& ref) { ref->Release(); }
};

/** Reference type for DX objects. */
template<typename T>
using DXReference = Unique<T, DXDeallocator>;

struct D2D::DXMemoryModel {
	/** Lock for creation of objects. */
	Concurrency::CriticalSection _lockBrushes, _lockFonts;
	/** Font collection. */
	BTree<D2D::Fonts, DXReference<IDWriteTextFormat>> _fonts;
	/** Brush collection. */
	BTree<Color, DXReference<ID2D1Brush>> _brushes;
	/** Default font format. */
	DXReference<IDWriteTextFormat> _defaultFormat;
	/** Default font color. */
	DXReference<ID2D1Brush> _defaultBrush;
	/** Default brush of background. */
	DXReference<ID2D1Brush> _defaultBackgroundBrush;
	/** Factory for creating all subsequent DirectWrite objects*/
	DXReference<IDWriteFactory> _writerFactory;
	/** Factory for creating Direct2D resources.*/
	DXReference<ID2D1Factory> _factory;
	/** Object that can receive drawing commands.  */
	DXReference<ID2D1RenderTarget> _renderTarget;
	/** Target texture. */
	DXReference<IDXGISurface> _backBuffer;
	/** Create or look-up brush by its color. */
	ID2D1Brush * GetBrush(Color color);
	/** Create or look-up brush by its font. */
	IDWriteTextFormat * GetFont(const D2D::Fonts & font);
	/** Constructor. */
	DXMemoryModel()
		: _writerFactory()
		, _factory()
		, _renderTarget()
		, _backBuffer()
		, _defaultFormat()
		, _defaultBrush()
		, _lockBrushes(10000)
		, _lockFonts(10000)
	{}
};

ID2D1Brush * D2D::DXMemoryModel::GetBrush(Color color) {
	Concurrency::CriticalSection::Scope lock(_lockBrushes);
	DXReference<ID2D1Brush> & brush = _brushes[color];
	if (brush == nullptr) {
		ID2D1SolidColorBrush * tmpBrush;
		_renderTarget->CreateSolidColorBrush(
			reinterpret_cast<D2D1::ColorF &>((float4)color),
			&tmpBrush
		);
		brush = tmpBrush;
	}
	return brush;
}

IDWriteTextFormat * D2D::DXMemoryModel::GetFont(const D2D::Fonts & font) {
	Concurrency::CriticalSection::Scope lock(_lockFonts);
	DXReference<IDWriteTextFormat> & format = _fonts[font];
	if (format == nullptr) {
		IDWriteTextFormat * tmpFormat;
		f32 width;
		parseSize(font->Size, width);
		_writerFactory->CreateTextFormat(
			font->Name,
			NULL,
			(DWRITE_FONT_WEIGHT)(u32)font->Weight,
			(DWRITE_FONT_STYLE)(u32)font->Style,
			(DWRITE_FONT_STRETCH)(u32)font->Stretch,
			width,
			font->Locale,
			&tmpFormat
		);
		format = tmpFormat;
	}
	return format;
}

/************************************************************************/
/* Fonts                                                                */
/************************************************************************/
D2D::Fonts D2D::Fonts::COURIER_12_BLACK      (L"Courier New", 12.f, Color::Collection::BLACK,  D2D::FWeights::Normal, D2D::FStyles::Normal, D2D::FStretches::Normal);
D2D::Fonts D2D::Fonts::COURIER_12_BLUE       (L"Courier New", 12.f, Color::Collection::BLUE,   D2D::FWeights::Normal, D2D::FStyles::Normal, D2D::FStretches::Normal);
D2D::Fonts D2D::Fonts::COURIER_12_GREEN      (L"Courier New", 12.f, Color::Collection::GREEN,  D2D::FWeights::Normal, D2D::FStyles::Normal, D2D::FStretches::Normal);
D2D::Fonts D2D::Fonts::COURIER_12_RED        (L"Courier New", 12.f, Color::Collection::RED,    D2D::FWeights::Normal, D2D::FStyles::Normal, D2D::FStretches::Normal);
D2D::Fonts D2D::Fonts::COURIER_12_YELLOW     (L"Courier New", 12.f, Color::Collection::YELLOW, D2D::FWeights::Normal, D2D::FStyles::Normal, D2D::FStretches::Normal);
D2D::Fonts D2D::Fonts::COURIER_12_WHITE      (L"Courier New", 12.f, Color::Collection::WHITE,  D2D::FWeights::Normal, D2D::FStyles::Normal, D2D::FStretches::Normal);
D2D::Fonts D2D::Fonts::COURIER_18_BLACK      (L"Courier New", 18.f, Color::Collection::BLACK,  D2D::FWeights::Normal, D2D::FStyles::Normal, D2D::FStretches::Normal);
D2D::Fonts D2D::Fonts::COURIER_18_BLUE       (L"Courier New", 18.f, Color::Collection::BLUE,   D2D::FWeights::Normal, D2D::FStyles::Normal, D2D::FStretches::Normal);
D2D::Fonts D2D::Fonts::COURIER_18_GREEN      (L"Courier New", 18.f, Color::Collection::GREEN,  D2D::FWeights::Normal, D2D::FStyles::Normal, D2D::FStretches::Normal);
D2D::Fonts D2D::Fonts::COURIER_18_RED        (L"Courier New", 18.f, Color::Collection::RED,    D2D::FWeights::Normal, D2D::FStyles::Normal, D2D::FStretches::Normal);
D2D::Fonts D2D::Fonts::COURIER_18_YELLOW     (L"Courier New", 18.f, Color::Collection::YELLOW, D2D::FWeights::Normal, D2D::FStyles::Normal, D2D::FStretches::Normal);
D2D::Fonts D2D::Fonts::COURIER_18_YELLOW_BOLD(L"Courier New", 18.f, Color::Collection::YELLOW, D2D::FWeights::Bold,   D2D::FStyles::Normal, D2D::FStretches::Normal);
D2D::Fonts D2D::Fonts::COURIER_18_WHITE      (L"Courier New", 18.f, Color::Collection::WHITE,  D2D::FWeights::Normal, D2D::FStyles::Normal, D2D::FStretches::Normal);
D2D::Fonts D2D::Fonts::COURIER_24_BLACK      (L"Courier New", 24.f, Color::Collection::BLACK,  D2D::FWeights::Normal, D2D::FStyles::Normal, D2D::FStretches::Normal);
D2D::Fonts D2D::Fonts::COURIER_24_BLUE       (L"Courier New", 24.f, Color::Collection::BLUE,   D2D::FWeights::Normal, D2D::FStyles::Normal, D2D::FStretches::Normal);
D2D::Fonts D2D::Fonts::COURIER_24_GREEN      (L"Courier New", 24.f, Color::Collection::GREEN,  D2D::FWeights::Normal, D2D::FStyles::Normal, D2D::FStretches::Normal);
D2D::Fonts D2D::Fonts::COURIER_24_RED        (L"Courier New", 24.f, Color::Collection::RED,    D2D::FWeights::Normal, D2D::FStyles::Normal, D2D::FStretches::Normal);
D2D::Fonts D2D::Fonts::COURIER_24_YELLOW     (L"Courier New", 24.f, Color::Collection::YELLOW, D2D::FWeights::Normal, D2D::FStyles::Normal, D2D::FStretches::Normal);
D2D::Fonts D2D::Fonts::COURIER_24_WHITE      (L"Courier New", 24.f, Color::Collection::WHITE,  D2D::FWeights::Normal, D2D::FStyles::Normal, D2D::FStretches::Normal);
D2D::Fonts D2D::Fonts::COURIER_28_BLACK      (L"Courier New", 28.f, Color::Collection::BLACK,  D2D::FWeights::Normal, D2D::FStyles::Normal, D2D::FStretches::Normal);
D2D::Fonts D2D::Fonts::COURIER_28_BLUE       (L"Courier New", 28.f, Color::Collection::BLUE,   D2D::FWeights::Normal, D2D::FStyles::Normal, D2D::FStretches::Normal);
D2D::Fonts D2D::Fonts::COURIER_28_GREEN      (L"Courier New", 28.f, Color::Collection::GREEN,  D2D::FWeights::Normal, D2D::FStyles::Normal, D2D::FStretches::Normal);
D2D::Fonts D2D::Fonts::COURIER_28_RED        (L"Courier New", 28.f, Color::Collection::RED,    D2D::FWeights::Normal, D2D::FStyles::Normal, D2D::FStretches::Normal);
D2D::Fonts D2D::Fonts::COURIER_28_YELLOW     (L"Courier New", 28.f, Color::Collection::YELLOW, D2D::FWeights::Normal, D2D::FStyles::Normal, D2D::FStretches::Normal);
D2D::Fonts D2D::Fonts::COURIER_28_WHITE      (L"Courier New", 28.f, Color::Collection::WHITE,  D2D::FWeights::Normal, D2D::FStyles::Normal, D2D::FStretches::Normal);
D2D::Fonts D2D::Fonts::COURIER_32_BLACK      (L"Courier New", 32.f, Color::Collection::BLACK,  D2D::FWeights::Normal, D2D::FStyles::Normal, D2D::FStretches::Normal);
D2D::Fonts D2D::Fonts::COURIER_32_BLUE       (L"Courier New", 32.f, Color::Collection::BLUE,   D2D::FWeights::Normal, D2D::FStyles::Normal, D2D::FStretches::Normal);
D2D::Fonts D2D::Fonts::COURIER_32_GREEN      (L"Courier New", 32.f, Color::Collection::GREEN,  D2D::FWeights::Normal, D2D::FStyles::Normal, D2D::FStretches::Normal);
D2D::Fonts D2D::Fonts::COURIER_32_RED        (L"Courier New", 32.f, Color::Collection::RED,    D2D::FWeights::Normal, D2D::FStyles::Normal, D2D::FStretches::Normal);
D2D::Fonts D2D::Fonts::COURIER_32_YELLOW     (L"Courier New", 32.f, Color::Collection::YELLOW, D2D::FWeights::Normal, D2D::FStyles::Normal, D2D::FStretches::Normal);
D2D::Fonts D2D::Fonts::COURIER_32_WHITE      (L"Courier New", 32.f, Color::Collection::WHITE,  D2D::FWeights::Normal, D2D::FStyles::Normal, D2D::FStretches::Normal);

D2D::Fonts D2D::Fonts::DEFAULT_BLACK_SMALL    = D2D::Fonts::COURIER_12_BLACK;
D2D::Fonts D2D::Fonts::DEFAULT_GREEN_SMALL    = D2D::Fonts::COURIER_12_GREEN;
D2D::Fonts D2D::Fonts::DEFAULT_RED_SMALL      = D2D::Fonts::COURIER_12_RED;
D2D::Fonts D2D::Fonts::DEFAULT_WHITE_SMALL    = D2D::Fonts::COURIER_12_WHITE;
D2D::Fonts D2D::Fonts::DEFAULT_YELLOW_SMALL   = D2D::Fonts::COURIER_12_YELLOW;
D2D::Fonts D2D::Fonts::DEFAULT_BLACK_SMALLER  = D2D::Fonts::COURIER_18_BLACK;
D2D::Fonts D2D::Fonts::DEFAULT_BLUE_SMALLER   = D2D::Fonts::COURIER_18_BLUE;
D2D::Fonts D2D::Fonts::DEFAULT_GREEN_SMALLER  = D2D::Fonts::COURIER_18_GREEN;
D2D::Fonts D2D::Fonts::DEFAULT_RED_SMALLER    = D2D::Fonts::COURIER_18_RED;
D2D::Fonts D2D::Fonts::DEFAULT_WHITE_SMALLER  = D2D::Fonts::COURIER_18_WHITE;
D2D::Fonts D2D::Fonts::DEFAULT_YELLOW_SMALLER = D2D::Fonts::COURIER_18_YELLOW;
D2D::Fonts D2D::Fonts::DEFAULT_BLACK          = D2D::Fonts::COURIER_24_BLACK;
D2D::Fonts D2D::Fonts::DEFAULT_BLUE           = D2D::Fonts::COURIER_24_BLUE;
D2D::Fonts D2D::Fonts::DEFAULT_GREEN          = D2D::Fonts::COURIER_24_GREEN;
D2D::Fonts D2D::Fonts::DEFAULT_RED            = D2D::Fonts::COURIER_24_RED;
D2D::Fonts D2D::Fonts::DEFAULT_YELLOW         = D2D::Fonts::COURIER_24_YELLOW;
D2D::Fonts D2D::Fonts::DEFAULT_WHITE          = D2D::Fonts::COURIER_24_WHITE;
D2D::Fonts D2D::Fonts::DEFAULT_BLACK_GREATER  = D2D::Fonts::COURIER_28_BLACK;
D2D::Fonts D2D::Fonts::DEFAULT_BLUE_GREATER   = D2D::Fonts::COURIER_28_BLUE;
D2D::Fonts D2D::Fonts::DEFAULT_GREEN_GREATER  = D2D::Fonts::COURIER_28_GREEN;
D2D::Fonts D2D::Fonts::DEFAULT_RED_GREATER    = D2D::Fonts::COURIER_28_RED;
D2D::Fonts D2D::Fonts::DEFAULT_YELLOW_GREATER = D2D::Fonts::COURIER_28_YELLOW;
D2D::Fonts D2D::Fonts::DEFAULT_WHITE_GREATER  = D2D::Fonts::COURIER_28_WHITE;
D2D::Fonts D2D::Fonts::DEFAULT_BLACK_GREAT    = D2D::Fonts::COURIER_32_BLACK;
D2D::Fonts D2D::Fonts::DEFAULT_BLUE_GREAT     = D2D::Fonts::COURIER_32_BLUE;
D2D::Fonts D2D::Fonts::DEFAULT_GREEN_GREAT    = D2D::Fonts::COURIER_32_GREEN;
D2D::Fonts D2D::Fonts::DEFAULT_RED_GREAT      = D2D::Fonts::COURIER_32_RED;
D2D::Fonts D2D::Fonts::DEFAULT_WHITE_GREAT    = D2D::Fonts::COURIER_32_WHITE;
D2D::Fonts D2D::Fonts::DEFAULT_YELLOW_GREAT   = D2D::Fonts::COURIER_32_YELLOW;
D2D::Fonts D2D::Fonts::DEFAULT_SMALL          = D2D::Fonts::DEFAULT_BLACK_SMALL;
D2D::Fonts D2D::Fonts::DEFAULT_SMALLER        = D2D::Fonts::DEFAULT_BLACK_SMALLER;
D2D::Fonts D2D::Fonts::DEFAULT                = D2D::Fonts::DEFAULT_BLACK;
D2D::Fonts D2D::Fonts::DEFAULT_GREATER        = D2D::Fonts::DEFAULT_BLACK_GREATER;
D2D::Fonts D2D::Fonts::DEFAULT_GREAT          = D2D::Fonts::DEFAULT_BLACK_GREAT;

/************************************************************************/
/* D2D                                                                  */
/************************************************************************/
void D2D::Draw() {
	Concurrency::Lock::ExclusiveScope scope(_lock);
	// enable drawing
	_dx->_renderTarget->BeginDraw();
	// lock command buffer
	auto proxy = _cmds.GetProxy();
	// process draw commands
	for (Action64<D2D&> & command : *proxy) {
		command->Run(*this);
	}
	// remove processed commands
	proxy->Clear();
	// disable drawing
	_dx->_renderTarget->EndDraw();
}

void D2D::DrawRectangle(const Rectangles & rectangle) {
	Concurrency::Lock::SharedScope scope(_lock);

	D2D1_RECT_F rect;
	parseRect(rectangle.Box, _dx->_renderTarget->GetSize(), rect);
	// if fully transparent, do not draw
	if (rectangle.Background.a) {
		ID2D1Brush * backgroundBrush = _dx->GetBrush(rectangle.Background);
		_cmds->Add([backgroundBrush, rect](D2D & d) {
			d._dx->_renderTarget->FillRectangle(rect, backgroundBrush);
		});
	}

	// if stroke of frame is zero, do nor draw
	if (rectangle.Frame.Size.Value > 0.f) {
		f32 width;
		parseSize(rectangle.Frame.Size, width);
		ID2D1Brush * frameBrush = _dx->GetBrush(rectangle.Frame.Brush);
		_cmds->Add([frameBrush, rect, width](D2D & d) {
			d._dx->_renderTarget->DrawRectangle(rect, frameBrush, width);
		});
	}
}

void D2D::DrawEllipse(const Rectangles & rectangle) {
	Concurrency::Lock::SharedScope scope(_lock);
	// prepare position
	D2D1_RECT_F rect;
	parseRect(rectangle.Box, _dx->_renderTarget->GetSize(), rect);
	// get target resolution
	D2D1_ELLIPSE ellipse;
	ellipse.point.x = (rect.left + rect.right) * 0.5f;
	ellipse.point.y = (rect.top + rect.bottom) * 0.5f;
	ellipse.radiusX = (rect.right - rect.left) * 0.5f;
	ellipse.radiusY = (rect.bottom - rect.top) * 0.5f;
	// lock commands
	auto proxy = *_cmds;
	// fill
	if (rectangle.Background.a > 0) {
		ID2D1Brush * backgroundBrush = _dx->GetBrush(rectangle.Background.a);
		proxy->Add([ellipse, backgroundBrush](D2D & d) {
			d._dx->_renderTarget->FillEllipse(ellipse, backgroundBrush);
		});
	}
	// border
	if (rectangle.Frame.Brush.a) {
		f32 stroke;
		parseSize(rectangle.Frame.Size, stroke);
		ID2D1Brush * strokeBrush = _dx->GetBrush(rectangle.Frame.Brush);
		proxy->Add([ellipse, strokeBrush, stroke](D2D & d) {
			d._dx->_renderTarget->DrawEllipse(ellipse, strokeBrush, stroke);
		});
	}
}

void D2D::DrawLine(const Lines & line) {
	Concurrency::Lock::SharedScope scope(_lock);
	// get color
	ID2D1Brush * brush = _dx->GetBrush(line.Brush);
	// parse position
	D2D1_SIZE_F size = _dx->_renderTarget->GetSize();
	D2D1_POINT_2F A, B;
	parsePoint(line.HAlign, line.VAlign, line.A, size, A);
	parsePoint(line.HAlign, line.VAlign, line.B, size, B);
	// parse size
	f32 lineWidth;
	parseSize(line.Size, lineWidth);
	// set command
	_cmds->Add([A, B, brush, lineWidth](D2D & d) {
		d._dx->_renderTarget->DrawLine(A, B, brush, lineWidth);
	});
}

void D2D::DrawPolyline(const Polylines & line) {
	Concurrency::Lock::SharedScope scope(_lock);
	// compute bounding box
	AABox2F aabb;
	for (const float2 & pt : line.Points)
		aabb.Include(aabb);

	// create box to be converted into the pixel space
	Boxes box;
	box.Origins = aabb.Min;
	box.Dimensions = aabb.Size();
	box.HAlign = line.HAlign;
	box.VAlign = line.VAlign;
	box.Mode = line.Mode;

	// get rectangle in the pixel space
	D2D1_RECT_F rect;
	parseRect(box, _dx->_renderTarget->GetSize(), rect);
	float2 targetMin(rect.left, rect.top);
	float2 targetMax(rect.right, rect.bottom);

	// convert points
	List<D2D1_POINT_2F, 256> points;
	Math::IntervalConverter<float2> converter(aabb.Min, aabb.Max, targetMin, targetMax);
	for (const float2 & pt : line.Points) {
		points.Add(reinterpret_cast<const D2D1_POINT_2F &>(converter(pt)));
	}

	// init geometry handle
	ID2D1PathGeometry * geometry;
	_dx->_factory->CreatePathGeometry(&geometry);
	// init sink for rendering
	ID2D1GeometrySink * sink;
	geometry->Open(&sink);
	// fill the geometry
	sink->BeginFigure({ points.First().x, points.First().y }, D2D1_FIGURE_BEGIN_FILLED);
	sink->AddLines(points.begin() + 1, (UINT32)points.Size() - 1);
	sink->EndFigure(D2D1_FIGURE_END_OPEN);
	sink->Close();

	// get brush
	ID2D1Brush * brush = _dx->GetBrush(line.Brush);
	// get width
	f32 width;
	parseSize(line.Size, width);

	// create render command
	_cmds->Add([geometry, brush, width](D2D & d) {
		d._dx->_renderTarget->DrawGeometry(geometry, brush, width);
		geometry->Release();
	});
}

void D2D::DrawStatistic(const Statistics & statistic) {
	Concurrency::Lock::SharedScope scope(_lock);
	// area of the statistic
	D2D1_RECT_F rectangle;
	parseRect(statistic.Box, _dx->_renderTarget->GetSize(), rectangle);
	// boundaries of the statistic
	f32 min, max, avg;
	avg = 0.f;
	if (statistic.Min == Math::ConstantsF::Max || statistic.Max == Math::ConstantsF::Min) {
		min = Math::ConstantsF::Max;
		max = Math::ConstantsF::Min;
		f32 inv = 1.f / statistic.Statistic.Samples.Length();
		for (f32 value : statistic.Statistic.Samples) {
			avg += inv * value;
			if (value < min)
				min = value;
			if (max < value)
				max = value;
		}
 		if (statistic.Min != Math::ConstantsF::Max)
 			min = statistic.Min;
 		if (statistic.Max != Math::ConstantsF::Min)
 			max = statistic.Max;
	}
	else {
		min = statistic.Min;
		max = statistic.Max;
	}
	const float2 offset(0.f, min);
	const float2 dimensions(statistic.Statistic.Samples.Length() - 1.f, max - min);
	const float2 targetMin(rectangle.left, rectangle.bottom);
	const float2 targetMax(rectangle.right, rectangle.top);
	const Math::IntervalConverter<float2> convert(offset, offset + dimensions, targetMin, targetMax);
	// fall-back for degenerated graphs
	if (statistic.Statistic.Samples.Length() == 0 || min == max) {

	}

	const float2 satMin(rectangle.left, rectangle.top);
	const float2 satMax(rectangle.right, rectangle.bottom);
	List<D2D1_POINT_2F, 256> points;
	for (size_t i = 0, m = statistic.Statistic.Samples.Length(); i < m; ++i) {
		float2 point = convert({ (f32)i, statistic.Statistic.Samples[(i + statistic.Statistic.IndexOffset) % m] });
		//float2::algebra_t::satv(satMin, satMax, point);
		points.Add(reinterpret_cast<const D2D1_POINT_2F &>(point));
	}

	ID2D1PathGeometry * geometry;
	_dx->_factory->CreatePathGeometry(&geometry);

	ID2D1GeometrySink * sink;
	geometry->Open(&sink);
	sink->BeginFigure(points.First(), D2D1_FIGURE_BEGIN_FILLED);
	sink->AddLines(points.begin() + 1, (UINT32) points.Size() - 1);
	sink->EndFigure(D2D1_FIGURE_END_OPEN);
	sink->Close();

	ID2D1Brush * brushGraph = _dx->GetBrush(statistic.Brush);
	f32 width;
	parseSize(statistic.Size, width);

	if (statistic.Callbacks.Computed) {
		Boxes box;
		box.Origins.Set(rectangle.left, rectangle.top);
		box.Dimensions.Set(rectangle.right - rectangle.left, rectangle.bottom - rectangle.top);
		statistic.Callbacks.Computed->Run(*this, statistic, box, min, max, avg);
	}

	_cmds->Add([geometry, brushGraph, width](D2D & d) {
		d._dx->_renderTarget->DrawGeometry(geometry, brushGraph, width);
		geometry->Release();
	});
}

void D2D::DrawLabel(const Labels & text) {
	Concurrency::Lock::SharedScope scope(_lock);
	// get target resolution
	D2D1_SIZE_F size = _dx->_renderTarget->GetSize();
	D2D1_RECT_F target;
	parseRect(text.Box, size, target);

	// prepare layout
	IDWriteTextLayout * layout = 0;

	// create layout from text and default font
	_dx->_writerFactory->CreateTextLayout(
		text.String.Text.begin(),
		(UINT)text.String.Text.Length(),
		_dx->GetFont(_defaultFont),
		target.right - target.left,
		target.bottom - target.top,
		&layout
	);

	if (layout == 0)
		return;

	// set valid scale of the font
	f32 width;
	parseSize(_defaultFont->Size, width);
	layout->SetFontSize(width *  text.Scale, {0U, (UINT)(text.String.Length())});

	// select alignment of text block
	layout->SetTextAlignment(parseTHAligns(text.THAlign));
	layout->SetParagraphAlignment(parseTVAligns(text.TVAlign));
	// for each tag, set valid formatting
	for (const D2D::Strings::Tag & item : text.String.Tags) {
		const Fonts & f = item.Font;
		// set range of tag
		DWRITE_TEXT_RANGE range;
		f32 fWidth;
		parseSize(f->Size, fWidth);
		range.startPosition = (UINT32)item.From;
		range.length = (UINT32)(item.Length);
		// set properties to layout
		layout->SetDrawingEffect(_dx->GetBrush(f->Brush), range);
		layout->SetFontSize(fWidth * text.Scale, range);
		layout->SetFontStretch((DWRITE_FONT_STRETCH)(u32)f->Stretch, range);
		layout->SetFontStyle((DWRITE_FONT_STYLE)(u32)f->Style, range);
		layout->SetFontWeight((DWRITE_FONT_WEIGHT)(u32)f->Weight, range);
		layout->SetFontFamilyName(f->Name, range);
		layout->SetUnderline((BOOL)f->Underline, range);
	}

	// compute size of rendered area
	DWRITE_TEXT_METRICS m;
	layout->GetMetrics(&m);

	Boxes result;
	result.Dimensions.Set(m.width, m.height);

	switch (text.Box.HAlign) {
		case HAligns::HLeft:   result.Origins.x = target.left; break;
		case HAligns::HCenter: result.Origins.x = target.left + (size.width - m.width) * 0.5f; break;
		case HAligns::HRight:	 result.Origins.x = target.left + (size.width - m.width); break;
		default: DD_WARNING("D2D Invalid HAlign value."); break;
	}

	switch (text.Box.VAlign) {
		case VAligns::VTop:    result.Origins.y = target.top; break;
		case VAligns::VCenter: result.Origins.y = target.top + (size.height - m.height) * 0.5f; break;
		case VAligns::VBottom: result.Origins.y = target.top + (size.height - m.height); break;
		default: DD_WARNING("D2D Invalid VAlign value."); break;
	}

	if (text.Callbacks.SizeComputed)
		text.Callbacks.SizeComputed->Run(result, *this);

	ID2D1Brush * color = _dx->GetBrush(_defaultFont->Brush);

	_cmds->Add([result, layout, color](D2D & d) {
 		// draw text
 		d._dx->_renderTarget->DrawTextLayout(
 			{ result.Origins.x, result.Origins.y },
 			layout,
 			color
 		);
 		// release layout
 		layout->Release();
 	});
}

D2D::Area D2D::Relative(const Boxes & box) {
	return Area(*this, box);
}

inline RECT GetWindowResolution(HWND window) {
	RECT desktop;
	GetWindowRect(window, &desktop);
	return desktop;
}

inline RECT GetDesktopResolution() {
	return GetWindowResolution(GetDesktopWindow());
}

D2D::D2D(Gpu::Device & device)
	: _dx(new DXMemoryModel())
	, _device(device)
	, _defaultFont(D2D::Fonts::DEFAULT_YELLOW)
{
	// get swap chain from D3D
	IDXGISwapChain * swapChain = reinterpret_cast<IDXGISwapChain *&>(_device->GetHandles()[2]);
	// create back buffer
	HR(swapChain->GetBuffer, NULL, __uuidof(IDXGISurface), (void**)_dx->_backBuffer.Init());

	// Factory draws geometries
	// https://learn.microsoft.com/en-us/windows/win32/api/d2d1/nf-d2d1-d2d1createfactory
	HR(D2D1CreateFactory, D2D1_FACTORY_TYPE_SINGLE_THREADED, _dx->_factory.Init());

	// Retrieve proper DPI
	// https://learn.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-monitorfromwindow
	HMONITOR monitor = MonitorFromWindow((HWND)_device->GetWindow()->NativeHandle(), NULL);
	// https://learn.microsoft.com/en-us/windows/win32/api/shellscalingapi/nf-shellscalingapi-getdpiformonitor
	::GetDpiForMonitor(monitor, MDT_EFFECTIVE_DPI, &_dpi.x, &_dpi.y);

	// Setup D2D render target
	// https://learn.microsoft.com/en-us/windows/win32/api/d2d1helper/nf-d2d1helper-rendertargetproperties
	D2D1_RENDER_TARGET_PROPERTIES rtProps = D2D1::RenderTargetProperties(
		D2D1_RENDER_TARGET_TYPE_HARDWARE,
		D2D1::PixelFormat(DXGI_FORMAT_UNKNOWN, D2D1_ALPHA_MODE_PREMULTIPLIED),
		(f32)(_dpi.x),
		(f32)(_dpi.y)
	);
	HR(_dx->_factory->CreateDxgiSurfaceRenderTarget, _dx->_backBuffer, &rtProps, _dx->_renderTarget.Init());

	// Init DWrite - drawing texts
	// https://learn.microsoft.com/en-us/windows/win32/api/dwrite/nf-dwrite-dwritecreatefactory
	HR(DWriteCreateFactory, DWRITE_FACTORY_TYPE_SHARED, __uuidof(IDWriteFactory), reinterpret_cast<IUnknown **>(_dx->_writerFactory.Init()));
}

D2D::~D2D() = default;

/************************************************************************/
/* Area                                                                 */
/************************************************************************/
D2D::Boxes D2D::Area::convert(const Boxes & box) {
	// rectangle computed in sub-area
	D2D1_RECT_F temp;
	// fake render target dimensions
	D2D1_SIZE_F * boxSize = (D2D1_SIZE_F *)&Dimensions;
	// compute relative area
	parseRect(box, *boxSize, temp);

	// resulting rectangle in pixels at the left-top aligned render target
	Boxes movedBox;
	movedBox.Origins.x = Offsets.x + temp.left;
	movedBox.Origins.y = Offsets.y + temp.top;
	movedBox.Dimensions.x = (temp.right - temp.left);
	movedBox.Dimensions.y = (temp.bottom - temp.top);

	return movedBox;
}

D2D::Rectangles D2D::Area::convert(const Rectangles & rect) {
	Rectangles movedRectangle;
	movedRectangle = rect;
	movedRectangle.Box = convert(rect.Box);

	return movedRectangle;
}

D2D::Lines D2D::Area::convert(const Lines & line) {
	Lines movedLine;
	// convert to pixel size
	D2D1_SIZE_F * d = (D2D1_SIZE_F *)&Dimensions;
	D2D1_POINT_2F * A = (D2D1_POINT_2F *)&movedLine.A;
	D2D1_POINT_2F * B = (D2D1_POINT_2F *)&movedLine.B;
	parsePoint(line.HAlign, line.VAlign, line.A, *d, *A);
	parsePoint(line.HAlign, line.VAlign, line.B, *d, *B);

	movedLine.A += Offsets;
	movedLine.B += Offsets;
	movedLine.Brush = line.Brush;
	movedLine.Size = line.Size;

	return movedLine;
}

D2D::Labels D2D::Area::convert(const Labels & text) {
	Labels movedText = text;
	movedText.Box = convert(text.Box);

	return movedText;
}

D2D::Area::Area(D2D & dev, const Boxes & area)
	: _device(dev)
{
	// get offsets of relative area
	D2D1_RECT_F smallArea;
	// compute relative area
	parseRect(area, _device._dx->_renderTarget->GetSize(), smallArea);

	Dimensions.x = smallArea.right - smallArea.left;
	Dimensions.y = smallArea.bottom - smallArea.top;
	Offsets.x = smallArea.left;
	Offsets.y = smallArea.top;
}
