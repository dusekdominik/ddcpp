#include <DD/Stopwatch.h>
#include <Windows.h>

using namespace DD;

struct MyFrequency {
	LARGE_INTEGER Value;
	operator u64 () { return Value.QuadPart; }
	MyFrequency() { while (QueryPerformanceFrequency(&Value) == 0); }
};

struct MyTicks {
	LARGE_INTEGER Value;
	operator u64 () { return Value.QuadPart; }
	MyTicks() { while (QueryPerformanceCounter(&Value) == 0); }
};

u64 Stopwatch::Frequency() {
	static thread_local MyFrequency f;
	return f;
}

u64 Stopwatch::TimeStampTicks() {
	return MyTicks();
}

u64 Stopwatch::TimeStampMilliseconds() {
	return 1000 * TimeStampTicks() / Frequency();
}

u64 Stopwatch::TimeStampSeconds() {
	return TimeStampTicks() / Frequency();
}

void Stopwatch::Start() {
	_startTime = TimeStampTicks();
}

void Stopwatch::Stop() {
	_elapsed += TimeStampTicks() - _startTime;
	_startTime = 0;
}

u64 Stopwatch::ElapsedTicks() const {
	if (_startTime)
		return _elapsed + (TimeStampTicks() - _startTime);
	else
		return _elapsed;
}

Units::Time_NanoSecondULL Stopwatch::ElapsedNanoseconds() const {
	return 1000000000 * ElapsedTicks() / (Frequency());
}

Units::Time_MicroSecondULL Stopwatch::ElapsedMicroseconds() const {
	return 1000000 * ElapsedTicks() / (Frequency());
}

Units::Time_MilliSecondULL Stopwatch::ElapsedMilliseconds() const {
	return 1000 * ElapsedTicks() / (Frequency());
}

Units::Time_SecondULL Stopwatch::ElapsedSeconds() const {
	return ElapsedTicks() / (Frequency());
}

Units::Time_MinuteULL Stopwatch::ElapsedMinutes() const {
	return ElapsedTicks() / (Frequency() * 60);
}

Units::Time_HourULL Stopwatch::ElapsedHours() const {
	return ElapsedTicks() / (Frequency() * 3600);
}

Units::Time_DayULL Stopwatch::ElapsedDays() const {
	return ElapsedTicks() / (Frequency() * 3600 * 24);
}

