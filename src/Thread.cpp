#include <DD/Concurrency/Thread.h>
#include <DD/Debug.h>
#include <windows.h>

#undef MemoryBarrier

using namespace DD;
using namespace DD::Concurrency;


struct ThreadBase::NativeView {
	/** Thread handle. */
	HANDLE Handle = NULL;
};

u32 _Thread_MainThread_Id = Thread::Id();

static DWORD WINAPI ThreadRoutine(LPVOID lpParam) {
	ThreadBase * thread = (ThreadBase *)lpParam;
	Lambda<void()> lambda = thread->Routine();
	lambda->Run();

	return 0;
}

void Thread::WaitAll(Thread * threads, size_t threadCount) {
	Array<HANDLE> handles(threadCount);
	for (HANDLE & handle : handles) {
		handle = (*(threads++))->nativeView()->Handle;
	}

	WaitForMultipleObjects(
		// The number of object handles in the array pointed to by lpHandles.
		(DWORD)threadCount,
		// An array of object handles. For a list of the object types whose handles can be specified, see the following Remarks section.
		handles.begin(),
		// If this parameter is TRUE, the function returns when the state of all objects in the lpHandles array is signaled.
		// If FALSE, the function returns when the state of any one of the objects is set to signaled.
		TRUE,
		// The time - out interval, in milliseconds.
		INFINITE
	);
}

bool Thread::WaitAll(Thread * threads, size_t threadCount, u32 milliseconds) {
	Array<HANDLE> handles(threadCount);
	for (HANDLE & handle : handles) {
		handle = (*(threads++))->nativeView()->Handle;
	}

	return WAIT_TIMEOUT != WaitForMultipleObjects(
		// The number of object handles in the array pointed to by lpHandles.
		(DWORD)threadCount,
		// An array of object handles. For a list of the object types whose handles can be specified, see the following Remarks section.
		handles.begin(),
		// If this parameter is TRUE, the function returns when the state of all objects in the lpHandles array is signaled.
		// If FALSE, the function returns when the state of any one of the objects is set to signaled.
		TRUE,
		// The time - out interval, in milliseconds.
		milliseconds
	);
}

void Thread::Sleep(u32 milliseconds) {
	::Sleep(milliseconds);
}

void Thread::MemoryBarrier() {
	__faststorefence();
}

u32 Thread::Id() {
	static thread_local u32 id = GetCurrentThreadId();
	return id;
}

u32 Thread::MainId() {
	return _Thread_MainThread_Id;
}

void Thread::AssertMain() {
	DD_ASSERT(Id() == MainId());
}

ThreadBase & ThreadBase::Detach() {
	nativeView()->Handle = 0;
	return *this;
}

ThreadBase & ThreadBase::Suspend() {
	SuspendThread(nativeView()->Handle);
	return *this;
}

ThreadBase & ThreadBase::Resume() {
	ResumeThread(nativeView()->Handle);
	return *this;
}

ThreadBase & ThreadBase::Terminate() {
	TerminateThread(nativeView()->Handle, 0);
	return *this;
}

ThreadBase & ThreadBase::Priority(ThreadPriority priority) {
	SetThreadPriority(nativeView()->Handle, (i32)priority);
	return *this;
}

ThreadBase & ThreadBase::Wait() {
	WaitForSingleObject(nativeView()->Handle, INFINITE);
	return *this;
}

bool ThreadBase::Wait(u32 milliseconds) {
	return WaitForSingleObject(nativeView()->Handle, milliseconds) == WAIT_OBJECT_0;
}

ThreadPriority ThreadBase::Priority() const {
	return (ThreadPriority)GetThreadPriority(nativeView()->Handle);
}

bool ThreadBase::IsCurrent() const {
	return ThreadId() == Thread::Id();
}

ThreadBase::ThreadBase(
	Lambda<void()> lambda,
	Text::StringView16 name,
	bool startSuspended,
	size_t stackSize
) : _routine(lambda)
	, _name(name)
{
	// init storage
	new (_storage) NativeView();

	// parse suspended flag
	DWORD flags = startSuspended ? CREATE_SUSPENDED : 0;

	nativeView()->Handle = CreateThread(
		// A pointer to a SECURITY_ATTRIBUTES structure that determines whether the returned handle can be inherited by child processes.
		NULL,
		// The initial size of the stack, in bytes.
		stackSize,
		// A pointer to the application-defined function to be executed by the thread.
		ThreadRoutine,
		// A pointer to a variable to be passed to the thread.
		this,
		// The flags that control the creation of the thread.
		flags,
		// A pointer to a variable that receives the thread identifier.
		(DWORD *)&_id
	);

	// set name
	SetThreadDescription(
		// Handle to the thread.
		nativeView()->Handle,
		// Name of the thread.
		_name
	);
}

ThreadBase::~ThreadBase() {
	if (nativeView()->Handle) {
		if (!Wait(25000)) {
			Debug::Log::PrintLine("Timeout of thread destructor, thread is being killed.");
			Terminate();
		}
	}
}
