#include <DD/XML.h>
#include <DD/Debug.h>

using namespace DD;
using namespace DD::XML;

/************************************************************************/
/* Helper structures declaration                                        */
/************************************************************************/

/**
 * Loader of simple xml-like tags.
 * Element must be in form <e[attributes]>[children]</e> or <e[attributes]/>.
 * Attribute must be in form  a="value".
 */
struct SimpleTagReader {
	friend struct SimpleXmlReader;
public: // nested
	/** Key value pair. */
	struct Attribute { StringLocal<32> Name, Value; };
	/** Reader states. */
	enum States {
		/** State for loading ignored tags as <?xml ...?> */
		IGNORE,
		/** Load tag of an element. */
		TAG_ELEMENT_NAME,
		/** Load name of a attribute. */
		TAG_ATTRIBUTE_NAME,
		/** Load value of an attribute. */
		TAG_ATTRIBUTE_VALUE,
		/** Spacing between attributes. */
		TAG_ATTRIBUTE_SPACING,
		/** Load content of an attribute. */
		DONE
	};
	/** Types of just read tag. */
	enum TagTypes {
		/** Tag in open form <tag[attributes]>. */
		OPEN,
		/** Tag in open close-form, e.g. <tag[attributes]/>. */
		BOTH,
		/** Tag min close form, e.g. </tag>. */
		CLOSED,
		/** Tag in form of xml header, e.g. <?...?>*/
		XML_HEADER,
		/** Tag in form of xml comment, i.e. <!...-> */
		XML_COMMENT
	};
public: // properties
	/** Current state of reader */
	States State = TAG_ELEMENT_NAME;
	/** Name of tag. */
	StringLocal<32> Name;
	/** Loaded attributes of tag, */
	List<Attribute, 16> Attributes;
	/** Type of loaded tag. */
	TagTypes TagType;
public: // methods
	/** Read the tag, char by char. */
	bool Fill(wchar_t c);
	/** Reset the reader for another reading. */
	void Reset();
public: // constructors
	/** Zero constructor. */
	SimpleTagReader() { Reset(); }
};

/**
 * Loader of simple xml-like documents.
 * If document is not formatted correctly, it could lead to process crash.
 */
struct SimpleXmlReader {
public: // nested
	/** States of xml reader, defines if the just read content is text or tag. */
	enum States {
		/** State defining that just read context is tag-element. */
		TAG,
		/** State defining that just read context is text-element. */
		TEXT,
		COMMENT
	};

public: // statics
	/** Per thread singleton instance. */
	static SimpleXmlReader & Instance() { static thread_local SimpleXmlReader _i; return _i; }

public: // properties
	/** State of reader @States. */
	States State;
	/** Document object which should be read. */
	Document Doc;
	/** Stack for tag reading. */
	List<SimpleTagReader, 16> TagElements;
	/** Buffer for text reading */
	StringLocal<4096> TextElement;

protected: // methods
	/** Create new element defined by xml document and stack depth and @return pointer to it. */
	Element * getNewElement();
	/** Appends tag element. */
	void appendTag(SimpleTagReader & tag);
	/** Appends text element. */
	void appendText(String & str);

public: // methods
	/** Read the document char by char */
	bool Fill(wchar_t c);
	/** Finish correctly the reading. */
	void Flush();
	/** Reset reader for another use. */
	void Reset(Document document);

protected: // constructors
	/** Zero constructor. */
	SimpleXmlReader() = default;
};

/************************************************************************/
/* Helper structures definition                                         */
/************************************************************************/
bool SimpleTagReader::Fill(wchar_t c) {
	switch (State) {
		case IGNORE:
			if (c == '>') State = DONE;
			break;
		case TAG_ELEMENT_NAME:
			switch (c) {
				case '!': TagType = XML_COMMENT; State = IGNORE; break;
				case '?': TagType = XML_HEADER; State = IGNORE; break;
				case '\t':
				case '\n':
				case ' ': State = TAG_ATTRIBUTE_SPACING; break;
				case '>': State = DONE; break;
				case '<': break;
				case '/': TagType = Name.Length() ? BOTH : CLOSED; break;
				default: Name.AppendEx(c); break;
			}
			break;
		case TAG_ATTRIBUTE_NAME:
			switch (c) {
				case '=': break;
				case '"': State = TAG_ATTRIBUTE_VALUE; break;
				default: Attributes.Last().Name.AppendEx(c); break;
			}
			break;
		case TAG_ATTRIBUTE_VALUE:
			switch (c) {
				case '"':
					XML::Attribute::Unescape(Attributes.Last().Value);
					State = TAG_ATTRIBUTE_SPACING;
					break;
				case '>': State = DONE; break;
				default:
					Attributes.Last().Value.AppendEx(c);
					break;
			}
			break;
		case TAG_ATTRIBUTE_SPACING:
			switch (c) {
				case '\t':
				case '\n':
				case ' ': break;
				case '/': TagType = BOTH; break;
				case '>': State = DONE; break;
				default: State = TAG_ATTRIBUTE_NAME; Attributes.Append().Name.AppendEx(c); break;
			}
			break;
	}
	return DONE == State;
}

void SimpleTagReader::Reset() {
	TagType = OPEN;
	State = TAG_ELEMENT_NAME;
	Name.Clear();
	for (auto & a : Attributes) {
		a.Name.Clear();
		a.Value.Clear();
	}
	Attributes.Clear();
}

Element * SimpleXmlReader::getNewElement() {
	Element * node = &(Doc->GetRoot());
	// go to the lowest layer
	for (size_t d = 2; d < TagElements.Size(); ++d)
		node = &(node->Children.Last());
	// if not root append
	if (TagElements.Size() > 1)
		node = &(node->DefineElement(ElementDeclaration()));
	// result
	return node;
}

void SimpleXmlReader::appendTag(SimpleTagReader & tag) {
	Element & node = *getNewElement();
	node.Declaration = Doc->DeclareElement(tag.Name);
	for (auto & attr : tag.Attributes)
		node[attr.Name] = attr.Value;
}

void SimpleXmlReader::appendText(String & str) {
	if (str.Trim().Length()) {
		getNewElement()->Text = Element::Unescape(str);
	}
	str.Clear();
}

bool SimpleXmlReader::Fill(wchar_t c) {
	switch (State) {
		case TEXT:
			switch (c) {
				case '<': State = TAG; appendText(TextElement); break;
				default: TextElement.AppendEx(c); break;
			}
			break;
		case TAG:
			if (TagElements.Last().Fill(c)) {
				switch (TagElements.Last().TagType) {
					case SimpleTagReader::OPEN:
						appendTag(TagElements.Last());
						TagElements.Append();
						break;
					case SimpleTagReader::BOTH:
						appendTag(TagElements.Last());
						TagElements.Last().Reset();
						break;
					case SimpleTagReader::CLOSED:
						TagElements.Last().Reset();
						TagElements.RemoveLast();
						if (TagElements.Size())
							TagElements.Last().Reset();
						break;
					case SimpleTagReader::XML_HEADER:
					case SimpleTagReader::XML_COMMENT:
						TagElements.Last().Reset();
						break;
					default:
						throw;
				}
				State = TEXT;
			}
			break;
		case COMMENT: {

		} break;
	}

	return TagElements.Size() == 0;
}

void SimpleXmlReader::Flush() {
	if (State == TEXT) {
		appendText(TextElement);
	}
}

void SimpleXmlReader::Reset(Document document) {
	State = TEXT;
	for (SimpleTagReader & tagReader : TagElements)
		tagReader.Reset();
	TagElements.Clear();
	TagElements.Append();
	Doc = document;
}

/************************************************************************/
/* XML::Reader                                                          */
/************************************************************************/
void Reader::init() {
	SimpleXmlReader::Instance().Reset(_doc);
	_state = READ;
}

void Reader::read(wchar_t c) {
	SimpleXmlReader::Instance().Fill(c);
}

void Reader::flush() {
	SimpleXmlReader::Instance().Flush();

	if (SimpleXmlReader::Instance().TagElements.Size() > 1) {
		_state = FAIL;
	}
	else {
		_state = DONE;
	}
}

/************************************************************************/
/* XML::Document                                                        */
/************************************************************************/
String DocumentBase::ToStringUtf8() const {
	// preallocate accumulator
	String accumulator(_root.GetSizeEstimate() * 4);
	// fill accumulator
	appendHeaderUTF8(accumulator);
	_root.AppendMe(accumulator);

	return accumulator;
}

String DocumentBase::ToStringUtf16() const {
	// preallocate accumulator
	String accumulator(_root.GetSizeEstimate());
	// fill accumulator
	appendHeaderUTF16(accumulator);
	_root.AppendMe(accumulator);

	return accumulator;
}

void DocumentBase::appendHeaderUTF16(String & acc) const {
	acc.Append("<?xml version=\"1.0\" encoding=\"UTF-16\" ?>");
}

void DocumentBase::appendHeaderUTF8(String & acc) const {
	acc.Append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
}

/************************************************************************/
/* XML::ElementDefinition                                               */
/************************************************************************/

DD::ArrayView1D<const DD::Text::StringView16> DD::XML::Element::EscapePattern() {
	static constexpr Text::StringView16 pattern[] = {
		L"<",
		L">",
		L"&",
		L"'",
		L"\""
	};

	return pattern;
}

DD::ArrayView1D<const DD::Text::StringView16> DD::XML::Element::EscapeReplacement() {
	static constexpr Text::StringView16 pattern[] = {
		L"&lt;",
		L"&gt;",
		L"&amp;",
		L"&apos;",
		L"&quot"
	};

	return pattern;
}

void Element::appendMeAsText(String & acc, size_t indent) const {
	// make copy for escape values
	StringLocal<2048> copy(Text);
	acc.Append('\n').AppendFill(indent).Append(Escape(copy));
}

void Element::appendMeAsTag(String & acc, size_t indent) const {
	const List<AttributeDeclaration> & attributes = Declaration->Attributes;
	const String & name = Declaration->Name;
	// opening tag
	acc.Append('\n').AppendFill(indent).AppendBatch('<', name);
	// attributes of the tag
	for (const Attribute & attribute : Attributes) {
		const String & attributeName = attribute.Declaration->_name;
		StringLocal<128> attributeValue = attribute.Value;
		Attribute::Escape(attributeValue);
		acc.AppendBatch(' ', attributeName, '=', '"', attributeValue, '"');
	}
	// short tag if no children
	if (Children.IsEmpty()) {
		acc.AppendBatch('/', '>');
	}
	else {
		acc.Append('>');
		// children
		for (const Element & sub : Children) {
			sub.AppendMe(acc, indent + 2);
		}
		// closing tag
		acc.Append('\n').AppendFill(indent).AppendBatch('<', '/', name, '>');
	}
}

String & Element::operator[](const Text::StringView16 & name) {
	AttributeDeclaration declaration = Declaration->GetAttributeDecl(name);

	// try to find
	for (Attribute & attribute : Attributes)
		if (attribute.Declaration == declaration)
			return attribute.Value;

	// add if not found
	Attributes.Append().Declaration = declaration;

	return Attributes.Last().Value;
}

Text::StringView16 Element::operator[](const Text::StringView16 & name) const {
	if (Declaration) {
		const AttributeDeclaration declaration = Declaration->GetAttributeDeclConst(name);

		// try to find
		for (const Attribute & attribute : Attributes)
			if (attribute.Declaration == declaration)
				return attribute.Value;
	}

	// return nothing if not found
	return nullptr;
}

size_t Element::GetSizeEstimate(size_t depth) const {
	size_t result = 0;
	if (Declaration) {
		result += Declaration->GetSizeEstimate();
		for (const Attribute & s : Attributes) {
			result += s.Value.Length();
		}
		for (const Element & e : Children) {
			result += e.GetSizeEstimate(depth + 1) + (depth * 2) + 1; // \n + indent
		}
	}
	else {
		result += Text.Length();
	}
	return result;
}

void Element::AppendMe(String & acc, size_t indent) const {
	switch (GetType()) {
		case Types::TYPE_TEXT: appendMeAsText(acc, indent); break;
		case Types::TYPE_TAG: appendMeAsTag(acc, indent); break;
		default: /*throw;*/ break;
	}
}

Element & Element::DefineElement(ElementDeclaration declaration) {
	Children.Add(Element(Document, declaration));
	return Children.Last();
}

Element & DD::XML::Element::DefineElement(Text::StringView16 elementName) {
	return DefineElement(Document->DeclareElement(elementName));
}

DD::XML::Element::Types DD::XML::Element::GetType() const {
	return Declaration == nullptr ? Types::TYPE_TEXT : Types::TYPE_TAG;
}

DD::Text::StringView16 DD::XML::Element::GetName() const {
	return Declaration != nullptr ? Declaration->Name : DD::Text::StringView16(nullptr);
}

Element & XML::Element::DefineAttribute(Text::StringView16 attrName, Text::StringView16 attrValue) {
	auto & attribute = Attributes.Append();
	attribute.Declaration = Declaration->GetAttributeDecl(attrName);
	attribute.Value = attrValue;

	return *this;
}

Element & Element::DefineText(const Text::StringView16 & text) {
	Children.Append().Text = text;
	return Children.Last();
}

Element::Element(DocumentBase * document, ElementDeclaration decl)
	: Declaration(decl)
	, Document(document)
	, Children(4U)
	, Text()
{}

/************************************************************************/
/* XML::ElementDeclaration                                              */
/************************************************************************/
AttributeDeclaration ElementDeclarationBase::GetAttributeDeclConst(const Text::StringView16 & name) const {
	for (const AttributeDeclaration & a : Attributes)
		if (a->_name.EqualsInsensitive(name))
			return a;

	return AttributeDeclaration();
}

AttributeDeclaration ElementDeclarationBase::GetAttributeDecl(const Text::StringView16 & name) {
	AttributeDeclaration result = GetAttributeDeclConst(name);
	if (result == nullptr) {
		result = AttributeDeclaration(name);
		Attributes.Add(result);
	}

	return result;
}

/************************************************************************/
/* XML::AttributeDeclarationBase                                        */
/************************************************************************/

AttributeDeclarationBase::AttributeDeclarationBase(const String & name, Types type)
	: _name(name)
	, _lowerName(name)
	, _type(type)
{
	_lowerName.ToLower();
}

/************************************************************************/
/* XML::AttributeDeclaration                                            */
/************************************************************************/

Attribute AttributeDeclaration::operator()(const String & value) {
	return Attribute(*this, value);
}

/************************************************************************/
/* XML::Attribute                                                       */
/************************************************************************/

ArrayView1D<const Text::StringView16> Attribute::EscapePattern() {
	static constexpr Text::StringView16 pattern[] = {
		L"&",
		L"<",
		L">",
		L"'",
		L"\""
	};

	return pattern;
}

ArrayView1D<const Text::StringView16> Attribute::EscapeReplacement() {
	static constexpr Text::StringView16 pattern[] = {
		L"&amp;",
		L"&lt;",
		L"&gt;",
		L"&apos;",
		L"&quot;"
	};

	return pattern;
}

Attribute::Attribute(AttributeDeclaration declaration, const String & value)
	: Declaration(declaration)
	, Value(value)
{}

bool Document::LoadUtf8(const FileSystem::Path & path) {
	if (!path.IsFile())
		return false;

	XML::Reader reader;
	reader.ReadFormIterable(path.AsFile().Read());

	if (reader.Succeed()) {
		*this = reader.GetDocument();
		return true;
	}

	return false;
}

bool Xml::DepthSearchSequencer::Reset() {
	if (!Accept || Accept(*SearchRoot))
		SearchStack.Add(SearchRoot);

	return SearchStack.IsNotEmpty();
}

bool Xml::DepthSearchSequencer::Next() {
	// pop item
	XML::Element * last = SearchStack.Last();
	SearchStack.RemoveLast();
	// push all the children
	if (!Accept || Accept(*last))
		for (XML::Element & child : last->Children)
			SearchStack.Add(&child);

	return SearchStack.IsNotEmpty();
}
