#include <DD/Building.h>
#include <DD/Debug.h>
#include <DD/Matrices.h>
#include <DD/Matrix.h>

DD_TODO_STAMP("Building roof type recoginition.");
DD_TODO_STAMP("Building roof params.");
DD_TODO_STAMP("TriangleModels based on textures.")

/**
 * @param globals - {nameOfMethod, callSignature}
 * @param locals  - enumValue
 */
#define _BUILDING_OVERLOAD_CASE(index, count, globals, locals)\
case Types::locals:\
	DD_HEAD(0, DD_UNWRAP(globals))<Types::locals>DD_HEAD(1, DD_UNWRAP(globals));\
break;
#define _BUILDING_OVERLOAD_SWITCH(callSignature, ...)\
switch (Params.Type) {\
	DD_XLIST_EX(_BUILDING_OVERLOAD_CASE, callSignature, __VA_ARGS__);\
	default:\
    DD_WARNING("Undefined case.");\
}

#define _PARAMS_XML_SERIALIZE(arg) ::DD::XML::DefaultSerializer::Serialize(arg, target)
#define _PARAMS_XML_DESERIALIZE(arg) ::DD::XML::DefaultSerializer::Deserialize(source, arg)
#define _PARAMS_XML_CASE(index, count, globals, locals) case Types::locals: globals(locals); break;
#define _PARAMS_XML_SWITCH(method, ...)\
switch (Type) {\
	DD_XLIST_EX(_PARAMS_XML_CASE, method, __VA_ARGS__)\
	default: DD_WARNING("Undefined case."); break;\
}

using namespace DD;
using namespace DD::Math;

BuildingCollection Building::Collection;

/************************************************************************/
/* Building                                                             */
/************************************************************************/
Simulation::PointCloud DD::Building::GeneratePointCloud(
	size_t wall, size_t roof, Random::Generator & generator
) const {
	Simulation::PointCloud result(Box.AABox());
	GeneratePointCloud(result, wall, roof, generator);

	return result;
}

void Building::GeneratePointCloud(
	Simulation::PointCloud & target, size_t wall, size_t roof, Random::Generator & generator
) const {
	Wall.GeneratePointCloud(target, wall, generator);
	Roof.GeneratePointCloud(target, roof, generator);
}

void Building::GenerateTriangleModel(Simulation::TriangleModel<Simulation::ColorVertex> & model) const {
	Wall.GenerateTriangleModel(model);
	Roof.GenerateTriangleModel(model);
}

/************************************************************************/
/* Building::Walls                                                      */
/************************************************************************/
#pragma region Building::Walls::GeneratePointCloud
void Building::Walls::GeneratePointCloud(
	Simulation::PointCloud & target, size_t count, Random::Generator & generator
) const {
	_BUILDING_OVERLOAD_SWITCH((GeneratePointCloud, (target, count, generator)), DD_BUILDING_WALL_TYPES);
}

template<>
void Building::Walls::GeneratePointCloud<Building::Walls::Types::Basic>(
	Simulation::PointCloud & target, size_t count, Random::Generator & generator
) const {
	auto & params = Params.Basic;
	// rotation and scale
	const float3 & position = Parent().Box.Position;
	DiagonalMatrix3F scale(Parent().Box.Radius);
	const float3x3 & transformation = scale * Parent().Box.Rotation;

	Array<float2> samples(count);
	Parent().Profile.GenerateRandomPointsOverCircumference(samples, generator);
	for (const float2 & sample : samples) {
		Simulation::QuadVertex v;
		v.Color = params.Color;
		v.Position.y = generator.Generate<f32>() * Height - 1.f;
		v.Position.xz = sample;

		// transform to building position
		v.Position = transformation * v.Position + position;
		target->AddPoint(v);
	}
}

template<>
void Building::Walls::GeneratePointCloud<Building::Walls::Types::Undefined>(
	Simulation::PointCloud &, size_t, Random::Generator &
) const {
	DD_WARNING("DD::Building: Undefined type of wall.");
}

#pragma endregion

#pragma region Building::Walls::GenerateTriangleModel
void Building::Walls::GenerateTriangleModel(
	Simulation::TriangleModel<Simulation::ColorVertex> & model
) const {
	_BUILDING_OVERLOAD_SWITCH((GenerateTriangleModel, (model)), DD_BUILDING_WALL_TYPES);
}

template<>
void Building::Walls::GenerateTriangleModel<Building::Walls::Types::Basic>(
	Simulation::TriangleModel<Simulation::ColorVertex> & model
) const {
	Simulation::TriangleModel<Simulation::ColorVertex> subModel;

	auto & params = Params.Basic;
	// rotation and scale
	DiagonalMatrix3F scale(Parent().Box.Radius);
	const float3 & position = Parent().Box.Position;
	const float3x3 & transformation = scale * Parent().Box.Rotation;

	f32 min = -1.f;
	f32 max = -1.f + Height;
	subModel.AddPolyhedron(min, max, Parent().Profile, params.Color);

	model.AddTriangleModel(subModel, transformation, position);
}

template<>
void Building::Walls::GenerateTriangleModel<Building::Walls::Types::Undefined>(
	Simulation::TriangleModel<Simulation::ColorVertex> &
) const {
	DD_WARNING("DD::Building: Undefined type of wall.");
}
#pragma endregion

#pragma region Building::Walls::Xml
static const Text::StringView16 WallType(L"Type");

void Building::Walls::Params::ToXml(XML::Element & target) const {
	target.DefineAttribute(WallType, Type.ToString());
	_PARAMS_XML_SWITCH(_PARAMS_XML_SERIALIZE, DD_BUILDING_WALL_TYPES);
}

void Building::Walls::Params::FromXml(const XML::Element & source) {
	Type = Types::FromString(source[WallType]);
	_PARAMS_XML_SWITCH(_PARAMS_XML_DESERIALIZE, DD_BUILDING_WALL_TYPES);
}
#pragma endregion

/************************************************************************/
/* Building::Roofs                                                      */
/************************************************************************/
#pragma region Building::Roofs::GeneratePointCloud
void Building::Roofs::GeneratePointCloud(
	Simulation::PointCloud & target, size_t count, Random::Generator & generator
) const {
	switch (Params.Type) {
		// automatically declare all enum calls
		DD_XLIST_EX(
			_BUILDING_OVERLOAD_CASE,
			(GeneratePointCloud, (target, count, generator)),
			DD_BUILDING_ROOF_TYPES
		);
	default:
		throw;
	}
}

template<>
void Building::Roofs::GeneratePointCloud<Building::Roofs::Types::Flat>(
	Simulation::PointCloud & target, size_t count, Random::Generator & generator
) const {
	auto & params = Params.Flat;
	// rotation and scale
	DiagonalMatrix3F scale(Parent().Box.Radius);
	const float3 & position = Parent().Box.Position;
	const float3x3 & transformation = scale * Parent().Box.Rotation;
	// height offsets
	f32 roofOffset = 1.f - Height;

	Array<float2> samples(count);
	Parent().Profile.GenerateRandomPointsOverSurface(samples, generator);

	for (const float2 & sample : samples) {
		Simulation::QuadVertex v;
		v.Color = params.Color;
		// set centralized position
		v.Position.y = roofOffset;
		v.Position.xz = sample;
		// transform to building position
		v.Position = transformation * v.Position + position;

		target->AddPoint(v);
	}
}

template<>
void Building::Roofs::GeneratePointCloud<Building::Roofs::Types::Pyramidal>(
	Simulation::PointCloud & target, size_t count, Random::Generator & generator
) const {
	typedef Polygon<f32, 3> triangle_t;

	auto & params = Params.Pyramidal;
	// rotation and scale
	DiagonalMatrix3F scale(Parent().Box.Radius);
	const float3 & position = Parent().Box.Position;
	const float3x3 & transformation = scale * Parent().Box.Rotation;
	// height offsets
	f32 roofOffset = 1.f - Height;

	Array<float2> samples(count);
	Parent().Profile.GenerateRandomPointsOverSurface(samples, generator);
	f32 exc = Parent().Profile.InscribedRadius();
	//exc = exc / sqrt(2);
	float2 S = params.Center;
	float2 A = float2(-exc, -exc) + S;
	float2 B = float2(exc, -exc) + S;
	float2 C = float2(exc, exc) + S;
	float2 D = float2(-exc, exc) + S;
	float2 AB = S - (A + B) * 0.5f;
	float2 BC = S - (C + B) * 0.5f;
	float2 CD = S - (C + D) * 0.5f;
	float2 DA = S - (A + D) * 0.5f;
	f32 ABN = Math::Norm(AB);
	f32 BCN = Math::Norm(BC);
	f32 CDN = Math::Norm(CD);
	f32 DAN = Math::Norm(DA);
	triangle_t t0(A, B, S);
	triangle_t t1(B, C, S);
	triangle_t t2(C, D, S);
	triangle_t t3(D, A, S);

	auto computeHeight = [&](f32 projectionSize, const float2 & projectionLine, const float2 & x) {
		float2 proj = projectionLine * (Math::Dot(x - S, projectionLine) / (projectionSize*projectionSize));
		f32 ratio = 1 - (Math::Norm(proj) / projectionSize);
		return roofOffset + Height * ratio;
	};

	for (const float2 & sample : samples) {
		Simulation::QuadVertex v;
		v.Color = params.Color;
		// set centralized position
		v.Position.xz = sample;

		if (t0.IsIn(sample)) {
			v.Position.y = computeHeight(ABN, AB, sample);
		}
		else if (t1.IsIn(sample)) {
			v.Position.y = computeHeight(BCN, BC, sample);
		}
		else if (t2.IsIn(sample)) {
			v.Position.y = computeHeight(CDN, CD, sample);
		}
		else if (t3.IsIn(sample)) {
			v.Position.y = computeHeight(DAN, DA, sample);
		}
		else {
			v.Position.y = roofOffset;
		}

		// transform to building position
		v.Position = transformation * v.Position + position;
		target->AddPoint(v);
	}
}

template<>
void Building::Roofs::GeneratePointCloud<Building::Roofs::Types::Gabled>(
	Simulation::PointCloud & target, size_t count, Random::Generator & generator
) const {
	typedef Polygon<f32, 4> square_t;
	typedef Polygon<f32, 3> triangle_t;

	auto & params = Params.Gabled;
	// rotation and scale
	DiagonalMatrix3F scale(Parent().Box.Radius);
	const float3 & position = Parent().Box.Position;
	const float3x3 & transformation = scale * Parent().Box.Rotation;
	// height offsets
	f32 roofOffset = 1.f - Height;

	Array<float2> samples(count);
	Parent().Profile.GenerateRandomPointsOverSurface(samples, generator);

	float2 S = params.Center;
	float2 A = float2(-1.f, -1.f) + S;
	float2 B = float2(+1.f, -1.f) + S;
	float2 C = float2(+1.f, +1.f) + S;
	float2 D = float2(-1.f, +1.f) + S;
	float2 BC = S - (C + B) * 0.5f;
	float2 DA = S - (A + D) * 0.5f;
	f32 BCN = Math::Norm(BC);
	f32 DAN = Math::Norm(DA);

	square_t t0(B, C, (C + D) * 0.5f, (A + B) * 0.5f);
	square_t t1(D, A, (A + B) * 0.5f, (C + D) * 0.5f);

	auto computeHeight = [&](f32 projectionSize, const float2 & projectionLine, const float2 & x) {
		float2 proj = projectionLine * (Math::Dot(x - S, projectionLine) / (projectionSize*projectionSize));
		f32 ratio = 1 - (Math::Norm(proj) / projectionSize);
		return roofOffset + Height * ratio;
	};

	Simulation::QuadVertex v;
	v.Color = params.MainColor;
	for (const float2 & sample : samples) {
		// set centralized position
		v.Position.xz = sample;
		if (t0.IsIn(sample)) {
			v.Position.y = computeHeight(BCN, BC, sample);
		}
		else if (t1.IsIn(sample)) {
			v.Position.y = computeHeight(DAN, DA, sample);
		}
		else {
			v.Position.y = roofOffset;
		}
		// transform to building position
		v.Position = transformation * v.Position + position;
		target->AddPoint(v);
	}


	triangle_t q0(
		float2(0.f, roofOffset),
		float2(0.5f, roofOffset + Height),
		float2(1.f, roofOffset)
	);

	Array<float2> sideSamples(count / 10);
	q0.GenerateRandomPointsOverSurface(sideSamples, generator);

	float2 BA = (B - A);
	float2 DC = (D - C);
	v.Color = params.SideColor;
	for (const float2 & sample : sideSamples) {
		v.Position.xz = (A + BA * sample.x);
		v.Position.y = sample.y;
		// transform to building position
		v.Position = transformation * v.Position + position;
		target->AddPoint(v);
		v.Position.xz = (C + DC * sample.x);
		v.Position.y = sample.y;
		// transform to building position
		v.Position = transformation * v.Position + position;
		target->AddPoint(v);
	}
}

template<>
void Building::Roofs::GeneratePointCloud<Building::Roofs::Types::Dome>(
	Simulation::PointCloud & target, size_t count, Random::Generator & generator
) const {
	auto & params = Params.Dome;
	// rotation and scale
	DiagonalMatrix3F scale(Parent().Box.Radius);
	const float3 & position = Parent().Box.Position;
	const float3x3 & transformation = scale * Parent().Box.Rotation;
	// height offsets
	f32 roofOffset = 1.f - Height;


	Array<float2> samples(count);
	Parent().Profile.GenerateRandomPointsOverSurface(samples, generator);

	//f32 r = Parent().Profile.InscribedRadius();
	f32 r2 = params.Radius * params.Radius;
	f32 norm = Height / params.Radius;

	for (const float2 & sample : samples) {
		Simulation::QuadVertex v;

		v.Position.xz = sample;
		v.Position.y = roofOffset;
		f32 distance2 = Math::Dot(params.Center - sample);
		if (distance2 < r2) {
			v.Color = params.DomeColor;
			v.Position.y = roofOffset + norm * Math::Sqrt(r2 - distance2);
		}
		else {
			v.Color = params.EdgeColor;
		}

		// transform to building position
		v.Position = transformation * v.Position + position;
		target->AddPoint(v);
	}
}

template<>
void Building::Roofs::GeneratePointCloud<Building::Roofs::Types::Undefined>(
	Simulation::PointCloud &, size_t, Random::Generator &
) const {
	DD_WARNING("DD::Building: Undefined type of roof.");
}
#pragma endregion

#pragma region Building::Roofs::GenerateTriangleRegion
void Building::Roofs::GenerateTriangleModel(Simulation::TriangleModel<Simulation::ColorVertex> & model) const {
	switch (Params.Type) {
		// automatically declare all enum calls
		DD_XLIST_EX(
			_BUILDING_OVERLOAD_CASE,
			(GenerateTriangleModel, (model)),
			DD_BUILDING_ROOF_TYPES
		);
	default:
		throw;
	}
}

template<>
void Building::Roofs::GenerateTriangleModel<Building::Roofs::Types::Dome>(
	Simulation::TriangleModel<Simulation::ColorVertex> & model
) const {
	auto & rParams = Params.Dome;
	auto & wParams = Parent().Wall;
	// rotation and scale
	DiagonalMatrix3F scale(Parent().Box.Radius);
	const float3 & position = Parent().Box.Position;
	const float3x3 & transformation = scale * Parent().Box.Rotation;

	Simulation::TriangleModel<Simulation::ColorVertex> subModel;
	f32 buildingOffset = 1.f;
	f32 roofOffset = buildingOffset - Height;

	subModel.AddDome(
		float3(rParams.Center.x, roofOffset, rParams.Center.y),
		rParams.Radius,
		Height,
		rParams.DomeColor
	);
	subModel.AddPolygon(roofOffset, Parent().Profile, rParams.EdgeColor);
	model.AddTriangleModel(subModel, transformation, position);
}

template<>
void Building::Roofs::GenerateTriangleModel<Building::Roofs::Types::Flat>(
	Simulation::TriangleModel<Simulation::ColorVertex> & model
) const {
	auto & params = Params.Flat;
	// rotation and scale
	DiagonalMatrix3F scale(Parent().Box.Radius);
	const float3 & position = Parent().Box.Position;
	const float3x3 & transformation = scale * Parent().Box.Rotation;

	f32 buildingOffset = 1.f;
	f32 roofOffset = buildingOffset - Height;

	Simulation::TriangleModel<Simulation::ColorVertex> subModel;
	subModel.AddPolygon(roofOffset, Parent().Profile.Points, params.Color);

	model.AddTriangleModel(subModel, transformation, position);
}

template<>
void Building::Roofs::GenerateTriangleModel<Building::Roofs::Types::Gabled>(
	Simulation::TriangleModel<Simulation::ColorVertex> & model
) const {
	auto & params = Params.Gabled;
	// rotation and scale
	DiagonalMatrix3F scale(Parent().Box.Radius);
	const float3 & position = Parent().Box.Position;
	const float3x3 & transformation = scale * Parent().Box.Rotation;

	f32 buildingOffset = 1.f;
	f32 roofOffset = buildingOffset - Height;

	float2 dims(1.f, 1.f);
	float2 S = params.Center;
	float2 A = float2(-dims.x, -dims.y) + S;
	float2 B = float2(dims.x, -dims.y) + S;
	float2 C = float2(dims.x, dims.y) + S;
	float2 D = float2(-dims.x, dims.y) + S;
	float2 AB = (A + B) * 0.5f;
	float2 CD = (C + D) * 0.5f;

	float3 a(A.x, roofOffset, A.y);
	float3 b(B.x, roofOffset, B.y);
	float3 c(C.x, roofOffset, C.y);
	float3 d(D.x, roofOffset, D.y);
	float3 ab(AB.x, 1.f, AB.y);
	float3 cd(CD.x, 1.f, CD.y);

	Simulation::TriangleModel<Simulation::ColorVertex> subModel;
	subModel.AddQuad(a, d, cd, ab, params.MainColor);
	subModel.AddQuad(b, c, cd, ab, params.MainColor);
	subModel.AddTriangle(a, b, ab, params.SideColor);
	subModel.AddTriangle(c, d, cd, params.SideColor);
	model.AddTriangleModel(subModel, transformation, position);
}

template<>
void Building::Roofs::GenerateTriangleModel<Building::Roofs::Types::Pyramidal>(
	Simulation::TriangleModel<Simulation::ColorVertex> & model
) const {
	auto params = Params.Pyramidal;
	// rotation and scale
	DiagonalMatrix3F scale(Parent().Box.Radius);
	const float3 & position = Parent().Box.Position;
	const float3x3 & transformation = scale * Parent().Box.Rotation;

	f32 buildingOffset = 1.f;
	f32 roofOffset = buildingOffset - Height;

	float3 top;
	top.xz = params.Center;
	top.y = 1.f;

	Simulation::TriangleModel<Simulation::ColorVertex> subModel;
	subModel.AddPyramidal(
		roofOffset,
		top,
		Parent().Profile,
		params.Color
	);

	model.AddTriangleModel(subModel, transformation, position);
}

template<>
void Building::Roofs::GenerateTriangleModel<Building::Roofs::Types::Undefined>(
	Simulation::TriangleModel<Simulation::ColorVertex> &
) const {
	DD_WARNING("DD::Building: Undefined type of roof.");
}
#pragma endregion

#pragma region Building::Roofs::Xml
static const Text::StringView16 RoofType(L"Type");

void Building::Roofs::Params::ToXml(XML::Element & target) const {
	target.DefineAttribute(RoofType, Type.ToString());
	_PARAMS_XML_SWITCH(_PARAMS_XML_SERIALIZE, DD_BUILDING_ROOF_TYPES);
}

void Building::Roofs::Params::FromXml(const XML::Element & source) {
	Type = Types::FromString(source[RoofType]);
	_PARAMS_XML_SWITCH(_PARAMS_XML_DESERIALIZE, DD_BUILDING_ROOF_TYPES);
}
#pragma endregion

/************************************************************************/
/* BuildingCollection                                                   */
/************************************************************************/
#pragma region BuildingCollection
BuildingCollection::BuildingCollection()
	: Array()
{
	// prepare profile for buildings
	Polygon<f32, 4> sqProfile(
		float2(-1.f, -1.f),
		float2(-1.f, +1.f),
		float2(+1.f, +1.f),
		float2(+1.f, -1.f)
	);

	// prepare wall types for buildings
	for (Building & sq : Squares) {
		sq.Profile = sqProfile;
		sq.Wall.Params.Type = DD::Building::Walls::Types::Basic;
		sq.Wall.Params.Basic.Color = DD::Color::Collection::GREY;
		sq.Wall.Height = 1.f;
	}

	// flat on square profile
	SquareFlat.Wall.Height = 2.f;
	SquareFlat.Roof.Height = 0.f;
	SquareFlat.Roof.Params.Type = DD::Building::Roofs::Types::Flat;
	SquareFlat.Roof.Params.Flat.Color = DD::Color::Collection::LIGHT_GREY;
	SquareFlat.Box.Position.Set(0.f, 1.f, 0.f);

	// gabled on square profile
	SquareGabled.Wall.Height = 1.0f;
	SquareGabled.Roof.Height = 1.0f;
	SquareGabled.Roof.Params.Type = Building::Roofs::Types::Gabled;
	SquareGabled.Roof.Params.Gabled.Center = 0.f;
	SquareGabled.Roof.Params.Gabled.MainColor = Color::Collection::RED;
	SquareGabled.Roof.Params.Gabled.SideColor = Color::Collection::WHITE;
	SquareGabled.Box.Position.Set(0.f, 1.f, 0.f);

	// pyramidal on square profile
	SquarePyramidal.Wall.Height = 1.0f;
	SquarePyramidal.Roof.Height = 1.0f;
	SquarePyramidal.Roof.Params.Type = DD::Building::Roofs::Types::Pyramidal;
	SquarePyramidal.Roof.Params.Pyramidal.Center = 0.f;
	SquarePyramidal.Roof.Params.Pyramidal.Color = Color::Collection::YELLOW;
	SquarePyramidal.Box.Position.Set(0.f, 1.f, 0.f);

	// dome on square profile
	SquareDome.Wall.Height = 1.5f;
	SquareDome.Roof.Height = 0.5f;
	SquareDome.Roof.Params.Type = DD::Building::Roofs::Types::Dome;
	SquareDome.Roof.Params.Dome.Center = 0.f;
	SquareDome.Roof.Params.Dome.EdgeColor = DD::Color::Collection::LIGHT_GREY;
	SquareDome.Roof.Params.Dome.DomeColor = DD::Color::Collection::LIGHT_BLUE;
	SquareDome.Roof.Params.Dome.Radius = 1.f;
	SquareDome.Box.Position.Set(0.f, 1.f, 0.f);
}
#pragma endregion
