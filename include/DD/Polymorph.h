#pragma once
namespace DD {
	/**
	 * Polymorph structure.
	 * Allocates data on locally or remotely based on param LOCAL.
	 */
	template<typename T, bool LOCAL>
	class Polymorph;
}

#include <DD/Unique.h>

namespace DD {
	template<typename T>
	class Polymorph<T, true> {
		/** Value storage. */
		T _value;

	public: // methods
		/** Get pointer to base type. */
		constexpr T * Ptr() { return &_value; }
		/** Get pointer to base type. */
		constexpr const T * Ptr() const { return &_value; }

	public: // constructors
		/** Fwd constructor. */
		template<typename...ARGS>
		Polymorph(ARGS...args) : _value(args...) {}
		/** Copy constructor. */
		Polymorph(const Polymorph &) = delete;
		/** Move constructor. */
		Polymorph(Polymorph &&) = delete;
	};

	template<typename T>
	class Polymorph<T, false> {
		/** Value storage. */
		Unique<T> _value;

	public: // methods
		/** Get pointer to base type. */
		constexpr T * Ptr() { return _value; }
		/** Get pointer to base type. */
		constexpr const T * Ptr() const { return _value; }

	public: // constructors
		/** Fwd constructor. */
		template<typename...ARGS>
		Polymorph(ARGS...args) : _value(new T(args...)) {}
		/** Copy constructor. */
		Polymorph(const Polymorph &) = delete;
		/** Move constructor. */
		Polymorph(Polymorph &&) = default;
	};
}
