#pragma once
namespace DD {
	template<typename T>
	struct DescComparer {
		static bool Compare(const T & l, const T & r) { return l <= r; }
	};

	template<typename T>
	struct AscComparer {
		static bool Compare(const T & l, const T & r) { return l >= r; }
	};

	template<typename KEY, typename VALUE, typename COMPARER = AscComparer<KEY>>
	class OederedList;
}

#include "Array.h"

namespace DD {
	template<typename KEY, typename VALUE, typename COMPARER>
	class OederedList {
	public: // nested
		struct Pair { const KEY Key; VALUE Value; };
	public: // constants
		static constexpr KEY * EMPTY_KEY = 0;
		static constexpr VALUE * EMPTY_VALUE = 0;
		static constexpr Pair EMPTY_PAIR{ *EMPTY_KEY, *EMPTY_VALUE }
		static constexpr size_t EMPTY_NODE = static_cast<size_t>(-1U);
		static constexpr size_t PREALLOCATION = static_cast<size_t>(-1U);
	private: // static methods
		static void swap(size_t & l, size_t & r) {
			size_t tmp = l;
			l = r;
			r = tmp;
		}
		static bool compare(const KEY & l, const KEY & r) {
			return COMPARER::Compare(l, r);
		}
	private: // properties
		Array<Pair> _data;
		Array<size_t> _mapping;
		size_t _size;
	private: // methods
		const KEY & key(size_t index) const { return _data[_mapping[index]].Key; }
		const VALUE & value(size_t index) const { return _data[_mapping[index]].Value; }
		size_t find(const KEY & key) {
			size_t index = 0;
			for (size_t i = _size >> 1; i > 0; i >>= 1) {
				if (compare(key(index + i), key)) {
					index += i;
				}
			}
		}
		void resize(size_t size) {
			_data.Expand(size);
			_mapping.Expand(size);
		}
		void bouble(size_t index) {
			while (compare(_data[index], _data[index - 1])) {
				swap(_mapping[index], _mapping[index - 1]);
				if (--index == 0) break;
			}
		}
		VALUE & insertKey(const KEY & key) {
			_data[_size].Key = key;
			_mapping[_size] = _size;
			bouble(_size);
			return _data[_size++].Value;
		}
	public: // operators
		Pair & operator[](size_t key) { return _data[_mapping[key]]; }
		const Pair & operator[](size_t key) const { return _data[_mapping[key]]; }
		Pair & operator[](KEY key) { return EMPTY_PAIR; }
		const Pair & operator[](KEY key) const { return EMPTY_PAIR; }
	public: // methods
		size_t Capacity() const { return _data.Size(); }
		/** Bouble sort. */
		void Insert(const KEY & key, const VALUE & value) {
			if (_size == _data.Size())

		}
		VALUE & InsertKey(KEY key) {

		}
	};
}

