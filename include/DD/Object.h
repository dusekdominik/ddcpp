#pragma once
namespace DD::Simulation {
	/**
	 * ObjectBase
	 * ObjectBase is instance of IModel.
	 * Contains info about scale, rotation and position.
	 * Graphic data are contained in model.
	 */
	class ObjectBase;

	/**
	 * Referential implementation of ObjectBase.
	 */
	struct Object;
}

#include <DD/Gpu/ConstantBuffer.h>
#include <DD/Delegate.h>
#include <DD/Primitives.h>
#include <DD/Reference.h>
#include <DD/Simulation/ConstantBuffers.h>
#include <DD/Simulation/IModel.h>
#include <DD/String.h>

namespace DD::Simulation {
	class ObjectBase
		: public IReferential
	{
		friend struct Object;

	protected: // properties
		/** Flag if  model is enabled. */
		bool _enabled, _expired;
		/** Flags for constant buffer handling. */
		bool _changed;
		/** Is effect enabled. */
		bool _effect;
		/** Reference to a model. */
		Simulation::Model _model;
		/** Position of object. */
		float3 _position, _scale;
		/** Rotation of object. */
		float3x3 _rotation;
		/** Constant buffer containing object information. */
		Gpu::ConstantBuffer<ObjectConstantBuffer> _cbuffer;
		/** Name of a object. */
		String _name;
		/** Drawing args. */
		IModel::DrawArgs _modelDrawArgs;

	public: // events
		/** Event fired when object expires. */
		Event<ObjectBase *> OnObjectExpiration;
		/** Simple update before rendering (should not be expensive). */
		Event<ObjectBase *> OnObjectUpdate;
		/** On property effect change. */
		Event<ObjectBase *, bool> OnEffectChange;
		/** */
		Event<ObjectBase *> OnInit;

	private: // methods
		/** Method handling init, update and bind of constant buffer. */
		void updateConstantBuffer();

	public: // getters
		/** Get bounding sphere of object. */
		Sphere3F BoundingSphere() const;
		/** Get bounding AABox of object. */
		AABox3F BoundingAABox() const;
		/** Get effect. */
		bool Effect() const { return _effect; }
		/** Check if object is expired. */
		bool Expired() const { return _expired; }
		/** Checks if object is enabled for rendering. */
		bool Enabled() const { return _enabled && _model->Enabled(); }
		/** Translation of model. */
		float3 & Position() { _changed = true; return _position; }
		/** Scale of model. */
		float3 & Scale() { _changed = true; return _scale; }
		/** Rotation of model. */
		float3x3 & Rotation() { _changed = true; return _rotation; }
		/** Model of object. */
		Simulation::Model Model() { return _model; }
		/** Translation of model. */
		const float3 & Position() const { return _position; }
		/** Scale of model. */
		const float3 & Scale() const { return _scale; }
		/** Rotation of model. */
		const float3x3 & Rotation() const { return _rotation; }
		/** Accumulated transformations of object. */
		float4x4 Transform() const;
		/** Getter of object name. */
		const String & Name() const { return _name; }

	public: // setters
		/** Set model parameters */
		ObjectBase & ModelDrawArgs(const IModel::DrawArgs & args) { _modelDrawArgs = args; return *this; }
		/** Set effect. */
		ObjectBase & Effect(bool enabled);
		/** Set object enabled.  */
		ObjectBase & Enabled(bool enabled) { _enabled = enabled; return *this; }
		/** Setter of object name. */
		ObjectBase & Name(const String & name) { _name = name; return *this; }
		/** Setter of object rotation. */
		ObjectBase & Position(const float3 & position) { Position() = position; return *this; }
		/** Setter of object position. */
		ObjectBase & Rotation(const float3x3 & rotation) { Rotation() = rotation; return *this; }

	public: // transformations
		/** Rotate object around X axis. Given angle in RAD. */
		ObjectBase & RotateX(f32 f);
		/** Rotate object around Y axis. Given angle in RAD. */
		ObjectBase & RotateY(f32 f);
		/** Rotate object around Z axis. Given angle in RAD. */
		ObjectBase & RotateZ(f32 f);
		/** Rotate object around given axis. Given angle in RAD. */
		ObjectBase & RotateAxis(f32 f, const float3 & axis);
		/** Move object by given translation. */
		ObjectBase & Translate(const float3 & v);
		/** Move object by given translation. */
		ObjectBase & Translate(f32 x, f32 y, f32 z);

	public: // render connected methods
		/** Expire object. */
		ObjectBase & Expire();
		/** Render object. */
		ObjectBase & Draw();
		/** Render object. */
		ObjectBase & DrawCulled(const Frustum3F & frustum);

	protected: // constructors
		/** Default instantiation of given model. */
		ObjectBase(Gpu::Device & device, Simulation::Model & model, const String & name);

	public:// constructors & assignment
		/** Assignment operator - assigns info about object, constant buffer is kept separately. */
		ObjectBase & operator=(const ObjectBase & o);
		/** Destructor. */
		~ObjectBase();
	};


	struct Object
		: public Reference<ObjectBase>
	{
		/** Zero constructor, inits object to the null reference. */
		Object() : Reference() {}
		/** Capture constructor. */
		Object(ObjectBase * base) : Reference(base) {}
		/** Default constructor, instantiates given model. */
		Object(Gpu::Device & device, Simulation::Model & model, const String & name);
	};
}
