#pragma once
/**
 * Library for work with las files.
 * @see http://www.asprs.org/a/society/committees/standards/asprs_las_format_v12.pdf
 * Simplified usage:
 *   @see DD::Las::LoadPointCloud(path);
 *   @see DD::Las::SavePointCloud(pointCloud, format)
 */
namespace DD::Las {
	/** Input stream. */
	typedef void * istream_t;
	/** Output stream */
	typedef void * ostream_t;
	/** Enum with classifications of point. */
	class Classification;
	/** Enum with point formats. */
	class Formats;
	/** Enum with header properties offsets. */
	enum struct HeaderOffsets : unsigned;
	/**
	 * Collection of methods for computing coordinates.
	 * @see Hoffmann-Wellenhof, B., Lichtenegger, H., and Collins, J.,
	 * GPS: Theory and Practice, 3rd ed.  New York: Springer-Verlag Wien, 1994.
	 */
	struct Math;
	/** Interface of interaction with points. */
	struct IPointInfo;
	/** Interface for interaction with file streams. */
	struct IIO;
	/** Implementation of IIO interface. */
	template<size_t S>
	struct IO;
	/** Header of las file. */
	struct Header;
	/** VLR of las file. */
	struct VariableLengthRecord;
	/** Point of las file defined by its format - implementation of IPoint. */
	template<unsigned char>
	struct Point;
	/** Las file abstraction. */
	struct File;
	/** Reader of Las files. */
	struct FileReader;
	/** Writer of las files. */
	struct FileWriter;
}

#include <DD/Array.h>
#include <DD/Enum.h>
#include <DD/IProgress.h>
#include <DD/Math.h>
#include <DD/Primitives.h>
#include <DD/Simulation/PointCloud.h>
#include <DD/Unique.h>
#include <DD/Vector.h>


namespace DD::Las {
	DD_ENUM_8(
		(Classification, "Common types of point."),
		(NeverClassified, 0),
		(Unclassified, 1),
		(Ground, 2),
		(LowVegetation, 3),
		(MediumVegetation, 4),
		(HighVegetation, 5),
		(Building, 6),
		(Mass, 7),
		(Noise, 8),
		(Water, 9)
	);
	DD_ENUM_8(
		(Formats, "Point formats of the LAS standard."),
		(Format0,    0, "Common colorless point."),
		(Format1,    1, "Common colorless point."),
		(Format2,    2, "Common colored point."),
		(Format3,    3, "Common colored point"),
		(FormatDD, 255, "Out of standard - colored point optimized for fast loading.")
	);


	enum struct HeaderOffsets : u32 {
		FileSignature = 0, FileSourceID = 4, GlobalEncoding = 6,
		GUID1 = 8, GUID2 = 12, GUID3 = 14, GUID4 = 16,
		VersionMajor = 24, VersionMinor = 25,
		SystemId = 26, SW = 58, FileCreationDay = 90, FileCreationYear = 92,
		HeaderSize = 94, PointOffset = 96, NumberOfVariableLengthRecords = 100,
		PointDataFormatID = 104, PointDataRecordLength = 105,
		NumberOfPointRecords = 107, NumberOfPointRecordsByReturn = 111,
		ScaleX = 131, ScaleY = 139, ScaleZ = 147,
		OffsetX = 155, OffsetY = 163, OffsetZ = 171,
		MaxX = 179, MinX = 187,
		MaxY = 195, MinY = 203,
		MaxZ = 211, MinZ = 219
	};


	struct Math {
	public: // constants
		static constexpr f64 MajorAxis = 6378137.0;
		static constexpr f64 MinorAxis = 6356752.314;
		static constexpr f64 sm_EccSquared = 6.69437999013e-03;
		static constexpr f64 UTMScaleFactor = 0.9996;
	public:
		/**
		 * Computes the ellipsoidal distance from the equator to a point at a
		 * given latitude.
		 * @param phi - Latitude of the point, in radians.
		 * @return The ellipsoidal distance of the point from the equator, in meters.
		 */
		static f64 ArcLengthOfMeridian(f64 phi);
		/**
		 * Determines the central meridian for the given UTM zone.
		 * @param  zone - An integer value designating the UTM zone, range [1,60].
		 *
		 * @return
		 *   The central meridian for the given UTM zone, in radians, or zero
		 *   if the UTM zone parameter is outside the range [1,60].
		 *   Range of the central meridian is the radian equivalent of [-177,+177].
		 */
		static f64 UTMCentralMeridian(u32 zone);
		/**
		 * Computes the footpoint latitude for use in converting transverse
		 * Mercator coordinates to ellipsoidal coordinates.
		 * @param y - The UTM northing coordinate, in meters.
		 * @return The footpoint latitude, in radians.
		 */
		static f64 FootpointLatitude(f64 y);
		/**
		 * Converts a latitude/longitude pair to x and y coordinates in the
		 * Transverse Mercator projection.  Note that Transverse Mercator is not
		 * the same as UTM; a scale factor is required to convert between them.
		 * @param   phi - Latitude of the point, in radians.
		 * @param   lambda - Longitude of the point, in radians.
		 * @param   lambda0 - Longitude of the central meridian to be used, in radians.
		 * @return xy - A 2-element array containing the x and y coordinates of the computed point.
		 */
		static void MapLatLonToXY(f64 phi, f64 lambda, f64 lambda0, f64 & x, f64 & y);
		/**
		 * Converts x and y coordinates in the Transverse Mercator projection to
		 * a latitude/longitude pair.  Note that Transverse Mercator is not
		 * the same as UTM; a scale factor is required to convert between them.
		 * @param  x - The easting of the point, in meters.
		 * @param  y - The northing of the point, in meters.
		 * @param  lambda0 - Longitude of the central meridian to be used, in radians.
		 *
		 * @return lat, lon
		 *
		 *   The locals Nf, nuf2, tf, and tf2 serve the same purpose as
		 *   N, nu2, t, and t2 in MapLatLonToXY, but they are computed with respect
		 *   to the footpoint latitude phif.
		 *
		 *   x1frac, x2frac, x2poly, x3poly, etc. are to enhance readability and
		 *   to optimize computations.
		 */
		static void MapXYToLatLon(f64 x, f64 y, f64 lambda0, f64 & lat, f64 & lon);
		/**
		 * Converts a latitude/longitude pair to x and y coordinates in the
		 * Universal Transverse Mercator projection.
		 * @param lat - Latitude of the point, in radians.
		 * @param lon - Longitude of the point, in radians.
		 * @param zone - UTM zone to be used for calculating values for x and y.
		 *          If zone is less than 1 or greater than 60, the routine
		 *          will determine the appropriate zone from the value of lon.
		 * @return
		 *   x, y - the UTM x and y values will be stored, by reference
		 *   The UTM zone used for calculating the values of x and y is returned.
		 */
		static u32 LatLonToUTMXY(f64 lat, f64 lon, u32 zone, f64 & x, f64 & y);
		/**
		 * Converts x and y coordinates in the Universal Transverse Mercator
		 * projection to a latitude/longitude pair.
		 * @param  x - The easting of the point, in meters.
		 * @param  y - The northing of the point, in meters.
		 * @param  zone - The UTM zone in which the point lies.
		 * @param  southhemi - True if the point is in the southern hemisphere; false otherwise.
		 * @return lat,lon - in radians.
		 */
		static void UTMXYToLatLon(f64 x, f64 y, u32 zone, bool southhemi, f64 & lat, f64 & lon);
		/**
		 * Input in degrees
		 * @return distance in meters
		 */
		static f64 DistanceAproximation(f64 xlat, f64 xlon, f64 ylat, f64 ylon);
	};


	struct IPointInfo {
		/** Point color getter. */
		virtual const V3<u16> GetColor() const = 0;
		/** Point position getter. */
		virtual const double3 GetPosition() const = 0;
		/** Point classification. */
		virtual const Classification GetClassification() const = 0;
		/** Point color setter. */
		virtual void SetColor(const V3<u16> &) = 0;
		/** Point position setter. */
		virtual void SetPosition(const double3 &) = 0;
		/** Point classification setter. */
		virtual void SetClassification(Classification) = 0;
		/** Format self identification. */
		virtual Formats GetFormat() const = 0;
	};


	struct IIO {
		/** Load from a stream. */
		virtual void Load(istream_t &) = 0;
		/** Save to a stream. */
		virtual void Save(ostream_t &) const = 0;
	};


	struct IPoint : public IPointInfo, public virtual IIO {};


	template<size_t SIZE>
	struct IO
		: public virtual IIO
	{
	public: // statics
		/** Gets flag from a data u32. */
		template<u32 FSIZE>
		static u32 flag(u32 data, int offset) { return (data >> offset) & (0xFFFFFFFF >> (32 - FSIZE)); }
		/** Real size of data. */
		static constexpr size_t IO_SIZE = SIZE;
	private: // properties
		/** Just data in native form. */
		u8 _data[SIZE];
	protected: // nonpublic methods
		/** Const getter of data. */
		template<typename T, size_t OFFSET>
		const T * get() const { return (T *)(_data + OFFSET); }
		/** Getter of data. */
		template<typename T, size_t OFFSET>
		T * get() { return (T *)(_data + OFFSET); }
	public: //  IIO
		/** Setter of data. */
		template<size_t OFFSET, typename SET>
		void Set(const SET & data) { memcpy(_data, &data, sizeof(SET)); }
	public: // IIO
		virtual void Load(istream_t & stream) override;
		virtual void Save(ostream_t & stream) const override;
	};

	struct Header
		: public IO<227>
	{
	public: // IIO
		virtual void Load(istream_t & stream) override;
		virtual void Save(ostream_t & stream) const override;

	public: // const getters
		/** File Signature (�LASF�) - char[4] 4 bytes. */
		const char * FileSignature() const { return get<char, (size_t)HeaderOffsets::FileSignature>(); }
		/** Point Data Format ID (0-99 for spec) - 1 byte. */
		Formats PointDataFormatID() const { return *get<Formats, (size_t)HeaderOffsets::PointDataFormatID>(); }

	public: // reference getters
		/** File Signature (�LASF�) - char[4] 4 bytes. */
		char * FileSignature() { return get<char, (size_t)HeaderOffsets::FileSignature>(); }
		/** System Identifier - char[32] 32 bytes */
		char * SystemId() { return get<char, (size_t)HeaderOffsets::SystemId>(); }
		/** Generating Software - char[32] 32 bytes. */
		char * SW() { return get<char, (size_t)HeaderOffsets::SW>(); }
		/** Project ID - GUID data 4 - u8[8] 8 bytes. */
		char * GUID4() { return get<char, (size_t)HeaderOffsets::GUID4>(); }
		/** Project ID - GUID data 1 - unsigned long 4 bytes. */
		u32 & GUID1() { return *get<u32, (size_t)HeaderOffsets::GUID1>(); }
		/** Offset to point data - unsigned long 4 bytes. */
		u32 & PointOffset() { return *get<u32, (size_t)HeaderOffsets::PointOffset>(); }
		/** Number of Variable Length Records - unsigned long 4 bytes. */
		u32 & NumberOfVariableLengthRecords() { return *get<u32, (size_t)HeaderOffsets::NumberOfVariableLengthRecords>(); }
		/** Number of point records - unsigned long 4 bytes. */
		u32 & NumberOfPointRecords() { return *get<u32, (size_t)HeaderOffsets::NumberOfPointRecords>(); }
		/** Number of points by return - unsigned long[5] 20 bytes. */
		u32 * NumberOfPointRecordsByReturn() { return get<u32, (size_t)HeaderOffsets::NumberOfPointRecordsByReturn>(); }
		/** File Source ID - unsigned short 2 bytes. */
		u16 & FileSourceID() { return *get<u16, (size_t)HeaderOffsets::FileSourceID>(); }
		/** Global Encoding - unsigned short 2 bytes. */
		u16 & GlobalEncoding() { return *get<u16, (size_t)HeaderOffsets::GlobalEncoding>(); }
		/** Project ID - GUID data 2 - unsigned short 2 byte. */
		u16 & GUID2() { return *get<u16, (size_t)HeaderOffsets::GUID2>(); }
		/** Project ID - GUID data 3 - unsigned short 2 byte. */
		u16 & GUID3() { return *get<u16, (size_t)HeaderOffsets::GUID3>(); }
		/** File Creation Day of Year - unsigned short 2 bytes. */
		u16 & FileCreationDay() { return *get<u16, (size_t)HeaderOffsets::FileCreationDay>(); }
		/** File Creation Year - unsigned short 2 bytes. */
		u16 & FileCreationYear() { return *get<u16, (size_t)HeaderOffsets::FileCreationYear>(); }
		/** Header Size - unsigned short 2 bytes. */
		u16 & HeaderSize() { return *get<u16, (size_t)HeaderOffsets::HeaderSize>(); }
		/** Point Data Record Length - unsigned short 2 bytes. */
		u16 & PointDataRecordLength() { return *get<u16, (size_t)HeaderOffsets::PointDataRecordLength>(); }
		/** Version Major - u8 1 byte. */
		u8 & VersionMajor() { return *get<u8, (size_t)HeaderOffsets::VersionMajor>(); }
		/** Version Minor - u8 1 byte. */
		u8 & VersionMinor() { return *get<u8, (size_t)HeaderOffsets::VersionMinor>(); }
		/** Point Data Format ID (0-99 for spec) - 1 byte. */
		Formats & PointDataFormatID() { return *get<Formats, (size_t)HeaderOffsets::PointDataFormatID>(); }
		/** X scale factor - float 8 bytes. */
		f64 & ScaleX() { return *get<f64, (size_t)HeaderOffsets::ScaleX>(); }
		/** Y scale factor - float 8 bytes. */
		f64 & ScaleY() { return *get<f64, (size_t)HeaderOffsets::ScaleY>(); }
		/** Z scale factor - float 8 bytes. */
		f64 & ScaleZ() { return *get<f64, (size_t)HeaderOffsets::ScaleZ>(); }
		/** X offset - float 8 bytes. */
		f64 & OffsetX() { return *get<f64, (size_t)HeaderOffsets::OffsetX>(); }
		/** Y offset - float 8 bytes. */
		f64 & OffsetY() { return *get<f64, (size_t)HeaderOffsets::OffsetY>(); }
		/** Z offset - float 8 bytes. */
		f64 & OffsetZ() { return *get<f64, (size_t)HeaderOffsets::OffsetZ>(); }
		/** Max X - float 8 bytes. */
		f64 & MinX() { return *get<f64, (size_t)HeaderOffsets::MinX>(); }
		/** Min X - float 8 bytes. */
		f64 & MaxX() { return *get<f64, (size_t)HeaderOffsets::MaxX>(); }
		/** Min Y - float 8 bytes. */
		f64 & MinY() { return *get<f64, (size_t)HeaderOffsets::MinY>(); }
		/** Max Y - float 8 bytes. */
		f64 & MaxY() { return *get<f64, (size_t)HeaderOffsets::MaxY>(); }
		/** Min Z - float 8 bytes. */
		f64 & MinZ() { return *get<f64, (size_t)HeaderOffsets::MinZ>(); }
		/** Max Z - float 8 bytes. */
		f64 & MaxZ() { return *get<f64, (size_t)HeaderOffsets::MaxZ>(); }

	public: // methods
		/** Set signature to default. */
		void InitSignature();
		/** Check if loaded header has a signature, */
		bool CheckSignature() const;
	};


	struct VariableLengthRecord
		: public IO<54>
	{
	private: // properties
		/** Dynamic data. */
		Array<u8> _data;

	public: // IIO
		virtual void Load(istream_t & stream) override;
		virtual void Save(ostream_t & stream) const override;

	public:
		/** @see http://www.asprs.org/a/society/committees/standards/asprs_las_format_v12.pdf */
		const u16 & Reserved() const { return *get<u16, 0>(); }
		/** @see http://www.asprs.org/a/society/committees/standards/asprs_las_format_v12.pdf */
		const char * UserId() const { return get<char, 2>(); }
		/** Id of record. */
		const u16 & RecordId() const { return *get<u16, 18>(); }
		/** Size of dynamic data allocation. */
		const u16 & RecordLengthAfterHeader() const { return *get<u16, 20>(); }
		/** Description of record. */
		const char * Description() const { return get<char, 22>(); }
		/** If dynamic data created, return ptr. */
		const u8 * Data() const { return _data.begin(); }
		/** If dynamic data created, return ptr. */
		u8 * Data() { return _data.begin(); }
	};


	template<u8>
	struct Point
		: IPoint
	{
	public: // IIO
		virtual void Load(istream_t & stream) override { throw; }
		virtual void Save(ostream_t & stream) const override { throw; }

	public: // IPointInfo
		virtual const double3 GetPosition() const override { throw; }
		virtual const V3<u16> GetColor() const override { throw; }
		virtual const Classification GetClassification() const override { throw; }
		virtual void SetColor(const V3<u16> & color) override { throw; }
		virtual void SetPosition(const double3 & point) override { throw; }
		virtual void SetClassification(Classification classification) override { throw; }
		virtual Formats GetFormat() const override { throw; }
	};


	template<>
	struct Point<Formats::Format0>
		: public IO<20>
		, public IPoint
	{
		/** X - coordinate of point. */
		const i32 & X() const { return *get<i32, 0>(); }
		/** Y - coordinate of point. */
		const i32 & Y() const { return *get<i32, 4>(); }
		/** Z - coordinate of point. */
		const i32 & Z() const { return *get<i32, 8>(); }

	public: // IIO
		virtual void Load(istream_t & stream) override;
		virtual void Save(ostream_t & stream) const override;

	public: // IPointInfo
		virtual const double3 GetPosition() const override { return double3((f64)X(), (f64)Y(), (f64)Z()); }
		virtual const V3<u16> GetColor() const override { return V3<u16>(0xFFFui16, 0xFFFFui16, 0xFFFFui16); }
		virtual const Classification GetClassification() const override { return Classification::Unclassified; }
		virtual void SetColor(const V3<u16> &) override { throw; }
		virtual void SetPosition(const double3 &) override { throw; }
		virtual void SetClassification(Classification) override { throw; }
		virtual Formats GetFormat() const override { return Formats::Format0; }
	};


	template<>
	struct Point<Formats::Format1>
		: public IO<28>
		, public IPoint
	{
		/** X - coordinate of point. */
		const i32 & X() const { return *get<i32, 0>(); }
		/** Y - coordinate of point. */
		const i32 & Y() const { return *get<i32, 4>(); }
		/** Z - coordinate of point. */
		const i32 & Z() const { return *get<i32, 8>(); }

	public: // IIO
		virtual void Load(istream_t & stream) override;
		virtual void Save(ostream_t & stream) const override;

	public: // IPointInfo
		virtual const double3 GetPosition() const override { return double3((f64)X(), (f64)Y(), (f64)Z()); }
		virtual const V3<u16> GetColor() const override { return V3<u16>(0xFFFFui16, 0xFFFFui16, 0xFFFFui16); }
		virtual const Classification GetClassification() const override { return Classification::Unclassified; }
		virtual void SetColor(const V3<u16> &) override { throw; }
		virtual void SetPosition(const double3 &) override { throw; }
		virtual void SetClassification(Classification) override { throw; }
		virtual Formats GetFormat() const override { return Formats::Format1; }
	};


	template<>
	struct Point<Formats::Format2>
		: public IO<26>
		, public IPoint
	{
	public: // getters
		/** X - coordinate of point. */
		const i32 & X() const { return *get<i32, 0>(); }
		/** Y - coordinate of point. */
		const i32 & Y() const { return *get<i32, 4>(); }
		/** Z - coordinate of point. */
		const i32 & Z() const { return *get<i32, 8>(); }
		/** X - coordinate of point. */
		const u16 & Intensity() const { return *get<u16, 12>(); }
		/** @see http://www.asprs.org/a/society/committees/standards/asprs_las_format_v12.pdf */
		u32 ReturnNumber() const { return flag<3>(*get<u8, 14>(), 0); }
		/** @see http://www.asprs.org/a/society/committees/standards/asprs_las_format_v12.pdf */
		u32 NumberOfReturns()  const { return flag<3>(*get<u8, 14>(), 3); }
		/** @see http://www.asprs.org/a/society/committees/standards/asprs_las_format_v12.pdf */
		u32 ScanDirectionFlag()  const { return (flag<3>(*get<u8, 14>(), 6)); }
		/** @see http://www.asprs.org/a/society/committees/standards/asprs_las_format_v12.pdf */
		u32 EdgeOfFlightLine()  const { return flag<3>(*get<u8, 14>(), 7); }
		/** @see http://www.asprs.org/a/society/committees/standards/asprs_las_format_v12.pdf */
		const u8 & ScanAngleRank() const { return *get<u8, 16>(); }
		/** @see http://www.asprs.org/a/society/committees/standards/asprs_las_format_v12.pdf */
		const u8 & UserData() const { return *get<u8, 17>(); }
		/** @see http://www.asprs.org/a/society/committees/standards/asprs_las_format_v12.pdf */
		const u16 & PointSourceID() const { return *get<u16, 18>(); }
		/** Red color channel. */
		const u16 & R() const { return *get<u16, 20>(); }
		/** Green color channel. */
		const u16 & G() const { return *get<u16, 22>(); }
		/** Blue color channel. */
		const u16 & B() const { return *get<u16, 24>(); }

	public: // IIO
		virtual void Load(istream_t & stream) override;
		virtual void Save(ostream_t & stream) const override;

	public: // IPointInfo
		virtual const double3 GetPosition() const override { return double3((f64)X(), (f64)Y(), (f64)Z()); }
		virtual const V3<u16> GetColor() const override { return *get<const V3<u16>, 20>(); }
		virtual const Classification GetClassification() const override { return *get<Classification, 15>(); }
		virtual void SetColor(const V3<u16> & color) override { *get<V3<u16>, 20>() = color; }
		virtual void SetPosition(const double3 & point) override { *get<double3, 0>() = point; }
		virtual void SetClassification(Classification classification) override { *get<Classification, 15>() = classification; }
		virtual Formats GetFormat() const override { return Formats::Format2; }
	};


	template<>
	class Point<Formats::Format3>
		: public IO<26 + 8>
		, public IPoint
	{
	public:
		/** X - coordinate of point. */
		const i32 & X() const { return *get<i32, 0>(); }
		/** Y - coordinate of point. */
		const i32 & Y() const { return *get<i32, 4>(); }
		/** Z - coordinate of point. */
		const i32 & Z() const { return *get<i32, 8>(); }
		/** @see http://www.asprs.org/a/society/committees/standards/asprs_las_format_v12.pdf */
		const u16 & Intensity() const { return *get<u16, 12>(); }
		/** @see http://www.asprs.org/a/society/committees/standards/asprs_las_format_v12.pdf */
		u32 ReturnNumber() const { return flag<3>(*get<u32, 14>(), 0); }
		/** @see http://www.asprs.org/a/society/committees/standards/asprs_las_format_v12.pdf */
		u32 NumberOfReturns() const { return flag<3>(*get<u32, 14>(), 3); }
		/** @see http://www.asprs.org/a/society/committees/standards/asprs_las_format_v12.pdf */
		u32 ScanDirectionFlag() const { return flag<3>(*get<u32, 14>(), 6); }
		/** @see http://www.asprs.org/a/society/committees/standards/asprs_las_format_v12.pdf */
		u32 EdgeOfFlightLine() const { return flag<3>(*get<u32, 14>(), 7); }
		/** @see http://www.asprs.org/a/society/committees/standards/asprs_las_format_v12.pdf */
		const u8 & ScanAngleRank() const { return *get<u8, 16>(); }
		/** @see http://www.asprs.org/a/society/committees/standards/asprs_las_format_v12.pdf */
		const u8 & UserData() const { return *get<u8, 17>(); }
		/** @see http://www.asprs.org/a/society/committees/standards/asprs_las_format_v12.pdf */
		const u16 & PointSourceID() const { return *get<u16, 18>(); }
		/** @see http://www.asprs.org/a/society/committees/standards/asprs_las_format_v12.pdf */
		const f64 & GPSTime() const { return *get<f64, 20>(); }
		/** Red color channel. */
		const u16 & R() const { return *get<u16, 28>(); }
		/** Green color channel. */
		const u16 & G() const { return *get<u16, 30>(); }
		/** Blue color channel. */
		const u16 & B() const { return *get<u16, 32>(); }

	public: // IIO
		virtual void Load(istream_t & stream) override;
		virtual void Save(ostream_t & stream) const override;

	public: // IPointInfo
		virtual const double3 GetPosition() const override { return double3((f64)X(), (f64)Y(), (f64)Z()); }
		virtual const V3<u16> GetColor() const override { return *get<V3<u16>, 28>(); }
		virtual const Classification GetClassification() const override { return *get<Classification, 15>(); }
		virtual void SetColor(const V3<u16> & color) override { *get<V3<u16>, 28>() = color; }
		virtual void SetPosition(const double3 & point) override { *get<double3, 0>() = point; }
		virtual void SetClassification(Classification classification) override { *get<Classification, 15>() = classification; }
		virtual Formats GetFormat() const override { return Formats::Format3; }
	};


	/** Special kind of format for direct synchronization. Uses 32-bit floats for coordinates and 8-bit color channels. */
	template<>
	struct Point<Formats::FormatDD> : public IO<4 + 4 + 4 + 4>, public IPoint {
		/** Conversion to 8-bit color. */
		static V3<u16> Color8To16(V3<u8> c) { V3<u16> r;  r.x = c.x << 8, r.y = c.y << 8, r.z = c.z << 8; return r; }
		/** Conversion to 16-bit color. */
		static V3<u8> Color16To8(V3<u16> c) { V3<u8> r;  r.x = c.x >> 8, r.y = c.y >> 8, r.z = c.z >> 8; return r; }

	public:
		/** X - coordinate of point. */
		const f32 & X() const { return *get<f32, 0>(); }
		/** Y - coordinate of point. */
		const f32 & Y() const { return *get<f32, 4>(); }
		/** Z - coordinate of point. */
		const f32 & Z() const { return *get<f32, 8>(); }
		/** X - coordinate of point. */
		f32 & X() { return *get<f32, 0>(); }
		/** Y - coordinate of point. */
		f32 & Y() { return *get<f32, 4>(); }
		/** Z - coordinate of point. */
		f32 & Z() { return *get<f32, 8>(); }
		/** Colors. */
		const V3<u8> & Color() const { return *get<V3<u8>, 12>(); }
		/** Colors. */
		V3<u8> & Color() { return *get<V3<u8>, 12>(); }
		/** Color and classification in alpha channel. */
		const u32 & ColorAndClassification() const { return *get<u32, 12>(); }
		/** Color and classification in alpha channel. */
		u32 & ColorAndClassification() { return *get<u32, 12>(); }

	public: // IIO
		virtual void Load(istream_t & stream) override;
		virtual void Save(ostream_t & stream) const override;

	public: // Inherited via IPointInfoforma
		virtual const V3<u16> GetColor() const override { return Color8To16(Color()); };
		virtual const double3 GetPosition() const override { return *get<double3, 0>(); }
		virtual const Classification GetClassification() const override { return *get<Classification, 15>(); }
		virtual void SetColor(const V3<u16> & color) override { Color() = Color16To8(color); }
		virtual void SetPosition(const double3 & position) override { *get<double3, 0>() = position; }
		virtual void SetClassification(Classification cls) override { *get<Classification, 15>() = cls; }
		virtual Formats GetFormat() const override { return Formats::FormatDD; }
	};


	struct File {
	protected: // properties
		/** Header of file. */
		mutable Header _header;
		/** Variable length records. */
		Array<VariableLengthRecord> _records;
		/** Point reader. */
		Unique<IPoint> _iPoint;

	protected: // methods
		/** Checks if format loaded in header is valid and inits _format property. */
		bool checkFormat();

	public: // methods
		/** Inits variable length records. */
		void InitRecords() { _records.Reallocate(_header.NumberOfVariableLengthRecords()); }
		/** Getter of vlr. */
		const Array<VariableLengthRecord> & GetVariableLengthRecords() const { return _records; }
		/** Getter of vlr. */
		Array<VariableLengthRecord> & GetVariableLengthRecords() { return _records; }
		/** Getter of point reader. */
		const IPoint * GetPoint() const { return _iPoint.Ptr(); }
		/** Getter of point reader. */
		IPoint * GetPoint() { return _iPoint.Ptr(); }
		/** Getter of file header. */
		const Header & GetHeader() const { return _header; }
		/** Getter of file header. */
		Header & GetHeader() { return _header; }
		/** Returns point count in file. */
		u32 GetTotalCount() const { return _header.NumberOfPointRecords(); }
	};


	struct FileReader
		: public File
	{
		/** Number of already read points. */
		u32 _counter;
		/** Reader. */
		istream_t _istream;
		/** UTM info. */
		struct { unsigned Mercator; bool South; } _UTM;

	public:
		/** Reads a point. Returns false if end of file has been reached. */
		bool ReadPoint();
		/** Returns point coordinations in native format. */
		double3 GetPosition() const { return _iPoint->GetPosition(); }
		/** Returns color of actual point. */
		V3<u16> GetColor() const { return _iPoint->GetColor(); }
		/** Returns classification of actual point. */
		Classification GetClassification() const { return _iPoint->GetClassification(); }
		/** Returns GPS coordinations of actual point. */
		double3 GetGPS() const;
		/** Number of already read points. */
		u32 GetReadCount() const { return _counter; }
		/** Number of vlr. */
		u32 GetVariableLengthRecordsCount() { return _header.NumberOfVariableLengthRecords(); }
		/** BBox in native representation. */
		AABox3D GetBBox() const;
		/** BBox in gps representation. */
		AABox3D GetGPSBBox() const;
		/** BBox in meter representation. */
		double3 GetDimensions() const;
		/** Opens file with specified path. Returns true if successful. */
		bool Open(const wchar_t * path);
		/** Simplified method for loading DD::Simulation::PointCloud structure. */
		Simulation::PointCloud LoadPointCloud(const float3 & cellDefinition);
		/** Simplified method for loading DD::Simulation::PointCloud structure. */
		Simulation::PointCloud LoadPointCloud() { return LoadPointCloud(GetDimensions()); }

		/** Zero constructor. */
		FileReader();
		/** constructor. */
		FileReader(const wchar_t * path) : FileReader() { Open(path); }
		/** Destructor. */
		~FileReader();
	};


	struct FileWriter
		: public File
	{
		/** Writer. */
		ostream_t _stream;
		/** Has been already initialized. */
		bool _init;
		/** Number of written points. */
		u32 _counter;

	public:
		/** Sets format of las file. */
		void SetFormat(Formats format) { _header.PointDataFormatID() = format; }
		/** Sets point in DD format. */
		void SetPoint(f32 x, f32 y, f32 z, u32 color);
		/** Sets point. */
		void SetPoint(double3 position, V3<u16> color, Classification classification = Classification::NeverClassified);
		/** Inits writer. File should be opened before. */
		bool Init();
		/** Opens path. */
		bool Open(const wchar_t * file);
		/** Finish writing. */
		void Close();
		/** Write point cloud. */
		void WritePointCloud(const Simulation::PointCloud & pointCloud, Formats format = Formats::FormatDD, IProgress * progress = 0);
		/** Write a point into file. */
		void WritePoint() { if (_init) _iPoint->Save(_stream), ++_counter; }
		/** Number of written points. */
		u32 GetWriteCount() const { return _counter; }
		/** Constructor. */
		FileWriter();
		/** Destructor. */
		~FileWriter();
	};


	/**
	 * Load point cloud from file.
	 * @param path - path to las file.
	 * @param cell - dimensions of cell of point cloud, if you want to use implicit
	 *               cloud size, let dimension be 0.f, e.g. float3(5.f, 0.f, 5.f)
	 *               means, that for y-dimension will be used implicit cloud size.
	 * @return loaded point cloud if loading was successful otherwise null point cloud.
	 */
	DD::Simulation::PointCloud LoadPointCloud(const String & path, const float3 & cell = 0.f, IProgress * progress = 0);

	/**
	 * Save point cloud into the given path.
	 * @param path   - path to file.
	 * @param cloud  - point cloud which should be saved.
	 * @param format - format in which should be file formatted.
	 * @return true if file was successfully saved.
	 */
	bool SavePointCloud(Text::StringView16 path, const DD::Simulation::PointCloud & cloud, Formats format = Formats::FormatDD, IProgress * progress = 0);
}
