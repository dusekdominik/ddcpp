#pragma once
namespace DD {
	/**
	 * Traits for formatting type in String::Format method.
	 *
	 * Example:
	 *   // define an arbitrary type
	 *   struct HelloWorldStruct {};
	 *   // define formatter
	 *   template<>
	 *   struct StringFormatter<HelloWorldStruct> {
	 *     static void Format(const HelloWorldStruct & source, const String & format, String & target) { target.Append("Hello world"); }
	 *   };
	 *   // use String::Format method
	 *   String myString = String::Format("{0}", HelloWorldStruct());
	 */
	template<typename SOURCE>
	struct StringFormatter;

	/**
	 * 16bit universal string.
	 * Allocated on heap.
	 * Serves as string buffer or string. Automatically convertible to std formats.
	 * For special formats use StringFormatter explicit overload.
	 *
	 * Examples:
	 *   String s0 = String();
	 *   String s1 = "Hello world";
	 *   String s2 = L"Hello world";
	 *   String s3 = String::From("The pi is approximately ", 3.14159265);
	 *   String s4 = String::Format("Timetable\n{0:10}{1:10}\n{2:10}{3:10}", (int)time0, "lunch", (int)time1, "dinner")
	 */
	class String;

	/**
	 * String with local allocation.
	 * Serves as allocation free variant of String.
	 * Functionality is same as for String.
	 * If string is bigger than local allocation, it starts allocating on heap.
	 * Examples:
	 *   StringLocal<32>::From("String ", L"has local allocation of ", 32, "chars");
	 *   StringLocal<32>::Format("{0} chars are {1} bytes", 32, 32 * sizeof(wchar_t));
	 */
	template<size_t SIZE>
	class StringLocal;
}

#include <DD/Array.h>
#include <DD/Lambda.h>
#include <DD/Text/Formatter.h>
#include <DD/Text/StringView.h>
#include <DD/Types.h>

#define DD_STRING_ASSERT(c) DD_LITE_DEBUG_ASSERT(c, "DD::String assertion failed.")

namespace DD {
	class String
		: public Text::StringViewT<wchar_t, size_t>
	{
		using CharType = wchar_t;
		using SizeType = size_t;
		using BufferType = Array<CharType>;

		using Base = Text::StringViewT<wchar_t, size_t>;


	public: // typedefs
		const bool operator<(const String & str) const { return Base::operator<(str); }
		using Base::operator<;
		using Base::operator!=;
		using Base::operator==;
		/** Interface for formatting type into string. */
		struct IFormatter;
		/** Implementation of IFormatter interface. */
		template<typename T>
		struct Formatter;

	protected: // statics
		/** Get formatter of a type. */
		template<typename T>
		static const IFormatter * getFormatter(const T &) { return &static_cast<IFormatter&>(::DD::String::Formatter<T>::Instance()); }

	public: // static methods
		/** Generator for composing string in one line. */
		template<typename...ARGS>
		static String From(const ARGS &...args) { return String().AppendBatch(args...); }
		/** String formatter. Format("{argIndex : alignment, format}", argument). */
		template<typename...ARGS>
		static String Format(const String & format, const ARGS &...args) { return String().AppendFormat(format, args...); }
		/** String formatter. Format("{argIndex : alignment, format}", argument). */
		template<typename...ARGS>
		static String Format(const char * format, const ARGS &...args) { return String().AppendFormat(format, args...); }
		/** String formatter. Format("{argIndex : alignment, format}", argument). */
		template<typename...ARGS>
		static String Format(const wchar_t * format, const ARGS &...args) { return String().AppendFormat(format, args...); }

	protected: // protected properties
		/** Storage of data. */
		BufferType _data;

	protected: // private methods
		/** Helper method returning how many chars are free at the string beginning. */
		size_t freeLeft() const;
		/** Helper method returning how many chars are free at the string ending. */
		size_t freeRight() const;
		/** Helper method which inserts content from source of given length into given position. Content is not removed, but shifted after the given position. */
		void insertAndShiftRight(size_t position, wchar_t * what, size_t size);
		/** Helper method which inserts content from source of given length into given position. Content is not removed, but shifted before the given position. */
		void insertAndShiftLeft(size_t position, wchar_t * what, size_t size);
		/** Helper method which inserts content from source of given length into given position by composing new storage. */
		void insertAndNoShift(size_t position, wchar_t * what, size_t size);
		/** Helper method which inserts content of given length into given position. Method automatically shifts current content or reallocate string. */
		void insert(size_t position, wchar_t * what, size_t size);
		/** Internal formatting representation. */
		String & appendFormat(const char * formatter, const Array<const void *> & refs, const Array<const IFormatter *> & formatters);
		/** Internal formatting representation. */
		String & appendFormat(const wchar_t * formatter, const Array<const void *> & refs, const Array<const IFormatter *> & formatters);
		/** Internal formatting representation. */
		String & appendFormat(const Text::StringView16 & formatter, const Array<const void *> & refs, const Array<const IFormatter *> & formatters);
		/** Append a char, don't bother whether enough space is available. */
		template<typename CHAR_TYPE>
		void appendCharUnsafe(CHAR_TYPE c) { DD_STRING_ASSERT(_begin + _length < _data.end());  _begin[_length++] = (wchar_t)c; }
		/** Append string view, don't bother whether enough space is available. */
		template<typename C, typename S>
		String & appendViewUnsafe(const Text::StringViewT<C, S> & view) { for (C c : view) appendCharUnsafe(c); return *this; }

	public:
		/** Add null terminator behind string view, can be writing outside local buffer. */
		String & TerminateUnsafe() { *end() = (wchar_t(0)); return *this; }
		/** Add null terminator at the end of string.  */
		String & Terminate() { return Reserve(1).TerminateUnsafe(); }

	public: // accession
		/** Iterator. */
		wchar_t * begin();
		/** Iterator. */
		wchar_t * end();
		/** Indexer. */
		wchar_t & operator[](size_t i);
		/** Const getter of storage type. */
		const BufferType & Buffer() const { return _data; }
		/** Const iterator. */
		const wchar_t * begin() const;
		/** Const iterator. */
		const wchar_t * end() const;
		/** Const indexer. */
		const wchar_t & operator[](size_t i) const;
		/** Get string view explicitly. */
		Text::StringView16 View() const { return { _begin, _length }; }
		/** Implicit conversion operator. */
		operator Text::StringView16() const { return View(); }

	public: // prepending
		template<typename CHAR_TYPE>
		void PrependCharUnsafe(CHAR_TYPE c) { *(--StringViewT::_begin) = c; ++StringViewT::_length; }
		template<typename C, typename S>
		String & PrependViewUnsafe(const StringViewT<C, S> & view) {
			size_t length = StringViewT::Length() + view.Length();
			StringViewT::_begin -= view.Length();
			StringViewT::_length = 0;
			appendViewUnsafe(view);
			StringViewT::_length = length;

			return *this;
		}
		template<typename C, typename S>
		void Prepend(const StringViewT<C, S> & view) { return Reserve(view.Length(), 0).PrependViewUnsafe(view); }
		void Prepend(wchar_t c) { return Reserve(1, 0).PrependCharUnsafe(c); }

	public: // appending
		/** */
		template<typename T, Text::Formats FORMAT, typename FORMATTER>
		String & AppendUnsafe(const Text::FormatType<T, FORMAT> & value, FORMATTER & formatter) {
			// append unsafe single char
			auto writer = [this](wchar_t c) { StringViewT::_begin[StringViewT::_length++] = c; };
			// call formatter to fill buffer up
			formatter(value, writer);
			// return reference
			return *this;
		}
		/** Append sequence. */
		template<typename SEQUENCER>
		String & AppendSequenceUnsafe(SEQUENCER sequence) { if (sequence.Reset()) do AppendUnsafe(sequence.Get()); while (sequence.Get()); return *this; }
		/** Append any string view. */
		template<typename C, typename S = size_t>
		String & Append(const Text::StringViewT<C, S> & view) { return Reserve(view.Length() + 1).appendViewUnsafe(view).TerminateUnsafe(); }
		/** Append value with a custom formatter. */
		template<typename T, Text::Formats FORMAT, typename FORMATTER>
		String & Append(const Text::FormatType<T, FORMAT> & value, FORMATTER & formatter) { return Append(StringLocal<128>().AppendUnsafe(value, formatter)); }
		/** */
		template<typename T, Text::Formats FORMAT>
		String & Append(const Text::FormatType<T, FORMAT> & value) { Text::Formatter<Text::FormatType<T, FORMAT>> formatter; return Append(value, formatter); }
		/** Append/merge strings row by row. */
		String & AppendCol(const String & str);
		/** Append given letter given-number-times. */
		String & AppendFill(wchar_t letter, size_t size) { Reserve(size + 1); while (size--) appendCharUnsafe(letter); return TerminateUnsafe(); }
		/** Append space given-number-times. */
		String & AppendFill(size_t size) { return AppendFill(L' ', size); }
		/** Append float number. */
		String & Append(f32 f);
		/** Append float number. */
		String & Append(f64 f);
		/** Append int number. */
		String & Append(i32 i) { return Append(Text::Dec(i)); }
		/** Append int number. */
		String & Append(u32 u) { return Append(Text::Dec(u)); }
		/** Append int number. */
		String & Append(i64 i) { return Append(Text::Dec(i)); }
		/** Append int number. */
		String & Append(u64 u) { return Append(Text::Dec(u)); }
		/** Append char. */
		String & Append(char letter) { return Reserve(1).AppendUnsafe(letter); }
		/** Append char. */
		String & Append(wchar_t letter) { return Reserve(1).AppendUnsafe(letter); }
		/** Append string. */
		String & Append(const char * ptr) { return Append(Text::StringView8(ptr)); }
		/** Append string. */
		String & Append(const wchar_t * ptr) { return Append(Text::StringView16(ptr)); }

		/** Append float number. */
		String & Append(f32 f, const Text::StringView16 & format);
		/** Append float number. */
		String & Append(f64 f, const Text::StringView16 & format);
		/** Append int number. */
		String & Append(i32 i, const Text::StringView16 & format);
		/** Append int number. */
		String & Append(u32 u, const Text::StringView16 & format);
		/** Append int number. */
		String & Append(i64 i, const Text::StringView16 & format);
		/** Append int number. Format could be a number(1-9) of leading zeros. */
		String & Append(u64 u, const Text::StringView16 & format);
		/** Append char. */
		String & Append(char c, const Text::StringView16 & format);
		/** Append char. */
		String & Append(wchar_t c, const Text::StringView16 & format);
		/** Append string. */
		String & Append(const char * str, const Text::StringView16 & format);
		/** Append string. */
		String & Append(const Text::StringView16 & str, const Text::StringView16 & format);
		/** Append with exponential expansion. */
		String & AppendEx(wchar_t letter) { return ReserveEx(2).AppendUnsafe(letter); }
		/** Append with exponential expansion. */
		template<typename C, typename S = size_t>
		String & AppendEx(const Text::StringViewT<C, S> & text) { return ReserveEx(text.Length() + 1).appendViewUnsafe(text).TerminateUnsafe(); }
		/** Append multiple appendants. */
		template<typename ARG, typename...ARGS>
		String & AppendBatch(const ARG & arg, const ARGS &...args) { return Append(arg).AppendBatch(args...); }
		/** Helper method for appending zero appendant. */
		String & AppendBatch() { return *this; }
		/** String formatter. Format("{argIndex : alignment, format}", argument). */
		template<typename...ARGS>
		String & AppendFormat(const Text::StringView16 & str, const ARGS &...args) { return appendFormat(str, Arrayize<const void *>(Unscent(args)...), Arrayize<const IFormatter *>(getFormatter(args)...)); }
		/** String formatter. Format("{argIndex : alignment, format}", argument). */
		template<typename...ARGS>
		String & AppendFormat(const char * str, const ARGS &...args) { return appendFormat(str, Arrayize<const void *>(Unscent(args)...), Arrayize<const IFormatter *>(getFormatter(args)...)); }
		/** String formatter. Format("{argIndex : alignment, format}", argument). */
		template<typename...ARGS>
		String & AppendFormat(const wchar_t * str, const ARGS &...args) { return appendFormat(str, Arrayize<const void *>(Unscent(args)...), Arrayize<const IFormatter *>(getFormatter(args)...)); }
		/** Append timestamp in format (YYYY-MM-DD hh-mm-ss-iii). */
		String & AppendTimestamp();
		/** Append single char without checking capacity. */
		String & AppendUnsafe(wchar_t c) { appendCharUnsafe(c); return *this; }

	public: // insertion
		/** Insert given character into given position. */
		String & InsertChar(size_t i, wchar_t c);
		/** Remove char on a position and insert char on another position. */
		String & RemoveAndInsertChar(size_t remove, size_t insert, wchar_t c);

	public: // buffer manipulation
		/** Align storage for contained data. */
		String & Align();
		/** Load data as null terminated string from storage. */
		void LoadFromBuffer();
		/** Load data as null terminated string from storage. */
		void LoadFromBuffer(size_t i) { StringViewT::_begin = _data.begin(); StringViewT::_length = i; }
		/** Set string end to the end of buffer. */
		void LoadFullBuffer();
		/** Load data to buffer. */
		template<typename C, typename S>
		String & Load(const Text::StringViewT<C, S> & view) { return Clear().Append(view); }
		/** Reserve number of characters in storage. */
		String & Reserve(size_t size);
		/** Reserve number of characters on beginning and on ending of string. */
		String & Reserve(size_t prepend, size_t append);
		/** Reserve number of characters in storage, expand exponentially. */
		String & ReserveEx(size_t size) { if (freeRight() < size) Reserve(DD::Max(_data.Length() * 2, size)); return *this; }
		/**
		 * Wrap an array - array will not be deallocated by the string.
		 * If more space necessary, string allocates its own buffer.
		 * @param load - scan buffer for null terminated string.
		 */
		void Wrap(ArrayView1D<wchar_t> array, bool load = true);
		constexpr bool ValidityCheck() const { return _data.begin() <= StringViewT::begin() && StringViewT::end() <= _data.end(); }

	public: // removing
		/** Removes content of string. */
		String & Clear() { DD_STRING_ASSERT(ValidityCheck()); StringViewT::_length = 0; return *this; }
		/** Removes character on given position. */
		String & RemoveChar(size_t i);

	public: // comparison
		/** Lexicographical comparison. */
		//bool operator<(const String & str) const;
		/** Check if string contains given letter. */
		bool Contains(wchar_t letter) const;
		/**
		 * Match() - case sensitinve
		 * MatchI() - case insensitinve
		 *
		 * Check if string contains given letter (possible to use wildcard - *).
		 * @warning allocation of match is living as long as the source is living.
		 * @warning
		 * @param pattern - patter which should be matched.
		 * @param begin	  - begin index of the match.
		 * @param end		  - end index of the match.
		 * @return empty string if there is no match, otherwise the matched substring.
		 *
		 * @example common usage
		 *   String source(L"Something");
		 *   const String & m0 = source.Match("met*n"); // use pointers targeting to original string (no allocation)
		 *   String m1 = source.Match("met*n");         // allocates new string (allocation)
		 *   StringLocal<8> m2 = source.Match("met*n");	// copy match to the locally allocated string (no allocation)
		 */
		Text::StringView16 Match(const wchar_t * pattern, size_t & begin, size_t & end) const;
		Text::StringView16 Match(const wchar_t * pattern) const;
		Text::StringView16 MatchI(const wchar_t * pattern, size_t & begin, size_t & end) const;
		Text::StringView16 MatchI(const wchar_t * pattern) const;
		/** Count occurrences of given char. */
		size_t CountChars(wchar_t) const;
		/** Count occurrences of given char. */
		size_t CountChars(char c) const { return CountChars((wchar_t)c); }
		/** Count lines. */
		size_t CountLines() const { return CountChars(L'\n'); }

	public: // transcription
		/** Aho-Corrasic like replacement. */
		String & Replace(const ArrayView1D<const Text::StringView16> & patterns, const ArrayView1D<const Text::StringView16> & replacements);
		/** Simple direct replacement. */
		String & Replace(const Text::StringView16 & pattern, const Text::StringView16 & replacement);
		/** Converts all the letters into lower-case format. */
		String & Transcript(char(*transcriptor)(char));
		/** Converts all the letters into lower-case format. */
		String & Transcript(wchar_t(*transcriptor)(wchar_t));
		/** Converts all the letters into lower-case format. */
		String & Transcript(int(*transcriptor)(int));
		/** Converts all the letters into lower-case format. */
		String & ToLower();
		/** Converts all the letters into upper-case format. */
		String & ToUpper();

	public: // converters
		/** Converts string into 64bit integer. */
		i64 ToInteger() const;
		/** Converts string into double float. */
		f64 ToDouble() const;
		/** Converts string into wchar*. */
		operator const wchar_t*() const { return begin(); }
		using StringViewT::TryCast;
 		bool TryCast(String & target) const { target = *this; return true; }
		template<size_t LENGTH>
 		bool TryCast(StringLocal<LENGTH> & target) const { target = *this; return true; }

	public: // comparison
		/** Comparison forwarder.*/
		bool operator==(const String & rhs) const { return Base::operator==(rhs.View()); }

	public: // assignment
		/** Method swapping content of two strings. */
		String & Swap(String &);
		/** Assignment operator for null-terminated strings. */
		template<typename CHAR_TYPE>
		String & operator=(const CHAR_TYPE * ptr) { return Load<const CHAR_TYPE, size_t>(ptr); }
		/** RValue assignment operator. */
		String & operator=(String && str) { _data.HasAllocator() ? Swap(str) : Load(str); DD_STRING_ASSERT(ValidityCheck()); return *this; }
		/** Assignment operator. */
		String & operator=(const String & str) { if (&str != this) Load(str); DD_STRING_ASSERT(ValidityCheck()); return *this; }
		/** Load data from given stringview. */
		template<typename C, typename S>
		String & operator=(const Text::StringViewT<C, S> & view) { Load(view); DD_STRING_ASSERT(ValidityCheck()); return *this; }

	public: // constructors
		/** Default constructor. */
		constexpr String() : StringViewT(nullptr, size_t(0)), _data() { DD_STRING_ASSERT(ValidityCheck()); }
		/** Move constructor. */
		String(String && rvalue) : String() { _data.HasAllocator() ? Swap(rvalue) : Load(rvalue); DD_STRING_ASSERT(ValidityCheck()); }
		/** Constructor reserving given number of letters. */
		explicit String(size_t size) : String() { Reserve(size); DD_STRING_ASSERT(ValidityCheck()); }
		/** From any view. */
		template<typename C, typename S>
		String(const Text::StringViewT<C, S> & view) : String() { Load(view); DD_STRING_ASSERT(ValidityCheck());}
		/** From null-terminated strings. */
		template<typename CHAR_TYPE>
		String(CHAR_TYPE *& ptr) : String(Text::StringView(ptr)) { DD_STRING_ASSERT(ValidityCheck()); }
		/** Copy constructor. */
		String(const String & string) : String(string.View()) { DD_STRING_ASSERT(ValidityCheck()); }
	};


	template<size_t ALLOCATION>
	class StringLocal
		: public String
	{
	public: // statics
		using String::operator=;
		/** Generator. */
		template<typename...ARGS>
		static StringLocal From(const ARGS &...args) { return StringLocal().AppendBatch(args...); }
		/** String formatter. Format("{argIndex : alignment, format}", argument). */
		template<typename...ARGS>
		static StringLocal Format(const String & format, const ARGS &...args) { return StringLocal().AppendFormat(format, args...); }
		/** String formatter. Format("{argIndex : alignment, format}", argument). */
		template<typename...ARGS>
		static StringLocal Format(const char * format, const ARGS &...args) { return StringLocal().AppendFormat(format, args...); }
		/** String formatter. Format("{argIndex : alignment, format}", argument). */
		template<typename...ARGS>
		static StringLocal Format(const wchar_t * format, const ARGS &...args) { return StringLocal().AppendFormat(format, args...); }

	protected: // properties
		/** Local allocation. */
		wchar_t _local[ALLOCATION];

	public: // assignment
		/** Copy assignment. */
		StringLocal & operator=(const String & string) { String::operator=(string); DD_STRING_ASSERT(ValidityCheck()); return *this; }
		/** Copy assignment. */
		StringLocal & operator=(const StringLocal & string) { operator=(static_cast<const String &>(string)); DD_STRING_ASSERT(ValidityCheck()); return *this; }
		/** Move assignment. */
		StringLocal & operator=(StringLocal && string) { String::operator=(static_cast<String &&>(string)); DD_STRING_ASSERT(ValidityCheck()); return *this; }
		/** Copy assignment. */
		template<typename C, typename S>
		StringLocal & operator=(const Text::StringViewT<C, S> & view) { String::operator=(view); DD_STRING_ASSERT(ValidityCheck()); return *this; }
		/** */
		operator Text::StringView16() const { return View(); }
		operator Text::StringView16() { return View(); }

	public: // constructors
		/** Zero constructor. */
		StringLocal() : String() {
			_data.Swap({ ArrayView(_local), nullptr });
			StringViewT::_begin = _local;
			DD_STRING_ASSERT(ValidityCheck());
		}
		/** Constructor loading data from native string. */
		StringLocal(const char * source) : StringLocal() { Load(Text::StringView8(source)); DD_STRING_ASSERT(ValidityCheck()); }
		/** Constructor loading data from native string. */
		StringLocal(const wchar_t * source) : StringLocal() { Load(Text::StringView16(source)); DD_STRING_ASSERT(ValidityCheck()); }
		/** Constructor loading data from another string. */
		template<typename C, typename S>
		StringLocal(const Text::StringViewT<C, S> & str) : StringLocal() { Load(str); DD_STRING_ASSERT(ValidityCheck()); }
		StringLocal(const Text::StringView16 & str) : StringLocal() { Load(str); DD_STRING_ASSERT(ValidityCheck()); }
		/** Constructor loading data from another string. */
		StringLocal(const StringLocal & str) : StringLocal() { Load(str); DD_STRING_ASSERT(ValidityCheck()); }
		/** Move constructor. */
		StringLocal(StringLocal && rvalue) : StringLocal() { rvalue._data.HasAllocator() ? Swap(rvalue) : Load(rvalue); }
		/** Destructor. */
		~StringLocal() { StringViewT::_begin = (wchar_t*)0xDEAD; }
	};


	struct String::IFormatter {
		/** Virtual method for processing String::Format. */
		virtual void Format(String & output, const void * input, const Text::StringView16 & format) const = 0;
	};


	template<typename T>
	struct String::Formatter
		: public String::IFormatter
	{
		/** Singleton instance. */
		static Formatter<T> & Instance() { static Formatter<T> instance; return instance; }

	public: // IFormatter
		virtual void Format(String & output, const void * input, const Text::StringView16 & format) const override { StringFormatter<T>::Format(*((const T *)input), format, output); }
	};


	template<typename SOURCE>
	struct StringFormatter {
		/** Append formatted string to the given target. */
		static void Format(const SOURCE & source, const Text::StringView16 & format, String & target) { target.Append(source, format); }
	};

	template<size_t A>
	struct StringFormatter<StringLocal<A>> {
		/** Append formatted string to the given target. */
		static void Format(const StringLocal<A> & source, const Text::StringView16 & format, String & target) { target.Append(source.View(), format); }
	};

	template<>
	struct StringFormatter<String> {
		/** Append formatted string to the given target. */
		static void Format(const String & source, const Text::StringView16 & format, String & target) { target.Append(source.View(), format); }
	};
}


template<>
struct DD::Text::Caster<DD::String> {
	/**
	 * Cast the given text as a value.
	 * @param text - StringViewT with a text.
	 * @param target - reference to the target value.
	 * @return true if cast has been successful, false otherwise.
	 */
	template<typename CHAR_TYPE, typename SIZE_TYPE>
	static constexpr bool TryCast(StringViewT<CHAR_TYPE, SIZE_TYPE> text, String & target) {
		target = text;
		return true;
	}
};


template<size_t ALLOC>
struct DD::Text::Caster<DD::StringLocal<ALLOC>> {
	/**
	 * Cast the given text as a value.
	 * @param text - StringViewT with a text.
	 * @param target - reference to the target value.
	 * @return true if cast has been successful, false otherwise.
	 */
	template<typename CHAR_TYPE, typename SIZE_TYPE>
	static constexpr bool TryCast(StringViewT<CHAR_TYPE, SIZE_TYPE> text, String & target) {
		target = text;
		return true;
	}
};

template<>
struct DD::Text::Formatter<DD::String> {
	template<typename WRITER>
	constexpr void operator()(const DD::String & value, WRITER & writer) { DD::Text::Format(DD::Text::Txt(value), writer); }
};
