#pragma once
namespace DD::Preprocessor {
	/**
	 * Token for simple slash, one line comment or multi-line comment.
	 */
	template<typename CHAR_TYPE>
	struct CommentToken {
		/** Letter type. */
		using CharType = CHAR_TYPE;
		/** Current state. */
		enum Modes {
			Undefined,
			Slash,        // "/" || "//" || "/*"
			SlashOrStar,  // "//" || "/*"
			LineEnd,      // "//...\n"
			Ignore,       // "\."
			Star,         // "*/"
			EndingSlash,  // "*/"
			Stop          // done
		} Mode = Slash;

	private:
		/** Process automaton changes wrt input and state.*/
		constexpr bool SearchFor(CharType c);
		/** If input is slash, accept; otherwise stop reading. */
		constexpr bool SearchForSlash(CharType c);
		/** If input is slash, expect one-liner comment; if star multiline comment; stop otherwise. */
		constexpr bool SearchForSlashOrStar(CharType c);
		/** If input is line break, stop reading. */
		constexpr bool SearchForLineEnd(CharType c);
		/** If input is star, switch to "EndingSlash" state. */
		constexpr bool SearchForStar(CharType c);
		/** If input is slash stop reading, otherwise continue in reading multiline comment. */
		constexpr bool SearchForEndingSlash(CharType c);
		/** Always return refuse char. */
		constexpr bool SearchForVoid(CharType c) { return false; }
		/** Accept anytrhing, then return to previous state. */
		constexpr bool SearchForIgnore(CharType c) { Mode = Modes::LineEnd; return true; }

	public:
		/** Is given input accepted. */
		constexpr bool operator()(CharType c) { return SearchFor(c); }
	};
}


namespace DD::Preprocessor {
	template<typename C>
	constexpr bool CommentToken<C>::SearchFor(CharType c) {
		switch (Mode) {
		case Modes::Slash:       return SearchForSlash(c);
		case Modes::SlashOrStar: return SearchForSlashOrStar(c);
		case Modes::LineEnd:     return SearchForLineEnd(c);
		case Modes::Ignore:      return SearchForIgnore(c);
		case Modes::Star:        return SearchForStar(c);
		case Modes::EndingSlash: return SearchForEndingSlash(c);
		case Modes::Stop:        return SearchForVoid(c);
		default: throw;
		}
	}


	template<typename C>
	constexpr bool CommentToken<C>::SearchForSlash(CharType c) {
		switch (c)
		{
		case '/':
			Mode = SlashOrStar;
			return true;

		default:
			Mode = Stop;
			return false;
		}
	}


	template<typename C>
	constexpr bool CommentToken<C>::SearchForSlashOrStar(CharType c) {
		switch (c)
		{
		case '*':
			Mode = Star;
			return true;

		case '/':
			Mode = LineEnd;
			return true;

		default:
			return false;
		}
	}


	template<typename C>
	constexpr bool CommentToken<C>::SearchForLineEnd(CharType c) {
		switch (c) {
		case '\n':
			Mode = Stop;
			return true;

		case '\\':
			Mode = Ignore;
			return true;

		default:
			return true;
		}
	}


	template<typename C>
	constexpr bool CommentToken<C>::SearchForStar(CharType c) {
		switch (c)
		{
		case '*':
			Mode = EndingSlash;
			return true;

		default:
			return true;
		}
	}


	template<typename C>
	constexpr bool CommentToken<C>::SearchForEndingSlash(CharType c) {
		switch (c)
		{
		case '*': // kepp the same mode
			return true;
		case '/':
			Mode = Modes::Stop;
			return true;
		default:
			Mode = Modes::Star;
			return true;
		}
	}
}
