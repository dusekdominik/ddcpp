#pragma once
namespace DD::Preprocessor {
	/** */
	struct MacroBankT;
}

#include <DD/Preprocessor/Define.h>
#include <DD/Preprocessor/ArgumentToken.h>
#include <DD/Collections/HashMapText.h>
#include <DD/Preprocessor/Token.h>
#include <DD/Text/StringWriter.h>

#define DD_TEXT_NAMESPACE(c, s)\
using CharType = c;\
using SizeType = s;\
using TextReader = Text::StringReader<const CharType, SizeType>;\
using TextWriter = Text::StringWriterT<CharType, SizeType>;\
using TextView = Text::StringViewT<const CharType, SizeType>;


namespace DD::Preprocessor {
	/** */
	struct MacroBankT {
		/** */
		DD_TEXT_NAMESPACE(char, size_t);
		/** */
		using Define = DefineT<CharType, SizeType>;
		/** */
		using Token = Token<CharType, SizeType>;
		/** */
		using Arg = ArgumentToken<CharType, SizeType>;


		static constexpr CharType TripleDot[] = { '.', '.', '.', '\0' };
		static constexpr CharType Hash[] = { '#', '\0' };
		static constexpr CharType Def[] = { 'd', 'e', 'f', 'i', 'n', 'e', '\0' };

		/** */
		Collections::HashMapText8<Define, 64> _defines;



		/** Read args for function macro, and apply them. */
		bool functionMacro(Define * define, TextReader & input, TextWriter & output) {
			input.SkipUntil<'('>().Skip();
			Collections::LazyList<TextView, 16> args;
			for (Arg arg; TextView argText = input.ReadWhile(arg); arg.Reset()) {
				if (arg.State == Arg::Undefined)
					break;

				args.Append(argText.Trim());
				input.Skip();

				if (arg.State == Arg::End)
					break;
			}

			TextReader reader = (define->Value.View());
			for (Token t; TextView tt = reader.ReadWhile(t); t = Token()) {
				if (t.Mode == Token::Word) {
					SizeType i = define->Arguments.GetIndex(tt);
					if (define->IsVariadic() && i == define->Arguments.Count() - 1 && i < args.Count()) {
						output.Write(args[i]);
						for (auto v : args.Since(i + 1))
							output.Write(", ", v);

						continue;
					}
					else if (i < args.Count()) {
						output.Write(args[i]);
						continue;
					}
				}

				output.Write(tt);
			}

			return true;
		}

		/**
		 * Replace macro by its value if exists.
		 * @param word - macro candidate name
		 * @param lineIn - reader to read follow up args, if macro is function macro
		 * @param output - where to write output.
		 * @return true if macro exists.
		 */
		bool Replace(TextView token, TextReader & input, TextWriter & output) {
			// DD_TODO_STAMP("Function Macros with zero args");
			if (Define * d = _defines.Get(token)) {
				if (d->Arguments.Count()) {
					return functionMacro(d, input, output);
				}
				else {
					output.Write(d->Value.View());
					return true;
				}
			}

			return false;
		}

		bool Load(TextView line) {
			TextReader lineReader(line);

			// WS + #
			if (!lineReader.SkipWhiteSpace().CheckAndSkip(Hash))
				return false;

			// WS + define
			if (!lineReader.SkipWhiteSpace().CheckAndSkip(Def))
				return false;

			// wd + macro name
			TextView name = lineReader.SkipWhiteSpace().ReadWordEx();
			if (!name)
				return false;

			// macro already exists
			if (_defines.Contains(name))
				return false;

			// register macro
			Define * newDefine = new (_defines.InsertKey(name)) Define();
			newDefine->Name.Write(name);

			// if is function macro, load args
			if (lineReader.SkipWhiteSpace().Cursor() == '(') {
				auto splitter = lineReader.ReadBrackets().SplitKeepEmpty(',');
				for (bool any = splitter.Reset(); any; any = splitter.Next()) {
					TextView arg = splitter.Get();
					if (arg.StartsWith("("))
						arg.RemoveFirst();

					if (arg.EndsWith(")"))
						arg.RemoveLast();

					if (arg.Trim() == TripleDot)
						arg = Define::VaArgs;

					newDefine->Arguments.Insert(arg);
				}
			}

			// read macro content token by token
			for (Token t; TextView token = lineReader.ReadWhile(t); t = Token()) {
				newDefine->Value.Write(token);
			}

			return true;
		}


		bool Defined(TextView name) { return _defines.Contains(name); }

	};
}