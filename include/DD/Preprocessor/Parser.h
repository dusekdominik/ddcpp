#pragma once
namespace DD::Preprocessor {
	/** */
	template<typename CHAR_TYPE, typename SIZE_TYPE>
	struct Parser;
}

#include <DD/Collections/HashMap.h>
#include <DD/Collections/LazyList.h>
#include <DD/Preprocessor/FileBank.h>
#include <DD/Preprocessor/ArgumentToken.h>
#include <DD/Preprocessor/MacroBank.h>
#include <DD/Preprocessor/Define.h>
#include <DD/Preprocessor/Evaluator.h>
#include <DD/Preprocessor/If.h>
#include <DD/Preprocessor/Token.h>
#include <DD/Text/StringReader.h>
#include <DD/Text/StringWriter.h>
#include <DD/Text/StringView.h>

namespace DD::Preprocessor {
	template<typename CHAR_TYPE, typename SIZE_TYPE>
	struct Parser {
		enum ParserError {
			E_OK,
			E_IF_MISSING_ARG,       // #ifdef
			E_IF_TRAILING_ARG,      // #ifdef A B
			E_IF_UNEXPECTED_BRANCH, // #ifdef #elif #endif
			E_IF_TRAILING_ENDIF,    // trailing #endif
			E_IF_UNEVALUATED,       // #if 5 5
			E_INCLUDE_MULTI,        // multiple files exists
			E_INCLUDE_NOTEXITST,    // no file
			E_INCLUDE_DEEP,         // deeper than a limit
			E_MACRO_NARGS,          // invalid number of args
			E_MACRO_UNPARSED,       // #define MACRO(a,) a
			E_PRAGMA_UNDEFINED,     // only pragma once is defined
		};
		/** */
		using SizeType = SIZE_TYPE;
		/** */
		using CharType = CHAR_TYPE;
		/** */
		using EValType = signed long long;
		/** */
		using StringReader = Text::StringReader<const CharType, SizeType>;
		/** */
		using StringWriter = Text::StringWriterT<CharType, SizeType>;
		/** */
		using StringView = Text::StringViewT<const CharType, SizeType>;
		/** */
		using Token = Token<CharType, SizeType>;
		/** */
		using Evaluator = Evaluator<EValType, CharType, SizeType>;
		/** */
		struct File {
			/** */
			StringWriter _text;
			/** */
			StringReader _reader;
			/** */
			Text::StringView8 _filename;
		};



		/** */
		static constexpr SizeType Hash(const StringView & def) { return def.Hash(); }
		static constexpr SizeType TokenDefine  = DD::Text::Hash("define");
		static constexpr SizeType TokenInclude = DD::Text::Hash("include");
		static constexpr SizeType TokenPragma  = DD::Text::Hash("pragma");
		static constexpr SizeType TokenIf      = DD::Text::Hash("if");
		static constexpr SizeType TokenIfdef   = DD::Text::Hash("ifdef");
		static constexpr SizeType TokenIfndef  = DD::Text::Hash("ifndef");
		static constexpr SizeType TokenElse    = DD::Text::Hash("else");
		static constexpr SizeType TokenElif    = DD::Text::Hash("elif");
		static constexpr SizeType TokenEndif   = DD::Text::Hash("endif");


		/** */
		Collections::LazyList<File, 7> _stack;
		/** Keeps information about preprocessor enabled/disabled code. */
		Collections::LazyList<If, 16> _ifStack;
		/** */
		StringWriter _writer;
		/** */
		StringWriter _tmpWriters[2];
		/** */
		FileBankT FileBank;
		/** */
		MacroBankT MacroBank;


		/** */
		void fail() {}


		/**
		 * Take given text and try to replace tokens with exiting macros, if such replacement suitable.
		 * @return true if parsing was applied (typically implies that we need one more iteration of parsing)
		 */
		bool parseInput(StringView input, StringWriter & output) {
			// prepare variables
			StringReader lineIn{ input };
			output.Reset();
			bool pasingApplied = false;

			// for each token, read if it is a macro, then apply it, otherwise forward the input
			for (Token toke; StringView word = lineIn.ReadWhile(toke); toke = Token()) {
				// if macro has been applied, notify
				if (toke.Mode == Token::Modes::Word && MacroBank.Replace(word, lineIn, output))
					pasingApplied = true;
				// forward output otherwise
				else
					output.Write(word);
			}

			return pasingApplied;
		}
		/** Take input and try apply replacing macros as long as terms keep being replaced. */
		StringView parseInput(StringView input) {
			// flip buffers to keep content between iterations
			bool iteration = 0;
			for (; parseInput(input, _tmpWriters[iteration]); iteration = !iteration) {
				// make output of one iteration input of another one
				input = _tmpWriters[iteration].View();
			}

			return input;
		}
		/** */
		void parseLine(StringView lineIn) {
			// check if read data are valid
			if (!_ifStack.IsEmpty() && !_ifStack.Last().IsBranchValid)
				return;

			// replace and output
			_writer.WriteN(parseInput(lineIn));
		}



		bool evaluateLine(StringView line) {
			StringView parsed = parseInput(line);
			Evaluator eval(parsed);
			EValType result;
			if (eval.Eval(result) == DD::Preprocessor::E_OK)
				return result != 0;

			fail();
			return false;
		}

		void PushFile(Text::StringView8 view) {
			File & file = _stack.Append();
			file._reader = { view };
		}

		void Parse() {
			while (!_stack.IsEmpty()) {
				if (_stack.Last()._reader.Any())
					ReadLine(1);
				else
					_stack.RemoveLast();
			}
		}

		/***/
		void ParseLine() {}

		StringReader & Reader() { return _stack.Last()._reader; }



		void Include(Text::StringView8 inc) {
			typename FileBankT::CachedFile * cachedFile = FileBank.GetFile(inc);
			if (cachedFile) {
				File & f = _stack.Append();
				f._reader = StringReader{ cachedFile->Data()};
				f._filename = cachedFile->GetKey();
			}
		}
		/** */
		void ReadLine(bool output) {
			StringView line = Reader().ReadLineEsc();
			StringReader reader{ line };
			reader.SkipWhiteSpace();
			if (reader.Cursor() == '#') {
				Text::StringView8 token = reader.Skip().SkipWhiteSpace().ReadWord();
				switch (token.Hash()) {
				case TokenDefine: MacroBank.Load(line); break;
				case TokenIf:     _ifStack.Append(If{ evaluateLine(line) }); break;
				case TokenElif:   _ifStack.Last().ApplyIf(_ifStack.Last().NeedsExpression() && evaluateLine(line)); break;
				case TokenIfdef:  _ifStack.Append(MacroBank.Defined(reader.ReadNextWord())); break;
				case TokenIfndef: _ifStack.Append(!MacroBank.Defined(reader.ReadNextWordEx())); break;
				case TokenElse:   _ifStack.Last().ApplyElse(); break;
				case TokenEndif:  _ifStack.RemoveLast(); break;
				case TokenInclude:
					switch (reader.SkipWhiteSpace().Cursor())
					{
					case '"': Include(reader.ReadQuotes().RemoveFirstLast()); break;
					case '<':Include(reader.ReadAngleBrackets().RemoveFirstLast()); break;
					default: fail(); break;
					}

					break;

				case TokenPragma:
					if (reader.ReadNextNonWS() == "once")
						FileBank.CachedFiles.Get(_stack.Last()._filename)->Once = true;
					break;

				default: fail(); break;
				}
			}
			else if (reader.CheckAndSkip("//")) {
				// just comment
				//reader.ReadUntil('\n');
			}
			else if (output) {
				parseLine(line);
			}
		}
	};
}
