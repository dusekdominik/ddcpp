#pragma once
namespace DD::Preprocessor {
	/**
	 * Char reader designed for Text:SringReader.
	 * Suited for reading function-macro-args of preprocessor language.
	 */
	template<typename CHAR_TYPE, typename SIZE_TYPE>
	struct ArgumentToken {
		/** Letter type. */
		using CharType = CHAR_TYPE;
		/** Indexing type */
		using SizeType = SIZE_TYPE;
		/** States of token. */
		enum States {
			Continue, // another token is reachable (ends with comma)
			End,      // final argument has bean reached (ends with right parenthesis)
			Undefined // An error occurred
		};

		/** Token state. */
		States State = Undefined;
		/** Counter for handling nested parenthesis. */
		SizeType BracketCnt = 0;
		/** Char reader impl. */
		constexpr bool operator()(CharType c);
		/** */
		constexpr void Reset() { BracketCnt = 0; }
	};

}


namespace DD::Preprocessor {
	template<typename C, typename S>
	constexpr bool ArgumentToken<C, S>::operator()(CharType c) {
		switch (c) {
		case '(':
			++BracketCnt;
			return true;

		case ')':
			// continue until bracket count is zero
			if (BracketCnt == 0) {
				State = End;
				return false;
			}
			else {
				--BracketCnt;
				return true;
			}

		case ',':
			if (BracketCnt == 0) {
				State = Continue;
				return false;
			}
			// next arg detected
			return true;

		default:
			return true;
		}
	}
}
