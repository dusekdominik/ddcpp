#pragma once
namespace DD::Preprocessor {
	/**
	 * Tool for evaluation text expressions
	 * Simple expression evaluator.
	 * https://en.wikipedia.org/wiki/Recursive_descent_parser
	 */
	template<typename NUMBER_TYPE, typename CHAR_TYPE, typename SIZE_TYPE>
	struct Evaluator;

	/** Errors produced by invalid input. */
	enum EvaluatorErrors {
		E_OK,
		E_SHIFT,   // shift must be done on positive value with admissible size of shift
		E_EMPTY,   // expression must not be empty
		E_REST,    // there must not be an unparsed text behind expression
		E_BRACKET, // all bracket must match their pairs
		E_CONST,   // constants must be from the list (true | false)
		E_NUMBER,  // number must be parse-able by TryCast method
	};
}

#include <DD/Math/FunctionsLite.h>
#include <DD/Text/StringReader.h>

#define _DD_PREPROCESSOR_FWD(call) {EvaluatorErrors e = call; if (e != E_OK) return e;}

namespace DD::Preprocessor {
	template<typename NUMBER_TYPE, typename CHAR_TYPE, typename SIZE_TYPE>
	struct Evaluator {
	public: // nested
		/** Type for evaluations numbers. */
		using NType = NUMBER_TYPE;
		/** Indexing type. */
		using SizeType = SIZE_TYPE;
		/** Letter type. */
		using CharType = const CHAR_TYPE;
		/** Text type. */
		using StringView = Text::StringViewT<CharType, SizeType>;

	public: // statics
		/** Maximal available shift. */
		static constexpr NType BitLength = sizeof(NType) * 8 - 1;
		/** Constant. */
		static constexpr CharType True[] = { 't', 'r', 'u', 'e', '\0'};
		/** Constant. */
		static constexpr CharType False[] = { 'f', 'a', 'l', 's', 'e', '\0' };

		/** Legacy impl of preprocessor shift operation. */
		static constexpr EvaluatorErrors LShift(NType & value, NType shift);
		/** Legacy impl of preprocessor shift operation. */
		static constexpr EvaluatorErrors RShift(NType & value, NType shift);

	public: // properties
		/** Reader of expression. */
		DD::Text::StringReader<CharType, SizeType> Reader;

	protected: // methods
		/** Expression = Term + Term - Term & Term | Term ^ Term << Term >> Term */
		EvaluatorErrors Expression(NType & target);
		/** Number = (123 | +123 | -123 | 0xDDEAD000). */
		EvaluatorErrors Number(NType & result);
		/** Constant = (true | false). */
		EvaluatorErrors ConstantAssert(NType & result, StringView text, NType value);
		/** Term = Factor * Factor / Factor % Factor. */
		EvaluatorErrors Term(NType & result);
		/** Factor = (Constant | Number | Expression) */
		EvaluatorErrors Factor(NType & result);

	public: // methods
		/** Evaluate expression, @return E_OK if succeded, error otherwise; parsed value is stored in @param result. */
		EvaluatorErrors Eval(NType & result);

	public: // constructors
		/** Constructor. */
		Evaluator(StringView view) : Reader(view) {}
	};
}


namespace DD::Preprocessor {
	/** Legacy impl of preprocessor shift operation. */
	template<typename N, typename C, typename S>
	constexpr EvaluatorErrors Evaluator<N, C, S>::LShift(NType & value, NType shift) {
		if (value < 0 || shift < 0) {
			// legacy return value
			value = 0;
			return E_SHIFT;
		}

		value = value << Math::Min(shift, BitLength);
		return E_OK;
	}


	template<typename N, typename C, typename S>
	constexpr EvaluatorErrors Evaluator<N, C, S>::RShift(NType & value, NType shift) {
		if (value < 0) {
			value = -1;
			return E_SHIFT;
		}

		if (shift < 0) {
			value = 0;
			return E_SHIFT;
		}

		value = value >> Math::Min(shift, BitLength);
		return E_OK;
	}


	template<typename N, typename C, typename S>
	EvaluatorErrors Evaluator<N, C, S>::Eval(NType & result) {
		if (Reader.Text.IsEmpty())
			return E_EMPTY;

		Reader.SeekBegin();
		_DD_PREPROCESSOR_FWD(Expression(result));
		return Reader.Any() ? E_REST : E_OK;

	}


	template<typename N, typename C, typename S>
	EvaluatorErrors Evaluator<N, C, S>::Expression(NType & target) {
		// load first operand
		_DD_PREPROCESSOR_FWD(Term(target));


		for (bool end = false; !end;) {
			NType rhs;
			switch (Reader.SkipWhiteSpace().Cursor()) {
			case '+': Reader.Skip(); _DD_PREPROCESSOR_FWD(Term(rhs)); target += rhs; break;
			case '-':	Reader.Skip(); _DD_PREPROCESSOR_FWD(Term(rhs)); target -= rhs; break;
			case '&': Reader.Skip(); _DD_PREPROCESSOR_FWD(Term(rhs)); target &= rhs; break;
			case '|': Reader.Skip(); _DD_PREPROCESSOR_FWD(Term(rhs)); target |= rhs; break;
			case '^': Reader.Skip(); _DD_PREPROCESSOR_FWD(Term(rhs)); target ^= rhs; break;
			case '<': Reader.Skip(); _DD_PREPROCESSOR_FWD(Term(rhs)); _DD_PREPROCESSOR_FWD(LShift(target, rhs)); break;
			case '>': Reader.Skip(); _DD_PREPROCESSOR_FWD(Term(rhs)); _DD_PREPROCESSOR_FWD(RShift(target, rhs)); break;
				break;

			default: end = true; break;
			}
		}

		return E_OK;
	}


	template<typename N, typename C, typename S>
	EvaluatorErrors Evaluator<N, C, S>::Number(NType & result) {
		// ignore unary plus (not handled by trycast now)
		if (Reader.Cursor() == '+')
			Reader.SkipOne();

		return (Reader.ReadNextNumberEx().TryCast(result)) ? E_OK : E_NUMBER;
	}


	template<typename N, typename C, typename S>
	EvaluatorErrors Evaluator<N, C, S>::ConstantAssert(NType & result, StringView text, NType value) {
		if (!Reader.CheckAndSkip(text))
			return E_CONST;

		result = value;
		return E_OK;
	}


	template<typename N, typename C, typename S>
	EvaluatorErrors Evaluator<N, C, S>::Term(NType & result) {
		_DD_PREPROCESSOR_FWD(Factor(result));
		for (;;) {
			NType rhs;
			switch (Reader.SkipWhiteSpace().Cursor()) {
			case '*': Reader.Skip(); _DD_PREPROCESSOR_FWD(Factor(rhs)); result *= rhs; break;
			case '/': Reader.Skip(); _DD_PREPROCESSOR_FWD(Factor(rhs)); result /= rhs; break;
			case '%': Reader.Skip(); _DD_PREPROCESSOR_FWD(Factor(rhs)); result %= rhs; break;
			default:
				return E_OK;
			}
		}
	}


	template<typename N, typename C, typename S>
	EvaluatorErrors Evaluator<N, C, S>::Factor(NType & result) {
		switch (Reader.SkipWhiteSpace().Cursor()) {
		case '(': { // ()
			Reader.Skip();
			_DD_PREPROCESSOR_FWD(Expression(result));
			// assert ending bracket
			return Reader.SkipWhiteSpace().CheckAndSkip(")") ? E_OK : E_BRACKET;
		}
		case 't': // true
			return ConstantAssert(result, True, 1);
		case 'f': // false
			return ConstantAssert(result, False, 0);
		default:
			return Number(result);
		}
	}
}
