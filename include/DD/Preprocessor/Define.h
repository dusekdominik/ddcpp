#pragma once
namespace DD::Preprocessor {
	/** */
	template<typename CHAR_TYPE, typename SIZE_TYPE>
	struct DefineT;
}

#include <DD/Collections/HashMap.h>
#include <DD/Text/Hash.h>
#include <DD/Text/StringReader.h>
#include <DD/Text/StringView.h>
#include <DD/Text/StringWriter.h>

namespace DD::Preprocessor {
	template<typename CHAR_TYPE, typename SIZE_TYPE>
	struct DefineT {
		/** Letter type. */
		using CharType = CHAR_TYPE;
		/** Size type. */
		using SizeType = SIZE_TYPE;
		/** String view. */
		using StringView = Text::StringViewT<const CharType, SizeType>;
		/** Writer. */
		using StringWriter = Text::StringWriterT<CharType, SizeType>;
		/** */
		using StringReader = Text::StringReader<CharType, SizeType>;

		/** __VA_ARGS__ */
		static constexpr CharType VaArgs[] = { '_', '_', 'V', 'A', '_', 'A', 'R', 'G', 'S', '_', '_', '\0' };

		/** Name of the macro. */
		StringWriter Name;
		/** Text of the macro. */
		StringWriter Value;
		/** Arguments. */
		Collections::HashMap<StringView, StringView, SizeType, 0, &Text::Hash> Arguments;

		/** Comparison op. */
		constexpr bool operator==(const DefineT & rhs) const { return Name.View() == rhs.Name.View(); }
		/** Check if args are variadic. */
		constexpr bool IsVariadic() const { return Arguments.Count() && Arguments.Values.Last() == VaArgs; }
		/** Hashmap key. */
		constexpr StringView GetKey() const { return Name.View(); }
	};
}