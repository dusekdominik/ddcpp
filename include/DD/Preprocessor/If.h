#pragma once
namespace DD::Preprocessor {
	/** Handler of if/elif/else/endif branches. */
	struct If {
		/** Is the current branch valid. */
		bool IsBranchValid = false;
		/** Was any of previous branches valid. handles elif/else sections. */
		bool HasBeenValid = false;

		/** If already was if branch found, we do not need to evaluate elif branches. */
		constexpr bool NeedsExpression() const { return !HasBeenValid; }
		/** If or elif handling. */
		constexpr void ApplyIf(bool expression);
		/** Last branch handling, */
		constexpr void ApplyElse();

		/** Constructor. */
		constexpr If(bool expression) : IsBranchValid(expression), HasBeenValid(expression) {}
	};
}


namespace DD::Preprocessor {
	constexpr void If::ApplyElse() {
		// not valid yet
		if (!HasBeenValid) {
			HasBeenValid = true;
			IsBranchValid = true;
		}
		else {
			IsBranchValid = false;
		}
	}


	constexpr void If::ApplyIf(bool expression) {
		// already was once valid or expression is negative
		if (!expression || HasBeenValid) {
			IsBranchValid = false;
		}
		// has not been valid and expression is positive
		else {
			HasBeenValid = true;
			IsBranchValid = true;
		}
	}
}
