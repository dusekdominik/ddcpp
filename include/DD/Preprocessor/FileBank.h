#pragma once
namespace DD::Preprocessor {
	/** */
	struct FileBankT;
}

#include <DD/Collections/HashMapText.h>
#include <DD/Collections/LazyList.h>
#include <DD/Com/HandleCache.h>
#include <DD/Com/FirstMatchProvider.h>
#include <DD/Text/StringView.h>

namespace DD::Preprocessor {
	struct FileBankT {
		/** File with cached full filename and file data. */
		struct CachedFile {
			/** Storage of data. */
			Com::HandleCache Cache;
			/** Pragma once applied. */
			bool Once = false;

			/** File data. */
			Text::StringView8 Data() { return Cache.GetCached(nullptr).AsText(); }
			/** Full filename */
			Text::StringView8 GetKey() const { return Cache.GetCached("id").AsText(); }

			/** Constructor - Cache from pointer. */
			CachedFile(Com::HandlePtr && handle) : Cache(Fwd(handle)) {}
			/** Move constructor.*/
			CachedFile(CachedFile &&) = default;
		};

		/** Directories that are scanned for includes. */
		Com::FirstMatchProvider Includes;
		/** Cached Data. */
		Collections::HashMapText8<CachedFile> CachedFiles;

		/** If filename can be loaded, return pointer, otherwise null. */
		CachedFile * GetFile(Text::StringView8 inc) {
			// try existence
			CachedFile file = Includes.ProduceEx(inc);
			// it does not exists
			if (!file.Cache.IsOpened())
				return nullptr;

			// get filename
			Text::StringView8 filename = file.Cache.Get("id").AsText();
			// try look-up filename
			CachedFile * cachedFile = CachedFiles.Get(filename);
			// nothing cached, cache it
			if (cachedFile == nullptr) {
				cachedFile = new (CachedFiles.InsertKey(filename)) CachedFile(Fwd(file));
				cachedFile->Cache.Get(nullptr);
				cachedFile->Cache.DisposeHandle();
			}

			// already included
			if (cachedFile->Once)
				return nullptr;

			return cachedFile;
		}
	};
}
