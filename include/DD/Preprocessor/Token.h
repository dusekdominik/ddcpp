#pragma once
namespace DD::Preprocessor {
	/**
	 * Char reader designed for Text:SringReader.
	 * Suited for reading tokens of preprocessor language.
	 */
	template<typename CHAR_TYPE, typename SIZE_TYPE>
	struct Token;
}

#include <DD/Preprocessor/CommentToken.h>
#include <DD/Text/StringReader.h>

namespace DD::Preprocessor {
	template<typename CHAR_TYPE, typename SIZE_TYPE>
	struct Token {
		/** Letter type. */
		using CharType = CHAR_TYPE;
		/** Indexing type */
		using SizeType = SIZE_TYPE;
		/** Default reader. */
		using StringReader = Text::StringReader<CharType, SizeType>;
		/** Modes of Token reader. */
		enum Modes {
			Undefined,   // We yet don't know what type of token is being read
			Quotes,      // String
			WhiteSpaces, // space, newline, tab
			Number,      // Number, HexaNumber
			Special,     // Typically operators with exception for slash
			LP,          // Left parenthesis
			RP,          // Right parenthesis
			SlashOrComment, // "/", "//...\n", "/*...*/"
			Word         // "_a1", "a1"
		};

		/** Current mode. */
		Modes Mode = Undefined;
		/** Reader for mode Word. */
		StringReader::WordEx _word;
		/** Reader for mode Quote. */
		StringReader::Quote _string;
		/** Reader for mode Number. */
		StringReader::NumberEx _number;
		/** Reader for mode WhiteSpace. */
		StringReader::WhiteSpace _whitespace;
		/** Reader for mode left parenthesis. */
		StringReader::template IsCharOnce<'('> _lp;
		/** Reader for mode right parenthesis. */
		StringReader::template IsCharOnce<')'> _rp;
		/** Reader for mode right SlashOrComment. */
		CommentToken<CharType> _slash;
		/** Reader for special operators. */
		StringReader::template IsChar<
			'+', '-', '*',
			'{', '}', '[', ']', '<', '>',
			'&', '|', '^', '%', '~', '!'
		> _special;

		/** Select mode. */
		constexpr bool select(CharType c);
		/** Process input. */
		constexpr bool operator()(CharType c);
	};
}


namespace DD::Preprocessor {
	template<typename C, typename S>
	constexpr bool Token<C, S>::select(CharType c) {
#define _FWD(call, mode) if (call) { Mode = mode; return true;}
		_FWD(_string(c), Quotes);
		_FWD(_whitespace(c), WhiteSpaces);
		_FWD(_number(c), Number);
		_FWD(_slash(c), SlashOrComment);
		_FWD(_special(c), Special);
		_FWD(_lp(c), LP);
		_FWD(_rp(c), RP);
		_FWD(_word(c), Word);
		return false;
#undef _FWD
	}


	template<typename C, typename S>
	constexpr bool Token<C, S>::operator()(CharType c) {
		switch (Mode) {
		case Undefined:
			return select(c);
		case Quotes:
			return _string(c);
		case WhiteSpaces:
			return _whitespace(c);
		case Number:
			return _number(c);
		case Special:
			return _special(c);
		case SlashOrComment:
			return _slash(c);
		case Word:
			return _word(c);
		case LP:
		case RP:
		default:
			return false;
		}
	}
}