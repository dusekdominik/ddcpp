#pragma once
namespace DD {
	/**
	 * Comparison traits for given type.
	 * operator< by default.
	 */
	template<typename T>
	struct SortedListTraitsCompare;

	/**
	 * Traits for sorted list.
	 * @param          T - Item Type
	 * @param ALLOCATION - Local allocation
	 * @param COMAPRE    - Compare function
	 */
	template<typename T, size_t ALLOCATION, typename COMPARE>
	struct SortedListTraits;

	/**
	 * Implementation of sorted array list.
	 * @param TRAITS - traits for list, @see SortedListTraits.
	 */
	template<typename TRAITS>
	struct SortedListBase;

	/**
	 * SortedList
	 * @param          T - item type
	 * @param ALLOCATION - local allocation of list (number of preallocated items)
	 * @param    COMPARE - compare traits
	 */
	template<typename T, size_t ALLOCATION = 0, typename COMPARE = SortedListTraitsCompare<T>>
	struct SortedList;
}

#include <DD/List.h>

namespace DD {
	template<typename T>
	struct SortedListTraitsCompare {
		/** Compare function, operator< by default. */
		static bool Compare(const T & l, const T & r) { return l < r; }
	};


	template<typename T, size_t ALLOC, typename COMPARE>
	struct SortedListTraits {
		/** Type of item. */
		typedef T TYPE;
		/** Number of locally allocated items. */
		static constexpr size_t ALLOCATION = ALLOC;
		/** Compare function, operator< by default. */
		static bool Compare(const TYPE & l, const TYPE & r) { return COMPARE::Compare(l, r); }
	};


	template<typename TRAITS>
	struct SortedListBase {
	public: // nested
		/** Item Type. */
		using Type = typename TRAITS::TYPE;

	protected: // properties
		/** Nested array-list. */
		List<Type, TRAITS::ALLOCATION> _list;

	protected: // methods
		/** Select index into which item should be inserted. */
		size_t select(const Type & t);

	public: // accession methods
		/** Iterator. */
		Type * begin() { return _list.begin(); }
		/** Iterator. */
		Type * end() { return _list.end(); }
		/** Indexer. */
		Type & operator[](size_t i) { return _list[i]; }
		/** Iterator. */
		const Type * begin() const { return _list.begin(); }
		/** Iterator. */
		const Type * end() const { return _list.end(); }
		/** Indexer. */
		const Type & operator[](size_t i) const { return _list[i]; }
		/** number of items. */
		size_t Size() const { return _list.Size(); }

	public: // insertion methods
		/** Sorted addition. */
		size_t Add(Type && arg);
		/** Sorted addition. */
		size_t Add(const Type & arg);
		/**
		 * Reset list size.
		 * Method does not deallocate stored items.
		 * Method does not touch the allocation.
		 * @see Free()
		 */
		void Clear() { _list.Clear(); }
		/**
		 * Reset list size.
		 * Method deallocates stored items.
		 * Method does not touch the allocation.
		 */
		void Free() { _list.Free(); }
	};


	template<typename T, size_t A, typename C>
	struct SortedList
		: public SortedListBase<SortedListTraits<T, A, C>>
	{

	};
}


namespace DD {
	template<typename TRAITS>
	size_t SortedListBase<TRAITS>::select(const Type & t) {
		DD_TODO_STAMP("Change to binary compare instead of linear.");
		for (size_t i = 0, m = Size(); i < m; ++i) {
			if (TRAITS::Compare(t, _list[i])) {
				return i;
			}
		}

		return Size();
	}


	template<typename TRAITS>
	inline size_t SortedListBase<TRAITS>::Add(Type && arg) {
		size_t result = select(arg);
		_list.Insert(result, arg);
		return result;
	}


	template<typename TRAITS>
	inline size_t SortedListBase<TRAITS>::Add(const Type & arg) {
		size_t result = select(arg);
		_list.Insert(result, arg);
		return result;
	}
}
