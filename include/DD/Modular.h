#pragma once
namespace DD {
	/** Static modular integer. */
	template<typename T, T MOD>
	class Modular;

	/** Dynamic Integer. */
	template<typename T>
	class DModular;

	template<unsigned __int64 MOD>
	using Modular64 = Modular<unsigned __int64, MOD>;
	template<unsigned __int32 MOD>
	using Modular32 = Modular<unsigned __int32, MOD>;
	template<unsigned __int16 MOD>
	using Modular16 = Modular<unsigned __int16, MOD>;
	template<unsigned __int8 MOD>
	using Modular8 = Modular<unsigned __int8, MOD>;

	using DModular64 = DModular<unsigned __int64>;
	using DModular32 = DModular<unsigned __int32>;
	using DModular16 = DModular<unsigned __int16>;
	using DModular8 = DModular<unsigned __int8>;
}

namespace DD {
	/** Type conversion helper. */
	template<size_t SIZE> struct UIntFromSize {};
	template<> struct UIntFromSize<8> { typedef unsigned __int64 type_t; };
	template<> struct UIntFromSize<4> { typedef unsigned __int32 type_t; };
	template<> struct UIntFromSize<2> { typedef unsigned __int16 type_t; };
	template<> struct UIntFromSize<1> { typedef unsigned __int8 type_t; };
	/** Type conversion helper. */
	template<typename T> struct UIntFromType { typedef typename UIntFromSize<sizeof(T)>::type_t uint_t; };

// shared implementation of static and dynamic modulars
#define __DEFINE_MODULAR(type, mod)\
operator const uint_t & () const { return _intData; }\
operator uint_t & () { return _intData; }\
operator const T & () const { return _data; }\
operator T & () { return _data; }\
type & operator++() { _intData = (_intData + 1) % mod; return *this; }\
type & operator--() { _intData = _intData ? _intData - 1 : mod - 1; return *this; }\
type & operator=(T i) { _intData = i % mod; return *this; }\
type & Increase() { return ++*this; }\
type & Decrease() { return --*this; }\
type & Crease(bool increase) { return increase ? Increase() : Decrease(); }\

	template<typename T, T MOD>
	class Modular : public UIntFromType<T> {
		static constexpr uint_t MOD_UINT = static_cast<uint_t>(MOD);
		static constexpr T MOD_T = (MOD);
		union { T _data;  uint_t _intData; };
	public:
		__DEFINE_MODULAR(Modular, MOD_UINT);
		Modular(T data) { *this = data; }
		Modular() : _data() { }
	};

	template<typename T>
	class DModular : public UIntFromType<T> {
		union { T _data;  uint_t _intData; };
		union { T _mod;  uint_t _intMod; };
	public:
		__DEFINE_MODULAR(DModular, _intMod);
		DModular & SetMod(T & mod) { _mod = mod; }
		DModular(T mod, T data) : _mod(mod) { *this = data; }
		DModular(T mod) : _mod(mod), _data() { }
	};

#undef __DEFINE_MODULAR
}