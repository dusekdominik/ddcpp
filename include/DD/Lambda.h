#pragma once
namespace DD {
	/**
	 * Interface of lambda capturers.
	 * @example use interface to parse callback as param
	 *   void Run(DD::ILambda<void()> * onStartCallback, DD::ILambda<void()> * onEndCallback) {
	 *     onStartCallback->Run();
	 *     ... doSomething ...
	 *     onEndCallback->Run();
	 *   }
	 */
	template<typename SIGNATURE>
	struct ILambda;

	/**
	 * Lambda with local storage.
	 * Could be used for using commands without an allocation.
	 * @examples
	 *   DD::Stack<LambdaLocal<void(), 32>> commands;
	 *   commands.Add([]() { ... });
	 *   commands.Add([]() { ... });
	 *   while (commands.Size())
	 *     commands.Pop()->Run();
	 */
	template<typename SIGNATURE, size_t ALLOCATION>
	struct LambdaLocal;

	/**
	 * Struct storing ILambda as reference.
	 * Implemented as referential type.
	 * If you want to avoid allocation on heap, @use LambdaLocal.
	 * @example common usage
	 *   Lambda<void(int)> myLambda = [](int i) { ... };
	 */
	template<typename SIGNATURE>
	struct Lambda;

	/** Interface for lambda with no return value. */
	template<typename...ARGS>
	using IAction = ILambda<void(ARGS...)>;

	/** Lambda with 16 (8 for vftable + 8 for local storage) allocated bytes. */
	template<typename SIGNATURE>
	using Lambda16 = LambdaLocal<SIGNATURE, 16>;
	/** Lambda with 32 (8 for vftable + 24 for local storage) allocated bytes. */
	template<typename SIGNATURE>
	using Lambda32 = LambdaLocal<SIGNATURE, 32>;
	/** Lambda with 64 (8 for vftable + 56 for local storage) allocated bytes. */
	template<typename SIGNATURE>
	using Lambda64 = LambdaLocal<SIGNATURE, 64>;

	/** Lambda with no return value. */
	template<typename...ARGS>
	using Action = Lambda<void(ARGS...)>;
	/** Locally allocated lambda with no return value. */
	template<typename...ARGS>
	using Action16 = Lambda16<void(ARGS...)>;
	/** Locally allocated lambda with no return value. */
	template<typename...ARGS>
	using Action32 = Lambda32<void(ARGS...)>;
	/** Locally allocated lambda with no return value. */
	template<typename...ARGS>
	using Action64 = Lambda64<void(ARGS...)>;
}

#include "Reference.h"
#include "Signature.h"

namespace DD {
	/************************************************************************/
	/* ILambda                                                              */
	/************************************************************************/
	template<typename RETURN, typename...ARGS>
	struct ILambda<RETURN(ARGS...)> {
	protected:
		/** Virtual method for running lambdas. */
		virtual RETURN iLambdaRun(ARGS...) = 0;

	public:
		/** Run lambda. */
		RETURN Run(ARGS...args) { return iLambdaRun(args...); }

	public:
		/** Auto conversion to lambda pointer. */
		operator ILambda *() { return this; }
		/** Destructor. */
		virtual ~ILambda() = 0;
	};

	/************************************************************************/
	/* LambdaStore                                                          */
	/************************************************************************/
	template<typename SIGNATURE>
	struct ILambdaStore : public ILambda<SIGNATURE> {
	protected: // ILambdaStore
		virtual void iLambdaStoreMove(void * ptr) = 0;
		virtual void iLambdaStoreCopy(void * target) = 0;
		virtual size_t iLambdaStoreSize() const = 0;
	public: // interface
		/** Move this lambda store to given target. */
		void Move(void * target) { iLambdaStoreMove(target); }
		/** Copy this lambda store to given target. */
		void Copy(void * target) { iLambdaStoreCopy(target); }
		/** Size of lambda store. */
		size_t Size() const { return iLambdaStoreSize(); }
	};


	/**
	 * Struct contains storage of a lambda.
	 * @param STORAGE          - native type of lambda
	 * @param SIGNATURE        - signature of ILambda interface
	 * @param NATIVE_SIGNATURE - signature of native lambda
	 * Native could have different signature than pattern which is captured.
	 * If these types are static castable, it is ok.
	 */
	template<typename STORAGE, typename SIGNATURE, typename NATIVE_SIGNATURE>
	struct LambdaStore;

	template<
		typename STORAGE,
		typename RETURN,
		typename...ARGS,
		typename NATIVE_RETURN,
		typename...NATIVE_ARGS
	> struct LambdaStore<STORAGE, RETURN(ARGS...), NATIVE_RETURN(NATIVE_ARGS...)>
		: public ILambdaStore<RETURN(ARGS...)>
	{
		/** Storage for lambda properties. */
		STORAGE Storage;

	protected: // ILambda
		RETURN iLambdaRun(ARGS...args) override { return static_cast<NATIVE_RETURN>(Storage(static_cast<NATIVE_ARGS>(args)...)); }

	protected: // ILambdaStore
		void iLambdaStoreMove(void * target) override { new (target) LambdaStore(Fwd(Storage)); }
		void iLambdaStoreCopy(void * target) override { new (target) LambdaStore(Storage); }
		size_t iLambdaStoreSize() const override { return sizeof(*this); }

	public: // constructors
		/** Lambda catch constructor. */
		LambdaStore(const STORAGE & storage) : Storage(storage) {}
		/** Lambda catch constructor. */
		LambdaStore(STORAGE && storage) : Storage(Fwd(storage)) {}
		/** Destructor. */
		virtual ~LambdaStore() = default;
	};

	/************************************************************************/
	/* Lambda                                                               */
	/************************************************************************/
	template<typename RETURN, typename...ARGS>
	struct Lambda<RETURN(ARGS...)>
		: Reference<ILambda<RETURN(ARGS...)>>
	{
		using Interface = ILambda<RETURN(ARGS...)>;
		using Base      = Reference<Interface>;
		/** Run lambda. */
		RETURN operator()(ARGS...args) { return Base::_ref->Run(args...); }
		/**
		 * Lambda catch constructor.
		 * Lambda is caught into @see LambdaBase structure.
		 */
		template<typename LAMBDA>
		Lambda(const LAMBDA & lambda)
			: Base(new LambdaStore<LAMBDA, RETURN(ARGS...), Signature<LAMBDA>>(lambda))
		{}
		/** Nullptr constructor. */
		Lambda() : Base() {}
	};

	/************************************************************************/
	/* LambdaLocal                                                          */
	/************************************************************************/
	template<typename RETURN, typename...ARGS, size_t ALLOCATION>
	struct LambdaLocal<RETURN(ARGS...), ALLOCATION> {
		/** Native type of lambda store. */
		template<typename LAMBDA>
		using Capture = LambdaStore<LAMBDA, RETURN(ARGS...), Signature<LAMBDA>>;
		/** Abstract type of lambda store. */
		using Interface = ILambdaStore<RETURN(ARGS...)> *;

	public: // statics
		/** Zero function. */
#if _DEBUG
		static constexpr auto ZERO = [](ARGS...) -> RETURN { throw; };
#else
		static constexpr auto ZERO = [](ARGS...) -> RETURN { return RETURN(); };
#endif

	protected: // properties
		/** Allocation for storing lambda locally. */
		char _allocation[ALLOCATION];

	public: // operators
		/** Conversion operator. */
		operator ILambda<RETURN(ARGS...)> *() { return (Interface)_allocation; }
		/** Conversion operator. */
		ILambda<RETURN(ARGS...)> * operator->() { return (Interface)_allocation; }
		/** Functor calling lambda. */
		RETURN operator()(ARGS...args) { return ((Interface)_allocation)->Run(args...); }
		/** Lambda capture assignment. */
		template<typename LAMBDA>
		LambdaLocal & operator=(const LAMBDA & lambda);

	public: // constructors
		/** Lambda capture constructor. */
		template<typename LAMBDA>
		LambdaLocal(const LAMBDA & lambda);
		/** Zero constructor. */
		LambdaLocal() : LambdaLocal(ZERO) {}
		/** Move constructor. */
		LambdaLocal(LambdaLocal && rvalue) { ((Interface)rvalue._allocation)->Move(_allocation); }
		/** Copy constructor. */
		LambdaLocal(const LambdaLocal & lvalue) { ((Interface)lvalue._allocation)->Copy(_allocation); }
		/** Destructor. */
		~LambdaLocal() { *this = (ZERO); }
	};
}

namespace DD {
	template<typename RETURN, typename...ARGS>
	ILambda<RETURN(ARGS...)>::~ILambda() {}

	template<typename RETURN, typename...ARGS, size_t ALLOCATION>
	template<typename LAMBDA>
	LambdaLocal<RETURN(ARGS...), ALLOCATION> & LambdaLocal<RETURN(ARGS...), ALLOCATION>::operator=(const LAMBDA & lambda) {
		static_assert(sizeof(Capture<LAMBDA>) <= ALLOCATION, "Allocation of lambda overflow.");
		((Interface)_allocation)->~ILambdaStore();
		new(_allocation) Capture<LAMBDA>(lambda);
		return *this;
	}

	template<typename RETURN, typename...ARGS, size_t ALLOCATION>
	template<typename LAMBDA>
	LambdaLocal<RETURN(ARGS...), ALLOCATION>::LambdaLocal(const LAMBDA & lambda) {
		static_assert(sizeof(Capture<LAMBDA>) <= ALLOCATION, "Allocation of lambda overflow.");
		new(_allocation) Capture<LAMBDA>(lambda);
	}
}