#pragma once
namespace DD {
	/**
	 * Public interface wrapper for all indexable collections.
	 * Provides methods for browsing indexables.
	 * Methods are sourced from virtual IIndexable interface.
	 */
	template<typename T>
	class Indexable;
	/** Interface providing virtual methods for RW collection items by index. */
	template<typename T>
	struct IIndexable;
	/** Factory for IIndexable interfaces. Wraps methods via sfinae functionality. */
	template<typename COLLECTION>
	struct IIndexableWrapper;

	/**
	 * Public interface of indexable collections.
	 * Provides methods for browsing indexables.
	 * Methods are sourced from virtual IConstIndexable interface.
	 */
	template<typename T>
	class ConstIndexable;
	/** Interface providing virtual methods for R collection items by index. */
	template<typename T>
	struct IConstIndexable;
	/** Factory for IIndexable interfaces. Wraps methods via sfinae functionality. */
	template<typename COLLECTION>
	struct IConstIndexableWrapper;
}

#include "Allocation.h"
#include "SFINAE.h"

namespace DD {
	template<typename T>
	struct IIndexable {
		friend class Indexable<T>;
		/** Target type of indexable collection. */
		typedef T target_t;
	protected: // properties
		/** Store data for an indexable. */
		Allocation<16> _allocation;
	protected: // virtual methods
		/** Return item on given index. */
		virtual target_t & iIndexableAt(size_t) = 0;
		/** Get size of the collection. */
		virtual size_t iIndexableSize() const = 0;
	};

	template<typename COLLECTION>
	struct IIndexableWrapper : public IIndexable<sfinaeAtType<COLLECTION>> {
    typedef typename IIndexable<sfinaeAtType<COLLECTION>>::target_t target_t;
	protected: // IIterable
		virtual target_t & iIndexableAt(size_t i) override { return sfinaeAt(*(_allocation.Cast<COLLECTION*>()), i); }
		virtual size_t iIndexableSize() const override { return sfinaeSize(*(_allocation.Cast<COLLECTION*>())); }
	public: // constructors
		/** Construct indexable from an collection. */
		IIndexableWrapper(COLLECTION & collection) { _allocation = &collection; }
	};

	template<typename T>
	class Indexable {
		/** Storage for indexable. */
		Allocation<32> _allocation;
	public: // methods
		/** Indexer. */
		T & operator[](size_t i) { return _allocation.Cast<IIndexable<T>>().iIndexableAt(i); }
		/** Get size of collection. */
		size_t Size() const { return _allocation.Cast<IIndexable<T>>().iIndexableSize(); }
	public: // constructors
		/** Constructor from an IIndexable interface. */
		Indexable(IIndexable<T> & indexable) : _allocation(indexable) {}
		/** Constructor from an IIndexable interface. */
		Indexable(IIndexable<T> * indexable) : _allocation(*indexable) {}
		/** Constructor via default wrapper. */
		template<typename COLLECTION>
		Indexable(COLLECTION & collection) : _allocation(IIndexableWrapper<COLLECTION>(collection)) {}
	};

	template<typename T>
	struct IConstIndexable {
		friend class ConstIndexable<T>;
		/** Target type of indexable collection. */
		typedef const T target_t;
	protected: // properties
		/** Store data for an indexable. */
		Allocation<16> _allocation;
	protected: // virtual methods
		/** Return item on given index. */
		virtual target_t & iConstIndexableAt(size_t) const = 0;
		/** Get size of the collection. */
		virtual size_t iConstIndexableSize() const = 0;
	};

	template<typename COLLECTION>
	struct IConstIndexableWrapper : public IConstIndexable<sfinaeAtConstType<COLLECTION>> {
    typedef typename IConstIndexable<sfinaeAtConstType<COLLECTION>>::target_t target_t;
	protected: // IConstIndexable
		virtual target_t & iConstIndexableAt(size_t i) const override { return sfinaeAtConst(*(_allocation.Cast<const COLLECTION*>()), i); }
		virtual size_t iConstIndexableSize() const override { return sfinaeSize(*(_allocation.Cast<const COLLECTION*>())); }
	public: // constructors
		/** Construct indexable from an collection. */
		IConstIndexableWrapper(const COLLECTION & collection) { _allocation = &collection; }
	};

	template<typename T>
	class ConstIndexable {
		/** Storage for indexable. */
		Allocation<32> _allocation;
	public: // methods
		/** Const indexer. */
		const T & operator[](size_t i) const { return _allocation.Cast<IConstIndexable<T>>().iConstIndexableAt(i); }
		/** Get size of collection. */
		size_t Size() const { return _allocation.Cast<IConstIndexable<T>>().iConstIndexableSize(); }
	public: // constructors
		/** Constructor from an IIndexable interface. */
		ConstIndexable(IConstIndexable<T> & indexable) : _allocation(indexable) {}
		/** Constructor from an IIndexable interface. */
		ConstIndexable(IConstIndexable<T> * indexable) : _allocation(*indexable) {}
		/** Constructor via default wrapper. */
		template<typename COLLECTION>
		ConstIndexable(const COLLECTION & collection) : _allocation(IConstIndexableWrapper<COLLECTION>(collection)) {}
	};
}
