#pragma once
namespace DD {
	/**
	 * Library for common matrices and matrix algorithm.
	 * 2D - Rotation
	 * 3D - Rotation
	 * 4D - Projection, Camera matrix
	 * ND - Covariance, QRAlgorithm, PCA analysis
	 */
	struct Matrices;
}

#define DD_MATRICES_LEFT_HANDED 1

#include "Math.h"
#include "Matrix.h"
#include "Macros.h"

DD_TODO_STAMP(
	"Add ascii art with declaration of coordinate system to 3D Rotation methods "
	"to be able declare what is (anti)-clock-wise rotation direction."
);

namespace DD {
	struct Matrices {
	public: // 2x2
		/**
		 * Rotation in 2D.
		 * @see other Matrices::Rotation() variants.
		 * @param c - cosine of angle.
		 * @param s - sine of angle.
		 * @example clock-wise rotation
		 *    f32 rad = Math::Deg2Rad(90);
		 *    f32 cos = Math::Cos(rad);
		 *    f32 sin = Math::Sin(rad);
		 *    float2(1.f, 1.f) * Matrices::Rotation(cos, sin); // ~ float2(1.f, -1.f)
		 * @example anti-clock-wise rotation
		 *    Matrices::Rotation(cos, sin) * float2(1.f, 1.f); // ~ float2(-1.f, 1.f)
		 */
		template<typename T>
		static M22<T> Rotation(const T & c, const T & s) { return M22<T>(c, -s, s, c); }
		/**
		 * Rotation in 2D.
		 * @param a - angle in radians [rad].
		 * @example clock-wise rotation
		 *    float2(1.f, 1.f) * Matrices::Rotation(Math::Deg2Rad(90)); // ~ float2(1.f, -1.f)
		 * @example anti-clock-wise rotation
		 *    Matrices::Rotation(Math::Deg2Rad(90)) * float2(1.f, 1.f); // ~ float2(-1.f, 1.f)
		 */
		template<typename T>
		static M22<T> Rotation(const T & a) { return Rotation(Math::Cos(a), Math::Sin(a)); }

		template<typename T>
		static T Rotation2Rad(const M22<T> & a) {
			 const T & sin = a.Cols(0)[1];
			 const T & cos = a.Cols(0)[0];
			 return Math::ArcTg2(sin, cos);
		}
		/**
		 * Rotation in 2D.
		 * @see other Matrices::Rotation() variants.
		 * @param norm - normalized orientation vector.
		 * @example angle computation
		 *   Matrices::Rotation(float( 1.f,  0.f)) ~ Matrices::Rotation(Math::Deg2Rad(0))
		 *   Matrices::Rotation(float( 0.f,  1.f)) ~ Matrices::Rotation(Math::Deg2Rad(90))
		 *   Matrices::Rotation(float(-1.f,  0.f)) ~ Matrices::Rotation(Math::Deg2Rad(180))
		 *   Matrices::Rotation(float( 0.f, -1.f)) ~ Matrices::Rotation(Math::Deg2Rad(300))
		 */
		template<typename T>
		static M22<T> Rotation(const V2<T> & norm) { return Rotation(norm.x, norm.y); }

	public: // 3x3
		/**
		 * Rotation matrix around given axis.
		 * @param angle - angle in radians [rad].
		 * @param axis  - axis around which rotation should be done.
		 */
		template<typename T, typename M>
		static M33<T> RotationAxis(const T & angle, const Math::Vector<M> &axis);
		/**
		 * Rotation matrix around axis X.
		 * @param angle - angle in radians [rad].
		 */
		template<typename T>
		static M33<T> RotationX(const T & angle);
		/**
		 * Rotation matrix around axis Y.
		 * @param angle - angle in radians [rad].
		 */
		template<typename T>
		static M33<T> RotationY(const T & angle);
		/**
		 * Rotation matrix around axis Z.
		 * @param angle - angle in radians [rad].
		 */
		template<typename T>
		static M33<T> RotationZ(const T & angle);

	public: // 4x4
		/**
		 * View matrix (left handed if @see DD_MATRICES_LEFT_HANDED).
		 * @param position  - world space camera position
		 * @param direction - direction of camera (normalized vector)
		 * @param up        - upward direction of camera (no need of normalized vector)
		 */
		template<typename M0, typename M1, typename M2>
		static M44<typename M0::Type> LookTo(
			const Math::Vector<M0> & position,
			const Math::Vector<M1> & direction,
			const Math::Vector<M2> & up
		);
		/** Projection matrix (left handed if @see DD_MATRICES_LEFT_HANDED). */
		template<typename T>
		static M44<T> Perspective(const T & fov, const T & aspect, const T & near, const T & far);

	public: // NxN
		/** Serves for extraction of template parameters from ITERABLE param. */
		template<typename ITERABLE, typename EXTRACT>
		class IterableHelper {
			/** Helper for removing reference. */
			template<typename T>
			static constexpr T unref(T &) {}
			/** Unrefed extracted vector. */
			typedef decltype(unref(EXTRACT::Extract(*((ITERABLE *)0)->begin()))) extract_t;
		public: // nested
			/** Basic type of vector. */
			typedef typename extract_t::type_t type_t;
			/** Vector with basic memory model. */
			typedef Vector<type_t, extract_t::DIMENSION> vector_t;
			/** Square matrix. */
			typedef Matrix<type_t, extract_t::DIMENSION> matrix_t;
		public: // statics
			/** Dimension of vector space. */
			static constexpr size_t DIMENSION = extract_t::DIMENSION;
		};

		/** Simple extractor returning identity. */
		struct NoExtract {
			template<typename VECTOR>
			static const auto & Extract(const VECTOR & v) { return v; }
		};

		/**
		 * Linear regression via least square algorithm.
		 * @param ITERABLE - an iterable collection with samples.
		 * @param EXTRACT  - a way how to extract a sample from given collection.
		 *
		 * As sample is considered extended vector.
		 * Assume following sample z = f(x, y):
		 *   this algorithm takes as its input three component vector (x y | z).
		 *   Last item of vector is assumed as output of an function.
		 *   First items of vector are considered as input of a linear function.
		 * @return vector with coefficients for linear equation.
		 *         if input is in format (x y | z) then output is (a b c) where
		 *         z = ax + by + c.
		 */
		template<typename ITERABLE, typename EXTRACT = NoExtract>
		static auto LinearRegression(ITERABLE & samples);

		/**
		 * Algorithm computes covariance matrix from points (similarly as octave cov(points)).
		 * @param DIMENSION - dimension of points
		 * @param ITERABLE  - a const iterable structure returning a structure over which we iterate.
		 *                  - e.g. Array<float2>
		 * @param EXTRACT   - traits for extraction of vector from ITERABLE item.
		 *                  - identity by default.
		 *
		 * @param points     [in]  - data to be extracted as vectors.
		 * @param covariance [out] - result of the method.
		 * @param mean       [out] - mean of the data set
		 */
		template<typename ITERABLE, typename EXTRACT = NoExtract>
		static void Covariance(
			ITERABLE & points,
			typename IterableHelper<ITERABLE, EXTRACT>::matrix_t & covariance,
			typename IterableHelper<ITERABLE, EXTRACT>::vector_t & mean
		);

		/**
		 * Matrix decomposition A = QR.
		 * Method is using Householder reflection.
		 * @param input - input matrix to be decomposed.
		 * @param Q     - output orthogonal matrix.
		 * @param R     - output upper triangular matrix.
		 */
		template<typename T, size_t SIZE>
		static void QRDecomposition(
			const Matrix<T, SIZE> & input,
			Matrix<T, SIZE> & Q,
			Matrix<T, SIZE> & R
		);
		/**
		 * Iterative algorithm for computing eigen vectors and eigen values.
		 * Columns of v are eigenvectors.
		 * i = vdv*, vv* = I
		 * @param d [in-out]   - diagonal of matrix d contains eigenvalues.
		 *                     - on the computation start, this matrix should be assigned
		 *                       by a matrix, which should be analyzed.
		 * @param v [in-out]   - matrix containing eigen vectors.
		 * @param q [out]      - matrix containing eigen vectors.
		 * @param r [out]      - matrix containing eigen vectors.
		 * @param maxIteration - number of iterations of algo.
		 */
		template<typename T, size_t SIZE>
		static void QRAlgorithm(
			Matrix<T, SIZE> & d,
			Matrix<T, SIZE> & v,
			Matrix<T, SIZE> & q,
			Matrix<T, SIZE> & r,
			size_t maxIteration = 10
		);

		/** Settings fro PCA. */
		struct PCAParams {
			/** Number of qr decomposition iteration in one batch. */
			size_t Batch = 10;
			/** Maximal number of batch iterations. */
			size_t Limit = 1000;
		};

		/** Output of PCA method. */
		template<typename T, size_t DIMENSION>
		struct PCAOutput {
			/** Mean vector of data-set. */
			Vector<T, DIMENSION> Mean;
			/** Covariance of data-set. */
			Matrix<T, DIMENSION> Covariance;
			/** Eigen vectors are column of this matrix. */
			Matrix<T, DIMENSION> EigenVectors;
			/** Eigen values - nth value of diagonal corresponds to n-th (col) eigen vector. */
			Matrix<T, DIMENSION> EigenValues;
			/** Flag if the computation got to convergent state. */
			bool Convergates;
		};

		/**
		 * Principal component analysis.
		 * This method serves to compute eigen values and eigen vectors
		 * of covariance matrix of given point set. As side effect,
		 * it is also computed mean point of set and its covariance matrix.
		 * Note that in common computational sw like Octave, eigen values
		 * are sorted asc, this method sets them desc.
		 * @param ITERABLE  - iterable set of input items.
		 * @param EXTRACTOR - Trait to extract DIMENSION-vector from an input item.
		 *                  - By default it expects, that input items are vectors.
		 */
		template<typename ITERABLE, typename EXTRACT = NoExtract>
		static auto PCA(ITERABLE & items, const PCAParams & params = PCAParams());

		/** Gauss-Jordan elimination. */
		template<typename T, size_t ROWS, size_t COLS>
		static void GaussJordanElimination(Matrix<T, ROWS, COLS> & matrix);
	};
}

namespace DD {
	/************************************************************************/
	/* Matrix3x3                                                            */
	/************************************************************************/
	template<typename T, typename M>
	M33<T> Matrices::RotationAxis(const T & angle, const Math::Vector<M> & axis) {
		static_assert(M::Length == 3, "Vector must be 3-dimensional");

		// Normalize axis
		const V3<T> axisN = Math::Normalize(V3<T>(axis));

		// Convert axis and angle into a quaternion (w, x, y, z)
		const T halfAngle = T(0.5) * angle;
		const T w = Math::Cos(halfAngle);
		const T sinHalfAngle = Math::Sin(halfAngle);
		const T x = sinHalfAngle * axisN.x;
		const T y = sinHalfAngle * axisN.y;
		const T z = sinHalfAngle * axisN.z;

		// Convert the quaternion into the matrix
		const T wx = w * x * T(2);
		const T wy = w * y * T(2);
		const T wz = w * z * T(2);
		const T xx = x * x * T(2);
		const T xy = x * y * T(2);
		const T xz = x * z * T(2);
		const T yy = y * y * T(2);
		const T yz = y * z * T(2);
		const T zz = z * z * T(2);

		return M33<T>(
			1 - yy - zz, xy - wz, xz + wy,
			xy + wz, 1 - xx - zz, yz - wx,
			xz - wy, yz + wx, 1 - xx - yy
			);
	}

	template<typename T>
	M33<T> Matrices::RotationX(const T & angle) {
		T s = Math::Sin(angle), c = Math::Cos(angle);
		return M33<T>(T(1), T(0), T(0), T(0), c, -s, T(0), s, c);
	}

	template<typename T>
	M33<T> Matrices::RotationY(const T & angle) {
		T s = Math::Sin(angle), c = Math::Cos(angle);
		return M33<T>(c, T(0), -s, T(0), T(1), T(0), s, T(0), c);
	}

	template<typename T>
	M33<T> Matrices::RotationZ(const T & angle) {
		T s = Math::Sin(angle), c = Math::Cos(angle);
		return M33<T>(c, -s, T(0), s, c, T(0), T(0), T(0), T(1));
	}

	/************************************************************************/
	/* Matrix4x4                                                            */
	/************************************************************************/
	template<typename T>
	M44<T> Matrices::Perspective(const T & tgFovX, const T & tgFovY, const T & nearZ, const T & farZ) {
		//const T halfAngle = T(0.5) * fovAngleY;
		//const T tg = Math::Tg(halfAngle);
		//const T tg = Math::Tg(fovAngleY) * 0.5f;
		const T w = T(1) / (tgFovX * T(0.5));
		const T h = T(1) / (tgFovY * T(0.5));
		const T range = farZ / (farZ - nearZ);
		return M44<T>(
			   w, T(0),  T(0),           T(0),
			T(0),    h,  T(0),           T(0),
			T(0), T(0), range, -range * nearZ,
			T(0), T(0),  T(1),           T(0)
			);
	}

	template<typename M0, typename M1, typename M2>
	M44<typename M0::Type> Matrices::LookTo(const Math::Vector<M0> & position, const Math::Vector<M1> & direction, const Math::Vector<M2> & up) {
		static_assert(M0::Length == 3 && M1::Length == 3 && M2::Length == 3, "Vectors must be 3-dimensional");
		static_assert(IsSame<typename M0::Type, typename M1::Type, typename M2::Type>, "Vectors are not compatible.");

		using T = typename M0::Type;

		// z-axis (depth) of camera coordinate system
		const V3<T> & Z = direction;
		// x-axis (width) of camera coordinate system
#if DD_MATRICES_LEFT_HANDED
		const V3<T> X = Math::Normalize(Math::Cross(up, Z));
#else
		const V3<T> X = Math::Normalize(Math::Cross(Z, up));
#endif
		// y-axis (height) of camera coordinate system
#if DD_MATRICES_LEFT_HANDED
		const V3<T> Y = Math::Normalize(Math::Cross(Z, X));
#else
		const V3<T> Y = Math::Normalize(Math::Cross(X, Z));
#endif

		// negative x-coordinate of camera origin in camera coordinate system
		const T x = -Math::Dot(X, position);
		// negative y-coordinate of camera origin in camera coordinate system
		const T y = -Math::Dot(Y, position);
		// negative z-coordinate of camera origin in camera coordinate system
		const T z = -Math::Dot(Z, position);

		return M44<T>(
			X[0], X[1], X[2], x,
			Y[0], Y[1], Y[2], y,
			Z[0], Z[1], Z[2], z,
			T(0), T(0), T(0), T(1)
			);
	}

	/************************************************************************/
	/* MatrixNxN                                                            */
	/************************************************************************/
	template<typename ITERABLE, typename EXTRACT /*= NoExtract*/>
	auto Matrices::LinearRegression(ITERABLE & samples) {
		typedef typename IterableHelper<ITERABLE, EXTRACT>::type_t type_t;
		typedef typename IterableHelper<ITERABLE, EXTRACT>::vector_t vector_t;
		typedef typename IterableHelper<ITERABLE, EXTRACT>::matrix_t matrix_t;
		static constexpr size_t DIM = IterableHelper<ITERABLE, EXTRACT>::DIMENSION;
		static constexpr size_t LAST = IterableHelper<ITERABLE, EXTRACT>::DIMENSION - 1;

		// linear regression matrix
		// xx xy | x
		// xy yy | y
		// ------+---
		//  x  y | n
		matrix_t A(matrix_t::Constants::Zero);
		// xo yo | o
		vector_t b(vector_t::Constants::Zero);
		// number of samples (counted in integers)
		size_t count = 0;

		// sample is assumed to be a Vector<T, N>
		for (const auto & sample : samples) {
			for (size_t i = 0; i < LAST; ++i) {
				// fill matrix (just one triangle)
				A.Rows[i][LAST] += sample[i];
				b[i] += sample[i] * sample[LAST];
				for (size_t j = i; j < LAST; ++j) {
					A.Rows[i][j] += sample[i] * sample[j];
				}
			}
			b[LAST] += sample[LAST];
			++count;
		}

		// fill second triangle of matrix A
		for (size_t i = 0; i < LAST; ++i) {
			for (size_t j = i + 1; j < DIM; ++j) {
				A.Rows[j][i] = A.Rows[i][j];
			}
		}

		A.Rows[LAST][LAST] = (type_t)count;

		// solve Ax = b:
		// A^-1 * b = x
		vector_t x = A.Inversion() * b;
		DD_TODO_STAMP("Solve linear regression for iregular matrices.");

		return x;
	}

	template<typename ITERABLE, typename EXTRACT>
	void Matrices::Covariance(
		ITERABLE & items,
		typename IterableHelper<ITERABLE, EXTRACT>::matrix_t & covariance,
		typename IterableHelper<ITERABLE, EXTRACT>::vector_t & mean
	) {
		// helper for deriving types
		typedef IterableHelper<ITERABLE, EXTRACT> ihelp_t;
		// average point
		mean = ihelp_t::vector_t::Constants::Zero;
		size_t count = 0;
		for (const auto & item : items) {
			mean += EXTRACT::Extract(item);
			++count;
		}
		mean /= ihelp_t::type_t(count);


		// covariance matrix
		covariance = ihelp_t::matrix_t::Constants::Zero;

		// compute just triangle matrix
		for (const auto & item : items) {
			const ihelp_t::vector_t & point = mean - EXTRACT::Extract(item);
			for (size_t i = 0; i < ihelp_t::DIMENSION; ++i) {
				for (size_t j = i; j < ihelp_t::DIMENSION; ++j) {
					covariance.Rows[i][j] += point[i] * point[j];
				}
			}
		}

		covariance /= ihelp_t::type_t(count - 1);

		for (size_t i = 0; i < ihelp_t::DIMENSION; ++i) {
			for (size_t j = i; j < ihelp_t::DIMENSION; ++j) {
				covariance.Rows[j][i] = covariance.Rows[i][j];
			}
		}
	}

	template<typename T, size_t SIZE>
	void Matrices::QRDecomposition(
		const Matrix<T, SIZE> & input,
		Matrix<T, SIZE> & Q,
		Matrix<T, SIZE> & R
	) {
		// array of factor Q1, Q2, ... Qm
		Matrix<T, SIZE> qv[SIZE];
		Matrix<T, SIZE> z(input);
		Matrix<T, SIZE> minor;

		for (size_t k = 0; k < SIZE - 1; ++k) {
			Matrix<T, SIZE> & qvk = qv[k];

			// compute minor
			minor = Matrix<T, SIZE>::Constants::Unit;
			for (size_t i = k; i < SIZE; ++i) {
				for (size_t j = k; j < SIZE; ++j) {
					minor.Cols[i][j] = z.Cols[i][j];
				}
			}

			Vector<T, SIZE> e = minor.Cols[k];
			e[k] += input.Cols[k][k] > T(0) ? -minor.Cols[k].Norm() : minor.Cols[k].Norm();
			if (e != Vector<T, SIZE>::Constants::Zero)
				e = Math::Normalize(e);

			// qv[k] = I - 2 *e*e^T (householder factor)
			for (size_t i = 0; i < SIZE; i++) {
				for (size_t j = 0; j < SIZE; j++) {
					qvk.Cols[i][j] = T(-2) * e[i] * e[j];
				}
			}

			for (size_t i = 0; i < SIZE; i++) {
				qvk.Cols[i][i] += 1;
			}

			z = qvk * minor;
		}

		// make product of householder factors
		Q = qv[0];
		for (size_t i = 1; i < SIZE - 1; i++) {
			Q = qv[i] * Q;
		}

		R = Q * input;
		Q = Q.Transposition();
	}

	template<typename T, size_t SIZE>
	void Matrices::QRAlgorithm(
		Matrix<T, SIZE> & d,
		Matrix<T, SIZE> & v,
		Matrix<T, SIZE> & q,
		Matrix<T, SIZE> & r,
		size_t maxIteration
	) {
		for (size_t i = 0; i < maxIteration; ++i) {
			QRDecomposition(d, q, r);
			d = r * q;
			v *= q;
		}
	}

	template<typename ITERABLE, typename EXTRACT>
	auto Matrices::PCA(ITERABLE & items, const Matrices::PCAParams & params /* = PCAParams() */) {
		// help for getting the types
		typedef IterableHelper<ITERABLE, EXTRACT> ihelp_t;
		// prepare output structure
		PCAOutput<typename ihelp_t::type_t, ihelp_t::DIMENSION> output;
		// get covariance matrix from input points
		Covariance<ITERABLE, EXTRACT>(items, output.Covariance, output.Mean);
		// qr algo eigens
		output.EigenValues = output.Covariance;
		output.EigenVectors = ihelp_t::matrix_t::Constants::Unit;
		output.Convergates = false;
		// copy covariance as input
		typename ihelp_t::matrix_t copy = output.Covariance;
		// qr algo  vectors
		typename ihelp_t::matrix_t q, r;
		// compute
		for (size_t i = 0; i < params.Limit; ++i) {
			// run qr algo batch
			QRAlgorithm(output.EigenValues, output.EigenVectors, q, r, params.Batch);
			// if value is consistent
			if (copy == output.EigenValues) {
				output.Convergates = true;
				break;
			}
			// assign input for next iteration
			copy = output.EigenValues;
		}

		return output;
	}

	template<typename T, size_t ROWS, size_t COLS>
	void Matrices::GaussJordanElimination(Matrix<T, ROWS, COLS> & matrix) {

		static constexpr size_t DiagonalElements = Min(ROWS, COLS);

		// iterate over diagonal
		for (size_t pivotIndex = 0; pivotIndex < DiagonalElements; ++pivotIndex) {
			// try to swap if pivot position (diagonal) is zero
			if (matrix[pivotIndex][pivotIndex] == 0) {
				// guard for propagating information between inner and outer loop
				bool noSwapFound = true;
				// try to find non-zero pivot value between unresolved rows
				for (size_t swapIndex = pivotIndex + 1; swapIndex < ROWS; ++swapIndex) {
					// if non-zero, swap
					if (matrix[swapIndex][pivotIndex] != 0) {
						// update permutation table
						Swap(matrix[pivotIndex], matrix[swapIndex]);
						noSwapFound = false;
						break;
					}
				}
				// no nonzero pivot value found, continue with other rows
				if (noSwapFound) {
					continue;
				}
			}

			// row that is being normalized
			const T pivot = matrix.Diagonal()[pivotIndex];
			// normalize row
			matrix[pivotIndex] /= pivot;

			// eliminate value from other rows
			for (size_t eliminationIndex = 0; eliminationIndex < ROWS; ++eliminationIndex) {
				// do not apply to itself
				if (eliminationIndex == pivotIndex) {
					continue;
				}
				// nothing to eliminate
				if (matrix[eliminationIndex][pivotIndex] == 0) {
					continue;
				}

				// normalization value
				const T norm = matrix[eliminationIndex][pivotIndex];
				matrix[eliminationIndex] -= matrix[pivotIndex] * norm;
			}
		}
	}
}