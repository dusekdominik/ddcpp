#pragma once
namespace DD {
	/**
	 * Static mapping between indices.
	 * @param T    - type of index (typically n-bit integers).
	 * @param SIZE - number of mapped items.
	 */
	template<typename T, T SIZE>
	class Permutation;
}

namespace DD {
	template<typename T, T SIZE>
	class Permutation {
		/** Storage map. */
		T _mapping[SIZE];
	public: // methods
		/** Make transposition of two indices. */
		void Transpose(T i, T j);
		/** Get mapping. */
		T operator()(T i) const { return _mapping[i]; }
	public: // constructors
		/** Constructor - initializes simple mapping n -> n. */
		Permutation();
	};
}

namespace DD {
	template<typename T, T SIZE>
	inline void Permutation<T, SIZE>::Transpose(T i, T j) {
		T swap = _mapping[i];
		_mapping[i] = _mapping[j];
		_mapping[j] = swap;
	}

	template<typename T, T SIZE>
	inline Permutation<T, SIZE>::Permutation() {
		for (T i = 0; i < SIZE; i++)
			_mapping[i] = i;
	}
}