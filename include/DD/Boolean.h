#pragma once
namespace DD {
	/**
	 * Tool for manipulation with boolean.
	 * Example:
	 *   Boolean value0 = Boolean::False;
	 *   value0 = Boolean::Change;
	 *   Boolean value1 = false;
	 *   value1.Switch();
	 */
	struct Boolean;
}

#include <DD/Types.h>

namespace DD {
	struct Boolean {
	public: // nested
		/** Values settable to boolean. */
		enum { False = false, True = true, Change };
		/** Wrap for boolean values. */
		struct Value;
	private: // properties
		/** Storage. */
		i32 _storage;
	public: // methods
		/** Get bool value. */
		bool Get() const { return _storage == True; }
		/** Set bool value. */
		Boolean & Set(bool b) { _storage = b; return *this; }
		/** Set bool value. */
		Boolean & Set(Value v);
		/** Set to different state than actual. */
		Boolean & Switch() { return Set(!_storage); }
		/** Set to true. */
		Boolean & Enable() { return Set(true); }
		/** Set to false. */
		Boolean & Disable() { return Set(false); }
	public: // operators
		/** Implicit conversion to bool. */
		operator bool() const { return Get(); }
		/** Assignment operator. */
		Boolean & operator=(bool b) { return Set(b); }
		/** Assignment operator. */
		Boolean & operator=(Value v);
	public: // constructors
		/** Zero constructor. */
		Boolean() {}
		/** Implicit conversion from bool. */
		Boolean(bool b) : _storage(b) {}
	};

	struct Boolean::Value {
	private: // properties
		/** Value storage. */
		i32 _value;
	public: // operators
		/** Implicit conversion to int. */
		operator i32() { return _value; }
		/** Implicit conversion to int. */
		operator bool() { return _value == True; }
	public: // constructors
		/** Conversion from false, true. */
		Value(bool b) : _value(b) {}
		/** Conversion from Boolean::False, Boolean::True, Boolean::Change. */
		Value(i32 b) : _value(b) {}
		/** Conversion from Boolean. */
		Value(Boolean b) : _value(b) {}
	};
}

namespace DD {
	inline Boolean & Boolean::operator=(Value v) {
		return Set(v);
	}

	inline Boolean & Boolean::Set(Value v) {
		switch ((i32)v) {
		case False: return Disable();
		case True: return Enable();
		case Change: return Switch();
		default: throw;
		};
	}
}
