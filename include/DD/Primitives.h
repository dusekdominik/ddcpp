#pragma once
namespace DD {
	/** Types of intersection. */
	enum Intersetion { NONE, ALL_IN, PART_IN };

	/**
	 * Center-Eccentricity-Orientation 3D oriented bounding box.
	 * @param T - numeric type of box.
	 * @param D - dimension of box.
	 */
	template<typename T, size_t D>
	struct OBox;

	/** Oriented bounding box in 3D. */
	template<typename T>
	using OBox3 = OBox<T, 3>;
	/** 32-bit oriented bounding box in 3D. */
	using OBox3F = OBox3<float>;
	/** 64-bit oriented bounding box in 3D. */
	using OBox3D = OBox3<double>;

	/** Oriented bounding box in 2D. */
	template<typename T>
	using OBox2 = OBox<T, 2>;
	/** 32-bit oriented bounding box in 2D. */
	using OBox2F = OBox2<float>;
	/** 64-bit oriented bounding box in 2D. */
	using OBox2D = OBox2<double>;
}

#include "Vector.h"
#include "Matrix.h"
#include "Math.h"
#include <DD/Math/HyperPlane.h>
#include <DD/Math/HyperSphere.h>
#include <DD/Math/LineSegment.h>
#include <DD/Math/Paraboloid.h>
#include <DD/Math/Polytope.h>
#include <DD/Math/Interval.h>
#include <DD/Units/Units.h>

namespace DD {
	// forward older representation for temporal compatibility
	template<typename T>
	using Plane3 = Math::Plane3<T>;
	using Plane3F = Math::Plane3F;
	using Plane3D = Math::Plane3D;
	template<typename T>
	using Ray3 = Math::Ray3<T>;
	template<typename T>
	using Sphere3 = Math::Sphere3<T>;
	//using Sphere3 = Math::Sphere3<Units::Length_Meter<T>>;
	using Sphere3F = Sphere3<f32>;
	using Sphere3D = Sphere3<f64>;
	template<typename T>
	using AABox2 = Math::Interval2<T>;
	using AABox2I = Math::Interval2I;
	using AABox2F = Math::Interval2F;
	using AABox2D = Math::Interval2D;
	template<typename T>
	using AABox3 = Math::Interval<T, 3>;
	using AABox3F = Math::Interval3F;
	using AABox3D = Math::Interval3D;
	template<typename T>
	using Frustum3 = Math::Polytope<T, 3, 6>;
	using Frustum3F = Frustum3<float>;
	using Frustum3D = Frustum3<double>;

	template<typename T>
	struct OBox<T, 3> {
	public: // nested
		/**
		 *   H--------G
		 *  /|       /|
		 * E--------F |
		 * | |      | |
		 * | D------|-C
		 * |/       |/
		 * A--------B
		 */
		struct VertexCollection {
			V3<T> A, B, C, D, E, F, G, H;
			V3<T> * begin() { return &A; }
			V3<T> * end() { return &A + 8; }
		};
	public: // properties
		/** Coordinate system of OBox. */
		M33<T> Rotation;
		/** Position of center of OBox. */
		V3<T> Position;
		/** Half size of obox (or scale of unit box). */
		V3<T> Radius;
	public: // methods
		/**
		 * Get local space coordinate of given global-space coordinate.
		 * Inner space of obox is (-1, -1, -1)...(+1, +1, +1), where
		 * (-1, -1, -1) is Position - Rotation * Radius and
		 * (+1, +1, +1) is Position + Rotation * Radius.
		 * @param gSpace - coordinates in global space.
		 */
		template<typename M>
		V3<T> LocalSpace(const V3<T, M> & gSpace) { return ((gSpace - Position) * Rotation) / Radius; }
		/**
		 * Get global space coordinate from given local-space coordinate.
		 * Inner space of obox is (-1, -1, -1)...(+1, +1, +1), where
		 * (-1, -1, -1) is Position - Rotation * Radius and
		 * (+1, +1, +1) is Position + Rotation * Radius.
		 * @param lSpace - coordinates in local space of obox.
		 */
		template<typename M>
		V3<T> GlobalSpace(const V3<T, M> & lSpace) { return (Rotation * (lSpace * Radius)) + Position; }
		/** Get bounding box as prism vertices. */
		VertexCollection Vertices() const;
		/** Check if obb contains a point. */
		template<typename M>
		bool Contains(const V3<T, M> & point) const;
		/** Get AABox from Obox. */
		AABox3<T> AABox() const;
	public: // constructors
		/** Constructor. */
		template<typename M1, typename M2>
		OBox(const V3<T, M1> & position, const V3<T, M2> & radius, const M33<T> & rotation);
		/** Constructor. */
		template<typename M1, typename M2>
		OBox(const V3<T, M1> & position, const V3<T, M2> & radius)
			: OBox(position, radius, DiagonalMatrix<T, 3>(Math::Constants<T>::One))
		{}
		/** Constructor. */
		template<typename M1>
		OBox(const V3<T, M1> & position) : OBox(position, V3<T>(1)) {}
		/** Zero constructor. */
		OBox() : OBox(V3<T>(0)) {}
	};

	template<typename T>
	struct OBox<T, 2> {
	public: // nested
		/**
		 *   D-------C
		 *  /   P   /
		 * A-------B
		 */
		struct VertexCollection {
			V2<T> A, B, C, D;
			V2<T> * begin() { return &A; }
			V2<T> * end() { return &A + 4; }
		};
	public: // properties
		/** Position of center of bounding square. */
		V2<T> Position;
		/** Half size of bounding square. */
		V2<T> Radius;
		/** Rotation of bounding square in unit size (cos, sin). */
		V2<T> Rotation;
	public: // methods
		/** Set rotation in radians. */
		void Angle(const T & radians) { Rotation.Set(Math::Cos(radians), Math::Sin(radians)); }
		/** Get rotation in radians. */
		T Angle() const { return Math::ArcTg2(Rotation.y, Rotation.x); }
		/** Rotate by angle in radians. */
		void Rotate(const T & angle) { Angle(Angle() + angle); }
		/** Get vertices of OBB. */
		VertexCollection Vertices() const;
		/** Compute surface of obox area. */
		T Surface() const { return T(4) * Radius.x * Radius.y; }
		/**
		 * Get local space coordinate of given global-space coordinate.
		 * Inner space of obox is (-1, -1)...(+1, +1), where
		 * (-1, -1) is Position - Rotation * Radius and
		 * (+1, +1) is Position + Rotation * Radius.
		 * @param gSpace - coordinates in global space.
		 */
		template<typename M>
		V2<T> LocalSpace(const V2<T, M> & gSpace) { return ((gSpace - Position) * Matrices::Rotation(Rotation)) / Radius; }
		/**
		 * Get global space coordinate from given local-space coordinate.
		 * Inner space of obox is (-1, -1)...(+1, +1), where
		 * (-1, -1) is Position - Rotation * Radius and
		 * (+1, +1) is Position + Rotation * Radius.
		 * @param lSpace - coordinates in local space of obox.
		 */
		template<typename M>
		V2<T> GlobalSpace(const V2<T, M> & lSpace) { return (Matrices::Rotation(Rotation) * (lSpace * Radius)) + Position; }

	public: // constructors
		/**
		 * Constructor.
		 * @param position - center of box.
		 * @param radius   - half size of box.
		 * @param angle    - angle in radians.
		 */
		template<typename M0, typename M1>
		OBox(const V2<T, M0> & position, const V2<T, M1> & radius, const T & angle);
		/**
		 * AABB Constructor - no angle input.
		 * @param position - center of box.
		 * @param radius   - half size of box.
		 */
		template<typename M0, typename M1>
		OBox(const V2<T, M0> & position, const V2<T, M1> & radius);
		/** Constructor. */
		OBox(const AABox2<T> & aabb);
		/** Constructor. */
		OBox(const AABox2<T> & aabb, const T & angle);
		/** Zero constructor. */
		OBox() {}
	};
}

namespace DD {
	/************************************************************************/
	/* OBB                                                                  */
	/************************************************************************/
	template<typename T>
	inline typename OBox<T, 2>::VertexCollection OBox<T, 2>::Vertices() const {
		const V2<T> & scaledX = Rotation * Radius;
		const V2<T> & scaledY = V2<T>(Rotation.y, -Rotation.x) * Radius;
		const V2<T> & pp = scaledX + scaledY;
		const V2<T> & np = scaledX - scaledY;

		VertexCollection vertices;
		vertices.A = Position - pp;
		vertices.B = Position + np;
		vertices.C = Position + pp;
		vertices.D = Position - np;
		return vertices;
	}

	template<typename T>
	template<typename M0, typename M1>
	OBox<T, 2>::OBox(const V2<T, M0> & position, const V2<T, M1> & radius, const T & angle)
		: Position(position)
		, Radius(radius)
		, Rotation(Math::Cos(angle), Math::Sin(angle))
	{}

	template<typename T>
	template<typename M0, typename M1>
	OBox<T, 2>::OBox(const V2<T, M0> & position, const V2<T, M1> & radius)
		: Position(position)
		, Radius(radius)
		, Rotation(T(1), T(0))
	{}

	template<typename T>
	OBox<T, 2>::OBox(const AABox2<T> & aabb)
		: OBox(aabb.Center(), aabb.Size() * T(0.5))
	{}

	template<typename T>
	OBox<T, 2>::OBox(const AABox2<T> & aabb, const T & angle)
		: OBox(aabb.Center(), aabb.Size() * T(0.5), angle)
	{}

	template<typename T>
	inline typename OBox<T, 3>::VertexCollection OBox<T, 3>::Vertices() const {
		VertexCollection result;
		result.A.Set(-Radius.x, -Radius.y, -Radius.z);
		result.B.Set(+Radius.x, -Radius.y, -Radius.z);
		result.C.Set(+Radius.x, -Radius.y, +Radius.z);
		result.D.Set(-Radius.x, -Radius.y, +Radius.z);
		result.E.Set(-Radius.x, +Radius.y, -Radius.z);
		result.F.Set(+Radius.x, +Radius.y, -Radius.z);
		result.G.Set(+Radius.x, +Radius.y, +Radius.z);
		result.H.Set(-Radius.x, +Radius.y, +Radius.z);
		for (V3<T> & vertex : result) {
			vertex = Position + Rotation * vertex;
		}
		return result;
	}

	template<typename T>
	template<typename M>
	inline bool OBox<T, 3>::Contains(const V3<T, M> & point) const {
		// remove offset
		const V3<T> & removedOffset = point - Position;
		// inv transformation is by transposition => multiplication from right
		const V3<T> & removedRotation = removedOffset * Rotation;
		// now we check just aabb
		return Math::Abs(removedRotation) <= Radius;
	}

	template<typename T>
	AABox3<T> OBox<T, 3>::AABox() const
	{
		AABox3<T> box;
		for (const V3<T> & v : Vertices())
			box.Include(v);

		return box;
	}

	template<typename T>
	template<typename M1, typename M2>
	inline OBox<T, 3>::OBox(const V3<T, M1> & position, const V3<T, M2> & halfSize, const M33<T> & rotation)
		: Rotation(rotation)
		, Position(position)
		, Radius(halfSize)
	{}

	/************************************************************************/
	/* Intersections                                                        */
	/************************************************************************/
	template<typename T0, typename T1, size_t D>
	bool operator*(const Math::Interval<T0, D> & a, const Math::Interval<T1, D> & b) {
		return Math::Interval<T0, D>::Intersects(a, b);
	}

	template<typename T>
	bool operator*(const Sphere3<T> & s1, const Sphere3<T> & s2) {
		const V3<T> diff = s1 - s2;
		return dot(diff, diff) < ((s1.Radius + s2.Radius) * (s1.Radius + s2.Radius));
	}

	template<typename T>
	bool operator*(const OBox3<T> & a, const OBox3<T> & b) {
		// http://www.jkh.me/files/tutorials/Separating%20Axis%20Theorem%20for%20Oriented%20Bounding%20Boxes.pdf
		const V3<T> & t = b.Position - a.Position;
		const M33<T> & A = a.Rotation;
		const M33<T> & B = b.Rotation;
		const M33<T> & R = B * A;

		// Ax
		const T & tAx = dot(t, A.Cols(0));
		if (Math::Abs(tAx) > (a.Radius.x + Math::Abs(b.Radius.x * R.Cols(0)[0]) + Math::Abs(b.Radius.y * R.Cols(0)[1]) + Math::Abs(b.Radius.z * R.Cols(0)[2])))
			return false;

		// Ay
		const T & tAy = dot(t, A.Cols(1));
		if (Math::Abs(tAy) > (a.Radius.y + Math::Abs(b.Radius.x * R.Cols(1)[0]) + Math::Abs(b.Radius.y * R.Cols(1)[1]) + Math::Abs(b.Radius.z * R.Cols(1)[2])))
			return false;

		// Az
		const T & tAz = dot(t, A.Cols(2));
		if (Math::Abs(tAz) > (a.Radius.z + Math::Abs(b.Radius.x * R.Cols(2)[0]) + Math::Abs(b.Radius.y * R.Cols(2)[1]) + Math::Abs(b.Radius.z * R.Cols(2)[2])))
			return false;

		// Bx
		const T & tBx = dot(t, B.Cols(0));
		if (Math::Abs(tBx) > (b.Radius.x + Math::Abs(a.Radius.x * R.Cols(0)[0]) + Math::Abs(a.Radius.y * R.Cols(1)[0]) + Math::Abs(a.Radius.z * R.Cols(2)[0])))
			return false;

		// By
		const T & tBy = dot(t, B.Cols(1));
		if (Math::Abs(tBy) > (b.Radius.y + Math::Abs(a.Radius.x * R.Cols(0)[1]) + Math::Abs(a.Radius.y * R.Cols(1)[1]) + Math::Abs(a.Radius.z * R.Cols(2)[1])))
			return false;

		// Bz
		const T & tBz = dot(t, B.Cols(2));
		if (Math::Abs(tBz) > (b.Radius.z + Math::Abs(a.Radius.x * R.Cols(0)[2]) + Math::Abs(a.Radius.y * R.Cols(1)[2]) + Math::Abs(a.Radius.z * R.Cols(2)[2])))
			return false;

		// Ax x Bx
		const T & left07 = Math::Abs(tAz * R.Cols(1)[0] - tAy * R.Cols(2)[0]);
		if (left07 > (Math::Abs(a.Radius.y * R.Cols(2)[0]) + Math::Abs(a.Radius.z * R.Cols(1)[0]) + Math::Abs(b.Radius.y * R.Cols(0)[2]) + Math::Abs(b.Radius.z * R.Cols(0)[1])))
			return false;

		// Ax x By
		const T & left08 = Math::Abs(tAz * R.Cols(1)[1] - tAy * R.Cols(2)[1]);
		if (left08 > (Math::Abs(a.Radius.y * R.Cols(2)[1]) + Math::Abs(a.Radius.z * R.Cols(1)[1]) + Math::Abs(b.Radius.x * R.Cols(0)[2]) + Math::Abs(b.Radius.z * R.Cols(0)[0])))
			return false;

		// Ax x Bz
		const T & left09 = Math::Abs(tAz * R.Cols(1)[2] - tAy * R.Cols(2)[2]);
		if (left09 > (Math::Abs(a.Radius.y * R.Cols(2)[2]) + Math::Abs(a.Radius.z * R.Cols(1)[2]) + Math::Abs(b.Radius.x * R.Cols(0)[1]) + Math::Abs(b.Radius.y * R.Cols(0)[0])))
			return false;

		// Ay x Bx
		const T & left10 = Math::Abs(tAx * R.Cols(2)[0] - tAz * R.Cols(0)[0]);
		if (left10 > (Math::Abs(a.Radius.x * R.Cols(2)[0]) + Math::Abs(a.Radius.z * R.Cols(0)[0]) + Math::Abs(b.Radius.y * R.Cols(1)[2]) + Math::Abs(b.Radius.z * R.Cols(1)[1])))
			return false;

		// Ay x By
		const T & left11 = Math::Abs(tAx * R.Cols(2)[1] - tAz * R.Cols(0)[1]);
		if (left11 > (Math::Abs(a.Radius.x * R.Cols(2)[1]) + Math::Abs(a.Radius.z * R.Cols(0)[1]) + Math::Abs(b.Radius.x * R.Cols(1)[2]) + Math::Abs(b.Radius.z * R.Cols(1)[0])))
			return false;

		// Ay x Bz
		const T & left12 = Math::Abs(tAx * R.Cols(2)[2] - tAz * R.Cols(0)[2]);
		if (left12 > (Math::Abs(a.Radius.x * R.Cols(2)[2]) + Math::Abs(a.Radius.z * R.Cols(0)[2]) + Math::Abs(b.Radius.x * R.Cols(1)[1]) + Math::Abs(b.Radius.y * R.Cols(1)[0])))
			return false;

		// Az x Bx
		const T & left13 = Math::Abs(tAy * R.Cols(0)[0] - tAx * R.Cols(1)[0]);
		if (left13 > (Math::Abs(a.Radius.x * R.Cols(1)[0]) + Math::Abs(a.Radius.y * R.Cols(0)[0]) + Math::Abs(b.Radius.y * R.Cols(2)[2]) + Math::Abs(b.Radius.z * R.Cols(2)[1])))
			return false;

		// Az x By
		const T & left14 = Math::Abs(tAy * R.Cols(0)[1] - tAx * R.Cols(1)[1]);
		if (left14 > (Math::Abs(a.Radius.x * R.Cols(1)[1]) + Math::Abs(a.Radius.y * R.Cols(0)[1]) + Math::Abs(b.Radius.x * R.Cols(2)[2]) + Math::Abs(b.Radius.z * R.Cols(2)[0])))
			return false;

		// Az x Bz
		const T & left15 = Math::Abs(tAy * R.Cols(0)[2] - tAx * R.Cols(1)[2]);
		if (left15 > (Math::Abs(a.Radius.x * R.Cols(1)[2]) + Math::Abs(a.Radius.y * R.Cols(0)[2]) + Math::Abs(b.Radius.x * R.Cols(2)[1]) + Math::Abs(b.Radius.y * R.Cols(2)[0])))
			return false;

		return true;
	}

	template<typename T>
	bool operator*(const OBox2<T> & a, const OBox2<T> & b) {
		// r(0) = (cos, sin) = (1, 0)
		// using separating axis theorem (SAT)
		const V2<T> & t = b.Position - a.Position;
		const V2<T> & aXRot = a.Rotation;
		const V2<T> & bXRot = b.Rotation;
		const V2<T> aYRot(-aXRot.y, aXRot.x);
		const V2<T> bYRot(-bXRot.y, bXRot.x);
		const V2<T> & bXRotScaled = bXRot * b.Radius.x;
		const V2<T> & bYRotScaled = bYRot * b.Radius.y;
		T left, right;

		left = Math::Abs(dot(t, aXRot));
		right = a.Radius.x + Math::Abs(dot(bXRotScaled, aXRot)) + Math::Abs(dot(bYRotScaled, aXRot));

		if (left > right)
			return false;

		left = Math::Abs(dot(t, aYRot));
		right = a.Radius.y + Math::Abs(dot(bXRotScaled, aYRot)) + Math::Abs(dot(bYRotScaled, aYRot));
		if (left > right)
			return false;

		const V2<T> & aXRotScaled = aXRot * a.Radius.x;
		const V2<T> & aYRotScaled = aYRot * a.Radius.y;

		left = Math::Abs(dot(t, bXRot));
		right = b.Radius.x + Math::Abs(dot(aXRotScaled, bXRot)) + Math::Abs(dot(aYRotScaled, bXRot));
		if (left > right)
			return false;

		left = Math::Abs(dot(t, bYRot));
		right = b.Radius.y + Math::Abs(dot(aXRotScaled, bYRot)) + Math::Abs(dot(aYRotScaled, bYRot));
		if (left > right)
			return false;

		return true;
	}

	template<typename T>
	bool operator*(const Sphere3<T> & s, const Ray3<T> & r) {
		return (dot(cross(r.Direction, r.Position - s.Position)) / dot(r.Direction)) < (s.Radius * s.Radius);
	}

	template<typename T>
	bool operator*(const Ray3<T> & r, const Sphere3<T> & s) { return s * r; }

	/**
	 * Tells whether or not b is intersecting f.
	 * @param f Viewing frustum.
	 * @param b An axis aligned bounding box.
	 * @return True if b intersects f, false otherwise.
	 */
	template<typename T>
	bool operator*(const Frustum3<T> & f, const AABox3<T> & b)
	{
		// Indexed for the 'index trick' later
		const V3<T> box[] = { b.Min, b.Max };

		// We only need to do 6 point-plane tests
		for (const Plane3<T> & p : f.Planes)
		{
			// p-vertex selection (with the index trick)
			// According to the plane normal we can know the
			// indices of the positive vertex
			const int px = static_cast<int>(p.a > 0.0);
			const int py = static_cast<int>(p.b > 0.0);
			const int pz = static_cast<int>(p.c > 0.0);

			// Dot product: project p-vertex on plane normal
			// (How far is p-vertex from the origin)
			const T dp = (p.a*box[px].x) + (p.b*box[py].y) + (p.c*box[pz].z);

			// Doesn't intersect if it is behind the plane
			if (dp < -p.d) { return false; }
		}
		return true;
	}

	template<typename T>
	Intersetion operator*(const Frustum3<T> & frustum, const Sphere3<T> & sphere) {
		Intersetion result = ALL_IN; // ASUME IN
		for (const Plane3<T> & plane : frustum.Planes) {
			const T dist = (Math::Dot(plane.Normal, sphere.Center) + plane.Distance) / Math::Sqrt(Math::Dot(plane.Normal));
			if (dist < -sphere.Radius) {
				return NONE; // OUT
			}
			if (Math::Abs(dist) < sphere.Radius) {
				result = PART_IN; // INTERSECT
			}
		}
		return result; // IN OR INTERSECT
	}
}