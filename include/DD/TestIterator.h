#pragma once
namespace DD {
	template<typename ITERATOR, typename ITEM, bool TEST(const ITEM&)>
	struct TestIterator {
		ITERATOR _state;
		ITERATOR _end;
		ITEM & operator*() { return *_state; }
		ITEM * operator->() { return &(*_state); }
		void check() { while (!over() && !valid()) ++_state; }
		bool over()const { return _state == _end; }
		bool valid()const { return TEST(*_state); }
		bool operator==(const TestIterator & i)const { return _state == i._state; }
		bool operator!=(const TestIterator & i)const { return _state != i._state; }
		TestIterator & operator++() { if (!over()) { ++_state; check(); } return *this; }
		TestIterator(ITERATOR state, ITERATOR end) :_state(state), _end(end) { check(); }
	};

	template<typename BASE, typename TEST_ITERATOR>
	struct TestIteratorInvoker {
		BASE & _base;
		TEST_ITERATOR begin() { return TEST_ITERATOR(_base.begin(), _base.end()); }
		TEST_ITERATOR end() { return TEST_ITERATOR(_base.end(), _base.end()); }
		/** Referential constructor - !!! no copy constructors.*/
		TestIteratorInvoker(BASE & base) :_base(base) {}
		TestIteratorInvoker(const BASE & base) :_base(base) {}
	};
}