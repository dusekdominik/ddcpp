﻿#pragma once
namespace DD {
	/**
	 * Support vector machine
	 * Based on SMO algorithm introduced by John Platt in 1998.
	 * https://www.microsoft.com/en-us/research/publication/sequential-minimal-optimization-a-fast-algorithm-for-training-support-vector-machines/
	 *
	 * Kernels:
	 *  SVMKernelLinear
	 *	SVMKernelPolynom
	 *	SVMKernelRadialBasis
	 *
	 * @example
	 *	SVMTrainingSet<float, 2> ts;
	 *	ts.Add({{0.f, 0.f}, 1});
	 *	ts.Add({{1.f, 1.f}, 1});
	 *	ts.Add({{0.f, 1.f}, -1});
	 *	ts.Add({{1.f, 2.f}, -1});
	 *
	 *	auto SVM = SVMTrain(SVMLinearKernel(), ts);
	 *
	 *	float classified0 = SVM({10.f, 10.f});
	 *	float classified1 = SVM({-10.f, -10.f});
	 */
	template<typename VECTOR_TYPE, typename FLOAT_TYPE, typename KERNEL_TYPE>
	class SVM;

	template<typename VECTOR_TYPE, typename FLOAT_TYPE>
	struct ISVMPair;

	template<typename V, typename F, typename STREAM>
	STREAM & operator<<(STREAM & stream, const ISVMPair<V, F> & pair);

	template<typename V, typename F, typename STREAM>
	STREAM & operator>>(STREAM & stream, ISVMPair<V, F> & pair);

	template<typename V, typename F, typename K, typename STREAM>
	STREAM & operator<<(STREAM & stream, const SVM<V, F, K> svm);

	template<typename V, typename F, typename K, typename STREAM>
	STREAM & operator>>(STREAM & stream, SVM<V, F, K> svm);
}

#include <random>
#include "List.h"
#include "Vector.h"
#include "Debug.h"
#include "Properties.h"

#define SVM_TOLERANCE 1e-6

namespace DD {
	/** SVM Training pair {vector x, int y}. */
	template<typename VECTOR_TYPE, typename FLOAT_TYPE>
	struct ISVMPair {
		/** +, -, *, dot */
		typedef VECTOR_TYPE vector_t;
		/** +, -, *, pow, exp */
		typedef FLOAT_TYPE float_t;
	public: // properties
		/** Input. */
		vector_t x;
		/** Output - { -1, 1 }. */
		int y;

		DD_PROPERTIES(PRIVATE(x), PRIVATE(y))
	};

	/** Standard SVM training pair based on DD::Vector. Define type and input size. */
	template<typename FLOAT_TYPE, size_t VECTOR_SIZE>
	using SVMPair = ISVMPair<Vector<FLOAT_TYPE, VECTOR_SIZE>, FLOAT_TYPE>;

	/** Training set. Use Add method to insert a training pair. */
	template<typename VECTOR_TYPE, typename FLOAT_TYPE>
	using ISVMTrainingSet = List<ISVMPair<VECTOR_TYPE, FLOAT_TYPE>>;

	/** Standard training set. Use Add method to insert a training pair. */
	template<typename FLOAT_TYPE, size_t VECTOR_SIZE>
	using SVMTrainingSet = ISVMTrainingSet<Vector<FLOAT_TYPE, VECTOR_SIZE>, FLOAT_TYPE>;

	/** SVM kernel based on linear function. */
	struct SVMKernelLinear {
		template<typename VECTOR>
		auto operator() (const VECTOR & v1, const VECTOR & v2) { return dot(v1, v2); }
	};

	/** SVM kernel based on polynomial function. */
	struct SVMKernelPolynom {
		float Degree = 8, Offset = 1;
		template<typename VECTOR>
		auto operator() (const VECTOR & u, const VECTOR & v) { return pow(dot(u, v) + Offset, Degree); }
	};

	/** SVM kernel based on Gaussian, so called radial basis kernel. */
	struct SVMKernelRadialBasis {
		/** Sigma2Neg = -2 * Sigma * Sigma */
		float Sigma2Neg = -2 * 4 * 4;
		template<typename VECTOR>
		auto operator() (const VECTOR & u, const VECTOR & v) { return exp((dot(u, u) + dot(v, v) - 2 * dot(u, v)) / Sigma2Neg); }

		DD_PROPERTIES(PRIVATE(Sigma2Neg))
	};

	/** Helper method - template-arg-free constructor helper. */
	template<typename VECTOR_TYPE, typename FLOAT_TYPE, typename KERNEL_TYPE>
	SVM<VECTOR_TYPE, FLOAT_TYPE, KERNEL_TYPE> SVMTrain(
		KERNEL_TYPE kernel,
		ISVMTrainingSet<VECTOR_TYPE, FLOAT_TYPE> & trainingSet,
		FLOAT_TYPE epsilon = 1E-6,
		FLOAT_TYPE C = 1,
		size_t iterationLimit = 1E6
	) {
		SVM<VECTOR_TYPE, FLOAT_TYPE, KERNEL_TYPE> svm(kernel, epsilon, C, iterationLimit);
		svm.Train(trainingSet);
		return svm;
	}

	template<typename VECTOR_TYPE, typename FLOAT_TYPE, typename KERNEL_TYPE>
	class SVM {
	public: // typedefs
		typedef ISVMTrainingSet<VECTOR_TYPE, FLOAT_TYPE> trainingset_t;
		typedef ISVMPair<VECTOR_TYPE, FLOAT_TYPE> pair_t;
		typedef typename pair_t::vector_t vector_t;
		typedef typename pair_t::float_t float_t;
		typedef KERNEL_TYPE kernel_t;
	protected: // static methods
		static size_t random();
		static void saturate(float_t & value, const float_t & min, const float_t & max);
	protected: // properties
		trainingset_t _trainingSet;
		Array<float_t> _alpha, _error;
		mutable kernel_t _kernel;
		float_t _c, _epsilon, _b, _deltaB;
		size_t _iterationLimit;
	public: // serialization
		DD_PROPERTIES(
			PROTECTED_SET(_kernel, Kernel),
			PROTECTED_SET(_trainingSet, TrainingSet),
			PROTECTED_SET(_alpha, Alpha),
			PROTECTED_SET(_error, Error),
			PROTECTED_SET(_b, B),
			PROTECTED_SET(_c, C),
			PROTECTED_SET(_deltaB, DeltaB),
			PROTECTED_SET(_epsilon, Epsilon),
			PROTECTED_SET(_iterationLimit, IterationLimit)
		);
	private: // methods - index helpers
		/** Index based methods - returns i-th training vectors. */
		int y(size_t i) const { return _trainingSet[i].y; }
		/** Index based methods - returns i-th training vectors. */
		const vector_t & x(size_t i) const { return _trainingSet[i].x; }
		/** Index based methods - returns alpha coefficient of i-th training vectors. */
		float_t & alpha(size_t i) { return _alpha[i]; }
		/** Index based methods - returns alpha coefficient of i-th training vectors. */
		const float_t & alpha(size_t i) const { return _alpha[i]; }
		/** Index based methods - returns kernel of i-th and j-th training vectors. */
		float_t kernel(size_t i, size_t j) const { return _kernel(x(i), x(j)); }
		/** Index based methods - returns count of training vectors. */
		size_t count() const { return _trainingSet.Size(); }
		/** Index based methods - returns index of random training vector. */
		size_t randomIndex() { return random() % count(); }
	protected:
		/** Computes output of k-th training vector. */
		float_t compute(size_t k) const;
		/** Platt's algorithm. */
		int examineSample(size_t i1);
		/** Solve QP optimization. */
		int optimizationQP(size_t i1, size_t i2);
	public: // methods
		/** SMO algorithm. */
		void Train(const trainingset_t & ts);
		/** Computes objective function - could be used for observing if data are changed. */
		float_t Objective() const;
		/** Computes SVM output. */
		float_t operator()(const vector_t & v) const;
		/** Ratio of errors : all samples */
		float_t ErrorRatio() const;
		/** Serialization. */
		template<typename STREAM>
		void Serialize(STREAM & stream) { stream << _c << _epsilon << _b << _deltaB << _alpha << _error << _trainingSet; }
		/** Deserialization. */
		void Deserialize(Array<char> & input) { stream >> _c >> _epsilon >> _b >> _deltaB >> _alpha >> _error >> _trainingSet; }
	public: //constructors
		/** Constructor. */
		SVM(kernel_t kernel, float_t epsilon = 1E-6, float_t c = 1, size_t iterationLimit = 1e6);
	};
}

namespace DD {
	template<typename VECTOR_TYPE, typename FLOAT_TYPE, typename KERNEL_TYPE>
	size_t SVM<VECTOR_TYPE, FLOAT_TYPE, KERNEL_TYPE>::random() {
		static std::uniform_int_distribution<size_t> distribution(0, 1 << 20);
		static std::default_random_engine generator;
		return distribution(generator);
	}

	template<typename VECTOR_TYPE, typename FLOAT_TYPE, typename KERNEL_TYPE>
	void SVM<VECTOR_TYPE, FLOAT_TYPE, KERNEL_TYPE>::saturate(float_t & value, const float_t & min, const float_t & max) {
		if (value < min) {
			value = min;
		}
		else if (value > max) {
			value = max;
		}
	}

	/** Computes output of k-th training vector. */
	template<typename VECTOR_TYPE, typename FLOAT_TYPE, typename KERNEL_TYPE>
	typename SVM<VECTOR_TYPE, FLOAT_TYPE, KERNEL_TYPE>::float_t SVM<VECTOR_TYPE, FLOAT_TYPE, KERNEL_TYPE>::compute(size_t k) const {
		float_t result = 0;
		for (int i = 0; i < count(); i++) {
			if (alpha(i) > 0) {
				result += alpha(i) * y(i) * kernel(i, k);
			}
		}
		result -= _b;
		return result;
	}

	/** Platt's algorithm. */
	template<typename VECTOR_TYPE, typename FLOAT_TYPE, typename KERNEL_TYPE>
	int SVM<VECTOR_TYPE, FLOAT_TYPE, KERNEL_TYPE>::examineSample(size_t i1) {
		int y1 = y(i1);
		float_t alpha1 = alpha(i1);
		float_t e1 = alpha1 > 0 && alpha1 < _c ? _error[i1] : compute(i1) - y1;
		float_t r1 = y1 * e1;
		if ((r1 < -SVM_TOLERANCE && alpha1 < _c) || (r1 > SVM_TOLERANCE && alpha1 > 0)) {
			int k0 = 0, k = 0, i2 = -1;
			float_t tmax = 0;
			for (i2 = -1, tmax = 0, k = 0; k < count(); k++) {
				if (_alpha[k] > 0 && _alpha[k] < _c) {
					float_t e2 = _error[k];
					float_t temp = fabs(e1 - e2);
					if (temp > tmax) {
						tmax = temp;
						i2 = k;
					}
				}
				if (i2 >= 0) {
					if (optimizationQP(i1, i2)) {
						return 1;
					}
				}
			}
			for (k0 = (int)randomIndex(), k = k0; k < count() + k0; k++) {
				i2 = k % count();
				if (_alpha[i2] > 0 && _alpha[i2] < _c) {
					if (optimizationQP(i1, i2)) {
						return 1;
					}
				}
			}
			for (k0 = (int)randomIndex(), k = k0; k < count() + k0; k++) {
				i2 = k % count();
				if (optimizationQP(i1, i2)) {
					return 1;
				}
			}
		}
		return 0;
	}

	/** Solve QP optimization. */
	template<typename VECTOR_TYPE, typename FLOAT_TYPE, typename KERNEL_TYPE>
	int SVM<VECTOR_TYPE, FLOAT_TYPE, KERNEL_TYPE>::optimizationQP(size_t i1, size_t i2) {
		int y1 = y(i1);
		int y2 = y(i2);
		if (i1 == i2)
			return 0;

		int s = y1 * y2;
		float_t alpha1 = alpha(i1);
		float_t alpha2 = alpha(i2);
		float_t e1 = alpha1 > 0 && alpha1 < _c ? _error[i1] : compute(i1) - y1;
		float_t e2 = alpha2 > 0 && alpha2 < _c ? _error[i2] : compute(i2) - y2;
		float_t low, high;

		// set H,L
		if (y1 == y2) {
			float_t gamma = alpha1 + alpha2;
			if (gamma > _c) {
				low = gamma - _c;
				high = _c;
			}
			else {
				low = 0;
				high = gamma;
			}
		}
		else {
			float_t gamma = alpha1 - alpha2;
			if (gamma > 0) {
				low = 0;
				high = _c - gamma;
			}
			else {
				low = -gamma;
				high = _c;
			}
		}

		if (abs(low - high) < SVM_TOLERANCE) {
			return 0;
		}

		float_t k11 = kernel(i1, i1);
		float_t k12 = kernel(i1, i2);
		float_t k22 = kernel(i2, i2);
		float_t eta = 2 * k12 - k11 - k22;
		float_t a1, a2;

		// set a1, a2
		if (eta < 0) {
			a2 = alpha2 + y2 * (e2 - e1) / eta;
			saturate(a2, low, high);
		}
		else {
			float_t c1 = eta / 2;
			float_t c2 = y2 * (e1 - e2) - eta * alpha2;
			float_t low_obj = c1 * low * low + c2 * low;
			float_t high_obj = c1 * high * high + c2 * high;
			if (low_obj > high_obj + _epsilon) {
				a2 = low;
			}
			else if (low_obj < high_obj - _epsilon) {
				a2 = high;
			}
			else {
				a2 = alpha2;
			}
		}

		if (abs(a2 - alpha2) < _epsilon * (a2 + alpha2 + _epsilon)) {
			return 0;
		}

		a1 = alpha1 - s * (a2 - alpha2);
		if (a1 < 0) {
			a2 += s * a1;
			a1 = 0;
		}
		else if (a1 > _c) {
			a2 += s * (a1 - _c);
			a1 = _c;
		}

		// update bias and alphas
		float_t bnew = 0;
		if (a1 > 0 && a1 < _c) {
			bnew = _b + e1 + y1 * (a1 - alpha1) * k11 + y2 * (a2 - alpha2) * k12;
		}
		else if (a2 > 0 && a2 < _c) {
			bnew = _b + e2 + y1 * (a1 - alpha1) * k12 + y2 * (a2 - alpha2) * k22;
		}
		else {
			float_t b1 = _b + e1 + y1 * (a1 - alpha1) * k11 + y2 * (a2 - alpha2) * k12;
			float_t b2 = _b + e2 + y1 * (a1 - alpha1) * k12 + y2 * (a2 - alpha2) * k22;
			bnew = (b1 + b2) / 2;
		}

		_deltaB = bnew - _b;
		_b = bnew;

		float_t t1 = y1 * (a1 - alpha1);
		float_t t2 = y2 * (a2 - alpha2);

		for (int i = 0; i < count(); i++) {
			if (_alpha[i] > 0 && _alpha[i] < _c) {
				_error[i] += t1 * kernel(i1, i) + t2 * kernel(i2, i) - _deltaB;
			}
		}

		_error[i1] = 0;
		_error[i2] = 0;
		_alpha[i1] = a1;
		_alpha[i2] = a2;
		return 1;
	}

	/** SMO algorithm. */
	template<typename VECTOR_TYPE, typename FLOAT_TYPE, typename KERNEL_TYPE>
	void SVM<VECTOR_TYPE, FLOAT_TYPE, KERNEL_TYPE>::Train(const trainingset_t & ts) {
		_trainingSet = ts;
		// resize Lagrange alphas
		_alpha(ts.Size(), 0);
		// error cache
		_error(ts.Size(), 0);

		// basic loop from Platt's algo
		for (
			size_t iterations = 0, num_changed = 0, examine_all = 1;
			iterations < _iterationLimit && (num_changed > 0 || examine_all);
			++iterations
			) {
			num_changed = 0;
			if (examine_all) {
				for (int k = 0; k < ts.Size(); k++) {
					num_changed += examineSample(k);
				}
			}
			else {
				for (size_t k = 0; k < count(); k++) {
					if (alpha(k) != 0 && alpha(k) != _c) {
						num_changed += examineSample(k);
					}
				}
			}

			if (examine_all == 1) {
				examine_all = 0;
			}
			else if (num_changed == 0) {
				examine_all = 1;
			}

			DD_DEBUG_LOG(iterations, "obj ", Objective(), "err", ErrorRatio());

			for (int i = 0; i < ts.Size(); i++) {
				if (alpha(i) < SVM_TOLERANCE) {
					alpha(i) = 0;
				}
			}
		}
	}

	/** Computes objective function - could be used for observing if data are changed. */
	template<typename VECTOR_TYPE, typename FLOAT_TYPE, typename KERNEL_TYPE>
	typename SVM<VECTOR_TYPE, FLOAT_TYPE, KERNEL_TYPE>::float_t SVM<VECTOR_TYPE, FLOAT_TYPE, KERNEL_TYPE>::Objective() const {
		float_t E_Ai = 0;
		float_t E_AiAjYiYjKij = 0;
		for (size_t i = 0; i < count(); i++) {
			E_Ai += alpha(i);
		}
		for (size_t i = 0; i < count(); i++) {
			for (size_t j = 0; j < count(); j++) {
				E_AiAjYiYjKij += alpha(i) * alpha(j) * y(i) * y(j) * kernel(i, j);
			}
		}
		return E_Ai - float_t(0.5) * E_AiAjYiYjKij;
	}

	/** Computes SVM output. */
	template<typename VECTOR_TYPE, typename FLOAT_TYPE, typename KERNEL_TYPE>
	typename SVM<VECTOR_TYPE, FLOAT_TYPE, KERNEL_TYPE>::float_t SVM<VECTOR_TYPE, FLOAT_TYPE, KERNEL_TYPE>::operator()(const vector_t & v) const {
		float_t s = 0;
		for (size_t i = 0; i < count(); i++) {
			if (alpha(i) > 0) {
				s += alpha(i) * y(i) * _kernel(x(i), v);
			}
		}
		s -= _b;
		return s;
	}

	/** Ratio of errors : all samples */
	template<typename VECTOR_TYPE, typename FLOAT_TYPE, typename KERNEL_TYPE>
	typename SVM<VECTOR_TYPE, FLOAT_TYPE, KERNEL_TYPE>::float_t SVM<VECTOR_TYPE, FLOAT_TYPE, KERNEL_TYPE>::ErrorRatio() const {
		int sum = 0;
		for (size_t i = 0; i < count(); i++) {
			if ((compute(i) >= 0 && y(i) < 0) || (compute(i) < 0 && y(i) > 0)) {
				sum++;
			}
		}
		return float_t(sum) / count();
	}

	/** Constructor. */
	template<typename VECTOR_TYPE, typename FLOAT_TYPE, typename KERNEL_TYPE>
	SVM<VECTOR_TYPE, FLOAT_TYPE, KERNEL_TYPE>::SVM(kernel_t kernel, float_t epsilon, float_t c, size_t iterationLimit)
		: _kernel(kernel)
		, _epsilon(epsilon)
		, _c(c)
		, _b(0)
		, _deltaB(0)
		, _iterationLimit(iterationLimit)
	{}

	template<typename V, typename F, typename K, typename STREAM>
	STREAM & operator<<(STREAM & stream, SVM<V, F, K> svm) {
		svm.Serialize(stream);
		return stream;
	}

	template<typename V, typename F, typename K, typename STREAM>
	STREAM & operator >> (STREAM & stream, SVM<V, F, K> svm) {
		svm.Deserialize(stream);
		return stream;
	}

	template<typename V, typename F, typename STREAM>
	STREAM & operator<<(STREAM & stream, const ISVMPair<V, F> & pair) {
		return stream << pair.x << pair.y;
	}

	template<typename V, typename F, typename STREAM>
	STREAM & operator>>(STREAM & stream, ISVMPair<V, F> & pair) {
		return stream >> pair.x >> pair.y;
	}
}