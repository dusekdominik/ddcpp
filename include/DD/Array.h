#pragma once
#include <DD/Collections/Array.h>

namespace DD {
	/** 1D Array. */
	template<typename T, size_t ALLOCATION = 0>
	using Array = Collections::ArrayT<T, 1, Memory::AllocatorT<T>, size_t, ALLOCATION>;

	/** 2D Array. */
	template<typename T, size_t WIDTH = 0, size_t HEIGHT = WIDTH>
	using Array2D = Collections::ArrayT<T, 2, Memory::AllocatorT<T>, size_t, WIDTH, HEIGHT>;

	/** 3D array. */
	template<typename T, size_t WIDTH = 0, size_t HEIGHT = 0, size_t DEPTH = 0>
	using Array3D = Collections::ArrayT<T, 3, Memory::AllocatorT<T>, size_t, WIDTH, HEIGHT, DEPTH>;

	/** Forward Arrayize to DD namespace.  */
	using Collections::Arrayize;
}
