#pragma once
namespace DD {
	/**
	 * Central register of all profile scopes and its recordings.
	 * @see DD_PROFILE_SCOPE_SIMPLE - log.
	 * @see DD_PROFILE_SCOPE        - record.
	 * @see DD_PROFILE_SCOPE_LOG    - record and log.
	 * @see DD_PROFILE_SCOPE_LIMIT  - record, log and check limit.
	 */
	struct Profiler;
}

#ifndef DD_PROFILER_ENABLED
/** Switch for profiler.  */
#define DD_PROFILER_ENABLED 1
#endif // !DD_PROFILER_ENABLED

#if DD_PROFILER_ENABLED
/**
 * Profile scope time measurement.
 * Measurement will be recorded into the debug console.
 * @example
 *   void MyStruct::MyMethod() {
 *     DD_PROFILE_SCOPE("MyStruct/MyMethod")
 *     ... do other work ...
 *   }
 */
#define DD_PROFILE_SCOPE(name)\
	static ::DD::Profiler::Scope & __PROFILER = ::DD::Profiler::RegisterScope(name, __FUNCTION__, __FILE__, __LINE__, 0);\
	::DD::Profiler::WatchRec __WATCH(__PROFILER);
/**
 * Profile scope time measurement.
 * Measurement will NOT be recorded into the debug console.
 * Measurement will be print out to the debug log.
 * @example
 *   void MyStruct::MyMethod() {
 *     DD_PROFILE_SCOPE_SIMPLE("MyStruct/MyMethod")
 *     ... do other work ...
 *   }
 */
#define DD_PROFILE_SCOPE_SIMPLE(name)\
	static ::DD::Profiler::Scope & __PROFILER = ::DD::Profiler::RegisterScope(name, __FUNCTION__, __FILE__, __LINE__, 0);\
	::DD::Profiler::WatchLog __WATCH(__PROFILER);

/**
 * Standard profile scope with printing records into the log.
 * Measurement will be recorded into the debug console.
 * Measurement will be print out to the debug log.
 * @example
 *   void MyStruct::MyMethod() {
 *     DD_PROFILE_SCOPE_LOG("MyStruct/MyMethod")
 *     ... do other work ...
 *   }
 */
#define DD_PROFILE_SCOPE_LOG(name)\
	static ::DD::Profiler::Scope & __PROFILER = ::DD::Profiler::RegisterScope(name, __FUNCTION__, __FILE__, __LINE__, 0);\
	::DD::Profiler::WatchRecLog __WATCH(__PROFILER);
/**
 * Standard profile scope with printing records into the log.
 * Measurement will be recorded into the debug console.
 * Measurement will be print out to the debug log.
 * Measurement will be compared with the limit time value,
 *  debugger will break the computation if limit would be exceeded.
 * @example
 *   void MyStruct::MyMethod() {
 *     DD_PROFILE_SCOPE_LIMIT("MyStruct/MyMethod", 2800)
 *     ... do other work ...
 *   }
 */
#define DD_PROFILE_SCOPE_LIMIT(name, milliseconds)\
	static ::DD::Profiler::Scope & __PROFILER = ::DD::Profiler::RegisterScope(name, __FUNCTION__, __FILE__, __LINE__, milliseconds);\
	::DD::Profiler::WatchLimit __WATCH(__PROFILER);
/**
 * Standard profile scope with recording a short-time history.
 */
#define DD_PROFILE_SCOPE_STATS(name)\
	static ::DD::Profiler::Scope & __PROFILER = ::DD::Profiler::RegisterScope(name, __FUNCTION__, __FILE__, __LINE__, 0);\
	::DD::Profiler::WatchStats __WATCH(__PROFILER);
#else
/** Turned off. @see DD_PROFILER_ENABLED. */
#define DD_PROFILE_SCOPE(name)
/** Turned off. @see DD_PROFILER_ENABLED. */
#define DD_PROFILE_SCOPE_SIMPLE(name)
/** Turned off. @see DD_PROFILER_ENABLED. */
#define DD_PROFILE_SCOPE_LOG(name)
/** Turned off. @see DD_PROFILER_ENABLED. */
#define DD_PROFILE_SCOPE_LIMIT(name, milliseconds)
#endif


#include <DD/Concurrency/Atomic.h>
#include <DD/Stopwatch.h>

namespace DD {
	struct Profiler {
	public: // nested
		/** Profiler record. */
		struct Record {
			/** Index of the scope descriptor in profiler. */
			i32 Index;
			/** Recorded duration in ticks. */
			u64 Time;
		};

		/** Profiler buffer */
		struct Buffer {
			/** Buffer of records. */
			Record Records[8192];
			/** Top item of buffer, -1 if buffer is empty. */
			i32 Top = -1;
			/** Push record to buffer. */
			__forceinline void Add(Record r) { Records[Concurrency::Atomic::i32::Inc(Top)] = r; }
		};

		/** Information about scope. */
		struct Scope {
			/** Item name. */
			const char * ScopeName = 0;
			/** Name of function where the profile item is placed. */
			const char * ScopeFuntion = 0;
			/** Name of file .*/
			const char * ScopeFileName = 0;
			/** Accumulated time in ticks. */
			u64 TotalTime = 0;
			/** Minimal measured time in ticks. */
			u64 MinimalTime = (0xFFFFFFFFFFFFFFFF);
			/** Maximal measured time in ticks. */
			u64 MaximalTime = 0;
			/** Time of last recorded scope in ticks. */
			u64 LastTime = 0;
			/** Limit in ticks. */
			u64 LimitTime = 0;
			/** How many times was item profiled. */
			i32 Count = 0;
			/** Index in register. */
			i32 Index = 0;
			/** Index of line of scope in a file. */
			i32 ScopeFileLine = 0;
			/** Buffer of the scope. */
			struct {
				/** Data of the buffer - micro seconds. */
				f32 Data[256];
				/** Modular index of buffer. */
				i32 Index = 256;
			} Buffer;
		};

		/** Common traits for Profiler::Watch. */
		struct WatchTraits {
			/** Record microseconds into local buffer. */
			struct Stats { static void Postprocess(Scope &, Record); };
			/** Just record scope. */
			struct Rec { static void Postprocess(Scope &, Record r) { Profiler::RecordScope(r); } };
			/** Just print & no record. */
			struct Log { static void Postprocess(Scope &, Record); };
			/** Print to debug console & record scope. */
			struct RecLog { static void Postprocess(Scope & i, Record r) { Rec::Postprocess(i, r), Log::Postprocess(i, r); } };
			/** Break if time limit has been exceeded. @warning should not be used as permanent profile scope. */
			struct Limit { static void Postprocess(Scope &, Record); };
		};

		/** Watch - measurement of a scope. */
		template<typename TRAITS>
		struct Watch {
			/** Time measurement tool. */
			Stopwatch _watch;
			/** Item which should be profiled. */
			Scope & _item;
			/** Constructor. */
			__forceinline Watch(Scope & item) : _item(item) { _watch.Start(); }
			/** Destructor. */
			__forceinline ~Watch() { _watch.Stop(); TRAITS::Postprocess(_item, { _item.Index, _watch.ElapsedTicks() }); }
		};

		typedef Watch<WatchTraits::Rec>    WatchRec;
		typedef Watch<WatchTraits::Log>    WatchLog;
		typedef Watch<WatchTraits::RecLog> WatchRecLog;
		typedef Watch<WatchTraits::Limit>  WatchLimit;
		typedef Watch<WatchTraits::Stats>  WatchStats;

	private: // static properties
		/** Buffers for recording. */
		static Buffer _recordingBuffers[];
		/** Index of current recording buffer. */
		static i32 _currentRecordingBuffer;
		/** Register of profile scopes. */
		static Scope _profileScopes[];
		/** Number of profile scopes. */
		static i32 _profileScopeTop;

	public: // static methods
		/** Get a scope. */
		static Scope & GetScope(size_t i) { return _profileScopes[i]; }
		/** Get number of registered scopes. */
		static i32 GetScopesCount() { return _profileScopeTop + 1; }
		/** Get all registered data. */
		template<typename PARSER>
		static void GetScopes(const PARSER & parser) { for (i32 i = 0; i <= _profileScopeTop; ++i) parser(_profileScopes[i]); }
		/** Method for recording profile scopes. */
		__forceinline static void RecordScope(Record record) { _recordingBuffers[_currentRecordingBuffer].Add(record); }
		/** Method for parsing recorded info about profile scopes. @return number of parsed arguments. */
		static i32 ProcessScopes();
		/** Method for registering profile scopes. */
		static Scope & RegisterScope(const char * name, const char * place, const char * file, i32 line, i32 limit);
	};
}
