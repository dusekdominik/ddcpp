#pragma once
namespace DD {
	/**
	 * List<TYPE, ALLOCATION>
	 *  Array list implementation.
	 *  Serves as basic container.
	 *  Template argument ALLOCATION, represents number
	 *   how many items could be stored without heap allocation.
	 */
	template<typename T, size_t ALLOCATION = 0>
	class List;
}

#include <DD/Array.h>
#include <DD/ArrayView.h>
#include <DD/MacrosLite.h>
#include <DD/Random.h>

#define _DD_LIST_ASSERT(condition) DD_LITE_DEBUG_ASSERT(condition, "List indexing failed.")

namespace DD {
	template<typename T, size_t ALLOCATION>
	class List {
		template<typename U, size_t A>
		friend class List;

	public: // statics & constants
		/** Where top is when zero elements contained. */
		constexpr static size_t ZERO = static_cast<size_t>(-1L);

	protected: // properties
		/** Top of the stack. */
		size_t _top = ZERO;
		/** Real data container. */
		Array<T, ALLOCATION> _data;

	protected: // methods
		/** Generic push helper. */
		constexpr void addUnsafe() {}
		/** Generic push helper. */
		template<typename ARG, typename...ARGS>
		constexpr void addUnsafe(ARG && item, ARGS && ... items) { AppendUnsafe(Fwd(item)); addUnsafe(Fwd(items)...); }
		/** Generic push helper. */
		template<typename ARG, typename...ARGS>
		constexpr void addUnsafe(const ARG & item, const ARGS & ... items) { AppendUnsafe(item); addUnsafe(items...); }
		/** Method for reallocation container. */
		constexpr void increase();
		/** Method for reallocation container. */
		constexpr void increase(size_t size);
		/** Data shifting helper. */
		constexpr void shiftRight(size_t from, size_t size) { size_t i = _top; while (i-- > from) _data[i + size] = Fwd(_data[i]); }
		/** Data shifting helper. */
		constexpr void shiftLeft(size_t to, size_t offset) { for (size_t i = to + offset, l = Size(); i < l; ++i) _data[i - offset] = Fwd(_data[i]); }

	public: // iterator
		/** Standard iterator. */
		constexpr T * begin() { return _data.begin(); }
		/** Standard iterator. */
		constexpr const T * begin() const { return _data.begin(); }
		/** Standard iterator. */
		constexpr T * end() { return _data.begin() + (_top + 1); }
		/** Standard iterator. */
		constexpr const T * end() const { return _data.begin() + (_top + 1); }

	public: // accession
		constexpr T & At(size_t i)         { _DD_LIST_ASSERT(i < Size());    return _data[i]; }
		constexpr T & First()              { _DD_LIST_ASSERT(_top != ZERO);  return _data[0]; }
		constexpr T & First(size_t i)      { _DD_LIST_ASSERT(i < Size());    return _data[i]; }
		constexpr T & Last()               { _DD_LIST_ASSERT(_top != ZERO);  return _data[_top]; }
		constexpr T & Last(size_t i)       { _DD_LIST_ASSERT(i < Size());    return _data[_top - i]; }
		constexpr T & operator[](size_t i) { _DD_LIST_ASSERT(i < Size());    return _data[i]; }
		constexpr const T & At(size_t i) const         { _DD_LIST_ASSERT(i < Size());   return _data[i]; }
		constexpr const T & First() const              { _DD_LIST_ASSERT(_top != ZERO); return _data[0]; }
		constexpr const T & First(size_t i) const      { _DD_LIST_ASSERT(i < Size());   return _data[i]; }
		constexpr const T & Last() const               { _DD_LIST_ASSERT(_top != ZERO); return _data[_top]; }
		constexpr const T & Last(size_t i) const       { _DD_LIST_ASSERT(i < Size());   return _data[_top - i]; }
		constexpr const T & operator[](size_t i) const { _DD_LIST_ASSERT(i < Size());   return _data[i]; }
		constexpr ArrayView1D<T> Since(size_t index) { return ArrayView(begin() + index, end()); }
		constexpr ArrayView1D<T> Since(size_t index, size_t length) { _DD_LIST_ASSERT(index + length <= Size()) return ArrayView(begin() + index, length); }

	public: // adding
		/** Add multiple items to the list, if no args parsed, no items will be stored. */
		template<typename...ARGS>
		constexpr void Add(const T & t, const ARGS & ... items) { Reserve(Arguments<T, ARGS...>); addUnsafe(t, items...); }
		template<typename...ARGS>
		constexpr void Add(T && t, ARGS && ... items) { Reserve(Arguments<T, ARGS...>); addUnsafe(Fwd(t), Fwd(items)...); }
		template<typename...ARGS>
		constexpr void AddUnsafe(const T & t, const ARGS &...args) { addUnsafe(t, args...); }
		template<typename...ARGS>
		constexpr void AddUnsafe(T && t, ARGS &&...args) { addUnsafe(t, args...); }
		/** Adds a group of elements. */
		constexpr List & AddIterable(const T * begin, const T * end) { Reserve(end - begin); while (begin < end) { _data[++_top] = *(begin++); } return *this; }
		template<typename ITERABLE>
		constexpr List & AddIterable(const ITERABLE & s) { return AddIterable(s.begin(), s.end()); }
		/** Add all items of a sequencer. */
		template<typename SEQUENCER>
		constexpr List & AddSequence(SEQUENCER sequencer) { if (sequencer.Reset()) do Append(sequencer.Get()); while (sequencer.Next()); return *this; }
		/** Return index of newly added item. */
		constexpr size_t AddNaiveUnsafe(size_t size = 1) { return _top += size; }
		constexpr size_t AddNaive(size_t size = 1) { return Reserve(size).AddNaiveUnsafe(size); }
		/** Insert without space check, @return reference of newly added item. */
		constexpr T & AppendUnsafe() { return *(new (&_data[++_top]) T()); }
		constexpr List & AppendUnsafe(T && rhs) { new (&_data[++_top]) T(Fwd(rhs)); return *this; }
		constexpr List & AppendUnsafe(const T & rhs) { new (&_data[++_top]) T(rhs); return *this; }
		/** Check space, then insert. @return reference of newly added item. */
		constexpr T & Append() { return Reserve().AppendUnsafe(); }
		constexpr List & Append(T && rhs) { new (&Append()) T(Fwd(rhs)); return *this; }
		constexpr List & Append(const T & rhs) { new (&Append()) T(rhs); return *this; }
		/** Inserts an element into position. */
		constexpr T & Insert(size_t index) { _DD_LIST_ASSERT(index <= Size()); AddNaive(); shiftRight(index, 1); return _data[index]; }
		constexpr List & Insert(size_t index, T && rhs) { new (&Insert(index)) T(Fwd(rhs)); return *this; }
		constexpr List & Insert(size_t index, const T & rhs) { new (&Insert(index)) T(rhs); return *this; }
		/** Inserts group of elements defined by iterators into position. */
		constexpr Array<T> & InsertMany(size_t index, size_t length) { AddNaive(length); shiftRight(index, length); return Array<T>(begin() + index, length, false); }
		constexpr List & InsertIterable(size_t index, const T * begin, const T * end) { InsertMany(index, end - begin).CopyFrom(begin); return *this; }
		/** Preallocates at least given size. */
		constexpr List & Reserve(size_t size = 1) { if (_top + size >= Capacity()) increase(Size() + size); return *this; }
		/** Resizes list to its capacity. */
		constexpr List & Resize() { _top = Capacity() - 1;  return *this; }
		/** Resizes list to given size and sets end of list onto the last item. */
		constexpr List & Resize(size_t size) { if (size > Capacity()) _data.Reform(size); _top = size - 1; return *this; }

	public: // removing
		/** Resets stacks. Does not touch the allocation. */
		constexpr List & Clear() { _top = ZERO; return *this; }
		/** Deallocate storage. */
		constexpr List & Free() { for (T & item : *this) item.~T(); _top = ZERO; return *this; }
		/** Removes element on given index. */
		constexpr List & RemoveAt(size_t index);
		/**
		 * Remove range.
		 * @warning method does not check if given values are valid.
		 * @param index - first index to be removed
		 * @param number of indices to delete
		 */
		constexpr List & RemoveAt(size_t index, size_t size);
		/**
		 * Remove item and replace by the end of collection.
		 * It is faster, because we do not need to shift the data.
		 * @warning After this operation, the ordering of the list items changes.
		 */
		constexpr List & RemoveAtFast(size_t index) { if (index != _top) ::DD::Swap(At(index), Last()); return RemoveLast(); }
		/**
		 * Removes element on given index.
		 * @param indices - sorted array of indices to be removed.
		 */
		template<typename ITERABLE>
		constexpr List & RemoveAt(const ITERABLE & indices);
		/** Removes data and decreases top pointer. */
		constexpr List & RemoveLast() { _DD_LIST_ASSERT(_top != ZERO); (begin() + _top--)->~T(); return *this; }
		/** Filter th whole list, positive occurrences are removed. */
		template<typename FILTER>
		constexpr List & RemoveAll(const FILTER & filter) { return Remove(filter, static_cast<size_t>(-1)); }
		/** Remove first positive occurrence. */
		template<typename FILTER>
		constexpr List & RemoveFirst(const FILTER & filter) { return Remove(filter, 1); }
		/** Filter out positive occurrences of filter - bool(T&). */
		template<typename FILTER>
		constexpr List & Remove(const FILTER & filter, size_t howMany);
		/** Look for given content, if exists then remove. */
		constexpr bool TryRemove(const T & i);

	public: // checks
		/** How many items can be stored. */
		constexpr size_t Capacity() const { return _data.Length(); }
		/** Checks if given item is contained. */
		constexpr bool Contains(const T & i) const { return IndexOf(i) != ZERO; }
		/** How many new items can be added. */
		constexpr size_t FreeSpace() const { return Capacity() - Size(); }
		/** Select first item with valid functor or maximum of size_t type. */
		template<typename FUNCTOR>
		constexpr size_t IndexOf(const FUNCTOR & functor) const { for (const T & i : *this) { if (functor(i)) { return &i - begin(); } } return ZERO; }
		/** Returns index of first contained object or returns -1. */
		constexpr size_t IndexOf(const T & i) const { return IndexOf([&](const T & t) { return t == i; }); }
		/** Checks if no data are contained. */
		constexpr bool IsEmpty() const { return _top == ZERO; }
		/** Checks if any data are contained. */
		constexpr bool IsNotEmpty() const { return _top != ZERO; }
		/** Stack size. */
		constexpr size_t Size() const { return _top + 1; }

	public: // methods
		/** Set all items to given value. */
		constexpr List & Fill(const T & value) { for (T & i : *this) i = value; return *this; }
		/** Set piece of native memory as buffer for the stack. */
		constexpr List & Catch(ArrayView1D<T> data, size_t itemCount);
		/** Set piece of native memory as buffer for the stack. */
		constexpr List & CatchConst(ArrayView1D<T> data, size_t itemCount);
		/** Cuts the memory base to be using just necessary space. */
		constexpr List & Cut();
		/** Swaps two lists. */
		constexpr List & Swap(List & rhs);
		/** Randomize list. */
		constexpr List & Randomize();

	public: // converters
		/** Conversion operator, List<T, N> is always convertible to List<T, 0>. */
		constexpr operator List<T> & () { return reinterpret_cast<List<T> &>(*this); }
		/** Conversion operator, List<T, N> is always convertible to List<T, 0>. */
		constexpr operator const List<T> & () const { return reinterpret_cast<const List<T> &>(*this); }
		/** Conversion operator. */
		constexpr operator ArrayView1D<T>() { return { begin(), Size() }; }
		/** Conversion operator. */
		constexpr operator ArrayView1D<const T>() const { return { begin(), Size() }; }
		/** Conversion. */
		constexpr ArrayView1D<const T> ToView() const { return { begin(), Size() }; }
		/** Conversion. */
		constexpr ArrayView1D<T> ToView() { return { begin(), Size() }; }

	public: // assignment
		/** Move assignment */
		template<size_t A>
		constexpr List & operator=(List<T, A> && rvalue);
		/** Move constructor. */
		constexpr List & operator=(List && rvalue) { return operator=<ALLOCATION>(Fwd(rvalue)); }
		/** Copy assignment */
		template<size_t A>
		constexpr List & operator=(const List<T, A> & lvalue);
		/** Copy assignment */
		constexpr List & operator=(const List & lvalue) { return operator=<ALLOCATION>(lvalue); }

	public: // constructors
		/** Zero constructor. */
		constexpr List() = default;
		/** Copy constructor. */
		template<size_t A>
		constexpr explicit List(const List<T, A> & lvalue) { *this = lvalue; }
		/** Copy constructor. */
		explicit constexpr List(const List & lvalue) { *this = lvalue; }
		/** Move constructor. */
		template<size_t A>
		constexpr List(List<T, A> && rvalue) : List() { *this = Fwd(rvalue); }
		/** Move constructor. */
		constexpr List(List && rvalue) : List() { *this = Fwd(rvalue); }
		/** Unique array forwarder. */
		constexpr List(Array<T> && arr) : _top(arr.Size() - 1) { _data.Swap(arr); }
		/** Constructor. Param specifies preallocation. */
		explicit constexpr List(size_t size) : _data(size) {};
	};
}

namespace DD {
	template<typename T, size_t ALLOCATION>
	constexpr List<T, ALLOCATION> & List<T, ALLOCATION>::RemoveAt(size_t index) {
		_data[index].~T();
		shiftLeft(index, 1);
		--_top;

		return *this;
	}


	template<typename T, size_t ALLOCATION>
	constexpr List<T, ALLOCATION> & List<T, ALLOCATION>::RemoveAt(size_t index, size_t size) {
		// boundaries
		T * begin = _data.begin() + index;
		T * end = begin + size;
		// release the old values
		for (T * i = begin; i < end; ++i) {
			i->~T();
		}

		// fix list size
		shiftLeft(index, size);
		_top -= size;

		return *this;
	}


	template<typename T, size_t ALLOCATION>
	constexpr bool List<T, ALLOCATION>::TryRemove(const T & i) {
		size_t index = IndexOf(i);
		if (index < Size()) {
			RemoveAt(index);
			return true;
		}

		return false;
	}


	template<typename T, size_t ALLOCATION>
	template<typename ITERABLE>
	constexpr List<T, ALLOCATION> & List<T, ALLOCATION>::RemoveAt(const ITERABLE & indices) {
		// get iterable information
		auto begin = indices.begin();
		auto end = indices.end();
		// if any indices
		if (begin != end) {
			// size of shift
			size_t shift = 0;
			// currently processed index
			size_t current = *begin;
			// for each index (index could be any number type -> auto)
			for (auto index = *begin; begin < end; ++begin) {
				// remove item on the index
				_data[index].~T();
				// shift items between indices
				while (current < index) {
					_data[current - shift] = Fwd(_data[current]);
					++current;
				}
				// move processed index
				current = index + 1;
				// increase shifting index
				++shift;
			}
			// shift items between last index and end of list
			while (current <= _top) {
				_data[current - shift] = Fwd(_data[current]);
				++current;
			}
			// fix number of items
			_top -= shift;
		}

		return *this;
	}


	template<typename T, size_t ALLOCATION>
	template<typename FILTER>
	constexpr List<T, ALLOCATION> & List<T, ALLOCATION>::Remove(const FILTER & filter, size_t howMany) {
		const size_t listSize = Size();
		// select first item if any - this prevents from unnecessary copying
		size_t firstOccurrence = listSize;
		for (size_t index = 0; index < listSize; ++index) {
			if (filter(At(index))) {
				firstOccurrence = index;
				break;
			}
		}

		// no occurrence found
		if (firstOccurrence == listSize)
			return *this;

		size_t removedItems = 1;
		// get next item that is not removed (to be shifted)
		auto nextItem = [&](size_t from) {
			// spin until unfiltered item is encountered
			while (from < listSize && removedItems < howMany && filter(At(from)))
				++from, ++removedItems;

			return from;
		};

		// scan and shift data
		for (size_t from = nextItem(firstOccurrence + 1), to = firstOccurrence; from < listSize; from = nextItem(from))
			At(to++) = Fwd(At(from++));

		// process removing
		return RemoveAt(listSize - removedItems, removedItems);
	}


	/************************************************************************/
	/* Methods                                                              */
	/************************************************************************/
	template<typename T, size_t ALLOCATION>
	constexpr void List<T, ALLOCATION>::increase() {
		size_t newSize = Capacity() ? Capacity() * 2 : 1;
		_data.Reform(newSize);
	}


	template<typename T, size_t ALLOCATION>
	constexpr void List<T, ALLOCATION>::increase(size_t size) {
		size_t newSize = 1;
		while ((newSize *= 2) < size);
		_data.Reform(newSize);
	}


	template<typename T, size_t ALLOCATION>
	constexpr List<T, ALLOCATION> & List<T, ALLOCATION>::Catch(ArrayView1D<T> data, size_t itemCount) {
		_data(data);
		_top = itemCount - 1;

		return *this;
	}


	template<typename T, size_t ALLOCATION>
	constexpr List<T, ALLOCATION> & List<T, ALLOCATION>::CatchConst(ArrayView1D<T> data, size_t itemCount) {
		_data.Swap({ data, nullptr });
		_top = itemCount - 1;

		return *this;
	}


	template<typename T, size_t ALLOCATION>
	constexpr List<T, ALLOCATION> & List<T, ALLOCATION>::Swap(List & rhs) {
		::DD::Swap(_top, rhs._top);
		_data.Swap(rhs._data);

		return *this;
	}


	template<typename T, size_t ALLOCATION>
	constexpr List<T, ALLOCATION> & List<T, ALLOCATION>::Cut() {
		DD_LITE_TODO("adapt for preallocated data");
		Array<T>(Size()).CopyFrom(0, _data.Size(), _data.begin()).Swap(_data);

		return *this;
	}


	template<typename T, size_t ALLOCATION>
	constexpr List<T, ALLOCATION> & List<T, ALLOCATION>::Randomize() {
		for (size_t i = 0, max = Size(); i < max; ++i)
			::DD::Swap(
				At(Random::Generate<u32>() % max),
				At(Random::Generate<u32>() % max)
			);

		return *this;
	}


	/************************************************************************/
	/* Assignment                                                           */
	/************************************************************************/
	template<typename T, size_t ALLOCATION>
	template<size_t A>
	constexpr List<T, ALLOCATION> & List<T, ALLOCATION>::operator=(List<T, A> && rhs) {
		// if responsibility could be taken over, take it
		if (rhs._data.HasAllocator())
			Swap(rhs);
		// otherwise move data one by one
		else
			Resize(rhs.Size())._data.MoveIn(rhs.ToView());

		return *this;
	}


	template<typename T, size_t ALLOCATION>
	template<size_t A>
	constexpr List<T, ALLOCATION> & List<T, ALLOCATION>::operator=(const List<T, A> & lvalue) {
		Clear();

		Reserve(lvalue.Size());
		for (const T & item : lvalue)
			AppendUnsafe(item);

		return *this;
	}
}
