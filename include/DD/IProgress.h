#pragma once
namespace DD {
	/**
	 * Struct to observe loading process of long running async tasks.
	 * Reports when task starts and when task ends.
	 * @warning do not use for micro jobs, calling overhead will not be negligible.
	 *
	 * @example common usage
	 *   struct StateReader : IProgress {    // reader
	 *     Action16<> Init, Term;            // callbacks
	 *     volatile bool Ready = false;      // synchronization boolean
	 *     void Process() {                  // reading process
	 *       while (!Ready);                 // spin until ready
	 *       while (Ready) {
	 *         f32 progress;
	 *         IProgress::Get(progress);     // read state
	 *         Print(progress * 100, "%");   // print percentage state
	 *         Thread::Sleep(10);            // sleep
	 *       }
	 *     }
	 *     StateReader()
	 *       : IProgress(Init, Term)         // set reader callbacks to progress
	 *       , Init([this]() { ready = 1; }) // begin callback
	 *       , Term([this]() { ready = 0; }) // end callback
	 *     {}
	 *   };
	 *
	 *   struct StateWriter {
	 *      void Proces(IProgress * progress = 0) {
	 *        IProgress::ScopeCounter32 counter(progress, beginIndex, endIndex);
	 *        for (; counter; ++counter) { ... }     // do the progress
	 *      }
	 *      StateWriter() { ... }                    // init somehow
	 *   };
	 *
	 *   StateReader reader;                        // init reader
	 *   Task readTask([&](){ reader.Process(); })  // process state - reader asynchronously
	 *   StateWriter().Process(&reader);            // run writing state (reading file)
	 *   task.Wait();                               // finalize safely
	 */
	struct IProgress;
}

#include <DD/Concurrency/AtomicLock.h>
#include <DD/Lambda.h>

namespace DD {
	struct IProgress {
		/** Scope handler for IProgress. */
		struct Scope;
		/** Structure for computing progress over for loops. */
		template<typename INDEX, typename BOUND>
		struct Counter;
		/** Scope with counter at once. */
		template<typename INDEX, typename BOUND = INDEX>
		struct ScopeCounter;
		/** 32bit counter. */
		struct ScopeCounter32;
		/** 32bit counter with atomic indexing. */
		struct ScopeCounter32A;

	private: // properties
		/** Synchronization primitive. */
		Concurrency::AtomicLock _lock;
		/** Callback for obtaining current state. */
		IAction<f32 &> * _iProgressGet = nullptr;

	protected: // properties
		/** Callback for preparation for loading. */
		IAction<> * _iProgressBegin;
		/** Callback for finalization of loading. */
		IAction<> * _iProgressEnd;

	public: // methods
		/**
		 * This callback is invoked when progress started.
		 * @param progress - handle for obtaining progress state,
		 *                 - callback is alive until End is called.
		 */
		void Begin(IAction<f32 &> * progress);
		/** This callback is invoked when progress ended. */
		void End();

	public: // method
		/**
		 * Get current state.
		 * @param state - if object is alive, in ref will be stored current value
		 *              - value of progress ([0.0...1.0]), otherwise no number will be stored.
		 */
		void Get(f32 & state);

	public: // constructors
		/**
		 * Progress callback for async reading of progress state.
		 * @param beginCallback - callback notifying when progress is initialized.
		 * @param endCallback   - callback notifying when progress is finalized.
		 */
		IProgress(IAction<> * beginCallback = 0, IAction<> * endCallback = 0)
			: _iProgressBegin(beginCallback)
			, _iProgressEnd(endCallback)
		{}
	};

	struct IProgress::Scope {
		/** Callback action. */
		Action32<f32 &> Callback;
		/** Pointer to main struct. */
		IProgress * Progress;

		/** Constructor. */
		template<typename LAMBDA>
		Scope(IProgress * progress, const LAMBDA & lambda);
		/** Constructor designed for ScopeCounter structure.  */
		template<typename INDEX, typename BOUND>
		Scope(IProgress * p, const Counter<INDEX, BOUND> & c);
		/** Destructor. */
		~Scope();
	};

	template<typename INDEX, typename BOUND>
	struct IProgress::Counter {
		/** Iterator value. */
		INDEX Index;
		/** Bounds. */
		BOUND Begin, End;

		/** Pre-increment operator. */
		bool operator++() { return ++Index < End; }
		/** Post-increment operator. */
		bool operator++(int) { return (++Index - 1) < End; }
		/** Functor for obtaining current state. */
		void operator()(f32 & result) const { result = (f32)(Index - Begin) / (f32)(End - Begin); }
		/** For cycle check. */
		operator bool() const { return Index < End; }

		/** Constructor. */
		Counter(BOUND begin, BOUND end) : Index(begin), Begin(begin), End(end) {}
	};

	template<typename INDEX, typename BOUND>
	struct IProgress::ScopeCounter
		: public Counter<INDEX, BOUND>
		, private Scope
	{
		/**
		 * Constructor.
		 * @param p - pointer to a progress structure.
		 * @param b - beginning of counter.
		 * @param e - ending of counter.
		 */
		ScopeCounter(IProgress * p, BOUND b, BOUND e);
	};

	struct IProgress::ScopeCounter32
		: public ScopeCounter<i32>
	{
		/**
		 * Constructor.
		 * @param p     - pointer ti IProgress structure.
		 * @param e - ending of counter, beginning will be set to zero.
		 */
		ScopeCounter32(IProgress * p, i32 e) : ScopeCounter(p, 0, e) {}
		/**
		 * Constructor.
		 * @param p - pointer to a progress structure.
		 * @param b - beginning of counter.
		 * @param e - ending of counter.
		 */
		ScopeCounter32(IProgress * p, i32 b, i32 e) : ScopeCounter(p, b, e) {}
	};

	struct IProgress::ScopeCounter32A
		: public ScopeCounter<volatile Concurrency::Atomic::i32, i32>
	{
		/**
		 * Constructor.
		 * @param p - pointer to a progress structure.
		 * @param e - ending of counter, beginning will be set to zero.
		 */
		ScopeCounter32A(IProgress * p, i32 e) : ScopeCounter(p, 0, e) {}
		/**
		 * Constructor.
		 * @param p - pointer to a progress structure.
		 * @param b - beginning of counter.
		 * @param e - ending of counter.
		 */
		ScopeCounter32A(IProgress * p, i32 b, i32 e) : ScopeCounter(p, b, e) {}
	};
}

namespace DD {
	/************************************************************************/
	/* IProgress::Scope                                                     */
	/************************************************************************/
	template<typename LAMBDA>
	__forceinline IProgress::Scope::Scope(
		IProgress * progress,
		const LAMBDA & callback
	) : Progress(progress)
		, Callback(callback)
	{
		if (Progress)
			Progress->Begin(Callback);
	}

	template<typename INDEX, typename BOUND>
	__forceinline IProgress::Scope::Scope(
		IProgress * p,
		const Counter<INDEX, BOUND> & c
	) : Scope(p, [&c](f32 & f) { return c(f); })
	{}

	__forceinline IProgress::Scope::~Scope() {
		if (Progress)
			Progress->End();
	}

	/************************************************************************/
	/* Counters                                                             */
	/************************************************************************/
	template<typename INDEX, typename BOUND>
	IProgress::ScopeCounter<INDEX, BOUND>::ScopeCounter(
		IProgress * progress,
		BOUND begin,
		BOUND end
	)
		: Counter<INDEX, BOUND>(begin, end)
		, Scope(progress, (const Counter<INDEX, BOUND> &)(*this))
	{}

	/************************************************************************/
	/* IProgress                                                            */
	/************************************************************************/
	__forceinline void IProgress::Begin(IAction<f32 &> * progress) {
		Concurrency::AtomicLock::Scope scope(_lock);
		_iProgressGet = progress;
		if (_iProgressBegin)
			_iProgressBegin->Run();
	}

	__forceinline void IProgress::End() {
		Concurrency::AtomicLock::Scope scope(_lock);
		_iProgressGet = nullptr;
		if (_iProgressEnd)
			_iProgressEnd->Run();
	}

	__forceinline void IProgress::Get(f32 & state) {
		Concurrency::AtomicLock::Scope scope(_lock);
		if (_iProgressGet)
			_iProgressGet->Run(state);
	}
}
