#pragma once
namespace DD::Random {
	/** Default singleton generator. */
	struct Generator;

	/** Generate random number of given type. */
	template<typename T>
	T Generate();

	/** Generate random number of given type in given bounds. */
	template<typename T>
	T Generate(T min, T max);

	/** Generate symmetrical noise, min = -value, max = +value. */
	template<typename T>
	T Symmetric(T value);
}

#include <DD/Stopwatch.h>
#include <DD/Random/SimpleGenerator.h>

namespace DD::Random {
	struct Generator
		: SimpleGenerator
	{
		/** Singleton. */
		static Generator & Instance() { static Generator instance; return instance; }

		/** Constructor. */
		Generator() : SimpleGenerator((u32)Stopwatch::TimeStampMilliseconds()) {}
		/** Constructor. */
		Generator(u32 seed) : SimpleGenerator(seed) {}
	};



	template<typename T>
	T Generate() { return Generator::Instance().Generate<T>(); }


	template<typename T>
	void Generate(T & target) { target = Generator::Instance().Generate<T>(); }


	template<typename T>
	T Generate(T min, T max) { return Generator::Instance().Generate<T>(min, max); }


	template<typename T>
	T Symmetric(T value) { return Generator::Instance().Generate<T>(-value, value); }
}

