#pragma once
namespace DD::Random {
	/**
	 * Sequence of random numbers.
	 * @param T - type of random number, e.g. i32, f32, etc.
	 */
	template<typename T>
	struct Sequencer;
}

#include <DD/Random/SimpleGenerator.h>
#include <DD/Sequencer/Driver.h>
#include <DD/Types.h>

namespace DD::Random {
	/** Sequencer for 32-bit floats. */
	using SequencerF = Sequencer<f32>;
	/** Sequencer for 64-bit floats. */
	using SequencerD = Sequencer<f64>;
	/** Sequencer for 32-bit signed integers. */
	using SequencerI = Sequencer<i32>;
	/** Sequencer for 32-bit unsigned integers. */
	using SequencerU = Sequencer<u32>;

	template<typename T>
	struct Sequencer {
	protected: // properties
		/** Random number generator. */
		Generator _generator;
		/** Number of already generated items. */
		size_t _counter;
		/** Size of the sequence, number of items to be generated. */
		size_t _size;
		/** Initial seed for random numbers. */
		u32 _randomSeed;
		/** Currently generated random number. */
		T _randomNumber;
		/** Bounds of generated numbers. */
		T _min, _max;

	public: // Sequencer interface
		T Get() { return _randomNumber; }
		bool Next();
		bool Reset();
		size_t Count() { return _size; }

	public: // constructors
		/**
		 * Constructor.
		 * @param sequenceSize - number of items to be generated.
		 * @param min - lower bound of generated numbers.
		 * @param max - upper bound of generated numbers.
		 * @param seed - initial seed for random generator, serves for safe Reset() call
		 *             - to keep the sequence repeatable.
		 */
		Sequencer(size_t sequenceSize, T min, T max, u32 seed = Generate<u32>(0, 1 << 16));
		/** Constructor. */
		Sequencer() = default;
		/** Constructor. */
		Sequencer(Sequencer &&) = default;
		/** Constructor. */
		Sequencer(const Sequencer &) = default;
	};
}


namespace DD::Random {
	template<typename T>
	bool Sequencer<T>::Next() {
		// generate new number
		_randomNumber = _generator.Generate<T>(_min, _max);
		// update counter
		return ++_counter <= _size;
	}


	template<typename T>
	bool Sequencer<T>::Reset() {
		// reset generator
		_generator.Seed(_randomSeed);
		// reset counter
		_counter = 0;
		// generate first item
		return Next();
	}


	template<typename T>
	Sequencer<T>::Sequencer(size_t sequenceSize, T min, T max, u32 seed /*= Generate<u32>(0, 1 << 16)*/)
		: _randomSeed(seed)
		, _size(sequenceSize)
		, _min(min)
		, _max(max)
	{}
}

