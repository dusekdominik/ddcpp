#pragma once
namespace DD {
	/**
	 * Standard heap with operator< as default comparer.
	 * @param ALLOCATION - number of locally allocated items.
	 */
	template<typename T, typename COMPARER, size_t ALLOCATION = 0>
	struct Heap;
}

#include <DD/Compare.h>
#include <DD/HeapLib.h>
#include <DD/Collections/LazyList.h>

namespace DD {
	/** Heap based on operator <, top is minimum. */
	template<typename T, size_t ALLOCATION = 0>
	using MinHeap = Heap<T, ComparerA, ALLOCATION>;

	/** Heap based on operator <, top is maximum. */
	template<typename T, size_t ALLOCATION = 0>
	using MaxHeap = Heap<T, ComparerD, ALLOCATION>;

	template<typename T, typename COMPARER, size_t ITEM_ALLOCATION>
	struct Heap {
		/** Comparer type. */
		using ComparerType = COMPARER;
		/** Item type. */
		using ItemType = T;

	public: // properties
		/** Comparer type. */
		ComparerType Comparer;
		/** Storage of the heap. */
		Collections::LazyList<ItemType, ITEM_ALLOCATION> Storage;

	protected: // methods
		/** Heap up last item. */
		constexpr void heapUp() { DD::HeapLib::HeapUp(Storage.ToArray(), Comparer, Storage.LastIndex()); }
		/** Heap down first item. */
		constexpr void heapDown() { DD::HeapLib::HeapDown(Storage.ToArray(), Comparer); }

	public: // methods
		/** Remove all items. */
		constexpr Heap & Clear() { Storage.Clear(); return *this; }
		/** Is heap empty. */
		constexpr bool IsEmpty() const { return Storage.IsEmpty(); }
		/** Make heap out of the heap storage. */
		constexpr Heap & Heapify() { DD::HeapLib::Heapify(Storage.ToArray(), Comparer); return *this; }
		/** Pop item from heap, user must ensure that there is something to pop. */
		constexpr Heap & Pop() { Storage.RemoveAtFast(0); heapDown(); return *this; }
		/** Add item to heap. */
		constexpr Heap & Push(ItemType && item) { Storage.Append(Fwd(item)); heapUp(); return *this; }
		/** Add item to heap. */
		constexpr Heap & Push(const ItemType & item) { Storage.Append(item); heapUp(); return *this; }
		/** Number of events. */
		constexpr size_t Size() const { return Storage.Count(); }
		/** Top of the heap. */
		constexpr T & Top() { return Storage[0]; }
		/** Top of the heap. */
		constexpr const T & Top() const { return Storage[0]; }
		/** If any items, store top to the given target and return true; false otherwise. */
		constexpr bool TryPop(T & target) { if (IsEmpty()) return false; target = DD::Fwd(Top()); Pop(); return true; }
	};
}
