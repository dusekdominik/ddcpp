#pragma once
namespace DD {
	/**
	 * Class for time measurement.
	 *
	 * @example basic measurement
	 *   DD::Stopwatch watch;
	 *   watch.Start();
	 *   measuredMethod1();
	 *   watch.Stop();
	 *   logTime(watch.ElapsedMilliseconds());
	 */
	class Stopwatch;
}

#include <DD/Types.h>
#include <DD/Units/Units.h>

namespace DD {
	class Stopwatch {
	public: // nested
		/** Tag for initialization. */
		enum RunTag { Run };

	public: // statics
		/** Processor tick frequency. */
		static u64 Frequency();
		/** System time stamp (ticks). */
		static u64 TimeStampTicks();
		/** System time stamp (ms). */
		static u64 TimeStampMilliseconds();
		/** System time stamp (s). */
		static u64 TimeStampSeconds();

	protected: // properties
		/** Timestamp of start time-point. */
		u64 _startTime;
		/** Already elapsed time. */
		u64 _elapsed;

	public: // methods
		/** Are watches turned on. */
		bool IsRunning() { return _startTime != 0; }
		/** Starts measurement - watches must be stopped before calling. */
		void Start();
		/** Stops measurement - watches must be started before calling.*/
		void Stop();
		/** Resets accumulated value and starts accumulating again. */
		void Restart() { Reset(); Start(); }
		/** Resets accumulated value and sets watches stopped. */
		void Reset() { _elapsed = _startTime = 0; }
		/** Accumulated time. */
		u64 ElapsedTicks() const;
		/** Accumulated time. */
		Units::Time_NanoSecondULL ElapsedNanoseconds() const;
		/** Accumulated time. */
		Units::Time_MicroSecondULL ElapsedMicroseconds() const;
		/** Accumulated time. */
		Units::Time_MilliSecondULL ElapsedMilliseconds() const;
		/** Accumulated time. */
		Units::Time_SecondULL ElapsedSeconds() const;
		/** Accumulated time. */
		Units::Time_MinuteULL ElapsedMinutes() const;
		/** Accumulated time. */
		Units::Time_HourULL ElapsedHours() const;
		/** Accumulated time. */
		Units::Time_DayULL ElapsedDays() const;

	public: // constructors
		/** Zero constructor. */
		Stopwatch() : _elapsed(0), _startTime(0) {}
		/**
		 * Constructor turning stopwatch on.
		 * @example
		 *   DD::Stopwatch watches(DD::Stopwatch::Run);
		 */
		Stopwatch(RunTag) : _elapsed(0), _startTime(TimeStampTicks()) {}
	};
}