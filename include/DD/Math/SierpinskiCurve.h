#pragma once
namespace DD::Math {
	/**  */
	struct SierpinskiCurve;
}

#include <DD/List.h>
#include <DD/Text/StringView.h>
#include <DD/Vector.h>
#include <DD/Types.h>

namespace DD::Math {
	struct SierpinskiCurve {
	public: // nested
		/** Commands of Sierpinski move. */
		enum Commands { Rotate45CW = '-', Rotate45CCW = '+', NestedCmd = 'X', Go = 'G' };

	public: // statics
		/** Precomputed value for sin(45)/cos(45). */
		static constexpr f64 A45 = 0.70710678118654752440084436210485;
		/** Helper table with precomputed values for shifts. */
		static constexpr double2 AngleStates[8] = {
			/*  0*/{  1.0,  0.0},
			/* 45*/{  A45, -A45 },
			/* 90*/{  0.0, -1.0 },
			/*135*/{ -A45, -A45 },
			/*180*/{ -1.0,  0.0},
			/*225*/{ -A45,  A45 },
			/*270*/{  0.0,  1.0 },
			/*315*/{  A45,  A45 },
		};
		/** Command set for level 0. */
		static constexpr Text::StringView8 HeadCommand = "G--XG--G--XG";
		/** COmmand set for levels above 0. */
		static constexpr Text::StringView8 NestedCommand = "XG+G+XG--G--XG+G+X";

	public: // properties
		/** Stack with nested states, items are indices pointing to command sets.. */
		List<size_t, 32> _stack;
		/** Number of levels, minimal value is 1. */
		size_t _level;
		/** Current angle, index to @see AngleStates. */
		size_t _angle;
		/** Current point/ */
		double2 _point;

	public: // sequencer interface
		constexpr bool Reset();
		constexpr bool Next();
		constexpr double2 Get() { return _point; }

	public: // constructors
		/** Constructor, @param level - depth of recursion. */
		SierpinskiCurve(size_t level) : _level(level) {}
	};
}


namespace DD::Math {
	inline constexpr bool SierpinskiCurve::Reset() {
		_stack.Clear();
		_stack.Add(0);
		//
		_point = DD::double2(0.0, A45);
		_angle = 1;

		return true;
	}

	inline constexpr bool SierpinskiCurve::Next() {
		while (_stack.IsNotEmpty()) {
			const Text::StringView8 & commandSet = _stack.Size() == 1 ? HeadCommand : NestedCommand;
			size_t index = _stack.Last()++;

			// command set is over
			if (index == commandSet.Length()) {
				_stack.RemoveLast();
			}
			else {
				switch ((Commands)commandSet[index]) {
				case Commands::Rotate45CW:
					_angle = _angle ? _angle - 1 : 7;
					break;
				case Commands::Rotate45CCW:
					_angle = (_angle + 1) % 8;
					break;
				case Commands::NestedCmd:
					if (_stack.Size() < _level)
						_stack.Add(0);
					break;
				case Commands::Go:
					_point += AngleStates[_angle];
					return true;
				}
			}
		}

		return false;
	}
}