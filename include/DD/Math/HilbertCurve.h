#pragma once
namespace DD::Math {
	/**
	 * Hilbert curve sequencer.
	 * Return pair of indices for browsing square structures by one pass and locally coherent.
	 */
	struct HilbertCurve;
}

#include <DD/Auxiliaries.h>
#include <DD/Sequencer/Driver.h>
#include <DD/Math/Vector.h>
#include <DD/SimpleSequence.h>
#include <DD/Types.h>

namespace DD::Math {
	struct HilbertCurve {
		/**  */
		using Point = DD::Math::CommonVector<DD::i64, 2>;

	protected: // statics
		/** Apply rectangular rotation on the given point */
		static constexpr void RotatePoint(size_t gridSize, Point & pt, Point rotation) {
			if (rotation.y == 0) {
				if (rotation.x == 1)
					pt = Point(gridSize - 1) - pt;

				DD::Swap(pt.x, pt.y);
			}
		}

	protected: // properties
		/** Index of current state, 0...2^(level * 2) - 1. */
		SimpleSequencer<size_t> _index;
		/** Length of grind in a single dimension. */
		size_t _gridSize;

	public:
		/** Get number of items. */
		constexpr size_t GetLength() const { return _gridSize * _gridSize; }
		/** Compute point from binary code of its index, log time. */
		constexpr Point ComputePoint(size_t distance) const {
			// initial point
			Point point = 0;

			// apply rotation over the levels
			for (size_t quadrantSize = 1; quadrantSize < _gridSize; quadrantSize *= 2) {
				Point rotation = { 1 & (distance / 2), 1 & (distance ^ (1 & (distance / 2))) };
				RotatePoint(quadrantSize, point, rotation);
				point += rotation * quadrantSize;
				distance /= 4;
			}

			return point;
		}

	public: // sequencer
		constexpr Point Get() { return ComputePoint(_index.Get()); }
		constexpr bool Reset() { return _index.Reset(); }
		constexpr bool Next() { return _index.Next(); }
		constexpr size_t Count() { return _index.Count(); }

	public:
		/** Constructor. */
		constexpr HilbertCurve(size_t level) : _gridSize(size_t(1) << level), _index(size_t(1) << (level * 2)) {}
	};

}
