#pragma once
namespace DD {
	/**
	 * IReferential
	 * Interface for objects which should be captured as references.
	 * This optimize behavior, when the object is captured by Reference.
	 */
	struct IReferential;

	/** Standard traits, deallocation via delete operator. */
	struct ReferenceTraits;

	/** Type for simple checking inheritance. */
	struct IReference {};

	/**
	 * Reference
	 * Smart pointer implementation.
	 * @warning Reference does not solve cyclic dependency.
	 *
	 * Reference<T> - standard smart pointer with deallocation via delete operator.
	 * Reference<T, TRAITS> - smart pointer with custom deallocation traits.
	 *
	 * If an objects is designed for Reference usage, it should be derived from IReferential.
	 * It saves allocation calls, also it guarantees the cache coherency.
	 * Reference automatically detects if the pointer is derived from IReferential.
	 *
	 * @example standard usage
	 *   Reference<int> i = new int(42);
	 *
	 * @example referential object
	 *   struct MyObject : public IReferential { ... };
	 *   Reference<MyObject> ref = new MyObject(...);
	 *
	 * @example absolute referential object
	 *   struct MyObjectBase : public IReferential { ... };
	 *   struct MyObject : public Reference<MyObjectBase> { MyObject(...) : Reference<MyObjectBase>(new MyObjectBase(...)) {} }
	 *   MyObject referentialObject;
	 *
	 * @example trait override
	 *   struct ReferenceTraitsDirectX { template<typename T> constexpr static void Delete(T *& ref) { ref->Release(); } };
	 *   template<typename T>
	 *   using DXReference = Reference<T, ReferenceTraitsDirectX>;
	 *   DXReference<ID3D11PixelShader> pixelShader = createPixelShader(...);
	 */
	template<typename T, typename TRAITS = ReferenceTraits>
	class Reference;
}

#include <DD/Concurrency/Atomic.h>
#include <DD/Auxiliaries.h>
#include <DD/TemplateHelpers.h>
#include <DD/Types.h>

namespace DD {
	struct ReferenceTraits {
		template<typename T>
		constexpr static void Delete(T *& ref) { delete ref; }
	};


	struct IReferential {
		template<typename, typename>
		friend class Reference;

	private: // properties
		/** Reference counter. */
		Concurrency::Atomic::i32 _cnt;

	protected: // constructors
		/** Zero constructor, sets counter onto one reference. */
		IReferential() : _cnt(0) {}

	public: // constructors
		/** Destructor. */
		virtual ~IReferential() = default;
	};


	template<typename T, typename TRAITS>
	class Reference
		: IReference
	{
		template<typename, typename>
		friend class Reference;

	public: // typedefs
		/** Reference of different type with the same deallocator. */
		template<typename U>
		using Friend = Reference<U, TRAITS>;

	protected: // properties
		/** Pointer to data. */
		mutable T * _ref = nullptr;
		/** Reference occurrences. */
		mutable Concurrency::Atomic::i32 * _refcount = nullptr;

	public: // accession
		/** Access to stored pointer. */
		constexpr T * Ptr() { return _ref; }
		/** Access to stored pointer. */
		constexpr const T * Ptr() const { return _ref; }
		/** Dereference. */
		constexpr T & operator*() { return *_ref; }
		/** Const dereference. */
		constexpr const T & operator*() const { return *_ref; }
		/** Dereference. */
		constexpr T * operator->() { return _ref; }
		/** Const dereference. */
		constexpr const T * operator->() const { return _ref; }
		/** Conversion operator. */
		constexpr operator T * () { return _ref; }
		/** Conversion operator. */
		constexpr operator const T * () const { return _ref; }

	public: // comparison
		constexpr bool operator==(const T * r) const { return _ref == r; }
		constexpr bool operator!=(const T * r) const { return _ref != r; }
		constexpr bool operator<=(const T * r) const { return _ref <= r; }
		constexpr bool operator>=(const T * r) const { return _ref >= r; }
		constexpr bool operator<(const T * r) const { return _ref < r; }
		constexpr bool operator>(const T * r) const { return _ref > r; }
		template<typename U>
		constexpr bool operator==(const Friend<U> & r) const { return _ref == static_cast<const T *>(r._ref); }
		template<typename U>
		constexpr bool operator!=(const Friend<U> & r) const { return _ref != static_cast<const T *>(r._ref); }
		template<typename U>
		constexpr bool operator<=(const Friend<U> & r) const { return _ref <= static_cast<const T *>(r._ref); }
		template<typename U>
		constexpr bool operator>=(const Friend<U> & r) const { return _ref >= static_cast<const T *>(r._ref); }
		template<typename U>
		constexpr bool operator<(const Friend<U> & r) const { return _ref < static_cast<const T *>(r._ref); }
		template<typename U>
		constexpr bool operator>(const Friend<U> & r) const { return _ref > static_cast<const T *>(r._ref); }

	public: // methods
		/** Count of references. */
		constexpr i32 Count() const { return _refcount ? (i32)*_refcount : 0; }
		/** Null check. */
		constexpr bool IsNull() const { return _ref == nullptr; }
		/** Null check. */
		constexpr bool IsNotNull() const { return _ref != nullptr; }
		/** Releases reference from pointer. */
		constexpr Reference & Release();
		/** Catch pointer into reference holder. */
		constexpr Reference & Catch(T * ptr);
		/** Swap. */
		constexpr void Swap(Reference & rhs) { ::DD::Swap(rhs._ref, _ref); ::DD::Swap(rhs._refcount, _refcount); }

	public: // assignment
		/** Catch assignment. */
		template<typename U>
		constexpr Reference & operator=(U * ptr) { return Release().Catch(static_cast<T *>(ptr)); }
		/** Copy assignment .*/
		template<typename U>
		constexpr Reference & operator=(const Friend<U> &);
		/** Copy assignment .*/
		constexpr Reference & operator=(const Reference & r) { return operator=<T>(r); }
		/** Template move assignment. */
		template<typename U>
		constexpr Reference & operator=(Friend<U> && r);
		/** Release assignment. */
		constexpr Reference & operator=(Null) { Release(); return *this; }

	public: // constructors
		/** Zero constructor. */
		constexpr Reference() = default;
		/** Copy constructor. */
		constexpr Reference(const Reference & r) { *this = r; }
		/** Move constructor. */
		constexpr Reference(Reference && r) { *this = Fwd(r); }
		/** Template copy constructor. */
		template<typename U>
		constexpr Reference(const Friend<U> & r) { *this = r; }
		/** Template catch constructor. */
		template<typename U>
		constexpr Reference(U * ptr) { *this = ptr; }
		/** Destructor. */
		~Reference() { Release(); }
	};
}

namespace DD {
	template<typename T, typename TRAITS>
	constexpr Reference<T, TRAITS> & Reference<T, TRAITS>::Release() {
		if (_ref) {
			if (_refcount->AtomicDec() == 0) {
				TRAITS::Delete(_ref);
				_ref = nullptr;

				// if the counter is stored externally, delete it
				if constexpr (!__is_base_of(IReferential, T))
					delete _refcount;
			}
			else {
				_ref = nullptr;
			}
		}

		return *this;
	}


	template<typename T, typename TRAITS>
	constexpr Reference<T, TRAITS> & Reference<T, TRAITS>::Catch(T * ptr) {
		_ref = ptr;
		// if the counter is stored internally, catch it
		if constexpr (__is_base_of(IReferential, T)) {
			_refcount = &(static_cast<IReferential &>(*ptr)._cnt);
			_refcount->AtomicInc();
		}
		// if the counter is stored externally, create it
		else {
			_refcount = new Concurrency::Atomic::i32(1);
		}

		return *this;
	}


	template<typename T, typename TRAITS>
	template<typename U>
	constexpr Reference<T, TRAITS> & Reference<T, TRAITS>::operator=(Friend<U> && rhs) {
		Release();
		_ref = static_cast<T *>(rhs._ref);
		_refcount = rhs._refcount;
		rhs._ref = nullptr;

		return *this;
	}


	template<typename T, typename TRAITS>
	template<typename U>
	constexpr Reference<T, TRAITS> & Reference<T, TRAITS>::operator=(const Friend<U> & r) {
		if (this != (Reference *)&r) {
			Release();
			_ref = static_cast<T *>(r._ref);
			if (_ref) {
				_refcount = r._refcount;
				_refcount->AtomicInc();
			}
		}

		return *this;
	}
}
