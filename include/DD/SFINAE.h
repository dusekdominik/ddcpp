#pragma once
namespace DD {
	/** Helper for unrefing type. */
	template<typename T>
	T sfinaeUnref(T&);
	/** Helper for unconsting type. */
	template<typename T>
	T sfinaeUnconst(const T);
	/** Helper for unpointering type. */
	template<typename T>
	T sfinaeUnptr(T*);
	/** Size of a collection. Use explicit specialization for undefined classes. */
	template<typename COLLECTION>
	size_t sfinaeSize(const COLLECTION &);
	/** Indexer of a collection. Use explicit specialization for undefined classes. */
	template<typename COLLECTION>
	auto & sfinaeAt(COLLECTION &, size_t);
	/** Indexer of a collection. Use explicit specialization for undefined classes. */
	template<typename COLLECTION>
	const auto & sfinaeAtConst(const COLLECTION &, size_t);
	/** Iterator of a collection. Use explicit specialization for undefined classes. */
	template<typename COLLECTION>
	auto sfinaeBegin(COLLECTION &);
	/** Iterator of a collection. Use explicit specialization for undefined classes. */
	template<typename COLLECTION>
	auto sfinaeBeginConst(const COLLECTION &);
	/** Iterator of a collection. Use explicit specialization for undefined classes. */
	template<typename COLLECTION>
	auto sfinaeEnd(COLLECTION &);
	/** Iterator of a collection. Use explicit specialization for undefined classes. */
	template<typename COLLECTION>
	auto sfinaeEndConst(const COLLECTION &);

	/** Class keeping sfinae functionality out of global scope. */
	struct SFINAE;
	/** Template helper for getting unreferenced type. */
	template<typename T>
	struct SfinaeUnrefType;
	/** */
	template<typename T>
	struct SfinaeUnconstType;
	/** */
	template<typename T>
	struct SfinaeUnptrType;
	/** */
	template<typename T>
	class SfinaeType;

	/** Shortcut for getting unreferenced type.*/
	template<typename T>
	using sfinaeUnrefType = typename SfinaeUnrefType<T>::type_t;
	/** Native type (unconst and unreffed). */
	template<typename T>
	using sfinaeNativeType = typename SfinaeType<T>::native_t;
	/** Shortcut for getting unreferenced type.*/
	template<typename T>
	using sfinaeUnptrType = decltype(sfinaeUnptr(**((T*)nullptr)));
	/** Raw type of result of sfinaeAt. */
	template<typename T>
	using sfinaeAtType = decltype(sfinaeUnref(sfinaeAt(*((T*)nullptr), 0)));
	/** Raw type of result of sfinaeAtConst. */
	template<typename T>
	using sfinaeAtConstType = decltype(sfinaeUnconst(sfinaeUnref(sfinaeAtConst(*((const T*)nullptr), 0))));
	/** Raw type of result of sfinaeBegin. */
	template<typename T>
	using sfinaeBeginType = decltype(sfinaeBegin(*((T*)nullptr)));
	/** Raw type of result of sfinaeBeginConst. */
	template<typename T>
	using sfinaeBeginConstType = decltype(sfinaeBeginConst(*((const T*)nullptr)));
	/** Raw type of result of *sfinaeBegin(). */
	template<typename T>
	using sfinaeBeginTargetType = decltype(sfinaeUnref(**((sfinaeBeginType<T>*)nullptr)));
	/** Raw type of result of *sfinaeBeginConst(). */
	template<typename T>
	using sfinaeBeginConstTargetType = decltype(sfinaeUnconst(sfinaeUnref(**((sfinaeBeginConstType<T>*)nullptr))));

	/** Helper detection method if T is native static array.  */
	template<typename T>
	constexpr bool isArray();
	/** Helper detection method if T is pointer.  */
	template<typename T>
	constexpr bool isPointer();
	/** Helper detection method if T is native string.  */
	template<typename T>
	constexpr bool isNativeString();
}

#include "Macros.h"

#define _DD_DECLARE_SFINAE_CHECK(nameOfCheck, nameOfMethod)\
template<typename YesClass>\
class SFINAE_##nameOfCheck {\
	struct NoClass { void nameOfMethod(int); };\
	typedef struct TestClass : public YesClass, public NoClass {} *TestClassPtr;\
 	template <typename T, T>\
	class Helper {};\
 	static constexpr bool Try(...) { return true; }\
 	template <typename T>\
	static constexpr bool Try(T*, Helper<void(NoClass::*)(int), &T::nameOfMethod>* = 0) { return false; }\
public:\
 	static constexpr bool Value() { return /*!IsNative<YesClass>() &&*/ Try(TestClassPtr()); }\
};
#define _DD_DECLARE_SFINAE_UNIMETHOD_WARNING(x) "T::" #x "(); "
#define _DD_DECLARE_SFINAE_UNIMETHOD_CHECK(index, count, param, arg) _DD_DECLARE_SFINAE_CHECK(index, arg)
#define _DD_DECLARE_SFINAE_UNIMETHOD_DETECT(index, count, param, x) if (SFINAE_##index<COLLECTION>::Value()) return index;
#define _DD_DECLARE_SFINAE_UNIMETHOD_HELPER(index, count, gParam, x)\
template<>\
struct Helper<index> {\
	template<typename C, typename...ARGS>\
	__forceinline static DD_UNWRAP(1, gParam) Method(const C * c, ARGS...args) { DD_UNWRAP(0, gParam) c->x(args...); }\
	template<typename C>\
	__forceinline static DD_UNWRAP(1, gParam) Method(const C * c) { DD_UNWRAP(0, gParam) c->x(); }\
	template<typename C, typename...ARGS>\
	__forceinline static DD_UNWRAP(1, gParam) Method(C * c, ARGS...args) { DD_UNWRAP(0, gParam) c->x(args...); }\
	template<typename C>\
	__forceinline static DD_UNWRAP(1, gParam) Method(C * c) { DD_UNWRAP(0, gParam) c->x(); }\
};
#define _DD_DECLARE_SFINAE_UNIMETHOD(returnMode, returnType, inputMode, name, ...)\
template<typename COLLECTION2>\
struct name {\
	typedef sfinaeUnrefType<COLLECTION2> COLLECTION;\
	DD_XLIST_EX(_DD_DECLARE_SFINAE_UNIMETHOD_CHECK, DD_EMPTY, __VA_ARGS__)\
	template<int> struct Helper {\
		template<typename...ARGS>\
		static returnType Method(ARGS...); /* {\
			static_assert(false, "SFINAE FAILED, Class does not contain any of these methods: " DD_XLIST(_DD_DECLARE_SFINAE_UNIMETHOD_WARNING, __VA_ARGS__) );\
		}*/\
	};\
	DD_XLIST_EX(_DD_DECLARE_SFINAE_UNIMETHOD_HELPER, (returnMode, returnType), __VA_ARGS__)\
	template<bool>\
	static constexpr int DeclareType() { DD_XLIST_EX(_DD_DECLARE_SFINAE_UNIMETHOD_DETECT, DD_EMPTY, __VA_ARGS__) return -1; }\
	template<>\
	static constexpr int DeclareType<false>() { return -1; }\
	static constexpr int DeclareType() { return DeclareType<__is_class(COLLECTION)>(); }\
	static constexpr bool Failed = DeclareType() == -1;\
	static constexpr bool Succeed = DeclareType() != -1;\
	template<typename...ARGS>\
	__forceinline static returnType Method(inputMode COLLECTION * t, ARGS...args) { returnMode Helper<DeclareType()>::template Method<COLLECTION, ARGS...>(t, args...); }\
	__forceinline static returnType Method(inputMode COLLECTION * t) { returnMode Helper<DeclareType()>::template Method<COLLECTION>(t); }\
}

#define _DD_TEMPLATE_VALUE(type, value, ...) template< __VA_ARGS__ >  struct DD_UNWRAP(type) { static constexpr int Value = value; };
#define _DD_TEMPLATE_VALUES_PARSER(index, count, param, arg)\
	 _DD_TEMPLATE_VALUE(\
			DD_UNWRAP(0, param) < DD_UNWRAP(0, arg) > ,\
			DD_UNWRAP(1, param),\
			DD_UNWRAP_TAIL(0, arg)\
		)
#define _DD_TEMPLATE_VALUES(type, value, ...) DD_XLIST_EX(_DD_TEMPLATE_VALUES_PARSER, (type, value), __VA_ARGS__)
namespace DD {
	struct SFINAE {
		enum { TYPE_POINTER, TYPE_ARRAY, TYPE_VALUE };
		/** Template decision between array, pointer and other. */
		_DD_TEMPLATE_VALUE(CheckType, TYPE_VALUE, typename T);
		_DD_TEMPLATE_VALUE(CheckType<T[A]>, TYPE_ARRAY,   typename T, size_t A);
		_DD_TEMPLATE_VALUE(CheckType<T*>,   TYPE_POINTER, typename T);
		/** Template check if type is native string. */
		_DD_TEMPLATE_VALUE(IsNativeString,  false,        typename T);
		_DD_TEMPLATE_VALUES(IsNativeString, true, char*, const char*, wchar_t*, const wchar_t*, (char[A], size_t A), (const char[A], size_t A), (wchar_t[A], size_t A), (const wchar_t[A], size_t A));
		/** Size of a collection. */
		_DD_DECLARE_SFINAE_UNIMETHOD(return, size_t, const, Size, size, Size, getSize, GetSize, count, Count, getCount, GetCount, length, Length, getLength, GetLength);
		/** Indexer method in a collection. */
		_DD_DECLARE_SFINAE_UNIMETHOD(return, auto &, DD_EMPTY, At, operator[], get, Get, at, At, getAt, GetAt);
		/** Const indexer method in a collection */
		_DD_DECLARE_SFINAE_UNIMETHOD(return, const auto &, const, AtConst, operator[], get, Get, at, At, getAt, GetAt);
		/** Begin iterator. */
		_DD_DECLARE_SFINAE_UNIMETHOD(return, auto, DD_EMPTY, Begin, begin, Begin);
		/** Begin const iterator. */
		_DD_DECLARE_SFINAE_UNIMETHOD(return, auto, const, BeginConst, begin, Begin);
		/** End iterator. */
		_DD_DECLARE_SFINAE_UNIMETHOD(return, auto, DD_EMPTY, End, end, End);
		/** End const iterator. */
		_DD_DECLARE_SFINAE_UNIMETHOD(return, auto, const, EndConst, end, End);
		/** isReference stamp method. */
		_DD_DECLARE_SFINAE_UNIMETHOD(DD_EMPTY, void, DD_EMPTY, HasProxy, usingProxy);
		/** Resize method. */
		_DD_DECLARE_SFINAE_UNIMETHOD(DD_EMPTY, void, DD_EMPTY, Resize, Resize, resize, SetSize, setSize);
		/** Serialize method. */
		_DD_DECLARE_SFINAE_UNIMETHOD(DD_EMPTY, void, DD_EMPTY, FromXml, FromXml);
		/** Serialize method. */
		_DD_DECLARE_SFINAE_UNIMETHOD(DD_EMPTY, void, DD_EMPTY, ToXml, ToXml);
	};
}

namespace DD {
	template<typename T>
	struct SfinaeUnrefType { typedef T type_t; };
	template<typename T>
	struct SfinaeUnrefType<T &> { typedef T type_t; };

	template<typename T>
	struct SfinaeUnconstType { typedef T type_t; };
	template<typename T>
	struct SfinaeUnconstType<const T> { typedef T type_t; };

	template<typename T>
	struct SfinaeUnptrType { typedef  T type_t; };
	template<typename T>
	struct SfinaeUnptrType<T *> { typedef T type_t; };

	template<typename T>
	class SfinaeType {
		typedef typename SfinaeUnconstType<T>::type_t unconst_t;
		typedef typename SfinaeUnrefType<unconst_t>::type_t unref_t;
	public:
		typedef unref_t native_t;
	};

	template<typename COLLECTION>
	__forceinline auto & sfinaeAt(COLLECTION & c, size_t i) { return SFINAE::At<COLLECTION>::template Method<size_t>(&c, i); }
	template<typename T, size_t A>
	__forceinline auto & sfinaeAt(T(&c)[A], size_t i) { return c[i]; }

	template<typename COLLECTION>
	__forceinline const auto & sfinaeAtConst(const COLLECTION & c, size_t i) { return SFINAE::AtConst<COLLECTION>::template Method<size_t>(&c, i); }
	template<typename T, size_t A>
	__forceinline const auto & sfinaeAtConst(T(&c)[A], size_t i) { return c[i]; }

	template<typename COLLECTION>
	__forceinline auto sfinaeBegin(COLLECTION & c) { return SFINAE::Begin<COLLECTION>::Method(&c); }
	template<typename T, size_t A>
	__forceinline auto sfinaeBegin(T(&c)[A]) { return c; }

	template<typename COLLECTION>
	__forceinline auto sfinaeBeginConst(const COLLECTION & c) { return SFINAE::BeginConst<COLLECTION>::Method(&c); }
	template<typename T, size_t A>
	__forceinline auto sfinaeBeginConst(const T(&c)[A]) { return c; }

	template<typename COLLECTION>
	__forceinline auto sfinaeEnd(COLLECTION & c) { return SFINAE::End<COLLECTION>::Method(&c); }
	template<typename T, size_t A>
	__forceinline auto sfinaeEnd(T(&c)[A]) { return c + A; }

	template<typename COLLECTION>
	__forceinline auto sfinaeEndConst(const COLLECTION & c) { return SFINAE::EndConst<COLLECTION>::Method(&c); }
	template<typename T, size_t A>
	__forceinline auto sfinaeEndConst(const T(&c)[A]) { return c + A; }

	template<typename COLLECTION>
	__forceinline size_t sfinaeSize(const COLLECTION & c) { return SFINAE::Size<COLLECTION>::Method(&c); }
	template<typename T, size_t A>
	__forceinline size_t sfinaeSize(const T(&)[A]) { return A; }

	template<typename COLLECTION>
	__forceinline void sfinaeResize(COLLECTION & c, size_t size) { SFINAE::Resize<COLLECTION>::template Method<size_t>(&c, size); }

	template<typename T>
	constexpr bool isArray() { return SFINAE::CheckType<T>::Value == SFINAE::TYPE_ARRAY; }

	template<typename T>
	constexpr bool isPointer() { return SFINAE::CheckType<T>::Value == SFINAE::TYPE_POINTER; }

	template<typename T>
	constexpr bool isNativeString() { return SFINAE::IsNativeString<T>::Value == true; }
}

#undef _DD_TEMPLATE_VALUE
#undef _DD_TEMPLATE_VALUES
#undef _DD_TEMPLATE_VALUES_PARSER
#undef _DD_DECLARE_SFINAE_CHECK
#undef _DD_DECLARE_SFINAE_UNIMETHOD
#undef _DD_DECLARE_SFINAE_UNIMETHOD_WARNING
#undef _DD_DECLARE_SFINAE_UNIMETHOD_CHECK
#undef _DD_DECLARE_SFINAE_UNIMETHOD_DETECT
#undef _DD_DECLARE_SFINAE_UNIMETHOD_HELPER
