#pragma once
namespace DD::XML {
	/**
	 * Functionality of XML document.
	 */
	struct DocumentBase;

	/**
	 * XML Document.
	 * Referential object.
	 * Contains declarations of document elements and root node.
	 */
	struct Document;

	/**
	 * Element declaration.
	 * Contains name of an element and attribute names.
	 */
	struct ElementDeclaration;

	/**
	 * Xml Element.
	 * Naive implementation.
	 * @warning element could contain at most 8 attributes
	 * Attribute names and element name is placed in element declaration.
	 * This structure contains just values of attributes.
	 */
	struct Element;

	/**
	 *
	 */
	struct AttributeDeclarationBase;
	/**
	 * Attribute of a xml element.
	 * Key-Value pair.
	 */
	struct AttributeDeclaration;

	/**
	 *
	 */
	struct Attribute;

	/**
	 * Simple structure for reading valid xml document.
	 * If the input is invalid, process could crash.
	 */
	struct Reader;

	/**
	 * Tools for xml serialization and deserialization with xml documents.
	 */
	class DefaultSerializer;

	/** Index. */
	typedef int Index;

	/**
	 * Serialize type to xml.
	 * @example override for special type
	 *   struct MyType {};
	 *   template<>
	 *   void DD::XML::Serialize<MyType>(const MyType & s, DD::XML::Element & t) {
	 *     ...my serialization process...
	 *   }
	 */
	template<typename TYPE>
	static void Serialize(const TYPE & source, Element & target);

	/**
	 * Deserialize type from xml.
	 * @example override for special type
	 *   struct MyType {};
	 *   template<>
	 *   void DD::XML::Serialize<MyType>(const DD::XML::Element & s, const MyType & t) {
	 *     ...my deserialization process...
	 *   }
	 */
	template<typename TYPE>
	static void Deserialize(const Element & source, TYPE & target);
}

#include <DD/BTree.h>
#include <DD/Enum.h>
#include <DD/Flag.h>
#include <DD/List.h>
#include <DD/Reference.h>
#include <DD/SFINAE.h>
#include <DD/String.h>
#include <DD/FileSystem/Path.h>
#include <DD/Sequencer/Driver.h>
#include <DD/Xml/DepthSearchSequencer.h>

namespace DD::XML {
	struct AttributeDeclarationBase
		: public IReferential
	{
		friend struct AttributeDeclaration;

	public: // nested
		/** Type of an attribute. */
		enum Types {
			/** Attribute is printed optionally, only if it contains not empty string. */
			Optional,
			/** Attribute is printed even if it contains empty string. */
			Mandatory
		};

	public: // properties
		/** Type of an attribute. */
		Types _type;
		/** Name of an attribute. */
		StringLocal<16> _name;
		/** Lower case name of an attribute for comparison. */
		StringLocal<16> _lowerName;

	private: // constructors
		/** Simple constructor. */
		AttributeDeclarationBase(const String & name, Types type = Types::Optional);
		/** Zero constructor. */
		AttributeDeclarationBase() {}
	};


	struct AttributeDeclaration
		: public Reference<AttributeDeclarationBase>
	{
		/** Enum declaring optionality of attribute. */
		using Types = AttributeDeclarationBase::Types;

	public: // methods
		/** Create attribute from an declaration. */
		Attribute operator()(const String & Value);

	public: // constructors
		/** Constructor. */
		AttributeDeclaration(const String & name, Types type = Types::Optional) : Reference(new AttributeDeclarationBase(name, type)) {}
		/** Zero constructor, generates null object. */
		AttributeDeclaration() : Reference() {}
	};


	struct Attribute {
	private: // statics
		/** Obtain patterns for escaping. */
		static ArrayView1D<const Text::StringView16> EscapePattern();
		/** Obtain replacement for escaping. */
		static ArrayView1D<const Text::StringView16> EscapeReplacement();

	public: // statics
		/** XML escape. */
		static String & Escape(String & str) { return str.Replace(EscapePattern(), EscapeReplacement()); }
		/** XML escape. */
		static String & Unescape(String & str) { return str.Replace(EscapeReplacement(), EscapePattern()); }

	public: // properties
		/** Name of attribute. */
		AttributeDeclaration Declaration;
		/** Value of attribute. */
		StringLocal<32> Value;

	public: // methods
		/** Get name of the attribute, currently XML namespace is part of name. */
		Text::StringView16 Name() const { return Declaration->_name; }

	public: // constructors
		/** Constructor. */
		Attribute(AttributeDeclaration declaration, const String & value);
		/** Zero constructor. */
		Attribute() = default;
		/** Move constructor. */
		Attribute(Attribute &&) = default;
	};


	struct ElementDeclarationBase
		: public IReferential
	{
		friend struct ElementDeclaration;

	public: // properties
		/** Estimate of chars to be printed. */
		size_t _size;
		/** Name of an element and its namespace. */
		StringLocal<16> WholeName;
		/** Just namespace without name. */
		Text::StringView16 Namespace;
		/** Name without namespaces. */
		Text::StringView16 Name;
		/** Attributes of an element. */
		List<AttributeDeclaration, 8> Attributes;

	public: // methods
		/**
		 * Look-up index of attribute by given name of an attribute.
		 * @return -1 if attribute is not contained.
		 */
		AttributeDeclaration GetAttributeDeclConst(const Text::StringView16 & name) const;
		/** Look-up index of attribute by given. If the name is not contained, register it. */
		AttributeDeclaration GetAttributeDecl(const Text::StringView16 & name);
		/** Estimate of chars to be printed. */
		size_t GetSizeEstimate() const { return _size; }

	protected: // constructors
		/** Constructor. */
		template<typename...ATTRIBUTES>
		ElementDeclarationBase(const Text::StringView16 &, const ATTRIBUTES &...);
	};


	struct ElementDeclaration
		: public Reference<ElementDeclarationBase>
	{
		/** Zero constructor. */
		ElementDeclaration() : Reference() {}
		/**
		 * Basic constructor.
		 * @param name - name of the element.
		 * @param attributes - declaration of attributes of type @see AttributeDeclaration.
		 */
		template<typename...ATTRIBUTES>
		ElementDeclaration(Text::StringView16 name, const ATTRIBUTES &...args)
			: Reference(new ElementDeclarationBase(name, args...))
		{}
	};


	struct Element {
		friend struct DocumentBase;

	public: // nested
		/** Type of an element. */
		enum struct Types {
			/** Type is tag. */
			TYPE_TAG,
			/** Type is just plain text. */
			TYPE_TEXT
		};

	protected: // statics
		/** Obtain patterns for escaping. */
		static ArrayView1D<const Text::StringView16> EscapePattern();
		/** Obtain replacement for escaping. */
		static ArrayView1D<const Text::StringView16> EscapeReplacement();

	public: // statics
		/** XML escape. */
		static String & Escape(String & str) { return str.Replace(EscapePattern(), EscapeReplacement()); }
		/** XML escape. */
		static String & Unescape(String & str) { return str.Replace(EscapeReplacement(), EscapePattern()); }

	public: // properties
		/** Pointer to document for item declaration. */
		DocumentBase * Document;
		/** Children elements if any. */
		List<Element> Children;
		/** Attributes of an element. */
		List<Attribute, 8> Attributes;
		/** Declaration of element name and its arguments. Null if element is TYPE_TEXT. */
		ElementDeclaration Declaration;
		/** Text of element if element is TYPE_TEXT. */
		String Text;

	protected: // methods
		/** Append this element (if TYPE_TEXT) as text to given string @param acc with given number of indent tabs via @param indent. */
		void appendMeAsText(String & acc, size_t indent) const;
		/** Append this element (if TYPE_TAG) as text to given string @param acc with given number of indent tabs via @param indent. */
		void appendMeAsTag(String & acc, size_t indent) const;

	public: // methods
		/** Type of the element. */
		Types GetType() const;
		/** Get name of the element. */
		Text::StringView16 GetName() const;
		/** Get name with namespace */
		Text::StringView16 GetWholeName() const { return Declaration ? (Text::StringView16)Declaration->WholeName : Text::StringView16(nullptr); }
		/** Get namespace. */
		Text::StringView16 GetNamespace() const { return Declaration ? (Text::StringView16)Declaration->Namespace : Text::StringView16(nullptr); }
		/** Clear values of attributes. */
		void ClearAttributes() { Attributes.Clear(); }
		/** Get sequencer browsing all the elements. */
		auto Search(Xml::DepthSearchSequencer::Acception a = Xml::DepthSearchSequencer::Acception()) { return Sequencer::Drive(Xml::DepthSearchSequencer(*this, a)); }
		/** Get a sequence of descendants of the given tag name. */
		auto SearchByTagName(const String & tag) { return Search().Filter([tag](Element & e) { return tag == e.GetName(); }); }
		/**  */
		auto SearchByAttribute(Text::StringView16 n, Text::StringView16 v) { return Search().Filter([=](Element & e) { return e[n] == v; }); }
		/**  */
		auto SearchById(Text::StringView16 v) { return SearchByAttribute(L"id", v); }

	public: // methods
		/** Get estimation of characters to be printed, @param depth serves for estimation of indent. */
		size_t GetSizeEstimate(size_t depth = 0) const;
		/** Appends XML element as string into string accumulator (@param acc) with given indent (@param indent). */
		void AppendMe(String & acc, size_t indent = 0) const;
		/** Defines element as sub-element. */
		Element & DefineElement(ElementDeclaration declaration);
		/** Defines element as sub-element. */
		Element & DefineElement(Text::StringView16 elementName);
		/** Create attribute with new decl if not exist or witch existing decl. */
		Element & DefineAttribute(Text::StringView16 name, Text::StringView16 value);
		/**
		 * Defines element as sub-element.
		 * @param declaration - declaration of element.
		 * @param attributes  - attributes of type DD::XML::Attribute.
		 */
		template<typename...ATTRIBUTES>
		Element & DefineElement(ElementDeclaration declaration, ATTRIBUTES &&...);
		/** Defines text element as sub-element. */
		Element & DefineText(const Text::StringView16 & text);

	public: // operators
		/** Returns attribute value by given attribute name. */
		String & operator[](const Text::StringView16 & name);
		/** Returns attribute value by given attribute name. */
		Text::StringView16 operator[](const Text::StringView16 & name) const;
		Text::StringView16 GetAttribute(Text::StringView16 name) const;

	protected: // constructors
		/** Tag constructor. */
		Element(DocumentBase * document, ElementDeclaration declaration);

	public: // constructors
		/** Move constructor. */
		Element(Element && rvalue) = default;
		/** Zero constructor. */
		Element() = default;
	};


	struct DocumentBase
		: public IReferential
	{
		friend struct Document;
		friend struct Reader;

	private: // properties
		/** Register of element declarations. */
		BTree<String, ElementDeclaration> _register;
		/** Root of document. */
		Element _root;

	private: // methods
		/** Fill header to given stream. */
		void appendHeaderUTF16(String & acc) const;
		/** Fill header to given stream. */
		void appendHeaderUTF8(String & acc) const;

	public: // method
		/** Returns reference to root. */
		Element & GetRoot() { return _root; }
		/** Returns reference to root. */
		const Element & GetRoot() const { return _root; }
		/** Creates or look up declaration of given name. */
		template<typename...ATTRIBUTES>
		ElementDeclaration DeclareElement(Text::StringView16 name, const ATTRIBUTES &...);
		/** Converts document to string. */
		String ToStringUtf16() const;
		String ToStringUtf8() const;
		String ToString() const { return ToStringUtf16(); };

	public: // operators
		/** Conversion to xml element. */
		operator const Element &() const { return _root; }

	private: // constructors
		/** Constructor. */
		template<typename...ATTRIBUTES>
		DocumentBase(Text::StringView16 name, const ATTRIBUTES &...);
		/** Zero constructor. */
		DocumentBase() : _root(this, ElementDeclaration()) {}
	};


	struct Document
		: public Reference<DocumentBase>
	{
		/** Use assignment from parent struct. */
		using Reference<DocumentBase>::operator=;
		/** Automatic conversion from document to Xml::Element. */
		operator Element &() { return Ptr()->GetRoot(); }
		/** Automatic conversion from document to Xml::Element. */
		operator const Element &() const { return Ptr()->GetRoot(); }
		/** Load file encoded in UTF8. */
		bool LoadUtf8(const FileSystem::Path & path);
		/** Zero constructor - null object. */
		Document() : Reference<DocumentBase>() {}
		/** Create new document by given name of root element. */
		template<typename...ATTRIBUTES>
		Document(Text::StringView16 name, const ATTRIBUTES &...attributes) : Reference<DocumentBase>(new DocumentBase(name, attributes...)) {};
	};


	struct Reader {
		/** Internal states of reader. */
		enum { INIT, READ, FAIL, DONE };

	protected: // properties;
		/** Internal state of reader. */
		int _state;
		/** Target of loading. */
		Document _doc;

	protected: // methods
		/** Initialize reader, */
		void init();
		/** Read a char. */
		void read(wchar_t c);
		/** Finish reading.  */
		void flush();
		/** Check if reading finished. */
		bool done() const { return _state > READ; }

	public: // methods
		/** Document. */
		const Document & GetDocument() const { return _doc; }
		/** Read from stream char by char. */
		template<typename STREAM>
		void ReadFromStream(STREAM & stream) { init(); wchar_t c; while (stream && !done()) { stream >> c; read(c); } flush(); }
		/** Iterate over chars. */
		template<typename ITERABLE>
		void ReadFormIterable(const ITERABLE & collection) { init(); for (wchar_t c : collection) { read(c); if (done()) break; } flush(); }
		/** Is reading successfully finished. */
		bool Succeed() const { return _state == DONE; }

	public: // constructors
		/** Constructor. */
		Reader() : _state(INIT), _doc() { _doc = new DocumentBase(); }
	};


	class DefaultSerializer {
		/** Types of serializers. */
		enum {
			UNDEFINED,  // undefined - throws errors
			INCLASS,    // inclass - class contains method for xml serialization
			NATIVE,     // type is native, so xml serialization has a implicit format
			ARRAY,      // array of fixed size (whole array is serialized no matter what it stores)
			DARRAY,     // dynamic array (kind of list) - only used items are serialized
			REFERENCE,  // DD::IReference ancestors.
			PROXY,      // Objects like DD::ThreadSafeObject
			STRING,     // char or wchar strings
			DD_STRING,  // DD::String
			DD_ENUM,    // DD::IEnum descendants
			DD_IFLAG,   // DD::IFlag descendants
		};
		/** Decision which serializer is well suited for given @param TYPE. */
		template<typename TYPE>
		static constexpr int type();
		/** Serializer classes, @see specialized templates for defined serializers */
		template<int> struct Helper {
			template<typename T> static void serialize(const T &, Element &); /* { static_assert(false, "Undefined serialization."); }*/
			template<typename T> static void deserialize(const Element &, T &); /*{ static_assert(false, "Undefined serialization."); }*/
		};
		template<> struct Helper<ARRAY> {
			static Text::StringView16 ArrayItem() { return L"ArrayItem"; }
			static Text::StringView16 Size() { return L"Size"; }
			static Text::StringView16 Index() { return L"Index"; }
			template<typename T> static void serialize(const T & source, Element & target);
			template<typename T> static void deserialize(const Element & source, T & target);
		};
		template<> struct Helper<DARRAY> {
			static Text::StringView16 Size() { return L"Size"; }
			template<typename T> static void serialize(const T & source, Element & target) { Helper<ARRAY>::serialize(source, target); }
			template<typename T> static void deserialize(const Element & source, T & target);
		};
		template<> struct Helper<INCLASS> {
			template<typename T> static void serialize(const T & source, Element & target) { source.ToXml(target); }
			template<typename T> static void deserialize(const Element & source, T & target) { target.FromXml(source); }
		};
		template<> struct Helper<NATIVE> {
			static Text::StringView16 Value() { return L"Value"; }
			template<typename T> static void serialize(const T & source, Element & target) { target[Value()].Clear().Append(source); }
			template<typename T> static void deserialize(const Element & source, T & target) { source[Value()].TryCast(target); }
		};
		template<> struct Helper<REFERENCE> {
			template<typename T> static void serialize(const T & source, Element & target) { XML::Serialize(*source, target); }
			template<typename T> static void deserialize(const Element & source, T & target) { XML::Deserialize(source, *target); }
		};
		template<> struct Helper<PROXY> {
			template<typename T> static void serialize(const T & source, Element & target) { auto proxy = *source; XML::Serialize(*proxy, target); }
			template<typename T> static void deserialize(const Element & source, T & target) { auto proxy = *target; XML::Deserialize(source, *proxy); }
		};
		template<> struct Helper<STRING> {
			template<typename T> static void serialize(const T &, Element &); /*{ static_assert(false, "Not implemented"); }*/
			template<typename T> static void deserialize(const Element &, T &); /*{ static_assert(false, "Not implemented"); }*/
		};
		template<> struct Helper<DD_STRING> {
			template<typename T> static void serialize(const T & source, Element & target) { target.DefineText(source); }
			template<typename T> static void deserialize(const Element & source, T & target) { target = source.Children.First().Text; }
		};
		template<> struct Helper<DD_ENUM> {
			template<typename T> static void serialize(const T & source, Element & target) { target.DefineText(source.ToString()); }
			template<typename T> static void deserialize(const Element & source, T & target) { target = T::FromString(source.Children.First().Text); }
		};
		template<> struct Helper<DD_IFLAG> {
			template<typename T> static void serialize(const T & source, Element & target) { StringLocal<64> buffer; T::ToString(source, buffer); target.DefineText(buffer); }
			template<typename T> static void deserialize(const Element & source, T & target) { T::FromString(source.Children.First().Text, target); }
		};
		template<int> struct Assignment { template<typename T> static void Assign(const T & s, T & t) { t = s; } };
		template<> struct Assignment<ARRAY> { template<typename T> static void Assign(const T & s, T & t) { for (size_t i = 0, m = sfinaeSize(s); i < m; ++i) DefaultSerializer::Assign(s[i], t[i]); } };
		template<> struct Assignment<DARRAY> { template<typename T> static void Assign(const T & s, T & t) { sfinaeResize(t, sfinaeSize(s)); for (size_t i = 0, m = sfinaeSize(s); i < m; ++i) DefaultSerializer::Assign(s[i], t[i]); } };

	public: // methods
		/** Safe assignment, balances problems with not assignable classes. */
		template<typename T>
		static void Assign(const T & source, T & target) { Assignment<type<T>()>::Assign(source, target); }
		/** Serialize a source object to s Xml element. */
		template<typename TYPE>
		static void Serialize(const TYPE & source, Element & target) { Helper<type<TYPE>()>::serialize(source, target); }
		/** Deserialize a source object from a Xml element. */
		template<typename TYPE>
		static void Deserialize(const Element & source, TYPE & target) { Helper<type<TYPE>()>::deserialize(source, target); }
	};

	template<typename TYPE>
	void Serialize(const TYPE & source, Element & target) { DefaultSerializer::Serialize(source, target); }

	template<typename TYPE>
	void Deserialize(const Element & source, TYPE & target) { DefaultSerializer::Deserialize(source, target); }
}


namespace DD::XML {
	template<typename...ATTRIBUTES>
	inline ElementDeclaration DocumentBase::DeclareElement(Text::StringView16 name, const ATTRIBUTES &...attributes) {
		ElementDeclaration & ref = _register[name];
		if (ref == nullptr) {
			ref = ElementDeclaration(name, attributes...);
		}
		return ref;
	}

	template<typename...ATTRIBUTES>
	inline DocumentBase::DocumentBase(Text::StringView16 name, const ATTRIBUTES &...attributes)
		: _root(this, DeclareElement(name, attributes...))
	{}

	template<typename...ATTRIBUTES>
	inline ElementDeclarationBase::ElementDeclarationBase(const Text::StringView16 & name, const ATTRIBUTES &...args)
		: WholeName(name)
	{
		// scan for delimiter
		size_t nsdelimiter = WholeName.View().IndexOf(L':');
		// if scan is valid, split the whole name between namespace and name
		if (nsdelimiter < WholeName.Length()) {
			Namespace = WholeName.View().HeadView(nsdelimiter);
			Name = WholeName.View().SubView(nsdelimiter + 1);
		}
		else {
			Namespace = nullptr;
			Name = WholeName;
		}

		//Attributes.Add(args...);
		_size = Attributes.Size() * 4; // =""
		_size += (WholeName.Length() * 2) + 5; //<></>
		for (AttributeDeclaration & a : Attributes) {
			_size += a->_name.Length();
		}
	}

	template<typename...ATTRIBUTES>
	inline Element & Element::DefineElement(ElementDeclaration declaration, ATTRIBUTES &&...args) {
		Element & result = DefineElement(declaration);
		result.Document = Document;
		result.Attributes.Add(Fwd(args)...);
		return result;
	}

	template<typename TYPE>
	constexpr int DD::XML::DefaultSerializer::type() {
		if constexpr (SFINAE::ToXml<TYPE>::Succeed && SFINAE::FromXml<TYPE>::Succeed)
			return INCLASS;
		if constexpr (__is_base_of(IEnum, TYPE))
			return DD_ENUM;
		if constexpr (__is_base_of(IFlag, TYPE))
			return DD_IFLAG;
		if constexpr (isNativeString<TYPE>())
			return STRING;
		if constexpr (__is_base_of(String, TYPE) || __is_base_of(Text::StringView16, TYPE))
			return DD_STRING;
		if constexpr (SFINAE::HasProxy<TYPE>::Succeed)
			return PROXY;
		if constexpr (isPointer<TYPE>() || __is_base_of(IReference, TYPE))
			return REFERENCE;
		if constexpr (SFINAE::Resize<TYPE>::Succeed)
			return DARRAY;
		if constexpr (SFINAE::Begin<TYPE>::Succeed || isArray<TYPE>())
			return ARRAY;
		if constexpr (!__is_class(TYPE) && !__is_union(TYPE))
			return NATIVE;

		return UNDEFINED;
	}

	template<typename T>
	void DD::XML::DefaultSerializer::Helper<DefaultSerializer::ARRAY>::serialize(const T & source, Element & target) {
		target[L"size"].Clear().Append(sfinaeSize(source));
		size_t index = 0;
		for (const auto & i : source) {
			auto & item = target.DefineElement(ArrayItem());
			item[Index()].Clear().Append(index++);
			DefaultSerializer::Serialize(i, item);
		}
	}

	template<typename T>
	void DD::XML::DefaultSerializer::Helper<DefaultSerializer::ARRAY>::deserialize(const Element & source, T & target) {
		size_t xmlSize, cppSize;
		source[Size()].TryCast(xmlSize);
		cppSize = sfinaeSize(target);
		if (cppSize == xmlSize && xmlSize == source.Children.Size()) {
			for (const auto & child : source.Children) {
				size_t xmlIndex;
				child[Index()].TryCast(xmlIndex);
				if (0 <= xmlIndex && xmlIndex < xmlSize)
					DefaultSerializer::Deserialize(child, target[xmlIndex]);
			}
		}
	}

	template<typename T>
	void DD::XML::DefaultSerializer::Helper<DefaultSerializer::DARRAY>::deserialize(const Element & source, T & target) {
		size_t size;
		source[Size()].TryCast(size);
		sfinaeResize(target, size);
		Helper<ARRAY>::deserialize(source, target);
	}
}

/** Fwd helpers. */
#define _DD_XML_WRAP_ATTRIBUTE(...)            __VA_ARGS__
#define _DD_XML_WRAP_ELEMENT(...)              __VA_ARGS__
#define _DD_XML(method, type)                  _DD_XML_##method##_##type
/** Forwarder. */
#define _DD_XML_FWD(fwd, ...)                  _DD_XML_##fwd(_DD_XML(WRAP, __VA_ARGS__))
/** Serialize attributes and elements. */
#define _DD_XML_DESERIALIZE(...)               _DD_XML(DESERIALIZE, __VA_ARGS__)
#define _DD_XML_DESERIALIZE_
#define _DD_XML_DESERIALIZE_ATTRIBUTE(...)     { _DD_XML_TYPE(__VA_ARGS__) temp; source[DD_SCAT_16(_DD_XML_NAME(__VA_ARGS__))].TryCast(temp); _DD_XML_SET(__VA_ARGS__)(temp); }
#define _DD_XML_DESERIALIZE_ELEMENT(...)
/** Serialize elements. */
#define _DD_XML_SERIALIZE(...)                 _DD_XML(SERIALIZE, __VA_ARGS__)
#define _DD_XML_SERIALIZE_
#define _DD_XML_SERIALIZE_ATTRIBUTE(...)       target.DefineAttribute(DD_SCAT_16(_DD_XML_NAME(__VA_ARGS__)), ::DD::StringLocal<32>::From(_DD_XML_GET(__VA_ARGS__)));
#define _DD_XML_SERIALIZE_ELEMENT(...)         ::DD::XML::Serialize(_DD_XML_GET(__VA_ARGS__), target.DefineElement(DD_SCAT_16(_DD_XML_NAME(__VA_ARGS__))));
/** Enumeration of attributes. */
#define _DD_XML_ENUM_ATTRIBUTE(...)            _DD_XML(ENUM_ATTRIBUTE, __VA_ARGS__)
#define _DD_XML_ENUM_ATTRIBUTE_ATTRIBUTE(...)  _DD_XML_NAME(__VA_ARGS__) = ::DD::Text::StringView(DD_SCAT_16(_DD_XML_NAME(__VA_ARGS__))).Hash(),
#define _DD_XML_ENUM_ATTRIBUTE_ELEMENT(...)
/** Enumeration of elements. */
#define _DD_XML_ENUM_ELEMENT(...)              _DD_XML(ENUM_ELEMENT, __VA_ARGS__)
#define _DD_XML_ENUM_ELEMENT_ATTRIBUTE(...)
#define _DD_XML_ENUM_ELEMENT_ELEMENT(...)      _DD_XML_NAME(__VA_ARGS__) = ::DD::Text::StringView(DD_SCAT_16(_DD_XML_NAME(__VA_ARGS__))).Hash(),
/** Set element by hash and xml attributes. */
#define _DD_XML_SET_XML(...)                   _DD_XML(SET_XML, __VA_ARGS__)
#define _DD_XML_SET_XML_
#define _DD_XML_SET_XML_ATTRIBUTE(...)
#define _DD_XML_SET_XML_ELEMENT(...)           case Elements::_DD_XML_NAME(__VA_ARGS__): { _DD_XML_TYPE(__VA_ARGS__) temp;  ::DD::XML::Deserialize(child, temp); _DD_XML_SET(__VA_ARGS__)(::DD::Fwd(temp)); } break;
/** Get item name. */
#define _DD_XML_NAME(...)                      _DD_OVERLOAD(_DD_XML_NAME, __VA_ARGS__)
#define _DD_XML_NAME_1(a1)                     a1
#define _DD_XML_NAME_2(a1, a2)                 a2
#define _DD_XML_NAME_3(a1, a2)                 a3
/** Get value. */
#define _DD_XML_GET(...)                       _DD_OVERLOAD(_DD_XML_GET, __VA_ARGS__)
#define _DD_XML_GET_1(a1)                      a1
#define _DD_XML_GET_2(a1, ...)                 a1
#define _DD_XML_GET_3(a1, ...)                 a1()
/** */
#define _DD_XML_SET(...)                       _DD_OVERLOAD(_DD_XML_SET, __VA_ARGS__)
#define _DD_XML_SET_1(a1)                      a1 =
#define _DD_XML_SET_2(a1, ...)                 a1 =
#define _DD_XML_SET_3(a1, a2, ...)             a2
/** */
#define _DD_XML_TYPE(...)                      decltype(sfinaeUnref(_DD_XML_GET(__VA_ARGS__)))

/**
 * Generate serialization arguments.
 *
 * @example common usage
 *  class Test {
 *     int _attribute0, _attribute1;
 *     int _element0, _element1;
 *     int x;
 *     int GetX() { return x; }
 *     void SetX(int y) { x = y; }
 *     DD_XML_SERIALIZATION(
 *       ATTRIBUTE(_attribute0),
 *       ATTRIBUTE(_attribute1, NiceNameOfAttr),
 *       ATTRIBUTE(GetX, SetX, X)
 *       ELEMENT(_elementValue),
 *       ELEMENT(_elementValue, NiceNameOfElement),
 *     );
 *  };
 *  Example produces:
 *  <*** _attribute0="0" NiceNameOfAttribute="0" X="0">
 *    <_elementValue value="0"/>
 *    <NiceNameOfElement value="0"/>
 *  </***>
 */
#define DD_XML_SERIALIZATION(...)\
public: /*nested*/\
	/** Enum with names of serialized items as xml attributes */\
	enum struct Attributes : ::DD::u64 { DD_XLIST(_DD_XML_ENUM_ATTRIBUTE, __VA_ARGS__) };\
	/** Enum with names of serialized items as xml sub-elements. */\
	enum struct Elements : ::DD::u64 { DD_XLIST(_DD_XML_ENUM_ELEMENT, __VA_ARGS__) };\
private: /*methods*/\
	/** Disable following warnings. */\
	__pragma(warning(push))\
	/** The formal parameter is not referenced in the body of the function. */\
	__pragma(warning(disable:4100))\
	/** Switch statement contains no 'case' or 'default' labels. */\
	__pragma(warning(disable:4060))\
	/** Deserialize all attributes from given element. */\
	void deserializeAttributes(const ::DD::XML::Element & source) { DD_XLIST(_DD_XML_DESERIALIZE, __VA_ARGS__); }	\
	/** Deserialize all children from given element. */\
	void deserializeElements(const ::DD::XML::Element & source) { for (auto & child : source.Children) { switch ((Elements)child.GetName().Hash()) { DD_XLIST(_DD_XML_SET_XML, __VA_ARGS__) } } }\
	/** Reset disabled warnings. */\
	__pragma(warning(pop))\
public: /*methods*/\
	/** Deserialize item from given element. */\
	void FromXml(const ::DD::XML::Element & source) { deserializeAttributes(source); deserializeElements(source); }\
	/** Serialize to given target. */\
	void ToXml(::DD::XML::Element & target) const { DD_XLIST(_DD_XML_SERIALIZE, __VA_ARGS__) }


/** External serialization. */
namespace DD::Xml::Mini {
	template<typename T>
	bool Deserialize(const ::DD::XML::Attribute & source, T & target);


	template<typename T>
	bool Deserialize(const ::DD::Text::StringView16 & source, T & target, const T & defaultValue) {
		if (source.TryCast(target))
			return true;

		target = defaultValue;
		return false;
	}


	inline bool Deserialize(
		const ::DD::Text::StringView16 & source,
		::DD::Text::StringView16 & target,
		const ::DD::Text::StringView16 & defaultValue
	) {
		Discard(defaultValue);

		target = source;
		return true;
	}


	template<typename T>
	bool Deserialize(const ::DD::XML::Element & source, T & target) {
		for (const ::DD::XML::Attribute & attribute : source.Attributes) {
			if (!Deserialize(attribute, target)) {
				/* Log invalid thing */;
			}
		}

		return true;
	}
}


#define _DD_XML_MINI_EXPAND(method, attribute) _DD_XML_MINI_##method##_##attribute
#define _DD_XML_MINI_XNAME_ATTRIBUTE(xmlName, variable, defaultValue) L###xmlName
#define _DD_XML_MINI_NAME_ATTRIBUTE(xmlName, variable, defaultValue) name
#define _DD_XML_MINI_DVAL_ATTRIBUTE(xmlName, variable, defaultValue) defaultValue
#define _DD_XML_MINI_VAR_ATTRIBUTE(xmlName, variable, defaultValue)  target.variable
#define _DD_XML_MINI_CASE_SET(i, n, fa, ua)                                                \
  case fa(_DD_XML_MINI_EXPAND(XNAME, ua)):                                                 \
    return ::DD::Xml::Mini::Deserialize(source.Value, _DD_XML_MINI_EXPAND(VAR, ua), _DD_XML_MINI_EXPAND(DVAL, ua));
/**
 * External serializer, should be placed outside namespaces.
 * Defines external method for serialization of xml data.
 * @param type - typename of the class that should be serialized.
 * @param hash - function for hashing strings of the xml names.
 * @param ...  - attribute definitions
 *             - ATTRIBUTE(attribute, property, defaultValue)
 *               attribute    - string used for attribute detection
 *               property     - name of the property of the given typename
 *               defaultValue - value used whether deserialization fails
 * @example common usage
 *   constexpr auto MyHashFnc(::DD::Text::StringView16 str) { return str.Hash(); }
 *   struct MyStruct { int A, B; };
 *   DD_XML_MINI(MyStruct, MyHashFnc, ATTRIBUTE(a, A, 0), ATTRIBUTE(b, B, 0));
 */
#define DD_XML_MINI(type, hash, ...)                                                             \
  template<>                                                                                     \
  inline bool ::DD::Xml::Mini::Deserialize(const ::DD::XML::Attribute & source, type & target) { \
    switch (hash(source.Name())) {                                                               \
      DD_XLIST_EX(_DD_XML_MINI_CASE_SET, hash, __VA_ARGS__)                                      \
      default: return false;                                                                     \
    }                                                                                            \
  }

