#pragma once
namespace DD {
	/**
	 * Signature signature of functions or functors.
	 * signature_t - method signature
	 * return_t    - return type of signature
	 */
	template<typename LAMBDA>
	struct SignatureEx;

	template<typename LAMBDA>
	using Signature = typename SignatureEx<LAMBDA>::signature_t;

	template<typename LAMBDA>
	using SignatureR = typename SignatureEx<LAMBDA>::return_raw_t;

	template<typename LAMBDA>
	using SignatureRU = typename SignatureEx<LAMBDA>::return_t;
}

#include "SFINAE.h"

namespace DD {
	template<typename LAMBDA>
	struct SignatureEx : public SignatureEx<decltype(&LAMBDA::operator())> {};

	/** Lambda. */
	template<typename LAMBDA, typename RETURN, typename... ARGS>
	struct SignatureEx<RETURN(LAMBDA::*)(ARGS...) const> {
		/** Function typedef. */
		typedef RETURN(signature_t)(ARGS...);
		/** Return type of given signature. */
		typedef RETURN return_raw_t;
		/** Return type (reference removed if exists). */
		typedef sfinaeUnrefType<RETURN> return_t;
	};

	/** Mutable lambda */
	template<typename LAMBDA, typename RETURN, typename...ARGS>
	struct SignatureEx<RETURN(LAMBDA::*)(ARGS...)> {
		/** Function typedef. */
		typedef RETURN(signature_t)(ARGS...);
		/** Return type of given signature. */
		typedef RETURN return_raw_t;
		/** Return type (reference removed if exists). */
		typedef sfinaeUnrefType<RETURN> return_t;
	};

	/** Function pointer. */
	template<typename RETURN, typename... ARGS>
	struct SignatureEx<RETURN(*)(ARGS...)> {
		/** Function typedef. */
		typedef RETURN(signature_t)(ARGS...);
		/** Return type of given signature. */
		typedef RETURN return_raw_t;
		/** Return type (reference removed if exists). */
		typedef sfinaeUnrefType<RETURN> return_t;
	};
}