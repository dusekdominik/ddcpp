#pragma once
namespace DD::Input {
	/** Input line, text + cursor + selection. */
	struct Line;
}

#include <DD/Math/Vector.h>
#include <DD/String.h>
#include <DD/Text/StringReader.h>

namespace DD::Input {
	struct Line {
	public: // nested
		/** Indexing type for cursor. */
		using CursorType = size_t;
		/** Type for storing selection range. */
		using SelectionType = Math::CommonVector<CursorType, 2>;

	public: // statics
		/** Constant denoting invalid cursor. */
		static constexpr CursorType INVALID = static_cast<CursorType>(-1);

	public: // properties
		/** Storage of the line. */
		String Text;
		/** Cursor index. */
		CursorType Cursor = 0;
		/** Selection - begin index, end index. */
		SelectionType Selection = { INVALID, INVALID };

	public: // public interface
		/** Common keyboard input. */
		void PressDelete() { (Selection != INVALID) ? DeleteSelection() : DeleteCursor(); }
		void PressBackSpace() { (Selection != INVALID) ? DeleteSelection() : BackspaceCursor(); }
		void PressLeft(bool shift) { MoveCursor(Cursor - 1, shift); }
		void PressRight(bool shift) { MoveCursor(Cursor + 1, shift); }
		void PressHome(bool shift) { MoveCursor(0, shift); }
		void PressEnd(bool shift) { MoveCursor(Text.Length(), shift); }
		void PressLetter(wchar_t c) { if (Selection != INVALID) DeleteSelection(); Text.InsertChar(Cursor++, c); }
		/** Designed for ctrl+v. */
		void Paste(Text::StringView16 w) { if (IsSelectionValid()) PressDelete(); for (wchar_t l : w) PressLetter(l); }
		/** Designed for ctrl+v. */
		Text::StringView16 Copy() const;
		/** Select all. */
		void SelectAll() { Selection = { 0, Text.Length() }; }
		/** Set command line text. */
		void Set(const Text::StringView16 & value, CursorType cursor = INVALID);
		/** Remove line text. */
		void Clear() { Set(nullptr); }
		/** Nothing is selected and the cursor is at the end of the line. */
		bool IsCursorNaive() const { return Selection == INVALID && Cursor == Text.Length(); }
		/** Is anything selected. */
		bool IsSelectionValid() const { return Selection != INVALID; }

	public: // internal interface
		/** Delete char at cursor. */
		void DeleteCursor() { Text.RemoveChar(Cursor); }
		/** Delete char before cursor, move cursor back. */
		void BackspaceCursor();
		/** Delete text at selection if exists. */
		void DeleteSelection();
		/** Move cursor, make selection if shift is pressed. */
		void MoveCursor(CursorType whereTo, bool shift);
		/** Select word where is the given index. */
		void SelectText(Text::StringView16 selectedText);
		/** Select next word from cursor's position. */
		void NextWord(bool backward = false);
	};
}
