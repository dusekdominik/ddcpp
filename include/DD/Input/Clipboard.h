#pragma once
namespace DD::Input {
	/**
	 * Clipboard.
	 * Serves for copy paste text-operations.
	 */
	struct Clipboard;
}

#include <DD/Singleton.h>
#include <DD/String.h>
#include <DD/Types.h>

namespace DD::Input {
	struct Clipboard
		: public Singleton<Clipboard>
	{
		/** Get current data saved in clipboard. */
		String Get(u32 history = 0);
		/** Set current clipboard data. */
		bool Set(const Text::StringView16 & value);
	};
}
