#pragma once
namespace DD::Input {
	/**
	 * Enumeration of keyboard keys.
	 * Value of enum represents VK_CODE.
	 * @see https://docs.microsoft.com/en-us/windows/desktop/inputdev/virtual-key-codes
	 */
	class Key;
}

#include <DD/Enum.h>

#define KEY_VALUES(KEY)\
	KEY(  MouseLeft, 0x01)\
	KEY( MouseRight, 0x02)\
	KEY(     Cancel, 0x03)\
	KEY(MouseMiddle, 0x04)\
	KEY(   XButton1, 0x05)\
	KEY(   XButton2, 0x06)\
	KEY(  Backspace, 0x08)\
	KEY(        Tab, 0x09)\
	KEY(      Enter, 0x0D)\
	KEY(      Shift, 0x10)\
	KEY(       Ctrl, 0x11)\
	KEY(        Alt, 0x12)\
	KEY(      Pause, 0x13)\
	KEY(   CapsLock, 0x14)\
	KEY(        Esc, 0x1B)\
	KEY(      Space, 0x20)\
	/* Center keyboard panel. */\
	KEY(       PgUp, 0x21)\
	KEY(     PgDown, 0x22)\
	KEY(        End, 0x23)\
	KEY(       Home, 0x24)\
	KEY(       Left, 0x25)\
	KEY(         Up, 0x26)\
	KEY(      Right, 0x27)\
	KEY(       Down, 0x28)\
	KEY(PrintScreen, 0x2C)\
	KEY(     Insert, 0x2D)\
	KEY(     Delete, 0x2E)\
	/* Number keys on main keyboard. */\
	KEY(TK0, 0x30)\
	KEY(TK1, 0x31)\
	KEY(TK2, 0x32)\
	KEY(TK3, 0x33)\
	KEY(TK4, 0x34)\
	KEY(TK5, 0x35)\
	KEY(TK6, 0x36)\
	KEY(TK7, 0x37)\
	KEY(TK8, 0x38)\
	KEY(TK9, 0x39)\
	/* Basic letter keys. */\
	KEY(A, 0x41)\
	KEY(B, 0x42)\
	KEY(C, 0x43)\
	KEY(D, 0x44)\
	KEY(E, 0x45)\
	KEY(F, 0x46)\
	KEY(G, 0x47)\
	KEY(H, 0x48)\
	KEY(I, 0x49)\
	KEY(J, 0x4A)\
	KEY(K, 0x4B)\
	KEY(L, 0x4C)\
	KEY(M, 0x4D)\
	KEY(N, 0x4E)\
	KEY(O, 0x4F)\
	KEY(P, 0x50)\
	KEY(Q, 0x51)\
	KEY(R, 0x52)\
	KEY(S, 0x53)\
	KEY(T, 0x54)\
	KEY(U, 0x55)\
	KEY(V, 0x56)\
	KEY(W, 0x57)\
	KEY(X, 0x58)\
	KEY(Y, 0x59)\
	KEY(Z, 0x5A)\
	/* Win keys. */\
	KEY( LeftWin, 0x5B)\
	KEY(RightWin, 0x5C)\
	KEY(    Menu, 0x5D)\
	/* Numpad keys. */\
	KEY(    Num0, 0x60)\
	KEY(    Num1, 0x61)\
	KEY(    Num2, 0x62)\
	KEY(    Num3, 0x63)\
	KEY(    Num4, 0x64)\
	KEY(    Num5, 0x65)\
	KEY(    Num6, 0x66)\
	KEY(    Num7, 0x67)\
	KEY(    Num8, 0x68)\
	KEY(    Num9, 0x69)\
	KEY( NumStar, 0x6A)\
	KEY( NumPlus, 0x6B)\
	KEY(NumMinus, 0x6D)\
	KEY(NumPoint, 0x6E)\
	KEY(NumSlash, 0x6F)\
	/* F-Keys. */\
	KEY( F1, 0x70)\
	KEY( F2, 0x71)\
	KEY( F3, 0x72)\
	KEY( F4, 0x73)\
	KEY( F5, 0x74)\
	KEY( F6, 0x75)\
	KEY( F7, 0x76)\
	KEY( F8, 0x77)\
	KEY( F9, 0x78)\
	KEY(F10, 0x79)\
	KEY(F11, 0x7A)\
	KEY(F12, 0x7B)\
	/*Locks*/\
  KEY(   NumLock, 0x90)\
  KEY(ScrollLock, 0x91)\
	/* Left-Right keys. */\
	KEY( LeftShift, 0xA0)\
	KEY(RightShift, 0xA1)\
	KEY(  LeftCtrl, 0xA2)\
	KEY( RightCtrl, 0xA3)\
	KEY(   LeftAlt, 0xA4)\
	KEY(  RightAlt, 0xA5)\
	/* Others. */\
	KEY(   Semicolon, 0xBA)\
	KEY(        Plus, 0xBB)\
	KEY(       Comma, 0xBC)\
	KEY(       Minus, 0xBD)\
	KEY(         Dot, 0xBE)\
	KEY(       Slash, 0xBF)\
	KEY(        Wave, 0xC0)\
	KEY( LeftBracket, 0xDB)\
	KEY(   BackSlash, 0xDC)\
	KEY(RightBracket, 0xDD)\
	KEY(       Quote, 0xDE)

namespace DD::Input {
	DD_ENUM_8_X(Key, KEY_VALUES);
}

#undef KEYS_VALUES
