#pragma once
namespace DD::Input {
	/**
	 * Interface between LineConsole that is reading the input in the form of lines
	 * and CommandLine::Program, that is processing the commands.
	 */
	struct CommandConsole;
}

#include <DD/Input/LineConsole.h>
#include <DD/Text/LogVirtual.h>

namespace DD::Input {
	struct CommandConsole
		: public Input::LineConsole
		, public Text::ILogVirtual8
	{
	protected:
		/** */
		CommandLine::Program & _program;
		/** */
		Delegate<Text::StringView16> _commitDelegate;

	public:
		void iLogVirtualLog(StringView view, Text::LogImportance i) {
			LineConsole::History.Append('\n').Append(view);
		}

	public:
		/** */
		void updateHint(Text::StringView16 text) {
			LineConsole::Hints.Clear();
			if (Line.IsCursorNaive()) {
				for (const CommandLine::Command * cmd : _program.Commands()) {
					if (cmd->Name().StartsWith<false>(text)) {
						String & hint = LineConsole::Hints.Append();
						hint.Append(cmd->Name());
						for (const CommandLine::IArgument * arg : cmd->Args()) {
							hint.Append(' ').Append(arg->Value());
						}
					}

					if (LineConsole::Hints.Size() == 5)
						break;
				}
			}

			if (Line.Selection != INVALID) {
				Text::StringReader16 reader{ text };
				Text::StringView16 cmdName = reader.ReadNextNonWS();
				const CommandLine::Command * cmd = nullptr;
				for (const CommandLine::Command * c : _program.Commands()) {
					if (c->Name().Equals(cmdName)) {
						cmd = c;
						break;
					}
				}

				if (cmd) {
					if (LineConsole::Line.Selection.x == 0) {
						String & hint = Hints.Append();
						hint.Append(cmdName);
						for (const CommandLine::IArgument * arg : cmd->Args())
							hint.Append(' ').Append(arg->Descriptor().Name);
					}
					else {
						Text::StringView16 arg = reader.ReadNextNonWS();
						for (size_t i = 0; arg != nullptr; ++i) {
							if (reader.Index == Line.Selection.y && i < cmd->Args().Length()) {
								const CommandLine::IArgument * iarg = cmd->Args().At(i);
								String & hint = Hints.Append();
								hint.Append(iarg->Descriptor().Name).Append(" - ").Append(iarg->Descriptor().Brief);
								break;
							}

							arg = reader.ReadNextNonWS();
						}
					}
				}
			}
		}

		void onCommit(Text::StringView16 command) {
			_program.Reset();
			Text::LogVirtual8 log{ { this, Text::LogImportance::Critical } };
			_program.TryParse(command, log);
		}

	public:
		/** Constructor. */
		CommandConsole(CommandLine::Program & program)
			: _program(program)
			, _commitDelegate([this](Text::StringView16 command) mutable { onCommit(command); }) {
			LineConsole::OnCommand += _commitDelegate;
			LineConsole::OnLineUpdate += [this](const Input::Line & text) { updateHint(text.Text); };
		}
		/** Destructor. */
		~CommandConsole() {}
	};
}
