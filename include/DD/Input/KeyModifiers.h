#pragma once
namespace DD::Input {
	/**
	 * Enumeration of modifier keys.
	 */
	class KeyModifiers;
}

#include <DD/Input/Key.h>

namespace DD::Input {
	class KeyModifiers {
	public: // nested
		/** Flags for pressed modifiers. */
		enum Modifiers {
			None = 0,
			Ctrl = 1 << 0,
			Alt = 1 << 1,
			Shift = 1 << 2,
			LeftCtrl = 1 << 3 | Ctrl,
			RightCtrl = 1 << 4 | Ctrl,
			LeftAlt = 1 << 5 | Alt,
			RightAlt = 1 << 6 | Alt,
			LeftShift = 1 << 7 | Shift,
			RightShift = 1 << 8 | Shift,
		};

	protected: // properties
		/** Storage of flags. */
		u16 _modifiers = None;

	public: // methods
		/** Is pressed any ctrl button. */
		constexpr bool CtrlPressed() const { return (*this)(Ctrl); }
		/** Is pressed any alt button. */
		constexpr bool AltPressed() const { return (*this)(Alt); }
		/** Is pressed any shift button. */
		constexpr bool ShiftPressed() const { return (*this)(Shift); }
		/** Check if the modifier is pressed. */
		constexpr bool operator()(Modifiers modifier) const { return (_modifiers & modifier) != 0; }

	public: // constructors
		/** Constructor. */
		constexpr KeyModifiers(bool leftCtrl, bool rightCtrl, bool leftAlt, bool rightAlt, bool leftShift, bool rightShift) {
			if (leftCtrl)
				_modifiers |= LeftCtrl;
			if (rightCtrl)
				_modifiers |= RightCtrl;
			if (leftAlt)
				_modifiers |= LeftAlt;
			if (rightAlt)
				_modifiers |= RightAlt;
			if (leftShift)
				_modifiers |= LeftShift;
			if (rightShift)
				_modifiers |= RightShift;
		}
	};
}
