#pragma once
namespace DD::Input {
	/**
	 * Keyboard processor.
	 *
	 * 1. Override KeyboardEventProcessor::IKeyboardEvent
	 * 2. Make it reference.
	 * 3. Register it.
	 *
	 * Example:
   *   Keyboard keybard;
	 *   KeyboardEventProcessor processor;
	 *   processor.RegisterWriteEvent(e0);
	 *   processor.RegisterKeyUpEvent(e1);
	 *   processor.RegisterKeyDownEvent(e2);
	 *   while (1) {
	 *     keybard.Refresh();
	 *     processor(keybard);
	 *   }
	 */
	class KeyboardEventProcessor;
}

#include <DD/Input/Keyboard.h>
#include <DD/List.h>
#include <DD/Reference.h>

namespace DD::Input {
	class KeyboardEventProcessor {
	public: // nested;
		/** Possible states of a key. */
		enum struct KeyState : char { Up, Down };
		/** Type of events of keyboard processor. */
		enum struct Events : unsigned { Write, KeyDown, KeyUp };
		/** Event for keyboard event processor. */
		struct IKeyboardEvent
			: public virtual IReferential
		{
		protected: // IKeyboardEvent
			/** Methods providing onDipose action. */
			virtual void iKeyboardEventDispose() {}
			/** Checks if the event should be disposed. */
			virtual bool iKeyboardEventIsDisposable() const { return !iKeyboardEventIsEnabled(); }
			/** Method checking if its action should be processed. (By default used for disposing either.) */
			virtual bool iKeyboardEventIsEnabled() const = 0;
			/** Method processing onKey* Action. */
			virtual void iKeyboardEventProcess(Input::Key key, Input::KeyModifiers modifiers) = 0;
			/** Method called when event is registered into processor. */
			virtual void iKeyboardEventOnRegister(Events) {}

		public: // Strict virtual interface
			/** Can be event disposed. */
			bool IsDisposable() { return iKeyboardEventIsDisposable(); }
			/** Is key event enabled to be shown. */
			bool IsEnabled() { return iKeyboardEventIsEnabled(); }
			/** Invoke dispose process. */
			void Dipose() { return iKeyboardEventDispose(); }
			/** Keyboard event is registered as given event. */
			void OnRegister(Events event) { return iKeyboardEventOnRegister(event); }
			/** Call process if event is done. */
			void ProcessKey(Input::Key key, Input::KeyModifiers modifiers) { iKeyboardEventProcess(key, modifiers); }

			/** Virtual destructor. */
			virtual ~IKeyboardEvent() = default;
		};
		/** Keyboard event. */
		using KeyboardEvent = Reference<IKeyboardEvent>;

	private: // properties
		/** States of keys. */
		KeyState _states[256];
		/** Time when was key state updated. */
		i64 _times[256];
		/** Storages by event types. */
		List<KeyboardEvent, 32> _onKeyDown, _onKeyUp, _onWrite;

	private: // methods
		void processKeyboard(const Input::Keyboard &);
		void processKeyboardAction(i64, i32, i32, Input::KeyModifiers);
		void processActions(List<KeyboardEvent> &, Input::Key, Input::KeyModifiers);
		void processDisposalAll();
		static void processDisposal(List<KeyboardEvent> &);
		static bool registerEvent(List<KeyboardEvent> &, KeyboardEvent, Events);

	public: // methods
		/** Process events by a given keyboard which is providing pressed keys. */
		void operator() (const Input::Keyboard & k) { processKeyboard(k); }
		/** Register keyboard event as OnWrite actions. */
		bool RegisterWriteEvent(KeyboardEvent event) { return registerEvent(_onWrite, event, Events::Write); }
		/** Is keyboard event registered as OnWrite action. */
		bool IsRegisteredWriteEvent(KeyboardEvent event) { return _onWrite.Contains(event); }
		/** Register keyboard event as OnWrite actions. */
		bool RegisterKeyDownEvent(KeyboardEvent event) { return registerEvent(_onKeyDown, event, Events::KeyDown); }
		/** Is keyboard event registered as OnWrite action. */
		bool IsRegisteredKeyDownEvent(KeyboardEvent event) { return _onKeyDown.Contains(event); }
		/** Register keyboard event as OnWrite actions. */
		bool RegisterKeyUpEvent(KeyboardEvent event) { return registerEvent(_onKeyUp, event, Events::KeyUp); }
		/** Is keyboard event registered as OnWrite action. */
		bool IsRegisteredKeyUpEvent(KeyboardEvent event) { return _onKeyUp.Contains(event); }

	public: // constructors
		/** Zero constructor. */
		KeyboardEventProcessor();
	};
}
