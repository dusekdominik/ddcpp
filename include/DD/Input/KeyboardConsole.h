#pragma once
namespace DD::Input {
	/**
	 * Console for handling keyboard input,
	 * a singleton reading keyboard and centrally propagating it.
	 */
	struct KeyboardConsole;
}

#include <DD/Delegate.h>
#include <DD/Input/Key.h>
#include <DD/Memory/NonCopyable.h>
#include <DD/Singleton.h>
#include <DD/Text/StringReader.h>
#include <DD/Text/StringReader.h>

namespace DD::Input {
	struct KeyboardConsole
		: public Singleton<KeyboardConsole>
		, Memory::NonCopyable
	{
	public: // nested
		/** Transcript for key stokes, two values, one with shift pressed ([1]) other without([0]). */
		union KeyTranscript { char Chars[2]; };
		/** Key states. */
		enum struct State : u8 { Released, Pressed };

	public: // properties
		/** Internal key state. */
		State States[256] = {};
		/** Transcript table. */
		KeyTranscript Transcript[256] = { 0 };
		/** When key state is changed from released to pressed. */
		Event<const KeyboardConsole &, Key> OnKeyPressed;
		/** When key state is changed from pressed to released. */
		Event<const KeyboardConsole &, Key> OnKeyReleased;
		/** On any state update. */
		Event<const KeyboardConsole &, Key> OnKeyUpdate;
		/** When a letter is passed, including backspace. */
		Event<const KeyboardConsole &, char> OnLetter;

	public: // const methods
		/** CHeck if the given key is pressed */
		bool IsPressed(Key key) const { return States[key] == State::Pressed; }
		/** Check if shift is pressed. */
		bool Shift() const { return IsPressed(Key::Shift) || IsPressed(Key::LeftShift) || IsPressed(Key::RightShift); }
		/** Check if ctrl is pressed. */
		bool Ctrl() const { return IsPressed(Key::Ctrl) || IsPressed(Key::LeftCtrl) || IsPressed(Key::RightCtrl); }
		/** Check if caps lock is pressed. */
		bool Caps() const { return IsPressed(Key::CapsLock); }
		/** Check upper letter should be used -> capsolock XOR shift. */
		bool UpperLetter() const { return Shift() /*!= Caps()*/; }

	public: // control methods
		/** Press given key. */
		void Press(Key key) { Update(key, State::Pressed); }
		/** Press release given key. */
		void Release(Key key) { Update(key, State::Released); }
		/** Press and release. */
		void Strike(Key key) { Press(key); Release(key); }
		/** Process key update */
		void Update(Key key, State state);

	public: // constructors
		/** Default constructor */
		KeyboardConsole();
	};
}

