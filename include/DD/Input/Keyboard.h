#pragma once
namespace DD::Input {
	/**
	 * Structure for capturing keyboard state, providing basic operations.
	 *
	 * Example:
	 *  ::DD::Keyboard keyboard;
	 *  if (keyboard.Refresh()) {
	 *    // test if a key is pressed
	 *    bool test0 = keyboard.Check(::DD::Key::LeftCtrl);
	 *    // test if a shortcut is pressed
	 *    bool test1 = keyboard.Check(::DD::Key::LeftCtrl & ::DD::Key::LeftShift & ::DD::Key::T);
	 *    // test if T-key is pressed
	 *    bool test2 = keyboard[::DD::Key::T];
	 *    // test if T-key is pressed
	 *    bool test3 = keyboard[84];
	 *    // test if T-key is pressed
	 *    bool test4 = keyboard.T();
	 *  }
	 */
	class Keyboard;
}

#include <DD/Input/Key.h>
#include <DD/Input/KeyModifiers.h>
#include <DD/Input/KeyShortcut.h>

namespace DD::Input {
	class Keyboard {
	public: // statics
		/** Convert pressed keys to text, return zero if no text is produced. */
		static constexpr char Key2Char(Input::Key key, Input::KeyModifiers modifiers);

	private: // properties
		/** Data storage. */
		unsigned short _data[256];
		/** Number of pressed keys. */
		u32 _pressed = 0;

	public: // universal accession
		/** Indexer, indexed by key enum. */
		bool operator[](Input::Key i) const { return Check(i); }
		/** Indexer, indexed by integers. */
		bool operator[](unsigned char i) const { return Check(Input::Key(i)); }
		/** Check if given key is pressed. */
		bool Check(Input::Key i) const { return (_data[(unsigned char)i] & 0x8000) != 0; }
		/** Check if given shortcut is pressed */
		bool Check(Input::KeyShortcut s) const;
		/** Number of pressed keys. */
		u32 NumberOfPressedKeys() const { return _pressed; }

	public: // special accession - special keys
		/** Check if the key is pressed. */
		bool Ctrl() const { return Check(Input::Key::Ctrl); }
		/** Check if the key is pressed. */
		bool Esc() const { return Check(Input::Key::Esc); }
		/** Check if the key is pressed. */
		bool Minus() const { return Check(Input::Key::Minus) || Check(Input::Key::NumMinus); }
		/** Check if the key is pressed. */
		bool Plus() const { return Check(Input::Key::Plus) || Check(Input::Key::NumPlus); }
		/** Check if the key is pressed. */
		bool Shift() const { return Check(Input::Key::Shift); }
		/** Check if the key is pressed. */
		bool Space() const { return Check(Input::Key::Space); }
		/** Get all pressed modifiers. */
		Input::KeyModifiers Modifiers() const;

	public: // special accession - letters
		/** Check if the key is pressed. */
		bool A() const { return Check(Input::Key::A); }
		/** Check if the key is pressed. */
		bool B() const { return Check(Input::Key::B); }
		/** Check if the key is pressed. */
		bool C() const { return Check(Input::Key::C); }
		/** Check if the key is pressed. */
		bool D() const { return Check(Input::Key::D); }
		/** Check if the key is pressed. */
		bool E() const { return Check(Input::Key::E); }
		/** Check if the key is pressed. */
		bool F() const { return Check(Input::Key::F); }
		/** Check if the key is pressed. */
		bool G() const { return Check(Input::Key::G); }
		/** Check if the key is pressed. */
		bool H() const { return Check(Input::Key::H); }
		/** Check if the key is pressed. */
		bool I() const { return Check(Input::Key::I); }
		/** Check if the key is pressed. */
		bool J() const { return Check(Input::Key::J); }
		/** Check if the key is pressed. */
		bool K() const { return Check(Input::Key::K); }
		/** Check if the key is pressed. */
		bool L() const { return Check(Input::Key::L); }
		/** Check if the key is pressed. */
		bool M() const { return Check(Input::Key::M); }
		/** Check if the key is pressed. */
		bool N() const { return Check(Input::Key::N); }
		/** Check if the key is pressed. */
		bool O() const { return Check(Input::Key::O); }
		/** Check if the key is pressed. */
		bool P() const { return Check(Input::Key::P); }
		/** Check if the key is pressed. */
		bool Q() const { return Check(Input::Key::Q); }
		/** Check if the key is pressed. */
		bool R() const { return Check(Input::Key::R); }
		/** Check if the key is pressed. */
		bool S() const { return Check(Input::Key::S); }
		/** Check if the key is pressed. */
		bool T() const { return Check(Input::Key::T); }
		/** Check if the key is pressed. */
		bool U() const { return Check(Input::Key::U); }
		/** Check if the key is pressed. */
		bool V() const { return Check(Input::Key::V); }
		/** Check if the key is pressed. */
		bool W() const { return Check(Input::Key::W); }
		/** Check if the key is pressed. */
		bool X() const { return Check(Input::Key::X); }
		/** Check if the key is pressed. */
		bool Y() const { return Check(Input::Key::Y); }
		/** Check if the key is pressed. */
		bool Z() const { return Check(Input::Key::Z); }

	public: // methods
		/** Check if the key is pressed. */
		bool Refresh();

	public: // constructors
		/** Zero constructor. */
		Keyboard() : _data() {}
	};
}

namespace DD::Input {
	constexpr char Keyboard::Key2Char(Input::Key key, Input::KeyModifiers modifiers) {
		switch (key) {
			case Input::Key::Num0: return '0';
			case Input::Key::Num1: return '1';
			case Input::Key::Num2: return '2';
			case Input::Key::Num3: return '3';
			case Input::Key::Num4: return '4';
			case Input::Key::Num5: return '5';
			case Input::Key::Num6: return '6';
			case Input::Key::Num7: return '7';
			case Input::Key::Num8: return '8';
			case Input::Key::Num9: return '9';
			case Input::Key::NumPoint: return '.';
			case Input::Key::NumSlash: return '/';
			case Input::Key::NumStar:  return '*';
			case Input::Key::NumPlus:  return '+';
			case Input::Key::NumMinus: return '-';
			case Input::Key::TK0:          return modifiers(Input::KeyModifiers::Shift) ? ')' : '0';
			case Input::Key::TK1:          return modifiers(Input::KeyModifiers::Shift) ? '!' : '1';
			case Input::Key::TK2:          return modifiers(Input::KeyModifiers::Shift) ? '@' : '2';
			case Input::Key::TK3:          return modifiers(Input::KeyModifiers::Shift) ? '#' : '3';
			case Input::Key::TK4:          return modifiers(Input::KeyModifiers::Shift) ? '$' : '4';
			case Input::Key::TK5:          return modifiers(Input::KeyModifiers::Shift) ? '%' : '5';
			case Input::Key::TK6:          return modifiers(Input::KeyModifiers::Shift) ? '^' : '6';
			case Input::Key::TK7:          return modifiers(Input::KeyModifiers::Shift) ? '&' : '7';
			case Input::Key::TK8:          return modifiers(Input::KeyModifiers::Shift) ? '*' : '8';
			case Input::Key::TK9:          return modifiers(Input::KeyModifiers::Shift) ? '(' : '9';
			case Input::Key::A:            return modifiers(Input::KeyModifiers::Shift) ? 'A' : 'a';
			case Input::Key::B:            return modifiers(Input::KeyModifiers::Shift) ? 'B' : 'b';
			case Input::Key::C:            return modifiers(Input::KeyModifiers::Shift) ? 'C' : 'c';
			case Input::Key::D:            return modifiers(Input::KeyModifiers::Shift) ? 'D' : 'd';
			case Input::Key::E:            return modifiers(Input::KeyModifiers::Shift) ? 'E' : 'e';
			case Input::Key::F:            return modifiers(Input::KeyModifiers::Shift) ? 'F' : 'f';
			case Input::Key::G:            return modifiers(Input::KeyModifiers::Shift) ? 'G' : 'g';
			case Input::Key::H:            return modifiers(Input::KeyModifiers::Shift) ? 'H' : 'h';
			case Input::Key::I:            return modifiers(Input::KeyModifiers::Shift) ? 'I' : 'i';
			case Input::Key::J:            return modifiers(Input::KeyModifiers::Shift) ? 'J' : 'j';
			case Input::Key::K:            return modifiers(Input::KeyModifiers::Shift) ? 'K' : 'k';
			case Input::Key::L:            return modifiers(Input::KeyModifiers::Shift) ? 'L' : 'l';
			case Input::Key::M:            return modifiers(Input::KeyModifiers::Shift) ? 'M' : 'm';
			case Input::Key::N:            return modifiers(Input::KeyModifiers::Shift) ? 'N' : 'n';
			case Input::Key::O:            return modifiers(Input::KeyModifiers::Shift) ? 'O' : 'o';
			case Input::Key::P:            return modifiers(Input::KeyModifiers::Shift) ? 'P' : 'p';
			case Input::Key::Q:            return modifiers(Input::KeyModifiers::Shift) ? 'Q' : 'q';
			case Input::Key::R:            return modifiers(Input::KeyModifiers::Shift) ? 'R' : 'r';
			case Input::Key::S:            return modifiers(Input::KeyModifiers::Shift) ? 'S' : 's';
			case Input::Key::T:            return modifiers(Input::KeyModifiers::Shift) ? 'T' : 't';
			case Input::Key::U:            return modifiers(Input::KeyModifiers::Shift) ? 'U' : 'u';
			case Input::Key::V:            return modifiers(Input::KeyModifiers::Shift) ? 'V' : 'v';
			case Input::Key::W:            return modifiers(Input::KeyModifiers::Shift) ? 'W' : 'w';
			case Input::Key::X:            return modifiers(Input::KeyModifiers::Shift) ? 'X' : 'x';
			case Input::Key::Y:            return modifiers(Input::KeyModifiers::Shift) ? 'Y' : 'y';
			case Input::Key::Z:            return modifiers(Input::KeyModifiers::Shift) ? 'Z' : 'z';
			case Input::Key::Space:        return ' ';
			case Input::Key::Tab:          return '\t';
			case Input::Key::Comma:        return modifiers(Input::KeyModifiers::Shift) ? '<' : ',';
			case Input::Key::Dot:          return modifiers(Input::KeyModifiers::Shift) ? '>' : '.';
			case Input::Key::BackSlash:    return modifiers(Input::KeyModifiers::Shift) ? '|' : '\\';
			case Input::Key::Plus:         return modifiers(Input::KeyModifiers::Shift) ? '+' : '=';
			case Input::Key::Minus:        return modifiers(Input::KeyModifiers::Shift) ? '_' : '-';
			case Input::Key::Slash:        return modifiers(Input::KeyModifiers::Shift) ? '?' : '/';
			case Input::Key::Semicolon:    return modifiers(Input::KeyModifiers::Shift) ? ':' : ';';
			case Input::Key::Quote:        return modifiers(Input::KeyModifiers::Shift) ? '"' : '\'';
			case Input::Key::LeftBracket:  return modifiers(Input::KeyModifiers::Shift) ? '{' : '[';
			case Input::Key::RightBracket: return modifiers(Input::KeyModifiers::Shift) ? '}' : ']';
			case Input::Key::Wave:         return modifiers(Input::KeyModifiers::Shift) ? '~' : '`';

			default: return 0;
		}
	}
}
