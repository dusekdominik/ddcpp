#pragma once
namespace DD::Input {
	/** Console for parsing input as lines. */
	struct LineConsole;
}

#include <DD/String.h>
#include <DD/Text/Ascii.h>
#include <DD/CommandLine/Program.h>
#include <DD/Delegate.h>
#include <DD/Input/KeyboardConsole.h>
#include <DD/Input/Line.h>
#include <DD/Math/Interval.h>
#include <DD/Concurrency/CriticalSection.h>

namespace DD::Input {
	struct LineConsole
		: Memory::NonCopyable
	{
		/** Constant. */
		static constexpr size_t INVALID = static_cast<size_t>(-1);

	protected: // properties
		/** Handler for reading letters from KeyboardConsole. */
		Delegate<const Input::KeyboardConsole &, char> _letterUpdateDelegate;
		/** Handler for reading non-letters from KeyboardConsole. */
		Delegate<const Input::KeyboardConsole &, Key> _navigationUpdateDelegate;

	public: // properties
		/** Suggestions for line auto-fill. */
		List<String, 12> Hints;
		/** Whenever command is being processed.  */
		Event<Text::StringView16> OnCommand;
		/** Whenever text/cursor/selection is changed. */
		Event<Input::Line &> OnLineUpdate;
		/** MT safety. */
		Concurrency::CriticalSection SectionLock;
		/** Console history text - not just commands also program response. */
		String History;
		/** History of commands. */
		List<String, 12> CommandHistory;
		/** Index for history browsing. */
		size_t historyIndex = INVALID;
		/** Cursor, selection range, line text.*/
		Input::Line Line;
		/** Is reading input from keyboard. */
		bool _active = false;

	protected: // methods
		/** Process the command event, reset line.. */
		void commit();
		/** Apply hint if available. */
		void fillHint(Key key = Key::Num1);
		/** Set something from command history to the command line. */
		void listHistory(bool up);
		/** Process navigation click. */
		void onNavigationUpdate(Key key, bool ctrl, bool shift);
		/** Process letter click. */
		void onLetterUpdate(char letter);

	public: // methods
		/** Enable keyboard input to the console. */
		void Activate();
		/** Disable keyboard input to the console. */
		void Deactivate();
		/** Return true if the console is reading input from KeyboardConsole. */
		bool Active() const { return _active; }
		/** Set if LineConsole should be reading from KeyboardConsole. */
		void Active(bool value) { value ? Activate() : Deactivate(); }

	public: // constructors
		/** Constructor. */
		LineConsole();
	};
}
