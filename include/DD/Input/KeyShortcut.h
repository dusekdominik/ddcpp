#pragma once
namespace DD::Input {
	/**
	 * Structure representing key shortcuts.
	 * Shortcut could contain at most 3-keys.
	 *
	 * Example:
	 *  KeyboardShortcut shortcut = ::DD::Key::Ctrl & ::DD::Key::Alt & ::DD::Key::S;
	 */
	class KeyShortcut;
}

#include <DD/Input/Key.h>
#include <DD/String.h>

namespace DD::Input {
	class KeyShortcut {
	public: // nested
		/** Key behavior. */
		enum struct KeyModes : u8 {
			/** Shortcut is not set. */
			Undefined,
			/** This key could be part of the shortcut. */
			Admissible,
			/** This key must be part of the shortcut. */
			Mandatory
		};

		/** Shortcut behavior. */
		enum struct ShortcutModes : u8 {
			/** Only enumerated keys must be set. */
			Strict,
			/** Other than enumerated keys could be set. */
			Benevolent
		};

		/** Settings of shortcut. */
		struct Modes {
			/** Settings of the shortcut. */
			ShortcutModes Shortcut : 1;
			/** Settings of the first key of the shortcut. */
			KeyModes First : 2;
			/** Settings of the second key of the shortcut. */
			KeyModes Second : 2;
			/** Settings of the third key of the shortcut. */
			KeyModes Third : 2;
		};

		/** Storage for keys */
		struct Keys3 {
			/** Third key of the shortcut. */
			Input::Key Third;
			/** Second key of the shortcut. */
			Input::Key Second;
			/** First key of the shortcut. */
			Input::Key First;
		};

	public: // properties
		union  {
			/** Number value, used as hash. */
			u32 Hash;
			/** Structured data. */
			struct {
				/** Keys of the shortcut. */
				Keys3 Keys;
				/** Settings of the shortcut. */
				Modes Mode;
			};
		};

	public: // methods
		/** Convert shortcut to integer hash. */
		u32 ToHash32() const { return Hash; }
		/** Convert shortcut to string. */
		String ToString() const;

	public: // operators
		/** Add a key to the shortcut. */
		KeyShortcut & operator&=(Input::Key key);
		/** Create new shortcut. */
		KeyShortcut operator&(Input::Key key) const;
		/** Add a key to the shortcut. */
		KeyShortcut & operator|=(Input::Key key);
		/** Create new shortcut. */
		KeyShortcut operator|(Input::Key key) const;

	public: // constructors
		/** Zero constructor. */
		KeyShortcut();
		/** Single key constructor. */
		KeyShortcut(Input::Key first);
		/** Double key constructor. */
		KeyShortcut(Input::Key first, Input::Key second);
		/** Triple key constructor. */
		KeyShortcut(Input::Key first, Input::Key second, Input::Key third);
	};
}