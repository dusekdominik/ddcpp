#pragma once
namespace DD{
	template<typename T>
	class Mipmap;
}

#include "Array2D.h"

namespace DD {
	/**
	 * Mipmap
	 * Mipmap contains mipmap levels - LODs.
	 * Level contains 2D grid of units.
	 * 
	 * Example:
	 *  Level0              Level1
	 *  +-----+-----+       +-----------+
	 *  | U00 | U01 |       |           |
	 *  +-----+-----+  -->  |    U00    |
	 *  | U10 | U11 |       |           |
	 *  +-----+-----+       +-----------+
	 */
	template<typename T>
	class Mipmap {
	public:
		typedef Array2D<T> unit_t;
		typedef Array2D<unit_t> level_t;
		typedef Array<level_t> levels_t;
	public:
		template<typename U>
		static bool IsCompatible(const Mipmap<T> & l, const Mipmap<U> & r);
	private:
		unsigned _level, _width, _height;
		levels_t _levels;
	public: // info
		/** Width of mipmap unit. */
		unsigned UnitWidth() const { return _width; }
		/** Height of mipmap unit. */
		unsigned UnitHeight() const { return _height; }
		/** Levels count. */
		unsigned Levels() const{ return _level; }
		/** Size compatibility check. */
		template<typename U>
		bool IsCompatible(const Mipmap<U> & other) const { return IsCompatible(*this, other); }
	private: // construction helpers
		void resize();
		void compute(T * data, unsigned width, unsigned height);
		void merge(unit_t & target, unit_t & A, unit_t & B, unit_t & C, unit_t & D);
		void merge(unit_t & target, unit_t & source, unsigned woffset, unsigned hoffset);
		static void merge(T & target, const T & A, const T & B, const T & C, const T & D);
	public:
		/** LOD access. */
		const level_t & operator[](unsigned i) const { return _levels[i]; }
		/** LOD access. */
		const level_t & Levels(unsigned i){ return _levels[i]; }
	public:
		/** RAW constructor. */
		Mipmap(T * data, unsigned width, unsigned height, unsigned level);
		/** Constructor. */
		Mipmap(Array2D<T> & data, unsigned level);
	};

	template<typename T>
	template<typename U>
	bool Mipmap<T>::IsCompatible(const Mipmap<T> & l, const Mipmap<U> & r){
		return
			l.Levels() == r.Levels() ||
			l.UnitWidth() == r.UnitWidth() ||
			l.UnitHeight() == r.UnitHeight()
		;
	}

	template<typename T>
	void Mipmap<T>::merge(T & target, const T & A, const T & B, const T & C, const T & D){
		target = (A + B + C + D) / 4;
	}

	template<typename T>
	void Mipmap<T>::merge(unit_t & target, unit_t & source, unsigned woffset, unsigned hoffset){
		for (unsigned row = 0; row < _height; row += 2) for (unsigned col = 0; col < _width; col += 2)
		{
			merge(
				target[hoffset + row / 2][woffset + col / 2],
				source[row][col],
				source[row][col + 1],
				source[row + 1][col],
				source[row + 1][col + 1]
			);
		}
	}

	template<typename T>
	void Mipmap<T>::merge(unit_t & target, unit_t & A, unit_t & B, unit_t & C, unit_t & D){
		merge(target, A, 0, 0);
		merge(target, B, _width / 2, 0);
		merge(target, C, 0, _height / 2);
		merge(target, D, _width / 2, _height / 2);
	}

	template<typename T>
	void Mipmap<T>::compute(T * data, unsigned width, unsigned height){
		// copy data
		auto & level = _levels[_level - 1];
		unsigned mmstride = width / _width;
		for (unsigned row = 0; row < height; ++row) for (unsigned col = 0; col < width; ++col)
		{
			unsigned mmrow = row / _height;
			unsigned mmcol = col / _width;
			unsigned incol = col - mmcol * _width;
			unsigned inrow = row - mmrow * _height;
			auto & unit = level[mmrow][mmcol];
			unit[inrow][incol] = data[row * width + col];
		}

		// create mipmap structure
		for (unsigned i = 1; i < _level; ++i)
		{
			auto & source = _levels[_level - (i)];
			auto & target = _levels[_level - (i + 1)];
			
			for (unsigned targetRow = 0; targetRow < target.Height(); ++targetRow)
			for (unsigned targetCol = 0; targetCol < target.Width(); ++targetCol)
			{
				unsigned sourceRow = targetRow * 2;
				unsigned sourceCol = targetCol * 2;

				auto & unit = target[targetRow][targetCol];
				auto & A = source[sourceRow][sourceCol];
				auto & B = source[sourceRow][sourceCol + 1];
				auto & C = source[sourceRow + 1][sourceCol];
				auto & D = source[sourceRow + 1][sourceCol + 1];
				merge(unit, A, B, C, D);
			}
		}
	}

	template<typename T>
	void Mipmap<T>::resize(){
		unsigned size = 1;
		for (auto & level : _levels){
			level.Resize(size, size);
			for (auto & unit : level){
				unit.Resize(_width, _height);
			}
			size *= 2;
		}
	}

	template<typename T>
	Mipmap<T>::Mipmap(T * data, unsigned width, unsigned height, unsigned level)
		: _level(level), _levels(level)
	{
		unsigned size = 1;
		while (--level) size *= 2;
		_width = width / size;
		_height = height / size;
		resize();
		compute(data, width, height);
	}

	template<typename T>
	Mipmap<T>::Mipmap(Array2D<T> & data, unsigned level)
		: Mipmap(data.Ptr(), data.Width(), data.Height(), level)
	{}
}