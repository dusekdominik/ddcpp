#pragma once
namespace DD {
	/**
	 * Exponential list, no reallocations, slower access.
	 * Preserves memory addresses of stored items.
	 * @param TYPE             - item type
	 * @param SIZE_TYPE        - indexing type
	 * @param ALLOCATED_LAYERS - number of layers of preallocated memory, minimally 1 (single item),
	 *                         - allocated memory ((2^n) - 1) * sizeof(TYPE), where n is number of layers
	 * @note Browsing via iterator should be faster than via indexer
	 */
	template<typename TYPE, size_t PREALLOCATION, typename SIZE_TYPE, typename ALLOCATOR>
	struct BlockListEx;
}

#include <DD/Auxiliaries.h>
#include <DD/BitLib.h>
#include <DD/MacrosLite.h>
#include <DD/Collections/LazyList.h>
#include <DD/Math/FunctionsLite.h>
#include <DD/Iterator/Functions.h>
#include <DD/Iterator/Iterable.h>
#include <DD/Types.h>

#define DD_BLOCKLISTEX_ASSERT(condition) DD_LITE_DEBUG_ASSERT(condition, "BlockListEx: Indexing failed.")

namespace DD {
	template<typename TYPE, size_t PREALLOCATION = 1, typename SIZE_TYPE = size_t, typename ALLOCATOR=void*>
	struct BlockListEx {
		/** Safety check. */
		static_assert(BitLib::HighBitIndex(PREALLOCATION) != BitLib::HighBitIndex(PREALLOCATION + 1), "Invalid preallocation.");

	public: // nested
		/** Indexing type. */
		using SizeType = SIZE_TYPE;
		/** Item type */
		using ItemType = TYPE;
		/** Unique pointer. */
		using BlockType = ItemType *;
		/** Index type, pair of indices, major for block indexing, minor for item indexing. */
		struct Index { SizeType Major, Minor; };
		/** Fwd iterator. */
		template<bool IS_CONST>
		struct IteratorT;
		/** Iterator */
		using Iterator = IteratorT<false>;
		/** Iterator. */
		using ConstIterator = IteratorT<true>;
		/** Iterable. */
		using Iterable = DD::Iterator::Iterable<Iterator>;
		/** Iterable. */
		using ConstIterable = DD::Iterator::Iterable<ConstIterator>;
		/** */
		using Allocator = Memory::AllocatorT<Memory::NaiveStorageT<ItemType>>;

	public: // statics
		/** Number of preallocated layers. */
		static constexpr SizeType PreallocatedLayers = BitLib::HighBitIndex(PREALLOCATION) + 1;
		/** Number of preallocated layers. */
		static constexpr SizeType PreallocatedItems = (1 << PreallocatedLayers) - 1;
		static_assert(PreallocatedItems == PREALLOCATION, "");
		/** Convert index to pair for major indexing (blocks) and minor indexing (items in a block). */
		static constexpr Index GetIndexPair(SizeType index);
		/** Convex index-pair to the offset. */
		static constexpr SizeType GetIndex(Index index);

	protected: // properties
		/** Reserve first 32 items. */
		Collections::LazyList<BlockType, sizeof(SizeType) * 8> _blocks;
		/** Number of stored items. */
		SizeType _size = 0;
		/** Preallocation. */
		struct Preallocation {
			Memory::NaiveStorageT<ItemType> data[PreallocatedItems];
		} _preallocation;

	public: // accession
		/** Indexer */
		ItemType * At(Index index) { DD_BLOCKLISTEX_ASSERT(GetIndex(index) < _size); return _blocks[index.Major] + index.Minor; }
		const ItemType * At(Index index) const { DD_BLOCKLISTEX_ASSERT(GetIndex(index) < _size); return _blocks[index.Major] + index.Minor; }
		/** Indexer. */
		ItemType * At(SizeType index) { DD_BLOCKLISTEX_ASSERT(index < _size); return At(GetIndexPair(index)); }
		const ItemType * At(SizeType index) const { DD_BLOCKLISTEX_ASSERT(index < _size); return At(GetIndexPair(index)); }
		/** Indexer */
		ItemType & operator[](Index index) { return *At(index); }
		const ItemType & operator[](Index index) const { return *At(index); }
		/** Indexer. */
		ItemType & operator[](SizeType index) { return *At(index); }
		const ItemType & operator[](SizeType index) const { return *At(index); }
		/** Get the first item in the list.  */
		ItemType & First(SizeType offset = 0) { return *At(offset); }
		const ItemType & First(SizeType offset = 0) const { return *At(offset); }
		/** Get the last item in the list.  */
		ItemType & Last(SizeType offset = 0) { return *At(_size - (1 + offset)); }
		const ItemType & Last(SizeType offset = 0) const { return *At(_size - (1 + offset)); }
		/** Iterator. */
		Iterator begin() { return { _blocks.begin(), *_blocks.begin(), *_blocks.begin() + 1 }; }
		ConstIterator begin() const { return { _blocks.begin(), *_blocks.begin(), *_blocks.begin() + 1 }; }
		/** Iterator. */
		Iterator end() { return begin() += _size; }
		ConstIterator end() const { return begin() += _size; }
		/** Iterable part of list. */
		Iterable Since(SizeType index) { DD_BLOCKLISTEX_ASSERT(index <= _size); return { begin() += index, end() }; }
		ConstIterable Since(SizeType index) const { DD_BLOCKLISTEX_ASSERT(index <= _size); return { begin() += index, end() }; }
		/** Iterable part of list. */
		Iterable Range(SizeType since, SizeType to) { DD_BLOCKLISTEX_ASSERT(since <= to && since <= _size && to <= _size); return { begin() += since, begin() += to }; }
		ConstIterable Range(SizeType since, SizeType to) const { DD_BLOCKLISTEX_ASSERT(since <= to && since <= _size && to <= _size); return { begin() += since, begin() += to }; }

	public: // counters
		/** Number of stored items. */
		constexpr SizeType Size() const { return _size; }
		/** Number of allocated items. */
		constexpr SizeType Capacity() const { return (SizeType(1) << _blocks.Count()) - 1; }
		/** Number of items that could be stored */
		constexpr SizeType FreeSlots() const { return Capacity() - _size; }

	public: // adding / removing
		/** Append new Item, return its address. */
		ItemType * AppendAllocation();
		/** Zero constructed item. */
		ItemType & Append() { return *new (AppendAllocation()) ItemType(); }
		/** Insert new item in the given position. */
		ItemType * InsertAllocation(SizeType index) { return Resize(_size + 1).ShiftRight(index, 1).At(index); }
		/** Insert new items in the given position. */
		Iterable InsertAllocation(SizeType index, SizeType length) { return Resize(_size + length).ShiftRight(index, length).Range(index, index + length); }
		/** Call destructor on items. */
		BlockListEx & Free(SizeType since = 0) { for (ItemType & item : Since(since)) item.~ItemType(); return ResizeUnsafe(since); }
		/** Remove sequence of elements */
		BlockListEx & RemoveAt(SizeType index, SizeType length = 1) { return ShiftLeft(index, length).Resize(_size - length); }
		/** Fast remove of an item, doesn't keep the list ordering. */
		BlockListEx & RemoveAtFast(SizeType index) { At(index) = Fwd(Last()); return RemoveLast(); }
		/** Remove the last element of the list. */
		BlockListEx & RemoveLast() { Last().~ItemType(); --_size; return *this; }
		/** Release allocated layers. */
		BlockListEx & Release();
		/** Reserve space for given number of items. */
		BlockListEx & Reserve(SizeType size = 1);
		/** Set size of the list. */
		BlockListEx & Resize(SizeType size) { if (_size < size) Reserve(size - _size); return Free(DD::Math::Min(size, _size)).ResizeUnsafe(size); }
		/** Set size of the list. */
		BlockListEx & ResizeUnsafe(SizeType size) { _size = size; return *this; }
		/** Shift data left. */
		BlockListEx & ShiftLeft(SizeType index, SizeType offset) { DD::Iterator::Move(begin() += index, Since(index + offset)); return *this; }
		/** Shift data right. */
		BlockListEx & ShiftRight(SizeType from, SizeType length) { DD::Iterator::MoveFromTail(begin() += _size - 1, Range(from, _size - length)); return *this; }

	public: // constructors
		/** Constructor. */
		constexpr BlockListEx();
		/** Destructor. */
		~BlockListEx() { Free().Release(); }
	};


	template<typename T, size_t A, typename S, typename AL>
	template<bool IS_CONST>
	struct BlockListEx<T, A, S, AL>::IteratorT {
		/** Helper for making some types const. */
		template<typename T>
		using ConstType = ConditionalConstType<IS_CONST, T>;

		/** Indexer over blocks. */
		ConstType<BlockType> * _major;
		/** Indexer over items in one block. */
		ConstType<ItemType> * _minor;
		/** Item beyond last one in a block. */
		ConstType<ItemType> * _minorEnd;

		/** Beginning of the current block. */
		constexpr ItemType * MinorBegin() const { return *_major; }
		/** Number of items in the current block. */
		constexpr SizeType BlockLength() const { return _minorEnd - MinorBegin(); }
		/** Index of the current block. */
		constexpr SizeType BlockIndex() const { return BitLib::HighBitIndex(BlockLength()); }
		/** Index of item wrt the current block. */
		constexpr SizeType ItemIndex() const { return _minor - MinorBegin(); }
		/** Index of current item wrt its parent list. */
		constexpr SizeType TotalIndex() const { return BlockLength() + ItemIndex() - 1; }
		/** Move to the previous block, set minor iterator to the last item of the block. */
		constexpr void DecrementBlock() { SizeType size = BlockLength(); _minor = (_minorEnd = *--_major + (size / 2)) - 1; }
		/** Move to the next block. */
		constexpr void IncrementBlock() { SizeType size = BlockLength(); _minorEnd = (_minor = *++_major) + (size * 2); }

		/** Random offset. */
		constexpr IteratorT & operator+=(SizeType offset);
		/** Decrementation. */
		constexpr IteratorT & operator--() { if (_minor-- == MinorBegin()) DecrementBlock(); return *this; }
		/** Decrementation. */
		constexpr IteratorT operator--(int) { IteratorT copy(*this); operator--(); return copy; }
		/** Incrementation. */
		constexpr IteratorT & operator++() { if (++_minor == _minorEnd) IncrementBlock(); return *this; }
		/** Post-incrementation. */
		constexpr IteratorT operator++(int) { IteratorT copy(*this); operator++(); return copy; }
		/** Dereference. */
		constexpr ConstType<ItemType> & operator*() { return *_minor; }
		/** Comparison. */
		constexpr bool operator!=(const IteratorT & rhs) const { return _minor != rhs._minor || _major != rhs._major; }
	};
}

namespace DD {
	template<typename T, size_t A, typename S, typename AL>
	constexpr typename BlockListEx<T, A, S, AL>::Index
		BlockListEx<T, A, S, AL>::GetIndexPair(SizeType index)
	{
		// 30 ~ 0b11110  msb(30) = 4
		//  1  ~   1     0.. 1    0.. 1
		//  2  ~   3     1.. 3    0.. 2
		//  4  ~   7     3.. 7    0.. 4
		//  8  ~  15     7..15    0.. 8
		// 16  ~  31    15..31    0..16
		// 32  ~  63    31..63    0..32
		if (index == SizeType(0))
			return { SizeType(0), SizeType(0) };


		SizeType msb = BitLib::HighBitIndex(index + 1);
		return { msb, (index + 1) - (SizeType(1) << (msb)) };
	}


	template<typename T, size_t A, typename S, typename AL>
	constexpr S BlockListEx<T, A, S, AL>::GetIndex(Index index) {
		DD_BLOCKLISTEX_ASSERT(index.Minor < (SizeType(1) << index.Major));

		return index.Major ? (SizeType(1) << (index.Major)) + index.Minor - 1 : SizeType(0);
	}


	template<typename T, size_t A, typename S, typename AL>
	T * BlockListEx<T, A, S, AL>::AppendAllocation() {
		// compute indices, increment counter
		Index index = GetIndexPair(_size);

		// check if enough space for appending
		if (_blocks.Count() <= index.Major)
			Reserve(1);

		// return newly added item
		++_size;
		return At(index);
	}


	template<typename T, size_t A, typename S, typename AL>
	BlockListEx<T, A, S, AL> & BlockListEx<T, A, S, AL>::Reserve(SizeType size) {
		// if not enough space
		if (FreeSlots() < size) {
			// compute the Major index
			Index index = GetIndexPair(_size + size - 1);
			// allocate following layers
			for (SizeType l = _blocks.Count(); l <= index.Major; ++l) {
				_blocks.Append() = (ItemType*)Allocator::Instance().Allocate(SizeType(1) << l);
			}
		}

		return *this;
	}


	template<typename T, size_t A, typename S, typename AL>
	BlockListEx<T, A, S, AL> & BlockListEx<T, A, S, AL>::Release() {
		// remove all
		for (SizeType l = PreallocatedLayers; l < _blocks.Count(); ++l)
			Allocator::Instance().Free(_blocks[l]);

		// resize layers
		_blocks.Resize(PreallocatedLayers);
		// fix counter
		_size = Min(_size, PreallocatedItems);

		return *this;
	}


	template<typename T, size_t A, typename S, typename AL>
	constexpr BlockListEx<T, A, S, AL>::BlockListEx() {
		ItemType * pointer = (ItemType *)(_preallocation.data);
		// fill all preallocated blocks
		for (SizeType layer = 0; layer < PreallocatedLayers; ++layer) {
			_blocks.Append(pointer);
			pointer += SizeType(1) << layer;
		}
	}


	/************************************************************************/
	/* IteratorT                                                            */
	/************************************************************************/
	template<typename T, size_t A, typename S, typename AL>
	template<bool IS_CONST>
	constexpr typename BlockListEx<T, A, S, AL>::IteratorT<IS_CONST> &
		BlockListEx<T, A, S, AL>::IteratorT<IS_CONST>::operator+=(SizeType offset) {
		Index index = GetIndexPair(TotalIndex() + offset);
		_major += index.Major - BlockIndex();
		_minor = MinorBegin() + index.Minor;
		_minorEnd = MinorBegin() + (SizeType(1) << index.Major);

		return *this;
	}
}
