#pragma once
namespace DD {
	/** */
	struct Time;
}

#include <DD/Types.h>

namespace DD {
	struct Time {
		/** Human readable time data. */
		struct Stamp { u32 Year, Month, Day, DayOfWeak, Hour, Minute, Seconds, Milliseconds; };
		/** Helper for time shifts. */
		struct Span;

	public:
		/** Conversion between eras, input in hundreds of nanoseconds, output in milliseconds. */
		static constexpr i64 Win2Unix(i64 hnseconds) { return (hnseconds - 116444736000000000LL) / 10000; }
		/** Conversion between eras, input in milliseconds, output in hundreds of nanoseconds. */
		static constexpr i64 Unix2Win(i64 milliseconds) { return milliseconds * 10000 + 116444736000000000LL; }
		/** Seconds since unix era. */
		static i64 EpochTimeSeconds();
		/** Milliseconds since unix era. */
		static i64 EpochTimeMilliseconds();

	protected: //
		/** Milliseconds since the unix era. */
		i64 _epochMilliseconds;

	public: //
		/** Get stamp from native data. */
		Stamp GetStamp() const;

	public:
		/** Constructor. */
		Time() : _epochMilliseconds(EpochTimeMilliseconds()) {}
		/** Constructor. */
		Time(i64 epochMilliseconds) : _epochMilliseconds(epochMilliseconds) {}
	};

	struct Time::Span {
		/** Milliseconds since the unix era. */
		i64 _milliseconds;
	};
}
