#pragma once
namespace DD {
	/**
	 * Static class with multiple methods for point cloud analysis.
	 */
	struct PointCloudAnalyzer;
}

#include <DD/Building.h>
#include <DD/Enum.h>
#include <DD/IProgress.h>
#include <DD/Lambda.h>
#include <DD/List.h>
#include <DD/Polygon.h>
#include <DD/Simulation/PointCloud.h>

namespace DD {
	struct PointCloudAnalyzer {
		/** Settings of region growing method. */
		struct RegionGrowingSettings;
		/** PointCloud with its convex hull. */
		struct Region;
		/** */
		struct Stick;
	public: // slice analysis
		/** Region growing on horizontal slices of point cloud. */
		struct SliceDecomposition { struct Input; struct Output; static void Evaluate(Input&, Output&); };
		/** Aggregate slices. */
		struct SliceComposition { struct Input; struct Output; static void Evaluate(Input&, Output&); };
		/** Aggregate slices. */
		struct SliceBuildings { struct Input; struct Output; static void Evaluate(Input&, Output&); };
	public: // analysis
		/**
		 * Create alpha hull, naive implementation, worst case O(n^3).
		 * Algorithm creating negative alpha-shape.
		 * @see https://graphics.stanford.edu/courses/cs268-11-spring/handouts/AlphaShapes/as_fisher.pdf
		 */
		struct AlphaHull2D { struct Input; struct Output; static void Evaluate(Input &, Output &); };
		/** Create convex hull of a point cloud. */
		struct ConvexHull2D { struct Input; struct Output; static void Evaluate(Input&, Output&); };
		/** Remove points from by histogram, designed for removing ground from relatively flat  point clouds.  */
		struct HistogramRemoval { struct Input; struct Output; static void Evaluate(Input&, Output&); };
	};

	/************************************************************************/
	/* REGION GROWING                                                       */
	/************************************************************************/
	struct PointCloudAnalyzer::RegionGrowingSettings {
		/** Nearest neighbor distance squared [m x m]. */
		f32 Distance2 = 12.f;
		/** Minimal area of convex hull [m x m]. */
		f32 MinimalArea = 50.f;
		/** Maximal area of convex hull [m x m]. */
		f32 MaximalArea = 10000.f;
		/** Regions with less than this count are not evaluated. */
		size_t MinimalPointCount = 200;
		/** Regions with more than this count are not evaluated. */
		size_t MaximalPointCount = 200000;
	};

	struct PointCloudAnalyzer::Region {
		/** Reference to a point cloud. */
		Simulation::PointCloud Cloud;
		/** Area of point cloud. */
		PolygonF Profile;
	};

	struct PointCloudAnalyzer::Stick {
		/** Centroid of the item. */
		float2 Centroid;
		/** Merged polygon. */
		PolygonF MergedPolygon;
		/** Points of merged clooud; */
		Simulation::PointCloud MergedCloud;
		/** Polygons which are aggregated to the merged polygon. */
		List<Region> SourceRegions;
	};

	/************************************************************************/
	/* AlphaHull2D                                                          */
	/************************************************************************/
	struct PointCloudAnalyzer::AlphaHull2D::Input {
		/** Input point cloud. */
		const Simulation::PointCloud * Cloud;
		/**
		 * Alpha factor.
		 * Radius of surrounding discs.
		 */
		f32 Alpha = 1.f;
	};

	struct PointCloudAnalyzer::AlphaHull2D::Output {
		/** Set of closed polygons of alpha shapes, usually not convex ones. */
		List<PolygonF> AlphaShapes;
	};

	/************************************************************************/
	/* ConvexHull2D                                                         */
	/************************************************************************/
	struct PointCloudAnalyzer::ConvexHull2D::Input {
		/** Input point cloud. */
		Simulation::PointCloud * Cloud;
	};

	struct PointCloudAnalyzer::ConvexHull2D::Output {
		/** Output convex hull. */
		Polygon<f32> * ConvexHull;
	};

	/************************************************************************/
	/* SliceDecomposition                                                   */
	/************************************************************************/
	struct PointCloudAnalyzer::SliceDecomposition::Input {
		/** Point cloud for segmentation. */
		Simulation::PointCloud Cloud;
		/**
		 * Settings of point cloud grid (x, y, z) = ([m], [m], [m]).
		 * On x, z depends how fast will be slicing processed.
		 * Y defines the height of the slice.
		 */
		float3 GridSetting = float3(5.f, 2.f, 5.f);
		/** Region growing method. */
		RegionGrowingSettings RegionGrowing;
		/** Optional callbacks for observing process. */
		struct {
			/** Reports when calculation of one slice has finished. */
			Action<> OneSliceDone;
			/** Reports number of slices.  */
			Action<u64> NumberOfSlices;
			/** Progress reporting. */
			IProgress * SlicingProgress = nullptr;
			/** Progress reporting. */
			IProgress * RegridProgress = nullptr;
		} Callbacks;
	};

	struct PointCloudAnalyzer::SliceDecomposition::Output {
		/** Output of Slicing method. */
		List<List<Region>> RegionsPerSlices;
	};

	/************************************************************************/
	/* SliceComposition                                                     */
	/************************************************************************/
	struct PointCloudAnalyzer::SliceComposition::Input {
		/** Output of Slicing method. */
		List<List<Region>> RegionsPerSlices;
		/** Unit of grid. */
		f32 Definition = 1.f;
		/** Maximal distance between centroids of a polygons. */
		f32 MaxDistance = 25.f;
		/** [not used] */
		f32 ConvexSimilarityCheck = 0.8f;
		/** Minimal number of slices (on vertical stick) to be stick accepted as valid. */
		size_t MinimalStick = 4;
	};

	struct PointCloudAnalyzer::SliceComposition::Output {
		/** Set of polygons defining areas representing the output. */
		List<Stick> Collection;
	};

	/************************************************************************/
	/* SliceBuildings                                                       */
	/************************************************************************/
	struct PointCloudAnalyzer::SliceBuildings::Input {
		/** Mode of rectangularity checks. */
		DD_ENUM_32(
			(RectangularityChecks, "Method that is used for doing the rectangularity check."),
			(OBoxRatio, 1, "Rectangular shapes are checked in the way that ratio between oriented bounding box and polygon is similar."),
			(AngleFilter, 2, "Rectangular shapes are checked by filtering out too short edges and too narrow angles from polygons.")
		);

		/** Source point cloud. */
		Simulation::PointCloud Cloud;
		/** Source properties computed in Slice composition. */
		const Stick * SliceStick;
		/**
		 * Deviation in percents from the first slice profile.
		 * Assumption is that all the wall slices are very similar.
		 * The coefficient of similarity is computed as:
		 *                             2 * surface(intersection(n, n+1))
		 *  C(slice(n), slice(n+1)) =  --------------------------------- * 100%
		 *                                 surface(n) + surface(n+1)
		 */
		f32 Treshold = 90.f;
		/**
		 * To check difference between gabled and pyramidal roof we compare ratio
		 * between longest and shortest line of the simplified polygon.
		 * If the ratio is relatively fixed, it is considered to be working with
		 * pyramidal, otherwise with gabled. Following coefficient defines how
		 * maximally could be coefficients different to be considered similar.
		 */
		f32 RatioDeviation = 0.1f;
		/** Indicates if rectangularity is checked by polygon filtering or by obox ratio. */
		RectangularityChecks RectangularityCheck = RectangularityChecks::AngleFilter;
		/**
		 * Parameter for polygon filtering.
		 * If conservative is set to true, filtering could increase the polygon
		 * surface, but cannot reduce it. If it is set to false, filtering could
		 * cause reduction of the polygon surface, but cannot increase it.
		 */
		bool Conservative = false;
		/**
		 * Parameter for polygon filtering [m].
		 * All lines shorter that this limit will be merged.
		 * Post process of filtering depends on @see Conservative param.
		 * For more details of filtering @see Polygon<T, N>::Filter*() methods.
		 */
		f32 Length = 1.0f;
		/**
		 * Parameter for polygon filtering [deg].
		 * All points where angle of neighbor lines is bigger than given limit,
		 * will be removed.
		 * Post process of filtering depends on @see Conservative param.
		 * For more details of filtering @see Polygon<T, N>::Filter*() methods.
		 */
		f32 Angle = 175.f;
		/**
		 * Parameter for BoxRatio filtering.
		 * Threshold for comparing oriented bounding box against the polygon.
		 * The polygon is considered rectangular if surface(polygon)/surface(obox)
		 * is bigger or equal to this value.
		 * If it is set to 1.f, only polygons identical to their boxes are considered
		 * rectangular, if to 0.f, all polygons are considered rectangular.
		 * The accepted value is in [0.f, 1.f] interval.
		 */
		f32 RectangularityRatio = 0.9f;
	};

	struct PointCloudAnalyzer::SliceBuildings::Output {
		/** Descriptor of building. */
		Building EvaluatedBuilding;
		/** Temporary debug properties. */
		PolygonLocalF<128> left, right, insec;
		/** Simplified polygons. */
		List<PolygonLocalD<16>, 128> Simplified;
	};

	/************************************************************************/
	/* HistogramRemoval                                                     */
	/************************************************************************/
	struct PointCloudAnalyzer::HistogramRemoval::Input {
		/** Mode what should be stored on output. */
		DD_ENUM_32(
			(Modes, "Mode of point cloud output."),
			(Negative, 1, "Point-cloud with removed points by histogram method."),
			(Positive, 2, "Point-cloud with points kept by histogram method."),
			(Both,     3, "Both point-clouds.")
		);
		/** Mode what should be stored on output. */
		Modes Mode = Modes::Both;
		/** Input point cloud.  */
		Simulation::PointCloud Cloud;
		/**
		 * Average deviation [dimensionless].
		 * Standard value 1.5 - 10.0.
		 * Method computes average histogram value, e.g. 10000 points per a height step.
		 * Average deviation set to 1.5 means that if histogram step contains 15000 points
		 * or more, it will be removed, so all points with corresponding height will be removed.
		 */
		f32 AverageDeviation = 1.5f;
		/**
		 * Height step [m].
		 * Step defines resolution of the histogram.
		 * Histogram length would be (maxY - minY) / step.
		 */
		f32 Step = 1.0f;
		/**
		 * Histogram area [m x m].
		 * Histogram is done over smaller areas to avoid problems when point cloud
		 * does not represent flat area.
		 */
		float2 Area = float2(100.f, 100.f);
		/*
		 * Histogram area is moved over the point cloud by defined shift [m x m].
		 * This settings is to avoid raw changes between neighbor tiles. SO histogram
		 * for filtering tile is made from its own histogram and a couple of neighbors.
		 * Sample setting [1 x 1] means that histogram is done on 9 tiles.
		 * Let us have a settings {2, 1}, so histogram will be collected as on following:
		 *   t t 1 t t  \
		 *   2 1 T 1 2   } so range would be 2 tiles in x-dimension and 1 tile in y-dimension
		 *   t t 1 t t  /
		 * Corners of histogram are not resolved as smaller areas, so corner is resolved
		 * as its nearest tile with defined range.
		 */
		uint2 Range = uint2(1, 1);

		/** Callbacks for obtaining info about process in while method is running. */
		struct {
			/** Callback meaning that a tile has been processed. */
			IAction<> * OneTileDone = 0;
			/** Callback getting a number of tiles. */
			IAction<size_t> * NumberOfTiles = 0;
			/** Progress observer.*/
			IProgress * Progress = 0;
		} Callbacks;
	};

	struct PointCloudAnalyzer::HistogramRemoval::Output {
		/** Output point cloud */
		Simulation::PointCloud Positive;
		/** Output point cloud */
		Simulation::PointCloud Negative;
	};
}
