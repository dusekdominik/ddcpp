#pragma once

#include "Vector.h"
#include "Permutation.h"
#include <DD/ConstantExpression.h>
#include <DD/Math/MatrixCommonModel.h>
#include <DD/Math/MatrixTransposeViewModel.h>
#include <DD/Math/MatrixDiagonalModel.h>
#include <DD/Math/Matrix.h>

namespace DD {

	template<typename T, size_t ROWS, size_t COLS = ROWS>
	using Matrix = Math::CommonMatrix<T, ROWS, COLS>;

	/**
	 * Optimized form of square diagonal matrix (only diagonal components are stored)
	 * With optimized multiplication behavior.
	 */
	template<typename T, size_t SIZE>
	using DiagonalMatrix = Math::Matrix<Math::MatrixDiagonalModel<T, SIZE, size_t>>;
	using DiagonalMatrix2D = DiagonalMatrix<f64, 2>;
	using DiagonalMatrix3D = DiagonalMatrix<f64, 3>;
	using DiagonalMatrix3F = DiagonalMatrix<f32, 3>;

	/** Matrix 2x2. */
	template<typename T>
	using M22 = Matrix<T, 2, 2>;
	/** Matrix 3x3. */
	template<typename T>
	using M33 = Matrix<T, 3, 3>;
	/** Matrix 4x4. */
	template<typename T>
	using M44 = Matrix<T, 4, 4>;
	/** 2x2 int matrix. */
	typedef M22<int> int2x2;
	/** 3x3 int matrix. */
	typedef M33<int> int3x3;
	/** 3x3 int matrix. */
	typedef M44<int> int4x4;
	/** 2x2 uint matrix. */
	typedef M22<unsigned> uint2x2;
	/** 3x3 uint matrix. */
	typedef M33<unsigned> uint3x3;
	/** 3x3 uint matrix. */
	typedef M44<unsigned> uint4x4;
	/** 2x2 float matrix. */
	typedef M22<float> float2x2;
	/** 3x3 float matrix. */
	typedef M33<float> float3x3;
	/** 4x4 float matrix. */
	typedef M44<float> float4x4;
	/** 2x2 double matrix. */
	typedef M22<double> double2x2;
	/** 3x3 double matrix. */
	typedef M33<double> double3x3;
	/** 4x4 double matrix. */
	typedef M44<double> double4x4;
}
