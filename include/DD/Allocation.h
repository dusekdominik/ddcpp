#pragma once
namespace DD {
	/**
	 * Reserves a piece of memory.
	 * In memory could be stored anything.
	 * Also the memory could be converted to anything.
	 *
	 * @example locally allocated virtual structures
	 *   struct ICommand {                       // declare virtual class
	 *     virtual void iCommandRun() = 0;       // virtual method
	 *     void Run() { iCommandRun(); }         // interface method (to be always called virtual one)
	 *   };
	 *
	 *   struct CommandInt : public ICommand {    // first implementation of ICommand interface
	 *     int MyInt;
	 *     void iCommandRun() override { ... }
	 *   };
	 *
	 *   struct CommandDouble : public ICommand { // second implementation of ICommand interface
	 *     double MyDouble;
	 *     void iCommandRun() override { ... }
	 *   };
	 *
	 *   using Command = Allocation<Math::Max(sizeof(CommandInt), sizeof(CommandDouble))>;
	 *
	 *   List<Command, 32> commands;               // list of locally allocated commands
	 *   commands.Add(CommandInt());               // feed the list
	 *   commands.Add(CommandDouble());            // feed the list
	 *
	 *   for (Command command : commands)          // run all the commands
	 *     command.Cast<ICommand>().Run();         // need to be casted
	 */
	template<size_t BYTES>
	struct Allocation;

	/**
	 * Typed variant of Allocation.
	 * @see Allocation
	 * @example locally allocated lambda
	 *   struct ICommand {                              // declare interface with an allocation
	 *     Allocation<16> _alloc;                       // 16 bytes is commonly enough
	 *     virtual void Run() = 0;                      // virtual method
	 *   };
	 *
	 *   template<typename LAMBDA>
	 *   struct LambdaCommand : public ICommand {       // declare lambda catcher
	 *     void Run() override { _d.Cast<LAMBDA>()(); } // override interface
	 *     LambdaCommand(LAMBDA l) { _alloc = l; }      // safe lambda to local allocation
	 *   };
	 *
	 *   using Command = AllocationT<ICommand>;         // command
	 *
	 *   List<Command, 32> _commands;                   // create list of commands (with local storage)
	 *   _commands.Add(LambdaCommand([](){ ... }));     // feed command list
	 *   _commands.Add(LambdaCommand([&]() { ... }));   // feed command list
	 *
	 *   for (Command & command : _commands)            // run all the commands
	 *     command->Run();
	 */
	template<typename T>
	struct AllocationT;
}

#include <DD/Types.h>

namespace DD {
	template<size_t BYTES>
	struct Allocation {
		static_assert(BYTES % 4 == 0, "Memory must be 32-bit aligned.");

	protected: // properties
		/** 32-bit aligned allocation. */
		i32 _alloc[BYTES / 4];

	protected: // methods
		/** Pull the content. */
		template<size_t I>
		constexpr __forceinline void pull(const int * source) { static_assert((I + 1) * 4 <= BYTES, "Allocation overflow."); pull<I - 1>(source); _alloc[I] = source[I]; }
		/** Pull the content. */
		template<>
		constexpr __forceinline void pull<0>(const int * source) { _alloc[0] = source[0]; }
		/** Pull the content. */
		template<typename T>
		constexpr __forceinline void pull(const T & t) { pull<(sizeof(T) - 1) / 4>((const int*)&t); }

	public:
		/** Get pointer to allocation. */
		void * Ptr() { return _alloc; }
		/** Caster. */
		template<typename T>
		constexpr T & Cast() { return reinterpret_cast<T &>(_alloc); }
		/** Const caster. */
		template<typename T>
		constexpr const T & Cast() const { return reinterpret_cast<const T &>(_alloc); }
		/** Explicit call of destructor. */
		template<typename T>
		constexpr void Destroy() { (&Cast<T>())->~T(); }

	public: // operators
		/** Assignment operator. */
		template<typename T>
		constexpr Allocation & operator=(const T & source) { pull<T>(source); return *this; }

	public: // constructors
		/** Constructor from a type. */
		template<typename T>
		constexpr Allocation(const T & source) { pull<T>(source); }
		/** Zero constructor. */
		constexpr Allocation() {}
	};

	template<typename T>
	struct AllocationT
		: protected Allocation<sizeof(T)>
	{
		/** Parent class. */
		using Base = Allocation<sizeof(T)>;

		/** Get reference to allocation as to stored object. */
		constexpr T & Ref() { return Base::template Cast<T>(); };
		/** Get reference to allocation as to stored object. */
		constexpr const T & Ref() const { return Base::template Cast<const T>(); };
		/** Get pointer to allocation as to stored object. */
		constexpr T * Ptr() { return &Ref(); };
		/** Get pointer to allocation as to stored object. */
		constexpr const T * Ptr() const { return &Ref(); };

		/** Accession. */
		constexpr T * operator->() { return Ptr(); }
		/** Accession. */
		constexpr const T * operator->() const { return Ptr(); }
		/** Assignment operator. */
		constexpr AllocationT & operator=(const T & source) { pull<T>(source); return *this; }

		/** Constructor. */
		constexpr AllocationT(const T & source) : Allocation(source) {}
		/** Zero constructor. */
		constexpr AllocationT() {}
		/** Move constructor. */
		constexpr AllocationT(AllocationT && alloc) { Allocation<sizeof(T)> tmp(Base::_alloc); pull(alloc._alloc); alloc.pull(tmp); }

		/** Destructor. */
		~AllocationT() { Destroy<T>(); }
	};

}