#pragma once
namespace DD {
	/**
	 * Class for obtaining information about environment.
	 */
	struct Environment;
}

#include "Primitives.h"

namespace DD {
	struct Environment {
		/** Description of area of all attached displays. */
		struct VirtualDesktop {
			/** Description of whole area. */
			static AABox2I Box();
			/** Left-top corner. */
			static int2 Begin();
			/** Right-bottom corner. */
			static int2 End();
			/** Dimensions of desktop. */
			static int2 Size();
			/** Number of displays. */
			static i32 Displays();
		};
	};
}