#pragma once
namespace DD {
	/***/
	class ShaderCompiler;
}

#include <DD/Gpu/Devices.h>
#include <DD/Gpu/VertexShader.h>
#include <DD/Gpu/PixelShader.h>

namespace DD {
	class ShaderCompiler {
	public:
		static Gpu::PixelShader CreatePixelShader(Gpu::Devices & device, const String & name);
		//static VertexShader CreateVertexShader(Devices & device, const String & name, const layout_t & layout);
		//static VertexShader CreateVertexShader(Devices & device, const String & name, layoutRaw_t layout, size_t layoutSize);
		//template<typename CBUFFER>
		//static ShaderSet<CBUFFER> * CreateShaderSet(Devices & device, const String & ps, const String & vs, const layout_t & layout);
// 		template<typename CBUFFER>
// 		static ShaderSet<CBUFFER> * CreateShaderSet(Devices & device, const String & ps, const String & vs, const layoutRaw_t & layout, size_t layoutSize);
// 		template<typename INPUT>
// 		static ShaderSet * CreateShaderSet(Devices & device, const String & ps, const String & vs);
	private:
		String _path, _vspath, _pspath;
		String _psmodel, _vsmodel, _error;
		unsigned long _flags;
		Gpu::Devices & _device;

	public:
		ShaderCompiler & operator=(const ShaderCompiler &) = delete;
		//VertexShader VS(const String & name, const layout_t & layout);
		Gpu::PixelShader  PS(const String & name);
// 		template<typename CONSTANTS>
// 		ShaderSet<CONSTANTS> * Shaders(const layout_t & layout, const String & vs, const String & ps);
		/** Text file compiler. */
		//ID3DBlob * operator()(const String & name, const String & model);
		/** CSO file loader. */
		//ID3DBlob * operator()(const String & name);
		//ID3DBlob * GetPSFromCSO(const String & name);
		const String & Error() const { return _error; }
		bool Succesful() const { return _error.Length() == 0; }
		ShaderCompiler(Gpu::Devices & device, Text::StringView16 path, Text::StringView16 vsmodel = L"vs_4_0", Text::StringView16 psmodel = L"ps_4_0");
		ShaderCompiler(Gpu::Devices & device, Text::StringView16 vspath, Text::StringView16 pspath);
	};
}

namespace DD {
// 	template<typename CBUFFER>
// 	inline ShaderSet<CBUFFER>* ShaderCompiler::CreateShaderSet(Devices & device, const String & ps, const String & vs, const layout_t & layout) {
// 		ShaderSet<CBUFFER> * result = new ShaderSet<CBUFFER>(device);
// 		result->PS() = CreatePixelShader(device, ps);
// 		result->VS() = CreateVertexShader(device, vs, layout);
// 		return result;
// 	}

// 	template<typename CBUFFER>
// 	inline ShaderSet<CBUFFER>* ShaderCompiler::CreateShaderSet(Devices & device, const String & ps, const String & vs, const layoutRaw_t & layout, size_t layoutSize)
// 	{
// 		ShaderSet<CBUFFER> * result = new ShaderSet<CBUFFER>(device);
// 		result->PS() = CreatePixelShader(device, ps);
// 		result->VS() = CreateVertexShader(device, vs, layout, layoutSize);
// 		return result;
// 	}

// 	template<typename INPUT>
// 	inline ShaderSet* ShaderCompiler::CreateShaderSet(Devices & device, const String & ps, const String & vs) {
// 		ShaderSet * result = new ShaderSet(device);
// 		result->PS() = CreatePixelShader(device, ps);
// 		result->VS() = CreateVertexShader(device, vs, INPUT::LAYOUT, INPUT::LAYOUT_SIZE);
// 		return result;
//		}


// 	template<typename CONSTANTS>
// 	DD::ShaderSet<CONSTANTS> * ShaderCompiler::Shaders(
// 		const layout_t & layout,
// 		const String & vs,
// 		const String & ps
// 	) {
// 		DD::ShaderSet<CONSTANTS> * result = new DD::ShaderSet<CONSTANTS>(_device);
// 		result->PS() = PS(ps);
// 		result->VS() = VS(vs, layout);
// 		return result;
// 	}
}