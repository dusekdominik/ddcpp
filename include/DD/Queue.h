#pragma once
namespace DD {
	/**
	 * class Queue<T, ALLOCATION>
	 * Fifo/Lifo structure.
	 *  T - type of queue storage
	 *  @param ALLOCATION - size of local preallocation
	 *    (data will be allocated locally, without need of dynamic allocation)
	 */
	template<typename T, size_t ALLOCATION = 0>
	class Queue;
}

#include <DD/Array.h>
#include <DD/Iterator/IndexIterator.h>

namespace DD {
	template<typename T, size_t ALLOCATION>
	class Queue {
	public: // nested
		typedef DD::Iterator::IndexIterator<Queue, T> iterator_t;
		typedef DD::Iterator::IndexIterator<Queue, const T> const_iterator_t;

	private: // properties
		/** Index of first item. */
		size_t _begin = 0;
		/** Number of stored items. */
		size_t _size = 0;
		/** Data storage. */
		Array<T, ALLOCATION> _data;

	private: // methods
		/** Increases circular index. */
		size_t inc(size_t & i);
		/** Decreases circular index. */
		size_t dec(size_t & i);
		/** Parses linear into circular index. */
		size_t index(size_t i);
		/** Reallocation method. */
		void increaseSize();

	public: // accession
		/** Iterator. */
		iterator_t begin() { return iterator_t(*this, 0); }
		/** Iterator. */
		iterator_t end() { return iterator_t(*this, _size); }
		/** Const iterator. */
		const_iterator_t begin() const { return const_iterator_t(*this, 0); }
		/** Const iterator. */
		const_iterator_t end() const { return const_iterator_t(*this, _size); }
		/** Indexer. */
		T & operator[](size_t i) { return _data[index(i)]; }
		/** Indexer. */
		const T & operator[](size_t i) const { return _data[index(i)]; }
		/** First item. */
		T & Front() { return _data[_begin]; }
		/** First item. */
		const T & Front() const { return _data[_begin]; }
		/** Last item. */
		T & Back() { return _data[index(_size - 1)]; }
		/** Last item. */
		const T & Back() const { return _data[index(_size - 1)]; }
		/** Returns how many items are stored. */
		size_t Size() const { return _size; }

	public: // insertion
		/** Prepends item - copy variant. */
		template<typename ARG>
		void PushFront(const ARG & lvalue) { if (IsFull()) increaseSize(); UnsafePushFront(lvalue); }
		/** Prepends item. */
		template<typename ARG>
		void PushFront(ARG && rvalue) { if (IsFull()) increaseSize(); UnsafePushFront(Fwd(rvalue)); }
		/** Prepends item. */
		void PushFront() { if (IsFull()) increaseSize(); UnsafePushFront(); }
		/** Appends item. */
		template<typename ARG>
		void PushBack(const ARG & lvalue) { if (IsFull()) increaseSize(); UnsafePushBack(lvalue); }
		/** Appends item. */
		void PushBack() { if (IsFull()) increaseSize(); UnsafePushBack(); }
		/** Appends item. */
		template<typename ARG>
		void PushBack(ARG && rvalue) { if (IsFull()) increaseSize(); UnsafePushBack(Fwd(rvalue)); }
		/** Prepends item - copy variant. */
		template<typename ARG>
		void UnsafePushFront(const ARG & lvalue) { ++_size; dec(_begin); new (&Front()) T(lvalue); }
		/** Prepends item. */
		template<typename ARG>
		void UnsafePushFront(ARG && rvalue) { ++_size; dec(_begin); new (&Front()) T(Fwd(rvalue)); }
		/** Prepends item. */
		void UnsafePushFront() { ++_size; dec(_begin); new (&Front()) T(); }
		/** Appends item. */
		template<typename ARG>
		void UnsafePushBack(const ARG & lvalue) { ++_size; new (&Back()) T(lvalue); }
		/** Appends item. */
		template<typename ARG>
		void UnsafePushBack(ARG && rvalue) { ++_size; new (&Back()) T(Fwd(rvalue)); }
		/** Appends item. */
		void UnsafePushBack() { ++_size; new (&Back()) T(); }

	public: // deletion
		/** Set properties to empty queue. */
		void Clear() { _begin = _size = 0; }
		/** Removes first item. */
		void PopFront() { --_size; _data[inc(_begin)].~T(); }
		/** Removes last item. */
		void PopBack() { _data[index(--_size)].~T(); }
		/** Safe version of PopFront. */
		bool TryPopFront(T & target);
		/** Safe version of PopBack. */
		bool TryPopBack(T & target);

	public: // methods
		/** Returns how many items can be stored. */
		size_t Capacity() const { return _data.Length(); }
		/** Returns how many additional items can be stored. */
		size_t FreeSpace() const { return Capacity() - Size(); }
		/** Checks if Queue is full. */
		bool IsFull() const { return FreeSpace() == 0; }
		/** Checks if Queue is empty. */
		bool IsEmpty() const { return Size() == 0; }

	public: // constructors
		/** Zero constructor. */
		Queue() = default;
		/** Constructor. */
		explicit Queue(size_t i) : _data(i) {}
	};
}


namespace DD {
	template<typename T, size_t ALLOCATION>
	size_t Queue<T, ALLOCATION>::inc(size_t & i) {
		size_t r = i;
		i = (i + 1) % Capacity();
		return r;
	}


	template<typename T, size_t ALLOCATION>
	size_t Queue<T, ALLOCATION>::dec(size_t & i) {
		size_t r = i;
		i = i == 0 ? _data.Size() - 1 : i - 1;
		return r;
	}


	template<typename T, size_t ALLOCATION>
	size_t Queue<T, ALLOCATION>::index(size_t i) {
		return (_begin + i) % Capacity();
	}


	template<typename T, size_t ALLOCATION>
	void Queue<T, ALLOCATION>::increaseSize() {
		Array<T> newData(Capacity() ? Capacity() * 2 : 1);
		for (size_t i = 0, m = Size(); i < m; ++i) {
			new (newData.begin() + i) T(Fwd(*(_data.begin() + index(i))));
		}
		_begin = 0;
		_data.Swap(newData);
	}


	template<typename T, size_t ALLOCATION>
	bool Queue<T, ALLOCATION>::TryPopFront(T & target) {
		if (IsEmpty())
			return false;

		target = Fwd(Front());
		PopFront();
		return true;
	}


	template<typename T, size_t ALLOCATION>
	bool Queue<T, ALLOCATION>::TryPopBack(T & target) {
		if (IsEmpty())
			return false;

		target = Fwd(Back());
		PopBack();
		return true;
	}
}
