#pragma once
namespace DD {
	/** Enumeration of signum results. */
	class Signum;
	/** Cast as signum. */
	template<typename T>
	constexpr Signum signum(const T &);
	/** Cast as signum with a dynamic tolerance. */
	template<typename T>
	constexpr Signum signum(const T &, const T &);
}

#include "Enum.h"

namespace DD {
	DD_ENUM_32(
		(Signum, "Result of signum() function."),
		(NEGATIVE, -1, "Value is smaller than zero."),
		(NEUTRAL,   0, "Value is zero"),
		(POSITIVE, +1, "Value is greater than zero.")
	);

	template<typename T>
	constexpr Signum signum(const T & value) {
		if (value == T(0))
			return Signum::NEUTRAL;
		else if (value > T(0))
			return Signum::POSITIVE;
		else
			return Signum::NEGATIVE;
	}

	template<typename T>
	constexpr Signum signum(const T & value, const T & tolerance) {
		if (tolerance < value)
			return Signum::POSITIVE;
		else if (value < -tolerance)
			return Signum::NEGATIVE;
		else
			return Signum::NEUTRAL;
	}
}
