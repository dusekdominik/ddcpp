#pragma once
#include <DirectXMath.h>

#pragma region declarations
// DirectX::XMFLOAT4 

inline DirectX::XMFLOAT4 & operator*=(DirectX::XMFLOAT4 & f4, const float f);
inline DirectX::XMFLOAT4 & operator/=(DirectX::XMFLOAT4 & f4, const float f);
inline DirectX::XMFLOAT4 & operator+=(DirectX::XMFLOAT4 & f, const DirectX::XMFLOAT4 & g);
inline DirectX::XMFLOAT4 & operator-=(DirectX::XMFLOAT4 & f, const DirectX::XMFLOAT4 & g);
inline DirectX::XMFLOAT4 operator*(const DirectX::XMFLOAT4 & f4, const float f);
inline DirectX::XMFLOAT4 operator*(const float f, const DirectX::XMFLOAT4 & f4);
inline DirectX::XMFLOAT4 operator/(const DirectX::XMFLOAT4 & f4, const float f);
inline DirectX::XMFLOAT4 operator+(const DirectX::XMFLOAT4 & f, const DirectX::XMFLOAT4 & g);
inline DirectX::XMFLOAT4 operator-(const DirectX::XMFLOAT4 & f, const DirectX::XMFLOAT4 & g);
inline float operator*(const DirectX::XMFLOAT4 & f, const DirectX::XMFLOAT4 & g);
inline bool operator!=(const DirectX::XMFLOAT4 & f, const DirectX::XMFLOAT4 & g);
inline bool operator==(const DirectX::XMFLOAT4 & f, const DirectX::XMFLOAT4 & g);

// DirectX::XMFLOAT3 

inline DirectX::XMFLOAT3 & operator*=(DirectX::XMFLOAT3 & f3, const float f);
inline DirectX::XMFLOAT3 & operator/=(DirectX::XMFLOAT3 & f3, const float f);
inline DirectX::XMFLOAT3 & operator+=(DirectX::XMFLOAT3 & f, const DirectX::XMFLOAT3 & g);
inline DirectX::XMFLOAT3 & operator-=(DirectX::XMFLOAT3 & f, const DirectX::XMFLOAT3 & g);
inline DirectX::XMFLOAT3 operator*(const DirectX::XMFLOAT3 & f3, const float f);
inline DirectX::XMFLOAT3 operator*(const float f, const DirectX::XMFLOAT3 & f3);
inline DirectX::XMFLOAT3 operator/(const DirectX::XMFLOAT3 & f3, const float f);
inline DirectX::XMFLOAT3 operator+(const DirectX::XMFLOAT3 & f, const DirectX::XMFLOAT3 & g);
inline DirectX::XMFLOAT3 operator-(const DirectX::XMFLOAT3 & f, const DirectX::XMFLOAT3 & g);
inline float operator*(const DirectX::XMFLOAT3 & f, const DirectX::XMFLOAT3 & g);
inline bool operator!=(const DirectX::XMFLOAT3 & f, const DirectX::XMFLOAT3 & g);
inline bool operator==(const DirectX::XMFLOAT3 & f, const DirectX::XMFLOAT3 & g);

// DirectX::XMFLOAT2

inline DirectX::XMFLOAT2 & operator*=(DirectX::XMFLOAT2 & f2, const float f);
inline DirectX::XMFLOAT2 & operator/=(DirectX::XMFLOAT2 & f2, const float f);
inline DirectX::XMFLOAT2 & operator+=(DirectX::XMFLOAT2 & f, const DirectX::XMFLOAT2 & g);
inline DirectX::XMFLOAT2 & operator-=(DirectX::XMFLOAT2 & f, const DirectX::XMFLOAT2 & g);
inline DirectX::XMFLOAT2 operator*(const DirectX::XMFLOAT2 & f2, const float f);
inline DirectX::XMFLOAT2 operator*(const float f, const DirectX::XMFLOAT2 & f2);
inline DirectX::XMFLOAT2 operator/(const DirectX::XMFLOAT2 & f2, const float f);
inline DirectX::XMFLOAT2 operator+(const DirectX::XMFLOAT2 & f, const DirectX::XMFLOAT2 & g);
inline DirectX::XMFLOAT2 operator-(const DirectX::XMFLOAT2 & f, const DirectX::XMFLOAT2 & g);
inline float operator*(const DirectX::XMFLOAT2 & f, const DirectX::XMFLOAT2 & g);
#pragma endregion

#pragma region definitions
inline DirectX::XMFLOAT4 & operator*=(DirectX::XMFLOAT4 & f4, const float f) {
	f4.x *= f, f4.y *= f, f4.z *= f, f4.w *= f;
	return f4;
}

inline DirectX::XMFLOAT4 & operator/=(DirectX::XMFLOAT4 & f4, const float f) {
	return f4 *= (1 / f);
}

inline DirectX::XMFLOAT4 & operator+=(DirectX::XMFLOAT4 & f, const DirectX::XMFLOAT4 & g) {
	f.x += g.x, f.y += g.y, f.z += g.z, f.w += g.w;
	return f;
}

inline DirectX::XMFLOAT4 & operator-=(DirectX::XMFLOAT4 & f, const DirectX::XMFLOAT4 & g) {
	f.x -= g.x, f.y -= g.y, f.z -= g.z, f.w -= g.w;
	return f;
}

inline DirectX::XMFLOAT4 operator*(const DirectX::XMFLOAT4 & f, const float g) {
	DirectX::XMFLOAT4 result = (f);
	return result *= g;
}

inline DirectX::XMFLOAT4 operator*(const float f, const DirectX::XMFLOAT4 & f4) {
	return f4 * f;
}

inline DirectX::XMFLOAT4 operator/(const DirectX::XMFLOAT4 & f, const float g) {
	DirectX::XMFLOAT4 result = (f);
	return result /= g;
}

inline DirectX::XMFLOAT4 operator+(const DirectX::XMFLOAT4 & f, const DirectX::XMFLOAT4 & g) {
	DirectX::XMFLOAT4 result = (f);
	return result += g;
}

inline DirectX::XMFLOAT4 operator-(const DirectX::XMFLOAT4 & f, const DirectX::XMFLOAT4 & g) {
	DirectX::XMFLOAT4 result = (f);
	return result -= g;
}

inline float operator*(const DirectX::XMFLOAT4 & f, const DirectX::XMFLOAT4 & g) {
	return f.x*g.x + f.y*g.y + f.z*g.z + f.w*g.w;
}

inline bool operator!=(const DirectX::XMFLOAT4 & f, const DirectX::XMFLOAT4 & g) {
	return f.x != g.x || f.y != g.y || f.z != g.z || f.w != g.w;
}

inline bool operator==(const DirectX::XMFLOAT4 & f, const DirectX::XMFLOAT4 & g) {
	return f.x == g.x && f.y == g.y && f.z == g.z && f.w == g.w;
}
// DirectX::XMFLOAT3 

inline DirectX::XMFLOAT3 & operator*=(DirectX::XMFLOAT3 & f3, const float f) {
	f3.x *= f, f3.y *= f, f3.z *= f;
	return f3;
}

inline DirectX::XMFLOAT3 & operator/=(DirectX::XMFLOAT3 & f3, const float f) {
	return f3 *= (1 / f);
}

inline DirectX::XMFLOAT3 & operator+=(DirectX::XMFLOAT3 & f, const DirectX::XMFLOAT3 & g) {
	f.x += g.x, f.y += g.y, f.z += g.z;
	return f;
}

inline DirectX::XMFLOAT3 & operator-=(DirectX::XMFLOAT3 & f, const DirectX::XMFLOAT3 & g) {
	f.x -= g.x, f.y -= g.y, f.z -= g.z;
	return f;
}

inline DirectX::XMFLOAT3 operator*(const DirectX::XMFLOAT3 & f, const float g) {
	DirectX::XMFLOAT3 result = (f);
	return result *= g;
}

inline DirectX::XMFLOAT3 operator*(const float f, const DirectX::XMFLOAT3 & f3) {
	return f3 * f;
}

inline DirectX::XMFLOAT3 operator/(const DirectX::XMFLOAT3 & f, const float g) {
	DirectX::XMFLOAT3 result = (f);
	return result /= g;
}

inline DirectX::XMFLOAT3 operator+(const DirectX::XMFLOAT3 & f, const DirectX::XMFLOAT3 & g) {
	DirectX::XMFLOAT3 result = (f);
	return  result += g;
}

inline DirectX::XMFLOAT3 operator-(const DirectX::XMFLOAT3 & f, const DirectX::XMFLOAT3 & g) {
	DirectX::XMFLOAT3 result = f;
	return result -= g;
}

inline float operator*(const DirectX::XMFLOAT3 & f, const DirectX::XMFLOAT3 & g) {
	return f.x*g.x + f.y*g.y + f.z*g.z;
}

inline bool operator!=(const DirectX::XMFLOAT3 & f, const DirectX::XMFLOAT3 & g) {
	return f.x != g.x || f.y != g.y || f.z != g.z;
}

inline bool operator==(const DirectX::XMFLOAT3 & f, const DirectX::XMFLOAT3 & g) {
	return f.x == g.x && f.y == g.y && f.z == g.z;
}

// DirectX::XMFLOAT2

inline DirectX::XMFLOAT2 & operator*=(DirectX::XMFLOAT2 & f2, const float f) {
	f2.x *= f, f2.y *= f;
	return f2;
}

inline DirectX::XMFLOAT2 & operator/=(DirectX::XMFLOAT2 & f2, const float f) {
	return f2*=(1 / f);
}

inline DirectX::XMFLOAT2 & operator+=(DirectX::XMFLOAT2 & f, const DirectX::XMFLOAT2 & g) {
	f.x += g.x, f.y += g.y;
	return f;
}

inline DirectX::XMFLOAT2 & operator-=(DirectX::XMFLOAT2 & f, const DirectX::XMFLOAT2 & g) {
	f.x -= g.x, f.y -= g.y;
	return f;
}

inline DirectX::XMFLOAT2 operator*(const DirectX::XMFLOAT2 & f, const float g) {
	DirectX::XMFLOAT2 result = (f);
	return result *= g;
}

inline DirectX::XMFLOAT2 operator*(const float f, const DirectX::XMFLOAT2 & f2) {
	return f2 * f;
}

inline DirectX::XMFLOAT2 operator/(const DirectX::XMFLOAT2 & f, const float g) {
	DirectX::XMFLOAT2 result = (f);
	return result /= g;
}

inline DirectX::XMFLOAT2 operator+(const DirectX::XMFLOAT2 & f, const DirectX::XMFLOAT2 & g) {
	DirectX::XMFLOAT2 result = (f);
	return result += g;
}

inline DirectX::XMFLOAT2 operator-(const DirectX::XMFLOAT2 & f, const DirectX::XMFLOAT2 & g) {
	DirectX::XMFLOAT2 result = (f);
	return result -= g;
}

inline float operator*(const DirectX::XMFLOAT2 & f, const DirectX::XMFLOAT2 & g) {
	return f.x*g.x + f.y*g.y;
}

#pragma endregion