#pragma once
namespace DD {
	/** Template default constants for BTree. */
	struct BTreeConstants {
		/** Default degree of BTree.  */
		static constexpr size_t DEGREE = 5;
		/** Default depth of local allocation of BTree. */
		static constexpr size_t DEPTH = 3;
	};
	/**
	 * Helper struct for BTree.
	 * Node view of BTreeCell - serves as view of LeftSon, RightSon and Item.
	 * Left and Right Sons are pointing into cells storage.
	 * Index is pointing into items storage.
	 */
	struct BTreeNode;
	/**
	 * Helper struct for BTree.
	 * Represents one cell of BTree.
	 */
	template<size_t SIZE>
	struct BTreeCell;
	/** Pair for key-value mapping. */
	template<typename KEY, typename VALUE>
	struct BTreePair;
	/**
	 * BTree implementation.
	 * BTree is implemented as B+Tree structure.
	 *
	 * Traits have to contain:
	 *   constants:
	 *     DEGREE - degree of tree
	 *     DEPTH  - locally preallocated storage for given depth of tree
	 *   typedefs:
	 *     item_t  - stored item
	 *     key_t   - compare key of item_t
	 *     value_t - value of the item_t
	 *   methods (static):
	 *     const key_t & GetKey(const item_t &)       - key extractor
	 *     const value_t & GetValue(const item_t &)   - value extractor
	 *     value_t & GetValue(item_t &)               - value extractor
	 *     bool Compare(const key_t &, const key_t &) - key comparer
	 */
	template<typename TRAITS>
	struct BTreeBase;
	/**
	 * Simple variants of BTree.
	 * BTree<Item>
	 * BTree<Key, Value>
	 */
	template<typename...>
	struct BTree;

	/** BTree traits for allocation mechanism. */
	template<size_t DEPTH = BTreeConstants::DEPTH, size_t DEGREE = BTreeConstants::DEGREE>
	struct BTreeTraitsAllocation;
	/** BTree traits for obtaining search key. By default method ITEM::GetKey(). */
	template<typename ITEM>
	struct BTreeTraitsKey;
	/** BTree traits for obtaining value from a type. By default returns whole stored item. */
	template<typename ITEM>
	struct BTreeTraitsValue;
	/** BTree traits for comparing key. By default returns result of KEY::operator<. */
	template<typename KEY>
	struct BTreeTraitsCompare;
	/** Collected default traits for BTree. */
	template<
		typename ITEM,
		typename ALLOCATION_TRAITS = BTreeTraitsAllocation<BTreeConstants::DEPTH, BTreeConstants::DEGREE>,
		typename KEY_TRAITS = BTreeTraitsKey<ITEM>,
		typename VALUE_TRAITS = BTreeTraitsValue<ITEM>,
		typename COMPARE_TRAITS = BTreeTraitsCompare<typename KEY_TRAITS::key_t>
	> struct BTreeTraits;
}

#include "List.h"
#include <DD/ArrayListLib.h>

namespace DD {
	/************************************************************************/
	/* Helpers                                                              */
	/************************************************************************/
	template<typename KEY, typename VALUE>
	struct BTreePair {
		/** Key of BtreePair, stored separately from value type. */
		KEY Key;
		/** Value of BtreePair. */
		VALUE Value;
	};

	struct BTreeNode {
		/** Index to son cell. */
		size_t Left;
		/** Index to stored item. */
		size_t Index;
		/** Index to son cell. */
		size_t Right;
	};

	template<size_t SIZE>
	struct BTreeCell {
		/** Index to parent cell. */
		size_t Parent;
		/** Number of items stored in the cell. */
		size_t Count;
		/** Number of items stored in the subtrees of the cell. */
		size_t Total;
		/** Array for data storage.  subtree | index to key item | subtree | index to key item | ... | subtree */
		size_t Data[2 * SIZE + 1];
		/** Indexer. */
		BTreeNode & operator[](size_t i) { return *((BTreeNode*)(Data + (2 * i))); }
		/** Indexer. */
		const BTreeNode & operator[](size_t i) const { return *((const BTreeNode *)(Data + (2 * i))); }
	};

	/************************************************************************/
	/* BTreeBase                                                            */
	/************************************************************************/
	template<typename TRAITS>
	struct BTreeBase
		: public TRAITS
	{
		static_assert(TRAITS::DEGREE >= 3, "Size of cell must be at least 3.");

	public: // nested
		/** Storage behavior. */
		enum Storage { INLINE, EXTENDED, DETECT };
		/** BTree iterator template. */
		template<typename TREE>
		struct Iterator;
		/** BTree iterator. */
		typedef Iterator<BTreeBase> iterator_t;
		/** BTree const iterator. */
		typedef Iterator<const BTreeBase> const_iterator_t;
		/** Cell type. */
		typedef BTreeCell<TRAITS::DEGREE> cell_t;
		/** Alias */
		using ItemType = typename TRAITS::item_t;
		/** Alias */
		using KeyType = typename TRAITS::key_t;
		/** Traits. */
		using Traits = TRAITS;

	public: // constants
		/** Integer power function. */
		static constexpr size_t pow(size_t base, size_t exp) { return exp == 0 ? 1 : base * pow(base, exp - 1); }
		/** Number of cells of tree. */
		static constexpr size_t tree(size_t base, size_t exp) { return exp == 0 ? 1 : tree(base, exp - 1) + pow(base, exp); }
		/** Constant index for all undefined. */
		static constexpr size_t UNDEFINED = static_cast<size_t>(-1);
		/** Is storage or stored separately. */
		static constexpr Storage STORAGE = sizeof(TRAITS::item_t) <= sizeof(size_t) ? INLINE : EXTENDED;
		/** Fwd from traits, */
		static constexpr size_t DEGREE = TRAITS::DEGREE;

	protected: // properties
		/** Number of layers after root => single cell tree is of _depth = 0. */
		size_t  _depth = 0;
		/** Index of root cell. */
		size_t _rootIndex = 0;
		/** Number of items. */
		size_t _size = 0;
		/** Storage of cells. */
		List<cell_t, tree(TRAITS::DEGREE, TRAITS::DEPTH)> _cells;
		/** Storage of items. */
		List<ItemType, STORAGE == INLINE ? 0 : tree(TRAITS::DEGREE, TRAITS::DEPTH) * TRAITS::DEGREE> _items;

	protected: // methods
		/** Copy-Store item. */
		template<Storage B = DETECT>
		size_t store(const ItemType & i) { return store<STORAGE>(i); }
		template<> size_t store<EXTENDED>(const ItemType & i) { size_t n = _items.Size(); _items.Add(i); return n; }
		template<> size_t store<INLINE>(const ItemType & i) { return *((const size_t *)(&i)); }
		/** Move-Store item. */
		template<Storage B = DETECT>
		size_t store(ItemType && i) { return store<STORAGE>(Fwd(i)); }
		template<> size_t store<EXTENDED>(ItemType && i) { size_t n = _items.Size(); _items.Add(Fwd(i)); return n; }
		template<> size_t store<INLINE>(ItemType && i) { return *((const size_t *)(&i)); }
		/** Item getter. */
		template<Storage = DETECT>
		ItemType & get(size_t cell, size_t offset) { return get<STORAGE>(cell, offset); }
		template<> ItemType & get<EXTENDED>(size_t cell, size_t offset) { return _items[_cells[cell][offset].Index]; }
		template<> ItemType & get<INLINE>(size_t cell, size_t offset) { return *((ItemType *)(&_cells[cell][offset].Index)); }
		/** Const item getter. */
		template<Storage = DETECT>
		const ItemType & get(size_t cell, size_t offset) const { return get<STORAGE>(cell, offset); }
		template<> const ItemType & get<EXTENDED>(size_t cell, size_t offset) const { return _items[_cells[cell][offset].Index]; }
		template<> const ItemType & get<INLINE>(size_t cell, size_t offset) const { return *((const ItemType *)(&_cells[cell][offset].Index)); }
		/** Insert reference or remove reference. */
		template<typename T>
		ItemType & insert(T t);
		/** Value extractor. */
		typename TRAITS::value_t & getValue(size_t cell, size_t offset) { return TRAITS::GetValue(get(cell, offset)); }
		/** Const value extractor. */
		const typename TRAITS::value_t & getValue(size_t cell, size_t offset) const { return TRAITS::GetValue(get(cell, offset)); }
		/** Key extractor. */
		KeyType getKey(size_t cell, size_t offset) const { return TRAITS::GetKey(get(cell, offset)); }
		/** Create new root, declare its sons. */
		void redeclareRoot(size_t leftIndex, size_t centerIndex, size_t rightIndex);
		/** Split full node.  */
		void splitNode(size_t cellIndex);
		/** Split full leaf. */
		void splitLeaf(size_t leftIndex, size_t & centerIndex, size_t & rightIndex);
		/** Select indices by given key. If equalOffset param is not UNDEFINED, key has been already stored. */
		void select(const KeyType & key, size_t & leafIndex, size_t & equalOffset, size_t & insertOffset) const;
		/** Insert an item. */
		void insert(size_t & cellIndex, size_t & cellOffset, size_t itemIndex);
		/** Insert an item. */
		void remove(size_t & cellIndex, size_t & cellOffset);
		/** Validate number of elements in cell. */
		void validateRoot(size_t cellIndex);
		/** Validate number of elements in cell. */
		void validateNode(size_t cellIndex);
		/** Helper method for insertion. */
		void recountLayer(size_t index);
		/** Helper method for insertion. */
		void recount(size_t index);
		/** Helper method for removing. */
		void triangleToRight(size_t cell, size_t offset);
		/** Helper method for removing. */
		void triangleToLeft(size_t cell, size_t offset);
		/** Helper method for removing. */
		void merge(size_t leftIndex, size_t rightIndex, size_t parentOffset);
		/** Helper method for get by index. */
		void selectByIndex(size_t desired, size_t cellIndex, size_t & resultCell, size_t & resultOffset) const;

	public: // iterator methods
		/** Iterator. */
		iterator_t begin() { return iterator_t(this); }
		/** Iterator. */
		iterator_t end() { return iterator_t(); }
		/** Const iterator. */
		const_iterator_t begin() const { return const_iterator_t(this); }
		/** Const iterator. */
		const_iterator_t end() const { return const_iterator_t(); }
		/** Number of stored items. */
		size_t Size() const { return _size; }

	public: // methods
		/** Copy inserter. */
		ItemType & Insert(const ItemType & item) { return insert<const ItemType &>(item); }
		/** Move inserter. */
		ItemType & Insert(ItemType && item) { return insert<ItemType &&>(item); }
		/** Get pointer to the item of given key, returns nullptr if key is not contained. */
		ItemType * GetItem(const KeyType & key);
		/** Get const pointer to the item of given key, returns nullptr if key is not contained. */
		typename const TRAITS::item_t * GetItem(const KeyType & key) const;
		/** Get n-th item, log(n). */
		ItemType & GetItemByIndex(size_t index);
		/** Get n-th item, log(n). */
		const ItemType & GetItemByIndex(size_t index) const;
		/** Boolean check to if a key is contained in tree. */
		bool ContainsKey(const KeyType & key) const { return GetItem(key) != nullptr; }
		/** Remove by key - log(n). */
		bool Remove(const KeyType & key);
		/** Reset the tree. */
		void Clear();
		/** Release all items. */
		void Free();

	public: // operators
		/** Return n-the element of tree. Log(n). */
		ItemType & operator()(size_t n) { return GetItemByIndex(n); }
		/** Return n-the element of tree. Log(n). */
		const ItemType & operator()(size_t n) const { return GetItemByIndex(n); }

	public: // constructors
		/** Zero constructor. */
		BTreeBase();
	};

	/************************************************************************/
	/* BTreeBase::Iterator                                                  */
	/************************************************************************/
	template<typename TRAITS>
	template<typename TREE>
	struct BTreeBase<TRAITS>::Iterator {
	public: // nested
		/** Pair of cell index and offset in the cell. */
		struct StackItem { size_t Cell, Index; };

	private: // properties
		/** Pointer to the parent tree. */
		TREE * _tree;
		/** Stack for unrolling tree nodes. */
		List<StackItem> _stack;

	public: // operators
		/** Iterator comparer. */
		bool operator!=(const Iterator & cmp) { return _stack.Size() != cmp._stack.Size(); }
		/** Iterator dereference. */
		auto & operator*() { return _tree->get(_stack.Last().Cell, _stack.Last().Index); }
		/** Iterator increment. */
		void operator++();

	public: // constructors
		/** Zero constructor for begin iterator. */
		Iterator() {}
		/** Constructor for end iterator. */
		Iterator(TREE * tree);
	};

	/************************************************************************/
	/* BTree                                                                */
	/************************************************************************/
	template<typename TYPE>
	struct BTree<TYPE>
		: public BTreeBase<BTreeTraits<TYPE>>
	{
		using Base = BTreeBase<BTreeTraits<TYPE>>;
		/** Traits. */
		typedef BTreeTraits<TYPE> TRAITS;

		/** Returns const reference to a value. If key is not stored, it stores it. */
		typename TRAITS::value_t & operator[](const typename TRAITS::key_t & key);
		/** Returns const reference to a value if key exists, otherwise throw an exception. */
		const typename TRAITS::value_t & operator[](const typename TRAITS::key_t & key) const;
	};

	template<typename UKEY, typename UVALUE>
	struct BTree<UKEY, UVALUE>
		: public BTreeBase<BTreeTraits<BTreePair<UKEY, UVALUE>>>
	{
		using Base = BTreeBase<BTreeTraits<BTreePair<UKEY, UVALUE>>>;
		/** Insert key-value mapping. */
		BTreePair<UKEY, UVALUE> & Insert(const UKEY & key, const UVALUE & value);
		/** Returns const reference to a value. If key is not stored, it stores it. */
		UVALUE & operator[](const UKEY & key);
		/** Returns const reference to a value if key exists, otherwise throw an exception. */
		const UVALUE & operator[](const UKEY & key) const;
	};

	/************************************************************************/
	/* BTree traits                                                         */
	/************************************************************************/
	template<size_t _DEPTH, size_t _DEGREE>
	struct BTreeTraitsAllocation {
		/** Degree of tree. Each cell can store d item and (d + 1) sons. */
		static constexpr size_t DEGREE = _DEGREE;
		/** Depth of tree which is locally allocated. */
		static constexpr size_t DEPTH = _DEPTH;
	};

	template<typename KEY>
	struct BTreeTraitsCompare {
		/** Compare function. */
		static bool Compare(const KEY & a0, const KEY & a1) { return a0 < a1; }
	};

	template<typename ITEM>
	struct BTreeTraitsKey {
		/** Type of key of stored item. */
		using key_t = decltype(((const ITEM *)0)->GetKey());

		/** Standard trait, requires GetKey() method. */
		static key_t GetKey(const ITEM & item) { return item.GetKey(); }
	};

	template<typename KEY, typename VALUE>
	struct BTreeTraitsKey<BTreePair<KEY, VALUE>> {
		/** Key type. */
		typedef KEY key_t;

		/** Trait for BtreePair. */
		static const key_t & GetKey(const BTreePair<KEY, VALUE> & item) { return item.Key; }
	};

	template<typename T>
	struct BTreeTraitsValue {
		/** Value type. */
		typedef T value_t;
		/** Standard trait, returns reference itself. */
		static T & GetValue(T & item) { return item; }
		/** Standard trait, returns reference itself. */
		static const T & GetValue(const T & item) { return item; }
	};

	template<typename KEY, typename VALUE>
	struct BTreeTraitsValue<BTreePair<KEY, VALUE>> {
		/** Value type. */
		typedef VALUE value_t;
		/** Value extractor. */
		static VALUE & GetValue(BTreePair<KEY, VALUE> & item) { return item.Value; }
		/** Const value extractor. */
		static const VALUE & GetValue(const BTreePair<KEY, VALUE> & item) { return item.Value; }
	};

	template<typename ITEM, typename ALLOCATION_TRAITS, typename KEY_TRAITS, typename VALUE_TRAITS, typename COMPARE_TRAITS>
	struct BTreeTraits
		: public ALLOCATION_TRAITS
		, public KEY_TRAITS
		, public VALUE_TRAITS
		, public COMPARE_TRAITS
	{
		/** Type which should be stored in the BTree. */
		typedef ITEM item_t;
	};
}

namespace DD {
	/************************************************************************/
	/* BTreeBase - private methods                                          */
	/************************************************************************/
#pragma region private methods
	template<typename TRAITS>
	void BTreeBase<TRAITS>::select(
		const typename TRAITS::key_t & key, // key that is being looked up
		size_t & leafIndex,                 // index of the node where search ended
		size_t & equalOffset,               // if the equal item was found, this is its index
		size_t & insertOffset
	) const {
		leafIndex = _rootIndex;
		equalOffset = UNDEFINED;
		insertOffset = 0;

		bool cmp = false;
		// from the root to the depth
		for (size_t depth = 0; depth != _depth; ++depth) {
			const cell_t & cell = _cells[leafIndex];
			for (insertOffset = 0; insertOffset < cell.Count; ++insertOffset) {
				// get first bigger item (key < itemInTree)
				cmp = TRAITS::Compare(key, getKey(leafIndex, insertOffset));
				if (cmp) {
					break;
				}
			}

			// if nothing in the cell was bigger than key, check if the Last() item is not equal
			if (!cmp) {
				size_t eqIndex = insertOffset == cell.Count ? cell.Count - 1 : insertOffset;
				if (!TRAITS::Compare(getKey(leafIndex, eqIndex), key)) {
					equalOffset = eqIndex;
					break;
				}
			}
			// if something was bigger than key, check if the previous item is not equal
			else if (insertOffset) {
				if (!TRAITS::Compare(getKey(leafIndex, insertOffset - 1), key)) {
					equalOffset = insertOffset - 1;
					break;
				}
			}

			// if nothing in the cell is equal to the key then follow left son of the first bigger item
			leafIndex = cell[insertOffset].Left;
		}

		// cell where the search ended, if something is found it could be any node
		// if nothing is found, it is a leaf
		const cell_t & leaf = _cells[leafIndex];

		// if nothing is equal to the given key, set index where to place that given key
		if (equalOffset == UNDEFINED && leaf.Count) {
			// look for the first bigger item
			for (insertOffset = 0; insertOffset < leaf.Count; ++insertOffset) {
				cmp = TRAITS::Compare(key, getKey(leafIndex, insertOffset));
				if (cmp) {
					break;
				}
			}

 			if (!cmp) {
 				if (!TRAITS::Compare(getKey(leafIndex, insertOffset - 1), key)) {
 					equalOffset = insertOffset - 1;
 				}
 			}
 			else if (insertOffset) {
 				if (!TRAITS::Compare(getKey(leafIndex, insertOffset - 1), key)) {
 					equalOffset = insertOffset - 1;
 				}
 			}
		}
	}

	template<typename TRAITS>
	void DD::BTreeBase<TRAITS>::splitLeaf(
		size_t leftIndex,          // index of the node that should be split (after op. it becomes the left subtree)
		size_t & centerIndex,      // [out] index of the item (in parent node) that separates left and right subtrees
		size_t & rightIndex        // [out] index of the right subtree
	) {
		// Idea of operation (DEGREE 5)
		//                                   [3]
		// [1] [2] [3] [4] [5]   ->         /   \
		//                           [1] [2]     [4] [5]

		// create new cell
		rightIndex = _cells.AddNaive();
		// index of the center point
		centerIndex = _cells[leftIndex][DEGREE / 2].Index;

		// reset new cell by UNDEFINED sons
		_cells[rightIndex][0].Left = UNDEFINED;
		for (size_t i = 0; i < DEGREE; ++i)
			_cells[rightIndex][i].Right = UNDEFINED;

		// new cell will obtain half of children
		_cells[rightIndex].Count = DEGREE - (DEGREE / 2) - 1;
		// set parent to the same as original cell
		_cells[rightIndex].Parent = _cells[leftIndex].Parent;
		// old cell will obtain half of the children
		_cells[leftIndex].Count = DEGREE / 2;
		// copy right part of the original cell to the new one
		for (size_t i = DEGREE / 2 + 1, j = 0; i < DEGREE; ++i, ++j)
			_cells[rightIndex][j].Index = _cells[leftIndex][i].Index;

		// if the leaf is root fix the indices
		if (_cells[leftIndex].Parent == UNDEFINED) {
			redeclareRoot(leftIndex, centerIndex, rightIndex);
		}
		// if the cell is not root
		else {
			// split parent if it is too big
			if (_cells[_cells[leftIndex].Parent].Count == DEGREE) {
				splitNode(_cells[leftIndex].Parent);
			}

			// get parent cell
			cell_t & parent = _cells[_cells[leftIndex].Parent];
			// set parent to the new cell (it could be redeclared by splitting)
			_cells[rightIndex].Parent = _cells[leftIndex].Parent;

			// search for offset of the left cell in the parent
			size_t select = 0;
			while (parent[select].Left != leftIndex)
				++select;

			// move the content to be able to place new value into the list
			ArrayLib::ShiftRight(parent.Data, select * 2 + 1, parent.Count++ * 2 + 1, 2ULL);

			// insert the index into the list
			BTreeNode& center = parent[select];
			center.Index = centerIndex;
			center.Right = rightIndex;
		}

		// recount the item in the subtrees
		recountLayer(leftIndex);
		recountLayer(rightIndex);
	}

	template<typename TRAITS>
	void BTreeBase<TRAITS>::splitNode(size_t cellIndex) {
		// index of the cell that should be split
		size_t leftIndex = cellIndex;
		// add new cell
		size_t rightIndex = _cells.AddNaive();
		size_t centerIndex = _cells[leftIndex][DEGREE / 2].Index;
		// initialize properties, except parent
		_cells[rightIndex].Count = DEGREE - (DEGREE / 2) - 1;
		_cells[leftIndex].Count = DEGREE / 2;
		// copy data
		static constexpr size_t DELIMITER_INDEX = ((1 + DEGREE / 2) * 2);
		static constexpr size_t COPY_SIZE = (2 * DEGREE + 1) - DELIMITER_INDEX;
		ArrayLib::Copy(_cells[leftIndex].Data + DELIMITER_INDEX, _cells[rightIndex].Data, COPY_SIZE);

		// fix the sons to be having correct parent
		for (size_t i = 0; i <= _cells[rightIndex].Count; ++i) {
			_cells[_cells[rightIndex].Data[i * 2]].Parent = rightIndex;
		}

		// create new root if the node was root
		if (_cells[leftIndex].Parent == UNDEFINED) {
			redeclareRoot(leftIndex, centerIndex, rightIndex);
		}
		else {
			// if parent is full, split it
			if (_cells[_cells[leftIndex].Parent].Count == DEGREE) {
				splitNode(_cells[leftIndex].Parent);
			}

			// fix right parent
			cell_t & parent = _cells[_cells[leftIndex].Parent];
			_cells[rightIndex].Parent = _cells[leftIndex].Parent;

			// position of pointer from parent to just being split cell
			auto marker = [leftIndex](const BTreeNode & n) { return n.Left == leftIndex; };
			size_t selectedIndex = ArrayLib::Find(&parent[0], parent.Count, marker, 0ULL);
			// shift content and insert
			ArrayLib::ShiftRight(parent.Data, 2 * selectedIndex + 1, 2 * parent.Count++ + 1, 2ULL);
			BTreeNode & selected = parent[selectedIndex];
			selected.Right = rightIndex;
			selected.Index = centerIndex;
		}

		recountLayer(leftIndex);
		recountLayer(rightIndex);
	}

	template<typename TRAITS>
	void BTreeBase<TRAITS>::redeclareRoot(size_t leftIndex, size_t centerIndex, size_t rightIndex) {
		// update cell pointers
		_rootIndex = _cells.AddNaive();
		_cells[leftIndex].Parent = _rootIndex;
		_cells[rightIndex].Parent = _rootIndex;

		// update depth
		++_depth;

		// initialize root cell
		cell_t & root = _cells[_rootIndex];
		root.Parent = UNDEFINED;
		root.Count = 1;

		// fix indices
		BTreeNode & n = root[0];
		n.Left = leftIndex;
		n.Index = centerIndex;
		n.Right = rightIndex;
	}

	template<typename TRAITS>
	void BTreeBase<TRAITS>::insert(size_t & leafIndex, size_t & leafOffset, size_t itemIndex) {
		// split if cell is full
		if (_cells[leafIndex].Count == DEGREE) {
			size_t centerIndex, rightIndex;
			splitLeaf(leafIndex, centerIndex, rightIndex);
			if (leafOffset > (DEGREE / 2)) {
				leafIndex = rightIndex;
				leafOffset -= (DEGREE / 2) + 1;
			}
		}

		// shift old content
		cell_t & leaf = _cells[leafIndex];
		for (size_t i = leaf.Count; i > leafOffset; --i)
			leaf[i].Index = leaf[i - 1].Index;

		// insert new content
		leaf[leafOffset].Index = itemIndex;
		++leaf.Count;
		++_size;
		recount(leafIndex);
		// ASSERT(_cells[_rootIndex].Total == _size);
	}

	template<typename TRAITS>
	void BTreeBase<TRAITS>::remove(size_t & cellIndex, size_t & cellOffset) {
		// helper for shrinking the lists
		auto shrink = [this](size_t cellIndex, size_t from) {
			// get cell
			cell_t & cell = _cells[cellIndex];
			// decrement num of items
			cell.Count--;
			// shrink
			for (size_t i = from; i < cell.Count; ++i) {
				cell[i].Index = cell[i + 1].Index;
			}
			// recursively update subtree count in parents
			while (cellIndex != UNDEFINED) {
				_cells[cellIndex].Total--;
				cellIndex = _cells[cellIndex].Parent;
			}
		};

		// remove if element is just leaf
		if (_cells[cellIndex][0].Left == UNDEFINED) {
			shrink(cellIndex, cellOffset);
			// if not root check minimal count
			if (cellIndex != _rootIndex)
				validateNode(cellIndex);
		}
		// swap with leaf if element is node
		else {
			// get right son
			size_t swp = _cells[cellIndex][cellOffset].Right;
			// continue to the leftist subtree in the right son
			while (_cells[swp][0].Left != UNDEFINED) {
				swp = _cells[swp][0].Left;
			}
			// place the leftist son as a key in the inner node
			_cells[cellIndex][cellOffset].Index = _cells[swp][0].Index;
			// remove the item from the leftist son
			shrink(swp, 0);
			// check minimal count in the leaf
			validateNode(swp);
		}

		--_size;
	}

	template<typename TRAITS>
	void BTreeBase<TRAITS>::selectByIndex(
		size_t desired,       // desired index that we are looking for
		size_t cellIndex,     // index of the cell where search starts
		size_t & resultCell,  // the cell where item is placed
		size_t & resultOffset // offset to item in the cell
	) const {
		size_t mostLeft = _cells[cellIndex][0].Left;
		if (mostLeft != UNDEFINED) {
			if (desired < _cells[mostLeft].Total) {
				selectByIndex(desired, mostLeft, resultCell, resultOffset);
				return;
			}
			else {
				desired -= _cells[mostLeft].Total;
			}
		}
		else {
			resultCell = cellIndex;
			resultOffset = desired;
			return;
		}

		// go  through indices to, subtract the subtree count, recursively follow in the sons
		for (size_t i = 0; i < _cells[cellIndex].Count; ++i) {
			if (desired-- == 0) {
				resultCell = cellIndex;
				resultOffset = i;
				return;
			}

			size_t right = _cells[cellIndex][i].Right;
			if (right != UNDEFINED) {
				if (desired < _cells[right].Total) {
					selectByIndex(desired, right, resultCell, resultOffset);
					return;
				}
				else {
					desired -= _cells[right].Total;
				}
			}
			else {
				resultCell = cellIndex;
				resultOffset = desired;
				return;
			}
		}
	}

	template<typename TRAITS>
	void BTreeBase<TRAITS>::triangleToRight(
		size_t cell,  // index of the cell where the subtree is a child
		size_t offset // offset to the item representing subtree that should be triangled
	) {
		//             |C|                       |L|
		//         /        \                /         \
		//     |X|L|         |R|    -->    |X|         |C|R|
		//   /   |   \      /   \         /   \      /   |   \
		// |Y|  |LL| |LR| |RL| |RR|     |Y|  |LL| |LR| |RL| |RR|
		BTreeNode & me = _cells[cell][offset];
		cell_t & left = _cells[me.Left];
		cell_t & right = _cells[me.Right];
		// expand right cell
		for (size_t i = right.Count; i > 0; --i) {
			right[i].Right = right[i - 1].Right;
			right[i].Index = right[i - 1].Index;
		}
		right[0].Right = right[0].Left;
		right[0].Left = UNDEFINED;
		++right.Count;
		// fill right cell
		right[0].Index = me.Index;
		right[0].Left = left[left.Count - 1].Right;
		if (right[0].Left != UNDEFINED) {
			_cells[right[0].Left].Parent = me.Right;
			right.Total += _cells[right[0].Left].Total;
			left.Total -= _cells[right[0].Left].Total;
		}
		me.Index = left[left.Count - 1].Index;
		--left.Count;
		--left.Total;
		++right.Total;
	}

	template<typename TRAITS>
	void BTreeBase<TRAITS>::triangleToLeft(
    size_t cell,  // index of the cell where the subtree is a child
    size_t offset // offset to the item representing subtree that should be triangled
	) {
		//          |C|                           |R|
		//       /       \                    /         \
		//     |L|       |R|X|             |L|C|         |X|
		//    /   \     /   \  \         /   |   \      /   \
		//  |LL| |LR| |RL| |RR| |Y|    |LL| |LR| |RL| |RR|  |Y|
		BTreeNode & me = _cells[cell][offset];
		cell_t & left = _cells[me.Left];
		cell_t & right = _cells[me.Right];
		left[left.Count].Index = me.Index;
		left[left.Count].Right = right[0].Left;

		if (left[left.Count].Right != UNDEFINED) {
			_cells[left[left.Count].Right].Parent = me.Left;
			left.Total += _cells[left[left.Count].Right].Total;
			right.Total -= _cells[left[left.Count].Right].Total;
		}
		++left.Count;
		++left.Total;
		--right.Total;

		me.Index = right[0].Index;
		// shrink right node
		right.Count--;
		right[0].Left = right[0].Right;
		for (size_t i = 0; i < right.Count; ++i) {
			right[i].Index = right[i + 1].Index;
			right[i].Right = right[i + 1].Right;
		}
	}

	template<typename TRAITS>
	void BTreeBase<TRAITS>::merge(size_t leftIndex, size_t rightIndex, size_t parentOffset) {
		//    | L| C| R|             | L| R|
		//   /   /  \   \          /    |    \
		//    |A|B|  |D|     -->    |A|B|C|D|
		//   /  |  \ / \            / | | | \
		//   0  1  2 3 4            0 1 2 3 4
		cell_t & left = _cells[leftIndex];
		cell_t & right = _cells[rightIndex];
		cell_t & parent = _cells[left.Parent];
		// separator to left node
		left[left.Count].Index = parent[parentOffset].Index;
		left[left.Count].Right = right[0].Left;
		if (right[0].Left != UNDEFINED)
			_cells[right[0].Left].Parent = leftIndex;
		++left.Count;
		// items from right node into left node
		for (size_t i = 0; i < right.Count; ++i) {
			left[left.Count].Index = right[i].Index;
			left[left.Count].Right = right[i].Right;
			if (right[i].Right != UNDEFINED)
				_cells[right[i].Right].Parent = leftIndex;
			++left.Count;
		}
		// shrink parent
		--parent.Count;
		for (size_t i = parentOffset; i < parent.Count; ++i) {
			parent[i].Index = parent[i + 1].Index;
			parent[i].Right = parent[i + 1].Right;
		}
		right.Count = 0;
		// fix total count
		left.Total += right.Total + 1;
	}

	template<typename TRAITS>
	void BTreeBase<TRAITS>::recount(size_t index) {
		// recursively update number of items from given item to the root
		while (index != UNDEFINED) {
			recountLayer(index);
			index = _cells[index].Parent;
		}
	}

	template<typename TRAITS>
	void BTreeBase<TRAITS>::recountLayer(size_t index) {
		// sum up the subtree count in the cell
		cell_t & cell = _cells[index];
		cell.Total = cell.Count;

		// if not leaf, count subtrees
		if (cell.Data[0] != UNDEFINED) {
			for (size_t i = 0; i <= cell.Count; ++i) {
				cell.Total += _cells[cell.Data[2 * i]].Total;
			}
		}
	}

	template<typename TRAITS>
	void BTreeBase<TRAITS>::validateNode(size_t cellIndex) {
		// if number of items in the subtree is too low, validate it
		if (_cells[cellIndex].Count < (DEGREE / 2)) {
			size_t upperBoundIndex = UNDEFINED;
			size_t lowerBoundIndex = UNDEFINED;
			cell_t & leaf = _cells[cellIndex];
			cell_t & parent = _cells[leaf.Parent];

			// look up neighborhood
			for (size_t i = 0; i < parent.Count; ++i) {
				if (parent[i].Left == cellIndex) {
					upperBoundIndex = i;
				}
				if (parent[i].Right == cellIndex) {
					lowerBoundIndex = i;
				}
			}

			// most left node
			if (lowerBoundIndex == UNDEFINED) {
				size_t rightNeighborIndex = parent[upperBoundIndex].Right;
				cell_t & rightNeighbor = _cells[rightNeighborIndex];
				// right neighbor cannot be split
				if (rightNeighbor.Count <= (DEGREE / 2)) {
					// merge nodes
					merge(cellIndex, rightNeighborIndex, upperBoundIndex);
					if (leaf.Parent == _rootIndex)
						validateRoot(leaf.Parent);
					else
						validateNode(leaf.Parent);
				}
				// right neighbor can be split
				else {
					triangleToLeft(leaf.Parent, upperBoundIndex);
				}
			}
			// most right node
			else if (upperBoundIndex == UNDEFINED) {
				size_t leftNeighborIndex = parent[lowerBoundIndex].Left;
				cell_t & leftNeighbor = _cells[leftNeighborIndex];
				// left neighbor cannot be split
				if (leftNeighbor.Count <= (DEGREE / 2)) {
					// merge nodes
					merge(leftNeighborIndex, cellIndex, lowerBoundIndex);
					if (leaf.Parent == _rootIndex)
						validateRoot(leaf.Parent);
					else
						validateNode(leaf.Parent);
				}
				// left neighbor can be split
				else {
					triangleToRight(leaf.Parent, lowerBoundIndex);
				}
			}
			// inner node
			else {
				size_t rightNeighborIndex = parent[upperBoundIndex].Right;
				cell_t & rightNeighbor = _cells[rightNeighborIndex];
				size_t leftNeighborIndex = parent[lowerBoundIndex].Left;
				cell_t & leftNeighbor = _cells[leftNeighborIndex];
				// rotate elements from right neighbor
				if (rightNeighbor.Count > (DEGREE / 2)) {
					triangleToLeft(leaf.Parent, upperBoundIndex);
				}
				// rotate elements from left neighbor
				else if (leftNeighbor.Count > (DEGREE / 2)) {
					triangleToRight(leaf.Parent, lowerBoundIndex);
				}
				// merge with right neighbor
				else {
					// merge nodes
					merge(cellIndex, rightNeighborIndex, upperBoundIndex);
					if (leaf.Parent == _rootIndex)
						validateRoot(leaf.Parent);
					else
						validateNode(leaf.Parent);
				}
			}
		}
	}

	template<typename TRAITS>
	void BTreeBase<TRAITS>::validateRoot(size_t cellIndex) {
		if (_cells[cellIndex].Count == 0) {
			_rootIndex = _cells[_rootIndex][0].Left;
			_cells[_rootIndex].Parent = UNDEFINED;
			--_depth;
		}
	}
#pragma endregion

	/************************************************************************/
	/* BTreeBase - public methods                                           */
	/************************************************************************/
#pragma region public methods
	template<typename TRAITS>
	template<typename T>
	typename TRAITS::item_t & BTreeBase<TRAITS>::insert(T item) {
		// select cell from root until a leaf is reached
		size_t leafIndex, equalIndex, insertIndex;
		select(Traits::GetKey(item), leafIndex, equalIndex, insertIndex);
		// insert or replace
		if (equalIndex == UNDEFINED) {
			insert(leafIndex, insertIndex, store(static_cast<T>(item)));
			return get(leafIndex, insertIndex);
		}
		else {
			return get(leafIndex, equalIndex) = item;
		}
	}

	template<typename TRAITS>
	const typename TRAITS::item_t * BTreeBase<TRAITS>::GetItem(const typename TRAITS::key_t & key) const {
		const typename TRAITS::item_t * result = nullptr;
		size_t cell, offset, dummy;
		select(key, cell, offset, dummy);
		if (offset != UNDEFINED)
			result = &get(cell, offset);
		return result;
	}

	template<typename TRAITS>
	typename TRAITS::item_t & BTreeBase<TRAITS>::GetItemByIndex(size_t index) {
		size_t cell, offset;
		selectByIndex(index, _rootIndex, cell, offset);
		return get(cell, offset);
	}

	template<typename TRAITS>
	const typename TRAITS::item_t & BTreeBase<TRAITS>::GetItemByIndex(size_t index) const {
		size_t cell, offset;
		selectByIndex(index, _rootIndex, cell, offset);
		return get(cell, offset);
	}

	template<typename TRAITS>
	typename TRAITS::item_t * BTreeBase<TRAITS>::GetItem(const typename TRAITS::key_t & key) {
		typename TRAITS::item_t * result = nullptr;
		size_t cell, offset, dummy;
		select(key, cell, offset, dummy);
		if (offset != UNDEFINED)
			result = &get(cell, offset);
		return result;
	}

	template<typename TRAITS>
	void BTreeBase<TRAITS>::Clear() {
		_items.Clear();
		_cells.Clear();
		_rootIndex = _cells.AddNaive();
		_depth = 0;
	}

	template<typename TRAITS>
	void BTreeBase<TRAITS>::Free() {
		_items.Free();
		_cells.Clear();
		_rootIndex = _cells.AddNaive();
		_depth = 0;
	}

	template<typename TRAITS>
	bool BTreeBase<TRAITS>::Remove(const typename TRAITS::key_t & key) {
		size_t cell, offset, dummy;
		select(key, cell, offset, dummy);
		if (offset != UNDEFINED) {
			get(cell, offset).~ItemType();
			remove(cell, offset);
			return true;
		}
		return false;
	}
#pragma endregion

	/************************************************************************/
	/* BTreeBase - constructors                                             */
	/************************************************************************/
	template<typename TRAITS>
	BTreeBase<TRAITS>::BTreeBase() {
		// add cell to zero depth layer
		cell_t & root = _cells[_rootIndex = _cells.AddNaive()];
		root.Count = 0;
		root.Parent = UNDEFINED;
		// declare all children as undefined
		for (size_t i = 0; i <= DEGREE; ++i) {
			root.Data[2 * i] = UNDEFINED;
		}
	}

	/************************************************************************/
	/* BTreeBase::Iterator                                                  */
	/************************************************************************/
	template<typename TRAITS>
	template<typename TREE>
	void BTreeBase<TRAITS>::Iterator<TREE>::operator++() {
		if (_stack.Size() <= _tree->_depth) {
			_stack.Add(StackItem{ _tree->_cells[_stack.Last().Cell][_stack.Last().Index++].Right, 0 });
			while (_stack.Size() <= _tree->_depth) {
				_stack.Add(StackItem{ _tree->_cells[_stack.Last().Cell][_stack.Last().Index].Left, 0 });
			}
		}
		else {
			if (++(_stack.Last().Index) >= _tree->_cells[_stack.Last().Cell].Count) {
				do {
					_stack.RemoveLast();
					if (_stack.IsEmpty())
						return;
				} while (!(_stack.Last().Index < _tree->_cells[_stack.Last().Cell].Count));
			}
		}
	}

	template<typename TRAITS>
	template<typename TREE>
	BTreeBase<TRAITS>::Iterator<TREE>::Iterator(TREE * tree)
		: _tree(tree)
		, _stack(_tree->_depth + 1)
	{
		if (_tree->_cells[_tree->_rootIndex].Count) {
			_stack.Add(StackItem{ _tree->_rootIndex, 0 });
			for (size_t d = 0; d < _tree->_depth; ++d) {
				_stack.Add(StackItem{ _tree->_cells[_stack.Last().Cell][0].Left });
			}
		}
	}

	/************************************************************************/
	/* BTree<Item>                                                          */
	/************************************************************************/
	template<typename TYPE>
	typename BTree<TYPE>::TRAITS::value_t & BTree<TYPE>::operator[](const typename TRAITS::key_t & key) {
		// search for key
		size_t leafIndex, equalIndex, insertIndex;
		Base::select(key, leafIndex, equalIndex, insertIndex);
		// insert if key not stored
		if (equalIndex == Base::UNDEFINED) {
			Base::insert(leafIndex, insertIndex, Base::store(TYPE()));
			return Base::getValue(leafIndex, insertIndex);
		}
		else {
			return Base::getValue(leafIndex, equalIndex);
		}
	}

	template<typename TYPE>
	const typename BTree<TYPE>::TRAITS::value_t & BTree<TYPE>::operator[](const typename TRAITS::key_t & key) const {
		// search for key
		size_t leafIndex, equalIndex, insertIndex;
		Base::select(key, leafIndex, equalIndex, insertIndex);
		// insert if key not stored
		if (equalIndex == Base::UNDEFINED) {
			Base::insert(leafIndex, insertIndex, Base::store(TYPE()));
			return Base::getValue(leafIndex, insertIndex);
		}
		// no such that key stored
		throw;
	}

	/************************************************************************/
	/* BTree<Key, Value>                                                    */
	/************************************************************************/
	template<typename UKEY, typename UVALUE>
	UVALUE & BTree<UKEY, UVALUE>::operator[](const UKEY & key) {
		// search for key
		size_t leafIndex, equalIndex, insertIndex;
		Base::select(key, leafIndex, equalIndex, insertIndex);
		// insert if key not stored
		if (equalIndex == Base::UNDEFINED) {
			Base::insert(leafIndex, insertIndex, Base::store({ key, UVALUE() }));
			return Base::getValue(leafIndex, insertIndex);
		}
		else {
			return Base::getValue(leafIndex, equalIndex);
		}
	}

	template<typename UKEY, typename UVALUE>
	const UVALUE & BTree<UKEY, UVALUE>::operator[](const UKEY & key) const {
		// search for key
		size_t leafIndex, equalIndex, insertIndex;
		Base::select(key, leafIndex, equalIndex, insertIndex);
		// insert if key not stored
		if (equalIndex == Base::UNDEFINED) {
			Base::insert(leafIndex, insertIndex, Base::store({ key, UVALUE() }));
			return Base::getValue(leafIndex, insertIndex);
		}
		// no such that key stored
		throw;
	}

	template<typename UKEY, typename UVALUE>
	BTreePair<UKEY, UVALUE> & BTree<UKEY, UVALUE>::Insert(const UKEY & key, const UVALUE & value) {
		// select cell from root until a leaf is reached
		size_t leafIndex, equalIndex, insertIndex;
		Base::select(key, leafIndex, equalIndex, insertIndex);
		// insert or replace
		if (equalIndex == Base::UNDEFINED) {
			Base::insert(leafIndex, insertIndex, Base::store({ key, value }));
			return Base::get(leafIndex, insertIndex);
		}
		else {
			Base::getValue(leafIndex, equalIndex) = value;
			return Base::get(leafIndex, equalIndex);
		}
	}
}
