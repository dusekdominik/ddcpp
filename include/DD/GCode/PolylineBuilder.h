#pragma once
namespace DD::GCode {
	/** Helper for creating polyline geometry. */
	struct PolylineBuilder;
}

#include <DD/Array.h>
#include <DD/Math/Interval.h>
#include <DD/Types.h>
#include <DD/Vector.h>

namespace DD::GCode {
	struct PolylineBuilder {
	public: // nested
		/** Helper object for building polyline by relative changes. */
		struct ProxyX {
			PolylineBuilder & builder;
			PolylineBuilder & operator+=(f64 x) { return builder.AddRelativeX(x); }
			PolylineBuilder & operator-=(f64 x) { return builder.AddRelativeX(-x); }
		};
		/** Helper object for building polyline by relative changes. */
		struct ProxyY {
			PolylineBuilder & builder;
			PolylineBuilder & operator+=(f64 y) { return builder.AddRelativeY(y); }
			PolylineBuilder & operator-=(f64 y) { return builder.AddRelativeY(-y); }
		};
		/** Helper object for building polyline by relative changes. */
		struct ProxyXY {
			PolylineBuilder & builder;
			PolylineBuilder & operator+=(double2 xy) { return builder.AddRelative(xy); }
			PolylineBuilder & operator-=(double2 xy) { return builder.AddRelative(-xy); }
		};
		struct ProxyLine {
			double2 & X, & Y;
		};
		struct LineSequencer {
			PolylineBuilder & Builder;
			size_t Index;
			bool Reset() { Index = 1; return 2 <= Builder.Points.Size(); }
			bool Next() { return ++Index < Builder.Points.Size(); }
			ProxyLine Get() { return { Builder.Points[Index - 1], Builder.Points[Index] }; }
		};

	protected: // statics
		/** Helper method - normalized planar cross product of line represented by two points. */
		static double2 ncross(double2 x, double2 y) { double2 n = Math::Normalize(x - y); return { n.y, -n.x }; }

	public: // statics
		/**
		 * Create polyline representing a rectangle with discretly rounded corners
		 * @param radius         - radius of the corner roundation.
		 * @param size           - dimensions of the rectangle.
		 * @param discreetFactor - number of points for approximation rounded corner.
		 * @param precision      - precision of the grid, where points are placed.
		 */
		static PolylineBuilder RoundedRectangle(f64 radius, double2 size, size_t discreteFactor = 16, f64 precision = 0.001);
		/** Create polyline representing rectangle shape. */
		static PolylineBuilder Rectangle(DD::Math::Interval2D rect) { return PolylineBuilder().AddAbsolute(rect.Min).AddAbsoluteX(rect.Max.x).AddAbsoluteY(rect.Max.y).AddAbsoluteX(rect.Min.x).AddAbsoluteY(rect.Min.y); }

	public: // properties
		/** Point storage. */
		List<double2, 1024> Points;
		/** Move cursor by X axis perspective. */
		ProxyX X{ *this };
		/** Move cursor by Y axis perspective. */
		ProxyY Y{ *this };
		/** Move cursor. */
		ProxyXY XY{ *this };

	public: // type conversion
		using Polyline = ArrayView1D<const double2>;
		Polyline ToArrayView() const { return Points; }
		Sequencer::Naive2<Polyline> ToSequence() const { return {ToArrayView()}; }
		operator Polyline() const { return Points; }

	public: // methods
		/** Get last point of the polyline (not safe, last point could be non-existent). */
		const double2 & Last() { return Points.Last(); }
		/** Append Point in absolute coordinates. */
		PolylineBuilder & AddAbsolute(double2 xy) { Points.Add(xy); return *this; }
		PolylineBuilder & AddAbsoluteX(f64 x) { return AddAbsolute({ x, Last().y }); }
		PolylineBuilder & AddAbsoluteY(f64 y) { return AddAbsolute({ Last().x, y }); }
		/** Append point in relative coordinates. */
		PolylineBuilder & AddRelative(double2 xy) { return AddAbsolute(Last() + xy); }
		PolylineBuilder & AddRelativeX(f64 x) { return AddRelative({ x, 0.0 }); }
		PolylineBuilder & AddRelativeY(f64 y) { return AddRelative({ 0.0, y }); }
		/** Move all points by given value. */
		PolylineBuilder & Translate(double2 xy) { for (auto & p : Points) p += xy; return*this; }
		PolylineBuilder & TranslateX(f64 x) { for (auto & p : Points) p.x += x; return*this; }
		PolylineBuilder & TranslateY(f64 y) { for (auto & p : Points) p.y += y; return*this; }
		/** Multiply all coords by -1.  */
		PolylineBuilder & InvertX() { for (auto & p : Points) p.x = -p.x; return *this; }
		PolylineBuilder & InvertY() { for (auto & p : Points) p.y = -p.y; return *this; }
		PolylineBuilder & InvertXY() { for (auto & p : Points) p = -p; return *this; }
		PolylineBuilder Clone() const { return PolylineBuilder(*this); }
		/** Multiply all points by scaling factor. */
		PolylineBuilder & Scale(DD::double2 scale) { for (auto & p : Points) p *= scale; return *this; }
		/**
		 * Add polygon or its part to the polyline.
		 * @param center    - center of the polygon.
		 * @param radius    - 2D radius, specifies first point in relation to the center point (ie start = center + radius).
		 * @param ngon      - how many vertices should polygon have.
		 * @param n         - how many of the vertices should be added [1...ngon].
		 * @param clockwise - should be polygon  built in the cw or ccw manner.
		 * @param precision - what precision should be used @see DD::Math::Round for usage.
		 */
		PolylineBuilder & AddPolygon(double2 center, double2 radius, size_t ngon, size_t n, bool clockwise = false, f64 precision = 1E-3);
		/** Move the polyline lines along their normals by given size. */
		PolylineBuilder & Expand(f64 size, bool closed, bool removeLoops);
		/** Reverse the points in the list. */
		PolylineBuilder & Reverse();
		/**
		 * Create a spiral around the origin of coordinate system.
		 * @param startPoint     - where the spiral starts.
		 * @param perCycleGrowth - distance between two neighboring cycles.
		 * @param cycles         - number of rotations.
		 * @param ngon           - how many vertices represents one cycle
		 */
		PolylineBuilder & AddSpiral(double2 startPoint, f64 perCycleGrowth, u32 cycles, u32 ngon);
		PolylineBuilder & AddPolyline(const ArrayView1D<const double2> & rhs) { Points.AddIterable(rhs); return *this; }
	};
}
