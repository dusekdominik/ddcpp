#pragma once
namespace DD::GCode {
	/** GCode::Code along with its parameters. */
	struct Command;
}

#include <DD/GCode/Codes.h>
#include <DD/String.h>

namespace DD::GCode {
	struct Command {
		/** Type of GCode. */
		Codes Code = Codes::_Invalid;
		/** Position params, NaN value is currently used as "unset value". */
		f64 X = NAN;
		f64 Y = NAN;
		f64 Z = NAN;
		/** Feed-rate param. */
		i32 F = 0;
		/** Fan/Head speed param. */
		i32 S = -1;
		/** Fan index param. */
		u8 P = 0;
		/** Debugging note pushed into gcode. */
		String Comment;

		/** Append gcode into the given buffer. */
		void Feed(String & buffer) const;

		/** Constructor. */
		Command(Codes code = Codes::_Invalid) : Code(code) {}
		/** Constructor. */
		Command(Codes code, i32 feedRate) : Code(code), F(feedRate) {}
		/** Constructor. */
		Command(String comments) : Code(Codes::_Comment), Comment(comments) {}
	};
}
