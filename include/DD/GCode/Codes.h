#pragma once
namespace DD::GCode {
	/** Codes for CNC, derived from snapmaker/onefinity machines. */
	enum struct Codes;
}

#include <DD/GCode/Hash.h>

namespace DD::GCode {
	enum struct Codes {
		_Invalid = 0,
		// Signature G
		_MoveCode = 1 << 31,
		// Signature M
		_DeviceCode = 1 << 30,
		// Debug purpose
		_Comment = 1 << 29,
		// Flag mask
		_Flags = _MoveCode | _DeviceCode | _Comment,

		// https://snapmaker.github.io/Documentation/gcode/G000-G001
		Go0 = _MoveCode | Hash("G0"),
		Go1 = _MoveCode | Hash("G1"),
		// https://snapmaker.github.io/Documentation/gcode/G004
		Dwell = _MoveCode | Hash("G4"),
		// https://snapmaker.github.io/Documentation/gcode/G020
		Inches = _MoveCode | Hash("G20"),
		// https://snapmaker.github.io/Documentation/gcode/G021
		Millimeters = _MoveCode | Hash("G21"),
		// https://snapmaker.github.io/Documentation/gcode/G090
		Absolute = _MoveCode | Hash("G90"),
		// https://snapmaker.github.io/Documentation/gcode/G091
		Relative = _MoveCode | Hash("G91"),
		// https://snapmaker.github.io/Documentation/gcode/M003
		HeadOn = _DeviceCode | Hash("M3"),
		// https://snapmaker.github.io/Documentation/gcode/M005
		HeadOff = _DeviceCode | Hash("M5"),
		// https://snapmaker.github.io/Documentation/gcode/M106
		FanOn = _DeviceCode | Hash("M106"),
		// https://snapmaker.github.io/Documentation/gcode/M107
		FanOff = _DeviceCode | Hash("M107"),

		// Special codes
		Laser40_2000 = _DeviceCode | Hash("M2000"),
		Laser40_2003 = _DeviceCode | Hash("M2003"),
		Laser40_2004 = _DeviceCode | Hash("M2004"),
		AirPumpOff = _DeviceCode | Hash("M9"),
	};
}
