#pragma once
#include <DD/Types.h>
#include <DD/Text/StringView.h>

namespace DD::GCode {
	/** Default hash function for gcode purposes - constexpr version. */
	template<typename CHAR, size_t SIZE>
	constexpr u16 Hash(const CHAR(&str)[SIZE]) { return ::DD::Text::StringView(str).Hash<u16>(); };
	/** Default hash function for gcode purposes - dynamic version. */
	inline u16 Hash(const Text::StringView16 & str) { return str.Hash<u16>(); };
}