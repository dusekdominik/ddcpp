#pragma once
namespace DD::GCode {
	/** Tool tracking state machine with respect to applied commands. */
	struct Tracker;

}

#include <DD/GCode/Command.h>
#include <DD/GCode/MachineState.h>
#include <DD/Math/Interval.h>
#include <DD/Types.h>

namespace DD::GCode {
	struct Tracker {
		/** Previous machine state. */
		MachineState PreviousState;
		/** Current machine state. */
		MachineState CurrentState;
		/** Current feed-rate (speed of the machine movement). */
		i32 FeedRate = 0;
		/** Estimated time (no acceleration considered) [min]. */
		f64 Time = 0.0;
		/** Time when toolhead is not activated. */
		f64 VoidTime = 0.0;
		/** Length of path [mm]. */
		f64 Track = 0.0;
		/** Length of the last track [mm]. */
		f64 LastTrack;
		/** Extremes of already applied commands. */
		struct {
			Math::Interval3D Position;
			Math::Interval1I FeedRate;
		} Extrema;

		/** Return true if command is changing position. */
		bool ApplyCommand(const Command & cmd);
	};
}
