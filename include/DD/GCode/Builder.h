#pragma once
namespace DD::GCode {
	/** Helper for creating GCodes. */
	struct Builder;
}

#include <DD/GCode/Command.h>
#include <DD/Gcode/Tracker.h>
#include <DD/FileSystem/Path.h>
#include <DD/List.h>
#include <DD/String.h>
#include <DD/Svg.h>
#include <DD/Types.h>
#include <DD/Vector.h>
#include <DD/Math/Interval.h>
#include <DD/Math/LineSegment.h>
#include <DD/Math/VectorSequence.h>
#include <DD/Sequencer/Argument2.h>

namespace DD::GCode {
	struct Builder {
	private: // nested
		/** Helper for initialization milling process. */
		struct MillingScope;
		/** Helper for explicit indexing of go commands. */
		enum MoveCommandMode { IDLE, WORK };

	public: // nested
		/** Alias for line strips */
		using Polyline = ArrayView1D<const double2>;
		/** Alis for input of multiple polylines. */
		using PolylineInput = Sequencer::Argument2<Polyline>;
		/** Append modes - when one gcode is appended to another. */
		enum AppendModes {
			/** No transformation will be applied. */
			NoTransformation = 0,
			/** Sign change of X-axis.  */
			FlipX = 1,
			/** Sign change of Y-axis.  */
			FlipY = 2,
			/** Swap x and y axes of coordinates. */
			SwapXY = 4,
			/** Sign change of X and Y axes. */
			FlipXY = FlipX | FlipY,
			/** Compound changes. */
			SwapXY_FlipXY = SwapXY | FlipXY,
			SwapXY_FlipX = SwapXY | FlipX,
			SwapXY_FlipY = SwapXY | FlipY,
		};
		/** Sequencer for clipping polylines. */
		struct Clipper;
		/**
		 * Helper for selection lines from given area only.
		 * Intention is to work with plywood segment by segment to prevent laser collision due to material bending.
		 */
		struct ClipSequencer;
		/** Common parameters for milling tasks. */
		struct MillingParams;
		/** Polyline descriptor. */
		struct PolylineCuttingParam;
		/** Rectangle descriptor. */
		struct RectangleMillingParams { Math::Interval2D Rectangle; };
		/** Parameters for Rectangle filling tasks. */
		struct RectangleCuttingParams;
		/** Helper for computing z-steps. */
		struct ZSteppingSequencer;

	public: // statics
		/** Get Z-stepping sequencer. */
		static Sequencer::Driver<ZSteppingSequencer> ZStepping(const MillingParams & mp);

	protected: // properties
		/** Commands. */
		List<Command, 1024> _commands;
		/** Relative or absolute. */
		Tracker _tracker;

	protected: // methods
		/** Load a single GCommand. */
		void loadGCommand(Text::StringView16::CharSplitter<Text::SplitterFlags::SkipEmptyMatches> params);
		/** Load one lien of GCode text representation. */
		void loadLine(const String & line);
		/** go to zero work position. */
		void goZero(const MillingParams & params, const float3 & zero);

	public: // basic commands
		/** Add a command. */
		void AddCommand(const Command & cmd);
		/** Init call, mm and relative positioning. */
		void AddHeader();
		/** Set positioning. */
		void AddAbsolute() { AddCommand(Codes::Absolute); }
		/** Set positioning. */
		void AddRelative() { AddCommand(Codes::Relative); }
		/** Set measurement. */
		void AddMillimeters() { AddCommand(Codes::Millimeters); }
		/** Set fan. */
		void AddFanOn(i32 power = 255);
		/** Turn the head on. */
		void AddTurnOn(i32 power = -1);
		/** Turn the head off. */
		void AddTurnOff() { AddCommand(Codes::HeadOff); }
		/** Set power of a toolhead. */
		void AddToolPower(i32 power = -1);
		/** Add move command. */
		void AddGo(f64 x, f64 y = NAN, f64 z = NAN, u8 index = IDLE, i32 f = 0);
		/** Add move command in X-axis. */
		void AddGoX(f64 x, u8 index = IDLE, i32 feedRate = 0) { AddGo(x, NAN, NAN, index, feedRate); }
		/** Add move command in Y-axis. */
		void AddGoY(f64 y, u8 index = IDLE, i32 feedRate = 0) { AddGo(NAN, y, NAN, index, feedRate); }
		/** Add move command in Z-axis. */
		void AddGoZ(f64 z, u8 index = IDLE, i32 feedRate = 0) { AddGo(NAN, NAN, z, index, feedRate); }
		/** Add move command in XY-axis. */
		void AddGoXY(const double2 & t, u8 index = IDLE, i32 feedRate = 0) { AddGo(t.x, t.y, NAN, index, feedRate); }
		/** Set current feed rate. */
		void AddFeedRate(i32 feedRate, u8 index = IDLE) { AddGo(NAN, NAN, NAN, index, feedRate); }
		/** Set current feed rate. */
		void AddFeedRates(i32 feedRate0, i32 feedRate1) { AddFeedRate(feedRate0, 0); AddFeedRate(feedRate1, 1); }
		/** Debugging thing pushes a comment to the gcode file. */
		void AddComment(const Text::StringView16 & comment) { _commands.Append(Command(comment)); }
		/** Add dwell time in milliseconds */
		void AddDwell(i32 milliseconds) { _commands.Add(Codes::Dwell); }
		/** Append another gcode. */
		void AddGCode(const Builder & gcode, const double2 & offset = 0.0, bool flipAxes = false, bool flipX = false, bool flipY = false);
		/** Remove all. */
		void Clear() { _tracker = Tracker(); _commands.Clear(); }
		/** Set a new value to each M3 command with S param. */
		void ResetHeadSpeedFactors(i32 S) { for (Command & command : _commands) if (command.Code == Codes::HeadOn && command.S != -1) command.S = S; }
		/** Set a new value to each G1 command with F param. */
		void ResetFeedRateFactors(i32 F) { for (Command & command : _commands) if (command.Code == Codes::Go1 && command.F != 0) command.F = F; }
		/**Move gcode by given offset */
		void TranslateXY(const double2 & offset, bool flipAxes = false, bool flipX = false, bool flipY = false) { Builder tmp; tmp.AddGCode(*this, offset, flipAxes, flipX, flipY); *this = tmp; }
		/** Make GCode to be in positive coordinates only. */
		void NormalizeXY() { TranslateXY(-AABox().Min.xy); }

	public: // accession
		/** AABB of currently generated gcode. */
		const Math::Interval3D & AABox() const { return _tracker.Extrema.Position; }
		/** Extrema of feed-rate of currently generated gcode. */
		const Math::Interval1I & FeedRates() const { return _tracker.Extrema.FeedRate; }
		/** Get currently set position. */
		const double3 & CurrentPosition() const { return _tracker.CurrentState.Position; }
		/** Get currently set feed rate. */
		f64 CurrentFeedRate() const { return _tracker.FeedRate; }
		/** Get commands. */
		const List<Command> & Commands() const { return _commands; }
		/** Get estimated gcode time [min]. */
		f64 Time() const { return _tracker.Time; }
		/** Get estimated gcode time [min] when toolhead is off. */
		f64 VoidTime() const { return _tracker.VoidTime; }
		/** Get estimated gcode track [mm]; */
		f64 Track() const { return _tracker.Track; }
		/** X-maximum of aabb. */
		f64 XMax() const { return _tracker.Extrema.Position.Max.x; }
		/** X-minimum of aabb. */
		f64 XMin() const { return _tracker.Extrema.Position.Min.x; }
		/** Y-maximum of aabb. */
		f64 YMax() const { return _tracker.Extrema.Position.Max.y; }
		/** Y-minimum of aabb. */
		f64 YMin() const { return _tracker.Extrema.Position.Min.y; }

	public: // AdvancedCommands
		/** Cut a polyline, only follow the path. . */
		void CutPolylineNative(const MillingParams & mparams, const Polyline & params);
		/** Layer-oriented cut. */
		void CutPolylinesNative(const MillingParams & mp, PolylineInput polylines, double2 partes = 1.0, double2 overlaps = 1.0);
		/**
		 * Fill the given set of polygons.
		 * Polygon overlaps are interpreted as an island in polygon.
		 * More specifically, an area covered by odd number of polygons is being filled,
		 * area covered by even number of polygons is not.
		 */
		void FillPolygonNative(const MillingParams & mparams, const Array<Array<double2>> & params);
		/**
		 * Do the outline and fill even parts (similarly as SVG does)
		 * @param polylines, sequencer with polylines.
		 */
		void CutAndFillLineModel(
			const MillingParams & mpOutline,
			const MillingParams & mpFill,
			PolylineInput polylines,
			bool invert = false
		);
		void CF2(const MillingParams & mpOutline, PolylineInput polylines, bool perSlice);

		void DashedLineModel(
			f64 dash,
			f64 undash,
			const MillingParams & mpOutline,
			Sequencer::ISequencer2<Array<double2>> * polylines
		);
		/** */
		//void CutLineModel(const MillingParams & mparams, Sequencer::ISequencer2<Array<double2>> & params);
		/** Drill a point. */
		void CutPointNative(const MillingParams & mparams, const double2 & point);
		/** Cut line defined by two points. */
		void CutLineNative(const MillingParams & mparams, const Math::LineSegment2D & params);

		/** Fill axis aligned rectangle area. */
		void FillRectangleAbsolute(const MillingParams & mparams, const RectangleMillingParams & rfparams);
		/** Cut axis aligned rectangle area. */
		void CutRectangleAbsolute(const MillingParams & mparams, const RectangleCuttingParams & params);
		/** Cut a polyline, only follow the path. */
		void CutPolyline(const MillingParams & mparams, const PolylineCuttingParam & params, const Polyline & polyline);

	public: // file handling
		/** Create string with gcode. */
		String ToString() const;
		/** Save data to a file. */
		void Save(const FileSystem::Path & path);
		/** Try to create a visualization.  */
		Svg::Document ToSvg();
		/** Export visualization to a file. */
		void ExportSvg(const FileSystem::Path & path) { ToSvg()->SaveUtf8(path); }
		/** Append code from a gcode file (does not remove already applied code). */
		bool Load(const FileSystem::Path & path);

	public: // constructors
		/** Default constructor. */
		Builder() = default;
		/** Read a file. */
		Builder(const FileSystem::Path & path) { Load(path); }
	};


	struct Builder::PolylineCuttingParam {
		/** Cutting strategy. */
		enum Strategies {
			Uniform,    // Cut all the edges at once
			LongestEdge // Cut first all the edges but the longest, let longest to be cut at last
		} Strategy = Strategies::Uniform;
		/** Inner cut of outer shape(true), outer cut of inner shape(false) */
		bool InnerCut = true;
		/** Should be corners cut. */
		bool FixCorners = true;
	};


	struct Builder::MillingParams {
		/** Used only in filling tasks [mm]. */
		f64 FillStep = 0.5;
		/** Step of milling from Z-Axis perspective. [mm] */
		f64 DownStep = 0.5;
		/** Absolute z-height considered as safe for router movement [mm]. */
		f64 SafeHeight = 30.0;
		/** Diameter of cutting bit of the router [mm]. */
		f64 BitDiameter = 3.175;
		/** Tool-head speed [mm/min]. */
		i32 FeedRateWork = 150;
		/** Tool-head speed [mm/min]. */
		i32 FeedRateIdle = 1500;
		/** Interval that should be milled. [mm] */
		Math::Interval1D Z = Math::Interval1D{ -1.0 };
		/** Use laser specific settings. */
		bool Laser = false;
		/** Number of passes at one layer, applies only when @see Laser enabled. */
		u32 LaserPasses = 1;
		/** -1 by default, otherwise set desired PWM for given toolhead. */
		i32 ToolHeadPower = -1;
		/** Between polylines jump up to safe height to cool the drill. */
		bool CoolJump = true;
	};


	struct Builder::RectangleCuttingParams
		: public Builder::RectangleMillingParams
	{
		/** Cutting strategy. */
		enum struct Strategies {
			Uniform,     // all edges are cut simultaneously
			LongEdge,    // final cut is done on a long edge
			ShortEdge,   // final cut is done on a short edge
			Top,         // final cut is done on the top edge (Y-)
			Bottom,      // final cut is done on the bottom edge (Y+)
			Left,        // final cut is done on the left edge (X+)
			Right        // final cut is done on the right edge (X-)
		} Strategy = Strategies::Uniform;

		/** If inner, the edges are shrieked by bit properties, otherwise shortened. */
		bool Inner = true;
	};


	struct Builder::ClipSequencer {
	private: // nested
		/** Helper enum for marking dimensions. */
		enum { XCOORD, YCOORD };

	public: // properties
		/** Source polyline. */
		const Polyline & Source;
		/** Area where lines are valid. */
		Math::Interval2D Clip;
		/** BUffer for subpolylines. */
		List<double2, 64> Buffer;
		/** Current state of sequencing (index to Source array). */
		size_t Index;

	private: // methods
		/**
		 * Compute an intersection with Clip boundary and check its validity.
		 * @param point - point with coordinate fixed in one dim (X or Y),
		 *                result will be computed to the complementary dim.
		 * @param a, d - point and direction
		 * @param c - equasion coefficient, serves for point ordering: point = (a + c * d).
		 */
		template<size_t COORD>
		bool IsFixable(double2 & point, double2 a, double2 d, f64 & c) {
			if (d[COORD] == 0.0)
				return false;

			// compute point on the clip line
			c = (point[COORD] - a[COORD]) / d[COORD];
			point[!COORD] = a[!COORD] + d[!COORD] * c;

			// point is on the given line and on the clip boundary
			return 0.0 <= c && c <= 1.0 && Clip.Contains<!COORD>(point[!COORD]);
		}
		/** Find intersection with clip boundary, @param a, b - points of line. */
		double2 SelectMissingPoint(double2 a, double2 b) {
			double2 d = b - a;
			double2 point;
			f64 c;
			if (
				!IsFixable<XCOORD>(point.Set<XCOORD>(Clip.Min.x), a, d, c) &&
				!IsFixable<XCOORD>(point.Set<XCOORD>(Clip.Max.x), a, d, c) &&
				!IsFixable<YCOORD>(point.Set<YCOORD>(Clip.Min.y), a, d, c) &&
				!IsFixable<YCOORD>(point.Set<YCOORD>(Clip.Max.y), a, d, c)
				) throw;

			return point;
		}
		/** @param a, b - points of line, @param aa, bb  output if intersection is valid. */
		bool TryClip(double2 a, double2 b, double2 & aa, double2 & bb) {
			// direction vector
			double2 d = b - a;
			// buffer for collected points
			double2 points[4];
			// buffer for collected coeficients
			f64 cs[4];
			// number of collected points
			size_t cnt = 0;

			// check all the clip sides for intersections
			cnt += IsFixable<XCOORD>(points[cnt].Set<XCOORD>(Clip.Min.x), a, d, cs[cnt]);
			cnt += IsFixable<XCOORD>(points[cnt].Set<XCOORD>(Clip.Max.x), a, d, cs[cnt]);
			cnt += IsFixable<YCOORD>(points[cnt].Set<YCOORD>(Clip.Min.y), a, d, cs[cnt]);
			cnt += IsFixable<YCOORD>(points[cnt].Set<YCOORD>(Clip.Max.y), a, d, cs[cnt]);

			// we should found at most two valid intersections
			if (cnt > 2)
				throw;

			// assign in correct order
			if (cs[0] < cs[1])
				aa = points[0], bb = points[1];
			else
				bb = points[0], aa = points[1];


			return cnt == 2;
		}

	public: // Sequencer methods
		bool Reset() { Index = 1; return Next(); }
		bool Next() {
			// reset buffer
			Buffer.Clear();

			// iterate over continues polyline, continuous part in the clip push to the buffer
			for (; Index < Source.Length(); ++Index) {
				// assign line points
				const double2 & a = Source[Index - 1];
				const double2 & b = Source[Index];

				// generate basic states according to having points in or out of the clip
				size_t hashA = Clip.ClipHash(a);
				size_t hashB = Clip.ClipHash(b);
				// if points are clipped by a same line, they cannot be in clip
				if (hashA & hashB)
					continue;

				// we add a if it is in and when we started collecting
				if (hashA == 0 && Buffer.IsEmpty())
					Buffer.Add(a);

				// both are in
				if (hashA == 0 && hashB == 0) {
					Buffer.Add(b);
				}
				// second is in
				else if (hashB == 0) {
					// if a is not in clip, we just started collecting
					Buffer.Add(SelectMissingPoint(a, b), b);
				}
				// first is in
				else if (hashA == 0) {
					Buffer.Add(SelectMissingPoint(a, b));
					// break because we found an interruption
					break;
				}
				// none is in
				else {
					double2 aa, bb;
					if (TryClip(a, b, aa, bb)) {
						Buffer.Add(aa, bb);
						// we got interrupted
						break;
					}
				}
			}

			++Index;
			return Buffer.Size() != 0;
		}
		Array<double2> Get() { return Array<double2>::Copy(Buffer); }

	public: // constructors
		/** Constructor. */
		ClipSequencer(const Polyline & source, const Math::Interval2D & clip) : Source(source), Clip(clip) {}
		ClipSequencer(ClipSequencer &&) = default;
	};


	struct Builder::ZSteppingSequencer {
		/** Iteration variables */
		DD::f64 Min, Max, Step, At;

		DD::f64 Get() { return At; }
		bool Next() { bool result = Min < At;  At = DD::Math::Max(Min, At - Step); return result; }
		bool Reset() { return Min <= (At = Max); }

		/** Constructor - make stepping out milling params. */
		ZSteppingSequencer(const MillingParams & mp) : Min(mp.Z.Min[0]), Max(mp.Z.Max[0]), Step(mp.DownStep) {}
	};


	struct Builder::Clipper {
		/** Polyline with bbox. */
		struct PolylineEx {
			/** BBox of the line. */
			Math::Interval2D BBox;
			/** Array of points. */
			Array<double2> Line;
			/** Compute bbox. */
			PolylineEx(const Polyline & polyline) : Line(Array<double2>::Copy(polyline)) { for (const double2 & pt : Line) BBox.Include(pt); }
			PolylineEx() = default;
		};

		/** Original polylines. */
		List<PolylineEx> Polylines;
		/** BBox of all polylines. */
		Math::Interval2D BBox;
		/** Current selection of clipped polylines. */
		List<Array<double2>> Selection;
		/** Indexing of clip offsets. */
		//Math::VectorSequencer2D ClipOffsets;

		int2 Cursor;
		/** Number of clips per dimension. */
		double2 Portions;
		/** Size of one clip.  */
		double2 ClipSize;
		/**  */
		double2 OverLap = { 1.0, 1.0 };

		/** Make selection data from polylines wrt current clip. */
		bool Load() {
			// reset buffer
			Selection.Clear();

			// compute current clip
			//double2 offset = ClipOffsets.Get();
			double2 offset = Cursor;
			Math::Interval2D clip(BBox.Min + offset * ClipSize - OverLap, BBox.Min + (offset + 1.0) * ClipSize + OverLap);

			// push clipped polylines to the buffer
			for (const PolylineEx & polyline : Polylines) {
				if (Math::Interval2D::Intersects(polyline.BBox, clip)) {
					Selection.AddSequence(ClipSequencer{ polyline.Line, clip });
				}
			}

			// return true if anything in the selection
			return Selection.IsNotEmpty();
		}
		bool Reset() { Cursor = 0; return /*ClipOffsets.Reset() && */(Load() || Next()); }

		bool NextCursor() {
			bool fwd = (Cursor.y % 2) == 0;
			if (fwd) {
				if (Portions.x <= ++Cursor.x) {
					Cursor.x = (i32)Portions.x - 1;
					Cursor.y += 1;
				}
			}
			else {
				if (--Cursor.x < 0) {
					Cursor.x = 0;
					Cursor.y += 1;
				}
			}

			return Cursor.y < Portions.y;
		}
		bool Next() { while (NextCursor()) if (Load()) return true; return false; }
		auto Get() { return Sequencer::Drive(Selection).Cast<Polyline>().Virtualize(); }

		/** Constructor. */
		Clipper(Sequencer::ISequencer2<Polyline> * polylines, double2 portions, double2 overlap)
			: Portions(portions)
			//, ClipOffsets(Math::VectorSequence2D(portions - 1.0))
			, OverLap(overlap)

		{
			// drop all the polylines from sequencer to list
			auto sequence = Sequencer::Drive(polylines).Cast<PolylineEx>();
			Polylines.AddSequence(sequence);
			// precompute bbox
			for (const PolylineEx & polyline : Polylines)
				BBox.Include(polyline.BBox);

			ClipSize = BBox.Size() / portions;
		}
	};
}
