#pragma once
namespace DD::GCode {
	/** Descriptor of a CNC machine state. */
	struct MachineState;
}

#include <DD/GCode/Codes.h>
#include <DD/Vector.h>
#include <DD/Types.h>

namespace DD::GCode {
	struct MachineState {
		/** Measurement modes. */
		enum struct MeasurementModes {
			Undefined = (i32)Codes::_Invalid,
			Inches = (i32)Codes::Inches,
			Millimeters = (i32)Codes::Millimeters,
		} MeasureMode = MeasurementModes::Undefined;
		/** Interpretation of Go commands. */
		enum struct PositionModes {
			Undefined = (i32)Codes::_Invalid,
			Absolute = (i32)Codes::Absolute,
			Relative = (i32)Codes::Relative
		} PositionMode = PositionModes::Undefined;
		/** Tool-head state. */
		enum struct HeadModes {
			On = (i32)Codes::HeadOn,
			Off = (i32)Codes::HeadOff
		} HeadMode = HeadModes::Off;
		/** Fan states */
		enum struct FanModes {
			On = (i32)Codes::FanOn,
			Off = (i32)Codes::FanOff
		} FanMode = FanModes::Off;
		/** Power of the head. */
		i32 HeadPower;
		/** Head position. */
		double3 Position = 0.0;
		/** Which way we were coming to the current position.  */
		double3 Direction = 0.0;
		/** Feed-rates set for G0 and G1 codes. */
		i32 FeedRates[2] = { 0, 0 };
	};
}
