#pragma once
namespace DD {
	/**
	 * Linked list implementation.
	 *
	 * Access (O(1) - O(n))
	 *   General access is linear.
	 *   Access is cached so access to indices near to following is very fast:
	 *     - First index.
	 *     - Last index.
	 *     - Last accessed index.
	 * Insertion (O(1) - O(n))
	 *   Constant time if insertion is done on cached indices (first/last/accessed).
	 *   Random insertion is basically linear.
	 *   Algo steps:
	 *     - Resize if no space left (binary expansion of storage).
	 *     - Get chain cell from the end of storage, store the data in.
	 *     - Access the position, insert chain cell.
	 * Removing (O(1) - O(n))
	 *   Removing time is constant if it is done on cached indices (first/last/accessed).
	 *   Random removing is basically linear.
	 *   Algo steps:
	 *     - Access the position.
	 *     - Remove the data stored in.
	 *     - Take the last item of storage and swap it with removed item (to keep consistent).
	 */
	template<typename T, size_t ALLOCATION = 0>
	class Chain;
}

#include <DD/Array.h>
#include <DD/Collections/Chain.h>
#include <DD/Iterator/Nested.h>

namespace DD {
	template<typename T, size_t ALLOCATION>
	class Chain {
		friend class Chain<T>;

	public: // nested
		template<typename RTYPE, typename LINKTYPE>
		struct Traits {
			using ReturnType = RTYPE;
			static constexpr ReturnType Apply(LINKTYPE lnk) { return *lnk; }
		};
		/**  */
		//using LinkType = DD::Collections::LinkT<T>;
		//using ChainType = DD::Collections::ChainT<T>;
		//using Iterator = DD::Iterator::Nested<typename ChainType::Iterator, Traits<T &, LinkType &>>;
		//using ConstIterator = DD::Iterator::Nested<typename ChainType::ConstIterator, const Traits<T &, const LinkType &>>;
		/** Bidirectional node. */
		struct Node;
		/** Iterator template. */
		template<typename NODE, typename VALUE>
		class Iterator;

		/** Iterator. */
		typedef Iterator<Node, T> iterator_t;
		/** Const iterator. */
		typedef Iterator<const Node, const T> const_iterator_t;

	public: // statics
		/** Default preallocation of Chain. */
		static constexpr size_t PREALLOCATION = 0;

	private: // properties
		//DD::Collections::ChainT<LinkType> _chain;


		/** Look up cache. */
		mutable struct { iterator_t Node; size_t Index; } _selected;
		/** Last node of chain - contains first value. */
		Node * _first;
		/** Last node of chain - contains no value. */
		Node * _last;
		/** Number of stored items (includes empty end and empty begin). */
		size_t _size;
		/** Items store. */
		Array<Node, ALLOCATION> _nodes;

	private: // methods
		/** Add item to storage. */
		Node * store(const T & t);
		/** Add item to storage. */
		Node * store(T && t);
		/** Selects cache on given index. */
		iterator_t select(size_t index) const;
		/** Connect node at the end of chain. */
		void appendNode(Node * node);
		/** Connect node into given index. */
		void insertNode(size_t index, Node * node);
		/** Remove node. */
		void removeNode(Node * n);
		/** Remove node - without reordering. */
		void removeNodeFast(Node * n);

	public: // accession
		/** Number of items which can be stored. */
		size_t Capacity() { return _nodes.Length() - (_size + 1); }
		/** Number of stored items. */
		size_t Size() const { return _size - 1; }
		/** Iterator. */
		iterator_t begin() { return { _first->Right }; }
		/** Iterator. */
		iterator_t end() { return { _last }; }
		/** First item of chain. */
		T & First() { return _first->Right->Value; }
		/** Last item of chain. */
		T & Last() { return _last->Left->Value; }
		/** Indexer. */
		T & operator[] (size_t index) { return *select(index); }
		/** Const iterator. */
		const_iterator_t begin() const { return { _first->Right }; }
		/** Const iterator. */
		const_iterator_t end() const { return { _last }; }
		/** First item of chain. */
		const T & First() const { return _first->Right->Value; }
		/** Last item of chain. */
		const T & Last() const { return _last->Left->Value; }
		/** Const indexer. */
		const T & operator[] (size_t index) const { return *select(index); }

	public: // insertion
		/** Adds value at the end of chain (O(1)). */
		void Add(const T & n) { appendNode(store(n)); }
		/** Adds value at the end of chain (O(1)). */
		void Add(T && n) { appendNode(store(n)); }
		/** Insert value into given index (O(1) - O(n)). */
		void Insert(size_t index, const T & value) { insertNode(index, store(value)); }
		/** Insert value into given index (O(1) - O(n)). */
		void Insert(size_t index, T && value) { insertNode(index, store(Fwd(value))); }
		/** Remove node with given content (O(n)). */
		void Remove(const T & remove);
		/** Remove first item (O(1)). */
		void RemoveFirst() { removeNode(_first->Right); --_selected.Index; }
		/** Remove last item (O(1)). */
		void RemoveLast() { removeNode(_last->Right); }
		/** Remove node at given index (O(1) - O(n)). */
		void RemoveAt(size_t index);
		/** Remove node at given index - should not be added new nodes before reordering. */
		void RemoveAtFast(size_t index);
		/** Reorder chain - chain indices will be corresponding to array list indices. */
		void Reorder();
		/** Reserve space for additional n nodes. */
		void Reserve(size_t n);
		/** Remove all items. */
		void Clear();

	public: // constructors
		/** Default constructor. */
		Chain(size_t preallocation = PREALLOCATION);
	};


	template<typename T, size_t ALLOCATION>
	struct Chain<T, ALLOCATION>::Node {
		/** Stored value. */
		T Value;

		/** Reference of index - previous node. */
		Node * Left = 0;
		/** Reference of index - next node. */
		Node * Right = 0;
		/** Constructor. */
		Node(const T & v) : Value(v), Left(0), Right(0) {}
		/** Move constructor. */
		Node(T && v) : Value(v), Left(0), Right(0) {}
		/** Zero constructor. */
		Node() : Left(0), Right(0) {}
	};


	template<typename T, size_t ALLOCATION>
	template<typename NODE, typename VALUE>
	class Chain<T, ALLOCATION>::Iterator {
		/** Node. */
		NODE * _node;

	public:
		/** Forward preincrement. */
		Iterator & operator++() { _node = _node->Right; return *this; }
		/** Backward preincrement. */
		Iterator & operator--() { _node = _node->Left; return *this; }
		/** Forward random access. */
		Iterator & operator+=(size_t i) { while (i--) ++(*this); return *this; }
		/** Backward random access. */
		Iterator & operator-=(size_t i) { while (i--) --(*this); return *this; }
		/** Forward postincrement. */
		Iterator operator++(int) const { return ++Iterator(_node); }
		/** Backward postincrement. */
		Iterator operator--(int) const { return --Iterator(_node); }
		/** Const forward random access. */
		Iterator operator+(size_t i) const { return Iterator(_node) += i; }
		/** Const backward random access. */
		Iterator operator-(size_t i) const { return Iterator(_node) -= i; }
		/** Inequality check. */
		bool operator!=(const Iterator & i) const { return _node != i._node; }
		/** Dereference. */
		VALUE & operator*() { return _node->Value; }
		/** Dereference. */
		VALUE & operator->() { return _node->Value; }
		/** Conversion operator. */
		operator NODE * () const { return _node; }
		/** Implicit constructor. */
		Iterator(NODE * node) : _node(node) {}
		/** Zero constructor. */
		Iterator() {}
	};
}


namespace DD {
	template<typename T, size_t ALLOCATION>
	typename Chain<T, ALLOCATION>::Node * Chain<T, ALLOCATION>::store(const T & t) {
		Reserve(1);
		_nodes[++_size] = Node(t);
		return _nodes.begin() + _size;
	}


	template<typename T, size_t ALLOCATION>
	typename Chain<T, ALLOCATION>::Node * Chain<T, ALLOCATION>::store(T && t) {
		Reserve(1);
		_nodes[++_size] = Node(t);
		return _nodes.begin() + _size;
	}


	template<typename T, size_t ALLOCATION>
	typename Chain<T, ALLOCATION>::iterator_t Chain<T, ALLOCATION>::select(size_t index) const {
		const size_t firstIndex = 0;
		const size_t lastIndex = Size() - 1;
		const size_t usedIndex = _selected.Index;
		const size_t firstDist = index;
		const size_t lastDist = lastIndex - index;
		const bool usedBigger = index < usedIndex;
		const size_t usedDist = usedBigger ? usedIndex - index : index - usedIndex;

		if (firstDist <= usedDist) {
			_selected.Index = index;
			_selected.Node = (iterator_t(_first->Right) += index);
		}
		else if (lastDist <= usedDist) {
			_selected.Index = index;
			_selected.Node = (iterator_t(_last->Left) -= lastDist);
		}
		else {
			_selected.Index = index;
			if (usedBigger) _selected.Node -= usedDist;
			else _selected.Node += usedDist;
		}
		return _selected.Node;
	}


	template<typename T, size_t ALLOCATION>
	void Chain<T, ALLOCATION>::appendNode(Node * node) {
		node->Left = _last->Left;
		node->Right = _last;
		node->Right->Left = node;
		if (node->Left) {
			node->Left->Right = node;
		}
	}


	template<typename T, size_t ALLOCATION>
	void Chain<T, ALLOCATION>::insertNode(size_t index, Node * node) {
		Node * selected = select(index);
		node->Left = selected->Left;
		node->Right = selected;
		node->Right->Left = node;
		if (node->Left) {
			node->Left->Right = node;
		}
		_selected.Node = node;
	}


	template<typename T, size_t ALLOCATION>
	void Chain<T, ALLOCATION>::removeNode(Node * n) {
		// disconnect
		Node * left = n->Left;
		Node * right = n->Right;
		left->Right = _selected.Node = right;
		right->Left = left;
		// remove data
		if (_nodes.GetIndex(n) != _size) {
			Node * last = _nodes.begin() + _size;
			last->Left->Right = n;
			last->Right->Left = n;
			n->Left = last->Left;
			n->Right = last->Right;
			n->Value = Fwd(last->Value);
		}
		// fix size
		--_size;
	}


	template<typename T, size_t ALLOCATION>
	void Chain<T, ALLOCATION>::removeNodeFast(Node * n) {
		// disconnect
		Node * left = n->Left;
		Node * right = n->Right;
		left->Right = _selected.Node = right;
		right->Left = left;
		// fix size
		--_size;
	}


	template<typename T, size_t ALLOCATION>
	void Chain<T, ALLOCATION>::Remove(const T & remove) {
		for (iterator_t i = begin(), e = end(); e != i; ++i) {
			if (*i == remove) {
				removeNode(i);
				return;
			}
		}
	}


	template<typename T, size_t ALLOCATION>
	void Chain<T, ALLOCATION>::RemoveAt(size_t index) {
		Node * nodeToRemove = select(index);
		// fix cache
		_selected.Node = _selected.Node->Right;
		// remove node
		removeNode(nodeToRemove);
	}


	template<typename T, size_t ALLOCATION>
	void Chain<T, ALLOCATION>::RemoveAtFast(size_t index) {
		Node * nodeToRemove = select(index);
		// fix cache
		++_selected.Node;
		// remove node
		removeNodeFast(nodeToRemove);
	}


	template<typename T, size_t ALLOCATION>
	void Chain<T, ALLOCATION>::Reorder() {
		// temporary storage
		Array<T> temp(Size());
		// move data to temp storage
		T * ptr = temp.begin();
		for (T & item : *this) {
			*(ptr++) = Fwd(item);
		}
		// clear storage
		Clear();
		// move data back
		for (T & item : temp) {
			appendNode(store(Fwd(item)));
		}
	}


	template<typename T, size_t ALLOCATION>
	void Chain<T, ALLOCATION>::Reserve(size_t n) {
		if (Capacity() < n) {
			// number of stored items
			size_t size = Size();
			// requirement for size
			size_t minimalRequirement = size + 2 + n;
			// minimal requirement (start size)
			size_t requirement = 32;
			// binary expansion for proper requirement
			while (requirement < minimalRequirement) {
				requirement <<= 1;
			}
			// create chain
			Chain<T> newChain(requirement);
			// move data from old to new
			for (T & item : newChain) {
				newChain.Add(Fwd(item));
			}
			// move array from new to old
			_nodes = Fwd(newChain._nodes);
			_first = newChain._first;
			_last = newChain._last;
			// reset cache
			_selected.Index = static_cast<size_t>(-1L);
			_selected.Node = _first;
		}
	}


	template<typename T, size_t ALLOCATION>
	void Chain<T, ALLOCATION>::Clear() {
		_size = 1;
		_first->Right = _last;
		_last->Left = _first;
		_selected.Index = static_cast<size_t>(-1L);
		_selected.Node = _first;
	}


	template<typename T, size_t ALLOCATION>
	Chain<T, ALLOCATION>::Chain(size_t preallocation)
		: _nodes()
		, _size(static_cast<size_t>(-1L))
	{
		if ((preallocation + 2) > ALLOCATION) {
			_nodes.Reallocate(preallocation + 2);
		}
		_first = &_nodes[0];
		_last = &_nodes[1];
		_size = 1;
		_first->Right = _last;
		_last->Left = _first;
		_selected.Index = static_cast<size_t>(-1L);
		_selected.Node = _first;
	}
}
