#pragma once
namespace DD {
	/**
	 * 2D Convex hull/polygon.
	 * Allows to check if a 2D point is in or out of polygon.
	 * Contains generators of random points over surface and circumference.
	 * Contains tools for smoothing and filtering 2D convex polygons.
	 * Contains computations of simple convex polygon properties.
	 *
	 * @param T - number type defining points, typically floating point types.
	 * @param ALLOCATION - number of how many points should be preallocated locally.
	 */
	template<typename T, size_t ALLOCATION>
	class Polygon;

	/** 32-bit polygon. */
	template<size_t ALLOCATION = 0>
	using PolygonLocalF = Polygon<float, ALLOCATION>;
	using PolygonF = Polygon<float, 0>;

	/** 64-bit polygon. */
	template<size_t ALLOCATION>
	using PolygonLocalD = Polygon<double, ALLOCATION>;
	using PolygonD = Polygon<double, 0>;
}

#include "Chain.h"
#include "List.h"
#include "Math.h"
#include "Matrices.h"
#include "Primitives.h"
#include "Random.h"
#include "Signum.h"
#include "Vector.h"
#include "Debug.h"

namespace DD {
	template<typename T, size_t ALLOCATION = 0>
	class Polygon {
	private: // statics
		/** Intersection of two lines. */
		static V2<T> intersection(const V2<T> & a, const V2<T> & b, const V2<T> & c, const V2<T> & d);
		/** Get perpendicular vector from given one. */
		static V2<T> cross(const V2<T> & point) { return V2<T>(-point.y, point.x); }
		/** Signum between line and point - which half-plane contains it. */
		template<typename M>
		static Signum signum(const V2<T> & a, const V2<T> & b, const V2<T, M> & point);
		/** Point-segment distance. */
		template<typename M>
		static T distance2(const V2<T, M> & point, const V2<T> & lineA, const V2<T> & lineB);

	public: // statics
		/** Coefficient returning ratio between intersected area and areas of polygons. */
		static T IntersectionCoefficient(const Polygon<T> & cvx0, const Polygon<T> & cvx1);
		/** Intersection of two convex polygons. */
		static void Intersection(const Polygon<T> & cvx0, const Polygon<T> & cvx1, Polygon<T> & cvxOut);
		/** Intersection of two convex polygons. */
		static Polygon<T, ALLOCATION> Intersection(const Polygon<T> & cvx0, const Polygon<T> & cvx1);
		/** Union of two convex polygons. */
		static void Union(const Polygon<T> & cvx0, const Polygon<T> & cvx1, Polygon<T> & cvxOut);
		/** Union of two convex polygons. */
		static Polygon<T, ALLOCATION> Union(const Polygon<T> & cvx0, const Polygon<T> & cvx1);

	public: // properties
		/** Polygon as sequence of points. */
		List<V2<T>, ALLOCATION> Points;

	private: // methods
		/** Returns distance of the furthest polygon point and given point. */
		T maxDistance(const V2<T> &) const;
		/** Returns distance of the nearest polygon point and given point. */
		T minDistance(const V2<T> &) const;

	public: // const methods
		/** Compute circumference value. */
		T Circumference() const;
		/** In check. */
		template<typename M>
		bool IsInClockwise(const V2<T, M> & point) const;
		/** In check. */
		template<typename M>
		bool IsInAnticlockwise(const V2<T, M> & point) const;
		/** In check. Points on the borders are not considered as being in. */
		template<typename M>
		bool IsIn(const V2<T, M> & point) const;
		/** Check if given point is in or on the border of polygon. */
		template<typename M>
		bool IsInExt(const V2<T, M> & point) const;
		/**
		 * Checks if polygon is clockwise.
		 * @warning this method does not guarantee convexity
		 */
		bool IsClockwise() const;
		/**
		 * Compute squared size of the shortest and the longest line of polygon.
		 * @param [out] shortestIndex - { Points[index], Points[index + 1 % m] }
		 *                            - is defining the shortest line of polygon.
		 * @param [out] shortestDist2 - squared size of the shortest line of polygon.
		 * @param [out] longestIndex  - { Points[index], Points[index + 1 % m] }
		 *                            - is defining the longest line of polygon.
		 * @param [out] longestDist2  - squared size of the longest line of polygon.
		 */
		void Extremes(
			size_t & shortestIndex,
			T & shortestDist2,
			size_t & longestIndex,
			T & longestDist2
		) const;

	public: // methods
		/** Include point into convex polygon. */
		template<typename M>
		void Include(const V2<T, M> & point);
		/** Include convex polygon into this convex polygon. */
		void Include(const Polygon<T> & cvx) { for (const auto & pt : cvx.Points) { Include(pt); } }
		/**
		 * Merges points between which is distance smaller than given, new point is avg.
		 * Method could reduce the polygon area.
		 * @see FilterByAngleConservative.
		 * @example merge very near points
		 *   polygon.FilterByLengthRadical(0.01);
		 * @param length - minimal length of polygon in meters.
		 */
		void FilterByLengthRadical(T length);
		/**
		 * Merges points between which is distance smaller than given, new point
		 * is computed as convex extension - method does not reduce the polygon area.
		 * Let us have A-B-C-D polygon and B-C edge to be deleted.
		 *   Conservative filter will define point P replacing points B and C
		 *   as the intersection of lines AB and CD.
		 * @example merge very near points
		 *   polygon.FilterByLengthRadical(0.01);
		 * @param length - minimal length of polygon in meters.
		 */
		void FilterByLengthConservative(T length);
		/**
		 * Removes points B, if angle ABC is bigger than given value.
		 * Method could reduce the polygon area.
		 * Designed for narrowing almost straight angles.
		 * @see FilterByAngleConservative
		 * @example narrow almost straight angles
		 *   polygon.FilterByAngleRadical(Math::Deg2Rad(175.0));
		 * @param angle - angle in radians.
		 */
		void FilterByAngleRadical(T angle);
		/**
		 * Designed for narrowing almost straight angles.
		 * Method does not reduce the polygon area.
		 * @scheme
		 *   B'-------C-------D' Let us have BCD angle to be removed, so we remove
		 *    \              /   point C. We define line B'C' to be parallel to BC
		 *     B------------D    and to be intersecting C. Then we move points
		 *      \          /     B and C to points B' and C'. Those points are
		 *       A        E      intersections of AB and B'C' and of ED and B'C'.
		 * @example narrow almost straight angles
		 *   polygon.FilterByAngleConservative(Math::Deg2Rad(175.0));
		 * @param angle - angle in radians.
		 */
		void FilterByAngleConservative(T angle);
		/**
		 * Filter points.
		 * Firstly remove not significant angles, secondly small edges.
		 * @see FilterByAngleRadical(), FilterByLengthRadical()
		 * @example common usage
		 *   polygon.Filter(Math::Deg2Rad(175.0), 0.01);
		 * @param angle  - angle in radians.
		 * @param length - minimal length of polygon in meters.
		 */
		void FilterRadical(T angle, T length);
		/**
		 * Filter points, method does not reduce the polygon area.
		 * Firstly remove not significant angles, secondly small edges.
		 * @see FilterByAngleConservative(), FilterByLengthConservative()
		 * @example common usage
		 *   polygon.Filter(Math::Deg2Rad(175.0), 0.01);
		 * @param angle  - angle in radians.
		 * @param length - minimal length of polygon in meters.
		 */
		void FilterConservative(T angle, T length);
		/**
		 * Filter points.
		 * @see FilterRadical(angle, length),
		 * @see FilterConservative(angle, length).
		 */
		void Filter(T angle, T length, bool conservative);
		/** Generate points on the circumference of polygon. */
		void GenerateRandomPointsOverCircumference(
			Array<V2<T>> & target,
			Random::Generator & generator = Random::Generator::Instance()
		) const;
		/** Generate points on the surface of polygon. */
		void GenerateRandomPointsOverSurface(
			Array<V2<T>> & target,
			Random::Generator & generator = Random::Generator::Instance()
		) const;

	public: // computable properties
		/** AABB 2D. */
		AABox2<T> AABox() const;
		/** OBox 2D of convex polygon. */
		OBox2<T> OBox() const;
		/** Surface of polygon. */
		T Surface() const;
		/** Center of mass. */
		V2<T> Centroid() const;
		/** Distance of nearest point to centroid. */
		T MinExcetricity() const;
		/** Distance of furthest point to centroid. */
		T MaxExcetricity() const;
		/** Maximal radius of inscribed circle with center in centroid. */
		T InscribedRadius() const;
		/** Convexity check of points. */
		bool IsConvex() const;
		/** Transformation of polygon. */
		void Transform(const M22<T> & transformation, const V2<T> & offset);

	public: // constructors
		/** Conversion operator. */
		operator const ArrayView1D<const V2<T>>() const { return Points; }
		/** Conversion operator. */
		operator const Polygon<T> &() const { return reinterpret_cast<const Polygon<T> &>(*this); }
		/** Conversion operator. */
		operator Polygon<T> &() { return reinterpret_cast<Polygon<T> &>(*this); }
		/** Zero constructor. */
		Polygon() = default;
		/** Copy constructor. */
		Polygon(const Polygon &) = default;
		/** Move constructor. */
		Polygon(Polygon &&) = default;
		/** Assignment operator. */
		Polygon & operator=(const Polygon &) = default;
		/** Item constructor. */
		template<typename...ARGS>
		explicit Polygon(const ARGS &...args) { Points.Add(args...); }
		/** Preallocation constructor. */
		explicit Polygon(size_t i) : Points(i) {}
		/** Copy constructor. */
		template<size_t SIZE>
		explicit Polygon(const Polygon<T, SIZE> & cvx) { Points.AddIterable(cvx.Points); }
	};
}

namespace DD {
	template<typename T, size_t ALLOCATION>
	inline V2<T> Polygon<T, ALLOCATION>::intersection(const V2<T> & a1, const V2<T> & a2, const V2<T> & b1, const V2<T> & b2) {
		const T x12 = (a1.x - a2.x);
		const T x34 = (b1.x - b2.x);
		const T y12 = (a1.y - a2.y);
		const T y34 = (b1.y - b2.y);
		const T l = (a1.x * a2.y - a1.y * a2.x);
		const T r = (b1.x * b2.y - b1.y * b2.x);
		const T d = (x12 * y34) - (y12 * x34);

		V2<T> result;
		result.x = (l * x34) - (x12 * r);
		result.y = (l * y34) - (y12 * r);
		return result /= d;
	}

	template<typename T, size_t ALLOCATION>
	template<typename M>
	inline Signum Polygon<T, ALLOCATION>::signum(const V2<T> & a, const V2<T> & b, const V2<T, M> & point) {
		const V2<T> & c = b - a;
		const V2<T> & d = point - a;
		return DD::signum(c.x * d.y - c.y * d.x, T(1E-5f));
	}

	template<typename T, size_t ALLOCATION>
	template<typename M>
	inline T Polygon<T, ALLOCATION>::distance2(const V2<T, M> & point, const V2<T> & lineA, const V2<T> & lineB) {
		const V2<T> & diff = lineB - lineA;
		const T l2 = dot(diff);  // i.e. |w-v|^2 -  avoid a sqrt
		const T t = Math::Saturate(dot(point - lineA, diff) / l2);
		const V2<T> projection = lineA + diff * t;  // projection falls on the segment
		return dot(point - projection);

		// V2<T> diff = lineB - lineA;
		// return Math::Abs((diff.y * point.x) - (diff.x * point.y) + (lineB.x * lineA.y) - (lineB.y * lineA.x)) / Math::Sqrt(dot(diff));
	}

	template<typename T, size_t ALLOCATION>
	inline T Polygon<T, ALLOCATION>::IntersectionCoefficient(const Polygon<T>& cvx0, const Polygon<T>& cvx1) {
		Polygon<T, 64> inter;
		Intersection(cvx0, cvx1, inter);
		const T S0 = cvx0.Surface();
		const T S1 = cvx1.Surface();
		const T SI = inter.Surface();
		return (T(2) * SI) / (S0 + S1);
	}

	template<typename T, size_t ALLOCATION>
	inline void Polygon<T, ALLOCATION>::Intersection(
		const Polygon<T> & aIn,
		const Polygon<T> & bIn,
		Polygon<T> & cvxOut
	) {
		// vector for sorting of vertices
		V2<T> pseudoCenter = 0;
		// a vertices - put all a-vertices which are inside b
		for (const V2<T> & v : aIn.Points) {
			if (bIn.IsInExt(v)) {
				cvxOut.Points.Add(v);
				pseudoCenter += v;
			}
		}
		// b vertices - put all b-vertices which are inside a
		for (const V2<T> & v : bIn.Points) {
			if (aIn.IsInExt(v)) {
				cvxOut.Points.Add(v);
				pseudoCenter += v;
			}
		}
		// ab vertices - put all intersections of polygons
		for (size_t ai = 0, am = aIn.Points.Size(); ai < am; ++ai) {
			const V2<T> & a1 = aIn.Points[ai];
			const V2<T> & a2 = aIn.Points[(ai + 1) % am];
			for (size_t bi = 0, bm = bIn.Points.Size(); bi < bm; ++bi) {
				const V2<T> & b1 = bIn.Points[bi];
				const V2<T> & b2 = bIn.Points[(bi + 1) % bm];
				// intersect lines
				const V2<T> i = intersection(a1, a2, b1, b2);
				// check intersection boundaries for x axis
				if (Math::Min(a1.x, a2.x) > i.x)
					continue;
				if (Math::Min(b1.x, b2.x) > i.x)
					continue;
				if (Math::Max(a1.x, a2.x) < i.x)
					continue;
				if (Math::Max(b1.x, b2.x) < i.x)
					continue;
				// check intersection boundaries for y axis
				if (Math::Min(a1.y, a2.y) > i.y)
					continue;
				if (Math::Min(b1.y, b2.y) > i.y)
					continue;
				if (Math::Max(a1.y, a2.y) < i.y)
					continue;
				if (Math::Max(b1.y, b2.y) < i.y)
					continue;
				// all checks are valid
				cvxOut.Points.Add(i);
				pseudoCenter += i;
			}
		}
		// compute pseudo-center as average of all preset points
		pseudoCenter /= (T)cvxOut.Points.Size();
		// compute polar coordinate of points
		List<T, 64> clock;
		for (const V2<T> & v : cvxOut.Points) {
			const V2<T> & result = v - pseudoCenter;
			clock.Add(Math::ArcTg2(result.y, result.x));
		}
		// simply sort according to clock
		DD_TODO_STAMP("Change current sort to QSort or merge sort");
		for (size_t i = 0, m = cvxOut.Points.Size(); i < m; ++i) {
			size_t minIndex = i;
			T minValue = Math::Constants<T>::Min;
			// search for minimal value
			for (size_t j = i; j < m; ++j) {
				if (minValue < clock[j]) {
					minValue = clock[j];
					minIndex = j;
				}
			}
			DD::Swap(clock[minIndex], clock[i]);
			DD::Swap(cvxOut.Points[minIndex], cvxOut.Points[i]);
		}
	}

	template<typename T, size_t ALLOCATION>
	inline Polygon<T, ALLOCATION> Polygon<T, ALLOCATION>::Intersection(const Polygon<T> & cvx0, const Polygon<T> & cvx1) {
		Polygon<T, ALLOCATION> result;
		Intersection(cvx0, cvx1, result);
		return result;
	}

	template<typename T, size_t ALLOCATION>
	inline void Polygon<T, ALLOCATION>::Union(
		const Polygon<T> & aIn,
		const Polygon<T> & bIn,
		Polygon<T> & cvxOut
	) {
		// protection for self union
		Polygon<T, 64> backupA;
		Polygon<T, 64> backupB;
		if (&aIn == &cvxOut)
			backupA.Points.AddIterable(aIn.Points);
		if (&bIn == &cvxOut)
			backupB.Points.AddIterable(bIn.Points);
		const Polygon<T> & a = (&aIn == &cvxOut) ? backupA : aIn;
		const Polygon<T> & b = (&bIn == &cvxOut) ? backupB : bIn;
		// union bigger with smaller
		if (aIn.Points.Size() < bIn.Points.Size()) {
			cvxOut.Points.AddIterable(b.Points);
			cvxOut.Include(a);
		}
		else {
			cvxOut.Points.AddIterable(a.Points);
			cvxOut.Include(b);
		}
	}

	template<typename T, size_t ALLOCATION>
	inline Polygon<T, ALLOCATION> Polygon<T, ALLOCATION>::Union(const Polygon<T> & cvx0, const Polygon<T> & cvx1) {
		Polygon<T, ALLOCATION> result;
		Union(cvx0, cvx1, result);
		return result;
	}

	template<typename T, size_t ALLOCATION>
	inline T Polygon<T, ALLOCATION>::maxDistance(const V2<T> & p) const {
		T result = 0;
		for (size_t i = 0, l = Points.Size(); i < l; ++i) {
			T distance2 = dot(p - Points[i]);
			if (result < distance2)
				result = distance2;
		}
		return sqrt(result);
	}

	template<typename T, size_t ALLOCATION>
	inline T Polygon<T, ALLOCATION>::minDistance(const V2<T> & p) const {
		T result = 1e10;
		for (size_t i = 0, l = Points.Size(); i < l; ++i) {
			T distance2 = dot(p - Points[i]);
			if (result > distance2)
				result = distance2;
		}
		return sqrt(result);
	}

	template<typename T, size_t ALLOCATION>
	inline T Polygon<T, ALLOCATION>::Circumference() const {
		T result = T(0);
		for (size_t i = 0, m = Points.Size(); i < m; ++i)
			result += Math::Norm((Points[i] - Points[(i + 1) % m]));

		return result;
	}

	template<typename T, size_t ALLOCATION>
	inline AABox2<T> Polygon<T, ALLOCATION>::AABox() const {
		AABox2<T> result;
		for (const V2<T> & pt : Points)
			result.Include(pt);

		return result;
	}

	template<typename T, size_t ALLOCATION>
	inline OBox2<T> Polygon<T, ALLOCATION>::OBox() const {
		// resulting box
		OBox2<T> box;
		// surface
		T minimalSurface = Math::Constants<T>::Max;

		// for each edge check size of collinear aabox
		for (size_t i = 0, m = Points.Size(); i < m; ++i) {
			// points of the edge
			const V2<T> & a = Points[i];
			const V2<T> & b = Points[(i + 1) % m];
			// create rotation
			const V2<T> & ndir = Math::Normalize(b - a);
			//  Y  +B          Angle XAB is defined by point
			//  | /            on unit circle (normalized) vector.
			//  |/
			// A+--------X
			const M22<T> & rotation = Matrices::Rotation(ndir);
			// compute aabox
			AABox2<T> aabb;

			for (size_t j = 0; j < m; ++j) {
				// to align side of polygon with X-Axis, rotate clock-wise
				const V2<T> & v = Points[j] * rotation;
				aabb.Include(v);
			}
			// check if it is minimal
			const V2<T> & size = aabb.Size();
			T surface = size.x * size.y;

			if (surface < minimalSurface) {
				minimalSurface = surface;
				box.Radius = size * T(0.5);
				// to get center of obox, move center of aabox anti-clock-wise
				box.Position = rotation * aabb.Center();
				box.Rotation = ndir;
			}
		}

		return box;
	}

	template<typename T, size_t ALLOCATION>
	inline T Polygon<T, ALLOCATION>::Surface() const {
		T surface = 0;
		size_t l = Points.Size();
		// Gauss's area formula
		for (size_t i = 0; i < l; ++i) {
			const V2<T> & x = Points[i];
			const V2<T> & y = Points[(i + 1) % l];
			surface += x.x * y.y - (x.y * y.x);
		}

		return Math::Abs(surface) / 2;
	}

	template<typename T, size_t ALLOCATION>
	inline V2<T> Polygon<T, ALLOCATION>::Centroid() const {
		V2<T> centroid = 0;
		size_t l = Points.Size();
		T surface = 0;

		// https://en.wikipedia.org/wiki/Centroid#Of_a_polygon
		for (size_t i = 0; i < l; ++i) {
			const V2<T> & x = Points[i];
			const V2<T> & y = Points[(i + 1) % l];
			T r = x.x * y.y - (x.y * y.x);
			surface += r;
			centroid.x += r * (x.x + y.x);
			centroid.y += r * (x.y + y.y);
		}

		if (surface != 0)
			centroid /= surface * 3;

		return centroid;
	}

	template<typename T, size_t ALLOCATION>
	inline T Polygon<T, ALLOCATION>::MinExcetricity() const {
		return minDistance(Centroid());
	}

	template<typename T, size_t ALLOCATION>
	inline T Polygon<T, ALLOCATION>::MaxExcetricity() const {
		return maxDistance(Centroid());
	}

	template<typename T, size_t ALLOCATION>
	inline T Polygon<T, ALLOCATION>::InscribedRadius() const {
		V2<T> x0 = Centroid();
		T radius = 1E10;
		for (size_t i = 0, l = Points.Size(); i < l; ++i) {
			const V2<T> & x1 = Points[i];
			const V2<T> & x2 = Points[(i + 1) % l];
			V2<T> x3 = x2 - x1;
			// compute square distance
			T distance2 = Math::Abs(x3.y * x0.x - x3.x * x0.y + x2.x * x1.y - x2.y * x1.x) / Math::Norm(x3);
			if (radius > distance2) {
				radius = distance2;
			}
		}
		return radius;
	}

	template<typename T, size_t ALLOCATION>
	inline bool Polygon<T, ALLOCATION>::IsConvex() const {
		if (Points.Size() < 3)
			return false;

		Signum sgn = sgn = Signum::NEUTRAL;
		switch (IsClockwise()) {
			case true:  sgn = Signum::POSITIVE;	break;
			case false:	sgn = Signum::NEGATIVE;	break;
		}
		DD_DEBUG_ASSERT(sgn != Signum::NEUTRAL);

		for (size_t i = 0, m = Points.Size() + 2; i < m; ++i) {
			const V2<T> & ai = Points[i];
			const V2<T> & bi = Points[(i + 1) % m];
			const V2<T> & ci = Points[(i + 2) % m];
			Signum cmp = signum(ai, bi, ci);
			if (sgn != cmp && cmp != Signum::NEUTRAL) {
				return false;
			}
		}
		return true;
	}

	template<typename T, size_t ALLOCATION>
	template<typename M>
	inline void Polygon<T, ALLOCATION>::Include(const V2<T, M> & point) {
		// check trivial cases
		switch (Points.Size()) {
			// point is automatically unique
		case 0:
			Points.Add(point);
			return;
		case 1: // point can be identical to

			if (Points[0] != point)
				Points.Add(point);
			return;
		case 2: // point can be on line
			if (Math::Abs(distance2(point, Points[0], Points[1])) < T(0.00001)) {
				const V2<T> dir = Points[1] - Points[0];
				const T t = dir.x == T(0) ? (point.y - Points[0].y) / dir.y : (point.x - Points[0].x) / dir.x;
				if (t < T(0))
					Points[0] = point;
				else if (t > T(1)) {
					Points[1] = point;
				}
			}
			else {
				Points.Add(point);
				DD_DEBUG_ASSERT(IsConvex(), "Polygon should be convex after this operation.", SILENT);
			}
			return;
		}

		// check if the point is out of the polygon
		if (IsInExt(point)) {
			return;
		}

		// select nearest point-segment distance
		size_t closeIndex = 0;
		T closeValue = Math::Constants<T>::Max;
		for (size_t i = 0, m = Points.Size(); i < m; ++i) {
			const T dist2 = distance2(point, Points[i], Points[(i + 1) % m]);
			if (dist2 < closeValue) {
				closeValue = dist2;
				closeIndex = i;
			}
		}

		// check if polygon is clockwise or anticlockwise
		bool clockwise = IsClockwise();
		// predefined constants
		size_t size = Points.Size();

		size_t upperIndex, lowerIndex;
		lowerIndex = size + closeIndex + 2;
		upperIndex = size + closeIndex;
		// check for while loops
		bool convexFailed;

		// check lower bound of indices to be removed
		do {
			--lowerIndex;
			const size_t indexA = (lowerIndex - 2 + size) % size;
			const size_t indexB = (lowerIndex - 1 + size) % size;
			// A---B---M---C---D
			const V2<T> & AB = Points[indexB] - Points[indexA];
			const V2<T> & BM = cross(point - Points[indexB]);
			// propagate value to condition check
			convexFailed = clockwise == (cos(AB, BM) >= T(0));
		} while (convexFailed);

		// check the upper bound of indices to be removed
		do {
			++upperIndex;
			const size_t indexC = (upperIndex) % size;
			const size_t indexD = (upperIndex + 1) % size;
			// A---B---M---C---D
			const V2<T> & MC = Points[indexC] - point;
			const V2<T> & CD = cross(Points[indexD] - Points[indexC]);
			// propagate value to condition check
			convexFailed = clockwise == (cos(MC, CD) >= T(0));
		} while (convexFailed);

		// remove indices from obtained bounds
		size_t removeSize = upperIndex - lowerIndex;
		// align indices
		lowerIndex = (lowerIndex + size) % size;
		upperIndex = (upperIndex) % size;
		switch (removeSize) {
		case 0: // nothing to delete
			Points.Insert(lowerIndex) = point;
			break;
		case 1: // no delete, just replace
			Points[lowerIndex] = point;
			break;
		default: // delete and replace
			if (lowerIndex < upperIndex) { // upper index is bigger
				Points.RemoveAt(lowerIndex, removeSize - 1);
				Points[lowerIndex] = point;
			}
			else { // lower index is bigger
				Points.RemoveAt(lowerIndex, size - lowerIndex);
				Points.RemoveAt(0, (lowerIndex + removeSize) % size);
				Points.Add(point);
			}
			break;
		}

		DD_DEBUG_ASSERT(IsConvex(), "Polygon should be convex after this operation.", SILENT);
	}

	template<typename T, size_t ALLOCATION>
	inline bool Polygon<T, ALLOCATION>::IsClockwise() const {
		// check polygon orientation
		for (size_t i = 2, l = Points.Size(); i < l; ++i) {
			const V2<T> & a(Points[i]);
			const V2<T> & b(Points[(i + 1) % l]);
			const V2<T> & c(Points[(i + 2) % l]);
			// get angle signum by cos of vectors
			Signum sgn = signum(a, b, c);
			// if line is straight, check another angle
			if (sgn == Signum::NEUTRAL)
				continue;

			return sgn == Signum::POSITIVE;
		}

		// formal value
		return false;
	}

	template<typename T, size_t ALLOCATION>
	inline void Polygon<T, ALLOCATION>::Extremes(
		size_t & shortestIndex,
		T & shortestDist2,
		size_t & longestIndex,
		T & longestDist2
	) const {
		shortestDist2 = Math::Constants<T>::Max;
		longestDist2 = Math::Constants<T>::Zero;
		for (size_t index = 0, m = Points.Size(); index < m; ++index) {
			// compute square distance of line
			T dist2 = dot(Points[index] - Points[(index + 1) % m]);
			// check validity of shortest
			if (dist2 < shortestDist2) {
				shortestDist2 = dist2;
				shortestIndex = index;
			}
			// check validity of longest
			if (longestDist2 < dist2) {
				longestDist2 = dist2;
				longestIndex = index;
			}
		}
	}

	template<typename T, size_t ALLOCATION>
	inline void Polygon<T, ALLOCATION>::FilterByLengthRadical(T minLength) {
		Chain<V2<T>> chain(Points.Size());
		// copy to chain for cheaper slicing
		for (V2<T> & pt : Points)
			chain.Add(pt);
		// set square length for cheaper comparison
		T length2 = minLength * minLength;
		// remove small
		for (size_t i = 0, m = chain.Size(); i < m; ++i, m = chain.Size()) {
			V2<T> & a = chain[i];
			V2<T> & b = chain[(i + 1) % m];
			if (dot(a - b) < length2 && m > 3) {
				b = (a + b) * T(0.5);
				chain.RemoveAtFast(i);
				--i;
			}
		}
		// copy back
		for (size_t i = 0, m = chain.Size(); i < m; ++i)
			Points[i] = chain[i];
		// fix size
		Points.Resize(chain.Size());
	}

	template<typename T, size_t ALLOCATION>
	inline void Polygon<T, ALLOCATION>::FilterByLengthConservative(T minLength) {
		Chain<V2<T>> chain(Points.Size());
		// copy to chain for cheaper slicing
		for (V2<T> & pt : Points)
			chain.Add(pt);

		// set square length for cheaper comparison
		T length2 = minLength * minLength;
		// remove small
		for (size_t i = 0, m = chain.Size(); i < m; ++i, m = chain.Size()) {
			V2<T> & a = chain[i];
			V2<T> & b = chain[(i + 1) % m];
			V2<T> & c = chain[(i + 2) % m];
			V2<T> & d = chain[(i + 3) % m];

			if (dot(b - c) < length2 && m > 3) {
				b = intersection(a, b, c, d);
				chain.RemoveAtFast((i + 2) % m);
				--i;
			}
		}
		// copy back
		for (size_t i = 0, m = chain.Size(); i < m; ++i)
			Points[i] = chain[i];
		// fix size
		Points.Resize(chain.Size());
	}

	template<typename T, size_t ALLOCATION>
	inline void Polygon<T, ALLOCATION>::FilterByAngleRadical(T minAngle) {
		// copy to chain for cheaper slicing
		Chain<V2<T>> chain(Points.Size());
		for (V2<T> & pt : Points)
			chain.Add(pt);
		T cosAngle = ::cos(minAngle);

		for (size_t ia = 0, m = chain.Size(); ia < m; ++ia, m = chain.Size()) {
			// compute indices
			size_t ib = (ia + 1) % m;
			size_t ic = (ia + 2) % m;
			// assign values
			V2<T> & a = chain[ia];
			V2<T> & b = chain[ib];
			V2<T> & c = chain[ic];
			// check angle size, remove
			if (cos(b - a, b - c) < cosAngle && m > 3) {
				chain.RemoveAtFast(ib);
				--ia;
			}
		}

		size_t newSize = chain.Size();
		// if anything filtered
		if (Points.Size() != newSize) {
			// copy back
			for (size_t i = 0; i < newSize; ++i)
				Points[i] = chain[i];
			// fix size
			Points.Resize(newSize);
		}
	}

	template<typename T, size_t ALLOCATION>
	inline void Polygon<T, ALLOCATION>::FilterByAngleConservative(T minAngle) {
		// copy to chain for cheaper slicing
		Chain<V2<T>> chain(Points.Size());
		for (V2<T> & pt : Points)
			chain.Add(pt);
		T cosAngle = ::cos(minAngle);

		for (size_t i = 0, m = chain.Size(); i < m; ++i, m = chain.Size()) {
			// assign values
			V2<T> & a = chain[i];
			V2<T> & b = chain[(i + 1) % m];
			V2<T> & c = chain[(i + 2) % m];
			V2<T> & d = chain[(i + 3) % m];
			V2<T> & e = chain[(i + 4) % m];
			// check angle size, remove
			if (cos(c - b, c - d) < cosAngle && m > 3) {
				const V2<T> & dir = b - d;
				const V2<T> & c2 = c + dir;
				const V2<T> & b2 = intersection(a, b, c, c2);
				const V2<T> & d2 = intersection(c, c2, e, d);
				b = b2;
				d = d2;
				chain.RemoveAtFast((i + 2) % m);
				--i;
			}
		}

		size_t newSize = chain.Size();
		// if anything filtered
		if (Points.Size() != newSize) {
			// copy back
			for (size_t i = 0; i < newSize; ++i)
				Points[i] = chain[i];
			// fix size
			Points.Resize(newSize);
		}
	}

	template<typename T, size_t ALLOCATION>
	inline void Polygon<T, ALLOCATION>::FilterRadical(T maxAngle, T maxLength) {
		FilterByAngleRadical(maxAngle);
		FilterByLengthRadical(maxLength);
	}

	template<typename T, size_t ALLOCATION>
	inline void Polygon<T, ALLOCATION>::FilterConservative(T angle, T length) {
		FilterByAngleConservative(angle);
		FilterByLengthConservative(length);
	}

	template<typename T, size_t ALLOCATION>
	inline void Polygon<T, ALLOCATION>::Filter(T angle, T length, bool conservative) {
		if (conservative)
			FilterConservative(angle, length);
		else
			FilterRadical(angle, length);
	}

	template<typename T, size_t ALLOCATION>
	inline void Polygon<T, ALLOCATION>::GenerateRandomPointsOverCircumference(
		Array<V2<T>> & target, Random::Generator & generator
	) const {
		Array<T> samples(target.Lengths());
		//generator.GenerateSortedArray(samples, T(0), Circumference());
		DD_TODO_STAMP("Need to reimplemented sorted array method.");
		// convert range into points on circumference
		T acc0 = 0.f, acc1 = 0.f;

		for (size_t i = 0, j = 0, m = Points.Size(), n = target.Length(); i < m && j < n; ++i) {
			// 2-d bounds
			V2<T> bound0 = Points[i];
			V2<T> bound1 = Points[(i + 1) % m];
			V2<T> direction = bound1 - bound0;
			// 1-d bounds
			T range = Math::Norm(direction);
			acc0 = acc1;
			acc1 += range;
			// convert samples into 2d points
			for (; j < n && samples[j] <= acc1; ++j) {
				T normalizedSample = (samples[j] - acc0) / range;
				target[j] = bound0 + (direction * normalizedSample);
			}
		}
	}

	template<typename T, size_t ALLOCATION>
	inline void Polygon<T, ALLOCATION>::GenerateRandomPointsOverSurface(
		Array<V2<T>> & target, Random::Generator & generator
	) const {
		// get area where to generate numbers
		AABox2<T> aabb = AABox();
		V2<T> range = aabb.Size();
		// generate initial data (over box)
		target.FillSequence(
			Sequencer::Parallelization2<Random::Sequencer<T>, Random::Sequencer<T>>(
				Random::Sequencer<T>(target.Length(), aabb.Min.x, aabb.Max.x),
				Random::Sequencer<T>(target.Length(), aabb.Min.y, aabb.Max.y)
			)
		);

		for (size_t i = 0; i < target.Length(); ++i) {
			// check if the initial data are in polygon - if not generate it again
			while (!IsIn(target[i])) {
				target[i].x = generator.Generate<T>();
				target[i].y = generator.Generate<T>();
				target[i] = (target[i] * range) + aabb.Min;
			}
		}
	}

	template<typename T, size_t ALLOCATION>
	template<typename M>
	inline bool Polygon<T, ALLOCATION>::IsInClockwise(const V2<T, M> & point) const {
		for (size_t i = 0, size = Points.Size(); i < size; ++i)
			if (signum(Points[i], Points[(i + 1) % size], point) != Signum::NEGATIVE)
				return false;

		return true;
	}

	template<typename T, size_t ALLOCATION>
	template<typename M>
	inline bool Polygon<T, ALLOCATION>::IsInAnticlockwise(const V2<T, M> & point) const {
		for (size_t i = 0, size = Points.Size(); i < size; ++i)
			if (signum(Points[i], Points[(i + 1) % size], point) != Signum::POSITIVE)
				return false;

		return true;
	}

	template<typename T, size_t ALLOCATION>
	template<typename M>
	inline bool Polygon<T, ALLOCATION>::IsIn(const V2<T, M> & point) const {
		size_t size = Points.Size() - 1;

		Signum check = signum(Points[size], Points[0], point);
		if (check == Signum::NEUTRAL)
			return false;

		for (size_t i = 0; i < size; ++i) {
			if (signum(Points[i], Points[i + 1], point) != check) {
				return false;
			}
		}
		return true;
	}

	template<typename T, size_t ALLOCATION>
	template<typename M>
	inline bool Polygon<T, ALLOCATION>::IsInExt(const V2<T, M>& point) const {
		size_t size = Points.Size() - 1;
		Signum check = signum(Points[size], Points[0], point);
		for (size_t i = 0; i < size; ++i) {
			switch (signum(Points[i], Points[i + 1], point)) {
			case Signum::NEGATIVE:
				if (check == Signum::POSITIVE)
					return false;
				if (check == Signum::NEUTRAL)
					check = Signum::NEGATIVE;
				break;
			case Signum::POSITIVE:
				if (check == Signum::NEGATIVE)
					return false;
				if (check == Signum::NEUTRAL)
					check = Signum::POSITIVE;
				break;
			case Signum::NEUTRAL:
				break;
			}
		}

		return true;
	}

	template<typename T, size_t ALLOCATION>
	inline void Polygon<T, ALLOCATION>::Transform(const M22<T> & transformation, const V2<T> & position) {
		for (V2<T> & point : Points) {
			point = (transformation * point + position);
		}
	}
}