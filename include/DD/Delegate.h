#pragma once
namespace DD {
	/**
	 * Helper class for creating delegate instance.
	 */
	template<typename...ARGS>
	class DelegateBase;

	/**
	 * Delegate, similar to c#.
	 * @example
	 *	int number = 0;
	 *	Event handler;
	 *	handler += Delegate([&](){ ++number; });
	 *	handler();
	 */
	template<typename...ARGS>
	class Delegate;

	/**
	 * Delegate, where zero constructor creates action free delegate.
	 */
	template<typename...ARGS>
	struct Event;
}

#include <DD/Concurrency/AtomicLock.h>
#include <DD/Lambda.h>
#include <DD/List.h>
#include <DD/Reference.h>

namespace DD {
	template<typename...ARGS>
	class DelegateBase
		: public IReferential
		, public IAction<ARGS...>
	{
		friend class Delegate<ARGS...>;

	private: // properties
		/** Attached delegates to the delegate. */
		List<Reference<DelegateBase<ARGS...>>, 8> _children;
		/** Function attached to the delegate. */
		Action<ARGS...> _functor;
		/** MT sync primitive. */
		Concurrency::AtomicLock _lock;

	protected: // IAction
		/** Call the attached function and function of attached delegates. */
		virtual void iLambdaRun(ARGS...args) override;

	public: // methods
		/** Attach delegate. */
		void Add(const Delegate<ARGS...> & delegate);
		/** Detach delegate. */
		bool Remove(const Delegate<ARGS...> & delegate);
		/** Check if another delegate is attached to this one. */
		bool Contains(const Delegate<ARGS...> & delegate) const;
		/** Clear the register of attached delegates. */
		void Free();
		/** Has no children. */
		bool IsEmpty() const;

	private: // constructor
		/** Constructor. */
		DelegateBase(const Action<ARGS...> & functor) : _functor(functor) {};

	public: // constructors
		/** Destructor. */
		 ~DelegateBase() = default;
	};


	template<typename...ARGS>
	class Delegate
		: public Reference<DelegateBase<ARGS...>>
	{
		friend class DelegateBase<ARGS...>;
		using Base = Reference<DelegateBase<ARGS...>>;

	public: // operators
		/** Equality operator. */
		bool operator==(const Delegate & d) { return Reference::operator==(d); }
		/** Non-equality operator. */
		bool operator!=(const Delegate & d) { return Reference::operator!=(d); }
		/** Attach delegate. */
		void operator+=(const Delegate & d) { Base::Ptr()->Add(d); }
		/** Detach delegate. */
		bool operator-=(const Delegate & d) { return Base::Ptr()->Remove(d); }
		/** Operator for function call. */
		void operator()(ARGS...args) { (Base::Ptr())->Run(args...); }

	public: // constructors
		/** Zero constructor - nullptr reference. */
		Delegate() : Base() {}
		/** DD::Lambda constructor. */
		Delegate(const Action<ARGS...> & functor) : Base(new DelegateBase<ARGS...>(functor)) {}
		/** Object constructor. */
		template<typename LAMBDA>
		Delegate(const LAMBDA & lambda) : Delegate(Action<ARGS...>(lambda)) {}
	};


	template<typename...ARGS>
	struct Event : public Delegate<ARGS...> {
		/** Zero constructor. */
		Event() : Delegate<ARGS...>(Action<ARGS...>()) {}
	};
}


namespace DD {
	template<typename ...ARGS>
	void DelegateBase<ARGS...>::iLambdaRun(ARGS...args) {
		List<Reference<DelegateBase<ARGS...>>, 16> copy;
		{
			// MT access.
			Concurrency::AtomicLock::Scope scope(_lock);
			// copy for access which is not changed while call
			copy = _children;
		}

		// Run method of delegate, if exists.
		if (_functor)
			_functor(args...);

		for (Reference<DelegateBase<ARGS...>> & child : copy)
			child->Run(args...);
	}

	/** Attach delegate. */
	template<typename...ARGS>
	void DelegateBase<ARGS...>::Add(const Delegate<ARGS...> & delegate) {
		// MT access.
		Concurrency::AtomicLock::Scope scope(_lock);
		//if (delegate == nullptr)
		//	throw;
		// process addition
		_children.Add(delegate);
	}

	/** Detach delegate. */
	template<typename ...ARGS>
	bool DelegateBase<ARGS...>::Remove(const Delegate<ARGS...> & delegate) {
		// MT access.
		Concurrency::AtomicLock::Scope scope(_lock);
		// get result
		return _children.TryRemove(delegate);
	}

	/** Check if another delegate is attached to this one. */
	template<typename...ARGS>
	bool DelegateBase<ARGS...>::Contains(const Delegate<ARGS...> & delegate) const {
		// MT access.
		Concurrency::AtomicLock::Scope scope(_lock);
		// get result
		return _children.Contains(delegate);
	}

	/** Clear the register of attached delegates. */
	template<typename...ARGS>
	void DelegateBase<ARGS...>::Free() {
		Concurrency::AtomicLock::Scope scope(_lock);
		_children.Free();
	}

	template<typename...ARGS>
	bool DelegateBase<ARGS...>::IsEmpty() const {
		Concurrency::AtomicLock::Scope scope(_lock);
		return _children.IsEmpty();
	}
}
