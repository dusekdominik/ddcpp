#pragma once
namespace DD {
	/**
	 * Public interface of iterator.
	 * Provides methods for C++11 for loop.
	 * Methods are sourced from IIterator interface.
	 */
	template<typename T>
	struct Iterator;

	/**
	 * Internal interface virtually providing iterator methods.
	 */
	template<typename T>
	struct IIterator;

	/**
	 * Default factory for creation of IIterator interfaces.
	 * Wraps operators ++, ==, and *.
	 */
	template<typename T, typename ITERATOR = T*>
	struct IIteratorWrapper;
}

namespace DD {
	template<typename T>
	struct IIterator {
		friend struct Iterator<T>;
	public: // nested
		struct Data192 { char _[24]; };
	protected: // properties
		/** Internal storage for iterator data. */
		mutable Data192 _iiterator;
	protected: // virtual interface
		/** Getter of iterator value.  */
		virtual T & iIteratorDereference() = 0;
		/** Move iterator to next value. */
		virtual void iIteratorIncrease() = 0;
		/** Is iterator equal to another. */
		virtual bool iIteratorEquals(const IIterator &) const = 0;
	};

	template<typename T, typename ITERATOR>
	struct IIteratorWrapper : public IIterator<T> {
	private: // methods
		/** Cast data to iterator. */
		ITERATOR & GetIterator() const { return reinterpret_cast<ITERATOR &>(_iiterator); }
	protected: // IIterator
		virtual T & iIteratorDereference() override { return *GetIterator(); }
		virtual void iIteratorIncrease() override { ++GetIterator(); }
		virtual bool iIteratorEquals(const IIterator & i) const override { return static_cast<const IIteratorWrapper &>(i).GetIterator() == GetIterator(); }
	public: // constructors
		/** Constructor from original iterator. */
		IIteratorWrapper(ITERATOR data) { GetIterator() = data; }
	};

	template<typename T>
	struct Iterator {
		/** Aligned data. */
		__declspec(align(16)) struct Data256 { int _[8]; };
	protected: // properties
		/** Iterator data. */
		mutable Data256 _iterator;
	private: // methods
		/** Cast data to IIterator value. */
		IIterator<T> * GetIterator() const { return reinterpret_cast<IIterator<T> *>(&_iterator); }
	public: // public interface
		/** Dereference. */
		T & operator*() { return  GetIterator()->iIteratorDereference(); }
		/** Dereference. */
		T & operator->() { return GetIterator()->iIteratorDereference(); }
		/** Move to next value. */
		Iterator & operator++() { GetIterator()->iIteratorIncrease(); return *this; }
		/** Equality operator. */
		bool operator==(const Iterator & i) const { return GetIterator()->iIteratorEquals(i); }
		/** Non-equality operator. */
		bool operator!=(const Iterator & i) const { return !(GetIterator()->iIteratorEquals(*i.GetIterator())); }
	public: // constructors
		/** Create iterator from arbitrary IIterator interface. */
		Iterator(IIterator<T> & iterator) : _iterator(reinterpret_cast<Data256 &>(iterator)) {}
	};
}
