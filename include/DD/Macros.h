#pragma once
/************************************************************************/
/* Settings                                                             */
/************************************************************************/

#define DD_DBG_STAMPS_ENABLED  0
#define DD_TODO_STAMPS_ENABLED 0


/************************************************************************/
/* Public Macros                                                        */
/************************************************************************/

/**
 * Concatenate given args.
 * @example
 *   DD_CAT(String, 2, Number) ~ String2Number
 */
#define DD_CAT(...) DD_PLAY(__DD_OVERLOAD, DD_CAT, __VA_ARGS__)
/**
 * Get number of arguments.
 * @warning zero-argument count is considered as error
 * @example DD_COUNT(Hello)        ~ 1
 * @example DD_COUNT(Hello, World) ~ 2
 */
#define DD_COUNT(...) DD_PLAY(DD_OVERLOAD_63, __VA_ARGS__, 63, 62, 61, 60, 59, 58, 57, 56, 55, 54, 53, 52, 51, 50, 49, 48, 47, 46, 45, 44, 43, 42, 41, 40, 39, 38, 37, 36, 35, 34, 33, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1)
/** Empty argument, servers syntactic trick for parsing empty argument. */
#define DD_EMPTY
/** Serves for syntactic trick with unwrapping __VA_ARGS__, @see DD_UNWRAP. */
#define DD_EXPAND(...) __VA_ARGS__
/**
 * Get n-th arg, counted from 0.
 *
 * @see DD_TAIL(...)
 * @example DD_HEAD(0, a1, a2, a3) ~ a1
 * @example DD_HEAD(1, a1, a2, a3) ~ a2
 * @example DD_HEAD(2, a1, a2, a3) ~ a3
 */
#define DD_HEAD(nth, ...) DD_PLAY(DD_HEAD_##nth, __VA_ARGS__)
/** Macro for counting arguments in DD_XLIST macros. */
#define DD_INC(...) + 1
/** Empty macro serves for generating no-output.  */
#define DD_NOTHING(...)
/**
 * First order overloader
 * DD_OVERLOAD(MACRO_PREFIX, [args]...)
 *
 * @warning overloading cannot be used recursively.
 * @scheme
 *   switch(numOfArgs) {
 *      case  1: MACRO_PREFIX_1(args...); break;
 *      ...
 *      case 63: MACRO_PREFIX_63(args...); break;
 *   }
 * @example
 *   #define MY_MACRO(...) DD_OVERLOAD(MY_MACRO, __VA_ARGS__) // declare macro overload
 *   #define MY_MACRO_1(name)                                 // process somehow data
 *   #define MY_MACRO_2(name, surname)                        // process somehow data
 *   #define MY_MACRO_3(name, surname, degree)                // process somehow data
 *   MY_MACRO(Gregory, House, MD)                             // call MY_MACRO_3
 *   MY_MACRO(Emilie, Chatelet, Marquise)                     // call MY_MACRO_3
 *   MY_MACRO(William, Shakespeare)                           // call MY_MACRO_2
 *   MY_MACRO(Xenophon)                                       // call MY_MACRO_1
 */
#define DD_OVERLOAD(x, ...) DD_PLAY(DD_PLAY(DD_CAT_3, x, _, DD_COUNT(__VA_ARGS__)), __VA_ARGS__)
/**
 * Pack the alignment of non 32-bit aligned structs.
 * @example 48-bit struct
 *   DD_PACK(struct MyStruct48 { u8 _bytes[6]; });
 */
#define DD_PACK(...) __pragma(pack(push, 1)) __VA_ARGS__ __pragma(pack(pop))
/** Run macro [process] with arguments in __VA_ARGS__. */
#define DD_PLAY(process, ...) DD_EXPAND(process(__VA_ARGS__))
/**
 * Concatenate given args and stringifies them into 8-bit string.
 * @example
 *   DD_SCAT(Hello, World) ~ "HelloWorld"
 */
#define DD_SCAT(...) DD_EXPAND(__DD_OVERLOAD(DD_SCAT, __VA_ARGS__))
/**
 * Concatenate given args and stringifies them into 8-bit string.
 * @example
 *   DD_SCAT(Hello, World) ~ L"HelloWorld"
 */
#define DD_SCAT_16(...) DD_PLAY(DD_CAT_2, L, DD_SCAT(__VA_ARGS__))
/**
 * Stamp prints a message into output in compile time.
 * @example
 *   void invalidPieceOfCode() {
 *     DD_STAMP("This piece of code needs a revision.")
 *   }
 */
#define DD_STAMP(msg) DD_EXPAND(__pragma(message(__FILE__ "(" DD_SCAT_1(__LINE__) "): " msg)));
/**
 * Make from argument 8-bit c-like string.
 * @see DD_SCAT, DD_SCAT_16
 */
#define DD_STRINGIFY(x) #x
/**
 * Get all args after n-th arg.
 * Args are counted from 0.
 * @see DD_HEAD(...)
 * @example DD_TAIL(0, a0, a1, a2) ~ a1, a2
 * @example DD_TAIL(1, a0, a1, a2) ~ a2
 * @example DD_TAIL(2, a0, a1, a2) ~
 */
#define DD_TAIL(nth, ...) DD_PLAY(DD_TAIL_##nth, __VA_ARGS__)
/**
 * Unwrap n-th arg from wrapped sequence.
 *  DD_UNWRAP([n-th], wrap)
 *  DD_UNWRAP(wrap) Unwraps sequence wrapped into the brackets.
 *  DD_UNWRAP(n-th, wrap) Unwraps sequence and return n-th argument of the sequence.
 *
 * @warning bracket expression must be containing at least 2 args.
 *
 * @example DD_UNWRAP(a1) ~ a1                   // not wrapped arguments are naturally not unwrapped
 * @example DD_UNWRAP((a1, a2, a3)) ~ a1, a2, a3 // wrapped sequence is unwrapped
 * @example DD_UNWRAP(0, (a1, a2, a3)) ~ a1      // select n-th arg, counted from 0
 * @example DD_UNWRAP(1, (a1, a2, a3)) ~ a2      // select n-th arg, counted from 0
 * @example DD_UNWRAP(2, (a1, a2, a3)) ~ a3      // select n-th arg, counted from 0
 */
#define DD_UNWRAP(...) __DD_OVERLOAD(DD_UNWRAP, __VA_ARGS__)
/**
 * Unwrap n-th arg from wrapped sequence.
 * @see DD_HEAD(), DD_UNWRAP_TAIL(), DD_TAIL()
 * @example DD_UNWRAP_HEAD(0, (a1, a2, a3)) ~ a1      // select n-th arg, counted from 0
 * @example DD_UNWRAP_HEAD(1, (a1, a2, a3)) ~ a2      // select n-th arg, counted from 0
 * @example DD_UNWRAP_HEAD(2, (a1, a2, a3)) ~ a3      // select n-th arg, counted from 0
 */
#define DD_UNWRAP_HEAD(nth, wrap) DD_HEAD(nth, _DD_UNWRAP(wrap))
/**
 * Unwrap n-th arg from wrapped sequence.
 * @see DD_HEAD(), DD_UNWRAP_HEAD(), DD_TAIL()
 * @example DD_UNWRAP_TAIL(0, (a1, a2, a3)) ~ a2, a3  // select n-th arg, counted from 0
 * @example DD_UNWRAP_TAIL(1, (a1, a2, a3)) ~ a3      // select n-th arg, counted from 0
 * @example DD_UNWRAP_TAIL(2, (a1, a2, a3)) ~         // select n-th arg, counted from 0
 */
#define DD_UNWRAP_TAIL(nth, wrap) DD_TAIL(nth, _DD_UNWRAP(wrap))
/**
 * Unwraps content from brackets.
 * If the brackets are used in macro argument, argument is considered
 *   as just one argument, no matter if argument is comma separated.
 * @example
 *   #define MY_MACRO_NAME(firstName, surName) // process somehow the name
 *   #define MY_MACRO_DATE(day, month, year)   // process somehow the date
 *   #define MY_MACRO(name, date) MY_MACRO_NAME(DD_UNWRAP_UNSAFE(name)) MY_MACRO_DATE(DD_UNWRAP_UNSAFE(date))
 *   MY_MACRO((Edmund, Husserl), (8, 4, 1859))
 */
#define DD_UNWRAP_UNSAFE(x) DD_EXPAND(DD_EXPAND x)
/**
 * Apply a given macro onto multiple args.
 * One argument is fixed and same for all applications.
 * DD_XLIST_EX([MACRO], [fixedArgument], [uniqueArgument]...)
 *
 * @scheme
 *   for each ([uniqueArgument])
 *     MACRO([indexOfUniqueArgument], [numberOfUniqueArguments], [fixedArgument], [uniqueArgument])
 *
 * @example
 *   #define MY_ASSIGN(index, count, sourceName, propertyName) this->propertyName[index] = sourceName.propertyName;
 *   #define MY_ASSIGNS(sourceName, ...) DD_XLIST_EX(MY_ASSIGN, sourceName, __VA_ARGS__)
 *    // assign all enumerated items from matrixSouyrce1
 *    MY_ASSIGNS(matrixSource0, m00, m01, m02, m10, m11, m12, m20, m21, m22);
 *    // assign all enumerated items from matrixSouyrce2
 *    MY_ASSIGNS(matrixSource1, m00, m01, m02, m10, m11, m12, m20, m21, m22);
 */
#define DD_XLIST_EX(X, P, ...) _DD_XLIST_EX(X, P, __VA_ARGS__)
/**
 * Apply a given macro onto multiple args.
 * DD_XLIST([MACRO], [argument]...)
 *
 * @scheme
 *  for each([uniqueArgument])
 *    MACRO([uniqueArgument])
 *
 * @example
 *   #define MY_ASSIGN(propertyName) this->propertyName = propertyName;
 *   #define MY_ASSIGNS(...) DD_XLIST(MY_ASSIGN, __VA_ARGS__)
 *   // assign all enumerated properties
 *   MY_ASSIGNS(m00, m01, m02, m10, m11, m12, m20, m21, m22)
 */
#define DD_XLIST(X, ...) DD_XLIST_EX(_DD_XLIST_UNWRAP, X, __VA_ARGS__)
/**
 * Assign indexable to references.
 * @param collection - an indexable collection, e.g. array
 * @param args...    - name of references
 * @example
 *   DD::Array<MyStruct> items;
 *   DD_LIST(items,
 *     item0,  // auto & item0 = items[0];
 *     item1   // auto & item1 = items[1];
 *   );
 *   item0 = item1 = MyStruct();
 */
#define DD_LIST(collection, ...) DD_XLIST_EX(DD_LIST_ITEM, collection, __VA_ARGS__)

/**
 * Template for using x-list or args in one template.
 */
#define DD_XPLAY(process, ...)\
 DD_TEMPLATE_PLAY(___DD_OVERLOAD_MS(DD_TEMPLATE_AUTOPLAY, __VA_ARGS__), process, __VA_ARGS__)
#define DD_TEMPLATE_AUTOPLAY_S(...) XLIST
#define DD_TEMPLATE_AUTOPLAY_M(...) VARGS
#define DD_TEMPLATE_PLAY(TYPE, ...) DD_EXPAND(DD_CAT_2(DD_TEMPLATE_PLAY_, TYPE)(__VA_ARGS__))
#define DD_TEMPLATE_PLAY_XLIST(process, xList) xList(process)
#define DD_TEMPLATE_PLAY_VARGS(process, ...) DD_XLIST_EX(DD_TEMPLATE_PLAY_UNWRAP, process, __VA_ARGS__)
#define DD_TEMPLATE_PLAY_UNWRAP(index, count, staticArg, instance) DD_PLAY(staticArg, DD_UNWRAP(instance))


/************************************************************************/
/* Special Macros, overloads...                                         */
/************************************************************************/

/** Stamp for debug operations. */
#if DD_DBG_STAMPS_ENABLED
#define DBG_STAMP DD_STAMP("Debug log output.");
#else
#define DBG_STAMP
#endif
/** Stamp highlighting missing implementations. */
#if DD_TODO_STAMPS_ENABLED
#define DD_TODO_STAMP(msg) DD_EXPAND(DD_STAMP("TO DO - "##msg))
#else
#define DD_TODO_STAMP(msg)
#endif
/** DD_CAT overloads, @see DD_CAT(...). */
#define _DD_CAT_1(a)                         a
#define _DD_CAT_2(a, b)                      a##b
#define _DD_CAT_3(a, b, c)                   a##b##c
#define _DD_CAT_4(a, b, c, d)                a##b##c##d
#define _DD_CAT_5(a, b, c, d, e)             a##b##c##d##e
#define _DD_CAT_6(a, b, c, d, e, f)          a##b##c##d##e##f
#define _DD_CAT_7(a, b, c, d, e, f, g)       a##b##c##d##e##f##g
#define _DD_CAT_8(a, b, c, d, e, f, g, h)    a##b##c##d##e##f##g##h
#define _DD_CAT_9(a, b, c, d, e, f, g, h, i) a##b##c##d##e##f##g##h##i
/** DD_CAT overloads, @see DD_CAT(...). */
#define DD_CAT_1(a)                         _DD_CAT_1(a)
#define DD_CAT_2(a, b)                      _DD_CAT_2(a, b)
#define DD_CAT_3(a, b, c)                   _DD_CAT_3(a, b, c)
#define DD_CAT_4(a, b, c, d)                _DD_CAT_4(a, b, c, d)
#define DD_CAT_5(a, b, c, d, e)             _DD_CAT_5(a, b, c, d, e)
#define DD_CAT_6(a, b, c, d, e, f)          _DD_CAT_6(a, b, c, d, e, f)
#define DD_CAT_7(a, b, c, d, e, f, g)       _DD_CAT_7(a, b, c, d, e, f, g)
#define DD_CAT_8(a, b, c, d, e, f, g, h)    _DD_CAT_8(a, b, c, d, e, f, g, h)
#define DD_CAT_9(a, b, c, d, e, f, g, h, i) _DD_CAT_9(a, b, c, d, e, f, g, h, i)
/** DD_SCAT overloads, @see DD_SCAT(...). */
#define DD_SCAT_1(a)                         DD_STRINGIFY(a)
#define DD_SCAT_2(a, b)                      DD_STRINGIFY(a##b)
#define DD_SCAT_3(a, b, c)                   DD_STRINGIFY(a##b##c)
#define DD_SCAT_4(a, b, c, d)                DD_STRINGIFY(a##b##c##d)
#define DD_SCAT_5(a, b, c, d, e)             DD_STRINGIFY(a##b##c##d##e)
#define DD_SCAT_6(a, b, c, d, e, f)          DD_STRINGIFY(a##b##c##d##e##f)
#define DD_SCAT_7(a, b, c, d, e, f, g)       DD_STRINGIFY(a##b##c##d##e##f##g)
#define DD_SCAT_8(a, b, c, d, e, f, g, h)    DD_STRINGIFY(a##b##c##d##e##f##g##h)
#define DD_SCAT_9(a, b, c, d, e, f, g, h, i) DD_STRINGIFY(a##b##c##d##e##f##g##h##i)
/** DD_TAIL overloads, @see DD_TAIL(...). */
#define DD_TAIL_0(a0, ...)                                     __VA_ARGS__
#define DD_TAIL_1(a0, a1, ...)                                 __VA_ARGS__
#define DD_TAIL_2(a0, a1, a2, ...)                             __VA_ARGS__
#define DD_TAIL_3(a0, a1, a2, a3, ...)                         __VA_ARGS__
#define DD_TAIL_4(a0, a1, a2, a3, a4, ...)                     __VA_ARGS__
#define DD_TAIL_5(a0, a1, a2, a3, a4, a5, ...)                 __VA_ARGS__
#define DD_TAIL_6(a0, a1, a2, a3, a4, a5, a6, ...)             __VA_ARGS__
#define DD_TAIL_7(a0, a1, a2, a3, a4, a5, a6, a7, ...)         __VA_ARGS__
#define DD_TAIL_8(a0, a1, a2, a3, a4, a5, a6, a7, a8, ...)     __VA_ARGS__
#define DD_TAIL_9(a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, ...) __VA_ARGS__
/** DD_HEAD overloads, @see DD_TAIL(...). */
#define DD_HEAD_0(a0, ...)                                     a0
#define DD_HEAD_1(a0, a1, ...)                                 a1
#define DD_HEAD_2(a0, a1, a2, ...)                             a2
#define DD_HEAD_3(a0, a1, a2, a3, ...)                         a3
#define DD_HEAD_4(a0, a1, a2, a3, a4, ...)                     a4
#define DD_HEAD_5(a0, a1, a2, a3, a4, a5, ...)                 a5
#define DD_HEAD_6(a0, a1, a2, a3, a4, a5, a6, ...)             a6
#define DD_HEAD_7(a0, a1, a2, a3, a4, a5, a6, a7, ...)         a7
#define DD_HEAD_8(a0, a1, a2, a3, a4, a5, a6, a7, a8, ...)     a8
#define DD_HEAD_9(a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, ...) a9
/** DD_UNWRAP overloads, @see DD_UNWRAP(). */
#define DD_UNWRAP_1(wrap) _DD_UNWRAP(wrap)
#define DD_UNWRAP_2(nth, wrap) DD_HEAD(nth, _DD_UNWRAP(wrap))


/************************************************************************/
/* Private Macros                                                       */
/************************************************************************/
 /** Assign one item to named reference. @see DD_LIST. */
#define DD_LIST_ITEM(index, count, globals, locals) auto & locals = globals[index];
/** Unwraps brackets (helper in case that argument contains commas). */
#define _DD_UNWRAP(wrap) DD_OVERLOAD_CUT_2_3_(_DD_UNWRAP, wrap, DD_UNWRAP_UNSAFE(wrap))
#define _DD_UNWRAP_2(x, ...) x
#define _DD_UNWRAP_3(x, ...) __VA_ARGS__
/** First order overloader. */
#define DD_OVERLOAD_9(_1, _2, _3, _4, _5, _6, _7, _8, _9, x, ...) DD_EXPAND(x)
/** First order overloader. */
#define DD_OVERLOAD_63(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20, _21, _22, _23, _24, _25, _26, _27, _28,_29, _30, _31, _32, _33, _34, _35, _36, _37, _38, _39, _40, _41, _42, _43, _44, _45, _46, _47, _48, _49, _50, _51, _52, _53, _54, _55, _56, _57, _58, _59, _60, _61, _62, _63, x, ...) DD_EXPAND(x)
/** Second order overloader. */
#define _DD_OVERLOAD_63(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20, _21, _22, _23, _24, _25, _26, _27, _28,_29, _30, _31, _32, _33, _34, _35, _36, _37, _38, _39, _40, _41, _42, _43, _44, _45, _46, _47, _48, _49, _50, _51, _52, _53, _54, _55, _56, _57, _58, _59, _60, _61, _62, _63, x, ...) DD_EXPAND(x)
/** Third order overloader - use only in this file. */
#define __DD_OVERLOAD_63(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20, _21, _22, _23, _24, _25, _26, _27, _28,_29, _30, _31, _32, _33, _34, _35, _36, _37, _38, _39, _40, _41, _42, _43, _44, _45, _46, _47, _48, _49, _50, _51, _52, _53, _54, _55, _56, _57, _58, _59, _60, _61, _62, _63, x, ...) DD_EXPAND(x)
/** Fourth order overloader - use only in this file. */
#define ___DD_OVERLOAD_63(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20, _21, _22, _23, _24, _25, _26, _27, _28,_29, _30, _31, _32, _33, _34, _35, _36, _37, _38, _39, _40, _41, _42, _43, _44, _45, _46, _47, _48, _49, _50, _51, _52, _53, _54, _55, _56, _57, _58, _59, _60, _61, _62, _63, x, ...) DD_EXPAND(x)
/** Second order overloader. */
#define _DD_OVERLOAD(x, ...) DD_EXPAND(_DD_OVERLOAD_63(__VA_ARGS__, x##_63, x##_62, x##_61, x##_60, x##_59, x##_58, x##_57, x##_56, x##_55, x##_54, x##_53, x##_52, x##_51, x##_50, x##_49, x##_48, x##_47, x##_46, x##_45, x##_44, x##_43, x##_42, x##_41, x##_40, x##_39, x##_38, x##_37, x##_36, x##_35, x##_34, x##_33, x##_32, x##_31, x##_30, x##_29, x##_28, x##_27, x##_26, x##_25, x##_24, x##_23, x##_22, x##_21, x##_20, x##_19, x##_18, x##_17, x##_16, x##_15, x##_14, x##_13, x##_12, x##_11, x##_10, x##_9, x##_8, x##_7, x##_6, x##_5, x##_4, x##_3, x##_2, x##_1)(__VA_ARGS__))
/** Third order overloader - use only in this file. */
#define __DD_OVERLOAD(x, ...) DD_EXPAND(__DD_OVERLOAD_63(__VA_ARGS__, x##_63, x##_62, x##_61, x##_60, x##_59, x##_58, x##_57, x##_56, x##_55, x##_54, x##_53, x##_52, x##_51, x##_50, x##_49, x##_48, x##_47, x##_46, x##_45, x##_44, x##_43, x##_42, x##_41, x##_40, x##_39, x##_38, x##_37, x##_36, x##_35, x##_34, x##_33, x##_32, x##_31, x##_30, x##_29, x##_28, x##_27, x##_26, x##_25, x##_24, x##_23, x##_22, x##_21, x##_20, x##_19, x##_18, x##_17, x##_16, x##_15, x##_14, x##_13, x##_12, x##_11, x##_10, x##_9, x##_8, x##_7, x##_6, x##_5, x##_4, x##_3, x##_2, x##_1)(__VA_ARGS__))
/** Fourth order overloader - use only in this file. */
#define ___DD_OVERLOAD(x, ...) DD_EXPAND(___DD_OVERLOAD_63(__VA_ARGS__, x##_63, x##_62, x##_61, x##_60, x##_59, x##_58, x##_57, x##_56, x##_55, x##_54, x##_53, x##_52, x##_51, x##_50, x##_49, x##_48, x##_47, x##_46, x##_45, x##_44, x##_43, x##_42, x##_41, x##_40, x##_39, x##_38, x##_37, x##_36, x##_35, x##_34, x##_33, x##_32, x##_31, x##_30, x##_29, x##_28, x##_27, x##_26, x##_25, x##_24, x##_23, x##_22, x##_21, x##_20, x##_19, x##_18, x##_17, x##_16, x##_15, x##_14, x##_13, x##_12, x##_11, x##_10, x##_9, x##_8, x##_7, x##_6, x##_5, x##_4, x##_3, x##_2, x##_1)(__VA_ARGS__))
/** Fourth order overloader (detection of single/multiple args) - use only in this file. */
#define ___DD_OVERLOAD_MS(x, ...) DD_EXPAND(___DD_OVERLOAD_63(__VA_ARGS__, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_M, x##_S)(__VA_ARGS__))
/** First order overloader. */
#define DD_OVERLOAD_CUT_1_2(x, ...) DD_EXPAND(DD_OVERLOAD_9(__VA_ARGS__, x##_2, x##_2, x##_2, x##_2, x##_2, x##_2, x##_2, x##_2, x##_1)(__VA_ARGS__))
/** First order overloader. */
#define DD_OVERLOAD_CUT_2_3(x, ...) DD_EXPAND(DD_OVERLOAD_9(__VA_ARGS__, x##_3, x##_3, x##_3, x##_3, x##_3, x##_3, x##_3, x##_2, x##_2)(__VA_ARGS__))
/** First order overloader. */
#define DD_OVERLOAD_CUT_3_4(x, ...) DD_EXPAND(DD_OVERLOAD_9(__VA_ARGS__, x##_4, x##_4, x##_4, x##_4, x##_4, x##_4, x##_3, x##_3, x##_3)(__VA_ARGS__))
/** Third order overloader - use only in this file. */
#define DD_OVERLOAD_CUT_2_3_(x, ...) DD_EXPAND(DD_OVERLOAD_9(__VA_ARGS__, x##_3, x##_3, x##_3, x##_3, x##_3, x##_3, x##_3, x##_2, x##_2)(__VA_ARGS__))
/** Overloader pushing all the arguments into single argument macro. */
#define _DD_XLIST_EX(process, params, ...) ___DD_OVERLOAD(_DD_XLIST_EX, process, params, __VA_ARGS__)
#define _DD_XLIST_EX_2(X, P)
#define _DD_XLIST_EX_3(X, P, a0) X(0, 1, P, a0)
#define _DD_XLIST_EX_4(X, P, a0, a1) X(0, 2, P, a0) X(1, 2, P, a1)
#define _DD_XLIST_EX_5(X, P, a0, a1, a2) X(0, 3, P, a0) X(1, 3, P, a1) X(2, 3, P, a2)
#define _DD_XLIST_EX_6(X, P, a0, a1, a2, a3) X(0, 4, P, a0) X(1, 4, P, a1) X(2, 4, P, a2) X(3, 4, P, a3)
#define _DD_XLIST_EX_7(X, P, a0, a1, a2, a3, a4) X(0, 5, P, a0) X(1, 5, P, a1) X(2, 5, P, a2) X(3, 5, P, a3) X(4, 5, P, a4)
#define _DD_XLIST_EX_8(X, P, a0, a1, a2, a3, a4, a5) X(0, 6, P, a0) X(1, 6, P, a1) X(2, 6, P, a2) X(3, 6, P, a3) X(4, 6, P, a4) X(5, 6, P, a5)
#define _DD_XLIST_EX_9(X, P, a0, a1, a2, a3, a4, a5, a6) X(0, 7, P, a0) X(1, 7, P, a1) X(2, 7, P, a2) X(3, 7, P, a3) X(4, 7, P, a4) X(5, 7, P, a5) X(6, 7, P, a6)
#define _DD_XLIST_EX_10(X, P, a0, a1, a2, a3, a4, a5, a6, a7) X(0, 8, P, a0) X(1, 8, P, a1) X(2, 8, P, a2) X(3, 8, P, a3) X(4, 8, P, a4) X(5, 8, P, a5) X(6, 8, P, a6) X(7, 8, P, a7)
#define _DD_XLIST_EX_11(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8) X(0, 9, P, a0) X(1, 9, P, a1) X(2, 9, P, a2) X(3, 9, P, a3) X(4, 9, P, a4) X(5, 9, P, a5) X(6, 9, P, a6) X(7, 9, P, a7) X(8, 9, P, a8)
#define _DD_XLIST_EX_12(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9) X(0, 10, P, a0) X(1, 10, P, a1) X(2, 10, P, a2) X(3, 10, P, a3) X(4, 10, P, a4) X(5, 10, P, a5) X(6, 10, P, a6) X(7, 10, P, a7) X(8, 10, P, a8) X(9, 10, P, a9)
#define _DD_XLIST_EX_13(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10) X(0, 11, P, a0) X(1, 11, P, a1) X(2, 11, P, a2) X(3, 11, P, a3) X(4, 11, P, a4) X(5, 11, P, a5) X(6, 11, P, a6) X(7, 11, P, a7) X(8, 11, P, a8) X(9, 11, P, a9) X(10, 11, P, a10)
#define _DD_XLIST_EX_14(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11) X(0, 12, P, a0) X(1, 12, P, a1) X(2, 12, P, a2) X(3, 12, P, a3) X(4, 12, P, a4) X(5, 12, P, a5) X(6, 12, P, a6) X(7, 12, P, a7) X(8, 12, P, a8) X(9, 12, P, a9) X(10, 12, P, a10) X(11, 12, P, a11)
#define _DD_XLIST_EX_15(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12) X(0, 13, P, a0) X(1, 13, P, a1) X(2, 13, P, a2) X(3, 13, P, a3) X(4, 13, P, a4) X(5, 13, P, a5) X(6, 13, P, a6) X(7, 13, P, a7) X(8, 13, P, a8) X(9, 13, P, a9) X(10, 13, P, a10) X(11, 13, P, a11) X(12, 13, P, a12)
#define _DD_XLIST_EX_16(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13) X(0, 14, P, a0) X(1, 14, P, a1) X(2, 14, P, a2) X(3, 14, P, a3) X(4, 14, P, a4) X(5, 14, P, a5) X(6, 14, P, a6) X(7, 14, P, a7) X(8, 14, P, a8) X(9, 14, P, a9) X(10, 14, P, a10) X(11, 14, P, a11) X(12, 14, P, a12) X(13, 14, P, a13)
#define _DD_XLIST_EX_17(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14) X(0, 15, P, a0) X(1, 15, P, a1) X(2, 15, P, a2) X(3, 15, P, a3) X(4, 15, P, a4) X(5, 15, P, a5) X(6, 15, P, a6) X(7, 15, P, a7) X(8, 15, P, a8) X(9, 15, P, a9) X(10, 15, P, a10) X(11, 15, P, a11) X(12, 15, P, a12) X(13, 15, P, a13) X(14, 15, P, a14)
#define _DD_XLIST_EX_18(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15) X(0, 16, P, a0) X(1, 16, P, a1) X(2, 16, P, a2) X(3, 16, P, a3) X(4, 16, P, a4) X(5, 16, P, a5) X(6, 16, P, a6) X(7, 16, P, a7) X(8, 16, P, a8) X(9, 16, P, a9) X(10, 16, P, a10) X(11, 16, P, a11) X(12, 16, P, a12) X(13, 16, P, a13) X(14, 16, P, a14) X(15, 16, P, a15)
#define _DD_XLIST_EX_19(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16) X(0, 17, P, a0) X(1, 17, P, a1) X(2, 17, P, a2) X(3, 17, P, a3) X(4, 17, P, a4) X(5, 17, P, a5) X(6, 17, P, a6) X(7, 17, P, a7) X(8, 17, P, a8) X(9, 17, P, a9) X(10, 17, P, a10) X(11, 17, P, a11) X(12, 17, P, a12) X(13, 17, P, a13) X(14, 17, P, a14) X(15, 17, P, a15) X(16, 17, P, a16)
#define _DD_XLIST_EX_20(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17) X(0, 18, P, a0) X(1, 18, P, a1) X(2, 18, P, a2) X(3, 18, P, a3) X(4, 18, P, a4) X(5, 18, P, a5) X(6, 18, P, a6) X(7, 18, P, a7) X(8, 18, P, a8) X(9, 18, P, a9) X(10, 18, P, a10) X(11, 18, P, a11) X(12, 18, P, a12) X(13, 18, P, a13) X(14, 18, P, a14) X(15, 18, P, a15) X(16, 18, P, a16) X(17, 18, P, a17)
#define _DD_XLIST_EX_21(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18) X(0, 19, P, a0) X(1, 19, P, a1) X(2, 19, P, a2) X(3, 19, P, a3) X(4, 19, P, a4) X(5, 19, P, a5) X(6, 19, P, a6) X(7, 19, P, a7) X(8, 19, P, a8) X(9, 19, P, a9) X(10, 19, P, a10) X(11, 19, P, a11) X(12, 19, P, a12) X(13, 19, P, a13) X(14, 19, P, a14) X(15, 19, P, a15) X(16, 19, P, a16) X(17, 19, P, a17) X(18, 19, P, a18)
#define _DD_XLIST_EX_22(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19) X(0, 20, P, a0) X(1, 20, P, a1) X(2, 20, P, a2) X(3, 20, P, a3) X(4, 20, P, a4) X(5, 20, P, a5) X(6, 20, P, a6) X(7, 20, P, a7) X(8, 20, P, a8) X(9, 20, P, a9) X(10, 20, P, a10) X(11, 20, P, a11) X(12, 20, P, a12) X(13, 20, P, a13) X(14, 20, P, a14) X(15, 20, P, a15) X(16, 20, P, a16) X(17, 20, P, a17) X(18, 20, P, a18) X(19, 20, P, a19)
#define _DD_XLIST_EX_23(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20) X(0, 21, P, a0) X(1, 21, P, a1) X(2, 21, P, a2) X(3, 21, P, a3) X(4, 21, P, a4) X(5, 21, P, a5) X(6, 21, P, a6) X(7, 21, P, a7) X(8, 21, P, a8) X(9, 21, P, a9) X(10, 21, P, a10) X(11, 21, P, a11) X(12, 21, P, a12) X(13, 21, P, a13) X(14, 21, P, a14) X(15, 21, P, a15) X(16, 21, P, a16) X(17, 21, P, a17) X(18, 21, P, a18) X(19, 21, P, a19) X(20, 21, P, a20)
#define _DD_XLIST_EX_24(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21) X(0, 22, P, a0) X(1, 22, P, a1) X(2, 22, P, a2) X(3, 22, P, a3) X(4, 22, P, a4) X(5, 22, P, a5) X(6, 22, P, a6) X(7, 22, P, a7) X(8, 22, P, a8) X(9, 22, P, a9) X(10, 22, P, a10) X(11, 22, P, a11) X(12, 22, P, a12) X(13, 22, P, a13) X(14, 22, P, a14) X(15, 22, P, a15) X(16, 22, P, a16) X(17, 22, P, a17) X(18, 22, P, a18) X(19, 22, P, a19) X(20, 22, P, a20) X(21, 22, P, a21)
#define _DD_XLIST_EX_25(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22) X(0, 23, P, a0) X(1, 23, P, a1) X(2, 23, P, a2) X(3, 23, P, a3) X(4, 23, P, a4) X(5, 23, P, a5) X(6, 23, P, a6) X(7, 23, P, a7) X(8, 23, P, a8) X(9, 23, P, a9) X(10, 23, P, a10) X(11, 23, P, a11) X(12, 23, P, a12) X(13, 23, P, a13) X(14, 23, P, a14) X(15, 23, P, a15) X(16, 23, P, a16) X(17, 23, P, a17) X(18, 23, P, a18) X(19, 23, P, a19) X(20, 23, P, a20) X(21, 23, P, a21) X(22, 23, P, a22)
#define _DD_XLIST_EX_26(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23) X(0, 24, P, a0) X(1, 24, P, a1) X(2, 24, P, a2) X(3, 24, P, a3) X(4, 24, P, a4) X(5, 24, P, a5) X(6, 24, P, a6) X(7, 24, P, a7) X(8, 24, P, a8) X(9, 24, P, a9) X(10, 24, P, a10) X(11, 24, P, a11) X(12, 24, P, a12) X(13, 24, P, a13) X(14, 24, P, a14) X(15, 24, P, a15) X(16, 24, P, a16) X(17, 24, P, a17) X(18, 24, P, a18) X(19, 24, P, a19) X(20, 24, P, a20) X(21, 24, P, a21) X(22, 24, P, a22) X(23, 24, P, a23)
#define _DD_XLIST_EX_27(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23, a24) X(0, 25, P, a0) X(1, 25, P, a1) X(2, 25, P, a2) X(3, 25, P, a3) X(4, 25, P, a4) X(5, 25, P, a5) X(6, 25, P, a6) X(7, 25, P, a7) X(8, 25, P, a8) X(9, 25, P, a9) X(10, 25, P, a10) X(11, 25, P, a11) X(12, 25, P, a12) X(13, 25, P, a13) X(14, 25, P, a14) X(15, 25, P, a15) X(16, 25, P, a16) X(17, 25, P, a17) X(18, 25, P, a18) X(19, 25, P, a19) X(20, 25, P, a20) X(21, 25, P, a21) X(22, 25, P, a22) X(23, 25, P, a23) X(24, 25, P, a24)
#define _DD_XLIST_EX_28(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23, a24, a25) X(0, 26, P, a0) X(1, 26, P, a1) X(2, 26, P, a2) X(3, 26, P, a3) X(4, 26, P, a4) X(5, 26, P, a5) X(6, 26, P, a6) X(7, 26, P, a7) X(8, 26, P, a8) X(9, 26, P, a9) X(10, 26, P, a10) X(11, 26, P, a11) X(12, 26, P, a12) X(13, 26, P, a13) X(14, 26, P, a14) X(15, 26, P, a15) X(16, 26, P, a16) X(17, 26, P, a17) X(18, 26, P, a18) X(19, 26, P, a19) X(20, 26, P, a20) X(21, 26, P, a21) X(22, 26, P, a22) X(23, 26, P, a23) X(24, 26, P, a24) X(25, 26, P, a25)
#define _DD_XLIST_EX_29(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23, a24, a25, a26) X(0, 27, P, a0) X(1, 27, P, a1) X(2, 27, P, a2) X(3, 27, P, a3) X(4, 27, P, a4) X(5, 27, P, a5) X(6, 27, P, a6) X(7, 27, P, a7) X(8, 27, P, a8) X(9, 27, P, a9) X(10, 27, P, a10) X(11, 27, P, a11) X(12, 27, P, a12) X(13, 27, P, a13) X(14, 27, P, a14) X(15, 27, P, a15) X(16, 27, P, a16) X(17, 27, P, a17) X(18, 27, P, a18) X(19, 27, P, a19) X(20, 27, P, a20) X(21, 27, P, a21) X(22, 27, P, a22) X(23, 27, P, a23) X(24, 27, P, a24) X(25, 27, P, a25) X(26, 27, P, a26)
#define _DD_XLIST_EX_30(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23, a24, a25, a26, a27) X(0, 28, P, a0) X(1, 28, P, a1) X(2, 28, P, a2) X(3, 28, P, a3) X(4, 28, P, a4) X(5, 28, P, a5) X(6, 28, P, a6) X(7, 28, P, a7) X(8, 28, P, a8) X(9, 28, P, a9) X(10, 28, P, a10) X(11, 28, P, a11) X(12, 28, P, a12) X(13, 28, P, a13) X(14, 28, P, a14) X(15, 28, P, a15) X(16, 28, P, a16) X(17, 28, P, a17) X(18, 28, P, a18) X(19, 28, P, a19) X(20, 28, P, a20) X(21, 28, P, a21) X(22, 28, P, a22) X(23, 28, P, a23) X(24, 28, P, a24) X(25, 28, P, a25) X(26, 28, P, a26) X(27, 28, P, a27)
#define _DD_XLIST_EX_31(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23, a24, a25, a26, a27, a28) X(0, 29, P, a0) X(1, 29, P, a1) X(2, 29, P, a2) X(3, 29, P, a3) X(4, 29, P, a4) X(5, 29, P, a5) X(6, 29, P, a6) X(7, 29, P, a7) X(8, 29, P, a8) X(9, 29, P, a9) X(10, 29, P, a10) X(11, 29, P, a11) X(12, 29, P, a12) X(13, 29, P, a13) X(14, 29, P, a14) X(15, 29, P, a15) X(16, 29, P, a16) X(17, 29, P, a17) X(18, 29, P, a18) X(19, 29, P, a19) X(20, 29, P, a20) X(21, 29, P, a21) X(22, 29, P, a22) X(23, 29, P, a23) X(24, 29, P, a24) X(25, 29, P, a25) X(26, 29, P, a26) X(27, 29, P, a27) X(28, 29, P, a28)
#define _DD_XLIST_EX_32(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23, a24, a25, a26, a27, a28, a29) X(0, 30, P, a0) X(1, 30, P, a1) X(2, 30, P, a2) X(3, 30, P, a3) X(4, 30, P, a4) X(5, 30, P, a5) X(6, 30, P, a6) X(7, 30, P, a7) X(8, 30, P, a8) X(9, 30, P, a9) X(10, 30, P, a10) X(11, 30, P, a11) X(12, 30, P, a12) X(13, 30, P, a13) X(14, 30, P, a14) X(15, 30, P, a15) X(16, 30, P, a16) X(17, 30, P, a17) X(18, 30, P, a18) X(19, 30, P, a19) X(20, 30, P, a20) X(21, 30, P, a21) X(22, 30, P, a22) X(23, 30, P, a23) X(24, 30, P, a24) X(25, 30, P, a25) X(26, 30, P, a26) X(27, 30, P, a27) X(28, 30, P, a28) X(29, 30, P, a29)
#define _DD_XLIST_EX_33(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23, a24, a25, a26, a27, a28, a29, a30) X(0, 31, P, a0) X(1, 31, P, a1) X(2, 31, P, a2) X(3, 31, P, a3) X(4, 31, P, a4) X(5, 31, P, a5) X(6, 31, P, a6) X(7, 31, P, a7) X(8, 31, P, a8) X(9, 31, P, a9) X(10, 31, P, a10) X(11, 31, P, a11) X(12, 31, P, a12) X(13, 31, P, a13) X(14, 31, P, a14) X(15, 31, P, a15) X(16, 31, P, a16) X(17, 31, P, a17) X(18, 31, P, a18) X(19, 31, P, a19) X(20, 31, P, a20) X(21, 31, P, a21) X(22, 31, P, a22) X(23, 31, P, a23) X(24, 31, P, a24) X(25, 31, P, a25) X(26, 31, P, a26) X(27, 31, P, a27) X(28, 31, P, a28) X(29, 31, P, a29) X(30, 31, P, a30)
#define _DD_XLIST_EX_34(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23, a24, a25, a26, a27, a28, a29, a30, a31) X(0, 32, P, a0) X(1, 32, P, a1) X(2, 32, P, a2) X(3, 32, P, a3) X(4, 32, P, a4) X(5, 32, P, a5) X(6, 32, P, a6) X(7, 32, P, a7) X(8, 32, P, a8) X(9, 32, P, a9) X(10, 32, P, a10) X(11, 32, P, a11) X(12, 32, P, a12) X(13, 32, P, a13) X(14, 32, P, a14) X(15, 32, P, a15) X(16, 32, P, a16) X(17, 32, P, a17) X(18, 32, P, a18) X(19, 32, P, a19) X(20, 32, P, a20) X(21, 32, P, a21) X(22, 32, P, a22) X(23, 32, P, a23) X(24, 32, P, a24) X(25, 32, P, a25) X(26, 32, P, a26) X(27, 32, P, a27) X(28, 32, P, a28) X(29, 32, P, a29) X(30, 32, P, a30) X(31, 32, P, a31)
#define _DD_XLIST_EX_35(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23, a24, a25, a26, a27, a28, a29, a30, a31, a32) X(0, 33, P, a0) X(1, 33, P, a1) X(2, 33, P, a2) X(3, 33, P, a3) X(4, 33, P, a4) X(5, 33, P, a5) X(6, 33, P, a6) X(7, 33, P, a7) X(8, 33, P, a8) X(9, 33, P, a9) X(10, 33, P, a10) X(11, 33, P, a11) X(12, 33, P, a12) X(13, 33, P, a13) X(14, 33, P, a14) X(15, 33, P, a15) X(16, 33, P, a16) X(17, 33, P, a17) X(18, 33, P, a18) X(19, 33, P, a19) X(20, 33, P, a20) X(21, 33, P, a21) X(22, 33, P, a22) X(23, 33, P, a23) X(24, 33, P, a24) X(25, 33, P, a25) X(26, 33, P, a26) X(27, 33, P, a27) X(28, 33, P, a28) X(29, 33, P, a29) X(30, 33, P, a30) X(31, 33, P, a31) X(32, 33, P, a32)
#define _DD_XLIST_EX_36(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23, a24, a25, a26, a27, a28, a29, a30, a31, a32, a33) X(0, 34, P, a0) X(1, 34, P, a1) X(2, 34, P, a2) X(3, 34, P, a3) X(4, 34, P, a4) X(5, 34, P, a5) X(6, 34, P, a6) X(7, 34, P, a7) X(8, 34, P, a8) X(9, 34, P, a9) X(10, 34, P, a10) X(11, 34, P, a11) X(12, 34, P, a12) X(13, 34, P, a13) X(14, 34, P, a14) X(15, 34, P, a15) X(16, 34, P, a16) X(17, 34, P, a17) X(18, 34, P, a18) X(19, 34, P, a19) X(20, 34, P, a20) X(21, 34, P, a21) X(22, 34, P, a22) X(23, 34, P, a23) X(24, 34, P, a24) X(25, 34, P, a25) X(26, 34, P, a26) X(27, 34, P, a27) X(28, 34, P, a28) X(29, 34, P, a29) X(30, 34, P, a30) X(31, 34, P, a31) X(32, 34, P, a32) X(33, 34, P, a33)
#define _DD_XLIST_EX_37(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23, a24, a25, a26, a27, a28, a29, a30, a31, a32, a33, a34) X(0, 35, P, a0) X(1, 35, P, a1) X(2, 35, P, a2) X(3, 35, P, a3) X(4, 35, P, a4) X(5, 35, P, a5) X(6, 35, P, a6) X(7, 35, P, a7) X(8, 35, P, a8) X(9, 35, P, a9) X(10, 35, P, a10) X(11, 35, P, a11) X(12, 35, P, a12) X(13, 35, P, a13) X(14, 35, P, a14) X(15, 35, P, a15) X(16, 35, P, a16) X(17, 35, P, a17) X(18, 35, P, a18) X(19, 35, P, a19) X(20, 35, P, a20) X(21, 35, P, a21) X(22, 35, P, a22) X(23, 35, P, a23) X(24, 35, P, a24) X(25, 35, P, a25) X(26, 35, P, a26) X(27, 35, P, a27) X(28, 35, P, a28) X(29, 35, P, a29) X(30, 35, P, a30) X(31, 35, P, a31) X(32, 35, P, a32) X(33, 35, P, a33) X(34, 35, P, a34)
#define _DD_XLIST_EX_38(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23, a24, a25, a26, a27, a28, a29, a30, a31, a32, a33, a34, a35) X(0, 36, P, a0) X(1, 36, P, a1) X(2, 36, P, a2) X(3, 36, P, a3) X(4, 36, P, a4) X(5, 36, P, a5) X(6, 36, P, a6) X(7, 36, P, a7) X(8, 36, P, a8) X(9, 36, P, a9) X(10, 36, P, a10) X(11, 36, P, a11) X(12, 36, P, a12) X(13, 36, P, a13) X(14, 36, P, a14) X(15, 36, P, a15) X(16, 36, P, a16) X(17, 36, P, a17) X(18, 36, P, a18) X(19, 36, P, a19) X(20, 36, P, a20) X(21, 36, P, a21) X(22, 36, P, a22) X(23, 36, P, a23) X(24, 36, P, a24) X(25, 36, P, a25) X(26, 36, P, a26) X(27, 36, P, a27) X(28, 36, P, a28) X(29, 36, P, a29) X(30, 36, P, a30) X(31, 36, P, a31) X(32, 36, P, a32) X(33, 36, P, a33) X(34, 36, P, a34) X(35, 36, P, a35)
#define _DD_XLIST_EX_39(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23, a24, a25, a26, a27, a28, a29, a30, a31, a32, a33, a34, a35, a36) X(0, 37, P, a0) X(1, 37, P, a1) X(2, 37, P, a2) X(3, 37, P, a3) X(4, 37, P, a4) X(5, 37, P, a5) X(6, 37, P, a6) X(7, 37, P, a7) X(8, 37, P, a8) X(9, 37, P, a9) X(10, 37, P, a10) X(11, 37, P, a11) X(12, 37, P, a12) X(13, 37, P, a13) X(14, 37, P, a14) X(15, 37, P, a15) X(16, 37, P, a16) X(17, 37, P, a17) X(18, 37, P, a18) X(19, 37, P, a19) X(20, 37, P, a20) X(21, 37, P, a21) X(22, 37, P, a22) X(23, 37, P, a23) X(24, 37, P, a24) X(25, 37, P, a25) X(26, 37, P, a26) X(27, 37, P, a27) X(28, 37, P, a28) X(29, 37, P, a29) X(30, 37, P, a30) X(31, 37, P, a31) X(32, 37, P, a32) X(33, 37, P, a33) X(34, 37, P, a34) X(35, 37, P, a35) X(36, 37, P, a36)
#define _DD_XLIST_EX_40(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23, a24, a25, a26, a27, a28, a29, a30, a31, a32, a33, a34, a35, a36, a37) X(0, 38, P, a0) X(1, 38, P, a1) X(2, 38, P, a2) X(3, 38, P, a3) X(4, 38, P, a4) X(5, 38, P, a5) X(6, 38, P, a6) X(7, 38, P, a7) X(8, 38, P, a8) X(9, 38, P, a9) X(10, 38, P, a10) X(11, 38, P, a11) X(12, 38, P, a12) X(13, 38, P, a13) X(14, 38, P, a14) X(15, 38, P, a15) X(16, 38, P, a16) X(17, 38, P, a17) X(18, 38, P, a18) X(19, 38, P, a19) X(20, 38, P, a20) X(21, 38, P, a21) X(22, 38, P, a22) X(23, 38, P, a23) X(24, 38, P, a24) X(25, 38, P, a25) X(26, 38, P, a26) X(27, 38, P, a27) X(28, 38, P, a28) X(29, 38, P, a29) X(30, 38, P, a30) X(31, 38, P, a31) X(32, 38, P, a32) X(33, 38, P, a33) X(34, 38, P, a34) X(35, 38, P, a35) X(36, 38, P, a36) X(37, 38, P, a37)
#define _DD_XLIST_EX_41(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23, a24, a25, a26, a27, a28, a29, a30, a31, a32, a33, a34, a35, a36, a37, a38) X(0, 39, P, a0) X(1, 39, P, a1) X(2, 39, P, a2) X(3, 39, P, a3) X(4, 39, P, a4) X(5, 39, P, a5) X(6, 39, P, a6) X(7, 39, P, a7) X(8, 39, P, a8) X(9, 39, P, a9) X(10, 39, P, a10) X(11, 39, P, a11) X(12, 39, P, a12) X(13, 39, P, a13) X(14, 39, P, a14) X(15, 39, P, a15) X(16, 39, P, a16) X(17, 39, P, a17) X(18, 39, P, a18) X(19, 39, P, a19) X(20, 39, P, a20) X(21, 39, P, a21) X(22, 39, P, a22) X(23, 39, P, a23) X(24, 39, P, a24) X(25, 39, P, a25) X(26, 39, P, a26) X(27, 39, P, a27) X(28, 39, P, a28) X(29, 39, P, a29) X(30, 39, P, a30) X(31, 39, P, a31) X(32, 39, P, a32) X(33, 39, P, a33) X(34, 39, P, a34) X(35, 39, P, a35) X(36, 39, P, a36) X(37, 39, P, a37) X(38, 39, P, a38)
#define _DD_XLIST_EX_42(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23, a24, a25, a26, a27, a28, a29, a30, a31, a32, a33, a34, a35, a36, a37, a38, a39) X(0, 40, P, a0) X(1, 40, P, a1) X(2, 40, P, a2) X(3, 40, P, a3) X(4, 40, P, a4) X(5, 40, P, a5) X(6, 40, P, a6) X(7, 40, P, a7) X(8, 40, P, a8) X(9, 40, P, a9) X(10, 40, P, a10) X(11, 40, P, a11) X(12, 40, P, a12) X(13, 40, P, a13) X(14, 40, P, a14) X(15, 40, P, a15) X(16, 40, P, a16) X(17, 40, P, a17) X(18, 40, P, a18) X(19, 40, P, a19) X(20, 40, P, a20) X(21, 40, P, a21) X(22, 40, P, a22) X(23, 40, P, a23) X(24, 40, P, a24) X(25, 40, P, a25) X(26, 40, P, a26) X(27, 40, P, a27) X(28, 40, P, a28) X(29, 40, P, a29) X(30, 40, P, a30) X(31, 40, P, a31) X(32, 40, P, a32) X(33, 40, P, a33) X(34, 40, P, a34) X(35, 40, P, a35) X(36, 40, P, a36) X(37, 40, P, a37) X(38, 40, P, a38) X(39, 40, P, a39)
#define _DD_XLIST_EX_43(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23, a24, a25, a26, a27, a28, a29, a30, a31, a32, a33, a34, a35, a36, a37, a38, a39, a40) X(0, 41, P, a0) X(1, 41, P, a1) X(2, 41, P, a2) X(3, 41, P, a3) X(4, 41, P, a4) X(5, 41, P, a5) X(6, 41, P, a6) X(7, 41, P, a7) X(8, 41, P, a8) X(9, 41, P, a9) X(10, 41, P, a10) X(11, 41, P, a11) X(12, 41, P, a12) X(13, 41, P, a13) X(14, 41, P, a14) X(15, 41, P, a15) X(16, 41, P, a16) X(17, 41, P, a17) X(18, 41, P, a18) X(19, 41, P, a19) X(20, 41, P, a20) X(21, 41, P, a21) X(22, 41, P, a22) X(23, 41, P, a23) X(24, 41, P, a24) X(25, 41, P, a25) X(26, 41, P, a26) X(27, 41, P, a27) X(28, 41, P, a28) X(29, 41, P, a29) X(30, 41, P, a30) X(31, 41, P, a31) X(32, 41, P, a32) X(33, 41, P, a33) X(34, 41, P, a34) X(35, 41, P, a35) X(36, 41, P, a36) X(37, 41, P, a37) X(38, 41, P, a38) X(39, 41, P, a39) X(40, 41, P, a40)
#define _DD_XLIST_EX_44(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23, a24, a25, a26, a27, a28, a29, a30, a31, a32, a33, a34, a35, a36, a37, a38, a39, a40, a41) X(0, 42, P, a0) X(1, 42, P, a1) X(2, 42, P, a2) X(3, 42, P, a3) X(4, 42, P, a4) X(5, 42, P, a5) X(6, 42, P, a6) X(7, 42, P, a7) X(8, 42, P, a8) X(9, 42, P, a9) X(10, 42, P, a10) X(11, 42, P, a11) X(12, 42, P, a12) X(13, 42, P, a13) X(14, 42, P, a14) X(15, 42, P, a15) X(16, 42, P, a16) X(17, 42, P, a17) X(18, 42, P, a18) X(19, 42, P, a19) X(20, 42, P, a20) X(21, 42, P, a21) X(22, 42, P, a22) X(23, 42, P, a23) X(24, 42, P, a24) X(25, 42, P, a25) X(26, 42, P, a26) X(27, 42, P, a27) X(28, 42, P, a28) X(29, 42, P, a29) X(30, 42, P, a30) X(31, 42, P, a31) X(32, 42, P, a32) X(33, 42, P, a33) X(34, 42, P, a34) X(35, 42, P, a35) X(36, 42, P, a36) X(37, 42, P, a37) X(38, 42, P, a38) X(39, 42, P, a39) X(40, 42, P, a40) X(41, 42, P, a41)
#define _DD_XLIST_EX_45(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23, a24, a25, a26, a27, a28, a29, a30, a31, a32, a33, a34, a35, a36, a37, a38, a39, a40, a41, a42) X(0, 43, P, a0) X(1, 43, P, a1) X(2, 43, P, a2) X(3, 43, P, a3) X(4, 43, P, a4) X(5, 43, P, a5) X(6, 43, P, a6) X(7, 43, P, a7) X(8, 43, P, a8) X(9, 43, P, a9) X(10, 43, P, a10) X(11, 43, P, a11) X(12, 43, P, a12) X(13, 43, P, a13) X(14, 43, P, a14) X(15, 43, P, a15) X(16, 43, P, a16) X(17, 43, P, a17) X(18, 43, P, a18) X(19, 43, P, a19) X(20, 43, P, a20) X(21, 43, P, a21) X(22, 43, P, a22) X(23, 43, P, a23) X(24, 43, P, a24) X(25, 43, P, a25) X(26, 43, P, a26) X(27, 43, P, a27) X(28, 43, P, a28) X(29, 43, P, a29) X(30, 43, P, a30) X(31, 43, P, a31) X(32, 43, P, a32) X(33, 43, P, a33) X(34, 43, P, a34) X(35, 43, P, a35) X(36, 43, P, a36) X(37, 43, P, a37) X(38, 43, P, a38) X(39, 43, P, a39) X(40, 43, P, a40) X(41, 43, P, a41) X(42, 43, P, a42)
#define _DD_XLIST_EX_46(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23, a24, a25, a26, a27, a28, a29, a30, a31, a32, a33, a34, a35, a36, a37, a38, a39, a40, a41, a42, a43) X(0, 44, P, a0) X(1, 44, P, a1) X(2, 44, P, a2) X(3, 44, P, a3) X(4, 44, P, a4) X(5, 44, P, a5) X(6, 44, P, a6) X(7, 44, P, a7) X(8, 44, P, a8) X(9, 44, P, a9) X(10, 44, P, a10) X(11, 44, P, a11) X(12, 44, P, a12) X(13, 44, P, a13) X(14, 44, P, a14) X(15, 44, P, a15) X(16, 44, P, a16) X(17, 44, P, a17) X(18, 44, P, a18) X(19, 44, P, a19) X(20, 44, P, a20) X(21, 44, P, a21) X(22, 44, P, a22) X(23, 44, P, a23) X(24, 44, P, a24) X(25, 44, P, a25) X(26, 44, P, a26) X(27, 44, P, a27) X(28, 44, P, a28) X(29, 44, P, a29) X(30, 44, P, a30) X(31, 44, P, a31) X(32, 44, P, a32) X(33, 44, P, a33) X(34, 44, P, a34) X(35, 44, P, a35) X(36, 44, P, a36) X(37, 44, P, a37) X(38, 44, P, a38) X(39, 44, P, a39) X(40, 44, P, a40) X(41, 44, P, a41) X(42, 44, P, a42) X(43, 44, P, a43)
#define _DD_XLIST_EX_47(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23, a24, a25, a26, a27, a28, a29, a30, a31, a32, a33, a34, a35, a36, a37, a38, a39, a40, a41, a42, a43, a44) X(0, 45, P, a0) X(1, 45, P, a1) X(2, 45, P, a2) X(3, 45, P, a3) X(4, 45, P, a4) X(5, 45, P, a5) X(6, 45, P, a6) X(7, 45, P, a7) X(8, 45, P, a8) X(9, 45, P, a9) X(10, 45, P, a10) X(11, 45, P, a11) X(12, 45, P, a12) X(13, 45, P, a13) X(14, 45, P, a14) X(15, 45, P, a15) X(16, 45, P, a16) X(17, 45, P, a17) X(18, 45, P, a18) X(19, 45, P, a19) X(20, 45, P, a20) X(21, 45, P, a21) X(22, 45, P, a22) X(23, 45, P, a23) X(24, 45, P, a24) X(25, 45, P, a25) X(26, 45, P, a26) X(27, 45, P, a27) X(28, 45, P, a28) X(29, 45, P, a29) X(30, 45, P, a30) X(31, 45, P, a31) X(32, 45, P, a32) X(33, 45, P, a33) X(34, 45, P, a34) X(35, 45, P, a35) X(36, 45, P, a36) X(37, 45, P, a37) X(38, 45, P, a38) X(39, 45, P, a39) X(40, 45, P, a40) X(41, 45, P, a41) X(42, 45, P, a42) X(43, 45, P, a43) X(44, 45, P, a44)
#define _DD_XLIST_EX_48(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23, a24, a25, a26, a27, a28, a29, a30, a31, a32, a33, a34, a35, a36, a37, a38, a39, a40, a41, a42, a43, a44, a45) X(0, 46, P, a0) X(1, 46, P, a1) X(2, 46, P, a2) X(3, 46, P, a3) X(4, 46, P, a4) X(5, 46, P, a5) X(6, 46, P, a6) X(7, 46, P, a7) X(8, 46, P, a8) X(9, 46, P, a9) X(10, 46, P, a10) X(11, 46, P, a11) X(12, 46, P, a12) X(13, 46, P, a13) X(14, 46, P, a14) X(15, 46, P, a15) X(16, 46, P, a16) X(17, 46, P, a17) X(18, 46, P, a18) X(19, 46, P, a19) X(20, 46, P, a20) X(21, 46, P, a21) X(22, 46, P, a22) X(23, 46, P, a23) X(24, 46, P, a24) X(25, 46, P, a25) X(26, 46, P, a26) X(27, 46, P, a27) X(28, 46, P, a28) X(29, 46, P, a29) X(30, 46, P, a30) X(31, 46, P, a31) X(32, 46, P, a32) X(33, 46, P, a33) X(34, 46, P, a34) X(35, 46, P, a35) X(36, 46, P, a36) X(37, 46, P, a37) X(38, 46, P, a38) X(39, 46, P, a39) X(40, 46, P, a40) X(41, 46, P, a41) X(42, 46, P, a42) X(43, 46, P, a43) X(44, 46, P, a44) X(45, 46, P, a45)
#define _DD_XLIST_EX_49(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23, a24, a25, a26, a27, a28, a29, a30, a31, a32, a33, a34, a35, a36, a37, a38, a39, a40, a41, a42, a43, a44, a45, a46) X(0, 47, P, a0) X(1, 47, P, a1) X(2, 47, P, a2) X(3, 47, P, a3) X(4, 47, P, a4) X(5, 47, P, a5) X(6, 47, P, a6) X(7, 47, P, a7) X(8, 47, P, a8) X(9, 47, P, a9) X(10, 47, P, a10) X(11, 47, P, a11) X(12, 47, P, a12) X(13, 47, P, a13) X(14, 47, P, a14) X(15, 47, P, a15) X(16, 47, P, a16) X(17, 47, P, a17) X(18, 47, P, a18) X(19, 47, P, a19) X(20, 47, P, a20) X(21, 47, P, a21) X(22, 47, P, a22) X(23, 47, P, a23) X(24, 47, P, a24) X(25, 47, P, a25) X(26, 47, P, a26) X(27, 47, P, a27) X(28, 47, P, a28) X(29, 47, P, a29) X(30, 47, P, a30) X(31, 47, P, a31) X(32, 47, P, a32) X(33, 47, P, a33) X(34, 47, P, a34) X(35, 47, P, a35) X(36, 47, P, a36) X(37, 47, P, a37) X(38, 47, P, a38) X(39, 47, P, a39) X(40, 47, P, a40) X(41, 47, P, a41) X(42, 47, P, a42) X(43, 47, P, a43) X(44, 47, P, a44) X(45, 47, P, a45) X(46, 47, P, a46)
#define _DD_XLIST_EX_50(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23, a24, a25, a26, a27, a28, a29, a30, a31, a32, a33, a34, a35, a36, a37, a38, a39, a40, a41, a42, a43, a44, a45, a46, a47) X(0, 48, P, a0) X(1, 48, P, a1) X(2, 48, P, a2) X(3, 48, P, a3) X(4, 48, P, a4) X(5, 48, P, a5) X(6, 48, P, a6) X(7, 48, P, a7) X(8, 48, P, a8) X(9, 48, P, a9) X(10, 48, P, a10) X(11, 48, P, a11) X(12, 48, P, a12) X(13, 48, P, a13) X(14, 48, P, a14) X(15, 48, P, a15) X(16, 48, P, a16) X(17, 48, P, a17) X(18, 48, P, a18) X(19, 48, P, a19) X(20, 48, P, a20) X(21, 48, P, a21) X(22, 48, P, a22) X(23, 48, P, a23) X(24, 48, P, a24) X(25, 48, P, a25) X(26, 48, P, a26) X(27, 48, P, a27) X(28, 48, P, a28) X(29, 48, P, a29) X(30, 48, P, a30) X(31, 48, P, a31) X(32, 48, P, a32) X(33, 48, P, a33) X(34, 48, P, a34) X(35, 48, P, a35) X(36, 48, P, a36) X(37, 48, P, a37) X(38, 48, P, a38) X(39, 48, P, a39) X(40, 48, P, a40) X(41, 48, P, a41) X(42, 48, P, a42) X(43, 48, P, a43) X(44, 48, P, a44) X(45, 48, P, a45) X(46, 48, P, a46) X(47, 48, P, a47)
#define _DD_XLIST_EX_51(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23, a24, a25, a26, a27, a28, a29, a30, a31, a32, a33, a34, a35, a36, a37, a38, a39, a40, a41, a42, a43, a44, a45, a46, a47, a48) X(0, 49, P, a0) X(1, 49, P, a1) X(2, 49, P, a2) X(3, 49, P, a3) X(4, 49, P, a4) X(5, 49, P, a5) X(6, 49, P, a6) X(7, 49, P, a7) X(8, 49, P, a8) X(9, 49, P, a9) X(10, 49, P, a10) X(11, 49, P, a11) X(12, 49, P, a12) X(13, 49, P, a13) X(14, 49, P, a14) X(15, 49, P, a15) X(16, 49, P, a16) X(17, 49, P, a17) X(18, 49, P, a18) X(19, 49, P, a19) X(20, 49, P, a20) X(21, 49, P, a21) X(22, 49, P, a22) X(23, 49, P, a23) X(24, 49, P, a24) X(25, 49, P, a25) X(26, 49, P, a26) X(27, 49, P, a27) X(28, 49, P, a28) X(29, 49, P, a29) X(30, 49, P, a30) X(31, 49, P, a31) X(32, 49, P, a32) X(33, 49, P, a33) X(34, 49, P, a34) X(35, 49, P, a35) X(36, 49, P, a36) X(37, 49, P, a37) X(38, 49, P, a38) X(39, 49, P, a39) X(40, 49, P, a40) X(41, 49, P, a41) X(42, 49, P, a42) X(43, 49, P, a43) X(44, 49, P, a44) X(45, 49, P, a45) X(46, 49, P, a46) X(47, 49, P, a47) X(48, 49, P, a48)
#define _DD_XLIST_EX_52(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23, a24, a25, a26, a27, a28, a29, a30, a31, a32, a33, a34, a35, a36, a37, a38, a39, a40, a41, a42, a43, a44, a45, a46, a47, a48, a49) X(0, 50, P, a0) X(1, 50, P, a1) X(2, 50, P, a2) X(3, 50, P, a3) X(4, 50, P, a4) X(5, 50, P, a5) X(6, 50, P, a6) X(7, 50, P, a7) X(8, 50, P, a8) X(9, 50, P, a9) X(10, 50, P, a10) X(11, 50, P, a11) X(12, 50, P, a12) X(13, 50, P, a13) X(14, 50, P, a14) X(15, 50, P, a15) X(16, 50, P, a16) X(17, 50, P, a17) X(18, 50, P, a18) X(19, 50, P, a19) X(20, 50, P, a20) X(21, 50, P, a21) X(22, 50, P, a22) X(23, 50, P, a23) X(24, 50, P, a24) X(25, 50, P, a25) X(26, 50, P, a26) X(27, 50, P, a27) X(28, 50, P, a28) X(29, 50, P, a29) X(30, 50, P, a30) X(31, 50, P, a31) X(32, 50, P, a32) X(33, 50, P, a33) X(34, 50, P, a34) X(35, 50, P, a35) X(36, 50, P, a36) X(37, 50, P, a37) X(38, 50, P, a38) X(39, 50, P, a39) X(40, 50, P, a40) X(41, 50, P, a41) X(42, 50, P, a42) X(43, 50, P, a43) X(44, 50, P, a44) X(45, 50, P, a45) X(46, 50, P, a46) X(47, 50, P, a47) X(48, 50, P, a48) X(49, 50, P, a49)
#define _DD_XLIST_EX_53(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23, a24, a25, a26, a27, a28, a29, a30, a31, a32, a33, a34, a35, a36, a37, a38, a39, a40, a41, a42, a43, a44, a45, a46, a47, a48, a49, a50) X(0, 51, P, a0) X(1, 51, P, a1) X(2, 51, P, a2) X(3, 51, P, a3) X(4, 51, P, a4) X(5, 51, P, a5) X(6, 51, P, a6) X(7, 51, P, a7) X(8, 51, P, a8) X(9, 51, P, a9) X(10, 51, P, a10) X(11, 51, P, a11) X(12, 51, P, a12) X(13, 51, P, a13) X(14, 51, P, a14) X(15, 51, P, a15) X(16, 51, P, a16) X(17, 51, P, a17) X(18, 51, P, a18) X(19, 51, P, a19) X(20, 51, P, a20) X(21, 51, P, a21) X(22, 51, P, a22) X(23, 51, P, a23) X(24, 51, P, a24) X(25, 51, P, a25) X(26, 51, P, a26) X(27, 51, P, a27) X(28, 51, P, a28) X(29, 51, P, a29) X(30, 51, P, a30) X(31, 51, P, a31) X(32, 51, P, a32) X(33, 51, P, a33) X(34, 51, P, a34) X(35, 51, P, a35) X(36, 51, P, a36) X(37, 51, P, a37) X(38, 51, P, a38) X(39, 51, P, a39) X(40, 51, P, a40) X(41, 51, P, a41) X(42, 51, P, a42) X(43, 51, P, a43) X(44, 51, P, a44) X(45, 51, P, a45) X(46, 51, P, a46) X(47, 51, P, a47) X(48, 51, P, a48) X(49, 51, P, a49) X(50, 51, P, a50)
#define _DD_XLIST_EX_54(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23, a24, a25, a26, a27, a28, a29, a30, a31, a32, a33, a34, a35, a36, a37, a38, a39, a40, a41, a42, a43, a44, a45, a46, a47, a48, a49, a50, a51) X(0, 52, P, a0) X(1, 52, P, a1) X(2, 52, P, a2) X(3, 52, P, a3) X(4, 52, P, a4) X(5, 52, P, a5) X(6, 52, P, a6) X(7, 52, P, a7) X(8, 52, P, a8) X(9, 52, P, a9) X(10, 52, P, a10) X(11, 52, P, a11) X(12, 52, P, a12) X(13, 52, P, a13) X(14, 52, P, a14) X(15, 52, P, a15) X(16, 52, P, a16) X(17, 52, P, a17) X(18, 52, P, a18) X(19, 52, P, a19) X(20, 52, P, a20) X(21, 52, P, a21) X(22, 52, P, a22) X(23, 52, P, a23) X(24, 52, P, a24) X(25, 52, P, a25) X(26, 52, P, a26) X(27, 52, P, a27) X(28, 52, P, a28) X(29, 52, P, a29) X(30, 52, P, a30) X(31, 52, P, a31) X(32, 52, P, a32) X(33, 52, P, a33) X(34, 52, P, a34) X(35, 52, P, a35) X(36, 52, P, a36) X(37, 52, P, a37) X(38, 52, P, a38) X(39, 52, P, a39) X(40, 52, P, a40) X(41, 52, P, a41) X(42, 52, P, a42) X(43, 52, P, a43) X(44, 52, P, a44) X(45, 52, P, a45) X(46, 52, P, a46) X(47, 52, P, a47) X(48, 52, P, a48) X(49, 52, P, a49) X(50, 52, P, a50) X(51, 52, P, a51)
#define _DD_XLIST_EX_55(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23, a24, a25, a26, a27, a28, a29, a30, a31, a32, a33, a34, a35, a36, a37, a38, a39, a40, a41, a42, a43, a44, a45, a46, a47, a48, a49, a50, a51, a52) X(0, 53, P, a0) X(1, 53, P, a1) X(2, 53, P, a2) X(3, 53, P, a3) X(4, 53, P, a4) X(5, 53, P, a5) X(6, 53, P, a6) X(7, 53, P, a7) X(8, 53, P, a8) X(9, 53, P, a9) X(10, 53, P, a10) X(11, 53, P, a11) X(12, 53, P, a12) X(13, 53, P, a13) X(14, 53, P, a14) X(15, 53, P, a15) X(16, 53, P, a16) X(17, 53, P, a17) X(18, 53, P, a18) X(19, 53, P, a19) X(20, 53, P, a20) X(21, 53, P, a21) X(22, 53, P, a22) X(23, 53, P, a23) X(24, 53, P, a24) X(25, 53, P, a25) X(26, 53, P, a26) X(27, 53, P, a27) X(28, 53, P, a28) X(29, 53, P, a29) X(30, 53, P, a30) X(31, 53, P, a31) X(32, 53, P, a32) X(33, 53, P, a33) X(34, 53, P, a34) X(35, 53, P, a35) X(36, 53, P, a36) X(37, 53, P, a37) X(38, 53, P, a38) X(39, 53, P, a39) X(40, 53, P, a40) X(41, 53, P, a41) X(42, 53, P, a42) X(43, 53, P, a43) X(44, 53, P, a44) X(45, 53, P, a45) X(46, 53, P, a46) X(47, 53, P, a47) X(48, 53, P, a48) X(49, 53, P, a49) X(50, 53, P, a50) X(51, 53, P, a51) X(52, 53, P, a52)
#define _DD_XLIST_EX_56(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23, a24, a25, a26, a27, a28, a29, a30, a31, a32, a33, a34, a35, a36, a37, a38, a39, a40, a41, a42, a43, a44, a45, a46, a47, a48, a49, a50, a51, a52, a53) X(0, 54, P, a0) X(1, 54, P, a1) X(2, 54, P, a2) X(3, 54, P, a3) X(4, 54, P, a4) X(5, 54, P, a5) X(6, 54, P, a6) X(7, 54, P, a7) X(8, 54, P, a8) X(9, 54, P, a9) X(10, 54, P, a10) X(11, 54, P, a11) X(12, 54, P, a12) X(13, 54, P, a13) X(14, 54, P, a14) X(15, 54, P, a15) X(16, 54, P, a16) X(17, 54, P, a17) X(18, 54, P, a18) X(19, 54, P, a19) X(20, 54, P, a20) X(21, 54, P, a21) X(22, 54, P, a22) X(23, 54, P, a23) X(24, 54, P, a24) X(25, 54, P, a25) X(26, 54, P, a26) X(27, 54, P, a27) X(28, 54, P, a28) X(29, 54, P, a29) X(30, 54, P, a30) X(31, 54, P, a31) X(32, 54, P, a32) X(33, 54, P, a33) X(34, 54, P, a34) X(35, 54, P, a35) X(36, 54, P, a36) X(37, 54, P, a37) X(38, 54, P, a38) X(39, 54, P, a39) X(40, 54, P, a40) X(41, 54, P, a41) X(42, 54, P, a42) X(43, 54, P, a43) X(44, 54, P, a44) X(45, 54, P, a45) X(46, 54, P, a46) X(47, 54, P, a47) X(48, 54, P, a48) X(49, 54, P, a49) X(50, 54, P, a50) X(51, 54, P, a51) X(52, 54, P, a52) X(53, 54, P, a53)
#define _DD_XLIST_EX_57(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23, a24, a25, a26, a27, a28, a29, a30, a31, a32, a33, a34, a35, a36, a37, a38, a39, a40, a41, a42, a43, a44, a45, a46, a47, a48, a49, a50, a51, a52, a53, a54) X(0, 55, P, a0) X(1, 55, P, a1) X(2, 55, P, a2) X(3, 55, P, a3) X(4, 55, P, a4) X(5, 55, P, a5) X(6, 55, P, a6) X(7, 55, P, a7) X(8, 55, P, a8) X(9, 55, P, a9) X(10, 55, P, a10) X(11, 55, P, a11) X(12, 55, P, a12) X(13, 55, P, a13) X(14, 55, P, a14) X(15, 55, P, a15) X(16, 55, P, a16) X(17, 55, P, a17) X(18, 55, P, a18) X(19, 55, P, a19) X(20, 55, P, a20) X(21, 55, P, a21) X(22, 55, P, a22) X(23, 55, P, a23) X(24, 55, P, a24) X(25, 55, P, a25) X(26, 55, P, a26) X(27, 55, P, a27) X(28, 55, P, a28) X(29, 55, P, a29) X(30, 55, P, a30) X(31, 55, P, a31) X(32, 55, P, a32) X(33, 55, P, a33) X(34, 55, P, a34) X(35, 55, P, a35) X(36, 55, P, a36) X(37, 55, P, a37) X(38, 55, P, a38) X(39, 55, P, a39) X(40, 55, P, a40) X(41, 55, P, a41) X(42, 55, P, a42) X(43, 55, P, a43) X(44, 55, P, a44) X(45, 55, P, a45) X(46, 55, P, a46) X(47, 55, P, a47) X(48, 55, P, a48) X(49, 55, P, a49) X(50, 55, P, a50) X(51, 55, P, a51) X(52, 55, P, a52) X(53, 55, P, a53) X(54, 55, P, a54)
#define _DD_XLIST_EX_58(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23, a24, a25, a26, a27, a28, a29, a30, a31, a32, a33, a34, a35, a36, a37, a38, a39, a40, a41, a42, a43, a44, a45, a46, a47, a48, a49, a50, a51, a52, a53, a54, a55) X(0, 56, P, a0) X(1, 56, P, a1) X(2, 56, P, a2) X(3, 56, P, a3) X(4, 56, P, a4) X(5, 56, P, a5) X(6, 56, P, a6) X(7, 56, P, a7) X(8, 56, P, a8) X(9, 56, P, a9) X(10, 56, P, a10) X(11, 56, P, a11) X(12, 56, P, a12) X(13, 56, P, a13) X(14, 56, P, a14) X(15, 56, P, a15) X(16, 56, P, a16) X(17, 56, P, a17) X(18, 56, P, a18) X(19, 56, P, a19) X(20, 56, P, a20) X(21, 56, P, a21) X(22, 56, P, a22) X(23, 56, P, a23) X(24, 56, P, a24) X(25, 56, P, a25) X(26, 56, P, a26) X(27, 56, P, a27) X(28, 56, P, a28) X(29, 56, P, a29) X(30, 56, P, a30) X(31, 56, P, a31) X(32, 56, P, a32) X(33, 56, P, a33) X(34, 56, P, a34) X(35, 56, P, a35) X(36, 56, P, a36) X(37, 56, P, a37) X(38, 56, P, a38) X(39, 56, P, a39) X(40, 56, P, a40) X(41, 56, P, a41) X(42, 56, P, a42) X(43, 56, P, a43) X(44, 56, P, a44) X(45, 56, P, a45) X(46, 56, P, a46) X(47, 56, P, a47) X(48, 56, P, a48) X(49, 56, P, a49) X(50, 56, P, a50) X(51, 56, P, a51) X(52, 56, P, a52) X(53, 56, P, a53) X(54, 56, P, a54) X(55, 56, P, a55)
#define _DD_XLIST_EX_59(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23, a24, a25, a26, a27, a28, a29, a30, a31, a32, a33, a34, a35, a36, a37, a38, a39, a40, a41, a42, a43, a44, a45, a46, a47, a48, a49, a50, a51, a52, a53, a54, a55, a56) X(0, 57, P, a0) X(1, 57, P, a1) X(2, 57, P, a2) X(3, 57, P, a3) X(4, 57, P, a4) X(5, 57, P, a5) X(6, 57, P, a6) X(7, 57, P, a7) X(8, 57, P, a8) X(9, 57, P, a9) X(10, 57, P, a10) X(11, 57, P, a11) X(12, 57, P, a12) X(13, 57, P, a13) X(14, 57, P, a14) X(15, 57, P, a15) X(16, 57, P, a16) X(17, 57, P, a17) X(18, 57, P, a18) X(19, 57, P, a19) X(20, 57, P, a20) X(21, 57, P, a21) X(22, 57, P, a22) X(23, 57, P, a23) X(24, 57, P, a24) X(25, 57, P, a25) X(26, 57, P, a26) X(27, 57, P, a27) X(28, 57, P, a28) X(29, 57, P, a29) X(30, 57, P, a30) X(31, 57, P, a31) X(32, 57, P, a32) X(33, 57, P, a33) X(34, 57, P, a34) X(35, 57, P, a35) X(36, 57, P, a36) X(37, 57, P, a37) X(38, 57, P, a38) X(39, 57, P, a39) X(40, 57, P, a40) X(41, 57, P, a41) X(42, 57, P, a42) X(43, 57, P, a43) X(44, 57, P, a44) X(45, 57, P, a45) X(46, 57, P, a46) X(47, 57, P, a47) X(48, 57, P, a48) X(49, 57, P, a49) X(50, 57, P, a50) X(51, 57, P, a51) X(52, 57, P, a52) X(53, 57, P, a53) X(54, 57, P, a54) X(55, 57, P, a55) X(56, 57, P, a56)
#define _DD_XLIST_EX_60(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23, a24, a25, a26, a27, a28, a29, a30, a31, a32, a33, a34, a35, a36, a37, a38, a39, a40, a41, a42, a43, a44, a45, a46, a47, a48, a49, a50, a51, a52, a53, a54, a55, a56, a57) X(0, 58, P, a0) X(1, 58, P, a1) X(2, 58, P, a2) X(3, 58, P, a3) X(4, 58, P, a4) X(5, 58, P, a5) X(6, 58, P, a6) X(7, 58, P, a7) X(8, 58, P, a8) X(9, 58, P, a9) X(10, 58, P, a10) X(11, 58, P, a11) X(12, 58, P, a12) X(13, 58, P, a13) X(14, 58, P, a14) X(15, 58, P, a15) X(16, 58, P, a16) X(17, 58, P, a17) X(18, 58, P, a18) X(19, 58, P, a19) X(20, 58, P, a20) X(21, 58, P, a21) X(22, 58, P, a22) X(23, 58, P, a23) X(24, 58, P, a24) X(25, 58, P, a25) X(26, 58, P, a26) X(27, 58, P, a27) X(28, 58, P, a28) X(29, 58, P, a29) X(30, 58, P, a30) X(31, 58, P, a31) X(32, 58, P, a32) X(33, 58, P, a33) X(34, 58, P, a34) X(35, 58, P, a35) X(36, 58, P, a36) X(37, 58, P, a37) X(38, 58, P, a38) X(39, 58, P, a39) X(40, 58, P, a40) X(41, 58, P, a41) X(42, 58, P, a42) X(43, 58, P, a43) X(44, 58, P, a44) X(45, 58, P, a45) X(46, 58, P, a46) X(47, 58, P, a47) X(48, 58, P, a48) X(49, 58, P, a49) X(50, 58, P, a50) X(51, 58, P, a51) X(52, 58, P, a52) X(53, 58, P, a53) X(54, 58, P, a54) X(55, 58, P, a55) X(56, 58, P, a56) X(57, 58, P, a57)
#define _DD_XLIST_EX_61(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23, a24, a25, a26, a27, a28, a29, a30, a31, a32, a33, a34, a35, a36, a37, a38, a39, a40, a41, a42, a43, a44, a45, a46, a47, a48, a49, a50, a51, a52, a53, a54, a55, a56, a57, a58) X(0, 59, P, a0) X(1, 59, P, a1) X(2, 59, P, a2) X(3, 59, P, a3) X(4, 59, P, a4) X(5, 59, P, a5) X(6, 59, P, a6) X(7, 59, P, a7) X(8, 59, P, a8) X(9, 59, P, a9) X(10, 59, P, a10) X(11, 59, P, a11) X(12, 59, P, a12) X(13, 59, P, a13) X(14, 59, P, a14) X(15, 59, P, a15) X(16, 59, P, a16) X(17, 59, P, a17) X(18, 59, P, a18) X(19, 59, P, a19) X(20, 59, P, a20) X(21, 59, P, a21) X(22, 59, P, a22) X(23, 59, P, a23) X(24, 59, P, a24) X(25, 59, P, a25) X(26, 59, P, a26) X(27, 59, P, a27) X(28, 59, P, a28) X(29, 59, P, a29) X(30, 59, P, a30) X(31, 59, P, a31) X(32, 59, P, a32) X(33, 59, P, a33) X(34, 59, P, a34) X(35, 59, P, a35) X(36, 59, P, a36) X(37, 59, P, a37) X(38, 59, P, a38) X(39, 59, P, a39) X(40, 59, P, a40) X(41, 59, P, a41) X(42, 59, P, a42) X(43, 59, P, a43) X(44, 59, P, a44) X(45, 59, P, a45) X(46, 59, P, a46) X(47, 59, P, a47) X(48, 59, P, a48) X(49, 59, P, a49) X(50, 59, P, a50) X(51, 59, P, a51) X(52, 59, P, a52) X(53, 59, P, a53) X(54, 59, P, a54) X(55, 59, P, a55) X(56, 59, P, a56) X(57, 59, P, a57) X(58, 59, P, a58)
#define _DD_XLIST_EX_62(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23, a24, a25, a26, a27, a28, a29, a30, a31, a32, a33, a34, a35, a36, a37, a38, a39, a40, a41, a42, a43, a44, a45, a46, a47, a48, a49, a50, a51, a52, a53, a54, a55, a56, a57, a58, a59) X(0, 60, P, a0) X(1, 60, P, a1) X(2, 60, P, a2) X(3, 60, P, a3) X(4, 60, P, a4) X(5, 60, P, a5) X(6, 60, P, a6) X(7, 60, P, a7) X(8, 60, P, a8) X(9, 60, P, a9) X(10, 60, P, a10) X(11, 60, P, a11) X(12, 60, P, a12) X(13, 60, P, a13) X(14, 60, P, a14) X(15, 60, P, a15) X(16, 60, P, a16) X(17, 60, P, a17) X(18, 60, P, a18) X(19, 60, P, a19) X(20, 60, P, a20) X(21, 60, P, a21) X(22, 60, P, a22) X(23, 60, P, a23) X(24, 60, P, a24) X(25, 60, P, a25) X(26, 60, P, a26) X(27, 60, P, a27) X(28, 60, P, a28) X(29, 60, P, a29) X(30, 60, P, a30) X(31, 60, P, a31) X(32, 60, P, a32) X(33, 60, P, a33) X(34, 60, P, a34) X(35, 60, P, a35) X(36, 60, P, a36) X(37, 60, P, a37) X(38, 60, P, a38) X(39, 60, P, a39) X(40, 60, P, a40) X(41, 60, P, a41) X(42, 60, P, a42) X(43, 60, P, a43) X(44, 60, P, a44) X(45, 60, P, a45) X(46, 60, P, a46) X(47, 60, P, a47) X(48, 60, P, a48) X(49, 60, P, a49) X(50, 60, P, a50) X(51, 60, P, a51) X(52, 60, P, a52) X(53, 60, P, a53) X(54, 60, P, a54) X(55, 60, P, a55) X(56, 60, P, a56) X(57, 60, P, a57) X(58, 60, P, a58) X(59, 60, P, a59)
#define _DD_XLIST_EX_63(X, P, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23, a24, a25, a26, a27, a28, a29, a30, a31, a32, a33, a34, a35, a36, a37, a38, a39, a40, a41, a42, a43, a44, a45, a46, a47, a48, a49, a50, a51, a52, a53, a54, a55, a56, a57, a58, a59, a60) X(0, 61, P, a0) X(1, 61, P, a1) X(2, 61, P, a2) X(3, 61, P, a3) X(4, 61, P, a4) X(5, 61, P, a5) X(6, 61, P, a6) X(7, 61, P, a7) X(8, 61, P, a8) X(9, 61, P, a9) X(10, 61, P, a10) X(11, 61, P, a11) X(12, 61, P, a12) X(13, 61, P, a13) X(14, 61, P, a14) X(15, 61, P, a15) X(16, 61, P, a16) X(17, 61, P, a17) X(18, 61, P, a18) X(19, 61, P, a19) X(20, 61, P, a20) X(21, 61, P, a21) X(22, 61, P, a22) X(23, 61, P, a23) X(24, 61, P, a24) X(25, 61, P, a25) X(26, 61, P, a26) X(27, 61, P, a27) X(28, 61, P, a28) X(29, 61, P, a29) X(30, 61, P, a30) X(31, 61, P, a31) X(32, 61, P, a32) X(33, 61, P, a33) X(34, 61, P, a34) X(35, 61, P, a35) X(36, 61, P, a36) X(37, 61, P, a37) X(38, 61, P, a38) X(39, 61, P, a39) X(40, 61, P, a40) X(41, 61, P, a41) X(42, 61, P, a42) X(43, 61, P, a43) X(44, 61, P, a44) X(45, 61, P, a45) X(46, 61, P, a46) X(47, 61, P, a47) X(48, 61, P, a48) X(49, 61, P, a49) X(50, 61, P, a50) X(51, 61, P, a51) X(52, 61, P, a52) X(53, 61, P, a53) X(54, 61, P, a54) X(55, 61, P, a55) X(56, 61, P, a56) X(57, 61, P, a57) X(58, 61, P, a58) X(59, 61, P, a59) X(60, 61, P, a60)
/** Helper macro converting DD_XLIST_EX(...) into DD_XLIST(...). */
#define _DD_XLIST_UNWRAP(index, count, param, arg) param(arg)
