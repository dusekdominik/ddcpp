#pragma once
namespace DD {
	/**
	 * System dialogs for loading/saving files.
	 */
	struct Dialog;
}

#include <DD/Array.h>
#include <DD/FileSystem/Path.h>
#include <DD/String.h>

namespace DD {
	struct Dialog {
	public: // nested
		/** Descriptions of file extension. */
		struct Extension;
	private: // static methods
		/** Helper method for parsing extensions. */
		static void parseExtension(const Extension & extension, wchar_t * wxtBuffer, size_t & offset);
		/** Helper method for parsing extensions. */
		static void parseExtensions(const Array<Extension> & extensions, wchar_t * extBuffer);

	public: // static methods
		/**
		 * Open file dialog, returns empty string if dialog fails.
		 * @param extensions - array of extension which can be opened.
		 * @return array of paths
		 */
		static Array<FileSystem::Path> OpenFiles(const Array<Extension> & extension);
		/**
		 * Open file dialog, returns empty string if dialog fails.
		 * @param extension - array of extension which can be opened.
		 * @example common usage
		 *   for (const DD::FileSystem::Path & path : DD::Dialog::OpenFiles(DD::Dialog::XML))
		 *     DD::Array<DD::i8> data = path.File().Read();
		 */
		static Array<FileSystem::Path> OpenFiles(const Extension & extension);
		/**
		 * Open file dialog, returns empty string if dialog fails.
		 * @param extensions - array of extension which can be opened.
		 */
		static FileSystem::Path OpenFile(const Array<Extension> & extensions = Array<Extension>());
		/**
		 * Open file dialog, returns empty string if dialog fails.
		 * @param extension - an extension which can be opened.
		 */
		static FileSystem::Path OpenFile(const Extension & extension);
		/**
		 * Save file dialog, returns empty string if dialog fails.
		 * @example
		 *   const DD::FileSystem::Path & file = DD::Dialog::SaveFile(DD::Dialog::XML);
		 *   if (path.IsFile())
		 *    path.File().Write(buffer, size);
		 */
		static FileSystem::Path SaveFile(const Extension & extension);
		/**
		 * Open dialog for selecting a directory.
		 * @example common usage
		 *  const DD::FileSystem::Path & dir = DD::Dialog::OpenDir();
		 *  if (dir.IsDirectory()) for (DD::FileSystem::File & file : dir.Directory().Open().Files())
		 *    doSomething(file);
		 */
		static FileSystem::Path OpenDir();
	};

	struct Dialog::Extension {
	public: // statics
		/** *.* */
		static const Extension ANY;
		/** *.las */
		static const Extension LAS;
		/** *.png */
		static const Extension PNG;
		/** *.xml */
		static const Extension XML;

	public: // properties
		/** Description of extension. */
		StringLocal<32> Title;
		/** Extension itself. */
		StringLocal<4> Definition;
	};
}
