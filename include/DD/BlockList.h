#pragma once
namespace DD {
	/**
	 * Simple array list with block allocation.
	 * List is not allocated in the exponential way as common array list.
	 * This list always allocates one block more when run out of storage.
	 * The benefit is that we do need to copy data from old storage to the new one,
	 *   while new allocation and the data addresses are still the same.
	 * The drawback is that scaling of list on programmer's side via param BLOCK_LENGTH.
	 *
	 * @param T - type that should be stored in the list.
	 * @param BLOCK_LENGTH - number of items stored in one block.
	 */
	template<typename T, size_t BLOCK_LENGTH>
	struct BlockList;

	/**
	 * Block list with locally allocated block.
	 * The first block is always allocated locally,
	 *   otherwise the list behaves the same as @see BlockList<T, N>.
	 *
	 * @param T - type that should be stored in the list.
	 * @param BLOCK_LENGTH - number of items stored in one block.
	 */
	template<typename T, size_t BLOCK_LENGTH>
	struct BlockListLocal;
}

#include <DD/DebugLite.h>
#include <DD/List.h>

#define _DD_BLOCKLIST_ASSERT(condition) DD_LITE_DEBUG_ASSERT(condition, "BlockList indexing failed.")

namespace DD {
	template<typename T, size_t BLOCK_LENGTH>
	struct BlockList {
	public: // nested
		/** Random access iterator implementation. */
		template<typename TYPE>
		struct IteratorT {
			/** Major iterator over the blocks. */
			TYPE ** BlockIterator;
			/** Offset of items in just one block. */
			size_t BlockOffset;

			/** Inequality operator. */
			bool operator!=(const IteratorT & rhs) const { return BlockIterator != rhs.BlockIterator || BlockOffset != rhs.BlockOffset; }
			/** Equality operator. */
			bool operator==(const IteratorT & rhs) const { return BlockIterator == rhs.BlockIterator && BlockOffset == rhs.BlockOffset; }
			/** Comparison operator. */
			bool operator<(const IteratorT & rhs) const { return BlockIterator < rhs.BlockIterator || (BlockIterator == rhs.BlockIterator && BlockOffset < rhs.BlockOffset); }
			/** Fwd operator. */
			IteratorT & operator++() { if (++BlockOffset == BLOCK_LENGTH) ++BlockIterator, BlockOffset = 0; return *this; }
			/** Fwd operator. */
			IteratorT operator++(int) { IteratorT tmp(*this); ++(*this); return tmp; }
			/** Backward operator.*/
			IteratorT & operator--() { if (BlockOffset-- == 0) BlockOffset = BLOCK_LENGTH - 1, --BlockIterator; return *this; }
			/** Random access operator. */
			IteratorT & operator+=(size_t offset) { BlockOffset += offset; BlockIterator += BlockOffset / BLOCK_LENGTH; BlockOffset %= BLOCK_LENGTH; return *this; }
			/** Random access operator. */
			IteratorT operator+(size_t index) { return IteratorT(*this) += index; }
			/** Random access operator. */
			IteratorT & operator-=(size_t offset);
			/** Random access operator. */
			IteratorT operator-(size_t index) { return IteratorT(*this) -= index; }
			/** Difference of items between two iterators. */
			ptrdiff_t operator-(const IteratorT & rhs) { return (BlockIterator - rhs.BlockIterator) * BLOCK_LENGTH + BlockOffset - rhs.BlockOffset; }
			/** Dereference operator. */
			TYPE & operator*() { return (*BlockIterator)[BlockOffset]; }
		};
		/** Iterator. */
		using Iterator = IteratorT<T>;
		/** Iterator. */
		using ConstIterator = IteratorT<const T>;
		/** An array. */
		using BlockNative = T[BLOCK_LENGTH];

	protected: // properties
		/** Number of items in the collection. */
		size_t _length = 0;
		/** List with pointers to the storage blocks. */
		List<T*, 2> _data;

	protected: // methods
		/** Move items to the end of the list. */
		constexpr void shiftRight(size_t since, size_t to, size_t offset) { size_t i = to - 1; while (i-- > since) At(i + offset) = Fwd(At(i)); }
		/** Move items from the end of the list. */
		constexpr void shiftLeft(size_t since, size_t to, size_t offset) { for (; since < to; ++since) At(since) = Fwd(At(since + offset)); }

	public: // accession
		/** Indexer. */
		constexpr T & At(size_t index) { _DD_BLOCKLIST_ASSERT(index < _length); return (_data[index / BLOCK_LENGTH])[index % BLOCK_LENGTH]; }
		/** Indexer. */
		constexpr const T & At(size_t index) const { _DD_BLOCKLIST_ASSERT(index < _length); return _data[index / BLOCK_LENGTH][index % BLOCK_LENGTH]; }
		/** First item in the collection. */
		constexpr T & First() { return At(0); }
		/** First item in the collection. */
		constexpr const T & First() const { return At(0); }
		/** First item in the collection. */
		constexpr T & First(size_t index) { return At(index); }
		/** First item in the collection. */
		constexpr const T & First(size_t index) const { return At(index); }
		/** Last item in the collection. */
		constexpr T & Last() { return At(_length - 1); }
		/** Last item in the collection. */
		constexpr const T & Last() const { return At(_length - 1); }
		/** Last item in the collection. */
		constexpr T & Last(size_t index) { return At(_length - 1 - index); }
		/** Last item in the collection. */
		constexpr const T & Last(size_t index) const { return At(_length - 1 - index); }
		/** Iterator. */
		constexpr Iterator begin() { return { _data.begin(), 0 }; }
		/** Iterator. */
		constexpr Iterator end() { return { _data.begin() + _length / BLOCK_LENGTH, _length % BLOCK_LENGTH }; }
		/** Iterator. */
		constexpr ConstIterator begin() const { return { _data.begin(), 0 }; }
		/** Iterator. */
		constexpr ConstIterator end() const { return { _data.begin() + _length / BLOCK_LENGTH, _length % BLOCK_LENGTH }; }

	public: // info
		/** Number items stored in the collection. */
		constexpr size_t Length() const { return _length; }
		/** Total number of items that is possible to stored in the collection. */
		constexpr size_t Capacity() const { return _data.Size() * BLOCK_LENGTH; }
		/** Number of items that can be stored in the collection. */
		constexpr size_t FreeSlots() const { return Capacity() - _length; }

	public: // insertion, removing
		/** Append data without checking the allocation. */
		constexpr T & AppendUnsafe() { return At(_length++); }
		/** Append data. */
		constexpr T & Append() { return Reserve().AppendUnsafe(); }
		/** Insert data at given index. */
		constexpr T & InsertUnsafe(size_t index) { shiftRight(index, _length++, 1); return At(index); }
		/** Insert data at given index. */
		constexpr T & Insert(size_t index) { return Reserve().InsertUnsafe(index); }
		/** Remove item from the end. */
		constexpr void RemoveLast() { Last().~T(); --_length; }
		/** Remove item of the given index. */
		constexpr void RemoveAt(size_t index) { At(index).~T(); shiftLeft(index, _length - 1, 1); --_length; }
		/**
		 * Remove item and replace by the end of collection.
		 * It is faster, because we do not need to shift the data.
		 * @warning After this operation, the ordering of the list items changes.
		 */
		constexpr void RemoveAtFast(size_t index) { At(index) = Fwd(Last()); RemoveLast(); }
		/** Check that it is possible to store given number of items in the collection. */
		constexpr BlockList & Reserve(size_t length = 1) { while (FreeSlots() < length) _data.Append(new BlockNative()); return *this; }

	public: // constructors
		/** Constructor.*/
		constexpr BlockList() = default;
		/** Destructor. */
		~BlockList() { for (T * item : _data) delete item; }
	};


	template<typename T, size_t BLOCK_LENGTH>
	struct BlockListLocal
		: public BlockList<T, BLOCK_LENGTH>
	{
	public: // nested
		/** Parent class. */
		using Base = BlockList<T, BLOCK_LENGTH>;

	protected: // properties
		/** The first block that is allocated locally. */
		Base::BlockNative _localAllocation;

	public: // constructors
		/** Constructor, put local allocation at the beginning of the list. */
		constexpr BlockListLocal() { Base::_data.Append(_localAllocation); }
		/** Destructor, remove local allocation from list. */
		~BlockListLocal() { Base::_data.RemoveAtFast(0); }
	};


	template<typename T, size_t BLOCK_LENGTH>
	template<typename TYPE>
	typename BlockList<T, BLOCK_LENGTH>::IteratorT<TYPE> & BlockList<T, BLOCK_LENGTH>::IteratorT<TYPE>::operator-=(
		size_t offset
	) {
		if (offset % BLOCK_LENGTH <= BlockOffset) {
			BlockOffset -= offset % BLOCK_LENGTH;
			BlockIterator -= offset / BLOCK_LENGTH;
		}
		else {
			BlockOffset = (BLOCK_LENGTH)-((offset - BlockOffset) % BLOCK_LENGTH);
			BlockIterator -= 1 + (offset / BLOCK_LENGTH);
		}

		return *this;
	}
}
