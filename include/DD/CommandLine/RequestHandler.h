#pragma once
namespace DD::CommandLine {
	/**
	 * Helper for adding callbacks to the program.
	 * Input of CommandLine::Program is considered as request.
	 * This objects combines commands and rules to parse the object.
	 */
	struct RequestHandler;
}

#include <DD/CommandLine/Program.h>
#include <DD/Text/LogVirtual.h>

namespace DD::CommandLine {
	struct RequestHandler {
	public: // nested
		/** Signature of the callback function. */
		typedef bool(*callback_t)(Text::LogVirtual8 &);
		/** Descriptor of request behavior. */
		enum struct Behaviors {
			NoCommandValid,        // valid if none of the given commands is valid
			AnyCommandValid,       // valid if any of the given commands is valid
			AllCommandsValid,      // valid if all of the given commands are valid
			SingleCommandValid,    // valid if just one of the given commands is valid
			NotSingleCommandValid, // invalid if single command is valid, valid otherwise
		};

	public: // properties
		/** How given commands are combined for validation. */
		Behaviors _behavior;
		/** Given commands. */
		List<Command *, 4> _commands;
		/** Pointer to function that should be called. */
		callback_t _callback;

	public: // methods
		/** Is request valid (with respect to set behavior). */
		bool Valid() const;
		/** Run handler if request is valid, @return true if request has been called. */
		bool TryCall(Text::LogVirtual8 & log);

	public: // constructors
		/** Internal constructor. */
		RequestHandler(callback_t callback, Behaviors behavior, Program * program);
		/** Constructor (behavior ~ AnyCommandValid). */
		template<typename...COMMANDS>
		RequestHandler(callback_t callback, COMMANDS...cmds)
			: RequestHandler(callback, Behaviors::AnyCommandValid, cmds...) {
		}
		/** Constructor with custom behavior. */
		template<typename...COMMANDS>
		RequestHandler(callback_t cbk, Behaviors bhv, COMMANDS...cmds)
			: RequestHandler(cbk, bhv, Program::LastInstance())
		{
			_commands.Add(cmds...);
		}
	};
}
