#pragma once
namespace DD::CommandLine {
}

#include <DD/CommandLine/ArgumentOption.h>
#include <DD/CommandLine/Assert.h>
#include <DD/CommandLine/Hash.h>
#include <DD/CommandLine/Versionable.h>
#include <DD/Text/StringView.h>

namespace DD::CommandLine {
	struct CommonArgumentOptions {
		/** Lower bound of option. */
		Versionable<double> Min;
		/** Upper bound of option. */
		Versionable<double> Max;
		/** Flag if value is mandatory. */
		Versionable<bool> Mandatory;

		/** Set an opt. */
		void Set(ArgumentOption opt);

		/** Set all opts from opt string. */
		CommonArgumentOptions(Text::StringView8 options) { for (ArgumentOption option : ArgumentOption::Text2Options(options)) Set(option); }
	};
}

namespace DD::CommandLine {
	inline void CommonArgumentOptions::Set(ArgumentOption opt) {
		switch (CommandLine::HashCI(opt.Name())) {
			case CommandLine::HashCI("min"):       Min = opt.ValueAs<double>(0.0);       break;
			case CommandLine::HashCI("max"):       Max = opt.ValueAs<double>(0.0);       break;
			case CommandLine::HashCI("mandatory"): Mandatory = opt.ValueAs<bool>(false); break;
			default: DD_COMMANDLINE_WARNING("Undefined option of command-line argument."); break;
		}
	}
}
