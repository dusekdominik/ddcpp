#pragma once
namespace DD::CommandLine {
	/** Helper for outreading format 'Name=Value'. */
	struct ArgumentOption;
}

#include <DD/CommandLine/Versionable.h>
#include <DD/Sequencer/Driver.h>
#include <DD/Text/StringView.h>

namespace DD::CommandLine {
	struct ArgumentOption {
		/** */
		static auto Text2Options(Text::StringView8 options) { return Sequencer::Drive(options.SplitSkipEmpty('&')).Cast<ArgumentOption>(); }
		/** */
		static Text::StringView8 ExtractOption(Text::StringView8 options, Text::StringView8 name) {
			return Text2Options(options)
				.Filter([&](ArgumentOption o) { return o.Name().EqualsInsensitive(name); })
				.Select([](ArgumentOption o) { return o.Value(); })
				.FirstOrDefault(nullptr);
		}

		/** Option text. */
		Text::StringView8 Option;

		/** Name of the option. */
		Text::StringView8 Name() const { return Text::StringView8(Option).TrimLast<&Text::IsChar<char, '='>, false>().RemoveLast(1).Trim(); }
		/** Value of the option. */
		Text::StringView8 Value() const { return Text::StringView8(Option).TrimFirst<&Text::IsChar<char, '='>, false>().RemoveFirst(1).Trim(); }
		/** Cast value as a type. */
		template<typename T>
		T ValueAs(const T & defVal) const { T target; return Value().TryCast(target) ? target : defVal; }
	};
}
