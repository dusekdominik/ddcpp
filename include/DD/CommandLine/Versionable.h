#pragma once
namespace DD::CommandLine {
	/** Allow user to observe if value has been set. */
	template<typename TYPE>
	struct Versionable {
		/** Native value */
		TYPE _value;
		/** Has been value alre4ady set. */
		size_t _version = 0;

		/** Has been already set. */
		bool IsSet() const { return _version != 0; }
		/** Is of the given version. */
		bool IsVersion(size_t version) const { return _version == version; }
		/** Is of the given version. */
		template<typename T>
		bool IsVersion(const Versionable<T>& rhs) const { return _version == rhs.Version(); }
		/** Get native value. */
		const TYPE & Get() const { return _value; }
		/** Get current version */
		size_t Version() const { return _version; }
		/** Set value and version. */
		void Set(const TYPE & value, size_t version) { _value = value, _version = version; }
		/** Set value and version. */
		void Set(const TYPE & value) { Set(value, _version + 1); }

		/** Assign native type. */
		Versionable & operator=(const TYPE & value) { Set(value); return *this; }
		/** Return native type. */
		operator const TYPE & () const { return Get(); }
	};
}
