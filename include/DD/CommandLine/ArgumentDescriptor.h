#pragma once
namespace DD::CommandLine {
	/** Interface for arguments that are associated with commands. */
	struct ArgumentDescriptor;
}

#include <DD/Text/StringView.h>

namespace DD::CommandLine {
	struct ArgumentDescriptor {
		/** Parser type. */
		Text::StringView8 Type = nullptr;
		/** Storage for the argument name. */
		Text::StringView8 Name = nullptr;
		/** Storage for the argument description. */
		Text::StringView8 Brief = nullptr;
		/** Default value of argument */
		Text::StringView8 DefaultValue = nullptr;
		/** Additional options of argument (e.g. value bounds). */
		Text::StringView8 Options = nullptr;
	};
}
