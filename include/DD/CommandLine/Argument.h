#pragma once
namespace DD::CommandLine {
	/** Generic arguments for basic types. */
	template<typename T>
	struct ArgumentT;
}

#include <DD/Auxiliaries.h>
#include <DD/CommandLine/IArgument.h>

namespace DD::CommandLine {
	/** String argument. */
	using String = ArgumentT<::DD::String>;
	/** Argument for 64-bit floating point number. */
	using Double = ArgumentT<::DD::f64>;
	/** Argument for boolean flag. */
	using Boolean = ArgumentT<bool>;

	template<typename T>
	struct ArgumentT
		: IArgument
	{
		/** */
		static constexpr Text::StringView8 Type() {
			if constexpr (IsSame<f64, T>)
				return "double";
			if constexpr (IsSame<bool, T>)
				return "boolean";
			if constexpr (IsSame<::DD::String, T>)
				return "string";

			return nullptr;
		}

	public: // properties
		/** Parsed value. */
		mutable Versionable<T> _storedValue;

	public:
		void Update() const { if (!_storedValue.IsVersion(IArgument::_value)) _storedValue.Set(GetParsedText<T>(), IArgument::_value.Version()); }
		/** Check for the argument. */
		operator bool() const { return IsReady(); }
		/** Implicit conversion for the value. */
		operator const T() const { return Get(); }
		/** Explicitly get value of the param. */
		const T & Get() const { Update(); return _storedValue.Get(); }

	public:
		/**
		 * Constructor.
		 * @param name         - name of the argument.
		 * @param brief        - description of the argument.
		 * @param defaultValue - value of the argument if it has not been parsed.
		 */
		ArgumentT(Text::StringView8 name, Text::StringView8 brief, Text::StringView8 defaultValue = nullptr, Text::StringView8 options = nullptr, Command * command = nullptr)
			: IArgument({ Type(), name, brief, defaultValue, options }, command)
		{}
	};
}
