#pragma once
#include <DD/Text/StringView.h>

namespace DD::CommandLine {
	/** Default hash function for cmd line purposes - constexpr version. */
	constexpr u32 HashCI(Text::StringView8 text) { return text.Hash<u32, DD::Text::HashMethods::FirstLastCI>(); };
}
