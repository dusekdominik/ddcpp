#pragma once
#include <DD/MacrosLite.h>

#define DD_COMMANDLINE_ASSERT(condition, text)  DD_LITE_DEBUG_ASSERT(condition, text)
#define DD_COMMANDLINE_WARNING(text) DD_LITE_DEBUG_WARNING(text)
