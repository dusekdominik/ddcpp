#pragma once
namespace DD::CommandLine {
	/** Argument for parsing keyword. */
	struct Keyword;
}

#include <DD/CommandLine/Command.h>
#include <DD/CommandLine/Argument.h>
#include <DD/CommandLine/Program.h>

namespace DD::CommandLine {

	struct Keyword
		: public DD::CommandLine::IArgument
	{
		operator bool() const { return IsReady(); }
		operator bool() { return IsReady(); }
		/** Constructor. */
		Keyword(Text::StringView8 name, Text::StringView8 brief, Command * command = nullptr)
			: IArgument({ "keyword", name, brief, nullptr, nullptr }, command)
		{}
	};
}
