#pragma once
namespace DD::CommandLine {
	/** Enum argument. */
	struct Enum;
}

#include <DD/CommandLine/Argument.h>

namespace DD::CommandLine {
	struct Enum
		: public IArgument
	{
	protected: // properties
		/** Parsed value. */
		mutable Versionable<size_t> _storedValue;

	public:
		/** Get index in enum. */
		size_t Get() const { if (!_storedValue.IsSet()) _storedValue = GetParsedText<size_t>(); return _storedValue.Get(); }
		/** Get index in enum. */
		operator size_t() const { return Get(); }

	public: // constructors
		/**
		 * Constructor.
		 * @param items - "|" separated list of the keywords.
		 */
		Enum(Text::StringView8 name, Text::StringView8 brief, Text::StringView8 defaultValue, Text::StringView8 items, Command * command = nullptr)
			: IArgument({"enum", name, brief, defaultValue, items }, command)
		{}
	};
}
