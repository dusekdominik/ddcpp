#pragma once
namespace DD::CommandLine {
	/** Command - set of arguments. */
	struct Command;
}

#include <DD/CommandLine/IArgument.h>
#include <DD/CommandLine/Keyword.h>
#include <DD/CommandLine/Program.h>
#include <DD/List.h>
#include <DD/Sequencer/Driver.h>

namespace DD::CommandLine {
	struct Command {
		friend struct IArgument;
		friend struct Program;

	public: // statics
		/** Check if the command has been parsed incorrectly. */
		static bool Invalid(const Command * cmd) { return cmd->KeywordFound() && !cmd->IsReady(); }
		/** Check if the command has been parsed correctly. */
		static bool Valid(const Command * cmd) { return cmd->IsReady(); }

	protected: // properties
		/** Name and also keyword. */
		Text::StringView8 _name;
		/** Brief description of the command. */
		Text::StringView8 _brief;
		/** Keyword parser. */
		union {
			Keyword _keyword;
			char _keywordData[sizeof(Keyword)];
		};
		/** Arguments of the command. */
		List<IArgument *> _arguments;

	public: // methods
		/** Number of args. */
		ArrayView1D<IArgument * const> Args() const { return _arguments.ToView().SubView(1); }
		/** Name of the command (also keyword). */
		Text::StringView8 Name() const { return _name; }
		/** Brief description of the command. */
		Text::StringView8 Brief() const { return _brief; }
		/** If true, keyword has been found and all the arguments were parsed. */
		bool IsReady() const { return Sequencer::Drive(_arguments).All(&IArgument::Ready); }
		/**
		 * Has been keyword found.
		 * Note that this does not mean that all arguments of the keyword have been found.
		 */
		bool KeywordFound() const { return _keyword.IsReady(); }
		/** Reset parsing state. */
		void Reset();
		/**
		 * Try parse arguments of the command
		 * @param argc - number of arguments in the following param.
		 * @param argv - arguments for parser, it is expected that first argument is keyword.
		 * @return number of arguments accepted by command if succeeds, zero otherwise.
		 */
		size_t TryParse(size_t argc, const char ** argv);

	public: // operators
		/** Implicit conversion for boolean checks. */
		operator bool() { return IsReady(); }

	public: // constructors
		/** Constructor. */
		Command(Text::StringView8 name, Text::StringView8 brief, Program * manager = nullptr);
		/** Destructor. */
		~Command() { _keyword.~Keyword(); }
	};
}