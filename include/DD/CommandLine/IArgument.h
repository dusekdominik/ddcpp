#pragma once
namespace DD::CommandLine {
	/** Generic arguments for basic types. */
	struct IArgument;
}

#include <DD/CommandLine/ArgumentDescriptor.h>
#include <DD/CommandLine/IArgumentParser.h>
#include <DD/CommandLine/Command.h>
#include <DD/Text/StringView.h>
#include <DD/String.h>
#include <DD/List.h>

namespace DD::CommandLine {
	struct IArgument {
		/** Comparer for arguments that are ready. */
		static bool Ready(IArgument * a) { return a->IsReady(); }
		/** Use default or custom command */
		static Command * CommandCast(Command *);

		/** Parameters of the argument. */
		ArgumentDescriptor _descriptor;
		/** Argument value. */
		Versionable<DD::String> _value;
		/** Temporary buffer before 8-bit string impl. */
		mutable List<char, 128> _buffer;
		/** Was value set.  */
		bool _ready = false;

		/** Get argument properties. */
		const ArgumentDescriptor & Descriptor() const { return _descriptor; }
		/** Value has been set. */
		bool IsReady() const { return _ready; }
		/** Get parser. */
		IArgumentParser * Parser() const { return IArgumentParser::Parser(_descriptor.Type); }
		/** Reset values. */
		void Reset() { _ready = false; _value = DD::String(_descriptor.DefaultValue); }
		/** Text value of the argument. */
		Text::StringView8 Value() const { _buffer.Clear(); for (wchar_t w :_value.Get()) _buffer.Add((char)w); _buffer.Add(0); return Text::StringView8(_buffer.begin()); }
		/** Text value. */
		template<typename T>
		T GetParsedText() const { T target; Parser()->Text2Value(Descriptor(), Value(), &target); return target; }
		/** Try parser given value. */
		bool TryParse(Text::StringView8 text) { if (Parser() == nullptr || !(_ready=Parser()->Text2Value(_descriptor, text, nullptr))) return false; _value = text; return true; }
		/** Return value of option with given name. */
		Text::StringView8 ExtractOption(Text::StringView8 name) { return ArgumentOption::ExtractOption(_descriptor.Options, name); }

		/** Constructor. */
		IArgument(ArgumentDescriptor && descriptor, Command * command = nullptr);
	};
}
