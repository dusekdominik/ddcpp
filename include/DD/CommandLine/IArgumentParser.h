#pragma once
namespace DD::CommandLine {
	/** Abstract type of argument parsers. */
	struct IArgumentParser;

	/** Implementation of typed parsers. */
	template<typename T>
	struct ArgumentParser;
}

#include <DD/CommandLine/ArgumentDescriptor.h>
#include <DD/CommandLine/Assert.h>
#include <DD/CommandLine/Hash.h>
#include <DD/CommandLine/CommonArgumentOptions.h>
#include <DD/Math/Functions.h>
#include <DD/Text/StringView.h>
#include <DD/String.h>
#include <DD/Types.h>

namespace DD::CommandLine {
	struct IArgumentParser {
		/** Static method, get parsor according to parser name. */
		static IArgumentParser * Parser(Text::StringView8 type);

		/** Convert text to the value, if given pointer is not null, it is expected that parsed value should be stored there. */
		virtual bool Text2Value(const ArgumentDescriptor & argument, Text::StringView8 text, Pointer value) = 0;
		/** Convert from value to the text. */
		virtual bool Value2Text(const ArgumentDescriptor & argument, const Pointer value, String * target) = 0;
	};


	template<typename T>
	struct ArgumentParser
		: public IArgumentParser
	{
		bool Text2Value(const ArgumentDescriptor & argument, Text::StringView8 text, Pointer target) override {
			// parse type
			T dummyValue;
			T & targetT = target == nullptr ? dummyValue : *(T *)target;
			if (!text.TryCast(targetT))
				return false;

			// parse additional options
			CommonArgumentOptions options(argument.Options);
			if constexpr (IsSame<T, f64>) {
				if (options.Min.IsSet())
					targetT = Math::Max(targetT, options.Min.Get());

				if (options.Max.IsSet())
					targetT = Math::Min(targetT, options.Max.Get());
			}

			return true;
		}

		bool Value2Text(const ArgumentDescriptor & argument, const Pointer source, String * target) override {
			// parse type
			const T & srcT = *(const T *)source;
			target->Clear().Append(srcT);

			return true;
		}
	};

	struct ArgumentParserKeyword
		: IArgumentParser
	{
		bool Text2Value(const ArgumentDescriptor & argument, Text::StringView8 text, Pointer value) override { return text.EqualsInsensitive(argument.Name); }
		bool Value2Text(const ArgumentDescriptor & argument, const Pointer value, String * target) override { return false; }
	};


	/** */
	struct ArgumentParserEnum
		: IArgumentParser
	{
		bool Text2Value(const ArgumentDescriptor & argument, Text::StringView8 text, Pointer value) override {
			size_t index = Sequencer::Drive(argument.Options.SplitSkipEmpty('|')).IndexOf(text);
			if (index == static_cast<size_t>(-1))
				return false;

			if (value != nullptr)
				*(size_t *)value = index;


			return true;
		}
		bool Value2Text(const ArgumentDescriptor & argument, const Pointer value, String * target) override { return false; }
	};
}


namespace DD::CommandLine {
	inline IArgumentParser * IArgumentParser::Parser(Text::StringView8 type) {
		switch (CommandLine::HashCI(type)) {
			case HashCI("double"): return Singleton<ArgumentParser<double>>::InstancePtr();
			case HashCI("boolean"): return Singleton<ArgumentParser<bool>>::InstancePtr();
			case HashCI("keyword"): return Singleton<ArgumentParserKeyword>::InstancePtr();
			case HashCI("enum"): return Singleton<ArgumentParserEnum>::InstancePtr();
			case HashCI("string"): return Singleton<ArgumentParser<String>>::InstancePtr();
			default: return nullptr;
		}
	}
}
