#pragma once
namespace DD::CommandLine {
	/**
	 * Object for parsing command line commands.
	 * Idea of the organization is to have commands, identified by keywords.
	 * Thus each command has keyword and set of arguments associated with the command.
	 * Object should be providing self-documentation of the arguments.
	 *
	 * Object is considered as singleton class and it is not intended
	 * to use multiple instances of the command line.
	 *
	 * @example simple usage
	 *   // firstly define a command, secondly its params
	 *   CommandLine::Command command("object", "properties of the generated object");
	 *   CommandLine::Double width("width", "width of generated object", 0.0);
	 *   CommandLine::Double height("height", "height of generated object", 0.0);
	 *   // add program description
	 *   CommandLine::Parser::Instance()->Name("An object generator");
	 *   CommandLine::Parser::Instance()->Brief("Program serving for something...");;
	 *   // use parser
	 *   CommandLine::Instance()->TryParse(argc, argv);
	 *   if (command)
	 *     doSomething(width, height);
	 */
	struct Program;
}

#include <DD/ArrayView.h>
#include <DD/CommandLine/Command.h>
#include <DD/CommandLine/RequestHandler.h>
#include <DD/List.h>
#include <DD/String.h>
#include <DD/Text/LogVirtual.h>

namespace DD::CommandLine {
	struct Program {
	public: // statics
		/**
		 * Last created instance of the Program class is stored here.
		 * Singleton reference.
		 */
		static Program * LastInstance(Program * program = nullptr);

	protected: // properties
		/** Name of the command line program. */
		Text::StringView8 _name;
		/** Description of the program. */
		Text::StringView8 _brief;
		/** Set of commands. */
		List<Command *> _commands;
		/** Unparsed arguments. */
		List<const char *> _unparsed;
		/** Requests. */
		List<RequestHandler *> _requestHandlers;

	public: // methods
		/** Print help to the given string buffer. */
		void Help(::DD::String & help) const;
		/** Parse arguments of the command line. */
		bool TryParse(size_t argc, const char ** argv, Text::LogVirtual8 &);
		/** Parse arguments of the command line caught in the given string. */
		bool TryParse(Text::StringView16 cmd, Text::LogVirtual8 &);
		/** Reset parser state. */
		void Reset();
		/** Last registered command if exists, null otherwise. */
		Command * LastCommand() { return _commands.Size() ? _commands.Last() : nullptr; }
		/** Register callback handler. */
		void Register(RequestHandler * handler) { _requestHandlers.Add(handler); }
		/** Register command. */
		void Register(Command * command) { _commands.Add(command); }
		/** Get commands. */
		DD::ArrayView1D<Command * const> Commands() const { return _commands; }
		/** Name. */
		Text::StringView8 Name() const { return _name; }
		/** Compose string line with the current values of the commands. */
		::DD::String Line() const;
		/** Text-based serialization method. */
		void Serialize(Text::StringWriter8 & target) const;
		/** Text-based deserialization methods. */
		void Deserialize(Text::StringView8 source);

	public: // constructors
		/** Zero constructor. */
		Program(Text::StringView8 name, Text::StringView8 brief) : _name(name), _brief(brief) { LastInstance(this); }
	};
}

