#pragma once
namespace DD::Xml {
	/** Helper structure for manipulation xml documents. */
	struct Builder;
}

#include <DD/Collections/LazyList.h>
#include <DD/Xml/Document.h>
#include <DD/Xml/Tools.h>

namespace DD::Xml {
	struct Builder {
	public: // nested
		/** Proxy for string view to keep data update when source string needs to reallocate.*/
		struct StringViewProxy {
			/** Proxy target. */
			Text::StringView16 & _target;
		};

		/** Proxy for xml element. */
		struct ElementProxy {
			/** Pointer to source element. */
			Xml::Element * _element;
			/** Pointer to the builder that created this proxy. */
			Xml::Builder * _parent;

			/** Append attribute. */
			ElementProxy & AppendAttribute(Text::StringView16 name, Text::StringView16 value);
			/** Append child element, return proxy to the element. */
			ElementProxy AppendChild(Text::StringView16 name, Xml::Element::Types = Xml::Element::Types::TagType);
			/** Shortcut for appending text elements. */
			ElementProxy AppendTextElement(Text::StringView16 name) { return AppendChild(name, Xml::Element::Types::TextType); }

			/** Check if stored element is really valid.  */
			bool IsValid() const { return _element != nullptr; }
			/** Get sibling, @return an invalid proxy if element does not exist. */
			ElementProxy GetSibling() const { return { _element->Sibling, _parent }; }
			/** Get first child, @return an invalid proxy if element does not exist. */
			ElementProxy GetChild() const { return { _element->Sibling, _parent }; }
			/** Native element. */
			Xml::Element * Native() const { return _element; }
		};

	protected:
		/** Storage of the xml text data. */
		String Buffer{4096};
		/** Document representing the xml. */
		Xml::Document Target;
		/** List of proxies, string-views that points to the source text.  */
		Collections::LazyList<StringViewProxy, 128> Proxies;

	protected:
		/** Append a text to the document, method provides escaping. */
		void appendText(Text::StringView16 value);
		/** Append a text to the document, add quotes and provide appropriate escaping. */
		void appendQuotes(Text::StringView16 value);
		/** Append a text to the document, save its proxy. */
		void appendProxy(Text::StringView16 text, Text::StringView16 & proxy, bool quoteEscape);
		/** Append header to the document. */
		void appendHeader(Text::StringView16 enconding, Text::StringView16 version);
		/** Append the root element to the document. */
		void appendRoot(Text::StringView16 name);

	public:
		/** Get document object. */
		Document & GetDocument() { return Target; }
		/** Get Root. */
		ElementProxy GetRoot() { return { Target.Root, this }; }
		/** Get Root. */
		ElementProxy GetSuperRoot();
		/** Initialize document, */
		ElementProxy Init(
			Text::StringView16 rootName,
			Text::StringView16 enconding = L"UTF-8",
			Text::StringView16 version = L"1.0"
		);
	};
}
