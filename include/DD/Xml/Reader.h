#pragma once
namespace DD::Xml {
	/** Struct for reading Xml::Document. */
	struct Reader;
}

#include <DD/Text/StringReader.h>
#include <DD/Xml/Document.h>

namespace DD::Xml {
	struct Reader {
	public: // nested
		/* Additional info about parsing process. **/
		enum ErrorCodes {
			NoError,               // Correctly parsed
			InvalidTagName,        // Malformatted tag name
			InvalidAttributeName,  // Malformatted arg name
			InvalidAttributeValue, // Argument quotes are unclosed
			InvalidComment,        // Unfinished comment brackets
			InvalidHeader,         // Mal formated header <?xml ?>
			MultipleHeaders,       // Xml must have single header
			InvalidCloseTag,       // A close tag does not match the open tag
			UnexpectedEnd,         // Xml ended before all tags were closed
			MultipleRoots          // Xml must have single root element
		};

	protected: // properties
		/** Target of parsing. */
		Xml::Document & _document;
		/** Helper for stacking. */
		Xml::Element * _topElement = nullptr;

	protected: // methods
		/** Create an element, stack in case it has some children. */
		Xml::Element & appendElement(bool stack);
		/** Name="value" || Name */
		ErrorCodes parseAttribute(Xml::Attribute & attribute, Text::StringReader16 & reader);
		/** <tag> || <tag/> */
		ErrorCodes parseOpenTag(Text::StringView16);
		/** </tag> */
		ErrorCodes parseCloseTag(Text::StringView16);
		/** <? ?> */
		ErrorCodes parseHeader(Text::StringView16);
		/** <!-- --!>  */
		ErrorCodes parseComment(Text::StringView16);
		/** Text between tags. */
		ErrorCodes parseText(Text::StringView16);
		/** Cut xml into separate tags and read them. */
		ErrorCodes parseXml(Text::StringView16);

	public: // methods
		/** Read given xml. */
		ErrorCodes Read(Text::StringView16 xmlText) { return parseXml(xmlText); }

	public:
		/** Constructor. */
		Reader(Xml::Document & document) : _document(document) {}
	};
}
