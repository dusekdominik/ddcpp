#pragma once
namespace DD::Xml {
	/** Xml document. */
	struct Document;
}

#include <DD/BlockListEx.h>
#include <DD/Memory/Rigid.h>
#include <DD/Xml/Element.h>

namespace DD::Xml {
	struct Document
		: Memory::Rigid
	{
		/** List with all elements, 1023 preallocated items. */
		BlockListEx<Element, 1023> Elements;

		/** Header tag, e.g. <?xml version="" ?> */
		Element * Header = nullptr;
		/** Root tag of the document. */
		Element * Root = nullptr;
		/** Super root, its children must be header and root and possibly some comments. */
		Element * SuperRoot = nullptr;
	};
}
