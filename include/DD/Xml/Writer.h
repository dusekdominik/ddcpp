#pragma once
namespace DD::Xml {
	/** */
	struct IWriter;

	/** */
	struct Writer;
}

#include <DD/Collections/LazyList.h>
#include <DD/Xml/Document.h>
#include <DD/Xml/Tools.h>

namespace DD::Xml {

	struct StringFile {
		/**  */
		String Buffer;
		/**  */
		Collections::LazyList<Text::StringView16 *> _register;

		void Apply() {
			// convert all offsets to pointers
			for (Text::StringView16 * text : _register) {
				*text = { Buffer.begin() + (size_t)text->begin(), text->Length() };
			}
		}

		void Write(Text::StringView16 & target, Text::StringView16 text) {
			// save beginning offset and length
			target = { (wchar_t *)Buffer.Length(), text.Length() };
			// add to buffer
			Buffer.AppendEx(text);
			// add for further evaluation
			_register.Append(&target);
		}
	};



	struct IWriter {
		virtual void Write(Text::StringView16) = 0;
	};

	struct StringWriter : IWriter {
		String XmlText = String(4096);
		void Write(Text::StringView16 text) override { XmlText.AppendEx(text); }
	};

	struct Writer {
		/** Constant. */
		static constexpr Text::StringView16 Space = L" ";
		/** Constant. */
		static constexpr Text::StringView16 Tab = L"\t";
		/** Constant. */
		static constexpr Text::StringView16 NewLine = L"\n";
		/** Constant. */
		static constexpr Text::StringView16 Equals = L"=";

		/** Source document. */
		Xml::Document & _document;
		/** */
		Xml::IWriter * _target;


		void Write() { WriteElement(0, _document.SuperRoot); }

		void WriteAttribute(Xml::Attribute & attribute) {
			_target->Write(Space);
			_target->Write(attribute.Name);

			if (attribute.Value != nullptr) {
				_target->Write(Equals);
				_target->Write(attribute.Value);
			}
		}


		void WriteTabs(size_t tabs) { while (tabs--)  _target->Write(Tab); }

		void WriteElement(size_t tabs, Xml::Element * element) {


			WriteTabs(tabs);
			switch (element->Type)
			{
			case Element::DocumentType:
				for (Element * child = element->Child; child; child = child->Sibling)
					WriteElement(0, child);
				break;

			case Element::HeaderType:
				_target->Write(Tools::OpenHeader);
				_target->Write(element->Name);
				for (Xml::Attribute attribute : element->Attributes)
					WriteAttribute(attribute);
				_target->Write(Tools::CloseHeader);
				break;

			case Element::CommentType:
				_target->Write(Tools::OpenComment);
				_target->Write(element->Name);
				_target->Write(Tools::CloseComment);
				break;

			case Element::TagType:
				_target->Write(Tools::OpenTag);
				_target->Write(element->Name);
				for (Xml::Attribute attribute : element->Attributes)
					WriteAttribute(attribute);
				if (element->Child) {
					_target->Write(Tools::CloseTag);
					// inline single child elements
					if (element->Child->Sibling == nullptr && element->Child->Type == Element::Types::TextType) {
						_target->Write(element->Child->Name);
						_target->Write(Tools::OpenEndTag);
						_target->Write(element->Name);
						_target->Write(Tools::CloseTag);
					}
					else {
						_target->Write(NewLine);
						for (Element * child = element->Child; child; child = child->Sibling)
							WriteElement(tabs + 1, child);

						WriteTabs(tabs);
						_target->Write(Tools::OpenEndTag);
						_target->Write(element->Name);
						_target->Write(Tools::CloseTag);
					}
				}
				else {
					_target->Write(Tools::CloseShortTag);
				}
				break;

			case Element::TextType:
				_target->Write(element->Name);
				break;

			default:
				break;
			}


			_target->Write(NewLine);
		}

	};
}