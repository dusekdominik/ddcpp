#pragma once
namespace DD::Xml {
	/** Name value pair. */
	struct Attribute;
}

#include <DD/BlockListEx.h>
#include <DD/String.h>
#include <DD/Text/StringView.h>
#include <DD/Text/StringReader.h>
#include <DD/Xml/Tools.h>

namespace DD::Xml {
	struct Attribute {
		/** Attribute name. */
		Text::StringView16 Name = nullptr;
		/** Attribute value, unescaped, contains quotes. */
		Text::StringView16 Value = nullptr;

		/** Get escaped value. */
		String EscapedValue() const;
		/** Try cast escaped value. */
		template<typename T>
		bool TryCast(T & target) const { return EscapedValue().TryCast(target); }
	};
}
