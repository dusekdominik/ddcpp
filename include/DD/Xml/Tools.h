#pragma once
namespace DD::Xml {
	struct Tools;
}

#include <DD/Text/StringView.h>

namespace DD::Xml {
	struct Tools {
	public: // statics
		/** Predefined constant. */
		static constexpr Text::StringView16 OpenComment = L"<!--";
		/** Predefined constant. */
		static constexpr Text::StringView16 CloseComment = L"--!>";
		/** Predefined constant. */
		static constexpr Text::StringView16 OpenHeader = L"<?";
		/** Predefined constant. */
		static constexpr Text::StringView16 CloseHeader = L"?>";
		/** Predefined constant. */
		static constexpr Text::StringView16 OpenTag = L"<";
		/** Predefined constant. */
		static constexpr Text::StringView16 CloseTag = L">";
		/** Predefined constant. */
		static constexpr Text::StringView16 CloseShortTag = L"/>";
		/** Predefined constant. */
		static constexpr Text::StringView16 OpenEndTag = L"</";

		/** Escaped sequence. */
		static constexpr Text::StringView16 EscapeQuote        = L"&quot;";
		/** Escaped sequence. */
		static constexpr Text::StringView16 EscapeApostrophe   = L"&apos;";
		/** Escaped sequence. */
		static constexpr Text::StringView16 EscapeOpenBracket  = L"&lt;";
		/** Escaped sequence. */
		static constexpr Text::StringView16 EscapeCloseBracket = L"&gt;";
		/** Escaped sequence. */
		static constexpr Text::StringView16 EscapeAmpersand    = L"&amp;";

	public: // statics
		/** Contains Short-Tag brackets. */
		static constexpr bool HasShortTagBrackets(const Text::StringView16 & tag) { return tag.StartsWith(OpenTag) && tag.EndsWith(CloseShortTag); }
		/** Contains header brackets. */
		static constexpr bool HasHeaderBrackets(const Text::StringView16 & tag) { return tag.StartsWith(OpenHeader) && tag.EndsWith(CloseHeader); }
		/** Contains close tag brackets */
		static constexpr bool HasCloseTagBrackets(const Text::StringView16 & tag) { return tag.StartsWith(OpenEndTag) && tag.EndsWith(CloseTag); }
		/** Contains Comment brackets. */
		static constexpr bool HasCommentBrackets(const Text::StringView16 & tag) { return tag.StartsWith(OpenComment) && tag.EndsWith(CloseComment); }
	};
}
