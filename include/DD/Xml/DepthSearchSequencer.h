#pragma once
namespace DD::Xml {
	/** DFS search for a Xml element. */
	struct DepthSearchSequencer;
}

#include <DD/Lambda.h>
#include <DD/XML.h>

namespace DD::Xml {
	struct DepthSearchSequencer {
		/** Lambda selector. */
		using Acception = Lambda<bool(XML::Element &)>;

	public: // properties
		/** Descendants of this element will be enumerated. */
		XML::Element * SearchRoot;
		/** Stack for DFS search. */
		List<XML::Element *, 64> SearchStack;
		/**
		 * Check if element (and subsequently its subtree) is enumerated.
		 * If nothing is assigned all nodes are accepted.
		 */
		Acception Accept;

	public: // methods
		/** Sequencer method, reset search. */
		bool Reset();
		/** Sequencer method, move to the next match, @return false if no more matches are in the sequence. */
		bool Next();
		/** Get item. */
		XML::Element & Get() { return *SearchStack.Last(); }

	public: // constructors
		/** Constructor. */
		DepthSearchSequencer(XML::Element & searchRoot, Acception accept = Acception())
			: SearchRoot(&searchRoot)
			, Accept(accept)
		{}
	};
}