#pragma once
namespace DD::Xml {
	/** Xml element, could be tag/header/comment/text. */
	struct Element;
}

#include <DD/Collections/LazyList.h>
#include <DD/Text/StringView.h>
#include <DD/Xml/Attribute.h>
#include <DD/Iterator/Iterable.h>

namespace DD::Xml {
	struct Element {
		/** Types of element. */
		enum Types {
			UndefinedType, // default value, serves for safety checks
			DocumentType,  // super root, representing entire document
			HeaderType,    // xml header, defines version and ecoding
			CommentType,   // xml comments
			TagType,       // xml tag, <tag></tag>
			TextType       // text element
		};

		/** Fwd iterator. */
		template<bool _CONST>
		struct IteratorT {
			/** Make type const if _CONST is true. */
			template<typename  T>
			using ConstType =  ConditionalConstType<_CONST, T>;

			ConstType<Element *> _element;
			IteratorT & operator++() { _element = _element->Sibling; }
			ConstType<Element &> operator*() const { return *_element; }
			bool operator!=(const IteratorT &) const { return _element != nullptr; }
		};

		using Iterator = IteratorT<false>;
		using ConstIterator = IteratorT<true>;
		using Iterable = DD::Iterator::Iterable<Iterator>;
		using ConstIterable = DD::Iterator::Iterable<ConstIterator>;

		/** Type of the item. */
		Types Type = UndefinedType;
		/** Tag name, comment or text wrt @see Type. */
		Text::StringView16 Name = nullptr;
		/** List of tag attributes. */
		Collections::LazyList<Attribute, 4> Attributes;
		/** Tag Child. */
		Element * Child = nullptr;
		/** Tag Sibling. */
		Element * Sibling = nullptr;
		/** Pointer to parent. */
		Element * Parent = nullptr;

		/** Enumerate all children.  */
		Iterable Children() { return { { Child }, { nullptr } }; };
		/** Enumerate all children.  */
		ConstIterable Children() const { return { { Child }, { nullptr } }; };
		/** Get attribute if exists, empty attribute otherwise. */
		Attribute GetAttribute(Text::StringView16 name) const { for (const Attribute & a : Attributes) if (a.Name == name) return a; return Attribute(); }
		/** Get attribute value if exists, null string otherwise. */
		Text::StringView16 GetAttributeValue(Text::StringView16 name) const { return GetAttribute(name).Value; }

		/** Add child to the linked list. */
		void AppendChild(Element* child) {
			// first child
			Element ** lastChild = &Child;
			// iterate until free slot is found
			while (*lastChild != nullptr)
				lastChild = &(*lastChild)->Sibling;

			// set the child
			*lastChild = child;
		}
	};
}
