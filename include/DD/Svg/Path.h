#pragma once
namespace DD::Svg {
	/**
	 * Svg Path.
	 */
	struct Path;
}

#include <DD/Matrix.h>
#include <DD/String.h>

namespace DD::Svg {
	struct Path {
	public: // nested objects
		/** Decompose path to the continuous pieces. */
		struct DecompositionSequencer;
		/** Sequencer for converting a path to set of continuous (non-interrupted) polylines. */
		struct PolylineSequencer;
		/** Command of path. */
		struct PathCommand {
			/** Command type. */
			enum struct Types {
				Cubic, Quadric, Horizontal, Vertical, Line, Move, Close, Arc
			} Type;
			/** Command interpretation. */
			enum Modes { Relative, Absolute } Mode;
			/** Command parameters. */
			union {
				struct { double2 Target, Radius; f64 Rotation; i32 SweepFlag, LargeArcFlag; } Arc;
				struct { double2 Target, ControlPointStart, ControlPointTarget; } Cubic;
				struct { double2 Target, ControlPoint; } Quadric;
				struct { double2 Target; } Line;
				struct { double2 Target; } Move;
				struct { f64 TargetX; } Horizontal;
				struct { f64 TargetY; } Vertical;
				struct {} Close;
			};
			/**
			 * Helper for tracking cursor on the path.
			 * @param opening  - [in/out] the position where a path begins
			 *                 - it is changed by move commands, and used by close commands
			 * @param position - [in/out] current cursor position, absolute values.
			 * @return true if path has been interrupted (move commands).
			 */
			bool UpdatePosition(double2 & opening, double2 & position) const;
			/** Copy constructor. */
			PathCommand(const PathCommand & cmd) : Mode(cmd.Mode), Type(cmd.Type), Arc(cmd.Arc) {}
			/** Zero constructor. */
			PathCommand() {}
			/** Basic constructor. */
			PathCommand(Types type, Modes mode) : Type(type), Mode(mode) {}
		};

	public: // properties
		/** Commands of the path. */
		List<PathCommand, 64> Commands;

	public: // commands
		/**
		 * Add arc with absolute positioning.
		 * @param point        - end point of the arc.
		 * @param radius       - 2d elliptical radius.
		 * @param rotation     - clock-wise rotation of axis in degrees.
		 * @param sweepFlag    - arc should begin moving at positive angles or negative ones.
		 * @param largeArcFlag - arc should be greater than or less than 180 degrees.
		 */
		void AddArcAbsolute(
			const double2 & point,
			const double2 & radius,
			f64 rotation,
			bool sweepFlag,
			bool largeArcFlag
		);
		/**
		 * Add arc with absolute positioning.
		 * @param point        - end point of the arc.
		 * @param radius       - 2d elliptical radius.
		 * @param rotation     - clock-wise rotation of axis in degrees.
		 * @param sweepFlag    - arc should begin moving at positive angles or negative ones.
		 * @param largeArcFlag - arc should be greater than or less than 180 degrees.
		 */
		void AddArcRelative(
			const double2 & point,
			const double2 & radius,
			f64 rotation,
			bool sweepFlag,
			bool largeArcFlag
		);
		/**
		 * Add circle to the path, parametrized by center and radius.
		 * @param centerPoint - center in absolute coordinates.
		 * @param radius      - radius of the circle.
		 */
		void AddCircleAbsolute(const double2 & centerPoint, f64 radius);
		/**
		 * Add circle to the path, parametrized by center and radius.
		 * @param centerPoint - center in relative coordinates.
		 * @param radius      - radius of the circle.
		 */
		void AddCircleRelative(const double2 & centerPoint, f64 radius);
		/**
		 * Draw line from current cursor position to the given target.
		 * @param target - target point in absolute coordinates.
		 */
		void AddLineAbsolute(const double2 & target);
		/**
		 * Draw line from current cursor position to the given target.
		 * @param target - target point in relative coordinates.
		 */
		void AddLineRelative(const double2 & target);
		/** Add horizontal move with relative positioning. */
		void AddHorizontalRelative(f64 x);
		/** Add horizontal move with relative positioning. */
		void AddHorizontalAbsolute(f64 x);
		/** Add vertical move with relative positioning. */
		void AddVerticalRelative(f64 y);
		/** Add vertical move with relative positioning. */
		void AddVerticalAbsolute(f64 y);
		/** Add move command with absolute positioning. */
		void AddMoveAbsolute(const double2 & target);
		/** Add move command with relative positioning. */
		void AddMoveRelative(const double2 & target);
		/** Add quadric curve with relative positioning. */
		void AddQuadricRelative(const double2 & controlPoint, const double2 & target);
		/** Add quadric curve with relative positioning. */
		void AddQuadricAbsolute(const double2 & controlPoint, const double2 & target);
		/** Add close command. */
		void AddClose();

	public: // add complex structures
		/** Add cross. */
		void AddCross(const double2 & point, f64 radius);
		/** Add line in absolute coordinates. */
		void AddLine(const Math::LineSegment2D & line);
		/** Add line strip in absolute coordinates. */
		void AddLineStrip(const ArrayView1D<const double2> & lineStrip);
		/**
		 * Add whole path object (with given transformation.
		 * @param path           - object that is being added
		 * @param transformation - transformation of the path that is being added.
		 * @param offset         - offset of the path that is being added.
		 */
		void AddPath(
			const Path & path,
			const double2x2 & transformation = double2x2(DiagonalMatrix2D(1.0)),
			const double2 & offset = double2(0.0)
		);
		/** Add with given offset. */
		void AddPath(const Path & path, const double2 & offset);
		/**
		 * Add quadric representing parabola.
		 * @param parabola - parabola.
		 * @param aabox    - maximal size quadric,
		 *                   x-coordinate represents with according to parabola directrix,
		 *                   y-coordinate represents with according to parabola axis.
		 */
		void AddParabola(const Math::Parabola2D & parbola, const double2 & aabox);
		/**
		 * Add sequence of absolute coordinates as polygon
		 * It is not important if first and last point are the same.
		 */
		void AddPolygon(const ArrayView1D<const double2> & lineStrip);
		/** Add rectangle. */
		void AddRectangle(const Math::Interval2D & box);
		/**
		 * Add strip of rectangles.
		 * @param box        - definition of the first rectangle.
		 * @param offset     - definition of move to the next rectangle.
		 * @param rectangles - number of rectangles.
		 */
		void AddRectangleStrip(const Math::Interval2D & box, const double2 & offset, i32 rectangles);
		/** Add square. */
		void AddSquare(const double2 & center, f64 halfSize);

	public: // methods
		/** Remove all stored commands. */
		Path & Clear() { Commands.Clear(); return *this; }
		/** Decompose path to the non-interrupted pieces. */
		DecompositionSequencer Decompose() const;
		/** Discretize path as polylines. */
		Sequencer::Driver<PolylineSequencer> ReadPolylines() const;
		Sequencer::Driver<PolylineSequencer> ReadPolylines(
			f64 yCorrection, double2 offset = 0.0, double2 scale = 1.0, f64 angleLimit = 0.999
		) const;
		/** Convert path to the completely absolute commands. */
		void Absolutize();
		/**
		 * Add mirror-like reflections of the current path by two main axes.
		 *   O |        O | O      O | O    + - origin of the coordinate system (0, 0)
		 *   --+--  ->  --+--  ->  --+--    So path made in a corner is reflected
		 *     |          |        O | O    firstly by horizontal then by vertical axis.
		 */
		void Mirror();
		/**
		 * Transformation of the path collection.
		 * @param transformation - matrix defining mathematical transformation.
		 * @param offset         - vector defining offset after transformation.
		 * @param index          - index where transformation starts, keep zero
		 *                         for application to whole commands set.
		 */
		void Transform(const double2x2 & transformation, const double2 & offset = double2(0.0), size_t index = 0, size_t length = ~size_t(0));
		/** Move the path by given offset. */
		void Translate(const double2 & offset) { Transform(DiagonalMatrix2D(1.0), offset); }
		/**
		 * Create svg string from the path.
		 * @param string - buffer where to store the string.
		 *               - it is expected some preallocation.
		 */
		void ToString(String & string) const;
	};


	struct Path::PolylineSequencer {
		DD_TODO_STAMP("Try to exploit decomposition sequencer.")
	public: // properties
		/** Absolutized path. */
		Path AbsolutePath;
		/** Index of the processed path command. */
		size_t Index = 0;
		/** Cursor like position, seqeuncer property. */
		double2 Position = 0.0;
		/** Accumulator for created polyline. */
		List<double2> Accumulator;
		/**
		 * If output correction is enabled it flips the y coordinate,
		 * applies @see Offset and @see Scale.
		 */
		bool OutputCorrectionEnabled = false;
		/** Flip orientation of Y axis. */
		f64 YCorrection = 0.0;
		/** Offset added to the output polylines. */
		double2 Offset = {0.0};
		/** Scale applied to the output polylines. */
		double2 Scale = {1.0};
		/** Angle limit during computation fo curves in cosine values ~, 0.999 ~ 2.5 deg. */
		f64 AngleLimit = 0.999;

	public: // sequencer methods
		bool Reset();
		bool Next();
		Array<double2> Get() { return Array<double2>::Copy(Accumulator); }

	public: // constructors
		/** Move assignment, note that all these constructors are necessary to be able to move and copy this structure. */
		PolylineSequencer & operator=(PolylineSequencer && rhs);
		/** Constructor accepting a path to be discretized. */
		PolylineSequencer(const Path & path, f64 yCorrection = 0.0, double2 offset = 0.0, double2 scale = 1.0, f64 angleLimit = 0.999);
		/** Default constructor. */
		PolylineSequencer(f64 angleLimit = 0.999) : Accumulator(), AbsolutePath(), OutputCorrectionEnabled(false), AngleLimit(angleLimit) {}
		/** Copy constructor. */
		PolylineSequencer(const PolylineSequencer & rhs);
	};


	struct Path::DecompositionSequencer {
		/** Position of the beginning of current loop. */
		double2 _begin;
		/** current position of cursor. */
		double2 _cursor;
		/** index of currently processed. */
		size_t _index;
		/** Storage for currently generated path. */
		Path _decomposedPath;
		/** Pointer to the source path. */
		const Path * _sourcePath;

	public: // iSequencer
		const Path & Get() { return _decomposedPath; }
		bool Next();
		bool Reset();

	public: // constructors
		/** Constructor. */
		DecompositionSequencer(const Path * source) : _sourcePath(source) {}
	};

}