#pragma once
namespace DD::Svg {
	/** Hashes of tag names of svg primitives. */
	enum struct Primitives : unsigned int;
	/** SVG common descriptor, helper for deserialization from xml data. */
	template<typename DERIVED_PRIMITIVE>
	struct Primitive;
	/** SVG Line descriptor, helper for deserialization from xml data. */
	struct XmlLine;
	/** SVG Polyline descriptor, helper for deserialization from xml data. */
	struct XmlPolyline;
	/** SVG Rectangle descriptor, helper for deserialization from xml data. */
	struct XmlRectangle;
	/** SVG Polygon descriptor, helper for deserialization from xml data. */
	struct XmlPolygon;
	/** SVG Circle descriptor, helper for deserialization from xml data. */
	struct XmlCircle;
	/** SVG Ellipse descriptor, helper for deserialization from xml data. */
	struct XmlEllipse;
	/** SVG Ellipse descriptor, helper for deserialization from xml data. */
	struct XmlPath;
	/** SVG Group descriptor, helper for deserialization from xml data. */
	struct XmlGroup;
	/** SVG descriptor for proxies, helper for deserialization from xml data. */
	struct XmlUse;
}

#include <DD/Svg/Hash.h>
#include <DD/Svg/Path.h>
#include <DD/XML.h>

namespace DD::Svg {
	enum struct Primitives : Svg::HashType {
		Link             = Svg::Hash(L"a"),
		Animate          = Svg::Hash(L"animate"),
		AnimateMotion    = Svg::Hash(L"animateMotion"),
		AnimateTransform = Svg::Hash(L"animateTransform"),
		Circle           = Svg::Hash(L"circle"),
		ClipPath         = Svg::Hash(L"clipPath"),
		Defs             = Svg::Hash(L"defs"),
		Description      = Svg::Hash(L"desc"),
		Ellipse          = Svg::Hash(L"ellipse"),
		Filter           = Svg::Hash(L"filter"),
		ForeignObject    = Svg::Hash(L"foreignObject"),
		Group            = Svg::Hash(L"g"),
		Image            = Svg::Hash(L"image"),
		Line             = Svg::Hash(L"line"),
		LinearGradient   = Svg::Hash(L"linearGradient"),
		Marker           = Svg::Hash(L"marker"),
		Mask             = Svg::Hash(L"mask"),
		Path             = Svg::Hash(L"path"),
		Pattern          = Svg::Hash(L"pattern"),
		Polygon          = Svg::Hash(L"polygon"),
		Polyline         = Svg::Hash(L"polyline"),
		RadialGradient   = Svg::Hash(L"radialGradient"),
		Rectangle        = Svg::Hash(L"rect"),
		Script           = Svg::Hash(L"script"),
		Style            = Svg::Hash(L"style"),
		Svg              = Svg::Hash(L"svg"),
		Switch           = Svg::Hash(L"switch"),
		Symbol           = Svg::Hash(L"symbol"),
		Text             = Svg::Hash(L"text"),
		TextPath         = Svg::Hash(L"textPath"),
		Use              = Svg::Hash(L"use"),
		View             = Svg::Hash(L"view"),
	};

	struct IPrimitive {
		/** Text representation of possible transformations. */
		Text::StringView16 Transformation = nullptr;
	};

	template<typename DERIVED_PRIMITIVE>
	struct Primitive : IPrimitive {
		/** Deserialize given primitive. */
		bool Deserialize(XML::Element & e) { return DD::Xml::Mini::Deserialize(e, static_cast<DERIVED_PRIMITIVE&>(*this)); }
	};

	struct XmlLine : Primitive<XmlLine> {
		/** Start point of the line. */
		double2 X = {0.0};
		/** End point of the line. */
		double2 Y = {0.0};
	};

	struct XmlPolyline : Primitive<XmlPolyline> {
		/** String with point coordinates. */
		Text::StringView16 Points;
	};

	struct XmlRectangle : Primitive<XmlRectangle> {
		/** Top-left position. */
		double2 Point = {0.0};
		/** Width and height of the rectangle. */
		double2 Dimensions = {0.0};
		/** Descriptor of rounded corners of the rectangle. */
		double2 Rounded = {0.0};
	};

	struct XmlPolygon : Primitive<XmlPolygon> {
		/** String with point coordinates. */
		Text::StringView16 Points = nullptr;
	};

	struct XmlCircle : Primitive<XmlCircle> {
		/** Center point of the circle. */
		double2 Center = {0.0};
		/** Radius of the circle. */
		f64 Radius = 0.0;
	};

	struct XmlEllipse : Primitive<XmlEllipse> {
		/** Center point of the ellipse. */
		double2 Center = { 0.0 };
		/** Ellipse axes descriptors. */
		double2 Radii = { 0.0 };
	};


	struct XmlPath : Primitive<XmlPath> {
		/** String with path commands. */
		Text::StringView16 Path = nullptr;
	};

	struct XmlGroup : Primitive<XmlGroup> {
	};

	struct XmlUse : Primitive<XmlUse> {
		/** Id of the element that should be used. */
		Text::StringView16 Link = nullptr;
		/** Offset of the reference. */
		double2 Offset = { 0.0 };
	};
}

// <rect x="10" y="10" width="30" height="30" />
DD_XML_MINI(
	DD::Svg::XmlRectangle,
	DD::Svg::Hash,
	ATTRIBUTE(transform, Transformation, Text::StringView16(nullptr)),
	ATTRIBUTE(        x,        Point.x, 0.0),
	ATTRIBUTE(        y,        Point.y, 0.0),
	ATTRIBUTE(       rx,      Rounded.x, 0.0),
	ATTRIBUTE(       ry,      Rounded.y, 0.0),
	ATTRIBUTE(    width,   Dimensions.x, 0.0),
	ATTRIBUTE(   height,   Dimensions.y, 0.0)
);

// <ellipse cx="75" cy="75" rx="20" ry="5" />
DD_XML_MINI(
	DD::Svg::XmlEllipse,
	DD::Svg::Hash,
	ATTRIBUTE(transform, Transformation, Text::StringView16(nullptr)),
	ATTRIBUTE(       cx,       Center.x, 0.0),
	ATTRIBUTE(       cy,       Center.y, 0.0),
	ATTRIBUTE(       rx,        Radii.x, 0.0),
	ATTRIBUTE(       ry,        Radii.y, 0.0)
);

// <circle cx="25" cy="75" r="20" />
DD_XML_MINI(
	DD::Svg::XmlCircle,
	DD::Svg::Hash,
	ATTRIBUTE(transform, Transformation, Text::StringView16(nullptr)),
	ATTRIBUTE(       cx,       Center.x, 0.0),
	ATTRIBUTE(       cy,       Center.y, 0.0),
	ATTRIBUTE(        r,         Radius, 0.0)
);

// <line x1="10" x2="50" y1="110" y2="150" />
DD_XML_MINI(
	DD::Svg::XmlLine,
	DD::Svg::Hash,
	ATTRIBUTE(transform, Transformation, Text::StringView16(nullptr)),
	ATTRIBUTE(       x0,            X.x, 0.0),
	ATTRIBUTE(       y0,            X.y, 0.0),
	ATTRIBUTE(       x1,            Y.x, 0.0),
	ATTRIBUTE(       y1,            Y.y, 0.0)
);

// <polyline points="60, 110 65, 120 70, 115 75, 130 80, 125 85, 140 90, 135 95, 150 100, 145"/>
DD_XML_MINI(
	DD::Svg::XmlPolyline,
	DD::Svg::Hash,
	ATTRIBUTE(transform, Transformation, Text::StringView16(nullptr)),
	ATTRIBUTE(   points,         Points, Text::StringView16(nullptr))
);

// <polygon points="50, 160 55, 180 70, 180 60, 190 65, 205 50, 195 35, 205 40, 190 30, 180 45, 180"/>
DD_XML_MINI(
	DD::Svg::XmlPolygon,
	DD::Svg::Hash,
	ATTRIBUTE(transform, Transformation, Text::StringView16(nullptr)),
	ATTRIBUTE(   points,         Points, Text::StringView16(nullptr))
);

// <path d="M20,230 Q40,205 50,230 T90,230" fill="none" stroke="blue" stroke-width="5"/>
DD_XML_MINI(
	DD::Svg::XmlPath,
	DD::Svg::Hash,
	ATTRIBUTE(transform, Transformation, Text::StringView16(nullptr)),
	ATTRIBUTE(        d,           Path, Text::StringView16(nullptr))
);

// <g/>
DD_XML_MINI(
	DD::Svg::XmlGroup,
	DD::Svg::Hash,
	ATTRIBUTE(transform, Transformation, Text::StringView16(nullptr))
);

// <use href="#myCircle" x="10" fill="blue" />
DD_XML_MINI(
	DD::Svg::XmlUse,
	DD::Svg::Hash,
	ATTRIBUTE(transform, Transformation, Text::StringView16(nullptr)),
	ATTRIBUTE(     href,           Link, Text::StringView16(nullptr)),
	ATTRIBUTE(        x,       Offset.x, 0.0),
	ATTRIBUTE(        y,       Offset.y, 0.0)
);

