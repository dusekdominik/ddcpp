#pragma once
namespace DD::Svg {
	/**
	 * Descriptor of a single svg transformation.
	 * Covers scale, skewX, skewY, translate, rotate and matrix.
	 */
	struct MatrixTransformation;
}

#include <DD/String.h>
#include <DD/Matrix.h>
#include <DD/Svg/Hash.h>

namespace DD::Svg {
	struct MatrixTransformation {
	public: // nested
		/** Types of matrix transformations. */
		enum struct Types : Svg::HashType {
			None, // internal value for incorrect transformation
			Unit, // internal value for unit transformation
			Scale     = Svg::Hash(L"scale"),
			SkewX     = Svg::Hash(L"skewX"),
			SkewY     = Svg::Hash(L"skewY"),
			Translate = Svg::Hash(L"translate"),
			Rotate    = Svg::Hash(L"rotate"),
			Matrix    = Svg::Hash(L"matrix")
		};

	public: // statics
		/**
		 * Parse given string into set of f64 numbers.
		 * @param text   - text to be parsed.
		 * @param target - array with numbers, where result of parsing should be stored.
		 * @return number of parsed numbers.
		 */
		static size_t TryParseArguments(const Text::StringView16 & text, Array<f64> & target);

	public: // properties
		/** Offset of the transformation. */
		double2 Offset;
		/** Rotation matrix of the transformation. */
		double2x2 Rotation;
		/** Explicit meaning of the transformation defined by user. */
		Types Type;

		/** Initialize parameters as translation transformation. */
		MatrixTransformation & InitTranslate(double2 offset) { return Init(Types::Translate, offset, { 1.0, 0.0, 0.0, 1.0 }); }
		/** Initialize parameters as rotation transformation. */
		MatrixTransformation & InitRotate(f64 angleDeg) { return Init(Types::Rotate, 0.0, Matrices::Rotation(Math::Deg2Rad(angleDeg))); }
		/** Initialize parameters as skew transformation. */
		MatrixTransformation & InitSkewX(f64 skewDeg) { return Init(Types::SkewX, 0.0, { 1.0, Math::Tg(Math::Deg2Rad(skewDeg)), 0.0, 1.0 }); }
		/** Initialize parameters as skew transformation. */
		MatrixTransformation & InitSkewY(f64 skewDeg) { return Init(Types::SkewY, 0.0, { 1.0, 0.0, Math::Tg(Math::Deg2Rad(skewDeg)), 1.0 }); }
		/** Initialize parameters as scale transformation. */
		MatrixTransformation & InitScale(double2 scale) { return Init(Types::Scale, 0.0, double2x2(scale.x, 0.0, 0.0, scale.y)); }
		/** Initialize parameters as unit transformation - does not have explicit meaning in svg, just technical usage. */
		MatrixTransformation & InitUnit() { return Init(Types::Unit, 0.0, { 1.0, 0.0, 0.0, 1.0 }); }
		/** Initialize parameters as matrix transformation. */
		MatrixTransformation & InitMatrix(double2 offset, double2x2 rotation) { return Init(Types::Matrix, offset, rotation); }
		/** Initialize all parameters by one call. */
		MatrixTransformation & Init(Types type, double2 offset, double2x2 rotation) { Type = Type, Offset = offset, Rotation = rotation; return *this; }
		/** Append the transformation to the given string in text fotm. */
		void AppendString(String & target) const {
			switch (Type) {
			case Types::Scale:
				target.AppendBatch("scale(", Rotation.Cols(0)[0], ",", Rotation.Cols(1)[1], ")");
				break;
			case Types::SkewX:
				target.AppendBatch("skewX(", Math::Rad2Deg(Math::ArcTg(Rotation.Cols(1)[0])), ")");
				break;
			case Types::SkewY:
				target.AppendBatch("skewY(", Math::Rad2Deg(Math::ArcTg(Rotation.Cols(0)[1])), ")");
				break;
			case Types::Translate:
				target.AppendBatch("translate(", Offset.x, ",", Offset.y, ")");
				break;
			case Types::Rotate:
				target.AppendBatch("rotate(", Math::Rad2Deg(Matrices::Rotation2Rad(Rotation)), ",", Offset.x, ",", Offset.y, ")");

				break;
			case Types::Matrix:
				target.AppendBatch("matrix(",
					Rotation.Cols(0)[0], ",",
					Rotation.Cols(1)[0], ",",
					Offset.x, ",",
					Rotation.Cols(0)[1], ",",
					Rotation.Cols(1)[1], ",",
					Offset.y, ")"
				);

				break;

			case Types::None:
			case Types::Unit:
			default:
				break;
			}

		}
		/** Apply transformation to given cooedinates. */
		double2 Apply(const double2 & input) const { return Rotation * input + Offset; }
		/** Try parse text as transformation. */
		bool TryParse(Text::StringView16 t);
	};


}