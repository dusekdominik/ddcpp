#pragma once
namespace DD::Svg {
	/**
	 * Svg reader, converts xml elements into path commands.
	 * Ignores things like colors or animations.
	 */
	struct Reader;
}

#include <DD/Svg/Document.h>
#include <DD/Debug.h>
#include <DD/Math/Interval.h>
#include <DD/Sequencer/Driver.h>
#include <DD/Svg/Primitives.h>
#include <DD/Svg/Path.h>
#include <DD/Svg/Hash.h>
#include <DD/Svg/Transformation.h>

namespace DD::Svg {
	struct Reader {
		DD_TODO_STAMP("1. Apply Transformation matrices.");
		DD_TODO_STAMP("2. Implement Group reading");
		DD_TODO_STAMP("3. Error reporting");

	public: // statics
		/** Detector for DFS search, which elements should be recursively checked. */
		static bool Follow(Text::StringView16 text);
		/** Helper method for loading numeric parameters of a single path command. */
		static void ReadNumbers(Text::StringView16 view, ArrayView1D<f64 *> numbers);

	public: // properties
		/** Reference to source document, for id search. */
		Reference<Svg::DocumentBase> Document;
		/** Element that is being read. */
		XML::Element & Element;
		/** Target where to put the deserialization. */
		Svg::Path & Path;

		/**
		 * Take element, convert it to a PRIMITIVE, and append to the path.
		 * Expected are elements from DD/Svg/Primitives.h,
		 */
		template<typename PRIMITIVE>
		bool AppendElement();
		/** Decode element to a path. */
		void AppendPath(const XmlRectangle & x);
		void AppendPath(const XmlEllipse & x);
		void AppendPath(const XmlCircle & x);
		void AppendPath(const XmlLine & x);
		void AppendPath(const XmlPolyline & x);
		void AppendPath(const XmlPolygon & x);
		void AppendPath(const XmlPath & x);
		void AppendPath(const XmlGroup & x);
		void AppendPath(const XmlUse & x);
		/** Deserialize Element. */
		bool Deserialize();
		/** Method for reading pairs of points, used for deserialization of "polyline" and "polygon" tags. */
		bool ReadPoints(Text::StringView16 points);
	};
}

namespace DD::Svg {
	template<typename PRIMITIVE>
	bool Reader::AppendElement() {
		// use default value for initialization
		PRIMITIVE primitive;
		// try to deserialize xml element (text form) to the digital form
		if (!DD::Xml::Mini::Deserialize(Element, primitive))
			return false;

		// if deserialization is successful, append element to the path
		size_t pathLength = Path.Commands.Size();
		AppendPath(primitive);

		// apply transformation
		IPrimitive & p = static_cast<IPrimitive &>(primitive);
		if (p.Transformation.IsNotEmpty()) {
			Transformation t;
			if (t.TryParse(p.Transformation)) {
				MatrixTransformation mt = t.Evaluate();
				Path.Transform(mt.Rotation, mt.Offset, pathLength);
			}
			else {
				throw;
			}
		}


		return true;
	}
}

