#pragma once
namespace DD::Svg {
	/**
	 * Svg document file.
	 * Smart pointer object.
	 */
	struct Document;

	/**
	 * Native svg document representation.
	 */
	struct DocumentBase;
}

#include <DD/FileSystem/Path.h>
#include <DD/Matrix.h>
#include <DD/Primitives.h>
#include <DD/Sequencer/Driver.h>
#include <DD/String.h>
#include <DD/Svg/Path.h>
#include <DD/Svg/Primitives.h>
#include <DD/Svg/Reader.h>
#include <DD/Vector.h>
#include <DD/XML.h>

namespace DD::Svg {
	struct Document
		: public Reference<DocumentBase>
	{
		/** Load svg path from the given path. */
		Document(const FileSystem::Path & path);
		/** Create new document with given viewbox dimensions. */
		Document(const double2 & dims);
		/** Create new document with given viewbox dimensions. */
		Document(f64 w, f64 h);
		/** Grab the given pointer and wrap it up. */
		Document(DocumentBase * doc) : Reference(doc) {}
		/** Zero constructor. */
		Document() = default;
	};

	struct DocumentBase
		: public IReferential
	{
	public: // statics
		/** Attribute name. */
		static constexpr Text::StringView16 ATTRIBUTE_WIDTH   = L"width";
		/** Attribute name. */
		static constexpr Text::StringView16 ATTRIBUTE_HEIGHT  = L"height";
		/** Attribute name. */
		static constexpr Text::StringView16 ATTRIBUTE_VIEWBOX = L"viewbox";
		/** Element name. */
		static constexpr Text::StringView16 ELEMENT_DEFS      = L"defs";

	public: // properties
		/** XML document, representing svg.  */
		XML::Document XMLDocument;
		/**
		 * Orientation of Y axis in SVG doc is mirroring current system (for gcodes).
		 * This value serve for mirroring it correctly.
		 */
		f64 YCorrection;
		/** Mapping from id's to given document. */
		BTree<Text::StringView16, XML::Element*> Id2Element;

	public: // methods
		/**
		 * Add path (binary form) to the svg document (text form).
		 * @param path        - binary path object.
		 * @param fillColor   - svg property of the object.
		 * @param strokeColor - svg property of the object.
		 *
		 */
		void AddPath(
			const Path & path,
			Text::StringView16 fillColor = (L"none"),
			Text::StringView16 strokeColor = (L"none")
		);
		/**
		 * Add rectangle to the svg document.
		 * @param rect        - descriptor of the rectangle.
		 * @param fillColor   - styling property.
		 * @param strokeColor - styling property.
		 */
		void AddRectangle(
			const Math::Interval2D & rect,
			Text::StringView16 fillColor = (L"none"),
			Text::StringView16 strokeColor = (L"none")
		);
		/** Add axes aligned ellipse. */
		void AddEllipse(
			const Math::Interval2D & rect,
			Text::StringView16 fillColor = (L"none"),
			Text::StringView16 strokeColor = (L"none")
		);
		/** Add text. */
		void AddText(
			double2 position,
			Text::StringView16 text,
			Text::StringView16 fillColor = (L"none"),
			Text::StringView16 strokeColor = (L"none")
		);
		/**
		 * Get sequencer for path reading from the xml document.
		 * Templated temporarily to use deferred compilation.
		 */
		template<size_t = 0>
		auto ReadPaths() {
			DD_TODO_STAMP("XML should be handling namespaces and the check should be against the correct svg namespace.");
			Reference<DocumentBase> r(this);
			return XMLDocument->GetRoot()
				.Search([r](XML::Element & e) { return Svg::Reader::Follow(e.GetName()); })
				.FilterEx([r](XML::Element & e, Svg::Path & p) {
					return e.GetNamespace().IsEmpty() && Svg::Reader{ r, e, p.Clear() }.Deserialize();
				});
		}

		/** Load an xml document */
		bool Load(const FileSystem::Path & path) { return XMLDocument.LoadUtf8(path); }
		/** Save svg to given path (UTF8). */
		void Save(const FileSystem::Path & path) { SaveUtf8(path); }
		/** Save svg to given path (UTF8). */
		void SaveUtf8(const FileSystem::Path & path);
		/** Save svg to given path UTF16. */
		void SaveUtf16(const FileSystem::Path & path);
		/** Get document width. */
		f64 Width() const { f64 result = 0.0; XMLDocument->GetRoot()[ATTRIBUTE_WIDTH].Trim<Text::IsNumeric, false>().TryCast(result); return result; }
		/** Get document height. */
		f64 Height() const { f64 result = 0.0; XMLDocument->GetRoot()[ATTRIBUTE_HEIGHT].Trim<Text::IsNumeric, false>().TryCast(result); return result; }
		/** */
		double2 Dimensions() const { return { Width(), Height() }; }

	public: // constructors
		/**
		 * Basic constructor.
		 * @param w - width of the document in millimeters.
		 * @param h - height of the document in millimeters.
		 */
		DocumentBase(f64 w, f64 h);
		/** Read Svg file from the given file. */
		DocumentBase(const FileSystem::Path & path);
	};
}
