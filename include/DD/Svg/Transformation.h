#pragma once
namespace DD::Svg {
	/**
	 * Set of transformations.
	 * In Svg transformation attribute could contain multiple transformations.
	 * This class serves to group them under all.
	 */
	struct Transformation;
}

#include <DD/List.h>
#include <DD/Svg/MatrixTransformation.h>

namespace DD::Svg {
	/** Set of transformations. */
	struct Transformation {
	public: // properties
		/** List with transformations. */
		List<MatrixTransformation> Transformations;

	public: // methods
		/** Apply transformation to the given point. */
		double2 Apply(const double2 & input);
		/** Append the transformation in the form of text to the given string. */
		void AppendString(String & target) const;
		/** Try parse the given text to the transformation. */
		bool TryParse(const Text::StringView16 & text);
		/** Get compound transformation from all the transformations in this set. */
		MatrixTransformation Evaluate() {
			MatrixTransformation t;
			t = Transformations.First();
			for (size_t i = 1; i < Transformations.Size(); ++i) {
				t.Offset += Transformations[i].Offset;
				t.Rotation *= Transformations[i].Rotation;
			}

			return t;
		}
	};
}
