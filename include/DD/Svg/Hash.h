#pragma once
#include <DD/Text/StringView.h>

namespace DD::Svg {
	/** Type of the hash in Svg namespace. */
	using HashType = u32;
	/** Hash function used for transformation hashing. */
	constexpr HashType Hash(const Text::StringView16 & str) { return str.Hash<HashType, Text::HashMethods::FirstLastCI>(); };
}
