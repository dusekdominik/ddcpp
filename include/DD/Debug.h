#pragma once
namespace DD::Debug {
	/** Interface for managing all registered debug vars. */
	struct IVariables;

	/** Interface for recording/playing state of debug variables. */
	struct IRecorder;

	/**
	 * Helper structure for deriving a type from debug variable.
	 * @param VISITOR - type derived visitor to be correctly parsed.
	 * @param PARAM   - param of visitor function.
	 */
	template<typename VISITOR, typename PARAM>
	struct IVisitor;

	/**
	 * Debug variable for assertion.
	 * @see DD_ASSERT, DD_ASSERT_SIMPLE
	 */
	struct Assert;

	/** Debug variable for boolean values. */
	struct Boolean;

	/** Debug variable for invoking an action. */
	struct Button;

	/** Debug variable for picking an option from a string list. */
	struct Options;

	/** Debug variable for loading paths. */
	struct Path;

	/**
	 * Debug enumeration.
	 * @example
	 *   DD_ENUM(States, STATE_ONE, STATE_TWO, STATE_THREE);
	 *   static Debug::Enum<States> myEnum(STATE_ONE, "MyModule", "MyEnum");
	 *   while (1) switch ((States)myEnum) {
	 *      case States::STATE_ONE:   processStateOne();   break;
	 *      case States::STATE_TWO:   processStateTwo();   break;
	 *      case States::STATE_THREE: processStateThree(); break;
	 *   }
	 */
	template<typename ENUM>
	struct Enum;

	/**
	 * Debug output.
	 * Use macros for std behavior.
	 * @see DD_LOG, DD_DEBUG_LOG   // Simple log
	 * @see DD_LOGF, DD_DEBUG_LOGF // Formatted log
	 * @see DD_VARS, DD_DEBUG_VARS // Simple logging of variables
	 * @see DD_WARNING(...)        // Invocation of code breaking
	 */
	struct Log;

	/**
	 * Debug native type or string.
	 * @example update from code
	 *   static Task t([myInt]() {
	 *     for(Debug::i32 myInt(42, "MyModule", "MyInt"); true; myInt = myInt + 1;)
	 *       Thread::Sleep(1000);
	 *   });
	 */
	template<typename T>
	struct Type;

	/**
	 * Debug native type with min-max boundaries.
	 * @example common usage
	 *   static Debug::f32Range unit(0.f, 0.f, 1.f, "MyModule", "Unit interval");
	 */
	template<typename T>
	struct Range;

	/**
	 * Method for debug breaking.
	 * If a debugger is attached, breaks the code, otherwise does nothing.
	 * @see DD_WARNING macro.
	 * @example
	 *   if (value < 0)
	 *      Debug::Break();         // code breaks when sqrt is undefined
	 *   value = Math::Sqrt(value);
	 */
	void Break();

	/** Crash the application by call. */
	void Fail();
}

/** Are macros enabled. */
#define DD_DEBUG_CODE_ENABLED _DEBUG

#if DD_DEBUG_CODE_ENABLED
#define DD_DEBUG_CODE(...) __VA_ARGS__
#else
#define DD_DEBUG_CODE(...)
#endif

/**
 * DD_ASSERT(condition, [description], [mode])
 * Advanced assertion.
 * @see DD_ASSERT_SIMPLE
 * Tool allows to observe how many times has assertion passed/failed.
 * or manage debugger breaking behavior.
 * @example
 *  void int saveMyAge(int age) {
 *    DD_ASSERT(0 <= age && age <= 120, "Check if the age is valid for human.", LOG);
 *    ...
 *  }
 * @param condition   - condition which should be satisfied to not fail the assert
 * @param description - a comment why the assert is needed
 * @param mode        - one of following modes [DISABLED|SILENT|LOG|BREAK|INTERACTIVE|FAIL]
 */
#define DD_ASSERT(...) DD_DEBUG_CODE(_DD_OVERLOAD(DD_ASSERT, __VA_ARGS__))
#define DD_ASSERT_1(condition) DD_ASSERT_2(condition, "")
#if DD_DEBUG_CODE_ENABLED
#define DD_ASSERT_2(condition, description) DD_ASSERT_3(condition, "", BREAK)
#else
#define DD_ASSERT_2(condition, description) DD_ASSERT_3(condition, "", LOG)
#endif
#define DD_ASSERT_3(condition, description, mode)\
  static ::DD::Debug::Assert DD_PLAY(DD_CAT_2, _DD_ASSERT_, __LINE__)(#condition, __FUNCSIG__, __FILE__, __LINE__, description, ::DD::Debug::Assert::Modes::mode);\
  DD_PLAY(DD_CAT_2, _DD_ASSERT_, __LINE__)(condition);
/**
 * Simple assertion.
 * If debugger is present breaks and the assertion fails it breaks the debugger.
 * @example
 *   void int saveTemperatureInCelsius(float temperature) {
 *     DD_ASSERT_SIMPLE(-273.15f < temperature);
 *     ...
 *   }
 */
#define DD_ASSERT_SIMPLE(condition, ...) DD_DEBUG_CODE( { if (!(condition)) { DD_WARNING(__VA_ARGS__); }});
/**
 * Shortcut for debug printing.
 * Arguments would be printed on a line one by one separated by \t.
 * @example
 *   DD_LOG("Hello", "world", 42);  // Prints out "Hello\tworld\t42\t\n"
 */
#define DD_LOG(...) ::DD::Debug::Log::PrintTabLine(__VA_ARGS__);
/**
 * Log with stamp printing.
 * First arg should be cstring, if not, put an empty cstring to the first place.
 * @example using empty cstring.
 *	DD_LOG_STAMP("", 4);
 */
#define DD_LOG_STAMP(...) ::DD::Debug::Log::PrintLine(__FILE__ "(" DD_SCAT_1(__LINE__) "): " __FUNCTION__ "\n\t\t" __VA_ARGS__)
/**
 * Shortcut for C#-like debug printing.
 * @see DD::String::Format() for usage details.
 * @example
 *  DD_LOGF("{0} = {1}", "living", "dying"); // Prints out "living = dying"
 */
#define DD_LOGF(...) ::DD::Debug::Log::PrintFormat(__VA_ARGS__);
/**
 * Helper macro for DD_VARS.
 * Prints one variable output.
 * DD_VAR(varName) ~ [varType] [varName] = [varValue].
 * @see DD_VARS
 */
#define DD_VAR(var) ::DD::Debug::Log::Print(::DD::Debug::Log::Type<decltype(var)>{}, " " #var, " = ", var, ";\t");
/**
 * DD_VARS(getterOrVar0, ...)
 * Prints a batch of DD_VAR format info into single line.
 * @example
 *   int data0, data1, data2, data3;
 *   loadData(data0, data1, data2, data3);
 *   DD_VARS(data0, data1, data2, data3);
 */
#define DD_VARS(...) {\
	::DD::Debug::Log::Print(__FILE__ "(" DD_SCAT_1(__LINE__) "): "); \
	::DD::Debug::Log::Print("\t");  DD_XLIST(DD_VAR, __VA_ARGS__) ::DD::Debug::Log::PrintLine();\
}
 /**
	* DD_WARNING(output, ...)
	* Prints output and breaks debugger if debugger is present.
	* @example
	*   if (!fileExists(file)) {
	*     DD_WARNING("File does not exist:", file);
	*   }
	*/
#define DD_WARNING(...) DD_LOG_STAMP("Warning: ", __VA_ARGS__), ::DD::Debug::Break()
/**
 * DD_LOGC(log, [mode, [name, [description]]])
 * @param log  - argument or arguments in brackets to be printed out
 * @param mode - way of printing log [DISABLED|SILENT|LOG|PRINT|LOG_EX|PRINT_EX]
 * @param name - name of log (serves for searching log between debug variables)
 * @param desc - description of log (help of debug variable)
 * @example
 *  void myFnc(const char * param0, const char * param1) {
 *    DD_LOGC("MyLog", "Log is printing out input of the function.", ("Param0 = ", param0, ", Param1 = ", param1), SILENT);
 *    ...
 *  }
 */
#define DD_LOGC(...) DD_DEBUG_CODE(_DD_OVERLOAD(DD_LOGC, __VA_ARGS__));
#define DD_LOGC_1(log)             DD_LOGC_2(log, SILENT)
#define DD_LOGC_2(log, mode)       DD_LOGC_3(log, mode, __FUNCTION__ "(" DD_SCAT_1(__LINE__) ")")
#define DD_LOGC_3(log, mode, name) DD_LOGC_4(log, mode, name, "No description")
#define DD_LOGC_4(log, mode, name, desc) _DD_LOGC(log, mode, name, desc, Print)
 /**
	* DD_LOGC(log, [mode, [name, [description]]])
	* @param log  - argument or arguments in brackets to be printed out
	* @param mode - way of printing log [DISABLED|SILENT|LOG|PRINT|LOG_EX|PRINT_EX]
	* @param name - name of log (serves for searching log between debug variables)
	* @param desc - description of log (help of debug variable)
	* @example
	*  void myFnc(const char * param0, const char * param1) {
	*    DD_LOGC("MyLog", "Log is printing out input of the function.", ("Param0 = ", param0, ", Param1 = ", param1), SILENT);
	*    ...
	*  }
	*/
#define DD_LOGCF(...) DD_DEBUG_CODE(_DD_OVERLOAD(DD_LOGCF, __VA_ARGS__));
#define DD_LOGCF_1(log)             DD_LOGCF_2(log, SILENT)
#define DD_LOGCF_2(log, mode)       DD_LOGCF_3(log, mode, __FUNCTION__ "(" DD_SCAT_1(__LINE__) ")")
#define DD_LOGCF_3(log, mode, name) DD_LOGCF_4(log, mode, name, "No description")
#define DD_LOGCF_4(log, mode, name, desc) _DD_LOGC(log, mode, name, desc, PrintFormat)
/** Helper for creating logs. */
#define _DD_LOGC(log, mode, name, desc, method)\
	static ::DD::Debug::Log DD_PLAY(DD_CAT_2, _DD_LOG_, __LINE__)(name, desc, __FUNCTION__, __FILE__, __LINE__, ::DD::Debug::Log::Modes::mode);\
  DD_PLAY(DD_CAT_2, _DD_LOG_, __LINE__)->method(DD_UNWRAP(log));

/** Turned on. @see DD_ASSERT */
#define DD_DEBUG_ASSERT(...)        DD_DEBUG_CODE(DD_EXPAND(DD_ASSERT(__VA_ARGS__)))
/** Turned on. @see DD_ASSERT_SIMPLE */
#define DD_DEBUG_ASSERT_SIMPLE(...) DD_DEBUG_CODE(DD_PLAY(DD_ASSERT_SIMPLE, __VA_ARGS__))
/** Turned on. @see DD_LOG */
#define DD_DEBUG_LOG(...)           DD_DEBUG_CODE(DD_PLAY(DD_LOG, __VA_ARGS__))
/** Turned on. @see DD_LOGF */
#define DD_DEBUG_LOGF(...)          DD_DEBUG_CODE(DD_PLAY(DD_LOGF, __VA_ARGS__))
/** Turned on. @see DD_VAR */
#define DD_DEBUG_VAR(...)           DD_DEBUG_CODE(DD_PLAY(DD_VAR, __VA_ARGS__))
/** Turned on. @see DD_VARS */
#define DD_DEBUG_VARS(...)          DD_DEBUG_CODE(DD_PLAY(DD_VARS, __VA_ARGS__))
/** Turned on. @see DD_WARNING */
#define DD_DEBUG_WARNING(...)       DD_DEBUG_CODE(DD_PLAY(DD_WARNING, __VA_ARGS__))

#include "Macros.h"
#include "Boolean.h"
#include "Enum.h"
#include "Flag.h"
#include "Delegate.h"
#include "Reference.h"
#include "String.h"

namespace DD::Debug {
	/************************************************************************/
	/* Auxiliary                                                            */
	/************************************************************************/
	#pragma region Auxiliary
	DD_ENUM_32(
		(VariableTypes, "type of debug variable"),
		(Undefined, 0, "undefined control"),
		(i8,        1, "signed 8-bit integer"),
		(i16,       2, "signed 16-bit integer"),
		(i32,       3, "signed 32-bit integer"),
		(i64,       4, "signed 64-bit integer"),
		(u8,        5, "unsigned 8-bit integer"),
		(u16,       6, "unsigned 16-bit integer"),
		(u32,       7, "unsigned 32-bit integer"),
		(u64,       8, "unsigned 64-bit integer"),
		(f32,       9, "32-bit float"),
		(f64,      10, "64-bit float"),
		(u8Range,  11, "unsigned  8-bit integer with upper and lower limits"),
		(u16Range, 12, "unsigned 16-bit integer with upper and lower limits"),
		(u32Range, 13, "unsigned 32-bit integer with upper and lower limits"),
		(u64Range, 14, "unsigned 64-bit integer with upper and lower limits"),
		(i8Range,  15, "signed  8-bit integer with upper and lower limits"),
		(i16Range, 16, "signed 16-bit integer with upper and lower limits"),
		(i32Range, 17, "signed 32-bit integer with upper and lower limits"),
		(i64Range, 18, "signed 64-bit integer with upper and lower limits"),
		(f32Range, 19, "32-bit float"),
		(f64Range, 20, "64-bit float"),
		(Boolean,  21, "32-bit bool"),
		(String,   22, "string"),
		(Button,   23, "button control"),
		(Enum,     24, "enumeration control"),
		(Assert,   25, "assertion"),
		(Log,      26, "conditional log"),
		(Path,     27, "path to a file or directory"),
		(Options,  28, "option list")
	);

	template<typename T> struct TypeControlTable { constexpr static VariableTypes TypeHash = VariableTypes::Undefined; };
	template<> struct TypeControlTable<f32>      { constexpr static VariableTypes TypeHash = VariableTypes::f32,    RangeHash = VariableTypes::f32Range;  };
	template<> struct TypeControlTable<f64>      { constexpr static VariableTypes TypeHash = VariableTypes::f64,    RangeHash = VariableTypes::f64Range;  };
	template<> struct TypeControlTable<i8>       { constexpr static VariableTypes TypeHash = VariableTypes::i8,     RangeHash = VariableTypes::i8Range;   };
	template<> struct TypeControlTable<i16>      { constexpr static VariableTypes TypeHash = VariableTypes::i16,    RangeHash = VariableTypes::i16Range;  };
	template<> struct TypeControlTable<i32>      { constexpr static VariableTypes TypeHash = VariableTypes::i32,    RangeHash = VariableTypes::i32Range;  };
	template<> struct TypeControlTable<i64>      { constexpr static VariableTypes TypeHash = VariableTypes::i64,    RangeHash = VariableTypes::i64Range;  };
	template<> struct TypeControlTable<u8>       { constexpr static VariableTypes TypeHash = VariableTypes::u8,     RangeHash = VariableTypes::u8Range;   };
	template<> struct TypeControlTable<u16>      { constexpr static VariableTypes TypeHash = VariableTypes::u16,    RangeHash = VariableTypes::u16Range;  };
	template<> struct TypeControlTable<u32>      { constexpr static VariableTypes TypeHash = VariableTypes::u32,    RangeHash = VariableTypes::u32Range;  };
	template<> struct TypeControlTable<u64>      { constexpr static VariableTypes TypeHash = VariableTypes::u64,    RangeHash = VariableTypes::u64Range;  };
	template<> struct TypeControlTable<String>   { constexpr static VariableTypes TypeHash = VariableTypes::String, RangeHash = VariableTypes::Undefined; };

	DD_FLAG_32(
		(Sources, "Descriptor of sources which can modify the Debug::Variable value."),
		(CODE,   "Common source - invoked by native operators of debug variables."),
		(GUI,    "Gui console (separate window with debug variables)."),
		(ENGINE, "Engine console (dialog in render target window)."),
		(SERIAL, "Serialization db, possibly called while variable is being created."),
		(TCP,    "External tcp console."),
		_6, // reserved source
		_7, // reserved source
		_8  // reserved source
	);
	#pragma endregion

	/************************************************************************/
	/* Variable                                                             */
	/************************************************************************/
	#pragma region Variable
	struct IVariableBase
		: public IReferential
	{
	protected: // properties
		/** Type of variable. */
		VariableTypes _type;
		/** List of source which can modify the variable. */
		Sources _source;
		/** Description of variable. */
		StringLocal<256> _description;
		/** Category of variable. */
		StringLocal<64>  _category;
		/** Name of variable */
		StringLocal<64>  _name;

	public: // events
		/** Event invoked while value of variable has changed. */
		Event<IVariableBase*, Sources> OnValueChanged;

	public: // methods
		/** Getter of variable name. */
		Text::StringView16 Name() const { return _name; }
		/** Getter of variable category. */
		Text::StringView16 Category() const { return _category; }
		/** Getter of variable description. */
		Text::StringView16 Description() const { return _description; }
		/** Getter of variable type. */
		VariableTypes Type() const { return _type; }
		/** Getter of enabled sources of variable. */
		Sources Source() const { return _source; }
		/** Stringize Debug::Variable. */
		void ToString(DD::String &) const;
		/**
		 * Save value of the variable to a serialization file.
		 * Value will be restored while next startup.
		 */
		void Saved(bool);
		/** Is Value saved. */
		bool Saved() const;
		/**
		 * Set tracking.
		 * If tracking is enabled each change of variable will be saved.
		 * to the serialization file and restored while next startup.
		 */
		void Tracked(bool);
		/** Is variable tracked. */
		bool Tracked() const;

	public: // constructors
		/** Constructor accepting its own type. */
		IVariableBase(
			VariableTypes type,
			const Text::StringView16 & category,
			const Text::StringView16 & name,
			const Text::StringView16 & description,
			Sources source
		);
		/** Destructor. */
		virtual ~IVariableBase() = default;
	};

	/** Variable. */
	typedef Reference<IVariableBase> Variable;
	typedef Delegate<IVariableBase *, Sources> Callback;
	#pragma endregion

	/************************************************************************/
	/* Category                                                             */
	/************************************************************************/
	#pragma region Category
	/** Interface for enumerating debug variables. */
	struct ICategoryBase
		: public IReferential
	{
		/** Event invoked if a new variable has been registered. */
		Event<IVariableBase *> OnVariableCreate;

		/** Enumerate debug variables in category. */
		virtual void Enumerate(IAction<IVariableBase *> *) = 0;
		/** Get name of category. */
		virtual Text::StringView16 Name() const = 0;
	};

	/** Category. */
	typedef Reference<ICategoryBase> Category;
	#pragma endregion

	/************************************************************************/
	/* Variables                                                            */
	/************************************************************************/
	#pragma region Variables
	/** Interface for enumerating debug variables. */
	struct IVariables {
		/** Event invoked when a new variable is registered. */
		Event<IVariableBase *> OnVariableCreate;
		/** Event invoked when a new category is registered. */
		Event<ICategoryBase *> OnCategoryCreate;

		/** Initialize gui variable manager. */
		virtual void InitGui() = 0;
		/** Create control. */
		virtual void Create(IVariableBase *) = 0;
		/** Get variable if exists, nullptr otherwise. */
		virtual IVariableBase * Get(const Text::StringView16 & category, const Text::StringView16 & variable) = 0;
		/** Enumerate categories of debug variables. */
		virtual void Enumerate(IAction<ICategoryBase *> *) = 0;
		/** Enumerate all debug variables. */
		virtual void Enumerate(IAction<IVariableBase *> *) = 0;
		/** Get exclusive access to the variables. */
		virtual void Lock() = 0;
		/** Recursive lock. */
		virtual void UnLock() = 0;
	} * Variables();
	#pragma endregion

	/************************************************************************/
	/* Assert                                                               */
	/************************************************************************/
	#pragma region Assert
	struct AssertBase
		: public IVariableBase
	{
		friend struct Assert;
		/** Modes of assert. */
		DD_ENUM_32(
			(Modes, "Mode of assertion."),
			(DISABLED,    0, "Do nothing."),
			(SILENT,      1, "Count (in)valid ticks only."),
			(LOG,         2, "Count (in)valid ticks and log when tick fails."),
			(BREAK,       3, "Count + log + break when tick fails and debugger is present."),
			(INTERACTIVE, 4, "Count + log + ask user what to do when tick fails."),
			(FAIL,        5, "Count + log + break if debugger is prsent, otherwise crash the app.")
		);

	private:
		/** Behavior of the assert. */
		Modes _mode;

	public:
		/** Ticks of assert. */
		DD::i32 InvalidTicks, ValidTicks;

	public: // methods
		/** Assert check. */
		void Check(bool assert);
		/** Behavior of the assert. */
		void Value(Modes mode, Sources s) { if (s & _source) { _mode = mode; OnValueChanged(this, s); } }
		/** Behavior of the assert. */
		Modes Value() const { return _mode; }

	private: // constructors
		/** Constructor. */
		AssertBase(
			const char * check,
			const char * function,
			const char * file,
			::DD::u32    line,
			const char * description,
			Modes        mode,
			IVariables * vars
		);
	};


	struct Assert
		: public Reference<AssertBase>
	{
		/** Modes of assert. */
		typedef AssertBase::Modes Modes;

		/** Assert check. */
		void operator()(bool check) { Ptr()->Check(check); }

		/** Nullptr constructor. */
		Assert() : Reference() {}
		/**
		 * Constructor.
		 * @param condition   - stringized condition of valid assert pass.
		 * @param function    - name of function where assert is placed.
		 * @param file        - file where assert is placed.
		 * @param line        - line where assert is placed in @param file.
		 * @param description - detail description of assert.
		 */
		Assert(
			const char * condition,
			const char * function,
			const char * file,
			::DD::u32    line,
			const char * description,
			Modes        mode,
			IVariables * vars = Variables()
		);
	};
	#pragma endregion

	/************************************************************************/
	/* Log                                                                  */
	/************************************************************************/
	#pragma region Log
	struct LogBase
		: public IVariableBase
	{
		/** Modes of log. */
		DD_ENUM_32(
			(Modes, "Mode of log."),
			(DISABLED, 0, "Do not do anything."),
			(SILENT,   1, "Count ticks."),
			(LOG,      2, "Log input into the main log."),
			(PRINT,    3, "Log stamp and input into main log."),
			(LOG_EX,   4, "Log input into extern log."),
			(PRINT_EX, 5, "Log stamp and input into extern log.")
		);

	private: // properties
		/** Mode of log. */
		Modes _mode;

	public: // properties
		/** State settings of log. */
		DD::i32 Ticks;
		/** Stamp of log. */
		DD::String Stamp;

	protected: // methods
		/** perform one tick. */
		void tick();
		/** Print into external log. */
		void printEx(const DD::String & log);

	public: // methods
		/** Perform log action. @see DD::String::From() */
		template<typename...ARGS>
		void Print(const ARGS &...args);
		/** Perform log action. @see DD::String::Format(). */
		template<typename...ARGS>
		void PrintFormat(const ARGS &...args);
		/** Set mode of log. */
		void Value(Modes mode, Sources s) { if (s & _source) { _mode = mode; OnValueChanged(this, s); } }
		/** Get Mode of log. */
		Modes Value() const { return _mode; }

	public: // constructors
		/** Constructor. */
		LogBase(
			const char * name,
			const char * description,
			const char * function,
			const char * file,
			DD::i32 line,
			Modes mode,
			IVariables * vars
		);
	};


	struct Log
		: public Reference<LogBase>
	{
		/** Helper. */
		template<typename T>
		struct Type {};
		/** Helper for overriding log format for given type. */
		template<typename T>
		struct Formatter {
			/** Default formatting function */
			static void Format(::DD::String & buffer, const T & value) { buffer.Append(value); }
		};
		template<typename T>
		struct Formatter<Type<T>> {
			/** Default formatting function */
			static void Format(::DD::String & buffer, const Type<T> &) { buffer.Append("__NOT_SUPPORTED__"); }
		};
		/** Modes of log instance. */
		typedef LogBase::Modes Modes;

	private: // static methods
		/** Thread-local buffer for parsing the output. */
		static DD::String & getBuffer() { static thread_local StringLocal<1024> _buffer; return _buffer; }
		/** pus */
		template<typename T>
		static void format(const T & arg) { Formatter<T>::Format(getBuffer(), arg); }
		/** Flush buffer to console. */
		static void flush();
		/** Variadic print. */
		template<typename ARG, typename...ARGS>
		static void print(const ARG & arg, const ARGS &...args) { format(arg), print(args...); }
		static void print() { flush(); }
		/** Variadic print separated by tabs. First call. */
		template<typename ARG, typename...ARGS>
		static void printtbd(const ARG & arg, const ARGS &...args);
		/** Variadic print separated by tabs. Ordinary call. */
		template<typename ARG, typename...ARGS>
		static void printtb(const ARG & arg, const ARGS &...args);
		/** Variadic print separated by tabs. Last call. */
		static void printtb() { flush(); }
		/** Prints typename. */
		template<typename ARG>
		static void printtype();
		/** Prints typename. */
		template<typename ARG, typename ARG1, typename...ARGS>
		static void printtype();

	public: // static methods
		/** Print arg by arg. */
		template<typename...ARGS>
		static void Print(const ARGS &...args) { print(args...); }
		/** Print arg by arg with string formatting. */
		template<typename...ARGS>
		static void PrintFormat(const char * formatter, const ARGS &...args) { getBuffer().AppendFormat(formatter, args...); flush(); }
		/** Print arg by arg with \n at the end. */
		template<typename...ARGS>
		static void PrintLine(const ARGS &...args) { print(args..., '\n'); }
		/** Print arg by arg separated by tabs. */
		template<typename...ARGS>
		static void PrintTab(const ARGS &...args) { printtbd(args...); }
		/** Print arg by arg separated by tabs with \n at the end.  */
		template<typename...ARGS>
		static void PrintTabLine(const ARGS &...args) { printtbd(args..., '\n'); }
		/** Print types. */
		template<typename...ARGS>
		static void PrintType() { printtype<ARGS...>(); }

	public: // methods
		/** Functor for log printing. */
		template<typename...ARGS>
		void operator()(const ARGS&...args) { Ptr()->Print(args...); }

	public: // constructors
		/**
		 * Constructor.
		 * @param name        - name of the log
		 * @param function    - name of function where log is placed
		 * @param file        - file where debug log is placed
		 * @param line        - line of log in code
		 * @param description - description what log means
		 * @param mode        - mode of log
		 */
		Log(
			const char * name,
			const char * description,
			const char * function,
			const char * file,
			DD::i32 line,
			Modes mode,
			IVariables * vars = Variables()
		);
		/** Nullptr constructor. */
		Log() : Reference() {}
	};
	#pragma endregion

	/************************************************************************/
	/* Boolean                                                              */
	/************************************************************************/
	#pragma region Boolean
	struct BooleanBase
		: public IVariableBase
	{
		friend struct Debug::Boolean;

	protected:
		/** Boolean value. */
		DD::Boolean _value;

	public: // methods
		/** Value setter. */
		void Value(bool v, Sources s = Sources::CODE) { if ((s & _source)) { _value = v; OnValueChanged(this, s); } }
		/** Value setter. */
		void Value(DD::Boolean v, Sources s = Sources::CODE) { if ((s & _source)) { _value = v; OnValueChanged(this, s); } }
		/** Value setter. */
		void Value(const DD::Boolean::Value & v, Sources s = Sources::CODE) { if ((s & _source)) { _value = v; OnValueChanged(this, s); } }
		/** Value getter. */
		bool Value() const { return _value; }

	protected: // constructors
		/** Constructor. */
		BooleanBase(
			bool value,
			const Text::StringView16 & module,
			const Text::StringView16 & name,
			const Text::StringView16 & help,
			Sources source,
			IVariables * vars
		);
	};


	struct Boolean
		: public Reference<BooleanBase>
	{
		/** Parent class. */
		using Base = Reference<BooleanBase>;
		using Reference::operator=;

		/** Conversion to native type. */
		operator bool() const { return Ptr()->Value(); }
		operator bool() { return Ptr()->Value(); }
		/** Conversion to native type. */
		operator DD::Boolean::Value() const { return Ptr()->Value(); }
		/** Assignment operator. */
		Boolean & operator=(bool v) { Ptr()->Value(v, Sources::CODE); return *this; }
		/** Assignment operator. */
		Boolean & operator=(DD::Boolean::Value v) { Ptr()->Value(v, Sources::CODE); return *this; }

		/** Zero constructor. */
		Boolean() : Reference() {}
		/** Constructor. */
		Boolean(
			bool value,
			const Text::StringView16 & module,
			const Text::StringView16 & name,
			const Text::StringView16 & help = nullptr,
			Sources source = Sources::ALL,
			IVariables * vars = Variables()
		) : Base(new BooleanBase(value, module, name, help, source, vars))
		{}
	};
	#pragma endregion

	/************************************************************************/
	/* Button                                                               */
	/************************************************************************/
	#pragma region Button
	struct ButtonBase
		: public IVariableBase
	{
		friend struct Button;

	protected: // properties
		/** Event captured. */
		bool _buttonPressed;

	public: // methods
		/** Check if the button is pressed, resets the state of button. */
		bool IsPressed(bool reset = true);
		/** Press the button. */
		void Press(Sources s);

	protected: // constructors
		/** Constructor. */
		ButtonBase(
			const Text::StringView16 & module,
			const Text::StringView16 & name,
			const Text::StringView16 & help,
			Sources source,
			IVariables * vars
		);
	};


	struct Button
		: public Reference<ButtonBase>
	{
		using Reference::operator=;

		/** Zero constructor. */
		Button() : Reference() {}
		/** Constructor. */
		Button(
			const Text::StringView16 & module,
			const Text::StringView16 & name,
			const Text::StringView16 & help = nullptr,
			Sources source = Sources::ALL,
			IVariables * vars = Variables()
		) : Reference(new ButtonBase(module, name, help, source, vars))
		{}
	};
	#pragma endregion

	/************************************************************************/
	/* Enum<E>                                                              */
	/************************************************************************/
	#pragma region Enum
	struct IEnumBase
		: public IVariableBase
	{
		/** Get enum descriptor. */
		virtual const ::DD::IEnum::Descriptor * Descriptor() const = 0;
		/** Get native value. */
		virtual void NativeValue(::DD::u64, Sources) = 0;
		/** Set native value. */
		virtual ::DD::u64 NativeValue() const = 0;
		/** Constructor. */
		IEnumBase(
			const Text::StringView16 & module,
			const Text::StringView16 & name,
			const Text::StringView16 & help,
			Sources source
		);
	};


	template<typename ENUM>
	struct EnumBase
		: public IEnumBase
	{
		friend struct Debug::Enum<ENUM>;

	public: // properties
		/** Int value of enum. */
		ENUM _value;

	public: // methods
		/** Index getter. */
		ENUM Value() const { return _value; }
		/** Index setter. */
		void Value(ENUM v, Sources s) { if ((_source & s) && (_value != v)) { _value = v; OnValueChanged(this, s); } }

	public: // IEnmuBase
		void NativeValue(::DD::u64 v, Sources s) override { Value((typename ENUM::Values)v, s); }
		::DD::u64 NativeValue() const override { return (::DD::u64)Value(); }
		const ::DD::IEnum::Descriptor * Descriptor() const override { return ENUM::GetDescriptor(); }

	public: // constructors
		/** Constructor. */
		EnumBase(
			ENUM value,
			const Text::StringView16 & module,
			const Text::StringView16 & name,
			const Text::StringView16 & help,
			Sources source,
			IVariables * vars
		) : IEnumBase(module, name, StringLocal<1024>::From(help, '\n', ENUM::GetDescriptor()->Help()), source)
			, _value(value)
		{ vars->Create(this); }
	};


	using IEnum = Reference<IEnumBase>;


	template<typename ENUM>
	struct Enum
		: public Reference<EnumBase<ENUM>>
	{
		using Base = Reference<EnumBase<ENUM>>;
		using Base::operator=;
		/** Conversion operator. */
		operator const ENUM () const { return Base::Ptr()->Value(); }
		/** Conversion operator for integral values, unary plus is workaround. */
		typename ENUM::Values operator+() const { return Base::Ptr()->Value().ToValue(); }

		/** Assignment operator from native type, */
		Enum & operator=(ENUM e) { Base::Ptr()->Value(e, Sources::CODE); return *this; }
		/** Zero constructor. */
		Enum() : Base() {}
		/** Constructor. */
		Enum(
			ENUM value,
			const Text::StringView16 & module,
			const Text::StringView16 & name,
			const Text::StringView16 & help = nullptr,
			Sources source = Sources::ALL,
			IVariables * vars = Variables()
		) : Base(new EnumBase<ENUM>(value, module, name, help, source, vars)) {}
	};
	#pragma endregion

	/************************************************************************/
	/* Type<T>                                                              */
	/************************************************************************/
	#pragma region Type
	template<typename T>
	struct TypeBase
		: public IVariableBase
	{
		template<typename> friend struct Type;

	protected: // properties
		/** Value of wrapper. */
		T _value;

	public: // methods
		/** Value getter. */
		const T & Value() const { return _value; }
		/** Value setter. */
		void Value(const T & v, Sources s = Sources::CODE) { if ((s & IVariableBase::_source) && (!(_value == v))) { _value = v; IVariableBase::OnValueChanged(this, s); } }

	protected: // constructor
		/** Constructor. */
		TypeBase(
			const T & value,
			const Text::StringView16 & module,
			const Text::StringView16 & name,
			const Text::StringView16 & help ,
			Sources source,
			IVariables * vars
		) : IVariableBase(TypeControlTable<T>::TypeHash, module, name, help, source)
			, _value(value)
		{ vars->Create(this); }
	};

	template<typename T>
	struct Type
		: public Reference<TypeBase<T>>
	{
		using Base = Reference<TypeBase<T>>;
		using Base::operator=;

		/** Assignment operator from native type, */
		Type & operator=(const T & value) { Base::Ptr()->Value(value); return *this; }
		/** Conversion operator. */
		operator const T &() const { return Base::Ptr()->Value(); }
		///** Conversion operator. */
		//operator bool() const { return (bool)Base::Ptr()->Value(); }

		/** Zero constructor. */
		Type() : Base() {}
		/** Constructor. */
		Type(
			const T & value,
			const Text::StringView16 & module,
			const Text::StringView16 & name,
			const Text::StringView16 & help = nullptr,
			Sources source = Sources::ALL,
			IVariables * vars = Variables()
		) : Base(new TypeBase<T>(value, module, name, help, source, vars)) {}
	};

	/** Debug 8-bit unsigned integer. */
	typedef Type<u8> u8;
	/** Debug 16-bit unsigned integer. */
	typedef Type<u16> u16;
	/** Debug 32-bit unsigned integer. */
	typedef Type<u32> u32;
	/** Debug 64-bit unsigned integer. */
	typedef Type<u64> u64;
	/** Debug 8-bit signed integer. */
	typedef Type<i8> i8;
	/** Debug 16-bit signed integer. */
	typedef Type<i16> i16;
	/** Debug 32-bit signed integer. */
	typedef Type<i32> i32;
	/** Debug 64-bit signed integer. */
	typedef Type<i64> i64;
	/** Debug 32-bit signed float. */
	typedef Type<f32> f32;
	/** Debug 64-bit signed float. */
	typedef Type<f64> f64;
	/** Debug string. */
	typedef Type<String> String;
	#pragma endregion

	/************************************************************************/
	/* Range<T>                                                             */
	/************************************************************************/
	#pragma region Range
	template<typename T>
	struct RangeBase
		: public IVariableBase
	{
		template<typename> friend struct Range;

	protected: // properties
		/** Value and range boundaries. */
		T _value, _min, _max;

	public: // methods
		/** Value setter. */
		void Value(const T & t, Sources s) { if ((s & _source) && (_min <= t) && (t <= _max) && (t != _value)) { _value = t; OnValueChanged(this, s); } }
		/** Value getter, */
		const T & Value() const { return _value; }
		/** Minimum value getter. */
		const T & Minimum() const { return _min; }
		/** Maximum value getter. */
		const T & Maximum() const { return _max; }

	protected: // constructors
		/** Constructor. */
		RangeBase(
			const T & value,
			const T & minimum,
			const T & maximum,
			const Text::StringView16 & module,
			const Text::StringView16 & name,
			const Text::StringView16 & help,
			Sources source,
			IVariables * vars
		) : IVariableBase(TypeControlTable<T>::RangeHash, module, name, help, source)
			, _min(minimum)
			, _max(maximum)
			, _value(value)
		{ vars->Create(this); }
	};

	template<typename T>
	struct Range
		: public Reference<RangeBase<T>>
	{
		/** Parent class. */
		using Base = Reference<RangeBase<T>>;
		/** Use assignment operator from parent class. */
		using Base::operator=;

		/** Cast operator. */
		operator const T &() const { return Base::Ptr()->Value(); }
		/** Conversion operator. */
		operator bool() const { return (bool)Base::Ptr()->Value(); }
		/** Assignment operator. */
		Range & operator=(const T & t) { Base::Ptr()->Value(t, Sources::CODE); return *this; }

		/** Zero constructor. */
		Range() : Base() {}
		/** Constructor. */
		Range(
			const T & value,
			const T & minimum,
			const T & maximum,
			const Text::StringView16 & module,
			const Text::StringView16 & name,
			const Text::StringView16 & help = nullptr,
			Sources source = Sources::ALL,
			IVariables * vars = Variables()
		) : Base(new RangeBase<T>(value, minimum, maximum, module, name, help, source, vars))
		{}
	};

	/** Debug 8-bit unsigned integer with min-max boundaries. */
	typedef Range<::DD::u8> u8Range;
	/** Debug 16-bit unsigned integer with min-max boundaries. */
	typedef Range<::DD::u16> u16Range;
	/** Debug 32-bit unsigned integer with min-max boundaries. */
	typedef Range<::DD::u32> u32Range;
	/** Debug 64-bit unsigned integer with min-max boundaries. */
	typedef Range<::DD::u64> u64Range;
	/** Debug 8-bit signed integer with min-max boundaries. */
	typedef Range<::DD::i8> i8Range;
	/** Debug 16-bit signed integer with min-max boundaries. */
	typedef Range<::DD::i16> i16Range;
	/** Debug 32-bit signed integer with min-max boundaries. */
	typedef Range<::DD::i32> i32Range;
	/** Debug 64-bit signed integer with min-max boundaries. */
	typedef Range<::DD::i64> i64Range;
	/** Debug 32-bit signed float with min-max boundaries. */
	typedef Range<::DD::f32> f32Range;
	/** Debug 64-bit signed float with min-max boundaries. */
	typedef Range<::DD::f64> f64Range;
	#pragma endregion

	/************************************************************************/
	/* Path                                                                 */
	/************************************************************************/
	#pragma region Path
	struct PathBase
		: IVariableBase
	{
		friend struct Path;
		/** Mode of path dialog. */
		DD_ENUM_32(
			(Modes, "Mode of path control."),
			(OPEN_FILE,  0, "Use control for opening a file."),
			(OPEN_FILES, 1, "Use control for opening multiple files."),
			(SAVE_FILE,  2, "Use control for saving file."),
			(SELECT_DIR, 3, "Use control for selecting a directory.")
		);

	protected: // properties
		/** Mode of path dialog. */
		Modes _mode;
		/** Path. */
		StringLocal<256> _path;

	public: // methods
		/** Mode of path dialog. */
		Modes Mode() const { return _mode; }
		/** Value setter. */
		void Value(const DD::Text::StringView16 & path, Sources source) { if (source & _source && _path != path) { _path = path; OnValueChanged(this, source); } }
		/** Value getter */
		Text::StringView16 Value() const { return _path; }

	protected: // constructors
		/** Constructor. */
		PathBase(
			Modes mode,
			const Text::StringView16 & path,
			const Text::StringView16 & module,
			const Text::StringView16 & name,
			const Text::StringView16 & help,
			Sources            source,
			IVariables *       vars
		);
	};


	struct Path
		: public Reference<PathBase>
	{
		/** Mode of path dialog. */
		using Modes = PathBase::Modes;
		/** Nullptr constructor. */
		Path() : Reference() {}
		/** Constructor. */
		Path(
			Modes mode,
			const Text::StringView16 & path,
			const Text::StringView16 & module,
			const Text::StringView16 & name,
			const Text::StringView16 & help = nullptr,
			Sources            source = Sources::ALL,
			IVariables *       vars = Variables()
		) : Reference(new PathBase(mode, path, module, name, help, source, vars)) {}
	};
	#pragma endregion

	/************************************************************************/
	/* Options                                                              */
	/************************************************************************/
	#pragma region Options
	struct OptionsBase
		: public IVariableBase
	{
		friend struct Options;
		/** Description for collection changed handler. */
		struct CollectionChangedDescriptor {
			/** Type of change. */
			DD_ENUM_32(
				(Changes, "Type of change of an option list."),
				(ADD,    0, "New item was added."),
				(CLEAR,  1, "All items were removed."),
				(INSERT, 2, "New item was inserted."),
				(REMOVE, 3, "An item was removed.")
			) Change;
			/** Additional info of insert change. */
			DD::i32 Index;
		};

	protected: // properties
		/** Value form serialization. */
		DD::StringLocal<64> _savedValue;
		/** Items to be selected. */
		List<DD::StringLocal<64>, 64> _items;
		/** Selected index. */
		DD::i32 _index;

	public: // properties
		/** Handler for collection changes. */
		Event<OptionsBase *, const CollectionChangedDescriptor &> OnCollectionChanged;

	public: // methods
		/** Insert item into the collection. */
		void Add(const Text::StringView16 & string, Sources source = Sources::CODE);
		/** Indexer. */
		const ::DD::String & At(size_t index) { return _items[index]; }
		/** Insert item into the collection. */
		void Insert(size_t index, const Text::StringView16 & string, Sources source = Sources::CODE);
		/** Free all the items. */
		void Free(Sources source);
		/** Index of selected string. */
		DD::i32 Index() const { return _index; }
		/** Index of selected string. */
		void Index(size_t index, Sources source = Sources::CODE);
		/** Remove item by index. */
		void RemoveAt(size_t index, Sources source = Sources::CODE);
		/** Number of items. */
		size_t Size() const { return _items.Size(); }
		/** Selected string. */
		Text::StringView16 Value() const { return _items[_index]; }

	protected: // constructors
		/** Constructor. */
		OptionsBase(
			const Text::StringView16 & module,
			const Text::StringView16 & name,
			const Text::StringView16 & help,
			Sources            source,
			IVariables *       vars
		);
	};

	struct Options
		: public Reference<OptionsBase>
	{
		/** Conversion operator. */
		operator size_t() const { return Ptr()->Index(); }
		/** Conversion operator. */
		operator const ::DD::String &() const { return Ptr()->Value(); }
		/** Assignment operator. */
		Options & operator=(size_t index) { Ptr()->Index(index, Sources::CODE); }
		/** Constructor. */
		Options() : Reference() {}
		/** Constructor. */
		Options(
			const Text::StringView16 & module,
			const Text::StringView16 & name,
			const Text::StringView16 & help = nullptr,
			Sources source = Sources::ALL,
			IVariables * vars = Variables()
		);
	};
	#pragma endregion

	/************************************************************************/
	/* Recorder                                                             */
	/************************************************************************/
	#pragma region Recorder
	struct IRecorder {
		/** Commands which can be recorded. */
		DD_ENUM_32(
			(Commands, "Type of commands that can be recorded."),
			(UNDEFINED,        0, "Default value for uninitialized command."),
			(SET,              1, "Set a value to variable."),
			(EQUAL,            2, "Wait while variable is equal to a value."),
			(NOT_EQUAL,        3, "Wait while variable is not equal to a value."),
			(LESS,             4, "Wait while variable is less than a value."),
			(LESS_OR_EQUAL,    5, "Wait while variable is less than or equal to a value."),
			(GREATER,          6, "Wait while variable is greater than a value."),
			(GREATER_OR_EQUAL, 7, "Wait while variable is greater than or equal to a value."),
			(WAIT,             8, "Wait for a specified time.")
		);
		/** Recording of recorder. */
		struct IRecording : public IReferential {
		protected: // IMacro
			/** Load from xml.*/
			virtual bool iMacroLoad(const ::DD::String &) = 0;
			/** Save to xml. */
			virtual bool iMacroSave(const DD::String &) = 0;

		public: // interface
			/** Load macro from xml file. */
			bool Load(const ::DD::String & path) { return iMacroLoad(path); }
			/** Save macro to xml file. */
			bool Save(const ::DD::String & path) { return iMacroSave(path); }

		public: // serialization
			/** Virtual destructor. */
			virtual ~IRecording() = default;
		};
		/** Typedef for referential capture of IRecording. */
		using Recording = Reference<IRecording>;
		/** Input for IRecorder::Start(). */
		struct RecorderSettings {
			/** Record all accessible variables. */
			bool LoadInitialState = true;
			/** If LoadInitialState is true, this method will filter the initial vars.  */
			ILambda<bool(IVariableBase *)> * InitialFilter = 0;
			/** Skip waiting commands (record without timeouts between commands). */
			bool SkipWaiting = false;
			/** Source which should be recorded. */
			Sources Source = Sources::GUI ^ Sources::ENGINE;
			/** Filter which variables should be recorded. */
			ILambda<bool(IVariableBase *)> * Filter = 0;
			/** Macro to be recorded. */
			IRecording * Recording = 0;
		};
		/** Input for IRecorder::Stop(). */
		struct PlayerSettings {
			/** Macro to be played. */
			IRecording * Recording = 0;
			/** Skip waiting commands (plays macro as fast as possible). */
			bool SkipWaiting = true;
		};
		/** Create an empty recording. */
		virtual IRecording * Create() = 0;
		/** Start recording.  */
		virtual bool Start(RecorderSettings &) = 0;
		/** Stop recording a macro. */
		virtual bool Stop() = 0;
		/** Play recording. */
		virtual bool Play(PlayerSettings &) = 0;
	} * Recorder();
	#pragma endregion

	/************************************************************************/
	/* Visitor                                                              */
	/************************************************************************/
	#pragma region Visitor
	template<typename VISITOR, typename INFO>
	struct IVisitor {
		/** Run visitor. */
		static void Run(IVariableBase *, INFO);
		/** Run visitor. */
		static void Run(const IVariableBase *, INFO);
	};
	#pragma endregion
}

namespace DD::Debug {
	template<typename ARG, typename ...ARGS>
	void Log::printtbd(const ARG & arg, const ARGS &...args) {
		format(arg);
		printtb(args...);
	}

	template<typename ARG, typename ...ARGS>
	void Log::printtb(const ARG & arg, const ARGS &...args) {
		getBuffer().Append('\t');
		format(arg);
		printtb(args...);
	}

	template<typename ARG>
	void Log::printtype() {
		getBuffer().AppendBatch(typeid(ARG).name(), '\n');
		flush();
	}

	template<typename ARG, typename ARG1, typename ...ARGS>
	void Log::printtype() {
		getBuffer().Append(typeid(ARG).name()).Append('\t');
		printtype<ARG1, ARGS...>();
	}

	template<typename...ARGS>
	void LogBase::Print(const ARGS &...args) {
		switch (_mode) {
			case Modes::DISABLED: break;
			case Modes::SILENT:   tick(); break;
			case Modes::PRINT:    tick(), Log::PrintLine(Stamp, ' ', args...); break;
			case Modes::LOG:      tick(), Log::PrintLine(args...); break;
			case Modes::PRINT_EX: tick(), printEx(StringLocal<256>::From(Stamp, ' ', args...)); break;
			case Modes::LOG_EX:   tick(), printEx(StringLocal<256>::From(args...)); break;
			default: throw;  break;
		}
	}

	template<typename...ARGS>
	void LogBase::PrintFormat(const ARGS &...args)	{
		switch (_mode) {
			case Modes::DISABLED: break;
			case Modes::SILENT:   tick(); break;
			case Modes::PRINT:    tick(), Log::Print(Stamp, ' '); Log::PrintFormat(args...); Log::Print('\n'); break;
			case Modes::LOG:      tick(), Log::PrintFormat(args...); Log::Print('\n'); break;
			case Modes::PRINT_EX: tick(), printEx(StringLocal<256>::From(Stamp, ' ').AppendFormat(args...).Append('\n')); break;
			case Modes::LOG_EX:   tick(), printEx(StringLocal<256>::Format(args...).Append('\n')); break;
			default: throw;  break;
		}
	}

#define __DECLARE_VISIT_CASE(aCase, aCast) case aCase: VISITOR::Visit(static_cast<aCast>(var), i); break;
#define __DECLARE_VISIT_CASE_TYPE(index, count, const, type) __DECLARE_VISIT_CASE(VariableTypes::type, const Debug::TypeBase<DD::type> *)
#define __DECLARE_VISIT_CASE_RANGE(index, count, const, type) __DECLARE_VISIT_CASE(VariableTypes::type##Range, const Debug::RangeBase<DD::type> *)
#define __DECLARE_VISIT_CASES(const)\
	__DECLARE_VISIT_CASE(VariableTypes::Assert, const AssertBase *)\
	__DECLARE_VISIT_CASE(VariableTypes::Boolean, const BooleanBase *)\
	__DECLARE_VISIT_CASE(VariableTypes::Button, const ButtonBase *)\
	__DECLARE_VISIT_CASE(VariableTypes::Enum, const IEnumBase *)\
	__DECLARE_VISIT_CASE(VariableTypes::Path, const PathBase *)\
	__DECLARE_VISIT_CASE(VariableTypes::Log, const LogBase *)\
	__DECLARE_VISIT_CASE(VariableTypes::Options, const OptionsBase *)\
	DD_XLIST_EX(__DECLARE_VISIT_CASE_RANGE, const, i8, i16, i32, i64, u8, u16, u32, u64, f32, f64)\
	DD_XLIST_EX(__DECLARE_VISIT_CASE_TYPE, const, i8, i16, i32, i64, u8, u16, u32, u64, f32, f64, String)\
	default: DD_WARNING("Invalid variable type."); break;

	template<typename VISITOR, typename INFO>
	void IVisitor<VISITOR, INFO>::Run(Debug::IVariableBase * var, INFO i) {
		switch (var->Type()) { __DECLARE_VISIT_CASES(DD_EMPTY); }
	}

	template<typename VISITOR, typename INFO>
	void IVisitor<VISITOR, INFO>::Run(const Debug::IVariableBase * var, INFO i) {
		switch (var->Type()) { __DECLARE_VISIT_CASES(const); }
	}
#undef __DECLARE_VISIT_CASE
#undef __DECLARE_VISIT_CASE_RANGE
#undef __DECLARE_VISIT_CASE_TYPE
#undef __DECLARE_VISIT_CASES
}

/************************************************************************/
/* Operator imitation of native types                                   */
/************************************************************************/
#define __DECLARE_OP_OUT(index, count, type, op)\
	template<typename T, typename U>\
	auto operator op(const type<T> & l, const U & r) { return l->Value() op r; }\
	template<typename T, typename U>\
	auto operator op(const T & l, const type<U> & r) { return l op r->Value(); }\
	template<typename T>\
	auto operator op(const type<T> & l, const type<T> & r) { return l->Value() op r->Value(); }
#define __DECLARE_OP_IN(index, count, type, op)\
	template<typename T, typename U>\
	T & operator op(T & l, const type<U> & r) { return l op r->Value(); }\
	template<typename T, typename U>\
	type<T> & operator op(type<T> & l, const U & r) { auto tmp = l->Value(); l->Value(tmp op r, Debug::Sources::CODE); return l; }
#define __DECLARE_OP_CREMENT(index, count, type, op) \
	template<typename T>\
	type<T> & operator op(type<T> & l) { l->Value(l->Value() + T(1), Debug::Sources::CODE); return l; }\
	template<typename T>\
	T operator op(type<T> & l, int) { T tmp = l->Value(); l->Value(l->Value() + T(1), Debug::Sources::CODE); return tmp; }
#define __DECLARE_OP_UN(index, count, type, op)\
template<typename T>\
auto operator op(const type<T> & t) { return op(t->Value()); }
/** Macro for forwarding operators of debug type to native type. */
#define __DECLARE_OPERATORS(type)\
	DD_XLIST_EX(__DECLARE_OP_OUT, type, +, -, *, / , %, <, >, <= , >= , == , !=, &, |, ^, &&, ||, <<, >>)\
	DD_XLIST_EX(__DECLARE_OP_IN, type, +=, -=, *=, /=, %=, &=, |=, ^=, <<=, >>=)\
	DD_XLIST_EX(__DECLARE_OP_CREMENT, type, ++, --)\
	DD_XLIST_EX(__DECLARE_OP_UN, type, -, !, ~)

namespace DD::Debug {
	__DECLARE_OPERATORS(Type);
	__DECLARE_OPERATORS(Range);
	__DECLARE_OPERATORS(Enum);
}

#undef __DECLARE_OP_CREMENT
#undef __DECLARE_OP_IN
#undef __DECLARE_OP_OUT
#undef __DECLARE_OPERATORS
