#pragma once
namespace DD {
	/** Abstract ancestor for all flags. */
	struct IFlag;
	/** Helper for natvis visualization. */
	template<typename>
	struct Flag;
}

#include <DD/Types.h>
#include <DD/String.h>

/**
 * Flag factory.
 * DD_FLAG_N(DEF, ITEMS...)
 * @param N     - number of bits occupied by flags - 8 | 16 | 32 | 64
 *              - note that 32-bit is most efficient on ordinary architectures.
 * @param DEF   - definition of flag structure
 *              - name | (name, "help")
 * @param ITEMS	- declaration of flag items - first item will use first bit
 *              - xlist | items
 *              - if there is only item, it is accepted as xlist, otherwise as item set.
 *              - ITEM - name | (name, "help")
 *
 * @example fast declaration
 *   DD_FLAG_32(MyFlags, ONE, TWO) myFlagInstance;
 *
 * @example declaration via item list
 *   DD_FLAG_32(
 *     (MyFlags, "Sample flags structure"),
 *     (ONE, "First flag item."),
 *     (TWO, "Second flag item")
 *   );
 *
 * @example declaration via xlist
 *   #define MyFlagsItems(X)\
 *     X(ONE, "First flag item.")\
 *     X(TWO, "Second flag item")
 *   DD_FLAG_32((MyFlags, "Sample flags structure"), MyFlagItems);
 *
 * @example common usage
 *   DD_FLAG_32(MyFlags, ONE, TWO) flags = MyFlags::ALL;
 *   for (FLAG flag : flags)
 *     DD_LOG("This single flag is used", MyFlags::Flag2String(flag)); // print all flags
 *   for (u32 index : flags)
 *     DD_LOG("Used flags", MyFlags::Flag2String(1 << index), index); // print all indices
 *   if (flags & MyFlags::ONE);  // check if a flag is used
 *   if (flags[0]);              // check if an index is used
 *   flags &= ~MyFlags::ONE;  // disable flag
 *   flags |= MyFlags::ONE;   // enable flag
 *   flags ^= MyFlags::ONE;   // switch flag
 *
 * @example string - flags parsing
 *   DD_FLAG_32(MyFlags, ONE, TWO) myFlag0 = MyFlags::ALL, myFlag1 = MyFlags::NONE;
 *   ::DD::StringLocal<64> buffer;                      // prepare buffer
 *   if (MyFlags::ToString(myFlagInstance, buffer))     // try parse flags to string
 *     DD_LOG(buffer);                                  // print "ONE | TWO"
 *   else
 *     DD_LOG("Something went wrong");                  // inform user if parsing fails
 *   if (MyFlags::FromString(buffer, myFlag1))          // try to parse string to flags
 *     doSomething(myFlag1);
 *   else
 *     DD_LOG("Something went wrong");                  // inform user if parsing fails
 */
#define DD_FLAG_8(DEF, ...)  DD_FLAG(DEF,  8, __VA_ARGS__)
#define DD_FLAG_16(DEF, ...) DD_FLAG(DEF, 16, __VA_ARGS__)
#define DD_FLAG_32(DEF, ...) DD_FLAG(DEF, 32, __VA_ARGS__)
#define DD_FLAG_64(DEF, ...) DD_FLAG(DEF, 64, __VA_ARGS__)

namespace DD {
	struct IFlag {
		/** Structure for separating single flags from a string. */
		struct Separator;
		/** Abstract descriptor of any of flags (no matter of size). */
		struct IDescriptor;
	};

	template<typename FLAG>
	struct Flag
		: public IFlag
	{
	public: // nested
		/** Helper struct for casting flags. */
		union Caster;
		/** Iterator over single flags. */
		struct Iterator;
		/** Sequencer over single flags. */
		struct Sequencer;
		/** Proxy to single flag in flags enum. */
		struct Proxy;
		/** Implementation of IFlag::IDescriptor. */
		struct DescriptorImpl;

	public: // statics
		/** Get descriptor for the flag. */
		static const IDescriptor * Descriptor();
		/**
		 * Try to convert flags to string.
		 * @return true if all parts of flag were correctly parsed to string.
		 */
		static bool ToString(FLAG source, String & target);
		/**
		 * Try to convert string to a flag.
		 * @return true if all parts of string were correctly parsed to flags.
		 */
		static bool FromString(const String & source, FLAG & target);
	};

	/************************************************************************/
	/* IFlag                                                                */
	/************************************************************************/
	struct IFlag::IDescriptor {
		/** Get number bits that can be used as flags. */
		virtual u32 Size() const = 0;
		/** Get help for the flag structure. */
		virtual const String & Help() const = 0;
		/**
		 * Get single flag name.
		 * @return empty string if flag is invalid, the name otherwise.
		 */
		virtual const String & Flag2String(u64) const = 0;
		/**
		 * Create string from a native value.
		 * @param source - source flags native value.
		 * @param target - string buffer to be filled.
		 * @return dirty check - true if all flags were parsed correctly.
		 */
		virtual bool ToString(u64 source, String & target) const = 0;
		/**
		 * Create flags from string.
		 * @param source - string flags.
		 * @param target - native flags.
		 * @return dirty check - true if all string parts were parsed correctly.
		 */
		virtual bool FromString(const String & source, u64 & target) const = 0;
	};

	struct IFlag::Separator
		: public ILambda<i32(wchar_t)>
	{
		/** Per thread instance. */
		static Separator * Instance();
		/** Number of matched chars. */
		i32 Count;

	public: // iLambda
		i32 iLambdaRun(wchar_t c);
	};

	/************************************************************************/
	/* Flag<F>                                                              */
	/************************************************************************/
	template<typename FLAG>
	union Flag<FLAG>::Caster {
		/** Check if maximal number of bytes does not overflow. */
		static_assert(sizeof(FLAG) <= sizeof(u64), "Maximal bit size is 64.");
		/** Helper structure for valid padding. */
		template<size_t BYTES>
		class PaddingHelper {
			/** Padding for valid alignment. */
			u8 _padding[BYTES];
		public:
			/** Flag values. */
			FLAG Value;
		};
		/** Helper structure for valid padding. */
		template<>
		class PaddingHelper<0> {
		public:
			/** Flag value. */
			FLAG Value;
		};
		/** Universal type for any type of flags, */
		u64 Universal;
		/** Native representation. */
		PaddingHelper<sizeof(u64) - sizeof(FLAG)> Native;
	};

	template<typename FLAG>
	struct Flag<FLAG>::Sequencer {
		/** Original value and processed value. */
		FLAG _copy, _worker;
		/** Index of flag. */
		u32 _index;

	public: // ISequencer
		FLAG Get() { return FLAG(1) << _index; };
		bool Next();
		bool Reset();

	public: // constructors
		/** Constructor.*/
		Sequencer(FLAG value) : _copy(value) {}
	};

	template<typename FLAG>
	struct Flag<FLAG>::Iterator {
		/** Index of current flag. */
		u32 _index;
		/** Flag holder. */
		FLAG _flag;
		/** Iteration. */
		bool operator++();
		/** Dummy equals operator. */
		bool operator!=(const Iterator &) const { return _flag.Any(); }
		/** Dereference. */
		Iterator & operator*() { return *this; }
		/** Auto conversion to indices. */
		operator u32() const { return _index; }
		/** Auto conversion to flag. */
		operator FLAG() const { return FLAG(1) << _index; }
		/** Zero constructor. */
		Iterator() {}
		/** Constructor. */
		Iterator(FLAG flag);
	};

	template<typename FLAG>
	struct Flag<FLAG>::Proxy {
		/** Reference to proxy. */
		FLAG & _flag;
		/** Index of flag. */
		u32 _index;
		/** Enable flag. */
		void Enable() { _flag |= (FLAG(1) << _index); }
		/** Disable flag. */
		void Disable() { _flag &= ~(FLAG(1) << _index); }
		/** Enable if disabled, disable if enabled. */
		void Switch() { _flag ^= (FLAG(1) << _index); }
		/** Assignment. */
		Proxy & operator=(bool b) { b ? Enable() : Disable();  return *this; }
		/** bool conversion. */
		operator bool() const { return (_flag & (FLAG(1) << _index)) != 0; }
	};

	template<typename FLAG>
	struct Flag<FLAG>::DescriptorImpl : public IFlag::IDescriptor {
		typedef Unsigned<FLAG::Bits() / 8> uint_t;
	public: // IDescriptor
		u32 Size() const override { return FLAG::Size(); }
		const String & Help() const override { return FLAG::Help(); }
		const String & Flag2String(u64 flag) const override { return FLAG::Flag2String((uint_t)flag); }
		bool ToString(u64 source, String & target) const override;
		bool FromString(const String & source, u64 & target) const override;
	};
}

/** Flag factory. */
#define DD_FLAG(DEF, BITS, ...)                                                   \
/** Structure for handling flags. */                                              \
struct _DD_FLAG_DEF_NAME(DEF)                                                     \
 : public ::DD::Flag<_DD_FLAG_DEF_NAME(DEF)>                                      \
{                                                                                 \
                                                                                  \
public: /*nested*/                                                                \
	/** Base type of flags. */                                                      \
	typedef ::DD::Unsigned<BITS / 8> uint_t;                                        \
	/** 64 bit hashes of strings of flags. */                                       \
	enum struct Hashes : ::DD::u64 { DD_XPLAY(_DD_FLAG_HASH, __VA_ARGS__) };        \
	/** 32 bit indices of flags. */                                                 \
	struct Indices { enum { DD_XPLAY(_DD_FLAG_INDEX, __VA_ARGS__) INDICES_SIZE }; };\
	/** Values of single flags. */                                                  \
	enum Flags { DD_XPLAY(_DD_FLAG_FLAG, __VA_ARGS__) };                            \
	/** Values of special flags - outside Flags enum, because of natvis. */         \
	enum Specials { NONE = 0, ALL = NONE DD_XPLAY(_DD_FLAG_ALL, __VA_ARGS__)};      \
                                                                                  \
public: /*static methods*/                                                        \
	/** Method for getting single-value-flag from string hash. */                   \
	/*static Flags _DD_FLAG_DEF_NAME(DEF) Hash2Flag(::DD::u64 hash) {                 \
		switch (hash) { DD_XPLAY(_DD_FLAG_H2F_CASE, __VA_ARGS__) }                    \
		return NONE;                                                                  \
	}*/                                                                               \
	/** Method for getting string from given single-flag-value. */                  \
	static const ::DD::String & Flag2String(_DD_FLAG_DEF_NAME(DEF) flag) {          \
		DD_XPLAY(_DD_FLAG_F2S_VAR, __VA_ARGS__)                                       \
		switch (flag._value) { DD_XPLAY(_DD_FLAG_F2S_CASE, __VA_ARGS__) }             \
		return DD::String();                                                          \
	}                                                                               \
	/** Help string for flag. */                                                    \
	static const ::DD::Text::StringView16 & Help() {                                \
		return ::DD::Text::StringView16(                                              \
			_DD_FLAG_DEF_HELP(DEF) DD_XPLAY(_DD_FLAG_HELP, __VA_ARGS__)                 \
		);                                                                            \
	}                                                                               \
	/** Number of defined flags. */                                                 \
	static constexpr ::DD::u32 Size() { return Indices::INDICES_SIZE; }             \
	/** Number of bits of base integer. */                                          \
	static constexpr size_t Bits() { return BITS; }                                 \
                                                                                  \
private: /*properties*/                                                           \
	/** Native value. */                                                            \
	uint_t _value;                                                                  \
                                                                                  \
public: /*accession*/                                                             \
	/** Conversion to native type. */                                               \
	constexpr operator uint_t &() {return _value;}                                  \
	/** Conversion to native type. */                                               \
	constexpr operator uint_t () const {return _value;}                             \
	/** Sequencer. */                                                               \
	Flag::Sequencer Sequencer() { return *this; }                                   \
	/** Iterator. */                                                                \
	Iterator begin() { return *this; }	                                            \
	/** Dummy iterator. */                                                          \
	Iterator end() { return Iterator(); }	                                          \
	/** Indexer. */                                                                 \
	constexpr Proxy operator[](::DD::u32 index) { return Proxy{*this, index}; }     \
                                                                                  \
public: /*checks*/                                                                \
	/** Is all flags set. */                                                        \
  constexpr bool All() const { return _value == ALL; }                            \
	/** Is anything set. */                                                         \
	constexpr bool Any() const { return _value != NONE; }                           \
	/** Is no flag set. */                                                          \
	constexpr bool None() const { return _value == NONE; }                          \
                                                                                  \
public: /*constructors*/                                                          \
	/** Constructor from native values. */                                          \
	constexpr _DD_FLAG_DEF_NAME(DEF) (uint_t value) : _value(value) {}              \
	/** Constructor. */                                                             \
	constexpr _DD_FLAG_DEF_NAME(DEF) (Flags flag) : _value((uint_t)flag) {}         \
	/** Constructor. */                                                             \
	constexpr _DD_FLAG_DEF_NAME(DEF) (Specials flag) : _value((uint_t)flag) {}      \
	/** Zero constructor.*/                                                         \
	_DD_FLAG_DEF_NAME(DEF) () {}                                                    \
}

/** Helper macros. */
#define _DD_FLAG_DEF_NAME(def) DD_UNWRAP_HEAD(0, def)
#define _DD_FLAG_DEF_HELP(def) _DD_OVERLOAD(_DD_FLAG_DEF_HELP, DD_UNWRAP(def))
#define _DD_FLAG_DEF_HELP_1(name) L ## #name
#define _DD_FLAG_DEF_HELP_2(name, help) L ## #name L" - " L ## help
#define _DD_FLAG_INDEX(name, ...) name,
#define _DD_FLAG_ALL(name, ...) | name
#define _DD_FLAG_FLAG(name, ...) name = 1 << Indices::name,
#define _DD_FLAG_HASH(name, ...) name = ::DD::Text::StringView(#name).Hash(),
#define _DD_FLAG_HELP(...) _DD_OVERLOAD(_DD_FLAG_HELP, __VA_ARGS__)
#define _DD_FLAG_HELP_1(name) L""
#define _DD_FLAG_HELP_2(name, help) L"\n - " L ## #name " - " L ## help
#define _DD_FLAG_F2S_VAR(name, ...) static const ::DD::Text::StringView16 __##name (L ## #name);
#define _DD_FLAG_F2H_VAR(...) static const ::DD::Text::StringView16 __##name (_DD_FLAG_HELP(__VA_ARGS__));
#define _DD_FLAG_F2S_CASE(name, ...) case Flags::name : return __##name;
#define _DD_FLAG_H2F_CASE(name, ...) case Hashes::name : return Flags::name;

namespace DD {
	/************************************************************************/
	/* IFlag::Separator                                                     */
	/************************************************************************/
	inline i32 IFlag::Separator::iLambdaRun(wchar_t c){
		if (c == ' ' || c == '|')
			++Count;
		else
			Count = 0;
		// no match
		if (Count < 3)
			return 0;
		// match
		Count = 0;
		return 3;
	}

	inline IFlag::Separator * IFlag::Separator::Instance() {
		thread_local static Separator instance;
		instance.Count = 0;

		return &instance;
	};

	/************************************************************************/
	/* FLag<T>                                                              */
	/************************************************************************/
	template<typename FLAG>
	inline const IFlag::IDescriptor * Flag<FLAG>::Descriptor() {
		static DescriptorImpl instance;
		return &instance;
	}

	template<typename FLAG>
	inline bool Flag<FLAG>::ToString(FLAG source, String & target) {
		bool clean = true;
		// if nothing is set do not do anything
		if (source.Any()) {
			Iterator i = source.begin();
			// parse first item without a delimiter
			while (*i) {
				// get string name of flag
				const String & fistItem = FLAG::Flag2String(*i);
				// check if flag is valid
				if (fistItem.IsEmpty()) {
					// first item not valid
					clean = false;
					++i;
				}
				else {
					// first item found, break the loop
					target.Append(fistItem);
					break;
				}
			}
			// parse other delimiters
			while (++i) {
				const String & item = FLAG::Flag2String(*i);
				clean &= !item.IsEmpty();
				target.Append(" | ").Append(item);
			}
		}

		return clean;
	}

	template<typename FLAG>
	inline bool Flag<FLAG>::FromString(const String & source, FLAG & target) {
		bool clean = true;
		// for each flags in string
		for (const String & flagString : source.Split(Separator())) {
			// parse single-flag string
			FLAG flag = FLAG::Hash2Flag(flagString.Hash<size_t, DD::Text::HashMethods::FirstLastCI>());
			// check if name of single flag is valid
			clean &= flag.Any();
			// add flag to target
			target |= flag;
		}

		return clean;
	}

	/************************************************************************/
	/* Flag<T>::Iterator                                                    */
	/************************************************************************/
	template<typename FLAG>
	bool Flag<FLAG>::Iterator::operator++() {
		_flag[_index].Disable();
		if (_flag.Any()) {
			while (!_flag[++_index]);
			return true;
		}

		return false;
	}

	template<typename FLAG>
	inline Flag<FLAG>::Iterator::Iterator(FLAG flag)
		: _flag(flag)
		, _index(0)
	{
		if (_flag.Any()) while (!_flag[_index]) ++_index;
	}

	/************************************************************************/
	/* Flag<T>::Separator                                                   */
	/************************************************************************/
	template<typename FLAG>
	bool Flag<FLAG>::Sequencer::Next() {
		while (_worker >> 1 && ++_index < FLAG::Size())
			if (_worker & 1)
				return true;

		return false;
	}

	template<typename FLAG>
	bool Flag<FLAG>::Sequencer::Reset() {
		_index = 0;
		if (((_worker = _copy) & 1) == 0)
			return Next();

		return true;
	}

	/************************************************************************/
	/* Flag<T>::DescriptorImpl                                              */
	/************************************************************************/
	template<typename FLAG>
	inline bool Flag<FLAG>::DescriptorImpl::ToString(u64 source, String & target) const {
		Caster * caster = (Caster *)(&source);
		bool clean = Flag::ToString(caster->Native.Value, target);
		return clean && (caster->Universal == caster->Native.Value);
	}

	template<typename FLAG>
	inline bool Flag<FLAG>::DescriptorImpl::FromString(const String & source, u64 & target) const {
		Caster * caster = (Caster *)(&target);
		bool clean = Flag::FromString(source, caster->Native.Value);
		return clean && (caster->Universal == caster->Native.Value);
	}
}

