#pragma once
namespace DD::Collections {
	/** Hashmap with serialization routines. */
	template<typename T>
	struct FileStash;
}

#include <DD/Base64.h>
#include <DD/Collections/QuickMap.h>
#include <DD/FileSystem/File.h>
#include <DD/Text/StringReader.h>
#include <DD/Text/StringView.h>
#include <DD/Text/StringWriter.h>

namespace DD::Collections {
	template<typename T>
	struct FileStash {
		/** SV used in local context. */
		using StringView = DD::Text::StringView8;

	public: // properties
		/** Where to store. */
		DD::FileSystem::File FileStorage;
		/** The look-up map */
		DD::Collections::QuickMap8<T> DataMap;

	public: // methods
		/** Get key value pair of the record, null if no such match. */
		DD::Collections::QuickMapRecord8<T> * GetPair(StringView name) { return DataMap.Get(name); }
		/** Get key value pair of the record, null if no such match. */
		const DD::Collections::QuickMapRecord8<T> * GetPair(StringView name) const { return DataMap.Get(name); }
		/** Get stored value, nullptr if no match. */
		T * GetValue(StringView name) { if (DD::Collections::QuickMapRecord8<T> * p = GetPair(name)) return &(p->Value); return nullptr; }
		/** Get stored value, nullptr if no match. */
		const T * GetValue(StringView name) const { if (const DD::Collections::QuickMapRecord8<T> * p = GetPair(name)) return &(p->Value); return nullptr; }
		/** Save item and serialize. */
		void Save(DD::Text::StringView8 name, const T & source);
		/** Remove item and serialize. */
		bool TryRemove(DD::Text::StringView8 name);
		/** Drop all the content to the associated file. */
		void Serialize();
		/** Read content from a file */
		void Deserialize();

	public:
		/** Constructor. */
		FileStash(const DD::FileSystem::Path & target) : FileStorage(target.AsFile()) { Deserialize(); }
	};
}


namespace DD::Collections {
	template<typename T>
	void FileStash<T>::Save(DD::Text::StringView8 name, const T & source) {
		DataMap.Insert(name, source);
		Serialize();
	}


	template<typename T>
	bool FileStash<T>::TryRemove(DD::Text::StringView8 name) {
		if (!DataMap.Remove(name))
			return false;

		Serialize();
		return true;
	}


	template<typename T>
	void FileStash<T>::Serialize() {
		// string builder
		DD::Text::StringWriter8 writer;

		for (const DD::Collections::QuickMapRecord8<T> & keyvalue : DataMap.Values) {
			// odd line is name
			DD::Base64::Encode(keyvalue.Key.View().ToArrayView(), writer);
			writer.WriteN();
			// even line is value
			DD::Base64::Encode(AsByteView(keyvalue.Value), writer);
			writer.WriteN();
		}

		// drop to file
		FileStorage.Write(writer.View().ToArrayView());
	}


	template<typename T>
	void FileStash<T>::Deserialize() {
		// read all the file
		DD::Array<DD::i8> data = FileStorage.Read();
		// reader for reading chunks
		DD::Text::StringReader8 reader(data.ToView());

		// Helper for deserialization
		T target;

		while (reader.Any()) {
			memset(&target, 0, sizeof(T));
			// read name
			DD::Array<char> name = DD::Base64::Decode(reader.ReadLine());
			DD::Text::StringView8 nameView((const char*)name.begin(), name.Length());
			// read value
			if (DD::Base64::TryDecode(reader.ReadLine(), target)) {
				// put to collection
				DataMap.Insert(nameView, target);
			}
		}
	}
}


