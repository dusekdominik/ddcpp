#pragma once
namespace DD::Concurrency {
	/** Simple thread pool implementation.  */
	class ThreadPool;
}

#include <DD/Array.h>
#include <DD/BlockChain.h>
#include <DD/Concurrency/CriticalSection.h>
#include <DD/Concurrency/ConditionVariable.h>
#include <DD/Concurrency/Thread.h>
#include <DD/Concurrency/ThreadSafeObject.h>
#include <DD/Heap.h>
#include <DD/Lambda.h>
#include <DD/List.h>
#include <DD/Reference.h>
#include <DD/String.h>

namespace DD::Concurrency {
	class ThreadPool {
	public: // typedefs
		/** State of single thread. */
		enum ThreadState { Occupied, Free };
		/**
		 * Minimalistic implementation of task.
		 * Contains pointer to routine and priority.
		 */
		struct ITask;
		/**
		 * Task with local allocation.
		 * For common usage @see TaskList.
		 * @warning param allocation means allocation for lambda,
		 *   not size of whole object.
		 * @warning object should not be copied anywhere.
		 * @warning task should not be deallocated until not finished.
		 * @param ALLOCATION - allocation for lambda function
		 */
		template<size_t ALLOCATION>
		struct TaskLocal;
		/**
		 * Linked list of TaskLocal objects;
		 * designed for storing async tasks.
		 * List itself is not thread-safe.
		 * First block is locally allocated, so it is not calling allocator.
		 * Additional blocks will be allocated one by one.
		 * @param BLOCK_SIZE - number of tasks in one block
		 * @param TASK_ALLOCATION - byte size of lambda which can be stored in the task.
		 */
		template<size_t BLOCK_SIZE = 256, size_t TASK_ALLOCATION = 32>
		struct TaskList;
		/** Declaration of referential async task. */
		struct AsyncBase;
		/** Declaration of referential async task. */
		struct TaskBase;
		/** Declaration of referential async task with result value. */
		template<typename RESULT>
		struct FutureBase;
		/**
		 * Class for multi-thread computation.
		 *
		 * @example common usage
		 *   Task t1([](){ (complex computation) });
		 *   Task t2([](){ (complex computation) });
		 *   t1->Wait(), t2->Wait();
		 */
		struct Task;
		/**
		 * Future is class for multi-thread computation.
		 *
		 * @example common usage
		 *   Future<int> myInt1([](){ (complex computation) return (int)i; });
		 *   Future<int> myInt2([](){ (complex computation) return (int)i; });
		 *   int result = myInt1->Result() + myInt2->Result();
		 */
		template<typename RESULT>
		struct Future;

	public: // statics
		/** Get number of concurrent threads. */
		static u32 CocurrentThreads();
		/** Global pool. */
		static ThreadPool & Instance();

	private: // properties
		/** Threads doing the work. */
		Array<Thread> _workers;
		/** Queue of tasks for workers. */
		MinHeap<ITask *, 256> _queue;
		/** Milliseconds of sleeping between task check. */
		u32 _sleep;
		/** Number of currently working threads. */
		volatile u32 _working;
		/** MT primitive for synchronization access to the _queue. */
		CriticalSection _section;
		/** MT primitive for sleeping and waking threads. */
		ConditionVariable _cvar;

	private: // methods
		/** Basic loop loading actions from the queue to the thread. */
		void loop();

	public: // methods
		/** Wait until queue is clear and tasks are done. */
		void WaitAll();
		/** Detach all the threads. */
		void DetachAll();
		/** Terminate all the threads. */
		void TerminateAll();
		/** Adds async action. */
		void RegisterAction(ITask * task);
		/** Number of tasks. */
		size_t NumberOfTasks() { return _queue.Size(); }
		/** Number of threads. */
		size_t NumberOfThreads() const { return _workers.Length(); }
		/** Number of used threads. */
		size_t NumberOfWorkingThreads() const { return _working; }

	public: // constructors
		/**
		 * Basic constructor.
		 * @param name    - name of the pool, its thread will be named as name##index.
		 * @param threads - number of parallel threads processing pool heap
		 *                - set zero to use maximal count of parallel threads
		 *                - on your architecture (usually 4/8/12/16).
		 */
		ThreadPool(Text::StringView16 name, u32 threads = 0);
		/** Destructor for detaching async threads. */
		~ThreadPool();
	};

	/************************************************************************/
	/* ITask                                                                */
	/************************************************************************/
	struct ThreadPool::ITask {
		friend class ThreadPool;
		/** Stamp of already processed task task. */
		static constexpr i32 EXPIRED = 0x80000000;

	protected: // properties
		/** Pointer to lambda with an action. */
		IAction<> * _routine = 0;
		/** Integer priority - could be anything except expired. */
		volatile i32 _priority;
		/** Number of access to the task. First accessor is the only one what can processed the task. */
		volatile i32 _access;

	public: // methods
		/** Non blocking check if task is done. */
		bool HasFinished() const { return _priority == EXPIRED; }
		/** Getter of priority. */
		i32 Priority() const { return _priority; }
		/** Thread safe methods for running the task. First caller is processing the task. All other calls will be skipped. */
		void Run();
		/** Reset task properties and push it to the given pool. */
		void Reset(IAction<> * routine, i32 priority = 0, ThreadPool & pool = ThreadPool::Instance());
		/** Wait until the task is not finished. Spinning by default. Sleeping on demand. */
		void Wait(u32 milliseconds = 0) const;
		/** Heap comparer */
		bool operator<(const ITask & rhs) const { return Priority() < rhs.Priority(); }

	protected:
		/**
		 * Constructor.
		 * @param routine  - virtual call to the task routine.
		 * @param priority - integer priority, higher number means higher priority.
		 * @param access   - number of calls without process - used for precedence call.
		 */
		ITask(IAction<> * routine, i32 priority, i32 access = 1);

	public:
		/** Zero constructor. */
		ITask() = default;
		/**
		 * Constructor with automatic registration to the given pool.
		 * @param routine  - virtual call to the task routine.
		 * @param priority - integer priority, higher number means higher priority.
		 * @param pool     - pool in which task should be processed.
		 */
		ITask(IAction<> * routine, i32 priority = 0, ThreadPool & pool = ThreadPool::Instance());
		/** Destructor. */
		~ITask();
	};

	/************************************************************************/
	/* TaskLocal                                                            */
	/************************************************************************/
	template<size_t ALLOCATION>
	struct ThreadPool::TaskLocal
		: public ThreadPool::ITask
	{
		/** Locally allocated lambda holder. */
		LambdaLocal<void(), ALLOCATION + 8> _lambda;

		/** Method for deferred instantiation of task. */
		template<typename LAMBDA>
		void Set(const LAMBDA & lambda, i32 priority = 0, ThreadPool & pool = ThreadPool::Instance());
		/** Assignment operator. */
		template<typename LAMBDA>
		TaskLocal & operator=(const LAMBDA & lambda) { Set(lambda); return *this; }

		/** Zero constructor. */
		TaskLocal() : ITask(_lambda, 1, 1) {}
		/** Move constructor. */
		TaskLocal(TaskLocal &&) = delete;
		/** Copy constructor. */
		TaskLocal(const TaskLocal &) = delete;
		/** Constructor. */
		template<typename LAMBDA>
		TaskLocal(const LAMBDA & lambda, i32 priority = 0, ThreadPool & pool = ThreadPool::Instance());
		/** Destructor. */
		~TaskLocal() = default;
	};

	template<size_t BLOCK_SIZE, size_t TASK_SIZE>
	struct ThreadPool::TaskList
		: public BlockChain<TaskLocal<TASK_SIZE>, BLOCK_SIZE>
	{
		using Base = BlockChain<TaskLocal<TASK_SIZE>, BLOCK_SIZE>;

	protected:
		/** Default thread pool (if task does not have specified pool, this one will be used). */
		ThreadPool & _pool;

	public: // methods
		/**
		 * Add task to the list.
		 * @param lambda   - lambda/functor to be processed
		 * @param priority - integer priority of the task (higher number, higher priority)
		 * @param pool     - thread pool in which should be task processed
		 */
		template<typename LAMBDA>
		void Add(const LAMBDA & lambda, i32 priority, ThreadPool & pool);
		/** Add task to the list - @see main Add function. */
		template<typename LAMBDA>
		void Add(const LAMBDA & lambda, ThreadPool & pool) { Add(lambda, 0, pool); }
		/** Add task to the list - @see main Add function. */
		template<typename LAMBDA>
		void Add(const LAMBDA & lambda, i32 priority) { Add(lambda, priority, _pool); }
		/** Add task to the list - @see main Add function. */
		template<typename LAMBDA>
		void Add(const LAMBDA & lambda) { Add(lambda, _pool); }
		/**
		 * Wait for all in the list.
		 * This call also removes tasks from the list
		 * Allocated memory will not be freed, because of possible recycling.
		 * @warning - do not use iAmWorker flag if list contains the precedence.
		 */
		void Wait();
		/**
		 * Constructor.
		 * @see _pool
		 * @param defaultPool - sets pool which will be default for this list.
		 */
		TaskList(ThreadPool & defaultPool = ThreadPool::Instance());
		/** Wait while destruction. */
		~TaskList() { Wait(); }
	};


	/************************************************************************/
	/* Async                                                                */
	/************************************************************************/
	struct ThreadPool::AsyncBase
		: public IReferential
		, public ThreadPool::ITask
	{
		template<typename T>
		friend struct Future;
		friend struct Task;
		friend struct TaskBase;

	protected: // properties
		/** Thread pool in which would be async done. */
		ThreadPool & _pool;
		/** List for storing continuations.*/
		ThreadSafeObject<List<AsyncBase *, 4>> _continuation;

	protected: // methods
		/** */
		template<typename...TAIL>
		void addMe(AsyncBase * precedence, TAIL &...tail);
		void addMe() {}
		/** Register an async as continuation of this task. */
		void addContinuation(AsyncBase * task);
		/** Run continuations after this task is done. */
		void registerContinuations();

	public: // constructors
		/** Constructor defining which pool this task belongs to. */
		AsyncBase(IAction<> * routine, ThreadPool & pool, i32 priority, i32 access);
		/** Destructor. */
		virtual ~AsyncBase() {}
	};


	/************************************************************************/
	/* Task                                                                 */
	/************************************************************************/
	struct ThreadPool::TaskBase
		: public AsyncBase
	{
		friend struct Task;

	protected: // properties
		/** Lambda to be done. */
		Action<> _routineHolder;

	protected: // constructors
		/** Constructor using referenced lambda. */
		TaskBase(Action<> action, i32 priority, ThreadPool & pool, i32 access);
		/** Constructor. */
		template<typename LAMBDA>
		TaskBase(const LAMBDA & lambda, ThreadPool & pool, i32 priority);
		/** Constructor with precedence tasks. */
		template<typename LAMBDA, typename...PRECEDENCE>
		TaskBase(const LAMBDA & lambda, i32 priority, PRECEDENCE &...precedence);
	};


	struct ThreadPool::Task
		: public Reference<ThreadPool::TaskBase>
	{
		/** conversion operator*/
		operator AsyncBase *() { return Ptr(); }

		/** Nullptr constructor. */
		Task() : Reference() {}
		/** Constructor. */
		template<typename LAMBDA>
		Task(const LAMBDA & lambda, ThreadPool & pool = ThreadPool::Instance(), i32 priority = 0)
			: Reference(new TaskBase(lambda, pool, priority))
		{}
		/** Constructor with precedence. */
		template<typename LAMBDA, typename...PRECEDENCE>
		Task(const LAMBDA & lambda, AsyncBase * f, PRECEDENCE &...precedences)
			: Reference(new TaskBase(lambda, 0, f, precedences...))
		{}
	};

	/************************************************************************/
	/* Future                                                               */
	/************************************************************************/
	template<typename RESULT>
	struct ThreadPool::FutureBase
		: public AsyncBase
	{
		friend struct Future<RESULT>;

	private: // properties
		/** Result of future. */
		RESULT _result;
		/** Lambda to be done. */
		Action<> _routine;

	public: // methods
		/** Wait until task is done, then return result. */
		const RESULT & Result(u32 milliseconds = 250) const { Wait(milliseconds); return _result; }
		/** Wait until task is done, then return result. */
		RESULT & Result(u32 milliseconds = 250) { Wait(milliseconds); return _result; }
		/** Get result without check if routine to obtain result has finished. */
		const RESULT & UnsafeResult() const { return _result; }
		/** Get result without check if routine to obtain result has finished. */
		RESULT & UnsafeResult() { return _result; }

	private: // constructors
		/** Constructor using referenced lambda. */
		FutureBase(Action<> action, i32 priority, ThreadPool & pool, i32 access);
		/** Constructor. */
		template<typename LAMBDA>
		FutureBase(const LAMBDA & lambda, ThreadPool & pool, i32 priority);
		/** Constructor with precedence tasks. */
		template<typename LAMBDA, typename...PRECEDENCE>
		FutureBase(const LAMBDA & lambda, i32 priority, PRECEDENCE &...precedence);
	};


	template<typename RESULT>
	struct ThreadPool::Future
		: public Reference<ThreadPool::FutureBase<RESULT>>
	{
		/** Parent class. */
		using Base = Reference<ThreadPool::FutureBase<RESULT>>;

		/** Zero constructor. */
		Future() : Base() {}
		/** Constructor. */
		template<typename LAMBDA>
		Future(const LAMBDA & lambda, ThreadPool & pool = ThreadPool::Instance(), i32 priority = 0)
			: Base(new FutureBase<RESULT>(lambda, pool, priority))
		{}
		/** Constructor with precedence. */
		template<typename LAMBDA, typename...PRECEDENCE>
		Future(const LAMBDA & lambda, AsyncBase * f, PRECEDENCE &...precedences)
			: Base(new FutureBase<RESULT>(lambda, 0, f, precedences...))
		{}
	};
}


namespace DD::Concurrency {
	/************************************************************************/
	/* TaskLocal                                                            */
	/************************************************************************/
	template<size_t ALLOCATION>
	template<typename LAMBDA>
	ThreadPool::TaskLocal<ALLOCATION>::TaskLocal(
		const LAMBDA & lambda,
		i32 priority,
		ThreadPool & pool
	) : ITask(_lambda, priority, 1)
		, _lambda(lambda)
	{
		pool.RegisterAction(this);
	}


	template<size_t ALLOCATION>
	template<typename LAMBDA>
	void ThreadPool::TaskLocal<ALLOCATION>::Set(
		const LAMBDA & lambda,
		i32 priority,
		ThreadPool & pool
	) {
		_access = 1;
		_priority = priority;
		_lambda = lambda;
		pool.RegisterAction(this);
	}


	/************************************************************************/
	/* TaskList                                                             */
	/************************************************************************/
	template<size_t BLOCK_SIZE, size_t TASK_SIZE>
	template<typename LAMBDA>
	void ThreadPool::TaskList<BLOCK_SIZE, TASK_SIZE>::Add(
		const LAMBDA & lambda,
		i32 priority,
		ThreadPool & pool
	) {
		Base::template Add<const LAMBDA &, i32, ThreadPool &>(lambda, priority, pool);
	}


	template<size_t BLOCK_SIZE, size_t TASK_SIZE>
	void ThreadPool::TaskList<BLOCK_SIZE, TASK_SIZE>::Wait() {
		// wait for tasks that are currently being processed
		for (TaskLocal<TASK_SIZE> & task : Sequencer::Drive(Base::Sequencer())) {
			task.Wait();
		}

		// remove all the content
		Base::Free();
	}


	template<size_t BLOCK_SIZE, size_t TASK_SIZE>
	ThreadPool::TaskList<BLOCK_SIZE, TASK_SIZE>::TaskList(
		ThreadPool & defaultPool
	)
		: _pool(defaultPool)
	{}


	/************************************************************************/
	/* Async                                                                */
	/************************************************************************/
	template<typename ...TAIL>
	void ThreadPool::AsyncBase::addMe(AsyncBase * precedence, TAIL & ...tail) {
		precedence->addContinuation(this);
		addMe(tail...);
	}


	/************************************************************************/
	/* Task                                                                 */
	/************************************************************************/
	inline ThreadPool::TaskBase::TaskBase(Action<> action, i32 priority, ThreadPool & pool, i32 access)
		: AsyncBase(action, pool, priority, access)
		, _routineHolder(action)
	{}


	template<typename LAMBDA>
	ThreadPool::TaskBase::TaskBase(const LAMBDA & lambda, ThreadPool & pool, i32 priority)
		: TaskBase(
			Action<>([this, lambda = LAMBDA(lambda)]() mutable { lambda(); registerContinuations(); }),
			priority,
			pool,
			1
		)
	{
		pool.RegisterAction(this);
	}


	template<typename LAMBDA, typename...PRECEDENCE>
	ThreadPool::TaskBase::TaskBase(const LAMBDA & lambda, i32 priority, PRECEDENCE &...precedence)
		: TaskBase(
			Action<>([this, lambda = LAMBDA(lambda)]() mutable { lambda(); registerContinuations(); }),
			priority,
			Argument<0>(precedence...)->_pool,
			(i32)ArgumentCount<PRECEDENCE...>()
		)
	{
		addMe(precedence...);
	}


	/************************************************************************/
	/* Future                                                               */
	/************************************************************************/
	template<typename R>
	ThreadPool::FutureBase<R>::FutureBase(Action<> action, i32 priority, ThreadPool & pool, i32 access)
		: AsyncBase(action, pool, priority, access)
		, _routine(action)
	{}


	template<typename R>
	template<typename L>
	ThreadPool::FutureBase<R>::FutureBase(const L & lambda, ThreadPool & pool, i32 priority)
		: FutureBase(
			Action<>([this, lambda = L(lambda)]() mutable { _result = lambda(); registerContinuations(); }),
			priority,
			pool,
			1
		)
	{
		pool.RegisterAction(this);
	}


	template<typename R>
	template<typename L, typename...PRE>
	ThreadPool::FutureBase<R>::FutureBase(const L & lambda, i32 priority, PRE &...precedence)
		: TaskBase(
			Action<>([this, lambda = LAMBDA(lambda)]() mutable { _result = lambda(); registerContinuations(); }),
			priority,
			Argument<0>(precedence...)->_pool,
			ArgumentCount<PRE...>() + 1
		)
	{
		addMe(precedence...);
	}
}
