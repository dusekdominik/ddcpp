#pragma once
namespace DD::Concurrency {
	/**
	 * Multi-thread synchronization primitive.
	 * Read write lock.
	 * Enables multiple access for reading at the same time, or just exclusive access for writing.
	 *
	 * @example
	 *   struct MTObject {
	 *     Data _data;
	 *     Lock  _lock;
	 *     Data GetData() { Lock::SharedScope _(_lock); return _data; }
	 *     void SetData(const Data & data) { Lock::ExclusiveScope _(_lock); _data = data; }
	 *   };
	 */
	class Lock;
}

#include <DD/Concurrency/ConditionVariable.h>
#include <DD/Memory/NonCopyable.h>
#include <DD/Memory/DeferredImpl.h>

namespace DD::Concurrency {
	class Lock
		: Memory::NonCopyable
	{
	private: // nested
		/** Helper for native view of lock.  */
		struct NativeView;

	public: // nested
		/** Scope lock for shared access. */
		class SharedScope;
		/** Scope lock for exclusive access. */
		class ExclusiveScope;

	private: // properties
		/** Storage for system handle. */
		mutable Memory::DeferredImpl<NativeView, 8> _storage;

	public: // internal methods
		/** Helper method for type casting. */
		NativeView * nativeView() const { return &_storage; }

	public: // methods
		/** Init exclusive access. */
		void AcquireExclusiveAccess() const;
		/** Init shared access. */
		void AcquireSharedAccess() const;
		/** Terminate exclusive access. */
		void ReleaseExclusiveAccess() const;
		/** Terminate shared access. */
		void ReleaseSharedAccess() const;
		/** Init exclusive access if possible. */
		bool TryAcquireExclusiveAccess() const;
		/** Init shared access if possible. */
		bool TryAcquireSharedAccess() const;
		/** Sleep thread via condition variable. */
		SleepingResult SleepShared(const ConditionVariable & cv);
		/** Sleep thread via condition variable with a timeout. */
		SleepingResult SleepShared(const ConditionVariable & cv, int milliseconds);
		/** Sleep thread via condition variable. */
		SleepingResult SleepExclusive(const ConditionVariable & cv);
		/** Sleep thread via condition variable with a timeout. */
		SleepingResult SleepExclusive(const ConditionVariable & cv, int milliseconds);

	public: // constructors
		/** Constructor. */
		Lock();
		/** Destructor. */
		~Lock();
	};


	class Lock::ExclusiveScope {
		/** Lock to be locked. */
		const Lock & _lock;

	public: // constructors
		/** Constructor. */
		ExclusiveScope(const Lock & lock) : _lock(lock) { lock.AcquireExclusiveAccess(); }
		/** Destructor. */
		~ExclusiveScope() { _lock.ReleaseExclusiveAccess(); }
	};


	class Lock::SharedScope {
		/** Lock to be locked. */
		const Lock & _lock;

	public: // constructors
		/** Constructor. */
		SharedScope(const Lock & lock) : _lock(lock) { lock.AcquireSharedAccess(); }
		/** Destructor. */
		~SharedScope() { _lock.ReleaseSharedAccess(); }
	};
}
