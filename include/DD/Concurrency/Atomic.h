#pragma once
namespace DD::Concurrency::Atomic {
	/** 32-bit signed integer with atomic methods. */
	struct i32;
	/** 32-bit unsigned integer with atomic methods. */
	struct u32;
	/** 64-bit signed integer with atomic methods. */
	struct i64;
	/** 64-bit unsigned integer with atomic methods. */
	struct u64;

	/**
	 * Table translating native type into atomic types.
	 * Helper ty fot Atomic::Type<T>;s
	 */
	template<typename>
	struct TranslationTable;

	/**
	 * Atomic type from native type.
	 */
	template<typename T>
	using Type = typename TranslationTable<T>::type_t;
}

#include <DD/Types.h>

namespace DD::Concurrency::Atomic {
	struct i32 {
		static DD::i32 Add(volatile DD::i32 & base, DD::i32 value);
		static DD::i32 And(volatile DD::i32 & base, DD::i32 value);
		static DD::i32  Or(volatile DD::i32 & base, DD::i32 value);
		static DD::i32 Xor(volatile DD::i32 & base, DD::i32 value);
		static DD::i32 Inc(volatile DD::i32 & base);
		static DD::i32 Dec(volatile DD::i32 & base);
		static DD::i32 Load(const volatile DD::i32 & base);
		static DD::i32 Exchange(volatile DD::i32 & base, DD::i32 value);
		static DD::i32 CompareExchange(volatile DD::i32 & value, DD::i32 newValue, DD::i32 oldValue);
	private: // properties
		/** Storage. */
		volatile ::DD::i32 _value;
	public: // methods
		/** Returns volatile struct, which means that operators will be atomic. */
		volatile i32 & Atomic() { return *this; }
		/** Returns added value. */
		DD::i32 AtomicAdd(DD::i32 value) volatile { return Add(_value, value); }
		/** Returns ANDed value. */
		DD::i32 AtomicAnd(DD::i32 value) volatile { return And(_value, value); }
		/** Returns ORed value. */
		DD::i32 AtomicOr(DD::i32 value) volatile { return Or(_value, value); }
		/** Returns XORed value. */
		DD::i32 AtomicXor(DD::i32 value) volatile { return Xor(_value, value); }
		/** Returns incremented value. */
		DD::i32 AtomicInc() volatile { return Inc(_value); }
		/** Returns decremented value. */
		DD::i32 AtomicDec() volatile { return Dec(_value); }
		/** Return the value that was in the storage before. */
		DD::i32 AtomicExchange(DD::i32 value) volatile { return Exchange(_value, value); }
		/**
		 * Compare exchange value.
		 * @param newValue - value to be set.
		 * @param oldValue - value that must be in storage in the time of setting value.
		 * @return the value that was in storage in the time of setting.
		 * @example common usage
		 *   void updateValue(volatile DD::Atomic::i32 & value) {
		 *     do {
		 *       DD::i32 oldValue = value;
		 *       DD::i32 newValue = recomputeValue(oldValue);
		 *     } while (oldValue != value.AtomicCompareExchange(newValue, oldValue));
		 *   }
		 */
		DD::i32 AtomicCompareExchange(DD::i32 newValue, DD::i32 oldValue) volatile { return CompareExchange(_value, newValue, oldValue); }
	public: // operators
		/** Non-atomic increase, @return increased value. */
		DD::i32 operator++() { return ++_value; }
		/** Atomic increase, @return increased value. */
		DD::i32 operator++() volatile { return AtomicInc(); }
		/** Non-atomic decrease, @return decreased value. */
		DD::i32 operator--() { return --_value; }
		/** Atomic decrease, @return decreased value. */
		DD::i32 operator--() volatile { return AtomicDec(); }
		/** Non-atomic increase, @return original value. */
		DD::i32 operator++(int) { return _value++; }
		/** Atomic increase, @return original value. */
		DD::i32 operator++(int) volatile { return AtomicInc() - 1; }
		/** Non-atomic decrease, @return original value. */
		DD::i32 operator--(int) { return _value--; }
		/** Atomic decrease, @return original value. */
		DD::i32 operator--(int) volatile { return AtomicDec() + 1; }
		/** Non-atomic bitwise and, @return ADDed value. */
		DD::i32 operator&=(DD::i32 value) { return _value &= value; }
		/** Atomic bitwise and, @return ADDed value. */
		DD::i32 operator&=(DD::i32 value) volatile { return AtomicAnd(value); }
		/** Non-atomic bitwise or, @return ORed value. */
		DD::i32 operator|=(DD::i32 value) { return _value |= value; }
		/** Atomic bitwise or, @return ORed value. */
		DD::i32 operator|=(DD::i32 value) volatile { return AtomicOr(value); }
		/** Non-atomic bitwise or, @return XORed value. */
		DD::i32 operator^=(DD::i32 value) { return _value ^= value; }
		/** Atomic bitwise or, @return XORed value. */
		DD::i32 operator^=(DD::i32 value) volatile { return AtomicXor(value); }
		/** Non-atomic add. */
		DD::i32 operator+=(DD::i32 value) { return _value += value; }
		/** Atomic add. */
		DD::i32 operator+=(DD::i32 value) volatile { return AtomicAdd(value); }
		/** Non-atomic sub. */
		DD::i32 operator-=(DD::i32 value) { return _value -= value; }
		/** Atomic sub. */
		DD::i32 operator-=(DD::i32 value) volatile { return AtomicAdd(-value); }
		/** Convert to int. */
		operator DD::i32() const volatile { return Load(_value); }
		/** Convert to int. */
		operator DD::i32() const { return _value; }
		/** Non atomic assignment. */
		i32 & operator=(DD::i32 value) { _value = value; return *this; }
		/** Atomic assignment. */
		volatile i32 & operator=(DD::i32 value) volatile { Exchange(_value, value); return *this; }
	public: // constructors
		/** Zero constructor - do not initialize value. */
		i32() {}
		/** Implicit constructor. */
		i32(DD::i32 value) : _value(value) {}
	};

	struct u32 {
		static DD::u32 Add(volatile DD::u32 & base, DD::u32 value);
		static DD::u32 And(volatile DD::u32 & base, DD::u32 value);
		static DD::u32  Or(volatile DD::u32 & base, DD::u32 value);
		static DD::u32 Xor(volatile DD::u32 & base, DD::u32 value);
		static DD::u32 Inc(volatile DD::u32 & base);
		static DD::u32 Dec(volatile DD::u32 & base);
		static DD::u32 Load(const volatile DD::u32 & base);
		static DD::u32 Exchange(volatile DD::u32 & base, DD::u32 value);
		static DD::u32 CompareExchange(volatile DD::u32 & value, DD::u32 newValue, DD::u32 oldValue);
	private: // properties
		/** Storage. */
		volatile ::DD::u32 _value;
	public: // methods
		/** Returns volatile struct, which means that operators will be atomic. */
		volatile u32 & Atomic() { return *this; }
		/** Returns added value. */
		DD::u32 AtomicAdd(DD::u32 value) volatile { return Add(_value, value); }
		/** Returns ANDed value. */
		DD::u32 AtomicAnd(DD::u32 value) volatile { return And(_value, value); }
		/** Returns ORed value. */
		DD::u32 AtomicOr(DD::u32 value) volatile { return Or(_value, value); }
		/** Returns XORed value. */
		DD::u32 AtomicXor(DD::u32 value) volatile { return Xor(_value, value); }
		/** Returns incremented value. */
		DD::u32 AtomicInc() volatile { return Inc(_value); }
		/** Returns decremented value. */
		DD::u32 AtomicDec() volatile { return Dec(_value); }
		/** Return the value that was in the storage before. */
		DD::u32 AtomicExchange(DD::u32 value) volatile { return Exchange(_value, value); }
		/**
		 * Compare exchange value.
		 * @param newValue - value to be set.
		 * @param oldValue - value that must be in storage in the time of setting value.
		 * @return the value that was in storage in the time of setting.
		 * @example common usage
		 *   void updateValue(volatile DD::Atomic::u32 & value) {
		 *     do {
		 *       DD::u32 oldValue = value;
		 *       DD::u32 newValue = recomputeValue(oldValue);
		 *     } while (oldValue != value.AtomicCompareExchange(newValue, oldValue));
		 *   }
		 */
		DD::u32 AtomicCompareExchange(DD::u32 newValue, DD::u32 oldValue) volatile { return CompareExchange(_value, newValue, oldValue); }
	public: // operators
		/** Non-atomic increase, @return increased value. */
		DD::u32 operator++() { return ++_value; }
		/** Atomic increase, @return increased value. */
		DD::u32 operator++() volatile { return AtomicInc(); }
		/** Non-atomic decrease, @return decreased value. */
		DD::u32 operator--() { return --_value; }
		/** Atomic decrease, @return decreased value. */
		DD::u32 operator--() volatile { return AtomicDec(); }
		/** Non-atomic increase, @return original value. */
		DD::u32 operator++(int) { return _value++; }
		/** Atomic increase, @return original value. */
		DD::u32 operator++(int) volatile { return AtomicInc() - 1; }
		/** Non-atomic decrease, @return original value. */
		DD::u32 operator--(int) { return _value--; }
		/** Atomic decrease, @return original value. */
		DD::u32 operator--(int) volatile { return AtomicDec() + 1; }
		/** Non-atomic bitwise and, @return ADDed value. */
		DD::u32 operator&=(DD::u32 value) { return _value &= value; }
		/** Atomic bitwise and, @return ADDed value. */
		DD::u32 operator&=(DD::u32 value) volatile { return AtomicAnd(value); }
		/** Non-atomic bitwise or, @return ORed value. */
		DD::u32 operator|=(DD::u32 value) { return _value |= value; }
		/** Atomic bitwise or, @return ORed value. */
		DD::u32 operator|=(DD::u32 value) volatile { return AtomicOr(value); }
		/** Non-atomic bitwise or, @return XORed value. */
		DD::u32 operator^=(DD::u32 value) { return _value ^= value; }
		/** Atomic bitwise or, @return XORed value. */
		DD::u32 operator^=(DD::u32 value) volatile { return AtomicXor(value); }
		/** Non-atomic add. */
		DD::u32 operator+=(DD::u32 value) { return _value += value; }
		/** Atomic add. */
		DD::u32 operator+=(DD::u32 value) volatile { return AtomicAdd(value); }
		/** Non-atomic sub. */
		DD::u32 operator-=(DD::u32 value) { return _value -= value; }
		/** Atomic sub. */
		DD::u32 operator-=(DD::u32 value) volatile { return AtomicAdd(~value + 1); }
		/** Convert to int. */
		operator DD::u32() const volatile { return Load(_value); }
		/** Convert to int. */
		operator DD::u32() const { return _value; }
		/** Non atomic assignment. */
		u32 & operator=(DD::u32 value) { _value = value; return *this; }
		/** Atomic assignment. */
		volatile u32 & operator=(DD::u32 value) volatile { Exchange(_value, value); return *this; }
	public: // constructors
		/** Zero constructor - do not initialize value. */
		u32() {}
		/** Implicit constructor. */
		u32(DD::u32 value) : _value(value) {}
	};

	struct i64 {
		static DD::i64 Add(volatile DD::i64 & base, DD::i64 value);
		static DD::i64 And(volatile DD::i64 & base, DD::i64 value);
		static DD::i64  Or(volatile DD::i64 & base, DD::i64 value);
		static DD::i64 Xor(volatile DD::i64 & base, DD::i64 value);
		static DD::i64 Inc(volatile DD::i64 & base);
		static DD::i64 Dec(volatile DD::i64 & base);
		static DD::i64 Load(const volatile DD::i64 & base);
		static DD::i64 Exchange(volatile DD::i64 & base, DD::i64 value);
		static DD::i64 CompareExchange(volatile DD::i64 & value, DD::i64 newValue, DD::i64 oldValue);
	private: // properties
		/** Storage. */
		volatile ::DD::i64 _value;
	public: // methods
		/** Returns volatile struct, which means that operators will be atomic. */
		volatile i64 & Atomic() { return *this; }
		/** Returns added value. */
		DD::i64 AtomicAdd(DD::i64 value) volatile { return Add(_value, value); }
		/** Returns ANDed value. */
		DD::i64 AtomicAnd(DD::i64 value) volatile { return And(_value, value); }
		/** Returns ORed value. */
		DD::i64 AtomicOr(DD::i64 value) volatile { return Or(_value, value); }
		/** Returns XORed value. */
		DD::i64 AtomicXor(DD::i64 value) volatile { return Xor(_value, value); }
		/** Returns incremented value. */
		DD::i64 AtomicInc() volatile { return Inc(_value); }
		/** Returns decremented value. */
		DD::i64 AtomicDec() volatile { return Dec(_value); }
		/** Return the value that was in the storage before. */
		DD::i64 AtomicExchange(DD::i64 value) volatile { return Exchange(_value, value); }
		/**
		 * Compare exchange value.
		 * @param newValue - value to be set.
		 * @param oldValue - value that must be in storage in the time of setting value.
		 * @return the value that was in storage in the time of setting.
		 * @example common usage
		 *   void updateValue(volatile DD::Atomic::i64 & value) {
		 *     do {
		 *       DD::i64 oldValue = value;
		 *       DD::i64 newValue = recomputeValue(oldValue);
		 *     } while (oldValue != value.AtomicCompareExchange(newValue, oldValue));
		 *   }
		 */
		DD::i64 AtomicCompareExchange(DD::i64 newValue, DD::i64 oldValue) volatile { return CompareExchange(_value, newValue, oldValue); }
	public: // operators
		/** Non-atomic increase, @return increased value. */
		DD::i64 operator++() { return ++_value; }
		/** Atomic increase, @return increased value. */
		DD::i64 operator++() volatile { return AtomicInc(); }
		/** Non-atomic decrease, @return decreased value. */
		DD::i64 operator--() { return --_value; }
		/** Atomic decrease, @return decreased value. */
		DD::i64 operator--() volatile { return AtomicDec(); }
		/** Non-atomic increase, @return original value. */
		DD::i64 operator++(int) { return _value++; }
		/** Atomic increase, @return original value. */
		DD::i64 operator++(int) volatile { return AtomicInc() - 1; }
		/** Non-atomic decrease, @return original value. */
		DD::i64 operator--(int) { return _value--; }
		/** Atomic decrease, @return original value. */
		DD::i64 operator--(int) volatile { return AtomicDec() + 1; }
		/** Non-atomic bitwise and, @return ADDed value. */
		DD::i64 operator&=(DD::i64 value) { return _value &= value; }
		/** Atomic bitwise and, @return ADDed value. */
		DD::i64 operator&=(DD::i64 value) volatile { return AtomicAnd(value); }
		/** Non-atomic bitwise or, @return ORed value. */
		DD::i64 operator|=(DD::i64 value) { return _value |= value; }
		/** Atomic bitwise or, @return ORed value. */
		DD::i64 operator|=(DD::i64 value) volatile { return AtomicOr(value); }
		/** Non-atomic bitwise or, @return XORed value. */
		DD::i64 operator^=(DD::i64 value) { return _value ^= value; }
		/** Atomic bitwise or, @return XORed value. */
		DD::i64 operator^=(DD::i64 value) volatile { return AtomicXor(value); }
		/** Non-atomic add. */
		DD::i64 operator+=(DD::i64 value) { return _value += value; }
		/** Atomic add. */
		DD::i64 operator+=(DD::i64 value) volatile { return AtomicAdd(value); }
		/** Non-atomic sub. */
		DD::i64 operator-=(DD::i64 value) { return _value -= value; }
		/** Atomic sub. */
		DD::i64 operator-=(DD::i64 value) volatile { return AtomicAdd(-value); }
		/** Convert to int. */
		operator DD::i64() const volatile { return Load(_value); }
		/** Convert to int. */
		operator DD::i64() const { return _value; }
		/** Non atomic assignment. */
		i64 & operator=(DD::i64 value) { _value = value; return *this; }
		/** Atomic assignment. */
		volatile i64 & operator=(DD::i64 value) volatile { Exchange(_value, value); return *this; }
	public: // constructors
		/** Zero constructor - do not initialize value. */
		i64() {}
		/** Implicit constructor. */
		i64(DD::i64 value) : _value(value) {}
	};

	struct u64 {
		static DD::u64 Add(volatile DD::u64 & base, DD::u64 value);
		static DD::u64 And(volatile DD::u64 & base, DD::u64 value);
		static DD::u64  Or(volatile DD::u64 & base, DD::u64 value);
		static DD::u64 Xor(volatile DD::u64 & base, DD::u64 value);
		static DD::u64 Inc(volatile DD::u64 & base);
		static DD::u64 Dec(volatile DD::u64 & base);
		static DD::u64 Load(const volatile DD::u64 & base);
		static DD::u64 Exchange(volatile DD::u64 & base, DD::u64 value);
		static DD::u64 CompareExchange(volatile DD::u64 & value, DD::u64 newValue, DD::u64 oldValue);
	private: // properties
		/** Storage. */
		volatile ::DD::u64 _value;
	public: // methods
		/** Returns volatile struct, which means that operators will be atomic. */
		volatile u64 & Atomic() { return *this; }
		/** Returns added value. */
		DD::u64 AtomicAdd(DD::u64 value) volatile { return Add(_value, value); }
		/** Returns ANDed value. */
		DD::u64 AtomicAnd(DD::u64 value) volatile { return And(_value, value); }
		/** Returns ORed value. */
		DD::u64 AtomicOr(DD::u64 value) volatile { return Or(_value, value); }
		/** Returns XORed value. */
		DD::u64 AtomicXor(DD::u64 value) volatile { return Xor(_value, value); }
		/** Returns incremented value. */
		DD::u64 AtomicInc() volatile { return Inc(_value); }
		/** Returns decremented value. */
		DD::u64 AtomicDec() volatile { return Dec(_value); }
		/** Return the value that was in the storage before. */
		DD::u64 AtomicExchange(DD::u64 value) volatile { return Exchange(_value, value); }
		/**
		 * Compare exchange value.
		 * @param newValue - value to be set.
		 * @param oldValue - value that must be in storage in the time of setting value.
		 * @return the value that was in storage in the time of setting.
		 * @example common usage
		 *   void updateValue(volatile DD::Atomic::i64 & value) {
		 *     do {
		 *       DD::i64 oldValue = value;
		 *       DD::i64 newValue = recomputeValue(oldValue);
		 *     } while (oldValue != value.AtomicCompareExchange(newValue, oldValue));
		 *   }
		 */
		DD::u64 AtomicCompareExchange(DD::u64 newValue, DD::u64 oldValue) volatile { return CompareExchange(_value, newValue, oldValue); }
	public: // operators
		/** Non-atomic increase, @return increased value. */
		DD::u64 operator++() { return ++_value; }
		/** Atomic increase, @return increased value. */
		DD::u64 operator++() volatile { return AtomicInc(); }
		/** Non-atomic decrease, @return decreased value. */
		DD::u64 operator--() { return --_value; }
		/** Atomic decrease, @return decreased value. */
		DD::u64 operator--() volatile { return AtomicDec(); }
		/** Non-atomic increase, @return increased value. */
		DD::u64 operator++(int) { return _value++; }
		/** Atomic increase, @return increased value. */
		DD::u64 operator++(int) volatile { return AtomicInc() - 1; }
		/** Non-atomic decrease, @return decreased value. */
		DD::u64 operator--(int) { return _value--; }
		/** Atomic decrease, @return decreased value. */
		DD::u64 operator--(int) volatile { return AtomicDec() + 1; }
		/** Non-atomic bitwise and, @return ADDed value. */
		DD::u64 operator&=(DD::u64 value) { return _value &= value; }
		/** Atomic bitwise and, @return ADDed value. */
		DD::u64 operator&=(DD::u64 value) volatile { return AtomicAnd(value); }
		/** Non-atomic bitwise or, @return ORed value. */
		DD::u64 operator|=(DD::u64 value) { return _value |= value; }
		/** Atomic bitwise or, @return ORed value. */
		DD::u64 operator|=(DD::u64 value) volatile { return AtomicOr(value); }
		/** Non-atomic bitwise or, @return XORed value. */
		DD::u64 operator^=(DD::u64 value) { return _value ^= value; }
		/** Atomic bitwise or, @return XORed value. */
		DD::u64 operator^=(DD::u64 value) volatile { return AtomicXor(value); }
		/** Non-atomic add. */
		DD::u64 operator+=(DD::u64 value) { return _value += value; }
		/** Atomic add. */
		DD::u64 operator+=(DD::u64 value) volatile { return AtomicAdd(~value + 1); }
		/** Non-atomic sub. */
		DD::u64 operator-=(DD::u64 value) { return _value -= value; }
		/** Convert to int. */
		operator DD::i64() const volatile { return Load(_value); }
		/** Convert to int. */
		operator DD::i64() const { return _value; }
		/** Non atomic assignment. */
		u64 & operator=(DD::u64 value) { _value = value; return *this; }
		/** Atomic assignment. */
		volatile u64 & operator=(DD::u64 value) volatile { Exchange(_value, value); return *this; }
	public: // constructors
		/** Zero constructor - do not initialize value. */
		u64() {}
		/** Implicit constructor. */
		u64(DD::u64 value) : _value(value) {}
	};

	template<> struct TranslationTable<DD::i32> { typedef typename DD::Concurrency::Atomic::i32 type_t; };
	template<> struct TranslationTable<DD::u32> { typedef typename DD::Concurrency::Atomic::u32 type_t; };
	template<> struct TranslationTable<DD::i64> { typedef typename DD::Concurrency::Atomic::i64 type_t; };
	template<> struct TranslationTable<DD::u64> { typedef typename DD::Concurrency::Atomic::u64 type_t; };
}
