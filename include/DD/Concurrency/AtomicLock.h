#pragma once
namespace DD::Concurrency {
	/**
	 * Lightweight implementation of Lock.
	 * @warning lock is not recursive.
	 * @example common usage
	 *   struct MTSafeStruct {
	 *     AtomicLock _lock;
	 *     void MTSafeMethod() {
	 *       AtomicLock::Scope scope(_lock);
	 *       ...MT safe computation...
	 *     }
	 *   };
	 */
	struct AtomicLock;
}

#include <DD/Concurrency/Atomic.h>
#include <DD/Concurrency/Thread.h>

namespace DD::Concurrency {
	struct AtomicLock {
	public: // nested
		/** Struct for locking scopes. */
		struct Scope {
			/** Handle to lock object. */
			const AtomicLock & _lock;

			/** Constructor. */
			Scope(const AtomicLock & lock) : _lock(lock) { _lock.Lock(); }
			/** Destructor. */
			~Scope() { _lock.Unlock(); }
		};

	protected: // properties
		/** Number of accessing threads.  */
		mutable volatile i32 _access;

	public: // methods
		/** Get the access, otherwise spin. */
		void Lock() const;
		/** Release the access. */
		void Unlock() const { Atomic::i32::Dec(_access); }

	public: // constructors
		/** Constructor. */
		AtomicLock() : _access(0) {}
	};
}

namespace DD::Concurrency {
	inline void AtomicLock::Lock() const {
		// spin until my access is exclusive
		while (Atomic::i32::Inc(_access) != 1)
			Atomic::i32::Dec(_access);
		// store caches for safe access
		Thread::MemoryBarrier();
	}
}
