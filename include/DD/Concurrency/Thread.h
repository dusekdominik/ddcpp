#pragma once
namespace DD::Concurrency {
	/** Priorities of thread. */
	enum struct ThreadPriority {
		Idle = -15,
		Lowest = -2,
		BelowNormal = -1,
		Normal = 0,
		AboveNormal = 1,
		Highest = 2,
		Critical = 15
	};

	/** Thread abstraction. */
	class ThreadBase;

	/** Referential object. */
	class Thread;
}

#include <DD/Array.h>
#include <DD/Lambda.h>
#include <DD/Reference.h>
#include <DD/String.h>
#include <DD/Types.h>

namespace DD::Concurrency {
	class ThreadBase
		: public IReferential
	{
		friend class Thread;

	protected: // nested
		/** Descriptor of internal storage. */
		struct NativeView;

	protected: // properties
		/** Storage for internal data. */
		u8 _storage[8];
		/** Id of the thread. */
		u32 _id;
		/** Name of the thread. */
		StringLocal<32> _name;
		/** Routine of the thread.  */
		Action<> _routine;

	protected: // methods
		/** Get native view of object storage. */
		NativeView * nativeView() const { return (NativeView *)_storage; }

	public: // methods
		/** If of thread. */
		u32 ThreadId() const { return _id; }
		/** Routine of thread. */
		Lambda<void()> Routine() const { return _routine; }
		/** Detach thread from object - destructor will not wait for end. */
		ThreadBase & Detach();
		/** Suspend running thread. */
		ThreadBase & Suspend();
		/** Resume suspended thread. */
		ThreadBase & Resume();
		/** Kill the thread. */
		ThreadBase & Terminate();
		/** Set priority of thread. */
		ThreadBase & Priority(ThreadPriority priority);
		/** Wait for end. */
		ThreadBase & Wait();
		/** Wait for end. If timeout elapsed, returns false. */
		bool Wait(u32 milliseconds);
		/** Priority of thread. */
		ThreadPriority Priority() const;
		/** Check if the thread is the running one. */
		bool IsCurrent() const;
		/** Name of the thread. */
		const Text::StringView16 & Name() const { return _name; }

	private: // constructors
		/** Constructor. */
		ThreadBase(Lambda<void()>, Text::StringView16, bool, size_t);

	public: // constructors
		/** Destructor. */
		~ThreadBase();
	};

	class Thread
		: public Reference<ThreadBase>
	{
	public: // statics
		/** Wait for multiple threads. */
		static void WaitAll(Thread * threads, size_t threadCount);
		/** Wait for multiple threads. */
		static bool WaitAll(Thread * threads, size_t threadCount, u32 milliseconds);
		/** Wait for multiple threads. */
		static void WaitAll(const ArrayView1D<Thread> & threads) { WaitAll(threads.begin(), threads.Length()); }
		/** Wait for multiple threads. */
		static bool WaitAll(const ArrayView1D<Thread> & threads, u32 milliseconds) { return WaitAll(threads.begin(), threads.Length(), milliseconds); }
		/** Sleep. */
		static void Sleep(u32 milliseconds);
		/** Memory barrier. */
		static void MemoryBarrier();
		/** Id of current thread. */
		static u32 Id();
		/** Id of the main thread. */
		static u32 MainId();
		/** Assert main thread. */
		static void AssertMain();
		/** Check if current thread is main.  */
		static bool IsMain() { return Id() == MainId(); }

	public: // constructors
		/** Zero constructor. */
		Thread() : Reference() {}
		/** Catch constructor. */
		Thread(ThreadBase * thread) : Reference(thread) {}
		/**
		 * Standard constructor.
		 * @param whatToDo  - lambda or functor which will be called by thread.
		 * @param name      - name of the thread to be simply identified in debugger.
		 * @param suspended - should be running since creation or by resume call.
		 * @param stackSize - custom size of stack, set 0 to use default size (typically 1 MB).
		 */
		template<typename LAMBDA>
		Thread(
			const LAMBDA & whatToDo,
			const Text::StringView16 & name = L"Unnamed thread",
			bool startSuspended = false,
			size_t stackSize = 0
		) : Reference(new ThreadBase(whatToDo, name, startSuspended, stackSize))
		{}
	};
}
