#pragma once
namespace DD::Concurrency {
	/** Helper class. */
	class AwaitableBase;

	/**
	 * Object which can obtain value from one thread and read it in another one.
	 * Synchronization object.
	 */
	template<typename T>
	class Awaitable;

	typedef Awaitable<bool> AwaitBool;
	typedef Awaitable<__int8> AwaitI8;
	typedef Awaitable<__int16> AwaitI16;
	typedef Awaitable<__int32> AwaitI32;
	typedef Awaitable<__int64> AwaitI64;
	typedef Awaitable<unsigned __int8> AwaitU8;
	typedef Awaitable<unsigned __int16> AwaitU16;
	typedef Awaitable<unsigned __int32> AwaitU32;
	typedef Awaitable<unsigned __int64> AwaitU64;
	typedef Awaitable<float> AwaitF32;
	typedef Awaitable<double> AwaitF64;
}

#include <DD/Reference.h>
#include <DD/Types.h>

namespace DD::Concurrency {
	class AwaitableBase {
	public: // statics
		/** Sleep at least for given ms. */
		static void Sleep(u32 ms);
	};


	template<typename T>
	class Awaitable
		: AwaitableBase
	{
	public: // nested
		struct Base { bool HasValue = false; T Value; };

	private: // properties
		/** Data storage. */
		Reference<Base> _data;

	public: // methods
		/** Wait until value is set and then return it. */
		const T & Get() const;
		/** Wait until value is set and then return it. */
		T & Get();
		/** Set value. */
		void Set(const T & value);
		/** Set value. */
		void Set(T && value);
		/** Disconnect this instance from current storage and create new one. */
		void Reset();
		/** Set value to current storage and disconnect it form this instance. */
		void SetAndReset(const T & value);
		/** Set value to current storage and disconnect it form this instance. */
		void SetAndReset(T && value);

	public: // operators
		/** Getter alias. */
		const T & operator*() const { return Get(); }
		/** Getter alias. */
		T & operator*() { return Get(); }
		/** Setter alias. */
		void operator=(const T & value) { Set(value); }
		/** Setter alias. */
		void operator=(T && value) { Set(value); }

	public: // constructors
		Awaitable() : _data(new Base) {}
	};
}

namespace DD::Concurrency {
	template<typename T>
	const T & Awaitable<T>::Get() const {
		while (!_data->HasValue) Sleep(1);
		return (_data->Value);
	}

	template<typename T>
	T & Awaitable<T>::Get() {
		while (!_data->HasValue) Sleep(1);
		return (_data->Value);
	}

	template<typename T>
	void Awaitable<T>::Set(const T & value) {
		_data->Value = value;
		_data->HasValue = true;
	}

	template<typename T>
	void Awaitable<T>::Set(T && value) {
		_data->Value = value;
		_data->HasValue = true;
	}

	template<typename T>
	void Awaitable<T>::Reset() {
		_data = new Base;
	}

	template<typename T>
	void Awaitable<T>::SetAndReset(const T & value) {
		Set(value);
		Reset();
	}

	template<typename T>
	void Awaitable<T>::SetAndReset(T && value) {
		Set(value);
		Reset();
	}
}
