#pragma once
namespace DD::Concurrency {
	/**
	 * CriticalSection
	 * Recursive-mutex-like structure.
	 * Cannot be shared between processes.
	 *
	 * @example
	 *   struct MTObject {
	 *     Data _data;
	 *     CriticalSection _section;
	 *     Data GetData() { CriticalSection::Scope _(_section); return _data; }
	 *     void SetData(const Data & data) { CriticalSection::Scope _(_section); _data = data; }
	 *   };
	 */
	class CriticalSection;
}

#include <DD/Concurrency/ConditionVariable.h>
#include <DD/Memory/DeferredImpl.h>
#include <DD/Memory/NonCopyable.h>
#include <DD/Types.h>

namespace DD::Concurrency {
	class CriticalSection
		: public Memory::NonCopyable
	{
	public: // nested
		/** Casting helper. */
		struct NativeView;
		/** Scope lock. */
		class Scope;

	private: // properties
		/** Reserved data for native handles. */
		mutable Memory::DeferredImpl<NativeView, 40> _criticalSection;

	public: // methods
		/** Internal cast method. */
		NativeView * nativeView() const { return &_criticalSection; }
		/** Lock critical section. */
		void Enter() const;
		/** Unlock critical section. */
		void Leave() const;
		/** Lock if it is possible. */
		bool TryEnter() const;
		/** Sleep section via condition variable. */
		SleepingResult Sleep(const ConditionVariable & cv);
		/** Sleep section via condition variable with a timeout. */
		SleepingResult Sleep(const ConditionVariable & cv, int milliseconds);

	public: // constructors
		/** Explicitly removed assignment. */
		CriticalSection & operator=(const CriticalSection &) = delete;
		/** Constructor. */
		CriticalSection();
		/**
		 * Constructor.
		 * @param spinCount - number of spinning cycles before sleep.
		 */
		CriticalSection(u32 spinCount);
		/** Copy constructor. */
		CriticalSection(const CriticalSection &) = delete;
		/** Destructor. */
		~CriticalSection();
	};


	class CriticalSection::Scope
		: Memory::NonCopyable
	{
		/** Section to be locked. */
		const CriticalSection & _section;

	public: // constructors
		/** Constructor. */
		Scope(const CriticalSection & section) : _section(section) { _section.Enter(); }
		/** Destructor. */
		~Scope() { _section.Leave(); }
	};
}
