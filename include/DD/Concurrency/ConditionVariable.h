#pragma once
namespace DD::Concurrency {
	/**
	 * Result of sleep methods.
	 */
	enum SleepingResult { Error, Succeed, Timeout };

	/**
	 * Multi-thread synchronization primitive.
	 * Typically used for consumer-producer pattern.
	 *
	 * @example
	 *   ConditionVariable cv;     // conditional variable for production
	 *   CriticalSection   cs;     // lock for mt sync
	 *   Queue<Data>       fifo;   // container for data storage
	 *   void produce() {
	 *     Data d = createData();  // create data for consumer
	 *     cs.Enter();
	 *     fifo.Push(d);           // safely add data for consumer
	 *     cs.Leave();
	 *     cv.WakeOne();           // notify consumer
	 *   }
	 *   void consume() {
	 *     Data d;
	 *     cs.Enter();
	 *     while (!fifo.TryPop(d)) // acquire data for consumer
	 *       cs.Sleep(cv);
	 *     cs.Leave();
	 *     doAnythingWithData(d);  // consume data
	 *   }
	 */
	class ConditionVariable;
}

#include <DD/Concurrency/CriticalSection.h>
#include <DD/Concurrency/Lock.h>
#include <DD/Memory/DeferredImpl.h>
#include <DD/Memory/NonCopyable.h>

namespace DD::Concurrency {
	class ConditionVariable
		: public Memory::NonCopyable
	{
	public: // nested
		/** Casting helper. */
		struct NativeView;

	private: // properties
		/** Storage for native object. */
		mutable Memory::DeferredImpl<NativeView, 8> _storage;

	public: // internal methods
		/** Casting helper. */
		NativeView * nativeView() const { return &_storage; }

	public: // methods
		/** Sleep section via condition variable. */
		SleepingResult SleepCriticalSection(const CriticalSection & cs) const;
		/** Sleep section via condition variable. */
		SleepingResult SleepCriticalSection(const CriticalSection & cs, int milliseconds) const;
		/** Sleep lock via condition variable. */
		SleepingResult SleepLockExclusive(const Lock & lock) const;
		/** Sleep lock via condition variable. */
		SleepingResult SleepLockExclusive(const Lock & lock, int milliseconds) const;
		/** Sleep lock via condition variable. */
		SleepingResult SleepLockShared(const Lock & lock) const;
		/** Sleep lock via condition variable. */
		SleepingResult SleepLockShared(const Lock & lock, int milliseconds) const;
		/** Wakes one sleeping thread, if exists. */
		void WakeOne() const;
		/** Wake all sleeping threads. */
		void WakeAll() const;

	public: // constructors
		/** Constructor. */
		ConditionVariable();
		/** Destructor. */
		~ConditionVariable();
	};
}
