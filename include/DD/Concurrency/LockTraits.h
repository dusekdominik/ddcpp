#pragma once
namespace DD::Concurrency {
	/**
	 * Traits for locking by universal interface.
	 * @param LOCK - locking primitive.
	 */
	template<typename LOCK>
	struct LockTraits;
}

#include <DD/Concurrency/AtomicLock.h>
#include <DD/Concurrency/CriticalSection.h>
#include <DD/Concurrency/Lock.h>

namespace DD::Concurrency {
	template<typename LOCK>
	struct LockTraits {
		/** Initialize shared scope. */
		static void AcquireShared(LOCK &); // { static_assert(false, "Not Implemented"); }
		/** Initialize exclusive scope. */
		static void AcquireExclusive(LOCK &); // { static_assert(false, "Not Implemented"); }
		/** Finalize shared scope. */
		static void ReleaseShared(LOCK &); // { static_assert(false, "Not Implemented"); }
		/** Finalize exclusive scope. */
		static void ReleaseExclusive(LOCK &); // { static_assert(false, "Not Implemented"); }
	};
}

namespace DD::Concurrency {
	template<>
	struct LockTraits<AtomicLock> {
		static void AcquireShared(AtomicLock & lock) { lock.Lock(); }
		static void AcquireExclusive(AtomicLock & lock) { lock.Lock(); }
		static void ReleaseShared(AtomicLock & lock) { lock.Unlock(); }
		static void ReleaseExclusive(AtomicLock & lock) { lock.Unlock(); }
	};

	template<>
	struct LockTraits<CriticalSection> {
		static void AcquireShared(CriticalSection & lock) { lock.Enter(); }
		static void AcquireExclusive(CriticalSection & lock) { lock.Enter(); }
		static void ReleaseShared(CriticalSection & lock) { lock.Leave(); }
		static void ReleaseExclusive(CriticalSection & lock) { lock.Leave(); }
	};

	template<>
	struct LockTraits<Lock> {
		static void AcquireShared(Lock & lock) { lock.AcquireSharedAccess(); }
		static void AcquireExclusive(Lock & lock) { lock.AcquireExclusiveAccess(); }
		static void ReleaseShared(Lock & lock) { lock.ReleaseSharedAccess(); }
		static void ReleaseExclusive(Lock & lock) { lock.ReleaseExclusiveAccess(); }
	};
}
