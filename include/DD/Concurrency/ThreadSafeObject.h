#pragma once
namespace DD::Concurrency {
	/**
	 * Thread safe interface for controlling an object.
	 * Creates an proxy object which locks synchronization
	 * primitive while construction and unlock while destruction.
	 *
	 * Recursively lockable.
	 */
	template<typename T, typename LOCK>
	class ThreadSafeObject;
}

#include <DD/Concurrency/CriticalSection.h>
#include <DD/Concurrency/LockTraits.h>
#include <DD/Memory/Rigid.h>

namespace DD::Concurrency {
	template<typename T, typename LOCK = CriticalSection>
	class ThreadSafeObject
		: Memory::Rigid
	{
		void usingProxy();

	public: // nested
		/** Proxy for reading. */
		class ConstProxy;
		/** Proxy for writing. */
		class Proxy;

	private: // properties
		/** Source object. */
		T _ref;
		/** Synchronization primitive, recursive mutex. */
		mutable LOCK _mtx;

	public: // methods
		/** Read access. */
		ConstProxy GetConstProxy() const { return ConstProxy(*this); }
		/** Read access. */
		ConstProxy GetProxy() const { return ConstProxy(*this); }
		/** Write access. */
		Proxy GetProxy() { return Proxy(*this); }
		/** Get access to object without locking. */
		T & GetUnsafeAccess() { return _ref; }
		/** Get const access to object without locking. */
		const T & GetUnsafeAccess() const { return _ref; }
		/** Read access. */
		ConstProxy operator->() const { return ConstProxy(*this); }
		/** Write access. */
		Proxy operator->() { return Proxy(*this); }
		/** Read access. */
		ConstProxy operator*() const { return ConstProxy(*this); }
		/** Write access. */
		Proxy operator*() { return Proxy(*this); }
		/** Assignment operator. */
		ThreadSafeObject & operator=(const T & object) { _ref = object; return *this; }
		/** Assignment operator. */
		ThreadSafeObject & operator=(T && object) { _ref = object; return *this; }
		/** Assignment operator. */
		ThreadSafeObject & operator=(ThreadSafeObject && rvalue) { _ref = static_cast<T&&>(rvalue._ref); return *this; }
		/** Assignment operator. */
		ThreadSafeObject & operator=(const ThreadSafeObject & rvalue) { _ref = (rvalue._ref); return *this; }

	public: // constructors
		/** Constructor provides argument forwarding to a source object. */
		template<typename...ARGS>
		explicit ThreadSafeObject(const ARGS &...args) : _ref(args...) {}
		/** Destructor. */
		~ThreadSafeObject() { LockTraits<LOCK>::AcquireExclusive(_mtx); }
	};


	template<typename T, typename LOCK>
	class ThreadSafeObject<T, LOCK>::ConstProxy {
	private: // properties
		/** Reference to source object. */
		const ThreadSafeObject<T, LOCK> & _obj;

	public: // methods
		/** Raw access. */
		const T & Object() const { return _obj._ref; }
		/** Simplified access. */
		const T * operator->() const { return &(_obj._ref); }
		/** Simplified access. */
		const T & operator*() const { return Object(); }
		/** Simplified access. */
		operator const T &() const { return Object(); }

	public: // constructor
		/** Constructor. */
		ConstProxy(const ThreadSafeObject & obj)
			: _obj(obj)
		{ LockTraits<LOCK>::AcquireShared(_obj._mtx); }
		/** Destructor. */
		~ConstProxy() { LockTraits<LOCK>::ReleaseShared(_obj._mtx); }
	};


	template<typename T, typename LOCK>
	class ThreadSafeObject<T, LOCK>::Proxy {
	private: // properties
		/** Reference to source object. */
		ThreadSafeObject<T, LOCK> & _obj;

	public: // methods
		/** Raw access. */
		T & Object() { return _obj._ref; }
		/** Simplified access. */
		T * operator->() const { return &(_obj._ref); }
		/** Simplified access. */
		T & operator*() { return Object(); }
		/** Simplified access. */
		operator T &() { return Object(); }

	public:
		/** Constructor. */
		Proxy(ThreadSafeObject & obj)
			: _obj(obj)
		{ LockTraits<LOCK>::AcquireExclusive(_obj._mtx); }
		/** Destructor. */
		~Proxy() { LockTraits<LOCK>::ReleaseExclusive(_obj._mtx); }
	};
}
