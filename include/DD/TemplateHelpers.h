#pragma once
namespace DD {
	/** Get number of arguments in compile time.*/
	template<typename...ARGS>
	constexpr size_t ArgumentCount();

	/** Get number of arguments in compile time.*/
	template<typename...ARGS>
	constexpr size_t ArgumentCountImplicit(const ARGS & ...) { return ArgumentCount<ARGS...>(); }

	/** Get argument of given index in compile time.  */
	template<size_t I, typename...ARGS>
	constexpr auto & Argument(ARGS &...args);

		/** Set indexable struct - Set(indexable, arg0, arg1, ...) ~  indexable[0] = arg0. indexable[1] = arg1 ...*/
	template<typename INDEXABLE, typename...ARGS>
	constexpr void Set(INDEXABLE & indexable, const ARGS &...args);

	/** Make endian flip on a n-byte struct. */
	template<typename T>
	__forceinline T Endian(const T & value);
}

#include <DD/Auxiliaries.h>

namespace DD {
	/************************************************************************/
	/* ArgumentCount                                                        */
	/************************************************************************/
	template<typename...ARGS>
	constexpr size_t ArgumentCount() { return Arguments<ARGS...>; }

	/************************************************************************/
	/* GetArgument                                                          */
	/************************************************************************/
	template<size_t I>
	struct GetArgumentHelper {
		template<typename ARG, typename...ARGS>
		static auto & Get(ARG & arg, ARGS &...args) { return GetArgumentHelper<I - 1>(args...); }
	};

	template<>
	struct GetArgumentHelper<0> {
		template<typename ARG, typename...ARGS>
		static auto & Get(ARG & arg, ARGS...) { return arg; }
	};

	template<size_t I, typename...ARGS>
	constexpr auto & Argument(ARGS &...args) { return GetArgumentHelper<I>::Get(args...); }


	/************************************************************************/
	/* Endian                                                               */
	/************************************************************************/
	template<typename T>
	union EndianConverter {
		constexpr static size_t LENGTH = sizeof(T);
		constexpr static size_t LAST = sizeof(T) - 1;
		constexpr static size_t HALF = LENGTH / 2;
		T Value; char Bytes[LENGTH];
		template<size_t INDEX = 0>
		inline void Invert() { Swap(Bytes[INDEX], Bytes[LAST - INDEX]); Invert<INDEX + 1>(); }
		template<>
		inline void Invert<HALF>() { }
		EndianConverter(const T & t) : Value(t) { Invert(); }
	};

	template<typename T>
	__forceinline T Endian(const T & value) { return EndianConverter<T>(value).Value; }
}