#pragma once
namespace DD {
	/**
	 * Traits for deallocation.
	 */
	template<typename T>
	struct UniqueTraits;

	/**
	 * Unique pointer.
	 */
	template<typename T, typename TRAITS = UniqueTraits<T>>
	class Unique;
}

#include <DD/Memory/NonCopyable.h>

namespace DD {
	template<typename T, typename TRAITS>
	class Unique
		:public Memory::NonCopyable
	{
	public: // typedefs
		/** Null pointer type. */
		typedef decltype(nullptr) nullptr_t;

	protected: // properties
		/** Pointer storage. */
		T * _ptr;

	public: // accession
		/** Forward dereferencing. */
		constexpr T * operator->() { return _ptr; }
		/** Const forward dereferencing. */
		constexpr const T * operator->() const { return _ptr; }
		/** Dereferencing. */
		constexpr T & operator*() { return *_ptr; }
		/** Const dereferencing. */
		constexpr const T & operator*() const { return *_ptr; }
		/** Conversion operator to native pointer type. */
		constexpr operator T*() { return _ptr; }
		/** Conversion operator to native pointer type. */
		constexpr operator const T*() const { return _ptr; }

	public: // methods
		/** Get address of pointer, note that this is unsafe method, prefer init instead. */
		constexpr T ** Address() { return &_ptr; }
		/** Release data if any, and return address to acquire new data. */
		constexpr T ** Init() { return _ptr ? Release().Address() : Address(); }
		/** Caster. */
		template<typename U = T>
		constexpr U * Ptr() { return (U *)_ptr; }
		constexpr T * Ptr() { return _ptr; }
		/** Const caster. */
		template<typename U = T>
		constexpr const U * Ptr() const { return (const U *)_ptr; }
		constexpr const T * Ptr() const { return _ptr; }
		/** Releaser. */
		constexpr Unique & Release();
		/** Set storage to null without releasing memory. */
		constexpr T * Unlock();

	public: // comparison
		/** Checks if nullptr is stored. */
		constexpr operator bool() const { return _ptr != nullptr; }
		/** Check if the pointer is null pointer. */
		constexpr bool operator==(nullptr_t) const { return _ptr == nullptr; }
		/** Check if the pointer is not null pointer. */
		constexpr bool operator!=(nullptr_t) const { return _ptr != nullptr; }
		/** Comparison operator. */
		constexpr bool operator==(const T * pointer) const { return _ptr == pointer; }
		/** Comparison operator. */
		constexpr bool operator!=(const T * pointer) const { return _ptr != pointer; }
		/** Comparison operator. */
		constexpr bool operator<=(const T * pointer) const { return _ptr <= pointer; }
		/** Comparison operator. */
		constexpr bool operator>=(const T * pointer) const { return _ptr >= pointer; }
		/** Comparison operator. */
		constexpr bool operator<(const T * pointer) const { return _ptr < pointer; }
		/** Comparison operator. */
		constexpr bool operator>(const T * pointer) const { return _ptr > pointer; }

	public: // assignment
		/** Move assignment. */
		constexpr Unique & operator=(Unique && rvalue);
		/** Release assignment. */
		constexpr Unique & operator=(nullptr_t) { return Release(); }
		/** Catch assignment. Releases actual storage. */
		constexpr Unique & operator=(T * ptr);

	public: // constructors
		/** Move constructor. */
		constexpr Unique(Unique && rvalue) : _ptr(rvalue._ptr) { rvalue._ptr = nullptr; }
		/** Pointer capturer. */
		constexpr Unique(T * ptr) : _ptr(ptr) {}
		/** Zero constructor. Stores nullptr. */
		constexpr Unique() : _ptr(nullptr) {}
		/** Destructor, releases memory. */
		~Unique() { Release(); }
	};

	template<typename T>
	struct UniqueTraits {
		/** Remove allocation. */
		static void Delete(T *& ptr) { delete ptr; }
	};
}

namespace DD {
	/************************************************************************/
	/* Methods                                                              */
	/************************************************************************/
	template<typename T, typename TRAITS>
	constexpr Unique<T, TRAITS> & Unique<T, TRAITS>::Release() {
		if (_ptr) {
			TRAITS::Delete(_ptr);
			_ptr = nullptr;
		}

		return *this;
	}

	template<typename T, typename TRAITS>
	constexpr T * Unique<T, TRAITS>::Unlock() {
		T * temp = _ptr;
		_ptr = nullptr;

		return temp;
	}

	/************************************************************************/
	/* Assignment                                                           */
	/************************************************************************/
	template<typename T, typename TRAITS>
	constexpr Unique<T, TRAITS> & Unique<T, TRAITS>::operator=(Unique && rvalue) {
		*this = rvalue._ptr;
		rvalue._ptr = nullptr;

		return *this;
	}

	template<typename T, typename TRAITS>
	constexpr Unique<T, TRAITS> & Unique<T, TRAITS>::operator=(T * ptr) {
		if (_ptr != ptr) {
			Release();
			_ptr = ptr;
		}

		return *this;
	}
}