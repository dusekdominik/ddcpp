#pragma once
namespace DD {
	/**
	 * Pattern for creating building.
	 * Building properties are defined by normalized Box.
	 * Each definition of building should be in the interval (-1, -1, -1) - (+1, +1, +1)
	 * Scale of building is parametrized by @see Building::Box::Radius
	 * @scheme normalized OBox
	 *          +------+------+
	 *         /|            /|
	 *        / |    D      / |  B = ( 0, +1,  0)
	 *       /  |       F  /  |  F = ( 0,  0, +1)
	 *      +-------------+   |  A = (-1,  0,  0)
	 *      | A |    O    | B |  O = ( 0,  0,  0)
	 *      |   +---------|---+  B = (+1,  0,  0)
	 *      |  /  E       |  /   E = ( 0, -1,  0)
	 *      | /      C    | /    C = ( 0, -1,  0)
	 *      |/            |/
	 *      +-------------+
	 * @scheme definition of wall and roof height
	 *             +---+------+ +1.0
	 * Roof.Height |   |  /\  |
	 *             |   | /__\ |----------+
	 *             +---|/|  |\|          | Wall.Height
	 *                 | +--+ |          |
	 *                 +------+-- -1.0 --+
	 */
	class Building;

	/**
	 * Collection of standard building models.
	 */
	class BuildingCollection;
}

#include <DD/Color.h>
#include <DD/Macros.h>
#include <DD/Polygon.h>
#include <DD/Random.h>
#include <DD/Simulation/PointCloud.h>
#include <DD/Simulation/TriangleModel.h>
#include <DD/XML.h>

/** Declaration of method for getting parent from given type. */
#define _UNIQUE_PLACEMENT(getterName, parentType, childType, childName, cst)\
static cst parentType & getterName(cst childType & address) {\
  return *(parentType*)(((const char *)&address) - &reinterpret_cast<char const volatile&>((((parentType*)0)->childName)));\
}
/**
 * Macro creating function for obtaining parent from uniquely placed child.
 * The type of child must be unique in the parent.
 */
#define UNIQUE_PLACEMNET(getterName, parentType, childType, childName) \
	/** Mutable parent getter. */\
	_UNIQUE_PLACEMENT(getterName, parentType, childType, childName, DD_EMPTY)\
	/** Const parent getter. */\
	_UNIQUE_PLACEMENT(getterName, parentType, childType, childName, const)

/** Helper for enumeration overloads of a templated function. */
#define _ENUM_OVERLOAD_X(index, count, globals, locals)\
	/** Function declaration - void fncName[0]<type[1]>(signature)[2]; */\
	template<>\
	void DD_UNWRAP_HEAD(0, globals)<DD_UNWRAP_HEAD(1, globals)::locals> DD_UNWRAP_HEAD(2, globals);

/**
 * Macro factory for creating all overload of a function templated
 * by an enumeration.
 */
#define _ENUM_OVERLOAD(name, enum, signature, ...)\
	/** Untemplated function (serves as default call - type automatically detected). */\
	void name signature;\
	/** Templated function - individual overloads, compile error for undefined call. */\
	template<enum E>\
	void name signature { static_assert(E != E, "Undefined overload."); }\
	/** Enumeration of individual overloads of above mentioned fnc. */\
	DD_XLIST_EX(_ENUM_OVERLOAD_X, (name, enum, signature), __VA_ARGS__)

namespace DD {
	class Building {
	public: // nested
		/************************************************************************/
		/* Declaration of wall properties                                       */
		/************************************************************************/
		struct Walls {
			friend class Building;
		public: // nested
			/** Type of wall. */
#define DD_BUILDING_WALL_TYPES Undefined, Basic
			DD_ENUM_32(Types, DD_BUILDING_WALL_TYPES);
			/** Texture of wall. */
			DD_ENUM_32(Textures, Basic);
			/** Dummy params. */
			struct UndefinedParams { DD_XML_SERIALIZATION(); };
			/** Params for Type = Types::Basic. */
			struct BasicParams {
				/** Number of floors, could serve for valid texturing. */
				u32 Floors;
				/** Color of building (should be replaced somehow by textures). */
				DD::Color Color;
				/** Descriptor of XML serialization. */
				DD_XML_SERIALIZATION(
					ELEMENT(Floors),
					ELEMENT(Color.AggU32, Color)
				);
			};
			/** Parameters of wall by types. */
			struct Params {
				/** Type of wall. */
				Walls::Types Type = Walls::Types::Undefined;
				/** Parameters for wall of Basic type. */
				union { BasicParams Basic; UndefinedParams Undefined; };
				/** Special serialization for union. */
				void FromXml(const XML::Element & source);
				/** Special serialization for union. */
				void ToXml(XML::Element & target) const;
				/** Zero constructor. */
				Params() {}
				/** Copy constructor. */
				Params(const Params & lvalue)
					: Basic(lvalue.Basic)
					, Type(lvalue.Type)
				{}
			};
		public: // properties
			/** Parameters of wall. */
			Walls::Params Params;
			/** Texture type of wall. */
			Walls::Textures Texture;
			/** Height of the wall. */
			f32 Height;
		private: // methods
			/** Getter of parent building. */
			Building & Parent() { return BuildingFromWall(*this); }
			/** Getter of parent building. */
			const Building & Parent() const { return BuildingFromWall(*this); }
		public: // methods
			/** Generate point cloud from wall properties. */
			_ENUM_OVERLOAD(GeneratePointCloud, Types::Values, (Simulation::PointCloud & target, size_t count, Random::Generator & generator) const, DD_BUILDING_WALL_TYPES);
			/** Generate triangle model from wall properties. */
			_ENUM_OVERLOAD(GenerateTriangleModel, Types::Values, (Simulation::TriangleModel<Simulation::ColorVertex> & model) const, DD_BUILDING_WALL_TYPES);
		private: // constructors
			/** Private constructor. */
			Walls() = default;
			/** */
			Walls(const Walls &) = default;
			/** Descriptor of XML serialization. */
			DD_XML_SERIALIZATION(
				ELEMENT(Texture),
				ELEMENT(Height),
				ELEMENT(Params)
			);
		};

		/************************************************************************/
		/*  Declaration of roof properties.                                     */
		/************************************************************************/
		struct Roofs {
			friend class Building;
		public: // nested
			/** Type of roof. */
#define DD_BUILDING_ROOF_TYPES Undefined, Flat, Gabled, Pyramidal, Dome
			DD_ENUM_32(Types, DD_BUILDING_ROOF_TYPES);
			/** Dummy params. */
			struct UndefinedParams { DD_XML_SERIALIZATION(); };
			/** Parameters for roof of flat type. */
			struct FlatParams {
				/** Color of the roof. */
				DD::Color Color;
				/** Descriptor of XML serialization. */
				DD_XML_SERIALIZATION(
					ELEMENT(Color.AggU32, Color)
				);
			};
			/** Parameters for roof of gabled type. */
			struct GabledParams {
				/** Center point of gabled. */
				float2 Center;
				/** Main roof color. */
				DD::Color MainColor;
				/** Color of side triangles */
				DD::Color SideColor;
				/** Descriptor of XML serialization. */
				DD_XML_SERIALIZATION(
					ELEMENT(Center.x, CenterX),
					ELEMENT(Center.y, CenterY),
					ELEMENT(MainColor.AggU32, MainColor),
					ELEMENT(SideColor.AggU32, SideColor)
				);
			};
			/** Parameters for roof of pyramidal type. */
			struct PyramidalParams {
				/** Center point of pyramid. */
				float2 Center;
				/** Main roof color. */
				DD::Color Color;
				/** Descriptor of XML serialization. */
				DD_XML_SERIALIZATION(
					ELEMENT(Center.x, CenterX),
					ELEMENT(Center.y, CenterY),
					ELEMENT(Color.AggU32, Color)
				);
			};
			/** Parameters for roof of dome type. */
			struct DomeParams {
				DomeParams & operator=(const DomeParams &) = default;
				/** Center point of dome. */
				float2 Center;
				/** Radius of the dome. */
				f32 Radius;
				/** Main roof color. */
				DD::Color DomeColor;
				/** Color of roof between profile and dome. */
				DD::Color EdgeColor;
				/** Descriptor of XML serialization. */
				DD_XML_SERIALIZATION(
					ELEMENT(Radius),
					ELEMENT(Center.x, CenterX),
					ELEMENT(Center.y, CenterY),
					ELEMENT(DomeColor.AggU32, DomeColor),
					ELEMENT(EdgeColor.AggU32, EdgeColor)
				);
			};

			/** Parameters of roof by a type. */
			struct Params {
				/** Type of roof. */
				Roofs::Types Type = Roofs::Types::Undefined;
				/** Type specific params. */
				union {
					UndefinedParams Undefined;
					FlatParams Flat;
					DomeParams Dome;
					PyramidalParams Pyramidal;
					GabledParams Gabled;
				};
				/** Special serialization for union. */
				void ToXml(XML::Element & target) const;
				/** Special serialization for union. */
				void FromXml(const XML::Element & source);
				/** Zero constructor. */
				Params() {};
				/** Copy constructor. */
				Params(const Params & lvalue)
					: Type(lvalue.Type)
					, Gabled(lvalue.Gabled)
				{}
			};
		public: // properties
			/** Params of roof. */
			Roofs::Params Params;
			/**
			 * Height of the roof.
			 * Value should be in (0.0 - 2.0)
			 */
			f32 Height;
		private: // methods
				 /** Getter of parent building. */
			Building & Parent() { return BuildingFromRoof(*this); }
			/** Getter of parent building. */
			const Building & Parent() const { return BuildingFromRoof(*this); }
		public: // methods
			/** Generate point cloud from roof properties. */
			_ENUM_OVERLOAD(
				GeneratePointCloud,
				Types::Values,
				(Simulation::PointCloud & target, size_t count, Random::Generator & generator) const,
				DD_BUILDING_ROOF_TYPES
			);
			/** Generate triangle model from roof properties. */
			_ENUM_OVERLOAD(
				GenerateTriangleModel,
				Types::Values,
				(Simulation::TriangleModel<Simulation::ColorVertex> & model) const,
				DD_BUILDING_ROOF_TYPES
			);
		private: // constructors
			/** Private constructor. */
			Roofs() = default;
			/** Private copy constructor. */
			Roofs(const Roofs &) = default;
			/** Descriptor of XML serialization. */
			DD_XML_SERIALIZATION(
				ELEMENT(Height),
				ELEMENT(Params)
			);
		};

		/************************************************************************/
		/* Building declaration                                                 */
		/************************************************************************/
	protected: // statics
		/** Derives building ptr from wall ptr. */
		UNIQUE_PLACEMNET(BuildingFromWall, Building, Walls, Wall);
		/** Derives building ptr from roof ptr. */
		UNIQUE_PLACEMNET(BuildingFromRoof, Building, Roofs, Roof);

	public: // statics
		/** Collection with patterns of common buildings. */
		static BuildingCollection Collection;

	public: // properties
		/** Wall of building. */
		Walls Wall;
		/** Roof of building. */
		Roofs Roof;
		/** Profile of building. */
		PolygonF Profile;
		/** Positioning and size of building. */
		OBox3F Box;

	public: // methods
		/** Check validity of descriptor. */
		bool IsValid() const { return Roof.Params.Type != Roofs::Types::Undefined && Wall.Params.Type != Walls::Types::Undefined; }
		/** Generate point cloud of the building. */
		Simulation::PointCloud GeneratePointCloud(
			size_t wall,
			size_t roof,
			Random::Generator & generator = Random::Generator::Instance()
		) const;
		/** Generate point cloud of the building. */
		void GeneratePointCloud(
			Simulation::PointCloud & target,
			size_t wall,
			size_t roof,
			Random::Generator & generator = Random::Generator::Instance()
		) const;
		/** Generate triangle model of the building. */
		void GenerateTriangleModel(Simulation::TriangleModel<Simulation::ColorVertex> & model) const;

	public: // constructors
		/** Copy assignment. */
		Building & operator=(const Building &) = default;
		/** Move assignment. */
		Building & operator=(Building &&) = default;
		/** Zero constructor. */
		Building() = default;
		/** Move constructor. */
		Building(Building &&) = default;
		/** Copy assignment. */
		Building(const Building &) = default;
		/** Descriptor of XML serialization. */
		DD_XML_SERIALIZATION(
			ELEMENT(Box.Position.x, PositionX),
			ELEMENT(Box.Position.y, PositionY),
			ELEMENT(Box.Position.z, PositionZ),
			ELEMENT(Box.Radius.x, RadiusX),
			ELEMENT(Box.Radius.y, RadiusY),
			ELEMENT(Box.Radius.z, RadiusZ),
			ELEMENT(Box.Rotation[0][0], Rotation00),
			ELEMENT(Box.Rotation[0][1], Rotation01),
			ELEMENT(Box.Rotation[0][2], Rotation02),
			ELEMENT(Box.Rotation[1][0], Rotation10),
			ELEMENT(Box.Rotation[1][1], Rotation11),
			ELEMENT(Box.Rotation[1][2], Rotation12),
			ELEMENT(Box.Rotation[2][0], Rotation20),
			ELEMENT(Box.Rotation[2][1], Rotation21),
			ELEMENT(Box.Rotation[2][2], Rotation22),
			ELEMENT(Profile.Points, Profile),
			ELEMENT(Wall),
			ELEMENT(Roof)
		);
	};

	class BuildingCollection {
	public: // properties
		union {
			Building Array[4];
			union {
				/** Building with square-shaped base. */
				Building Squares[4];
				struct { Building SquareGabled, SquarePyramidal, SquareFlat, SquareDome; };
			};
		};
	public: // constructors
		/** Indexer. */
		template<typename INDEX_TYPE>
		const Building & operator[](INDEX_TYPE index) const { return Array[index]; }
		/** Zero constructor. */
		BuildingCollection();
		/** Destructor.*/
		~BuildingCollection() {}
	};
}

#undef _ENUM_OVERLOAD_X
#undef _ENUM_OVERLOAD
#undef _UNIQUE_PLACEMENT
#undef UNIQUE_PLACEMNET
