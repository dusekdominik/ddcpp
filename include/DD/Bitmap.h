#pragma once
namespace DD {
	/** Structure for basic manipulation with bmp files. */
	struct Bitmap;
}

#include <DD/Array.h>
#include <DD/Color.h>
#include <DD/Debug.h>
#include <DD/FileSystem/Path.h>
#include <DD/Math/Gauss.h>
#include <DD/Math/Interval.h>
#include <DD/Types.h>

namespace DD {
	struct Bitmap {
		/** Signature of windows bitmap. */
		static constexpr const u16 SIGNATURE_WINDOWS = 'MB';
		/** Indexing type. */
		using SizeType = u32;

#pragma pack(push, 1)
		/** Platform unspecific header. */
		struct UnscentHeader {
			/** File signature. */
			/*00*/u16 Signature = SIGNATURE_WINDOWS;
			/*02*/SizeType ByteSize;
			/*06*/u32 Reserved;
			/*10*/SizeType Offset;
		};

		/** Platform specific header. */
		struct WinHeader : UnscentHeader {
			/*14*/SizeType HeaderByteSize = 40;
			/*18*/SizeType Width;
			/*22*/SizeType Height;
			/*26*/u16 Planes = 1;
			/*28*/u16 BitsPerPixel;
			/*30*/u32 Compression = 0;
			/*34*/SizeType BmpSize;
			/*38*/u32 HResolution = 2835;
			/*42*/u32 VResolution = 2835;
			/*46*/u32 Palette = 0;
			/*50*/u32 ImportantColors = 0;
		};

		/** Common 24bit BGR pixel. */
		struct Pixel24 { u8 Blue, Green, Red; };

		/** Common 8bit pixel. */
		struct Pixel8 { u8 Index; };

		/** Calculator for rescaling images. */
		struct PixelCoverage {
			/** Inverted dimensions. */
			f64 _iw, _ih;
			/** Get coverage. */
			Math::Interval2D operator()(size_t x, size_t y) const { return { {x * _iw, y * _ih}, { (x + 1) * _iw, (y + 1) * _ih} }; }
			/** Constructor. */
			PixelCoverage(size_t w, size_t h) : _iw(1.0 / w), _ih(1.0 / h) {}
		 };
#pragma pack(pop)

	public: // properties
		/** All data are stored in this array. */
		Array<i8> _data;

	public: // header connected methods
		/** Get properly casted header. */
		WinHeader * Header() { return (WinHeader *)_data.begin(); }
		/** Get properly casted header. */
		const WinHeader * Header() const { return (const WinHeader *)_data.begin(); }
		/** Number of pixels in a picture row. */
		SizeType Width() const { return Header()->Width; }
		/** Number of pixels in a picture col. */
		SizeType Height() const { return Header()->Height; }
		/** Offset (in bytes) to pixel part of the picture. */
		SizeType Offset() const { return Header()->Offset; }
		/** Number of bits per pixel. */
		SizeType BitsPerPx() const { return Header()->BitsPerPixel; }
		/** Number of bytes per pixel. */
		SizeType BytesPerPx() const { return BitsPerPx() / 8; }
		/** Get byte padding of a picture row. */
		SizeType Padding() const { return (4 - (BytesPerPx() * Width()) % 4) % 4; }
		/** Number of bytes per pixel row. */
		SizeType Stride() const { return Width() * BytesPerPx() + Padding(); }

	public: // pixel oriented ops
		/** Get pointer to pixel data of the given coordinates. */
		void * PixelAddress(SizeType col, SizeType row);
		/** Get pointer to pixel data of the given coordinates. */
		const void * PixelAddress(SizeType col, SizeType row) const;
		/** Get data formatted as given template arg.  */
		template<typename PIXEL>
		PIXEL & Pixel(SizeType col, SizeType row);
		/** Get data formatted as given template arg.  */
		template<typename PIXEL>
		const PIXEL & Pixel(SizeType col, SizeType row) const;
		/** Fill bitmap-area by given value. */
		template<typename PIXEL>
		Bitmap & Fill(SizeType bx, SizeType ex, SizeType by, SizeType ey, PIXEL value);

	public: // palette specific
		/** Get n-th (0...255) pixel from palette. */
		Pixel24 & PalettePixel(u8 index);
		/** Return color (from palette) for the given  */
		Pixel24 & PalettePixel(SizeType col, SizeType row) { return PalettePixel(Pixel<Pixel8>(col, row).Index); }
		/** Set palette value at given index. */
		Bitmap & PalettePixel(u8 index, const Pixel24 & value) { PalettePixel(index) = value; return *this; }
		/** Get index to palette at given coords. */
		const u8 PixelAsPaletteIndex(SizeType col, SizeType row) const { return Pixel<Pixel8>(col, row).Index; }
		/** Compute palette histogram. */
		const Array<SizeType> PaletteHistorgram() const;
		/** Get maximum pixel value from given area. */
		u8 Max(SizeType bx, SizeType ex, SizeType by, SizeType ey) const { return Aggregate(bx, ex, by, ey, &Math::Max); }
		u8 Max() { return Max(0, Width(), 0, Height()); }
		/** Get minimum pixel value from given area. */
		u8 Min(SizeType bx, SizeType ex, SizeType by, SizeType ey) const { return Aggregate(bx, ex, by, ey, &Math::Min); }
		u8 Min() { return Min(0, Width(), 0, Height()); }
		/** Method for data-aggregation via a given method. */
		u8 Aggregate(SizeType bx, SizeType ex, SizeType by, SizeType ey, u8(*afnc)(const u8 &, const u8 &)) const;

	public: // bitmap reprojection
		/** Do convolution with given kernel. */
		Bitmap Convolution(f64 * kernel, SizeType w, SizeType h) const;
		/** Cut the edges if the bitmap. */
		Bitmap Crop(SizeType lc, SizeType tc, SizeType rc, SizeType bc) const;
		/** Copy current bitmap to a bitmap with the given resolution */
		Bitmap Rescale(SizeType width, SizeType height) const;
		/** Smooth the surface, cnc milling purposes. */
		Bitmap Smooth(SizeType ex, SizeType ey) const;
		/** Smooth the surface with Gaussian kernel. */
		Bitmap SmoothGauss(i32 ex, f64 sigma);
		/** Do maximal pooling on bitmap partitions. */
		Bitmap Squaring(SizeType xP = 4, SizeType yP = 4) const;
		/** Convert bitmap to the 8bit grayscale format. */
		Bitmap To8Bit() const;

	public: // file handling methods
		/** Data exists and it has valid signature. */
		bool IsValid() const { return _data.Ptr() && Header()->Signature == SIGNATURE_WINDOWS; }
		/** Create new bitmap with given params. */
		void Create(SizeType width, SizeType height, u16 bpp);
		/** Load file from the given path. */
		bool Load(const FileSystem::Path & bmpFile);
		/** Save file to the given path. */
		bool Save(const FileSystem::Path & path);

	public: // constructors
		/** Default constructor. */
		Bitmap() = default;
		/** Load file to the bitmap. */
		Bitmap(const FileSystem::Path & path) { Load(path); }
		/** Create new bitmap according. */
		Bitmap(SizeType width, SizeType height, u16 bpp) { Create(width, height, bpp); }
	};
}

namespace DD {
	inline typename Bitmap::Pixel24 & Bitmap::PalettePixel(u8 index) {
		// get offset of the data
		static constexpr size_t paletteOffset = sizeof(WinHeader);
		size_t pxOffset = paletteOffset + (index * 4);

		// safety checks
		DD_DEBUG_ASSERT(BitsPerPx() == 8, "Palette is implemented only in 8-bit format.");
		DD_DEBUG_ASSERT(pxOffset < Offset(), "The requested offset is behind the palette.");

		// return data
		return *((Pixel24 *)(_data.begin() + pxOffset));
	}

	inline void * Bitmap::PixelAddress(SizeType col, SizeType row) {
		// safety checks
		DD_DEBUG_ASSERT(BitsPerPx() % 8 == 0, "Data must be byte aligned.");
		DD_DEBUG_ASSERT(col < Width(), "Out of range.");
		DD_DEBUG_ASSERT(row < Height(), "Out of range.");

		// beginning byte of pixel data
		size_t byteIndex = Offset() + Stride() * row + col * BytesPerPx();
		return (void *)(_data.begin() + byteIndex);
	}

	inline const void * Bitmap::PixelAddress(SizeType col, SizeType row) const {
		// safety checks
		DD_DEBUG_ASSERT(BitsPerPx() % 8 == 0, "Data must be byte aligned.");
		DD_DEBUG_ASSERT(col < Width(), "Out of range.");
		DD_DEBUG_ASSERT(row < Height(), "Out of range.");

		// beginning byte of pixel data
		size_t byteIndex = Offset() + Stride() * row + col * BytesPerPx();
		return (const void *)(_data.begin() + byteIndex);
	}

	inline const Array<typename Bitmap::SizeType> Bitmap::PaletteHistorgram() const {
		Array<SizeType> histogram = Array<SizeType>::Uniform(256, (SizeType)0);
		for (SizeType row = 0, height = Height(); row < height; ++row) {
			for (SizeType col = 0, width = Width(); col < width; ++col) {
				++histogram[PixelAsPaletteIndex(col, row)];
			}
		}

		return histogram;
	}

	inline void Bitmap::Create(SizeType width, SizeType height, u16 bpp) {
		// precomputed values
		SizeType paletteSize = bpp == 8 ? 256 * 4 : 0;
		SizeType bmpSize = width * height;
		SizeType padding = (4 - ((bpp * width) / 8) % 4) % 4;
		SizeType stride = padding + width;
		SizeType totalSize = (SizeType)sizeof(WinHeader) + stride * height * bpp / 8 + paletteSize;

		// initialize data
		_data.Reallocate(totalSize).FillUniform(0);

		// fill header
		*Header() = WinHeader();
		Header()->Offset = sizeof(WinHeader) + paletteSize;
		Header()->HeaderByteSize = sizeof(WinHeader) - sizeof(UnscentHeader);
		Header()->Width = width;
		Header()->Height = height;
		Header()->BitsPerPixel = bpp;
		Header()->BmpSize = bmpSize;
		Header()->ByteSize = totalSize;

		// fill palette if needed
		if (paletteSize) {
			for (u8 i = 0; ++i;) {
				PalettePixel(i) = { (u8)i,(u8)i,(u8)i };
			}
		}
	}


	inline bool Bitmap::Load(const FileSystem::Path & bmpFile) {
		// read file if exists
		if (bmpFile.Exists() && bmpFile.IsFile()) {
			_data = bmpFile.AsFile().Read();
			// if file is not supported report it as fail
			if (Header()->Signature == SIGNATURE_WINDOWS)
				return true;
		}

		return false;
	}


	template<typename PIXEL>
	PIXEL & Bitmap::Pixel(SizeType col, SizeType row) {
		// safety checks
		DD_DEBUG_ASSERT(BytesPerPx() == sizeof(PIXEL));

		return *((PIXEL *)PixelAddress(col, row));
	}


	template<typename PIXEL>
	const PIXEL & Bitmap::Pixel(SizeType col, SizeType row) const {
		// safety checks
		DD_DEBUG_ASSERT(BytesPerPx() == sizeof(PIXEL));

		return *((const PIXEL *)PixelAddress(col, row));
	}


	inline bool DD::Bitmap::Save(const FileSystem::Path & path) {
		if (!path.Exists() || path.IsFile()) {
			path.AsFile().Write(ConstByteView((const char *)_data.begin(), _data.Length()));
			return true;
		}

		return false;
	}


	inline Bitmap Bitmap::To8Bit() const {
		// safety checks
		DD_ASSERT(BitsPerPx() == 24, "Not implemented.");

		Bitmap target;
		SizeType height = Height();
		SizeType width = Width();
		target.Create(width, height, 8);
		for (SizeType row = 0; row < height; ++row) {
			for (SizeType col = 0; col < width; ++col) {
				const Pixel24 & px = Pixel<Pixel24>(col, row);
				target.Pixel<u8>(col, row) = (u8)(DD::Color(px.Red, px.Green, px.Blue).Luminance() * 255.f);
			}
		}

		return target;
	}


	inline Bitmap Bitmap::Crop(SizeType lc, SizeType tc, SizeType rc, SizeType bc) const {
		DD_ASSERT(BitsPerPx() == 8, "Not implemented.");
			SizeType width = Width() - (lc + rc);
		SizeType height = Height() - (tc + bc);

		Bitmap target(width, height, 8);
		for (SizeType y = 0; y < height; ++y) {
			for (SizeType x = 0; x < width; ++x) {
				target.Pixel<u8>(x, y) = Pixel<u8>(x + lc, y + tc);
			}
		}

		return target;
	}


	inline Bitmap Bitmap::Rescale(SizeType width, SizeType height) const {
		// safety checks
		DD_ASSERT(BitsPerPx() == 8, "Not implemented.");

 		Bitmap target;
 		target.Create(width, height, BitsPerPx());

		// coverage calculators
		PixelCoverage srcCoverage(Width(), Height());
		PixelCoverage tgtCoverage(width, height);
		// target data (we need floats for accurate accumulation)
		Array2D<f64> accValue = Array2D<f64>::Uniform({ width, height }, 0.0);

		// create accumulated table data
		for (SizeType row = 0; row < Height(); ++row) {
			for (SizeType col = 0; col < Width(); ++col) {
				// pixel we are trying to project
				Math::Interval2D srcCov = srcCoverage(col, row);
				// coordinates of projection of the left top corner
				SizeType x = (SizeType)(srcCov.Min.x * width);
				SizeType y = (SizeType)(srcCov.Min.y * height);
				u8 px = Pixel<u8>(col, row);
				// project main data
				accValue[{y, x}] += tgtCoverage(x, y).Ratio(srcCov) * px;
				// project potential overlaps
				if (x + 1 < width)
					accValue[{y, x + 1}] += tgtCoverage(x + 1, y).Ratio(srcCov) * px;
				if (y + 1 < height)
					accValue[{y + 1, x}] += tgtCoverage(x, y + 1).Ratio(srcCov) * px;
				if (x + 1 < width && y + 1 < height)
					accValue[{y + 1, x + 1}] += tgtCoverage(x + 1, y + 1).Ratio(srcCov) * px;
			}
		}

		// copy accumulated data back to the picture
		for (SizeType row = 0; row < height; ++row) {
			for (SizeType col = 0; col < width; ++col) {
				target.Pixel<u8>(col, row) = (u8)accValue[{row, col}];
			}
		}

		return target;
	}


	inline Bitmap Bitmap::Convolution(f64 * kernel, SizeType w, SizeType h) const {
		DD_ASSERT(BitsPerPx() == 8, "Not implemented.");

		// prepare values
		Bitmap target = *this;
		SizeType height = Height();
		SizeType width = Width();

		// compute excentricity
		SizeType ex = w / 2;
		SizeType ey = h / 2;

		// go through pixel
		for (SizeType row = ey, rowl = height - ey - 1; row < rowl; ++row) {
			for (SizeType col = ex, coll = width - ex - 1; col < coll; ++col) {
				f64 acc = 0.0;
				// go through kernel
				SizeType i = 0;
				for (SizeType y = row - ey, yl = row + ey; y <= yl; ++y) {
					for (SizeType x = col - ex, xl = col + ex; x <= xl; ++x) {
						acc += kernel[i++] * Pixel<u8>(x, y);
					}
				}
				// normalize
				target.Pixel<u8>(col, row) = (u8)DD::Math::Saturate(acc, 0.0, 255.0);
			}
		}

		return target;
	}


	inline Bitmap Bitmap::Smooth(SizeType ex, SizeType ey) const {
		// safety checks
		DD_ASSERT(BitsPerPx() == 8, "Not implemented.");

		// copy current data
		Bitmap target = *this;
		SizeType height = Height();
		SizeType width = Width();
		f64 kernelAvg = 1.0 / ((ex * 2 + 1) * (ey * 2 + 1));

		// go through pixel
		for (SizeType row = ey, rowl = height - ey- 1; row < rowl; ++row) {
			for (SizeType col = ex, coll = width - ex - 1; col < coll; ++col) {
				// accumulate color
				f64 acc = 0.0;
				// make avg from kernel value
				for (SizeType y = row - ey, yl = row + ey; y <= yl; ++y) {
					for (SizeType x = col - ex, xl = col + ex; x <= xl; ++x) {
						acc += kernelAvg * Pixel<u8>(x, y);
					}
				}
				// normalize
				target.Pixel<u8>(col, row) = (u8)DD::Math::Saturate(acc, 0.0, 255.0);
			}
		}

		return target;
	}


	inline Bitmap Bitmap::SmoothGauss(i32 ex, f64 sigma) {
		SizeType kernelWidth = 1 + ex + ex;
		SizeType kernelHeight = kernelWidth;
		Array<f64> gaussKernel(kernelWidth * kernelHeight);
		DD::Math::GaussD g(0.0, 0.0, sigma, sigma, 0.0);
		DD::f64 * k = gaussKernel.begin();
		for (i32 i = -ex; i <= ex; ++i)
			for (i32 j = -ex; j <= ex; ++j)
				*k++ = g(i, j);

		// normalize kernel
		DD::f64 kenelSum = 0.0;
		for (DD::f64 i : gaussKernel)
			kenelSum += i;

		for (DD::f64 & i : gaussKernel)
			i /= kenelSum;


		return Convolution(gaussKernel.begin(), kernelWidth, kernelHeight);

	}


	inline u8 Bitmap::Aggregate(
		SizeType bx,
		SizeType ex,
		SizeType by,
		SizeType ey,
		u8(*afnc)(const u8 &, const u8 &)
	) const {
		DD_ASSERT(BitsPerPx() == 8, "Not implemented.");
		// init value
		u8 value = Pixel<u8>(0, 0);
		// look for maximum
		for (SizeType y = by; y < ey; ++y) {
			for (SizeType x = bx; x < ex; ++x) {
				u8 px = Pixel<u8>(x, y);
				value = afnc(value, px);
			}
		}

		return value;
	}


	template<typename PIXEL>
	inline Bitmap & Bitmap::Fill(SizeType bx, SizeType ex, SizeType by, SizeType ey, PIXEL value) {
		DD_ASSERT(BitsPerPx() == 8, "Not implemented.");

		for (SizeType y = by; y < ey; ++y) {
			for (SizeType x = bx; x < ex; ++x) {
				Pixel<PIXEL>(x, y) = value;
			}
		}

		return *this;
	}


	inline Bitmap Bitmap::Squaring(SizeType xP /* = 4 */, SizeType yP /* = 4 */) const {
		DD_ASSERT(BitsPerPx() == 8, "Not implemented.");

		Bitmap target = *this;
		for (SizeType x = 0; x < xP; ++x) {
			for (SizeType y = 0; y < yP; ++y) {
				SizeType bx = x * Width() / xP;
				SizeType ex = (x + 1) * Width() / xP;
				SizeType by = y * Height() / yP;
				SizeType ey = (y + 1) * Height() / yP;
				u8 max = Max(bx, ex, by, ey);
				target.Fill(bx, ex, by, ey, max);
			}
		}
		return target;
	}
}
