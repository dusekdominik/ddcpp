#pragma once
namespace DD {
	/**
	 * Defines the icon, which is texture with specified dimensions.
	 */
	struct Icon;
}

#include "Vector.h"
#include "String.h"

#define ICON(name, w, h)  DD::Icon{ { w, h }, DD::Text::StringView16(L"Textures/" #name ".dds") }
#define ICON_GARFIELD ICON(Grafield, 100.f, 50.f)
#define ICON_BIOME	  ICON(Biome, 50.f, 50.f)
#define ICON_BUILDING ICON(Building, 50.f, 50.f)
#define ICON_QUESTION ICON(QuestionMark, 50.f, 50.f)

namespace DD {
	struct Icon {
		/** Dimensions in projection. */
		float2 Dimensions;
		/** Name is equal to path. */
		String Path;
	};
}