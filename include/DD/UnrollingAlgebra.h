#pragma once
#define __INLINE __forceinline
#define __CONSTEXPR constexpr
/** Vector-Vector-Vector. */
#define  __DEFINE_VVV(name, ofnc, cast)\
template<size_t I = 0> __INLINE static __CONSTEXPR void name(LIN l, RIN r, UA_OUT result) { ofnc(l[I], r[I], result[I], cast); name<I + 1>(l, r, result); }\
template<> __INLINE static __CONSTEXPR void name<LIMIT>(LIN, RIN, UA_OUT) {}
/** Vector-Vector. */
#define __DEFINE_VV(name, ifnc, cast)\
template<size_t I = 0> __INLINE static __CONSTEXPR void name(UA_INOUT l, UA_IN r) { ifnc(l[I + LEFT_OFFSET], r[I + RIGHT_OFFSET], cast); name<I + 1>(l, r); }\
template<> __INLINE static __CONSTEXPR void name<LIMIT>(UA_INOUT, UA_IN) {}
/** Vector-Scalar-Vector. */
#define __DEFINE_VSV(name, ofnc, cast)\
template<size_t I = 0> __INLINE static __CONSTEXPR void name(VIN l, SIN r, UA_OUT result) { ofnc(l[I], r, result[I], cast); name<I + 1>(l, r, result); }\
template<> __INLINE static __CONSTEXPR void name<LIMIT>(VIN, SIN, UA_OUT) {}
/** Vector-Scalar. */
#define __DEFINE_VS(name, ifnc, cast)\
template<size_t I = 0> __INLINE static __CONSTEXPR void name(UA_INOUT l, SIN r) { ifnc(l[I], r, cast); name<I + 1>(l, r); }\
template<> __INLINE static __CONSTEXPR void name<LIMIT>(UA_INOUT, SIN) {}
/** Vector-Vector-Scalar. */
#define __DEFINE_VVS(name, op0, op1)\
template<size_t I = 0> __INLINE static __CONSTEXPR void name(LIN l, RIN r, T & result) { result op0 l[I] op1 r[I]; name<I + 1>(l, r, result); }\
template<> __INLINE static __CONSTEXPR void name<LIMIT>(LIN, RIN, T &) {}
/** Vector-Vector-Boolean. */
#define __DEFINE_VVB(name, op0, op1, neutral)\
template<size_t I = 0> __INLINE static __CONSTEXPR bool name(LIN l, RIN r) { return (l[I] op0 r[I]) op1 name<I + 1>(l, r); }\
template<> __INLINE static __CONSTEXPR bool name<LIMIT>(LIN, RIN) { return neutral; }
#define __DEFINE_VSB(name, op0, op1, neutral)\
template<size_t I = 0> __INLINE static __CONSTEXPR bool name(LIN l, RIN r) { return (l[I] op0 r) op1 name<I + 1>(l, r); }\
template<> __INLINE static __CONSTEXPR bool name<LIMIT>(LIN, RIN) { return neutral; }
/** Out-place functions. */
#define __OF_ADD(l, r, result, cast) result = cast(l + r);
#define __OF_SUB(l, r, result, cast) result = cast(l - r);
#define __OF_MUL(l, r, result, cast) result = cast(l * r);
#define __OF_DIV(l, r, result, cast) result = cast(l / r);
#define __OF_MAX(l, r, result, cast) result = cast(l >= r ? l : r);
#define __OF_MIN(l, r, result, cast) result = cast(l <= r ? l : r);
#define __OF_SAT(min, max, res, cast) if (res < cast(min)) res = cast(min); else if (res > cast(max)) res = cast(max);
/** In-place functions. */
#define __IF_ABS(l, r, cast) l = r < 0 ? cast(-r) : cast(r);
#define __IF_ADD(l, r, cast) l += cast(r);
#define __IF_SUB(l, r, cast) l -= cast(r);
#define __IF_MUL(l, r, cast) l *= cast(r);
#define __IF_DIV(l, r, cast) l /= cast(r);
#define __IF_SET(l, r, cast) l = cast(r);
#define __IF_NEG(l, r, cast) l = -cast(r);
#define __IF_MIN(l, r, cast) if (l > cast(r)) l = cast(r);
#define __IF_MAX(l, r, cast) if (l < cast(r)) l = cast(r);
/** Out-place collection. */
#define __OUT_COLLECTION(def, cast)\
	def(add, __OF_ADD, cast)\
	def(sub, __OF_SUB, cast)\
	def(mul, __OF_MUL, cast)\
	def(div, __OF_DIV, cast)\
	def(max, __OF_MAX, cast)\
	def(min, __OF_MIN, cast)\
	def(sat, __OF_SAT, cast)
/** In-place collection. */
#define __IN_COLLECTION(def, cast)\
	def(abs, __IF_ABS, cast)\
	def(add, __IF_ADD, cast)\
	def(sub, __IF_SUB, cast)\
	def(mul, __IF_MUL, cast)\
	def(div, __IF_DIV, cast)\
	def(set, __IF_SET, cast)\
	def(neg, __IF_NEG, cast)\
	def(min, __IF_MIN, cast)\
	def(max, __IF_MAX, cast)
/** Casters.*/
#define __STATIC_T_CAST(x) static_cast<T>(x)
#define __NO_CAST(x) x

namespace DD {
	/**
	 * UnrollingAlgebra
	 * Class serves as basic unrolled algebra for index based vector operations.
	 */
	template<size_t LIMIT, size_t LEFT_OFFSET = 0, size_t RIGHT_OFFSET = 0, size_t RESULT_OFFSET = 0>
	class UnrollingAlgebra {
		/** Vector-Vector-Vector helper class. */
		template<typename LIN, typename RIN, typename UA_OUT>
		struct VVV { __OUT_COLLECTION(__DEFINE_VVV, __NO_CAST) };
		/** Vector-Vector-Vector-Cast helper class. */
		template<typename LIN, typename RIN, typename UA_OUT, typename T>
		struct VVVC { __OUT_COLLECTION(__DEFINE_VVV, __STATIC_T_CAST) };
		/** Vector-Vector helper class. */
		template<typename UA_INOUT, typename UA_IN>
		struct VV { __IN_COLLECTION(__DEFINE_VV, __NO_CAST) };
		/** Vector-Vector-Cast helper class. */
		template<typename UA_INOUT, typename UA_IN, typename T>
		struct VVC { __IN_COLLECTION(__DEFINE_VV, __STATIC_T_CAST) };
		/** Vector-Scalar-Vector helper class. */
		template<typename VIN, typename SIN, typename UA_OUT>
		struct VSV { __OUT_COLLECTION(__DEFINE_VSV, __NO_CAST) };
		/** Vector-Scalar helper class. */
		template<typename UA_INOUT, typename SIN>
		struct VS { __IN_COLLECTION(__DEFINE_VS, __NO_CAST) };
		/** Vector-Vector-Scalar helper class. */
		template<typename LIN, typename RIN, typename T>
		struct VVS { __DEFINE_VVS(dot, +=, *); };
		/** Vector-Vector-Boolean helper class. */
		template<typename LIN, typename RIN>
		struct VVBOperations {
			__DEFINE_VVB(eq, == , &&, true);
			__DEFINE_VVB(neq, != , || , false);
			__DEFINE_VVB(lt, < , &&, true);
			__DEFINE_VVB(lte, <= , &&, true);
			__DEFINE_VVB(gt, > , &&, true);
			__DEFINE_VVB(gte, >= , &&, true);
		};
		/** Vector-Scalar-Boolean helper class. */
		template<typename LIN, typename RIN>
		struct VSBOperations {
			__DEFINE_VSB(eq, == , &&, true);
			__DEFINE_VSB(neq, != , || , false);
			__DEFINE_VSB(lt, < , &&, true);
			__DEFINE_VSB(lte, <= , &&, true);
			__DEFINE_VSB(gt, > , &&, true);
			__DEFINE_VSB(gte, >= , &&, true);
		};
	public:
		/** Dot product - dot(VECTOR, VECTOR) = SCALAR. */
		template<typename T, typename LIN, typename RIN>
		__INLINE static __CONSTEXPR T dot(const LIN & lin, const RIN & rin) { T result = 0; VVS<const LIN &, const RIN &, T>::dot(lin, rin, result); return result; }
		/** Dot product - dot(VECTOR, VECTOR) = SCALAR. */
		template<typename LIN, typename RIN, typename T>
		__INLINE static __CONSTEXPR void dot(const LIN & lin, const RIN & rin, T & result) { VVS<const LIN &, const RIN &, T>::dot(lin, rin, result); }
		/** Vector-Scalar addition - VECTOR + SCALAR = VECTOR. */
		template<typename VIN, typename SIN, typename UA_OUT>
		__INLINE static __CONSTEXPR void adds(const VIN& lin, SIN rin, UA_OUT& result) { VSV<const VIN&, SIN, UA_OUT&>::add(lin, rin, result); }
		/** Vector-Scalar addition - VECTOR += SCALAR. */
		template<typename UA_INOUT, typename SIN>
		__INLINE static __CONSTEXPR void adds(UA_INOUT& io, SIN s) { VS<UA_INOUT&, SIN>::add(io, s); }
		/** Vector-Vector addition - VECTOR + VECTOR = VECTOR. */
		template<typename LIN, typename RIN, typename UA_OUT>
		__INLINE static __CONSTEXPR void addv(const LIN & lin, const RIN & rin, UA_OUT & out) { VVV<const LIN &, const RIN &, UA_OUT &>::add(lin, rin, out); }
		/** Vector-Vector addition - VECTOR + VECTOR = VECTOR. */
		template<typename CAST, typename LIN, typename RIN, typename UA_OUT>
		__INLINE static __CONSTEXPR void addv(const LIN & lin, const RIN & rin, UA_OUT & out) { VVVC<const LIN &, const RIN &, UA_OUT &, CAST>::add(lin, rin, out); }
		/** Vector-Vector addition - VECTOR += VECTOR. */
		template<typename UA_INOUT, typename UA_IN>
		__INLINE static __CONSTEXPR void addv(UA_INOUT & io, const UA_IN & i) { VV<UA_INOUT &, const UA_IN &>::add(io, i); }
		/** Vector-Vector addition - VECTOR += VECTOR. */
		template<typename CAST, typename UA_INOUT, typename UA_IN>
		__INLINE static __CONSTEXPR void addv(UA_INOUT & io, const UA_IN & i) { VVC<UA_INOUT &, const UA_IN &, CAST>::add(io, i); }
		/** Vector-Scalar subtraction - VECTOR - SCALAR = VECTOR. */
		template<typename VIN, typename SIN, typename UA_OUT>
		__INLINE static __CONSTEXPR void subs(const VIN& lin, SIN rin, UA_OUT& result) { VSV<const VIN&, SIN, UA_OUT&>::sub(lin, rin, result); }
		/** Vector-Scalar subtraction - VECTOR -= SCALAR. */
		template<typename UA_INOUT, typename SIN>
		__INLINE static __CONSTEXPR void subs(UA_INOUT& io, SIN s) { VS<UA_INOUT&, SIN>::sub(io, s); }
		/** Vector-Vector subtraction - VECTOR - VECTOR = VECTOR. */
		template<typename LIN, typename RIN, typename UA_OUT>
		__INLINE static __CONSTEXPR void subv(const LIN & lin, const RIN & rin, UA_OUT & out) { VVV<const LIN &, const RIN &, UA_OUT &>::sub(lin, rin, out); }
		/** Vector-Vector subtraction - VECTOR - VECTOR = VECTOR. */
		template<typename CAST, typename LIN, typename RIN, typename UA_OUT>
		__INLINE static __CONSTEXPR void subv(const LIN & lin, const RIN & rin, UA_OUT & out) { VVVC<const LIN &, const RIN &, UA_OUT &, CAST>::sub(lin, rin, out); }
		/** Vector-Vector subtraction - VECTOR -= VECTOR. */
		template<typename UA_INOUT, typename UA_IN>
		__INLINE static __CONSTEXPR void subv(UA_INOUT & io, const UA_IN & i) { VV<UA_INOUT &, const UA_IN &>::sub(io, i); }
		/** Vector-Vector subtraction - VECTOR -= VECTOR. */
		template<typename CAST, typename UA_INOUT, typename UA_IN>
		__INLINE static __CONSTEXPR void subv(UA_INOUT & io, const UA_IN & i) { VVC<UA_INOUT &, const UA_IN &, CAST>::sub(io, i); }
		/** Vector-Vector multiplication - VECTOR * VECTOR = VECTOR. */
		template<typename LIN, typename RIN, typename UA_OUT>
		__INLINE static __CONSTEXPR void mulv(const LIN & lin, const RIN & rin, UA_OUT & out) { VVV<const LIN &, const RIN &, UA_OUT &>::mul(lin, rin, out); }
		/** Vector-Vector multiplication - VECTOR * VECTOR = VECTOR. */
		template<typename CAST, typename LIN, typename RIN, typename UA_OUT>
		__INLINE static __CONSTEXPR void mulv(const LIN & lin, const RIN & rin, UA_OUT & out) { VVVC<const LIN &, const RIN &, UA_OUT &, CAST>::mul(lin, rin, out); }
		/** Vector-Vector multiplication - VECTOR * VECTOR = VECTOR. */
		template<typename UA_INOUT, typename UA_IN>
		__INLINE static __CONSTEXPR void mulv(UA_INOUT & io, const UA_IN & i) { VV<UA_INOUT &, const UA_IN &>::mul(io, i); }
		/** Vector-Vector multiplication - VECTOR * VECTOR = VECTOR. */
		template<typename CAST, typename UA_INOUT, typename UA_IN>
		__INLINE static __CONSTEXPR void mulv(UA_INOUT & io, const UA_IN & i) { VVC<UA_INOUT &, const UA_IN &, CAST>::mul(io, i); }
		/** Vector-Scalar multiplication - VECTOR * SCALAR = VECTOR. */
		template<typename VIN, typename SIN, typename UA_OUT>
		__INLINE static __CONSTEXPR void muls(const VIN & lin, SIN rin, UA_OUT & result) { VSV<const VIN &, SIN, UA_OUT &>::mul(lin, rin, result); }
		/** Vector-Scalar multiplication - VECTOR *= SCALAR. */
		template<typename UA_INOUT, typename SIN>
		__INLINE static __CONSTEXPR void muls(UA_INOUT & io, SIN s) { VS<UA_INOUT &, SIN>::mul(io, s); }
		/** Vector-Vector division - VECTOR / VECTOR = VECTOR. */
		template<typename LIN, typename RIN, typename UA_OUT>
		__INLINE static __CONSTEXPR void divv(const LIN & lin, const RIN & rin, UA_OUT & out) { VVV<const LIN &, const RIN &, UA_OUT &>::div(lin, rin, out); }
		/** Vector-Vector division - VECTOR / VECTOR = VECTOR. */
		template<typename CAST, typename LIN, typename RIN, typename UA_OUT>
		__INLINE static __CONSTEXPR void divv(const LIN & lin, const RIN & rin, UA_OUT & out) { VVVC<const LIN &, const RIN &, UA_OUT &, CAST>::div(lin, rin, out); }
		/** Vector-Vector division - VECTOR /= VECTOR. */
		template<typename UA_INOUT, typename UA_IN>
		__INLINE static __CONSTEXPR void divv(UA_INOUT & io, const UA_IN & i) { VV<UA_INOUT &, const UA_IN &>::div(io, i); }
		/** Vector-Vector division - VECTOR /= VECTOR. */
		template<typename CAST, typename UA_INOUT, typename UA_IN>
		__INLINE static __CONSTEXPR void divv(UA_INOUT & io, const UA_IN & i) { VVC<UA_INOUT &, const UA_IN &, CAST>::div(io, i); }
		/** Vector-Scalar division - VECTOR / SCALAR = VECTOR. */
		template<typename VIN, typename SIN, typename UA_OUT>
		__INLINE static __CONSTEXPR void divs(const VIN & lin, SIN rin, UA_OUT & result) { VSV<const VIN &, SIN, UA_OUT &>::mul(lin, 1 / rin, result); }
		/** Vector-Scalar division - VECTOR /= SCALAR. */
		template<typename UA_INOUT, typename SIN>
		__INLINE static __CONSTEXPR void divs(UA_INOUT & io, SIN s) { VS<UA_INOUT &, SIN>::mul(io, 1 / s); }
		/** Vector-Vector setter - VECTOR = Vector. */
		template<typename UA_INOUT, typename UA_IN>
		__INLINE static __CONSTEXPR void setv(UA_INOUT & io, const UA_IN & in) { VV<UA_INOUT &, const UA_IN &>::set(io, in); }
		/** Vector-Vector setter - VECTOR = Vector. */
		template<typename CAST, typename UA_INOUT, typename UA_IN>
		__INLINE static __CONSTEXPR void setv(UA_INOUT & io, const UA_IN & in) { VVC<UA_INOUT &, const UA_IN &, CAST>::set(io, in); }
		/** Vector-Scalar setter - VECTOR = SCALAR. */
		template<typename UA_INOUT, typename SIN>
		__INLINE static __CONSTEXPR void sets(UA_INOUT & io, SIN s) { VS<UA_INOUT &, SIN>::set(io, s); }
		/** Vector negation - VECTOR = abs(VECTOR). */
		template<typename UA_INOUT, typename UA_IN>
		__INLINE static __CONSTEXPR void absv(UA_INOUT & io, const UA_IN & s) { VV<UA_INOUT &, const UA_IN &>::abs(io, s); }
		/** Vector negation - VECTOR = -VECTOR. */
		template<typename UA_INOUT, typename UA_IN>
		__INLINE static __CONSTEXPR void negv(UA_INOUT & io, const UA_IN & s) { VV<UA_INOUT &, const UA_IN &>::neg(io, s); }
		/** Vector-Vector-Vector saturation MIN <= OUT <= MAX. */
		template<typename LIN, typename RIN, typename UA_OUT>
		__INLINE static __CONSTEXPR void satv(const LIN & min, const RIN & max, UA_OUT & out) { VVV<const LIN &, const RIN &, UA_OUT &>::sat(min, max, out); }
		/** Vector-Vector-Vector min, per component  out[i] = l[i] < r[i] ? l[i] : r[i]. */
		template<typename LIN, typename RIN, typename UA_OUT>
		__INLINE static __CONSTEXPR void minv(const LIN & lin, const RIN & rin, UA_OUT & out) { VVV<const LIN &, const RIN &, UA_OUT &>::min(lin, rin, out); }
		/** Vector-Vector-Vector max, per component  out[i] = r[i] < l[i] ? l[i] : r[i]. */
		template<typename LIN, typename RIN, typename UA_OUT>
		__INLINE static __CONSTEXPR void maxv(const LIN & lin, const RIN & rin, UA_OUT & out) { VVV<const LIN &, const RIN &, UA_OUT &>::max(lin, rin, out); }
		/** Vector-Vector minimum - if (VECTOR_INOUT[?] > VECTOR_OUT[?]) VECTOR_INOUT[?] = VECTOR_OUT[?];  */
		template<typename UA_INOUT, typename UA_IN>
		__INLINE static __CONSTEXPR void minv(UA_INOUT & io, const UA_IN & in) { VV<UA_INOUT &, const UA_IN &>::min(io, in); }
		/** Vector-Vector maximum - if (VECTOR_INOUT[?] < VECTOR_OUT[?]) VECTOR_INOUT[?] = VECTOR_OUT[?];  */
		template<typename UA_INOUT, typename UA_IN>
		__INLINE static __CONSTEXPR void maxv(UA_INOUT & io, const UA_IN & in) { VV<UA_INOUT &, const UA_IN &>::max(io, in); }

	public: // vector-vector-boolean operations
		/** Vector-Vector comparison - VECTOR == VECTOR. */
		template<typename LIN, typename RIN>
		__INLINE static __CONSTEXPR bool eq(const LIN & lin, const RIN & rin) { return VVBOperations<const LIN &, const RIN &>::eq(lin, rin); }
		/** Vector-Vector comparison - VECTOR != VECTOR. */
		template<typename LIN, typename RIN>
		__INLINE static __CONSTEXPR bool neq(const LIN & lin, const RIN & rin) { return VVBOperations<const LIN &, const RIN &>::neq(lin, rin); }
		/** Vector-Vector comparison - VECTOR < VECTOR, return true if all corresponding elements of l are less than r. */
		template<typename LIN, typename RIN>
		__INLINE static __CONSTEXPR bool lt(const LIN & lin, const RIN & rin) { return VVBOperations<const LIN &, const RIN &>::lt(lin, rin); }
		/** Vector-Vector comparison - VECTOR <= VECTOR, return true if all corresponding elements of l are less than or equal to r. */
		template<typename LIN, typename RIN>
		__INLINE static __CONSTEXPR bool lte(const LIN & lin, const RIN & rin) { return VVBOperations<const LIN &, const RIN &>::lte(lin, rin); }
		/** Vector-Vector comparison - VECTOR > VECTOR, return true if all corresponding elements of l are greater than to r. */
		template<typename LIN, typename RIN>
		__INLINE static __CONSTEXPR bool gt(const LIN & lin, const RIN & rin) { return VVBOperations<const LIN &, const RIN &>::gt(lin, rin); }
		/** Vector-Vector comparison - VECTOR >= VECTOR, return true if all corresponding elements of l are greater than or equal to r. */
		template<typename LIN, typename RIN>
		__INLINE static __CONSTEXPR bool gte(const LIN & lin, const RIN & rin) { return VVBOperations<const LIN &, const RIN &>::gte(lin, rin); }

	public: // vector-scalar-boolean operations
		/** Vector-Scalar comparison - VECTOR[?] == SCALAR, true of all components are equal to scalar. */
		template<typename LIN, typename RIN>
		__INLINE static __CONSTEXPR bool eqs(const LIN & lin, const RIN & rin) { return VSBOperations<const LIN &, const RIN &>::eq(lin, rin); }
		/** Vector-Scalar comparison - VECTOR[?] != SCALAR, true if not all components are equal to scalar. */
		template<typename LIN, typename RIN>
		__INLINE static __CONSTEXPR bool neqs(const LIN & lin, const RIN & rin) { return VSBOperations<const LIN &, const RIN &>::neq(lin, rin); }
		/** Vector-Scalar comparison - VECTOR[?] < SCALAR, true if all components are less than scalar. */
		template<typename LIN, typename RIN>
		__INLINE static __CONSTEXPR bool lts(const LIN & lin, const RIN & rin) { return VSBOperations<const LIN &, const RIN &>::lt(lin, rin); }
		/** Vector-Scalar comparison - VECTOR[?] <= SCALAR, true if all components are less than or equal to scalar. */
		template<typename LIN, typename RIN>
		__INLINE static __CONSTEXPR bool ltes(const LIN & lin, const RIN & rin) { return VSBOperations<const LIN &, const RIN &>::lte(lin, rin); }
		/** Vector-Scalar comparison - VECTOR[?] > SCALAR, true if all componenets are greater than scalar. */
		template<typename LIN, typename RIN>
		__INLINE static __CONSTEXPR bool gts(const LIN & lin, const RIN & rin) { return VSBOperations<const LIN &, const RIN &>::gt(lin, rin); }
		/** Vector-Scalar comparison - VECTOR[?] >= SCALAR, true if all components are greater than or equal to scalar. */
		template<typename LIN, typename RIN>
		__INLINE static __CONSTEXPR bool gtes(const LIN & lin, const RIN & rin) { return VSBOperations<const LIN &, const RIN &>::gte(lin, rin); }
	};
}


#undef __OF_ADD
#undef __OF_DIV
#undef __OF_MAX
#undef __OF_MIN
#undef __OF_MUL
#undef __OF_SAT
#undef __OF_SUB
#undef __IF_ABS
#undef __IF_ADD
#undef __IF_DIV
#undef __IF_MAX
#undef __IF_MIN
#undef __IF_MUL
#undef __IF_NEG
#undef __IF_SET
#undef __IF_SUB
#undef __OUT_COLLECTION
#undef __IN_COLLECTION
#undef __STATIC_T_CAST
#undef __NO_CAST
#undef __DEFINE_VVV
#undef __DEFINE_VV
#undef __DEFINE_VSV
#undef __DEFINE_VS
#undef __DEFINE_VVS
#undef __DEFINE_VVB
#undef __CONSTEXPR
#undef __INLINE
