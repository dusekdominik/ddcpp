#pragma once
namespace DD {
	/** Base64 encoding. */
	struct Base64;
}

#include <DD/ArrayView.h>
#include <DD/Array.h>
#include <DD/String.h>
#include <DD/Types.h>
#include <DD/Text/StringWriter.h>

namespace DD {
	struct Base64 {
		/** 8-bit type. */
		using Byte = char;
		/** Reader individual values of a quadruplet.  */
		struct Quadruplet {
			/** Data storage, @see Triplet::GetQuadruplet(). */
			u32 _value;
			/** Get value of quadruplet item, indices 3..1. */
			u32 Get(u32 index) { return (_value >> (index * 6)) & 0x3f; }
		};
		/** Memory aligned triplet, represents first 24bit at a 32bit frame. */
		struct Triplet32 {
			/** Byte view of 32 bit data. */
			u8 X, Y, Z, _padding;
			/** Convert to quadruplet, need to solve endian-ness */
			Quadruplet GetQuadruplet() const { return { (u32)X << 16 | Y << 8 | Z }; }
		};

		/** Table mapping. */
		static constexpr char Mapping[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
		/** Constant used for ends of strings. */
		static constexpr char End = '=';
		/** Mapping offset. */
		static constexpr u32 OffsetUpperLetter = 0;
		/** Mapping offset. */
		static constexpr u32 OffsetLowerLetter = 26;
		/** Mapping offset. */
		static constexpr u32 OffsetNumber = 52;

		/** Compute number of bytes needed to store in base64. */
		static constexpr size_t Binary2Base_Count(size_t length) { return ((length / 3) + (length % 3 != 0)) * 4; }
		/** Compute number of bytes needed after decode from base64. */
		static constexpr size_t Base2Binary_Count(size_t length) { return (length / 4) * 3; }
		/** */
		template<typename CT, typename ST>
		static constexpr size_t Waste(Text::StringViewT<const CT, ST> view) { return (view.Last() == '=') + (view.Last(1) == '='); }
		/** */
		template<typename CT, typename ST>
		static constexpr size_t Base2Binary_Count(Text::StringViewT<const CT, ST> view) { return Base2Binary_Count(view.Length()) - Waste(view); }
		/** Decode single letter from */
		template<typename CHAR_TYPE>
		static constexpr u32 DecodeU8(CHAR_TYPE value);

		/** Encode binary data to Base64. */
		template<typename CT, typename ST, size_t N>
		static void Encode(ConstByteView binaryData, Text::StringWriterT<CT, ST, N> & writer);
		/** Encode binary data to Base64. */
		template<typename CT = char, typename ST = size_t>
		static Text::StringWriterT<CT, ST, 0> EncodeH(ConstByteView binaryData, Text::StringViewT<const CT, ST> header = nullptr);
		/** Decode binary data from Base64. */
		template<typename CHAR_TYPE>
		static Array<Byte> Decode(Text::StringViewT<const CHAR_TYPE> baseData);
		/** Decode binary data from Base64. */
		template<typename CHAR_TYPE>
		static void Decode(Text::StringViewT<const CHAR_TYPE> baseData, ByteView target);
		/** Decode binary data from Base64. */
		template<typename CHAR_TYPE, typename T>
		static bool TryDecode(Text::StringViewT<const CHAR_TYPE> baseData, T & target);
	};
}


namespace DD {
	template<typename CT, typename ST, size_t N>
	void Base64::Encode(ConstByteView  binaryData, Text::StringWriterT<CT, ST, N> & writer) {
		writer.Reserve(Binary2Base_Count(binaryData.Length()) + 1);
		for (size_t i = 0, l = binaryData.Length() / 3; i < l; ++i) {
			const Triplet32 & triplet = *((Triplet32 *)(binaryData.begin() + i * 3));
			Quadruplet quadruplet = triplet.GetQuadruplet();
			writer.Write(Mapping[quadruplet.Get(3)]);
			writer.Write(Mapping[quadruplet.Get(2)]);
			writer.Write(Mapping[quadruplet.Get(1)]);
			writer.Write(Mapping[quadruplet.Get(0)]);
		}

		ArrayView1D<const u8> undata((const u8 *)binaryData.begin(), binaryData.Length());
		// resolve incomplete triplet at the end of data if necessary
		switch (binaryData.Length() % 3) {
			case 1: {
				Quadruplet quadruplet = Triplet32{ undata.Last(), 0, 0, 0 }.GetQuadruplet();
				writer.Write(Mapping[quadruplet.Get(3)]);
				writer.Write(Mapping[quadruplet.Get(2)]);
				writer.Write(End);
				writer.Write(End);
			} break;
			case 2: {
				Quadruplet quadruplet = Triplet32{ undata.Last(1), undata.Last(0), 0, 0 }.GetQuadruplet();
				writer.Write(Mapping[quadruplet.Get(3)]);
				writer.Write(Mapping[quadruplet.Get(2)]);
				writer.Write(Mapping[quadruplet.Get(1)]);
				writer.Write(End);
			} break;
		}
	}


	template<typename CT, typename ST>
	Text::StringWriterT<CT, ST, 0> Base64::EncodeH(ConstByteView binaryData, Text::StringViewT<const CT, ST> header /*= nullptr*/) {
		// preallocate, extra byte for null terminator
		Text::StringWriterT<CT, ST, 0> writer;
		writer.Reserve(header.Length());
		Encode(binaryData, writer);
		writer.Write(header);
		return writer;
	}


	template<typename CHAR_TYPE>
	constexpr u32 Base64::DecodeU8(CHAR_TYPE value) {
		if (Text::IsUpperLetter(value))
			return value - 'A' + OffsetUpperLetter;

		if (Text::IsLowerLetter(value))
			return value - 'a' + OffsetLowerLetter;

		if (Text::IsNumber(value))
			return value - '0' + OffsetNumber;

		switch (value) {
			case '+': return 62;
			case '/': return 63;
		}

		return 0xFFFFFFFF;
	}


	template<typename CHAR_TYPE>
	void Base64::Decode(Text::StringViewT<const CHAR_TYPE> baseData, ByteView target) {
		struct T { u8 C, B, A, _; };
		union U { u32 V; T C; };
		for (size_t index = 0, limit = (baseData.Length() - Waste(baseData)) / 4; index < limit; ++index) {
			size_t s = index * 4;
			size_t t = index * 3;

			U decoder{ DecodeU8(baseData[s]) << 18 | DecodeU8(baseData[s + 1]) << 12 | DecodeU8(baseData[s + 2]) << 6 | DecodeU8(baseData[s + 3]) << 0 };
			target[t + 0] = decoder.C.A;
			target[t + 1] = decoder.C.B;
			target[t + 2] = decoder.C.C;
		}

		switch (Waste(baseData)) {
			case 1: {
				U decoder{ DecodeU8(baseData.Last(3)) << 18 | DecodeU8(baseData.Last(2)) << 12 | DecodeU8(baseData.Last(1)) << 6 };
				target.Last(1) = decoder.C.A;
				target.Last(0) = decoder.C.B;
			} break;

			case 2: {
				U decoder{ DecodeU8(baseData.Last(3)) << 18 | DecodeU8(baseData.Last(2)) << 12 };
				target.Last(0) = decoder.C.A;
			} break;
		}
	}


	template<typename CHAR_TYPE>
	Array<Base64::Byte> Base64::Decode(Text::StringViewT<const CHAR_TYPE> baseData) {
		Array<Base64::Byte> result;

		if (baseData.Length()) {
			result.Reallocate(Base2Binary_Count(baseData));
			Decode(baseData, result.ToView());
		}

		return result;
	}


	template<typename CHAR_TYPE, typename T>
	bool Base64::TryDecode(Text::StringViewT<const CHAR_TYPE> baseData, T & target) {
		ByteView view = AsByteView(target);
		if (Base2Binary_Count(baseData) != view.Length())
			return false;

		Decode(baseData, view);
		return true;
	}
}
