#pragma once
namespace DD {
	template<typename T>
	class NotifyWrapper;

	typedef NotifyWrapper<int> NotifyInt;
	typedef NotifyWrapper<unsigned> NotifyUnsigned;
	typedef NotifyWrapper<float> NotifyFloat;
	typedef NotifyWrapper<double> NotifyDouble;
	typedef NotifyWrapper<size_t> NotifySize;
}

#include "Delegate.h"

namespace DD {
	template<typename T>
	class NotifyWrapper {
		T _value;
	public:
		Event<const T &, const T &> Changed;
	public:
		void operator=(const T & value) {
			Changed(_value, value);
			_value = value;
		}
		operator const T &() const { return _value; }
	};
}
