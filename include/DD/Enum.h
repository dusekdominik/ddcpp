#pragma once
namespace DD {
	/**
	 * Enumeration type.
	 * Abstract parent of all enums.
	 * Serves for SFINAE check.
	 */
	struct IEnum;
}

#include <DD/Macros.h>
#include <DD/Text/StringView.h>

namespace DD {
	struct IEnum {
		/** Info type about enums. */
		struct Descriptor {
			/** Get number of items. */
			virtual u32 Size() const = 0;
			/** Get index by name. */
			virtual i32 String2Index(const Text::StringView16 &) const = 0;
			/** Get numeric value by given index. */
			virtual u64 Index2Value(i32) const = 0;
			/** Get index by numeric value. */
			virtual i32 Value2Index(u64) const = 0;
			/** Get help for whole enum. */
			virtual ::DD::Text::StringView16 Help() const = 0;
			/** Get name of value by given index (not by enum value). */
			virtual ::DD::Text::StringView16 Index2String(i32) const = 0;

			/** Get name of value by given enum value. */
			::DD::Text::StringView16 Value2String(u64 value) const { return Index2String(Value2Index(value)); }
			/** Get numeric value by given string. */
			const u64 String2Value(const Text::StringView16 & string) const { return Index2Value(String2Index(string)); }
		};
	};

	/** Natvis helper for displaying enum tags. */
	template<typename>
	struct Enum : public IEnum {};
}

/**
 * DD_ENUM_N(enumName, [enumValues])
 * inline enum declaration.
 * @example common usage
 *   DD_ENUM_32(MyEnum, One, Two, Three);      // declare enum
 *   MyEnum test1 = MyEnum::Two;               // instance of enum
 *   MyEnum test2 = MyEnum::FromString("Two"); // FromString method
 *   MyEnum test3 = MyEnum::FromIndex(1);      // FromIndex method
 *   DD::Debug::PrintLine(test1.ToString());   // ToString method
 * @example explicit values
 *   DD_ENUM_32(MyEnum, (One, 1), (Two, 2), (Three, 3));
 * @example fully documented enum
 *   DD_ENUM_32(
 *     (MyEnum, "My comments what about enum"),
 *     (One, 1, "My first item"),
 *     (Two, 2, "My second item")
 *   );
 *   DD::Debug::PrintLine(MyEnum::Help());      // print documentation
 */
#define DD_ENUM_8(enumDef, ...)  _DD_ENUM_BIT_TEMPLATE(VARGS,  8, enumDef, __VA_ARGS__)
#define DD_ENUM_16(enumDef, ...) _DD_ENUM_BIT_TEMPLATE(VARGS, 16, enumDef, __VA_ARGS__)
#define DD_ENUM_32(enumDef, ...) _DD_ENUM_BIT_TEMPLATE(VARGS, 32, enumDef, __VA_ARGS__)
#define DD_ENUM_64(enumDef, ...) _DD_ENUM_BIT_TEMPLATE(VARGS, 64, enumDef, __VA_ARGS__)

/**
 * DD_ENUM_N_X(enumName, xList)
 * enum declaration via xList.
 * @example common usage
 *   #define MyEnumKeys(X) X(One) X(Two) X(Three) // declare xList
 *   DD_ENUM_32_X(MyEnum, MyEnumKeys)             // declare enum
 *   MyEnum test1 = MyEnum::Two;                  // instance of enum
 *   MyEnum test2 = MyEnum::FromString("Two");    // FromString method
 *   MyEnum test3 = MyEnum::FromIndex(1);         // FromIndex method
 *   DD::Debug::PrintLine(test1.ToString());      // ToString method
 * @example explicit values
 *   #define MyEnumKeys(X) X(One, 1) X(Two, 2) X(Three, 3)
 * @example fully documented enum
 *   #define MyEnumKeys(X) X(One, 1, "My first item") X(Two, 2, "My second item")
 *   DD_ENUM_32_X((MyEnum, "My comments what about enum."), MyEnumKeys)
 *   DD::Debug::PrintLine(MyEnum::Help());      // print documentation
 */
#define DD_ENUM_8_X(enumDef, ...)  _DD_ENUM_BIT_TEMPLATE(XLIST,  8, enumDef, __VA_ARGS__)
#define DD_ENUM_16_X(enumDef, ...) _DD_ENUM_BIT_TEMPLATE(XLIST, 16, enumDef, __VA_ARGS__)
#define DD_ENUM_32_X(enumDef, ...) _DD_ENUM_BIT_TEMPLATE(XLIST, 32, enumDef, __VA_ARGS__)
#define DD_ENUM_64_X(enumDef, ...) _DD_ENUM_BIT_TEMPLATE(XLIST, 64, enumDef, __VA_ARGS__)

/** Main enum properties */
#define _DD_ENUM_DEF_NAME(def) DD_UNWRAP_HEAD(0, def)
#define _DD_ENUM_DEF_HELP(def) _DD_OVERLOAD(_DD_ENUM_DEF_HELP, DD_UNWRAP(def))
#define _DD_ENUM_DEF_HELP_1(...) L""
#define _DD_ENUM_DEF_HELP_2(name, help) L ## #name L" - " DD_CAT_2(L, help)

/** Enum static string. */
#define _DD_ENUM_ESS(name, /*value*/...) static constexpr ::DD::Text::StringView16 _##name(L ## #name);
#define _DD_ENUM_ESSH(name, ...) static constexpr ::DD::Text::StringView16 _##name(L"Not implemented help.");
/** Help documentation. */
#define _DD_ENUM_EH(...) _DD_OVERLOAD(_DD_ENUM_EH, __VA_ARGS__)
#define _DD_ENUM_EH_1(name, ...) // no documentation
#define _DD_ENUM_EH_2(name, ...) // no documentation
#define _DD_ENUM_EH_3(name, value, help) L"\n - " L ## #name L" - " L ## help
/** Switch case providers. */
#define _DD_ENUM_E2H(name, /*value*/...) case Hashes::Hash_##name: return Values::name;
#define _DD_ENUM_E2I(name, /*value*/...) case Values::name: return (::DD::i32)Indices::Index_##name;
#define _DD_ENUM_I2E(name, /*value*/...) case Indices::Index_##name: return Values::name;
#define _DD_ENUM_E2S(name, /*value*/...) case Values::name: return _##name;
/** Enum declarations. */
#define _DD_ENUM_HASH(name, /*value*/...) Hash_##name = ::DD::Text::StringView(#name).Hash(),
#define _DD_ENUM_INDEX(name, /*value*/...) Index_##name,
#define _DD_ENUM_VALUE(...) _DD_OVERLOAD(_DD_ENUM_VALUE, __VA_ARGS__)
#define _DD_ENUM_VALUE_1(name) name,
#define _DD_ENUM_VALUE_2(name, value) name = value,
#define _DD_ENUM_VALUE_3(name, value, ...) name = value,
#define _DD_ENUM_HELP(...) _DD_OVERLOAD(_DD_ENUM_HELP, __VA_ARGS__)
#if 0 // _DEBUG
#define _DD_ENUM_THROW(value) throw
#else
#define _DD_ENUM_THROW(value) {return value;}
#endif

/**
 * Enum with helper operations.
 *   Conversion from/into string.
 *   Conversion from/into index.
 *   Self-documenting help string.
 */
#define _DD_ENUM_BIT_TEMPLATE(TYPE, BITS, DEF, ...)\
class _DD_ENUM_DEF_NAME(DEF)\
 : public ::DD::Enum<_DD_ENUM_DEF_NAME(DEF)>\
{\
	/** Descriptor implementation. */\
	struct DescImpl\
  : public Descriptor\
	{\
		::DD::u32 Size() const override {\
			 return (::DD::u32)Indices::SIZE;\
		}\
		::DD::Text::StringView16 Index2String(::DD::i32 index) const override {\
			return ToString(FromIndex(index));\
		}\
		::DD::i32 String2Index(const ::DD::Text::StringView16 & str) const override {\
			return ToIndex(FromString(str));\
		}\
		::DD::u64 Index2Value(::DD::i32 index) const override {\
			return (::DD::u64)(FromIndex(index));\
		}\
		::DD::i32 Value2Index(::DD::u64 value) const override {\
			return ToIndex((Values)value);\
		}\
		::DD::Text::StringView16 Help() const override {\
			return _DD_ENUM_DEF_NAME(DEF)::Help();\
		}\
	};\
\
public: /* nested & statics */\
	/** Native integer value according given bits. */\
	typedef ::DD::Unsigned<BITS/8> native_t;\
  /** Index of value. */\
  enum struct Indices : ::DD::i32 { DD_TEMPLATE_PLAY(TYPE, _DD_ENUM_INDEX, __VA_ARGS__) SIZE };\
  /** Hash of value. */\
  enum struct Hashes : ::DD::u64 { DD_TEMPLATE_PLAY(TYPE, _DD_ENUM_HASH, __VA_ARGS__) };\
	/** Value tags. */\
	enum Values { DD_TEMPLATE_PLAY(TYPE, _DD_ENUM_VALUE, __VA_ARGS__) };\
	/** Descriptor getter. */\
	static const Descriptor * GetDescriptor() { static DescImpl d; return &d; }\
	/** Convert from option string hash into option value. */\
	static constexpr _DD_ENUM_DEF_NAME(DEF) FromHash(::DD::u64 hash) {\
		switch((Hashes)hash) {\
			DD_TEMPLATE_PLAY(TYPE, _DD_ENUM_E2H, __VA_ARGS__)\
			default: _DD_ENUM_THROW(_DD_ENUM_DEF_NAME(DEF)());\
		}\
	}\
	/** Convert from option index hash into option value. */\
	static constexpr _DD_ENUM_DEF_NAME(DEF) FromIndex(::DD::i32 index) {\
		switch((Indices)index) {\
			DD_TEMPLATE_PLAY(TYPE, _DD_ENUM_I2E, __VA_ARGS__)\
			default: _DD_ENUM_THROW(_DD_ENUM_DEF_NAME(DEF)());\
		}\
	}\
	/** Convert from option string hash into option value. */\
	static const _DD_ENUM_DEF_NAME(DEF) FromString(const ::DD::Text::StringView16 & str) {\
		return FromHash(str.Hash());\
	}\
	/** Convert from option value hash into option index. */\
	static constexpr ::DD::i32 ToIndex(_DD_ENUM_DEF_NAME(DEF) value) {\
		switch ((Values)value._value) {\
			DD_TEMPLATE_PLAY(TYPE, _DD_ENUM_E2I, __VA_ARGS__)\
			default: return -1;\
		}\
	}\
	/** Convert from option value hash into option string. */\
	static DD::Text::StringView16 ToString(_DD_ENUM_DEF_NAME(DEF) value) {\
		DD_TEMPLATE_PLAY(TYPE, _DD_ENUM_ESS, __VA_ARGS__)\
		switch ((Values)value._value) {\
			DD_TEMPLATE_PLAY(TYPE, _DD_ENUM_E2S, __VA_ARGS__)\
			default: _DD_ENUM_THROW(nullptr);\
		}\
	}\
	/** Help for given value. */\
	static ::DD::Text::StringView16 Help(_DD_ENUM_DEF_NAME(DEF) value) {\
		DD_TEMPLATE_PLAY(TYPE, _DD_ENUM_ESSH, __VA_ARGS__)\
		switch ((Values)value._value) {\
			DD_TEMPLATE_PLAY(TYPE, _DD_ENUM_E2S, __VA_ARGS__)\
			default: _DD_ENUM_THROW(nullptr);\
		}\
	}\
	/** Help for entire enum. */\
	static ::DD::Text::StringView16 Help() {\
		static constexpr ::DD::Text::StringView16 _help(\
			_DD_ENUM_DEF_HELP(DEF)\
			DD_TEMPLATE_PLAY(TYPE, _DD_ENUM_EH, __VA_ARGS__)\
		); return _help;\
	}\
	/** Number of named items. */\
	static constexpr ::DD::u32 Size() { return (::DD::u32)Indices::SIZE; }\
protected: /* properties*/ \
	/** Value representation. */\
	union {\
		/** Native representation. */\
		native_t _value;\
	};\
\
public: /* methods */\
	/** Convert to native type. */\
	constexpr operator const native_t &() const { return _value; }\
	/** Convert from native type. */\
	constexpr _DD_ENUM_DEF_NAME(DEF) & operator=(const _DD_ENUM_DEF_NAME(DEF) & value) = default;\
	constexpr _DD_ENUM_DEF_NAME(DEF) & operator=(const native_t & value) { _value = value; return *this; }\
	/** Convert from native type. */\
	constexpr _DD_ENUM_DEF_NAME(DEF) & operator=(Values value) { _value = (native_t)value; return *this; }\
	/** Modula increase of option (not of value). */\
	constexpr _DD_ENUM_DEF_NAME(DEF) operator++() { return *this = FromIndex((ToIndex() + 1) % Size()); } \
	/** String value of option. */\
	::DD::Text::StringView16 ToString() const { return ToString(*this); }\
	/** Index of option. */\
	constexpr ::DD::i32 ToIndex() const { return ToIndex(*this); }\
	constexpr Values ToValue() const { return (Values)_value; }\
\
public: /* constructors */\
	/** Zero constructor. */\
	constexpr _DD_ENUM_DEF_NAME(DEF) () = default;\
	/** Constructor from tag. */\
	constexpr _DD_ENUM_DEF_NAME(DEF) (Values v) : _value((native_t)v) {}\
	/** Constructor from integer value. */\
	constexpr _DD_ENUM_DEF_NAME(DEF) (native_t v) : _value(v) {}\
}



