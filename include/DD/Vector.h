#pragma once
#include <DD/Math/Vector.h>
#include <DD/Types.h>
#include <DD/Units/Units.h>

namespace DD {
	/** */
	template<typename T, size_t SIZE, typename MODEL = Math::VectorCommonModel<T, SIZE, size_t>>
	using Vector = Math::Vector<MODEL>;
	/** 1-dimensional vector. */
	template<typename T, typename M = Math::VectorCommonModel<T, 1, size_t>>
	using V1 = Math::Vector<M>;
	/** 2-dimensional vector. */
	template<typename T, typename M = Math::VectorCommonModel<T, 2, size_t>>
	using V2 = Math::Vector<M>;
	/** 3-dimensional vector. */
	template<typename T, typename M = Math::VectorCommonModel<T, 3, size_t>>
	using V3 = Math::Vector<M>;
	/** 4-dimensional vector. */
	template<typename T, typename M = Math::VectorCommonModel<T, 4, size_t>>
	using V4 = Math::Vector<M>;
	/** 2-dimensional float vector. */
	typedef V2<f32> float2;
	/** 3-dimensional float vector. */
	typedef V3<f32> float3;
	/** 4-dimensional float vector. */
	typedef V4<f32> float4;
	/** 1-dimensional double vector. */
	typedef V1<f64> double1;
	/** 2-dimensional double vector. */
	typedef V2<f64> double2;
	/** 3-dimensional double vector. */
	typedef V3<f64> double3;
	/** 4-dimensional double vector. */
	typedef V4<f64> double4;
	/** 1-dimensional int vector. */
	typedef V1<i32> int1;
	/** 2-dimensional int vector. */
	typedef V2<i32> int2;
	/** 3-dimensional int vector. */
	typedef V3<i32> int3;
	/** 4-dimensional int vector. */
	typedef V4<i32> int4;
	/** 2-dimensional uint vector. */
	typedef V2<u32> uint2;
	/** 3-dimensional uint vector. */
	typedef V3<u32> uint3;
	/** 4-dimensional uint vector. */
	typedef V4<u32> uint4;

	using PositionF = Vector<Units::Length_MeterF, 3>;



	/** Cos of vectors. */
	template<typename M0, typename M1>
	auto cos(const Math::Vector<M0> & u, const Math::Vector<M1> & v) {
		return Math::Dot(u, v) / Math::Sqrt(Math::Dot(u) * Math::Dot(v));
	}

	template<typename M0, typename M1>
	auto dot(const Math::Vector<M0> & u, const Math::Vector<M1> & v) {
		return Math::Dot(u, v);
	}

	template<typename M0>
	auto dot(const Math::Vector<M0> & u) {
		return Math::Dot(u);
	}

	template<typename M0, typename M1, typename M2>
	void cross( const Math::Vector<M0> & l, const Math::Vector<M1> & r, Math::Vector<M2> & result) {
		Math::Cross(l, r, result);
	}

	template<typename T0, typename T1>
	V3<T0> cross(const V3<T0> & l, const V3<T1> & r) {
		return Math::Cross(l, r);
	}


}
