#pragma once
namespace DD {
	template<typename T, size_t SIZE>
	class StaticArray {
	public:
		typedef T data_t[SIZE];
	private:
		data_t _data;
	public:
		T & operator[](size_t i) { return _data[i]; }
		T * begin() { return _data; }
		T * end() { return _data + SIZE; }
		const T & operator[](size_t i) const { return _data[i]; }
		const T * begin() const { return _data; }
		const T * end() const { return _data + SIZE; }
		size_t Count() const { return SIZE; }
		StaticArray(const data_t & input) : _data(input) {}
		StaticArray(data_t && input) : _data(input) {}

		template<size_t INDEX, typename T, typename...ARGS>
		void Set(T & arg, ARGS...args) {
			_data[INDEX] = arg;
			Set<INDEX + 1, ARGS...>(args...);
		}
		template<size_t INDEX, typename T>
		void Set(T & arg) { _data[INDEX] = arg; }

		template<size_t INDEX>
		void Set() { }
		
		template<typename...ARGS>
		StaticArray(ARGS...args) { Set<0, ARGS...>(args...); }
	};
}