#pragma once
namespace DD {
	/**
	 * Public interface of iterable collections.
	 * Provides methods for C++11 for loop.
	 * Methods are sourced from IIterable interface.
	 */
	template<typename T>
	struct Iterable;
	/** IIterable interface, provides methods for invoking begin/end iterators as virtual methods. */
	template<typename T>
	struct IIterable;
	/** Standard wrapper for wrapping collections into IIterable. */
	template<typename COLLECTION>
	struct IIterableWrapper;

	/**
	 * Public interface of iterable collections.
	 * Provides methods for C++11 for loop.
	 * Methods are sourced from IConstIterable interface.
	 */
	template<typename T>
	struct ConstIterable;
	/** IConstIterable interface, provides methods for invoking begin/end iterators as virtual methods. */
	template<typename T>
	struct IConstIterable;
	/** Standard wrapper for wrapping collections into IConstIterable. */
	template<typename COLLECTION>
	struct IConstIterableWrapper;
}

#include "Allocation.h"
#include "Iterator.h"
#include "SFINAE.h"

namespace DD {
	template<typename T>
	struct IIterable {
		friend struct Iterable<T>;
		/** Standard name of iterator. */
		typedef Iterator<T> iterator;
	protected:
		/** Storage for interface data. */
		Allocation<16> _iiterable;
	protected:
		/** Iterator. */
		virtual iterator iIterableBegin() = 0;
		/** Iterator. */
		virtual iterator iIterableEnd() = 0;
	};

	template<typename COLLECTION>
	struct IIterableWrapper : public IIterable<sfinaeBeginTargetType<COLLECTION>> {
		typedef IIteratorWrapper<sfinaeBeginTargetType<COLLECTION>, sfinaeBeginType<COLLECTION>> wrapper_t;
	protected: // IIterable
		virtual iterator iIterableBegin() override { return wrapper_t(sfinaeBegin(*(_iiterable.Cast<COLLECTION*>()))); }
		virtual iterator iIterableEnd() override { return wrapper_t(sfinaeEnd(*(_iiterable.Cast<COLLECTION*>()))); }
	public: // constructors
		/** Create from collection. */
		IIterableWrapper(COLLECTION & collection) { _iiterable = &collection; }
	};

	template<typename T>
	struct Iterable {
		/** Storage for iterable data. */
		Allocation<24> _iterable;
	public: // methods
		/** Iterator. */
		Iterator<T> begin() { return _iterable.Cast<IIterable<T>>().iIterableBegin(); }
		/** Iterator. */
		Iterator<T> end() { return _iterable.Cast<IIterable<T>>().iIterableEnd(); }
	public: // constructors
		/** Constructor from an IIterable interface. */
		Iterable(IIterable<T> & iterable) : _iterable(iterable) {}
		/** Constructor from an IIterable interface. */
		Iterable(IIterable<T> * iterable) : _iterable(*iterable) {}
		/** Constructor via default wrapper. */
		template<typename COLLECTION>
		Iterable(COLLECTION & collection) : _iterable(IIterableWrapper<COLLECTION>(collection)) {}
	};

	template<typename T>
	struct IConstIterable {
		friend struct ConstIterable<T>;
		/** Standard name of iterator. */
		typedef Iterator<T> const_iterator;
	protected:
		/** Storage for interface data. */
		Allocation<16> _iiterable;
	protected:
		/** Const iterator. */
		virtual const_iterator iConstIterableBegin() const = 0;
		/** Const iterator. */
		virtual const_iterator iConstIterableEnd() const = 0;
	};

	template<typename COLLECTION>
	struct IConstIterableWrapper : public IConstIterable<sfinaeBeginConstTargetType<COLLECTION>> {
		typedef IIteratorWrapper<const sfinaeBeginConstTargetType<COLLECTION>, sfinaeBeginConstType<COLLECTION>> wrapper_t;
	protected: // IIterable
		virtual const_iterator iConstIterableBegin() const override { return wrapper_t(sfinaeBeginConst(*_iiterable.Cast<COLLECTION*>())); }
		virtual const_iterator iConstIterableEnd() const override { return wrapper_t(sfinaeEndConst(*_iiterable.Cast<COLLECTION*>())); }
	public: // constructors
		/** Create from collection. */
		IConstIterableWrapper(const COLLECTION & collection) { _iiterable = &collection; }
	};

	template<typename T>
	struct ConstIterable {
		/** Storage for iterable data. */
		Allocation<24> _iterable;
	public: // methods
		/** Const Iterator. */
		Iterator<const T> begin() const { return _iterable.Cast<IConstIterable<T>>().iConstIterableBegin(); }
		/** Const Iterator. */
		Iterator<const T> end() const { return _iterable.Cast<IConstIterable<T>>().iConstIterableEnd(); }
	public: // constructors
		/** Constructor from an IIterable interface. */
		ConstIterable(IIterable<T> & iterable) : _iterable(iterable) {}
		/** Constructor from an IIterable interface. */
		ConstIterable(IIterable<T> * iterable) : _iterable(*iterable) {}
		/** Constructor via default wrapper. */
		template<typename COLLECTION>
		ConstIterable(const COLLECTION & collection) : _iterable(IConstIterableWrapper<COLLECTION>(collection)) {}
	};
}
