#pragma once
namespace DD::FileSystem {
	/** Determinated fs item, requires existence, otherwise initialized as null. */
	class File;
}

#include <DD/Array.h>
#include <DD/FileSystem/Item.h>
#include <DD/String.h>

namespace DD::FileSystem {
	class File
		: public Item
	{
	public: // methods
		/** Get file size. */
		size_t Size();
		/** Read whole file. */
		Array<i8> Read();
		/** Read whole file, user is responsible for allocation. */
		bool Read(ByteView target);
		/** Write buffer into the file. */
		bool Write(ConstByteView);
		/** Write buffer into the file. */
		bool Write(const void * begin, size_t size) { return Write(ConstByteView{ (const char *) begin, size }); }

	public: // constructors
		/** Zero constructor. */
		File();
		/** Item constructor - provides type conversion. */
		File(Item item);
		/** Basic constructor - loads given path if exists. */
		File(const String & path);
	};
}
