#pragma once
namespace DD::FileSystem {
	/** Fwd ref. */
	class DirectoryBrowser;

	/** Determinated fs item, requires existence, otherwise initialized as null. */
	class Directory;
}

#include <DD/FileSystem/Item.h>
#include <DD/Text/StringView.h>

namespace DD::FileSystem {
	class Directory
		: public Item
	{
	public: // methods
		/** List directory items. */
		DirectoryBrowser Open(const Text::StringView16 & filter = L"*") const;

	public: // constructors
		/** Zero constructor, initializes dir to current working dir. */
		Directory();
		/** Item constructor - provides type conversion. */
		Directory(Item item) : Item(item) {}
		/** Basic constructor - loads given path if exists. */
		Directory(const Text::StringView16 & path);
	};
}
