#pragma once
#include <DD/DebugLite.h>
#include <DD/List.h>
#include <DD/Text/StringView.h>

#define DD_FS_ASSERT(condition) DD_LITE_DEBUG_ASSERT(condition, "FileSystem failed.");

namespace DD::FileSystem {
	/************************************************************************/
	/* Modes for global methods                                             */
	/************************************************************************/
	/** Modes of file creation. */
	enum struct CreateFileMode { Standard, Recursive };
	/** Modes of directory creation. */
	enum struct CreateDirectoryMode { Standard, Recursive };
	/** Modes of file removing. */
	enum struct RemoveFileMode { Standard };
	/** Modes of directory removing. */
	enum struct RemoveDirectoryModes { Standard };
	/** Modes of copying file. */
	enum struct CopyFileModes { OverwriteIfExists = 0, FailIfExists = 1, Standard = FailIfExists };
	/** Modes of moving. */
	enum struct MoveModes { Standard = 0, ReplaceExisting = 1, AllowCopy = 2 };


	/************************************************************************/
	/* Global filesystem methods                                            */
	/************************************************************************/
	/** Check if path exists in fs. */
	bool Exists(const Text::StringView16 & path);
	/** Check if given path is directory. */
	bool IsDirectory(const Text::StringView16 & path);
	/** Check if given path is file. */
	bool IsFile(const Text::StringView16 & path);
	/** Check if given path belongs to readable fs item. */
	bool IsReadable(const Text::StringView16 & path);
	/** Check if given path belongs to writable fs item. */
	bool IsWritable(const Text::StringView16 & path);
	/** Create file of given path. */
	bool CreateFile(const Text::StringView16 & path, CreateFileMode mode = CreateFileMode::Standard);
	/** Create directory of given path. */
	bool CreateDirectory(const Text::StringView16 & path, CreateDirectoryMode mode = CreateDirectoryMode::Standard);
	/** Remove file of given path. */
	bool RemoveFile(const Text::StringView16 & path, RemoveFileMode mode = RemoveFileMode::Standard);
	/** Remove directory of given path. */
	bool RemoveDirectory(const Text::StringView16 & path, RemoveDirectoryModes mode = RemoveDirectoryModes::Standard);
	/** Copy file. */
	bool CopyFile(const Text::StringView16 & from, const Text::StringView16 & to, CopyFileModes mode = CopyFileModes::Standard);
	/** Move dir or file. */
	bool Move(const Text::StringView16 & from, const Text::StringView16 & to, MoveModes mode = MoveModes::Standard);
	/** Change working directory. */
	bool Cd(const Text::StringView16 & path);
}
