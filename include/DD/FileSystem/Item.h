#pragma once
namespace DD::FileSystem {
	/** Fwd ref. */
	class Path;

	/** File system item - could be dir or file. */
	class Item;
}

#include <DD/Reference.h>
#include <DD/String.h>

namespace DD::FileSystem {
	class Item {
	public: // nested
		/** Referential object of filesystem. */
		struct Data
			: IReferential
		{
			/** Path to the item - does not contain name of the item. */
			StringLocal<260> Path;
			/** Name of the item - does not contain path to item. */
			StringLocal<64> Name;
			/**
			 * Attributes of the file or dir.
			 * 0x0800 - compressed
			 * 0x0001 - readonly
			 * 0x0002 - hidden
			 * 0x0004 - system
			 * 0x0010 - directory
			 * 0x0020 - archive
			 * 0x0100 - temp
			 */
			i64 Attributes;
			/**
			 * File times.
			 * Contains a 64-bit value representing the number
			 * of 100-nanosecond intervals since January 1, 1601 (UTC).
			 */
			i64 CreateTime, AccessTime, WriteTime;
			/**
			 * Access flags to file or dir.
			 * 02 - Read permission.
			 * 04 - Write permission.
			 */
			i32 Access;
			/** Number of bytes of the file. */
			size_t Size;
		};

	public: // properties
		/** File descriptor. @see Data. */
		Reference<Data> _data;

	protected: // methods
		/** Load fs description of given path. */
		void loadInstant(const Text::StringView16 & path);

	public: // methods
		/** Check if item exists. */
		bool Exists() const;
		/** Check if item is readable. */
		bool IsReadable() const;
		/** Check if item is writable. */
		bool IsWritable() const;
		/** Check if item is file. */
		bool IsFile() const;
		/** Check if item is directory. */
		bool IsDirectory() const;
		/** Check if item is system. */
		bool IsSystem() const;
		/** Check if item is hidden. */
		bool IsHidden() const;
		/** Return path (parent path - not including name of current item). */
		Text::StringView16 Path() const;
		/** Return name of fs item. */
		Text::StringView16 Name() const;
		/** Full path - dir + name. */
		FileSystem::Path FullPath() const;

	public: // constructors
		/** Zero constructor. */
		Item() = default;
	};
}