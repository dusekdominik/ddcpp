#pragma once
namespace DD::FileSystem {
	/** A structure for observing changes in given directory.*/
	struct Watchdog;
}

#include <DD/Array.h>
#include <DD/FileSystem/Path.h>
#include <DD/FileSystem/WatchdogRecord.h>
#include <DD/Memory/DeferredImpl.h>
#include <DD/Types.h>

namespace DD::FileSystem {
	struct Watchdog {
	public: // nested
		/** Internal representation. */
		struct InternalData;

	public: // statics
		/** Number of bytes for writing storage. */
		static constexpr size_t BufferSize = 8192;

	private: // properties
		/** Dir that is being observed. */
		Path _path;
		/** Buffer where changes are written. */
		alignas(4) u8 _buffer[BufferSize];
		/** System handle.  */
		Memory::DeferredImpl<InternalData, 8> _internal;

	public: // methods
		/** Start watching. */
		bool Start();
		/** Stop watching. */
		void Stop();
		/** Get changes */
		WatchdogRecordCollection GetRecords();

	public: // constructors
		/** Constructor. */
		Watchdog(const Path & path);
		/** Destructor */
		~Watchdog();
	};
}
