#pragma once
namespace DD::FileSystem {
	/**
	 * Singleton class, that observes directories
	 * When singleton instance is created it generates its own threads.
	 * Two threads are here for the processing.
	 *   MessageQueueProcessing - in this thread, there are processed callbacks and registration requests.
	 *   ReadingThread - reads changes from the file system, and copy them for further processing.
	 */
	struct WatchdogAsync;
}

#include <DD/Delegate.h>
#include <DD/FileSystem/Path.h>
#include <DD/FileSystem/WatchdogRecord.h>
#include <DD/Memory/DeferredImpl.h>
#include <DD/Singleton.h>

namespace DD::FileSystem {
	struct WatchdogAsync
		: public Singleton<WatchdogAsync>
	{
		/** Fwd declaration for internal implementation. */
		struct Implementation;
		/** Settings for single handler. */
		struct Settings;

		/** Interface for message implementation. */
		struct IMessage;
		/** Smart pointer message. */
		using Message = Reference<IMessage>;

		/** Structure for listening changes at the given directory. */
		struct IListener;
		/** Smart pointer. */
		using Listener = Reference<IListener>;

		/** Alias for delegates processing file changes. */
		using EventHandler = Delegate<const DD::FileSystem::Path &, const WatchdogRecord &>;

	public: // statics
		/** Maximal number of listeners. */
		static constexpr size_t ListenerCount = 62;

	protected: // properties
		/** Reserved memory for internal implementation. */
		Memory::DeferredImpl<Implementation, 1024> _impl;

	protected: // methods
		/** A tick of reading loop, @return false when processing should be aborted. */
		bool tickReadingThread();
		/** A tick of loop on the MessageProcessingThread, @return false when processing should be aborted. */
		bool tickMessageThread();
		/** Process listener adding on MessageProcessingThread.  */
		void addListener(Message message);
		/** Process listener removing on MessageProcessingThread.  */
		void removeListener(Message message);
		/** Process callbacks on MessageProcessingThread.  */
		void callbacks(Message message);
		/** Producers call - send message to the queue from any thread. */
		void sendMessage(Message message);
		/** Consumer call - receive a message on the MessageProcessingThread. */
		Message receiveMessage();

	public: // methods
		/** Start watching a folder - call from any thread.
		 * @param wait - wait until handler is added.
		 * @return
		 *   if @param wait == false, always false
		 *   if @param wait == true, return true if registration was successful.
		 */
		bool AddListenerAsync(const Settings & settigns, EventHandler handler, bool wait = true);
		/**
		 * End watching folder - call from any thread.
		 * @param wait - wait until handler is removed.
		 * * @return
		 *   if @param wait == false, always false
		 *   if @param wait == true, return true if unregistration was successful.
		 */
		bool RemoveListenerAsync(const Settings & settigns, EventHandler handler, bool wait = true);

	public:// constructors
		/** Constructor. */
		WatchdogAsync();
		/** Destructor. */
		~WatchdogAsync();
	};


	struct WatchdogAsync::Settings {
		/** Path to the observed directory. */
		FileSystem::Path Path;
		/** Show observe the filesystem subtree. */
		bool Recursive = true;
		/** Which changes are observed. */
		WatchdogFlags Flags = WatchdogFlags::_List;

		/** comparison. */
		bool operator==(const Settings & rhs) const { return Path == rhs.Path && Recursive == rhs.Recursive && Flags == rhs.Flags; };
	};


	struct WatchdogAsync::IMessage
		: IReferential
	{
		/** Type of the message */
		enum struct Types { Add, Remove, Callback, Exit, Undefined } Type = Types::Undefined;
		/** Was message processing successful. */
		bool Result = false;;

		/** Constructor. */
		IMessage(Types type) : IReferential(), Type(type) {};
	};
}
