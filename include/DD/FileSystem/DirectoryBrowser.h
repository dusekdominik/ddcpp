#pragma once
namespace DD::FileSystem {
	/** Objects for browsing directories. */
	class DirectoryBrowser;
}

#include <DD/ArrayView.h>
#include <DD/FileSystem/Directory.h>
#include <DD/FileSystem/File.h>
#include <DD/List.h>
#include <DD/Reference.h>
#include <DD/Text/StringView.h>

namespace DD::FileSystem {
	class DirectoryBrowser {
	public: // nested
		/** Stored data, referential. */
		struct Data
			: IReferential
		{
			/** List of subdirectories in the directory. */
			List<Directory, 32> Directories;
			/** List of files in directory. */
			List<File, 32> Files;
			/** Directory that is being browsed. */
			Directory Dir;
			/** Applied wildcard filter. */
			String Filter;
		};

	private: // properties
		/** Data storage. */
		Reference<Data> _data;

	public: // methods
		/** Subdirectories of directory. */
		ArrayView1D<Directory> Directories();
		/** Files of directory. */
		ArrayView1D<File> Files();
		/** Wildcard filter of directory browser. */
		Text::StringView16 Filter()  const;
		/** Reload directory. */
		void Reload(const Text::StringView16 & fiter);

	public: // constructors
		/** Basic constructor. */
		DirectoryBrowser(Directory dir, const Text::StringView16 & filter);
	};
}
