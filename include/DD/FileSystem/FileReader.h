#pragma once
namespace DD::FileSystem {
	/**
	 * Asynchronous file reader.
	 * Could be used for reading char by char or by batches.
	 * @example read char by char
	 *   for (wchar_t letter : DD::FileSystem::FileReader16("MyUtf16File.xml"))
	 *     doSomethingWithLetter(letter);
	 * @example read binary
	 *   DD::Array<DD::u8, 17> buffer;                      // create buffer
	 *   DD::FileSystem::FileReader8 reader("MyFile.bin");  // create reader
	 *   while (reader.Read(buffer.begin(), buffer.Size())	// read
	 *     doSomethingWithBuffer(buffer);                   // process
	 */
	template<typename T>
	struct FileReader;
}

#include <DD/FileSystem/FileChunkReader.h>
#include <DD/String.h>
#include <DD/Types.h>

namespace DD::FileSystem {
	/** Typedefs. */
	using FileReader64 = FileReader<u64>;
	using FileReader32 = FileReader<u32>;
	using FileReader16 = FileReader<u16>;
	using FileReader8 = FileReader<u8>;

	template<typename T>
	struct FileReader {
	protected: // properties
		/** Reader pointers. */
		union { T * Typed; i8 * Native; } _source, _end;
		/** File reader. */
		FileChunkReader _chunkReader;

	public: // sequencer
		T Get() { return *_source.Typed; }
		bool Next();
		bool Reset();

	public: // methods
		/** File path. */
		const String & Path() const { return _chunkReader.Path(); }
		/** File size - working after Reset(). */
		size_t Bytes() const { return _chunkReader.Bytes(); }
		/**
		 * Read n-items.
		 * @param target - pointer to memory, where data should be loaded.
		 * @param size - number of items (not necessarily bytes) to be read.
		 */
		bool Read(T * target, size_t size);
		/** Read n-bytes. */
		bool ReadBytes(ByteView target);
		/** Seek byte position (from beginning of the file). */
		bool Seek(size_t);

	public: // constructors
		/**
		 * Constructor.
		 * @param path  - string path to a file.
		 * @param chunk - byte size of file pieces to be loaded.
		 */
		FileReader(const String & path, size_t chunk = 64 * 1024)
			: _chunkReader(path, chunk)
		{}
	};
}




namespace DD::FileSystem {
	template<typename T>
	bool FileReader<T>::Reset() {
		// reset chunk reader
		bool result = _chunkReader.Reset();
		// assign local
		_source.Native = (i8 *)_chunkReader.Get().begin();
		_end.Native = (i8 *)_chunkReader.Get().end();

		return result;
	}


	template<typename T>
	bool FileReader<T>::Next() {
		// if data in current chunk
		if (++_source.Typed < _end.Typed)
			return true;
		// if data in next chunk
		if (_chunkReader.Next()) {
			_source.Native = (i8 *)_chunkReader.Get().begin();
			_end.Native = (i8 *)_chunkReader.Get().end();
			return true;
		}
		// otherwise nothing to read
		return false;
	}


	template<typename T>
	bool FileReader<T>::ReadBytes(ByteView target) {
		for (;;) {
			// number of items that can be read from current chunk
			size_t readSize = (_end.Native - _source.Native);
			// if it could be read by one read
			if (target.Length() <= readSize) {
				target.CopyIn(ArrayView(_source.Native, target.Length()));
				_source.Native += target.Length();
				return true;
			}
			// copy whole chunk content, and continue
			target.CopyIn(ArrayView(_source.Native, readSize));
			_source.Native += readSize;
			target = target.SubView(readSize);
			// get next chunk
			if (!_chunkReader.Next() && target.Length())
				return false;
			// assign new chunk
			_source.Native = (i8 *)_chunkReader.Get().begin();
			_end.Native = (i8 *)_chunkReader.Get().end();
		}
	}


	template<typename T>
	bool FileReader<T>::Read(T * target, size_t size) {
		return ReadBytes({ (char *)target, size * sizeof(T) });
	}


	template<typename T>
	bool FileReader<T>::Seek(size_t byte) {
		// seek chunk and index in chunk
		size_t chunk = byte / _chunkReader.Chunk();
		size_t index = byte % _chunkReader.Chunk();
		// if chunk exists
		if (_chunkReader.Seek(chunk)) {
			// select index in chunk
			_source.Native = (i8 *)_chunkReader.Get().begin() + byte;
			_end.Native = (i8 *)_chunkReader.Get().end();
			// check if offset is inside in chunk
			return _source.Native < _end.Native;
		}

		// no chunk exists
		return false;
	}
}
