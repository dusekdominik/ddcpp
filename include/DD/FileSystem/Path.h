#pragma once
namespace DD::FileSystem {
	/**
	 * Path.
	 * @example
	 *   Path path("C:/My/Path/File.txt");
	 *   if (!file.Exists()) {
	 *     path.CreateFile();
	 *     path.File().Write(byteBuffer, byteSize);
	 *   }
	 */
	class Path;
}

#include <DD/FileSystem/Functions.h>
#include <DD/FileSystem/Directory.h>
#include <DD/FileSystem/File.h>
#include <DD/String.h>

namespace DD::FileSystem {
	/************************************************************************/
	/* Path                                                                 */
	/************************************************************************/
	class Path {
	public: // statics
		/** Path to currently running exe file. */
		static Path Exe();
		/** Path to working directory of current process. */
		static Path WorkingDirectory();
		/** Path to user documents. */
		static Path Documents();
		/** Iterative approach, e.g. Pattern(L"MyFile_{0,4}.txt") . */
		static Path Pattern(Text::StringView16 pattern, size_t limit = 9999);
		/**
		 * Automatically create pattern.
		 * @example simple usage
		 *   DD::FileSystem::Path::PatternSimple(L"MyPng_", "png"); // -> "MyPng_{0,4}.png"
		 */
		static Path PatternSimple(Text::StringView16 prefix, Text::StringView16 suffix, size_t limit = 9999);

	private: // properties
		/** Locally allocated path string. */
		StringLocal<260> _path;

	public: // methods
		/** Is path existing file or directory. */
		bool Exists() const { return ::DD::FileSystem::Exists(_path); }
		/** Is the path of existing directory. */
		bool IsDirectory() const { return Exists() && ::DD::FileSystem::IsDirectory(_path); }
		/** Is the path of existing file. */
		bool IsFile() const { return Exists() && ::DD::FileSystem::IsFile(_path); }
		/** Is path initialized. */
		bool IsInvalid() const { return _path.IsEmpty(); }
		/** Complementary method to @see IsRelative(). */
		bool IsAbsolute() const { return !IsRelative(); }
		/** Check if the path is relative (it does not contain root). */
		bool IsRelative() const;

		/** Create directory if it is not already existing. */
		bool CreateDirectory() const { return (!Exists() && (Parent().Exists() || Parent().CreateDirectory()) && FileSystem::CreateDirectory(_path)); }
		/** Create file if it is not already existing. */
		bool CreateFile() const { return !Exists() && (Parent().Exists() || Parent().CreateDirectory()) && ::DD::FileSystem::CreateFile(_path); }
		/** Delete file or directory if exits. */
		bool Delete() const { return DeleteDirectory() || DeleteFile(); }
		/** Delete directory of exists. */
		bool DeleteDirectory() const { return IsDirectory() && ::DD::FileSystem::RemoveDirectory(_path); }
		/** Delete file if exists. */
		bool DeleteFile() const { return IsFile() && ::DD::FileSystem::RemoveFile(_path); }

		/** Directory access, the dir either must exist or can be successfully created. */
		Directory AsDirectory() const;
		/** File access, the file either must exist or can be successfully created. */
		File AsFile() const;
		/** Full path. */
		Path FullPath() const;
		/** Parent directory of path, @returns invalid path if parent is root. */;
		Path Parent() const;
		/** Infer a path from current one. */
		template<typename STRING_VIEW>
		Path Infer(const STRING_VIEW & suffix) const { Path copy(*this); copy._path.Append(suffix).Terminate(); return copy; }

		/** Path access. */
		operator Text::StringView16() const { return _path; }
		/** Path access. */
		Text::StringView16 ToString() const { return _path; }
		/** Get path as native string. */
		operator const wchar_t * () const { DD_FS_ASSERT(_path.IsNullTerminated()); return _path.begin(); }

	public: // constructors
		/** Use given text and try to store as a path string. */
		template<typename STRING_VIEW>
		Path(const STRING_VIEW & path) : _path(path) { if (!_path.IsNullTerminated()) _path.Terminate(); }
		/** Zero constructor. */
		Path() = default;
	};
}
