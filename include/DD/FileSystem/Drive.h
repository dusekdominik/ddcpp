#pragma once
namespace DD::FileSystem {
	/** Physical drive. */
	class Drive;
}

#include <DD/FileSystem/Functions.h>
#include <DD/FileSystem/Item.h>
#include <DD/FileSystem/Directory.h>
#include <DD/FileSystem/DirectoryBrowser.h>
#include <DD/List.h>
#include <DD/Text/StringView.h>
#include <DD/String.h>

namespace DD::FileSystem {
	/** List all drives in fs. */
	List<Drive> Drives();

	class Drive {
		friend List<Drive> FileSystem::Drives();

	public: // nested
		/** Data storage. */
		struct Data { String Name, Path, FileSystem; };

	private: // properties
		/** Pointer to the storage. */
		Reference<Data> _data;

	public:
		/** Return if drive exists. */
		bool Exists() const;
		/** FileSystem of drive. */
		Text::StringView16 FileSystem() const;
		/** Path of drive. */
		Text::StringView16 Path() const;
		/** Name of drive. */
		Text::StringView16 Name() const;
		/** Drive root directory. */
		Directory Root() const;

	public: // constructors
		/** Zero constructor - just for array init. */
		Drive() : _data() {}
		/** Constructor. */
		Drive(const String & path);
	};

}
