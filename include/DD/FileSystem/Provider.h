#pragma once
namespace DD::FileSystem {
	/** File provider with locally defined root. */
	struct Provider;
}

#include <DD/FileSystem/Path.h>
#include <DD/Com/IProvider.h>

namespace DD::FileSystem {
	struct Provider
		: Com::IProvider
	{
		/** Path to the root dir. */
		FileSystem::Path Root;

		/** Get info about loadable data. */
		Com::Handle * Produce(Text::StringView8 request) override;
		/** Load data. */
		void Dispose(Com::Handle * target) override;

		/** Constructor, */
		Provider(const Path & path) : Root(path){}
	};
}
