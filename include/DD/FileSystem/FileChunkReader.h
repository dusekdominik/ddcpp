#pragma once
namespace DD::FileSystem {
	/**
	 * Asynchronous reader of file chunks.
	 * Serves as auxiliary reader.
	 * Using double buffering technique.
	 * Back buffer is read from file while front buffer is read by a process.
	 * Chunks are filled in a single-thread thread pool to avoid mutual usage
	 *   of storage device.
	 */
	struct FileChunkReader;
}

#include <DD/Array.h>
#include <DD/String.h>

namespace DD::FileSystem {
	struct FileChunkReader {
	protected: // nested
		/** Auxiliary type - descriptor for _storeage property. */
		struct NativeView;

	protected: // statics
		/** Reserved space for struct data. */
		static constexpr size_t STORAGE_SIZE = 696;

	protected: // properties
		/** Storage for native data types.  */
		u8 _storage[STORAGE_SIZE];

	protected: // methods
		/** Parse to native view. */
		NativeView * nativeView() const { return (NativeView *)_storage; }

	public: // sequencer
		const Array<u8> & Get();
		bool Next();
		bool Reset();

	public: // methods
		/** File path. */
		const String & Path() const;
		/** File size - working after Reset(). */
		size_t Bytes() const;
		/** Get size of chunk in bytes. */
		size_t Chunk() const;
		/** Seek n-th chunk. */
		bool Seek(size_t);

	public: // constructors
		/** Constructor. */
		FileChunkReader(const String & path, size_t chunk);
		/** Copy constructor. */
		FileChunkReader(const FileChunkReader & copy);
		/** Destructor. */
		~FileChunkReader();
	};
}
