#pragma once
namespace DD {
	class StringTable;
}

#include <DD/Array.h>
#include <DD/Gpu/D2D.h>
#include <DD/String.h>

namespace DD {
	class StringTable {
	public: // typedefs & nested
		/** Cell alignment. */
		enum Alignment { Left = 'L', Center = 'C', Right = 'R' };
		/** Column parameters. */
		struct Column { Alignment Alignment = Left;  size_t Size = 0; String Name; };
		/** Function pointer. */
		typedef Gpu::D2D::Fonts(*formater_t)(size_t row, size_t col);
	private: // properties
		/** Table data. */
		Array2D<String> _table;
		/** Cols parameters definition. */
		Array<Column> _colParams;
		/** Dimensions. */
		size_t _cols, _rows;
		/** Pointer for adding strings into table. */
		size_t _ptr;
	public:
		Column & operator[](size_t i) { return _colParams[i]; }
		const Column & operator[](size_t i) const { return _colParams[i]; }
		StringTable Add(const String & str) {
			Column & col = _colParams[_ptr % _colParams.Length()];
			if (col.Size <= str.Length()) {
				col.Size = str.Length() + 1;
			}
			*(_table.Ptr() + _ptr++) = str;
			return *this;
		}
		StringTable Add(String && str) {
			Column & col = _colParams[_ptr % _colParams.Length()];
			if (col.Size <= str.Length()) {
				col.Size = str.Length() + 1;
			}
			*(_table.Ptr() + _ptr++) = str;
			return *this;
		}
		String ToString() const {
			size_t rowSize = 0;
			for (const Column & col : _colParams) {
				rowSize += col.Size;
			}
			String result(rowSize * _rows +  1);
			for (size_t iRow = 0; iRow < _rows; ++iRow) {
				for (size_t iCol = 0; iCol < _cols; ++iCol) {
					const String & cell = _table[iRow][iCol];
					const Column & col = _colParams[iCol];
					const size_t fill = col.Size - cell.Length();
					if (fill) switch (col.Alignment) {
					case Left:
						result.Append(cell).AppendFill(L' ', fill);
						break;
					case Center:
						result.AppendFill(L' ', fill / 2).Append(cell).AppendFill(L' ',  fill - (fill / 2));
						break;
					case Right:
						result.AppendFill(L' ', fill - 1).Append(cell).Append(L' ');
						break;
					}
				}
				result.Append(L'\n');
			}
			return result;
		}
		Gpu::D2D::Strings ToFormattedString(formater_t colorer) const {
			size_t index = 0;
			Gpu::D2D::Strings result(ToString());
			for (size_t iRow = 0; iRow < _rows; ++iRow) {
				for (size_t iCol = 0; iCol < _cols; ++iCol) {
					const Column & col = _colParams[iCol];
					result.Color(colorer(iRow, iCol), index, col.Size);
					index += col.Size;
				}
				index += 1;
			}
			return result;
		}

	public: // constructors
		/** Constructor. */
		StringTable(size_t rows, size_t cols)
			: _cols(cols)
			, _rows(rows)
			, _ptr(0)
			, _colParams(cols)
			, _table({ rows, cols })
		{}
	};
}