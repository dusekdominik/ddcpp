#pragma once
namespace DD::Simulation {
	/**
	 * Model for rendering squares.
	 * Square are oriented orthogonally to the camera.
	 */
	class QuadModel;
}

#include <DD/Gpu/PixelShader.h>
#include <DD/Gpu/StructuredBuffer.h>
#include <DD/Gpu/VertexShader.h>
#include <DD/Reference.h>
#include <DD/Simulation/Camera.h>
#include <DD/Simulation/ICullableModel.h>
#include <DD/Simulation/QuadVertex.h>

namespace DD::Simulation {
	class QuadModel
		: public ICullableModel
	{
	public: // statics
		static Camera * _CAMERA;
		/** Initialize static */
		static void InitStatic(Gpu::Device & device);
		/** Constant buffer. */
		static Gpu::ConstantBuffer<QuadConstantBuffer> & QBuffer();

	private: // instance properties
		/** Handle to device. */
		Gpu::Device _device;
		/** Structured buffer for the points. */
		Gpu::StructuredBuffer<QuadVertex> _points;
		/** Pixel shader. */
		Reference<Gpu::PixelShader> _ps;
		/** Vertex shader. */
		Reference<Gpu::VertexShader> _vs;
		/** Size of point. */
		f32 _pointSize;
		/** Coefficient of LoD reduction. */
		f32 _lodReduction;

	private: // methods
		/** Compute reduction based on distance. */
		void setLoDReduction();

	protected: // IModel
		virtual const char * iModelName() const override { return "QuadModel"; }
		virtual void iModelInit(Gpu::Device & device) override;
		virtual void iModelReinit(Gpu::Device & device) override;
		virtual void iModelDraw(DrawArgs & args) override;

	public: // accession
		/** Iterator. */
		auto begin() { return _points.begin(); }
		/** Iterator. */
		auto end() { return _points.end(); }
		/** Iterator. */
		auto begin() const { return _points.begin(); }
		/** Iterator. */
		auto end() const { return _points.end(); }
		/** Number of stored vertices. */
		size_t Count() const { return _points.Size(); }
		/** Indexer. */
		QuadVertex & operator[](size_t i) { return _points[i]; }
		/** Const indexer. */
		const QuadVertex & operator[](size_t i) const { return _points[i]; }

	public: // methods
		/** Add vertex. */
		QuadModel & Add(const float3 & x, Color c, i32 e = 0) { return Add({x, c, e}); }
		/** Add vertex. */
		QuadModel & Add(const QuadVertex & qv) { _points.Add(qv); return *this; }
		/** Get point size. */
		f32 PointSize() const { return _pointSize; }
		/** Set point size, */
		QuadModel & PointSize(f32 size) { _pointSize = size; return *this; }
		/** Reserve space for given number of vertices.*/
		QuadModel & Reserve(size_t size) { _points.Reserve(size); return *this; }

	public: // constructors
		/** Basic constructor. */
		QuadModel(f32 pointSize = 1.f);
		/** Construct quad model from raw data. */
		QuadModel(QuadVertex * data, size_t count, f32 pointSize = 0.5f);
		/** Destructor. */
		virtual ~QuadModel() override {}
	};
}
