#pragma once
namespace DD::Simulation {
	/**
	 * Holder of point cloud based on simple oct-tree grid.
	 *
	 * Point coordinates x, y, z ~ latitude, height, longitude
	 * Grid coordinates x, y, z ~ latitude, longitude, height
	 *
	 * PointCloud pointCloud(min, max, cell);
	 */
	struct PointCloud;
}

#include <DD/Array.h>
#include <DD/IProgress.h>
#include <DD/Polygon.h>
#include <DD/Primitives.h>
#include <DD/List.h>
#include <DD/Reference.h>
#include <DD/Unique.h>
#include <DD/Simulation/QuadVertex.h>
#include <DD/Vector.h>

namespace DD::Simulation {
	struct PointCloudBase
		: public IReferential
	{
		friend struct PointCloud;

	private: // nested
		struct CellSequencerBase;
		template<typename, typename>
		struct CellSequencer;

	public: // nested
		/** Class representing one cell of the grid. */
		struct Cell : List<QuadVertex> {};
		/** Convex2D area. */
		typedef Polygon<f32> cvx_t;
		/** Sequencers for selecting cells by given AABox, or selecting  whole area. */
		using CellSequencerMutable = CellSequencer<Cell, PointCloudBase>;
		using CellSequencerConst = CellSequencer<const Cell, const PointCloudBase>;

	public: // statics
		/** Unfilled value of height profiles. */
		static constexpr f32 UNFILLED = -3.402823466e+38F;

	private: // properties
		/** Number of points stored in grid. */
		size_t _pointCount;
		/** Locks of point cloud. Restricts functionality. */
		bool _gridLock, _pointLock;
		/** Area of point cloud grid. */
		AABox3F _gridArea;
		/** Dimensions of grid. */
		float3 _gridRange;
		/** Definition of grid cells. */
		float3 _cellDimensions, _cellCount;
		/** Offset added to each inserted point. */
		float3 _offset;
		/** Grid storage. */
		Array3D<Cell> _grid;

	private: // methods
		/** Converts 3D position to indices in float form. */
		float3 positionToIndices(const float3 & position) const;

	public: // getters
		/** Getter of single grid-cell dimensions. */
		const float3 & CellDimensions() const { return _cellDimensions; }
		/** Getter if whole pt grid. */
		const Array3D<Cell> & Grid() const { return _grid; }
		/** Getter of grid area aabb description. */
		const AABox3F & Area() const { return _gridArea; }
		/** Getter of grid dimensions (axis-aligned). */
		const float3 & GridRange() const { return _gridRange; }
		/** Getter of offset. */
		const float3 & Offset() const { return _offset; }
		/** Getter if whole pt grid. */
		Array3D<Cell> & Grid() { return _grid; }
		/** Surface of grid. */
		f32 GridSurface() const { return _gridRange.x * _gridRange.z; }
		/** Point surface recommended by point cloud properties. */
		f32 RecommendedPointSurface() const { return GridSurface() / (_pointCount * 16); }
		/** Point size recommended by point cloud properties. */
		f32 RecommendedPointSize() const { return 0.75f * Math::Sqrt(RecommendedPointSurface()); }
		/** Number of points. */
		size_t Size() const { return _pointCount; }

	public: // setters
		/** Setter of single grid-cell dimensions. */
		void CellDimensions(const float3 & cell) { if (!_gridLock) _cellDimensions = cell; }
		/** Setter of grid maximum (axis-aligned). */
		void Area(const AABox3F & area) { if (!_gridLock) _gridArea = area, _gridRange = area.Size(); }
		/** Setter of offset. */
		void Offset(const float3 & off) { _offset = off; }

	public: // methods
		/** Add point to point cloud - returns false, if point is out of defined grid. */
		bool AddPoint(const QuadVertex & vertex);
		/** Add noise, noise is between [-x/2, +x/2]. */
		void AddNoise(f32 x, f32 y, f32 z);
		/** Add noise. */
		void AddNoise(f32 n) { AddNoise(n, n, n); }
		/** Init point cloud - you should have already defined dims of grid and cell. */
		void InitGrid();
		/** Change grid properties of area. If point cloud is already attached to a model, re-grid process fails. */
		PointCloud RegridArea(const AABox3F & area, const float3 & cell, IProgress * progress = 0) const;
		/** Change grid properties of area. If point cloud is already attached to a model, re-grid process fails. */
		PointCloud RegridArea(const float3 & cell, IProgress * progress = 0) const;
		/** Check if a position is inside the defined grid. */
		bool IsPointInGrid(const float3 & position) const { return _gridArea.Contains(position); }
		/** Select cells of given AABB area. */
		Array<Cell *> SelectCells(const AABox3F & area);
		/** Select points of given AABB area. */
		Array<QuadVertex *> SelectPoints(const AABox3F & area);
		/** Select points of given convex area. */
		Array<QuadVertex *> SelectPoints(const Polygon<f32> & profile);
		/** Select points of given convex area. */
		Array<QuadVertex *> SelectPoints(const Polygon<f32> & profile, f32 minHeight, f32 maxHeight);
		/** Remove points in given selection. */
		void RemovePoints(const Polygon<f32> & profile);
		/** Create height map of defined granularity. */
		Array2D<f32> CreateHeightMap(size_t w, size_t h);
		/** Create default height map - granularity is defined implicitly by grid. */
		Array2D<f32> CreateHeightMap();
		/** Create height profile between given points. Size of array defines granularity. */
		void CreateHeightProfile(float2 begin, float2 end, f32 width, Array<f32> & store);
		/** Create height profile between given points. */
		Array<f32> CreateHeightProfile(float2 begin, float2 end, f32 width, size_t granularity);
		/** Create normalized (0, 1) height profile between given points. Size of array defines granularity. */
		void CreateNormalizedHeightProfile(float2 begin, float2 end, f32 width, Array<f32> & store);
		/** Create normalized (0, 1) height profile between given points. */
		Array<f32> CreateNormalizedHeightProfile(float2 begin, float2 end, f32 width, size_t granularity);
		/** Create height slice of point cloud in selected area. Result is also point cloud. */
		PointCloud CreateHeightSlice(const cvx_t & area, f32 heightBegin, f32 heightEnd);
		/** Create height slice of whole point cloud. Result is also point cloud. */
		PointCloud CreateHeightSlice(f32 heightBegin, f32 heightEnd);
		/** Swap content of two point clouds. */
		void Swap(PointCloudBase & ptCloud);
		/** Locks manipulation with points. */
		void LockPoints() { _pointLock = true; }
		/**
		 * Recount number of points - if point clouds are directly accessed,
		 * it could be made some mess in the count of the points.
		 * This call should fix it.
		 */
		void RecountPoints();

	public: // sequencers
		/** Select cells of given AABB area. */
		auto SelectCellsSeq(const AABox3F & area) const;
		/** Select cells of given AABB area. */
		auto SelectCellsSeq(const AABox3F & area);
		/** Select points of given AABB area. */
		auto SelectPointsSeq(const AABox3F & area);
		/** Select points all points in point-cloud as sequence. */
		auto SelectPointsSeq();
		/** Select points of given AABB area. */
		auto SelectPointsSeq(const AABox3F & area) const;
		/** Select points all points in point-cloud as sequence. */
		auto SelectPointsSeq() const;

	private: // constructors
		/** Zero constructor. */
		PointCloudBase();
		/** Single cell constructor - whole cloud is created as single grid - cell. */
		PointCloudBase(const AABox3F & area);
		/** Standard constructor - grid is initialized after construction. */
		PointCloudBase(const AABox3F & area, const float3 & cell);
		/** Single cell point cloud from list of vertices. */
		PointCloudBase(const ArrayView1D<QuadVertex *> & vertices);
		/** Single cell point cloud from list of another point clouds. */
		PointCloudBase(const ArrayView1D<const PointCloudBase*> & clouds);
	};


	struct PointCloudBase::CellSequencerBase {
		/** Descriptors of iteration in all three dimensions. */
		struct { size_t Iterator, Begin, End; } X, Y, Z;
		/** Number of cells in selection. */
		size_t Size;

	protected: // ISequencer
		bool next();
		bool reset();

	public: // constructors
		/** Constructor selecting a specified area. */
		CellSequencerBase(const PointCloudBase * source, const AABox3F & area);
		/** Constructor selecting whole point-cloud area. */
		CellSequencerBase(const PointCloudBase * source);
		/** Copy constructor. */
		CellSequencerBase(const CellSequencerBase &) = default;
	};


	template<typename CELL, typename POINT_CLOUD>
	struct PointCloudBase::CellSequencer
		: public CellSequencerBase
	{
		/** Source cell data. */
		POINT_CLOUD * Source;

	public: // sequencer interface
		CELL & Get() { return Source->_grid[{Z.Iterator, Y.Iterator, X.Iterator }]; }
		bool Next() { return next(); }
		bool Reset() { return reset(); }

	public: // constructors
		/** Copy constructor. */
		CellSequencer(const CellSequencer &) = default;
		/** Constructor. */
		CellSequencer(POINT_CLOUD * source)
			: CellSequencerBase(source)
			, Source(source)
		{}
		/** Constructor. */
		CellSequencer(POINT_CLOUD * source, const AABox3F & area)
			: CellSequencerBase(source, area)
			, Source(source)
		{}
	};


	struct PointCloud
		: public Reference<PointCloudBase>
	{
		/** Parent class. */
		using Base = Reference<PointCloudBase>;
		/** Alias. */
		typedef typename PointCloudBase::Cell Cell;

		/** Nullptr constructor. */
		PointCloud() : Reference() {}
		/** Single cell constructor - whole cloud is created as single grid - cell. */
		PointCloud(const AABox3F & area);
		/** Standard constructor - grid is initialized after construction. */
		PointCloud(const AABox3F & area, const float3 & cell);
		/** Single cell point cloud from list of vertices. */
		PointCloud(const ArrayView1D<QuadVertex *> & vertices);
		/** Single cell point cloud from list of another point clouds. */
		PointCloud(const ArrayView1D<const PointCloudBase*> & clouds);
	};
}


namespace DD::Simulation {
	inline auto PointCloudBase::SelectCellsSeq(const AABox3F & area) {
		return Sequencer::Drive(CellSequencerMutable(this, area));
	}


	inline auto PointCloudBase::SelectPointsSeq(const AABox3F & area) {
		return Sequencer::Drive(CellSequencerMutable(this, area)).Unroll().Filter(
			[area](const QuadVertex & vertex) { return area.Contains(vertex.Position); }
		);
	}


	inline auto PointCloudBase::SelectPointsSeq() {
		return Sequencer::Drive(CellSequencerMutable(this)).Unroll();
	}


	inline auto PointCloudBase::SelectCellsSeq(const AABox3F & area) const {
		return Sequencer::Drive(CellSequencerConst(this, area));
	}


	inline auto PointCloudBase::SelectPointsSeq(const AABox3F & area) const {
		return Sequencer::Drive(CellSequencerConst(this, area)).Unroll().Filter(
			[area](const QuadVertex & vertex) { return area.Contains(vertex.Position); }
		);
	}


	inline auto PointCloudBase::SelectPointsSeq() const {
		return Sequencer::Drive(CellSequencerConst(this)).Unroll();
	}
}