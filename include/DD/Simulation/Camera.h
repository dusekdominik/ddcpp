#pragma once
namespace DD::Simulation {
	/**
	 * Camera.
	 * Struct holds constant buffer information.
	 * Camera is designed for horizontal moving.
	 * For constant buffer definition, @see CameraConstantBuffer.
	 *
	 * @example common control
	 *   Camera camera;
	 *   camera.ForwardXZ(-1.f); // move backward
	 *   camera.ForwardXZ(1.f);  // move forward
	 *   camera.RightXZ(-1.f);   // move left
	 *   camera.RightXZ(1.f);    // move right
	 *   camera.TopXZ(-1.f);     // move up
	 *   camera.TopXZ(1.f);      // move down
	 *   camera.RotateX(0.1f);   // rotate right
	 *   camera.RotateX(-0.1f);  // rotate left
	 *   camera.RotateY(0.1f);   // rotate up
	 *   camera.RotateY(-0.1f);  // rotate down
	 *
	 * @example engine frame
	 *   if (firstRun)
	 *     camera.Init(myDevice); // if constant buffer is not initialized, init it
	 *   camera.FrameUpdate();    // update matrices for shader computing
	 *   camera.Bind();           // bind constant buffer to its register for shader access
	 */
	class Camera;
}

#include <DD/Concurrency/ThreadSafeObject.h>
#include <DD/Gpu/ConstantBuffer.h>
#include <DD/Primitives.h>
#include <DD/Properties.h>
#include <DD/Simulation/ConstantBuffers.h>
#include <DD/Types.h>
#include <DD/Units/Units.h>

namespace DD::Simulation {
	class Camera
		: public DD::Gpu::ConstantBuffer<CameraConstantBuffer>
	{
	public: // nested
		using PositionF = Math::CommonVector<Units::Length_MeterF, 3>;
		/** Projection types. */
		enum Projections { Orthographic, Perspective };
		/** Default constants for camera. */
		struct Constants {
			/** Default up vector. */
			static constexpr float3 Up = float3(0.f, 1.f, 0.f);
			/** Default position vector. */
			static constexpr float3 Position = 0.f;
			/** Default direction vector. */
			static constexpr float3 Direction = float3(1.f, 0.f, 0.f);
			/** Default near plane distance. */
			static constexpr f32 Near = 0.01f;
			/** Default far plane distance. */
			static constexpr f32 Far = 100000.f;
			/** Default values for tangent fov. */
			static constexpr float2 TgFov = { 1920.f / 1080.f, 1.0f };
			/** Default projection. */
			static constexpr Projections Projection = Perspective;
		};

		/** Properties defining camera state. */
		struct State {
			/** Where camera is. */
			float3 Position;
			/** Where to camera looks. */
			float3 Direction;
			/** Where is camera top (rotation). */
			float3 UpVector;
			/** Beginning and end of the camera space. */
			f32 Near, Far;
			/** Field of view in tangents (at one meter distance we see TgFov.x X tgFov.y square meters area). */
			float2 TgFov;
			/** Type. */
			Projections Projection = Perspective;
		};

	protected: // properties
		/** Camera state. */
		Concurrency::ThreadSafeObject<State> _state;
		/** View transformation. */
		matrix _view;
		/** Projection transformation. */
		matrix _projection;
		/** Premultiplied view and projection transformations. */
		matrix _viewProjection;

	public: // properties
		DD_PROPERTIES(
			PUBLIC(_state->Direction, Direction),
			PUBLIC(_state->Position, Position),
			PUBLIC(_state->UpVector, UpVector),
			PUBLIC(_state->Near, Near),
			PUBLIC(_state->Far, Far),
			PUBLIC(_state->TgFov, TgFov)
			//PUBLIC(_state->Projection, Projection)
		);

	public: // methods
		/**
		 * Create camera for frustum culling based on camera properties.
		 * @warning relatively expensive.
		 */
		Frustum3F CreateFrustum() const;
		/** Update view-projection matrix. */
		void FrameUpdate();
		/**
		 * Matrix of camera view-projection.
		 * Premultiplied matrix making camera-space
		 *   transformation and perspective projection.
		 */
		const matrix & ViewProjectionTransformation() const { return _viewProjection; }
		/**
		 * Matrix of camera view transformation.
		 * Consists from aside, up, direction and position vectors.
		 * Converts vectors to camera space.
		 */
		const matrix & ViewTransformation() const { return _view; }
		/** Get cam state. */
		State CameraState() { return *_state; }
		/** Set cam state. */
		Camera & CameraState(const State & value) { _state = value; return *this; }

	public: // methods - camera control
		/**
		 * Direction in horizontal way.
		 * @warning direction is not normalized.
		 */
		PositionF DirectionXZ() const;
		/**
		 * Move camera by given delta vector.
		 * @param gain - gain is delta in meters [m].
		 */
		Camera & Move(const float3 & v);
		/**
		 * Move camera in direction of fwd vector.
		 * @param gain - optimized for keyboard input.
		 *             - gain is delta in meters [m].
		 */
		Camera & ForwardXZ(Units::Length_MeterF gain);
		/**
		 * Move camera in direction of right vector.
		 * @param gain - optimized for keyboard input.
		 *             - gain is delta in meters [m].
		 */
		Camera & RightXZ(Units::Length_MeterF gain);
		/**
		 * Move camera in direction of up vector.
		 * @param gain - optimized for keyboard input.
		 *             - gain is delta in meters [m].
		 */
		Camera & TopXZ(Units::Length_MeterF gain);
		/**
		 * Rotate camera around up vector.
		 * Moving head left and right.
		 * @param gain - optimized for x-coordinate delta of mouse.
		 *             - gain is delta in radians.
		 */
		Camera & RotateX(Units::PlaneAngle_RadianF gain);
		/**
		 * Rotate camera around aside vector.
		 * Moving head up and down.
		 * @param gain - optimized for y-coordinate delta of mouse.
		 *             - gain is delta in radians.
		 */
		Camera & RotateY(Units::PlaneAngle_RadianF gain);

	public:
		/** Zero constructor. */
		Camera();
	};
}
