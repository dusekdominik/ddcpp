#pragma once
namespace DD::Simulation {
	/**
	 * ICullableModel
	 * Partial implementation of IModel.
	 * Frustum culling is based on bounding sphere.
	 */
	class ICullableModel;
}

#include <DD/Boolean.h>
#include <DD/Simulation/IModel.h>

namespace DD::Simulation {
	class ICullableModel
		: public IModel
	{
	private: // nested
		/** Debugging interface of ICullableModel. */
		struct Debug {
			/** Draw flags. */
			Boolean DrawSpheres = 0, DrawBoxes = 0;
			/** Wireframe flags. */
			Boolean WireSpheres = 0, WireBoxes = 0;
			/** Method for visualization bounding objects. */
			void Draw(Gpu::Device &);
		};

	public: // statics
		/** Returns new identification of ICullableModel. */
		static u32 GetNewId() { static u32 _id = 0; return _id++; }
		/** Debug tools for visualization of spheres. */
		static Debug DebugTools;

	protected: // properties
		/** Identification of ICullableModel. */
		u32 _id;
		/** Bounding sphere of model. */
		Sphere3F _boundingSphere;
		/** Bounding box of model. */
		AABox3F _boundingBox;

	protected: // virtual interface
		Intersetion iModelIntersects(const Frustum3F &, const float4x4 &) const override;

	public: // methods
		/** Getter of model bounding sphere. */
		const Sphere3F & GetBoundingSphere() const { return _boundingSphere; }
		/** Getter of model axis aligned bounding box. */
		const AABox3F & GetBoundingBox() const { return _boundingBox; }

	public: // constructors
		/** Constructor. */
		ICullableModel() : _id(GetNewId()) {}
		/** Destructor. */
		virtual ~ICullableModel() override = default;
	};
}
