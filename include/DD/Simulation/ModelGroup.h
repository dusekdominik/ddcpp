#pragma once
namespace DD::Simulation {
	/**
	 * List of IModel.
	 * Models are served as single model.
	 */
	class ModelGroup;

	/**
	 * List of ICullableModel.
	 * Group contains its own bounding volumes.
	 */
	class IntersectableModelGroup;
}

#include <DD/Gpu/Devices.h>
#include <DD/List.h>
#include <DD/Reference.h>
#include <DD/Simulation/ICullableModel.h>
#include <DD/Simulation/IModel.h>

namespace DD::Simulation {
	class ModelGroup
		: public IModel
	{
	public: // typedefs
		/** Reference to a model. */
		typedef Reference<IModel> model_r;
		/** List of models. */
		typedef List<model_r> models_t;

	private: // properties
		/** [Obsolete] Device for model initialization, for registration models after group initialization. */
		Gpu::Device _device;
		/** List of models. */
		models_t _models;
		/** Switch if rendering enabled. */
		bool _enabled;

	protected: // Inherited via IModel
		virtual const char * iModelName() const override { return "ModelGroup"; }
		virtual bool iModelEnabled() const override { return _enabled; }
		virtual void iModelInit(Gpu::Device & device) override;
		virtual void iModelDraw(DrawArgs & args) override;
		virtual void iModelDrawCulled(const Frustum3F & f, const float4x4 & transform, DrawArgs & args) override;

	public: // accession
		/** Iterator. */
		auto begin() { return _models.begin(); }
		/** Iterator. */
		auto end() { return _models.end(); }
		/** Iterator. */
		auto begin() const { return _models.begin(); }
		/** Iterator. */
		auto end() const { return _models.end(); }

	public: // methods
		/** Enabling/disabling of model group. */
		void SetEnabled(bool value) { _enabled = value; }
		/** Add model into group. */
		void Add(model_r model);
		/** Clear storage of models. */
		void Clear();

	public: // constructors
		/** Zero constructor. */
		ModelGroup();
		/** Destructor. */
		virtual ~ModelGroup() override = default;
	};


	class IntersectableModelGroup
		: public ICullableModel
	{
	public: // typedefs
		/** Reference to a model. */
		typedef Reference<ICullableModel> model_r;
		/** List of models. */
		typedef List<model_r> models_t;

	protected: // properties
		/** [Obsolete] Device for model initialization, for registration models after group initialization. */
		Gpu::Device _device;
		/** Already initialized models. */
		models_t _models;
		/** Models to be initialized*/
		models_t _init;
		/** Switch if rendering is enabled. */
		bool _enabled;
		/** Switch for reinitialization of all the contained models. */
		bool _reinit;

	protected: // Inherited via IIntersectableModel
		virtual const char * iModelName() const override { return "IntersectableModelGroup"; }
		virtual bool iModelEnabled() const override { return _enabled; }
		virtual void iModelInit(Gpu::Device & device) override;
		virtual void iModelDraw(DrawArgs & args) override;
		virtual void iModelDrawCulled(const Frustum3F & f, const float4x4 & t, DrawArgs & args) override;
		virtual void iModelReinit(Gpu::Device &) override;

	public: // methods
		/** Add model into group. */
		void Add(model_r model, bool intialized = false);
		/** Check if group is empty. */
		bool IsEmpty() const;
		/** Set reinit - it will be done while next calling of Init or Draw. */
		void Reinit();
		/** Enabling/disabling of model group. */
		void SetEnabled(bool value);

	public: // constructor
		/** Zero constructor. */
		IntersectableModelGroup();
		/** Destructor. */
		virtual ~IntersectableModelGroup() override = default;
	};
}
