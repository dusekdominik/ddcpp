#pragma once
namespace DD::Simulation {
	/**
	 * Graphic engine based of directX 11
	 * Basically holds constant buffers, and DX objects
	 *   and processing input.
	 */
	class Engine;
}

#include <DD/Boolean.h>
#include <DD/Concurrency/Awaitable.h>
#include <DD/Concurrency/Atomic.h>
#include <DD/Concurrency/ThreadPool.h>
#include <DD/Controls/Window.h>
#include <DD/Debug.h>
#include <DD/Gpu/Devices.h>
#include <DD/Gpu/D2D.h>
#include <DD/IProgress.h>
#include <DD/Input/KeyboardEventProcessor.h>
#include <DD/Lambda.h>
#include <DD/List.h>
#include <DD/NotifyWrapper.h>
#include <DD/Object.h>
#include <DD/Properties.h>
#include <DD/Reference.h>
#include <DD/Stopwatch.h>
#include <DD/Simulation/Camera.h>
#include <DD/Simulation/ISprite.h>
#include <DD/Simulation/IMessage.h>
#include <DD/Simulation/StatisticSprite.h>
#include <DD/String.h>
#include <DD/Unique.h>
#include <DD/Units/Unit.h>

namespace DD::Simulation {
	class Engine {
	public: // nested
		/** Common message with countdown. */
		struct ITimeMessage;
		/** Basic kind of message (time based). */
		struct BasicMessage;
		/** Action bound on particular key shortcut. */
		struct KeyboardAction { Lambda<void(Engine&)> action; Input::KeyShortcut key; DD::Units::Time_MicroSecondULL time; String name; };
		/** Abstract requirements for engine dialogs. */
		struct IDialog;
		/** Common dialog type. */
		using Dialog = Reference<IDialog>;
		/** Base for simple dialogs. */
		struct DialogSimpleBase;
		/**
		 * Referential implementation of simple dialogs.
		 * @example common usage
		 *   // define variables you want used in dialog
		 *   DD::f32 myNumber1 = 42.f, myNumber2 = -42.f;
		 *   // define items for dialog - use appropriate type, fill label and tooltip
		 *   DD::Simulation::Engine::DialogSimple::Float ditem1(myNumber1, "My Number 1", "My explanation what about my number");
		 *   DD::Simulation::Engine::DialogSimple::Float ditem2(myNumber2, "My Number 2", "My explanation what about my number");
		 *   // define dialog by main title and its items
		 *   DD::Simulation::Engine::DialogSimple dlg("My numbers", myNumber1Dlg, myNumber2Dlg);
		 *   // use engine to ask dialog, and if dialog is confirmed, do your job...
		 *   if (engine.RegisterDialogAndWait(dlg))
		 *     printMyNumbers(ditem1, ditem2);
		 */
		struct DialogSimple;

		/** Mode of camera rendering (flag like modes). */
		DD_ENUM_32(
			(CameraModes, "Mode of camera visualization."),
			(COLOR,       _CAMERA_FLAG_COLOR,       "Color rendering."),
			(DEPTH,       _CAMERA_FLAG_DEPTH,       "Depth visualization via color scale rendering."),
			(HEIGHT,      _CAMERA_FLAG_HEIGHT,      "Heigh visualization via color scale rendering."),
			(HEIGHT_TUNE, _CAMERA_FLAG_HEIGHT_TUNE, "Visualization of the parts, that are out of height scale.")
		);
		/** Modes of mouse. */
		DD_ENUM_32(
			(MouseModes, "Modes of mouse access to camera orientation."),
			(ATTACHED,          0, "Camera is moving always while mouse is moving."),
			(REVERSED_ATTACHED, 1, "Reversed ATTACHED."),
			(CLICK,             2, "Camera is moving only if the left-mouse-button is pressed."),
			(REVERSED_CLICK,    3, "Reversed CLICK."),
			(NONE,              4, "Mouse cannot move camera.")
		);
		/** Visualization modes. */
		DD_ENUM_32(
			(BoundingVisualizations, "Types of visualization of bounding volumes."),
			(NONE,      0, "No visualization."),
			(SOLID,     1, "Use solid volumes for visualization."),
			(WIREFRAME, 2, "Use just wireframe of volumes for visualization.")
		);
		/** Methods for manipulation with objects in object register. */
		DD_ENUM_32(
			(ObjectFunctions, "Basic manipulation with engine objects."),
			(Select, 1),
			(Release,                2, "Remove the object from the engine."),
			(Hide,                   4, "Make the object invisible."),
			(Show,                   8, "Make the object visible."),
			(Blink,                 16, "Make the object blinking."),
			(CameraPPP, 32 | 4 | 2 | 1, "Move camera to the AABox corner (+++) of the object and observe it."),
			(CameraPPN, 32 | 4 | 2 | 0, "Move camera to the AABox corner (++-) of the object and observe it."),
			(CameraPNP, 32 | 4 | 0 | 1, "Move camera to the AABox corner (+-+) of the object and observe it."),
			(CameraPNN, 32 | 4 | 0 | 0, "Move camera to the AABox corner (+--) of the object and observe it."),
			(CameraNPP, 32 | 0 | 2 | 1, "Move camera to the AABox corner (-++) of the object and observe it."),
			(CameraNPN, 32 | 0 | 2 | 0, "Move camera to the AABox corner (-+-) of the object and observe it."),
			(CameraNNP, 32 | 0 | 0 | 1, "Move camera to the AABox corner (--+) of the object and observe it."),
			(CameraNNN, 32 | 0 | 0 | 0, "Move camera to the AABox corner (---) of the object and observe it."),
			(Effect,                64, "Tooltip")
		);
		/** Methods for manipulation with sprites in sprite register. */
		DD_ENUM_32(
			(SpriteFunctions, "Basic manipulation with engine sprites."),
			(Select,  0),
			(Enable,  1, "Show sprite."),
			(Disable, 2, "Hide sprite.")
		);
		/** Item of object register. */
		struct ObjectEx { Debug::Enum<ObjectFunctions> Functions; Object Object; };
		/** */
		struct SpriteEx { Debug::Enum<SpriteFunctions> Functions; Sprite Sprite; };
		/** Struct holding interface for asynchronous initialization of models. */
		struct ModelEx { IModel::AsyncInitInterface * Interface; Concurrency::ConditionVariable * Variable; };

	private: // properties
		/** */
		Debug::Button _clearObjectRegister;
		/** Time measurement. */
		Stopwatch _time, _frameWatch;
		/** Forced rendering switches. */
		Debug::Boolean _forcedRendering;
		/** Boolean if main window is classified as focused. */
		bool _focused, _lastFrameFocused;
		/** Switch for turning engine off */
		bool _enabled;
		/** Handle mouse and keyboard actions. */
		bool _processInput = true;
		/** Boolean for internal engine messages. */
		Debug::Boolean _msgCamera, _msgDevice, _msgMT;
		/** FPS measurement. */
		Array<f32, 600> _fpsHistory;
		/** Elapsed microseconds of last 600 frames.*/
		Array<f32, 600> _frameHistory;
		/** Microseconds since between engine start and frame beginning. */
		Units::Time_MicroSecondULL _fpsTime;
		Units::Time_MicroSecondULL _renderTime;
		Units::Time_MicroSecondULL _renderTimeGap;
		Units::Time_MicroSecondULL _frameTime;
		Units::Time_MicroSecondULL _frameTimeMeasure;
		Units::Time_MicroSecondULL _fpsLongest;
		Units::Time_MicroSecondULL _fpsLongestRecord;
		u64 _frameCountMeasure;
		Debug::u32 _frameCount;
		/** Time of last processing of camera control from keyboard input. */
		Units::Time_MicroSecondULL _keyboardCameraTime;
		/** FPS measurement. */
		Debug::f32 _fps;
		/** Seconds since engine has started. */
		Debug::f32 _engineTime;
		/** Thread poll for processing async action, e.g. KeyAction. */
		Concurrency::ThreadPool _pool;
		/** Camera frustum for frustum culling. */
		Frustum3F _frustum;
		/** Frustum culling with frustum at a fixed position. */
		Debug::Boolean _frustumCullingFix;
		/** Frustum culling enabled/disabled. */
		Debug::Boolean _frustumCulling;
		/** Keyboard object. */
		Input::Keyboard _keyboard;
		/** Keyboard processor for handling camera input and invoking KeyDown, KeyUp... events. */
		Input::KeyboardEventProcessor _keyboardProcessor;
		/** Data for mouse reading. */
		Debug::i32 _mouseX, _mouseY;
		/** Helper for mouse control. */
		bool _mouseMoveEnabled;
		/** Sensitivity of mouse. */
		Debug::Range<f32> _mouseSensitivity, _keyboardSensitivity;
		/** Mode of mouse behavior. */
		Debug::Enum<MouseModes> _mouseMode;
		/** Visualization  of bounding volumes. */
		Debug::Enum<BoundingVisualizations> _bboxVisual, _bsphereVisual;
		/** Mode of camera visualization. */
		Debug::Enum<CameraModes> _cameraMode;
		/** Camera height visualization settings. */
		Debug::f32 _cameraHeightMin, _cameraHeightMax;
		/** Camera height visualization settings. */
		Debug::Boolean _cameraHeightManual;
		/** Device frame settings. */
		struct {
			Debug::Boolean AlphaBlending;
			Debug::Boolean DepthBuffer;
			Debug::Boolean VSync;
			Debug::Boolean WireFrame;
			Debug::Enum<Gpu::Devices::MSAA> MultiSampling;
		} _deviceSettings;
		/** Manipulation with all the objects at once. */
		DD::Debug::Enum<ObjectFunctions> _objectFunctions;
		/** 2d Objects for drawing. */
		Concurrency::ThreadSafeObject<List<SpriteEx, 64>> _sprites;
		Concurrency::ThreadSafeObject<List<ISprite *, 8>, Concurrency::AtomicLock> _spriteRemove;
		/** Expiration handlers. */
		Delegate<ISprite *> _spriteExpirationDelegate;
		Delegate<ObjectBase *> _objectExpirationDelegate;
		/** Objects (instantiated models) for drawing. */
		Concurrency::ThreadSafeObject<List<ObjectEx, 1024>> _objects;
		/** Objects that were not initialized yet, after initialization they moved to @see _objects. */
		Concurrency::ThreadSafeObject<List<ObjectEx, 1024>> _objectsToInit;
		/** Objects that expired and have not been removed yet, removing is not done each frame. */
		Concurrency::ThreadSafeObject<List<Object, 128>, Concurrency::AtomicLock> _objectsExpired;
		/** Models being asynchronously initialized. */
		Concurrency::ThreadSafeObject<Queue<ModelEx, 8>> _modelsToAsyncInit;
		/** Text messages displayed. */
		Concurrency::ThreadSafeObject<List<Message>> _messages;
		/** Registered async action invokers. */
		List<KeyboardAction> _keyboardActions;
		/** Number of highlighted objects. */
		Concurrency::Atomic::i32 _numberOfHighlightedObjects;
		/** Sync actions. */
		Concurrency::ThreadSafeObject<List<Action<Engine &>>> _syncActions;
		/** Action along with number of frame, when it should be triggered. */
		struct TickTask {
			/** Frame number. */
			u64 Tick;
			/** Action to be processed */
			Action<Engine &> Action;
			/** Heap[ comparer. */
			constexpr bool operator<(const TickTask & rhs) const { return Tick < rhs.Tick; }
		};
		/** Tasks to be processed. */
		Concurrency::ThreadSafeObject<MinHeap<TickTask>> _delayedTasks;
		/** Holder for async tasks. */
		Concurrency::ThreadPool::TaskList<64> _asyncTasks;

	public:
		/** 3D device holder. */
		Gpu::Device _d3d;
		/** 2D device holder. */
		Unique<Gpu::D2D> _d2d;
		/** Info about text rendering. */
		struct {
			/** Is text-block enabled. */
			Debug::Boolean Enabled;
			/** Transparency of rendered text. */
			Debug::Range<u8> Alpha;
			/** Background of rendered text. */
			Debug::Enum<Colors> Background;
			/** Text alignment. */
			Debug::Enum<Gpu::D2D::THAligns> TAlign;
			/** Block alignment. */
			Debug::Enum<Gpu::D2D::HAligns> HAlign;
			/** Block alignment. */
			Debug::Enum<Gpu::D2D::VAligns> VAlign;
			/** Font size of rendered text. */
			Debug::Range<f32> Scale;
			/** Buffer for engine messaging. */
			Gpu::D2D::Strings Buffer;
		} _messaging, _console;
		/** Info about statistics. */
		struct {
			/** Is displayed. */
			Debug::Enum<StatisticSprite::Modes> Mode;
			/** Dimensions of the statistics. */
			Debug::f32Range SizeX, SizeY;
			/** Position of the statistic. */
			Debug::f32Range OffsetX, OffsetY;
			/** Sample count. */
			Debug::u32Range Samples;
		} _fpsStatistics, _frameStatistics;
		/** Number of items on console browser page. */
		Debug::i32Range _consolePageSize;
		/** Basic background. */
		Debug::Enum<Colors> _background;
		/** Window. */
		Controls::Window & _window;
		/**  */
		List<Gpu::Devices::Screenshot *> _screenshots, _screenshotsOnly3D;
		/** Camera object - holding constantVS buffers. */
		Camera _camera;
		/** Engine name. */
		StringLocal<32> _name;
		/** Device statistics. */
		struct {
			Debug::u32 Bytes, CBBytes, IBBytes, VBBytes, SBBytes, RWSBBytes,
				CBCount, IBCount, VBCount, SBCount, RWSBCount,
				CBInits, IBInits, VBInits, SBInits, RWSBInits,
				DrawCalls, Lines, Points, Triangles;
		} _statistics;
		/** Devices. */
		Debug::Options _adapters;

	public: // Events
		/** Called when an object is registered. */
		Event<Object> OnObjectRegistration;
		/** Call when an object is being unregistered from engine. */
		Event<Object> OnObjectUnregistration;
		/** Called when object buffer is cleared. */
		Event<> OnObjectRegisterClearing;
		/** Called while destruction of engine object. */
		Event<> OnEngineClosed;
		/** Invoked if mouse mode changed. */
		Event<Engine&, MouseModes> OnMouseModeChanged;

	private: // methods
		/** Blur texture to the RenderTarget. */
		void gauss2RT(Gpu::Texture &);
		/** Draw aura effects. */
		void aura2RT(Gpu::Texture &, Gpu::Texture &);
		/** Init _d3d. */
		void establishD3D();
		/** Init _d2d. */
		void establishD2D();
		/** Process input influencing the camera object. */
		void processKeyboardCamera();
		/** Process all keyboard actions. */
		void processKeyboard();
		/** Process messages to be printed on screen. */
		void processTextMessages();
		/** Print all maintained text. */
		void processText();
		/** Process whole input - done in separate thread. */
		bool processInput();
		/** Update camera constant buffer before rendering. */
		void cameraUpdate();
		/** Draw directly to render target. */
		void directDraw(bool);
		/** Draw effects with swapping textures to draw aura effect. */
		void effectDraw();
		/** Draw 3D objects. */
		void draw3D();
		/** Draw text. */
		void draw2D();

	public: // visual
		/** Clear all 3D objects. */
		void ClearObjectRegister();
		/** Asynchronously initialize given model. */
		void InitModelAsync(IModel * model, IProgress * progress = 0);
		/** Get object register copy. */
		Array<Object> ObjectRegisterState();
		/**
		 * Register object via model.
		 * @param model    - model which should be instantiated.
		 * @param name     - name of the object.
		 * @param async    - should be model initialized directly or asynchronously.
		 *                 - false - model is initialized as fast as possible.
		 *                 - true  - background model initialization is not blocking rendering.
		 * @param progress - observation if param async is set to true.
		 * @return reference to the instance.
		 */
		Object RegisterObject(Model model, Text::StringView16 name, bool async = false, IProgress * progress = 0);
		/** Register sprite.  */
		void RegisterSprite(Sprite sprite);
		/** Remove object from the engine. */
		void UnregisterObject(Object o);

	public: // misc
		/** Ignore or accept input, serves for suppressing key strokes when another process should process them. */
		void ProcessInputEnabled(bool enabled) { _processInput = enabled; }
		/** Get keyboard object. */
		const Input::Keyboard & GetKeyboard() { return _keyboard; }
		/** Get time when last rendering begin. */
		const DD::Units::Time_MicroSecondULL & GetRenderTime() const { return _renderTime; }
		/** Microseconds since engine has started. */
		const Units::Time_MilliSecondULL GetTime() const { return _time.ElapsedMicroseconds(); }
		/** Get keyboard actions. */
		const List<KeyboardAction> & GetKeyboardActions() const { return _keyboardActions; }
		/** Close engine. */
		void Exit();
		/** Synchronous tasks processed in main threads, will be removed after their processed. */
		void RegisterSyncAction(Action<Engine &> action) { _syncActions->Add(Fwd(action)); }
		/** Process action with a given delay in frames. */
		void RegisterTickAction(size_t delay, Action<Engine &> action) { _delayedTasks->Push({ _frameCount + delay, Fwd(action) }); }
		/** Set async action of engine in format of [](Engine&){...} which can be invoked by given keyboard shortcut. */
		void RegisterKeyboardAction(Text::StringView16 name, Input::KeyShortcut key, Lambda<void(Engine&)> action);
		/** */
		void Wait(size_t n = 1) { for (size_t s = _frameCount + n; _frameCount < s;); }

	public:// engine specific
		/** Get Camera object. */
		Camera & GetCamera() { return _camera; }
		/** Mouse mode getter. */
		MouseModes MouseMode() const { return _mouseMode; }
		Engine & MouseMode(MouseModes mode);
		/** Rendering of objects' bboxes. */
		BoundingVisualizations VisualizationBBox() const { return _bboxVisual; }
		Engine & VisualizationBBox(BoundingVisualizations v) { _bboxVisual = v; return *this; }
		/** Rendering of objects' bspheres. */
		BoundingVisualizations VisualizationBSpheres() const { return _bsphereVisual; }
		Engine & VisualizationBSpheres(BoundingVisualizations v) { _bsphereVisual = v; return *this; }
		/** Engine would be forced to render even if window is not focused. */
		bool ForcedRendering() const { return _forcedRendering; }
		Engine & ForcedRendering(Boolean::Value b) { _forcedRendering = b; return *this; }
		/** Check whether frustum culling is turned on.. */
		bool FixedFrustumCulling() const { return _frustumCullingFix; }
		Engine & FixedFrustumCulling(Boolean::Value b) { _frustumCullingFix = b; return *this; }
		/** Check whether frustum culling is done with fixed frustum. */
		bool FrustumCulling() const { return _frustumCulling; }
		Engine & FrustumCulling(Boolean::Value b) { _frustumCulling = b; return *this; }
		/** background of rendering. */
		Colors Background() const { return _background; }
		Engine & Background(Colors c) { _background = c; return *this; }

	public: // device control
		/** Get d3d device. */
		Gpu::Devices::AsyncInterface GetDevice3D() { return _d3d->Async(); }
		/** Enable/disable rendering of transparent colors. */
		bool DeviceAlphaBlending() const { return _deviceSettings.AlphaBlending; }
		Engine & DeviceAlphaBlending(Boolean::Value b) { _deviceSettings.AlphaBlending = b; return *this; }
		/** Enable/disable z-ordered rendering. */
		bool DeviceDepthBuffer() const { return _deviceSettings.DepthBuffer; }
		Engine & DeviceDepthBuffer(Boolean::Value b) { _deviceSettings.DepthBuffer = b; return *this; }
		/** Enable/Disable VSync. */
		bool DeviceVSync() const { return _deviceSettings.VSync; }
		Engine & DeviceVSync(Boolean::Value b) { _deviceSettings.VSync = b; return *this; }
		/** Set rendering of solid/wireframed models. */
		bool DeviceWireframe() const { return _deviceSettings.WireFrame; }
		Engine & DeviceWireframe(Boolean::Value b) { _deviceSettings.WireFrame = b; return *this; }
		/** Waits until rendering is done, then captures screenshot. */
		Gpu::Devices::Screenshot CaptureScreenshot(bool only3d = false);
		using ScreenshotAsync = Concurrency::ThreadPool::Future<Gpu::Devices::Screenshot>;
		ScreenshotAsync CaptureScreenshotAsync() { return [this]() { return CaptureScreenshot(); }; }

	public: // sprites
		/** Enable/disable message with brief info about device resources. */
		bool ShowDeviceStatistic() const { return _msgDevice; }
		Engine & ShowDeviceStatistic(Boolean::Value b) { _msgDevice = b; return *this; }
		/** Enable/disable message with info about camera xyz and fps. */
		bool ShowCameraPosition() const { return _msgCamera; }
		Engine & ShowCameraPosition(Boolean::Value b) { _msgCamera = b; return *this; }
		/** Enable/disable message with info about multithreading. */
		bool ShowMTInfo() const { return _msgMT; }
		Engine & ShowMTInfo(Boolean::Value b) { _msgMT = b; return *this; }
		/** Enable disable console browser. */
		bool Console() const { return _console.Enabled; }
		Engine & Console(Boolean::Value v) { _console.Enabled = v; return *this; }
		/**
		 * Register time based message.
		 * @param str  - formatted string to be printed.
		 * @param time - duration of message display in milliseconds.
		 */
		Reference<Engine::BasicMessage> RegisterMessage(const Gpu::D2D::Strings & str, Units::Time_MilliSecondULL time = 5000);
		/** Register custom message. */
		template<typename...ARGS>
		Reference<Engine::BasicMessage> RegisterMessage(const char * format, const ARGS & ... args) { return RegisterMessage(Gpu::D2D::Strings(StringLocal<256>::Format(format, args...).View())); }
		/** Register custom message. */
		bool RegisterMessage(Message message);
		/** Register progress bar into engine. */
		IProgress * RegisterProgressBar(const Text::StringView16 & name);
		/** Register grid with sprites for rendering statistics. */
		void EnableProfileScopeStatistics();

	public: // deprecated
		/** Register dialog, @return false if dialog has been already registered.. */
		bool RegisterDialog(Dialog dialog);
		/** Registers dialog, waits until dialog is over, returns if dialog was successful. */
		bool RegisterDialogAndWait(Dialog dialog);

	public:
		/** Draw one frame - should be done in main thread.  Returns if engine is still valid. */
		bool Draw();

	public:
		/** Constructor. Window, pixel dimensions. */
		Engine(const String & engineName, Controls::Window & window);
		/** Destructor. */
		~Engine() { Exit(); }
	};


	struct Engine::ITimeMessage
		: public IMessage
	{
		/** Time countdown (microseconds). */
		Units::Time_MicroSecondULL Time;

	protected: // IMessage
		virtual bool iMessageEnabled() const override { return Units::Time_MicroSecondULL(0) < Time; }
		virtual bool iMessageEnabled(Units::Time_MicroSecondULL delta) override;

	public: // constructors
		/** Milliseconds. */
		ITimeMessage(Units::Time_MilliSecondULL time) : Time(time) {}
	};


	struct Engine::BasicMessage
		: public Engine::ITimeMessage
	{
		Gpu::D2D::Strings _text;

	protected: // IMessage Text
		virtual Gpu::D2D::Strings iMessageGetText() const override { return _text; }

	public:
		/** */
		Gpu::D2D::Strings & Text() { return _text; }
		/** Milliseconds. */
		BasicMessage(const Gpu::D2D::Strings & text, Units::Time_MilliSecondULL time);
		/** Milliseconds. */
		BasicMessage(const Text::StringView16 & text, Units::Time_MilliSecondULL time) : ITimeMessage(time), _text(text) {}
	};


	struct Engine::IDialog
		: public virtual IReferential
		, public IMessage
		, public Input::KeyboardEventProcessor::IKeyboardEvent
	{
	protected: // IDialog
		virtual void iDialogReset() = 0;
		virtual bool iDialogSuccessfull() const = 0;

	public: // methods
		/** Reset dialog result, to be neither submitted nor canceled. */
		void Reset() { iDialogReset(); }
		/** Block thread until dialog is canceled or submitted. */
		void Wait();
		/** Return true if dialog was submitted (not canceled). */
		bool Successfull() { return iDialogSuccessfull(); }
	};


	struct Engine::DialogSimpleBase
		: public Engine::IDialog
	{
	public: // constants
		/** The cursor visualization in text dialog. */
		constexpr static wchar_t CURSOR = L'_';

	public: // nested
		/** Basic operations for dialog item. */
		struct IItem;
		/** Common integer functionality for dialog item. */
		struct IntegerBase;
		/** Common float functionality for dialog item. */
		struct FloatBase;
		/** Common select functionality for dialog item. */
		struct SelectBase;
		/** Implementation of integers as dialog item. */
		template<typename T>
		struct IntegerAlias;
		/** Implementation of floats as dialog item. */
		template<typename T>
		struct FloatAlias;
		/** Select dialog item. */
		template<typename T, typename ALIAS = T>
		struct SelectAlias;
		/** Referential item. */
		using Item = Reference<IItem>;
		/** Referential float dialog item. */
		template<typename T>
		struct Float;
		/** Referential float integer item. */
		template<typename T>
		struct Integer;
		/** Referential float enum item. */
		template<typename T, typename ALIAS>
		struct Select;

	private: // properties
		/** Flags for dialog. */
		bool _enabled, _successful;
		/** Index of focused item. */
		size_t _focus;
		/** Main title of the dialog. */
		DD::String _question;
		/** All dialog items. */
		Concurrency::ThreadSafeObject<List<Item>> _items;
		/** Message to be built and message already built. */
		Concurrency::ThreadSafeObject<Gpu::D2D::StringsLocal<512>> _copy, _msg;
		/** Serialization. */
		DD_PROPERTIES(
			PRIVATE(_items, DialogItems),
			PRIVATE(_focus, FocusedItem)
		);

	private:
		/** Paste cursor into a item. */
		void fixCursor(int cursor);
		/** Make text from dialog. */
		void build();
		/** Import with file dialog. */
		void exportXmlFile();
		/** Export to given path */
		void exportXmlFile(const String & path);
		/** Import from given path. */
		void importXmlFile(const String & path);
		/** Import with file dialog. */
		void importXmlFile();

	protected: // Inherited via IDialog
		virtual void iMessageFeed(Gpu::D2D::Strings & target) const override { target.Append((Gpu::D2D::Strings &)**_msg); }
		virtual bool iMessageEnabled(Units::Time_MicroSecondULL) override { return _enabled; }
		virtual bool iMessageEnabled() const override { return _enabled; }
		virtual bool iKeyboardEventIsEnabled() const override { return _enabled; }
		virtual void iKeyboardEventProcess(Input::Key key, Input::KeyModifiers modifiers) override;
		virtual void iDialogReset() override;
		virtual bool iDialogSuccessfull() const override { return _successful; }

	public:
		/** Add item to dialog. */
		void Add(Item item);
		/** Enable dialog item. */
		void Enable(size_t i);
		/** Disable dialog item. */
		void Disable(size_t i);
		/** Remove item from dialog. */
		void RemoveAt(size_t i);

	public: // constructors
		/** Constructor. */
		DialogSimpleBase(const DD::String & question, const Array<Item> & items);
		/** Generic constructor, arguments must be of Dialog::Item type. */
		template<typename...ARGS>
		DialogSimpleBase(const DD::String & question, ARGS...args)
			: DialogSimpleBase(question, static_cast<const Array<Item>&>(Arrayize<Item>(args...)))
		{}
	};


	struct Engine::DialogSimpleBase::IItem
		: public IReferential
	{
		friend struct Engine::DialogSimpleBase;
		typedef Concurrency::ThreadSafeObject<StringLocal<256>> tooltip_t;
		typedef Concurrency::ThreadSafeObject<StringLocal<128>> label_t;
		typedef Concurrency::ThreadSafeObject<StringLocal<128>> input_t;
		typedef Concurrency::ThreadSafeObject<Gpu::D2D::StringsLocal<128>> formattedInput_t;

	protected: // properties
		/** Item flags. */
		bool _focused, _enabled;
		/** MT safe string with a tooltip. */
		tooltip_t _tooltip;
		/** MT safe string with a label. */
		label_t _label;
		/** MT safe string with an input. */
		input_t _input;
		/** MT safe string with a formatted input. */
		formattedInput_t _formattedInput;
		/** Serialization. */
		DD_PROPERTIES(
			PRIVATE(_input.GetProxy().Object(), Value),
			PRIVATE(_enabled, Enabled),
			PRIVATE(_focused, Focused)
		);

	public:
		/** Focus getter. */
		bool Focused() const { return _focused; }
		/** Focus setter. */
		IItem & Focused(bool value) { _focused = value; iItemBuild(); return *this; }
		/** Enabled getter. */
		bool Enabled() const { return _enabled; }
		/** Enabled setter. */
		IItem & Enabled(bool value) { _enabled = value; return *this; }

	public: // IItem interface
		/** Parse keyboard input. */
		virtual void iItemProcess(Input::Key key) = 0;
		/** Generates formatted output. */
		virtual void iItemBuild() = 0;

	public: // constructor
		/** Constructor, requires filled common info about item. */
		IItem(const String & label, const String & tooltip, const String & input);
		/** Virtual destructor. */
		virtual ~IItem() {}
	};


	struct Engine::DialogSimpleBase::FloatBase
		: Engine::DialogSimpleBase::IItem
	{
	protected: // properties
		/** Index of cursor. */
		size_t _cursor;
		/** Value parsed from the input. */
		DD::f64 _value;

	protected: // IItem
		virtual void iItemBuild() override;
		virtual void iItemProcess(Input::Key key) override;

	public: // constructors
		/** Constructor. */
		FloatBase(const DD::String & label, const DD::String & tooltip, const DD::f64 & value = 0);
		/** Destructor. */
		virtual ~FloatBase() override = default;
	};


	template<typename T>
	struct Engine::DialogSimpleBase::FloatAlias
		: public FloatBase {
	protected: // properties
		/** Storage of original value. */
		T & _alias;

	protected: // IItem
		virtual void iItemBuild() override { FloatBase::iItemBuild();  _alias = (T)_value; }

	public: // constructors
		/** Constructor. */
		FloatAlias(T & alias, const DD::String & label, const DD::String & tooltip)
			: FloatBase(label, tooltip, alias), _alias(alias)
		{}
		/** Destructor. */
		virtual ~FloatAlias() override = default;
	};


	struct Engine::DialogSimpleBase::IntegerBase
		: Engine::DialogSimpleBase::IItem
	{
	protected: // properties
		/** Index of cursor. */
		size_t _cursor;
		/** Value parsed from the input. */
		i64 _value;

	protected: // IItem
		virtual void iItemBuild() override;
		virtual void iItemProcess(Input::Key key) override;

	public: // constructors
		/** Constructor. */
		IntegerBase(const DD::String & label, const DD::String & tooltip, const i64 & value = 0);
		/** Destructor. */
		virtual ~IntegerBase() override = default;
	};


	template<typename T>
	struct Engine::DialogSimpleBase::IntegerAlias
		: public IntegerBase
	{
	protected: // properties
		/** Storage of original value. */
		T & _alias;

	protected: // IItem
		virtual void iItemBuild() override { IntegerBase::iItemBuild();  _alias = (T)_value; }

	public: // constructors
		/**
		 * Constructor.
		 * @param alias   - reference to variable into which will be dialog item parsed.
		 * @param label   - name of the dialog item.
		 * @param tooltip - hint to dialog item (expected larger explanation than in name),
		 */
		IntegerAlias(T & alias, const DD::String & label, const DD::String & tooltip)
			: IntegerBase(label, tooltip, static_cast<i64>(alias)), _alias(alias)
		{}
		/** Destructor. */
		virtual ~IntegerAlias() {}
	};


	struct Engine::DialogSimpleBase::SelectBase
		: public Engine::DialogSimpleBase::IItem
	{
		typedef StringLocal<32> string_t;
		typedef Array<string_t> options_t;

	protected: // properties
		/** Options of the selection. */
		options_t _options;
		/** Index of selected item. */
		size_t _selected;
		/** Delimiter between the items. */
		string_t _delimiter;

	protected: // Inherited via Item
		virtual void iItemProcess(Input::Key key) override;
		virtual void iItemBuild() override;

	public: // constructors
		/** Constructor. */
		SelectBase(size_t selected, const DD::String & label, const DD::String & tooltip, options_t && options);
		/** Generic constructor. */
		template<typename...ARGS>
		SelectBase(size_t selected, const DD::String & label, const DD::String & tooltip, ARGS...args)
			: SelectBase(selected, label, tooltip, options_t::From(args...))
		{}
		/** Destructor. */
		virtual ~SelectBase() override = default;
	};


	template<typename T, typename ALIAS>
	struct Engine::DialogSimpleBase::SelectAlias
		: public SelectBase
	{
	public: // statics
		/** Safe cast from size_t. */
		template<typename U>
		static U Cast(size_t u) { return (T)u; }
		/** Safe cast from size_t. */
		template<>
		static bool Cast(size_t u) { return u ? true : false; }

	private: // properties
		/** Storage of original value. */
		ALIAS & _alias;

	protected: // IItem
		virtual void iItemBuild() override { SelectBase::iItemBuild();  _alias = Cast<T>(_selected); }

	public: // constructors
		/**
		 * Constructor.
		 * @param alias   - reference to variable into which will be dialog item parsed.
		 * @param label   - name of the dialog item.
		 * @param tooltip - hint to dialog item (expected larger explanation than in name),
		 * @param args    - names of the items in selection.
		 */
		template<typename...ARGS>
		SelectAlias(ALIAS & alias, const DD::String & label, const DD::String & tooltip, const ARGS &...options)
			: SelectBase((size_t)alias, label, tooltip, options...)
			, _alias(alias)
		{}
		/**
		 * Constructor.
		 * @param alias   - reference to variable into which will be dialog item parsed.
		 * @param label   - name of the dialog item.
		 * @param tooltip - hint to dialog item (expected larger explanation than in name),
		 * @param options - names of the items in selection.
		 */
		template<typename...ARGS>
		SelectAlias(ALIAS & alias, const DD::String & label, const DD::String & tooltip, Array<StringLocal<32>> && options)
			: SelectBase((size_t)alias, label, tooltip, Fwd(options))
			, _alias(alias)
		{}
		/** Destructor. */
		virtual ~SelectAlias() override = default;
	};


	template<typename T>
	struct Engine::DialogSimpleBase::Float
		: public Reference<Engine::DialogSimpleBase::FloatAlias<T>>
	{
		using Base = Reference<Engine::DialogSimpleBase::FloatAlias<T>>;
	public: // constructors
		/** Nullptr constructor. */
		Float() : Base() {}
		/**
		 * Constructor.
		 * @param alias   - reference to variable into which will be dialog item parsed.
		 * @param label   - name of the dialog item.
		 * @param tooltip - hint to dialog item (expected larger explanation than in name),
		 */
		Float(T & alias, const String label, const String & tooltip)
			: Base(new FloatAlias<T>(alias, label, tooltip))
		{}
	};


	template<typename T>
	struct Engine::DialogSimpleBase::Integer
		: public Reference<Engine::DialogSimpleBase::IntegerAlias<T>>
	{
		using Base = Reference<Engine::DialogSimpleBase::IntegerAlias<T>>;

	public: // constructors
		/** Nullptr constructor. */
		Integer() : Base() {}
		/**
		 * Constructor.
		 * @param alias - reference to variable into which will be dialog item parsed.
		 * @param label - name of the dialog item.
		 * @param tooltip - hint to dialog item(expected larger explanation than in name),
		 */
		Integer(T & alias, const String label, const String & tooltip)
			: Base(new IntegerAlias<T>(alias, label, tooltip))
		{}
	};


	template<typename T, typename ALIAS = T>
	struct Engine::DialogSimpleBase::Select
		: public Reference<Engine::DialogSimpleBase::SelectAlias<T, ALIAS>>
	{
		using Base = Reference<Engine::DialogSimpleBase::SelectAlias<T, ALIAS>>;

	public: // constructors
		/** Nullptr constructor. */
		Select() : Base() {}
		/**
		 * Constructor.
		 * @param alias   - reference to variable into which will be dialog item parsed.
		 * @param label   - name of the dialog item.
		 * @param tooltip - hint to dialog item (expected larger explanation than in name),
		 * @param options - names of the items in selection (expected strings).
		 */
		template<typename...OPTIONS>
		Select(ALIAS & alias, const String label, const String & tooltip, const OPTIONS &...options)
			: Base(new SelectAlias<T, ALIAS>(alias, label, tooltip, options...))
		{}
		/**
		 * Constructor.
		 * @param alias   - reference to variable into which will be dialog item parsed.
		 * @param label   - name of the dialog item.
		 * @param tooltip - hint to dialog item (expected larger explanation than in name),
		 * @param options - names of the items in selection (expected strings).
		 */
		Select(ALIAS & alias, const String label, const String & tooltip, Array<StringLocal<32>> && options)
			: Base(new SelectAlias<T, ALIAS>(alias, label, tooltip, Fwd(options)))
		{}
	};


	struct Engine::DialogSimple : public Reference<Engine::DialogSimpleBase> {
	private:
		/** Parent class. */
		using Base = Reference<Engine::DialogSimpleBase>;

	public:
		/** Item of dialog (referential). */
		using Item = DialogSimpleBase::Item;
		/** 32-bit float dialog item (referential). */
		using Float = DialogSimpleBase::Float<float>;
		/** 64-bit float dialog item (referential). */
		using Double = DialogSimpleBase::Float<double>;
		/** size_t dialog item (referential). */
		using SizeT = DialogSimpleBase::Integer<size_t>;
		/** Select. */
		template<typename T, typename ALIAS = T>
		using Select = DialogSimpleBase::Select<T, ALIAS>;

	public: // constructors
		/** Constructor. */
		DialogSimple() : Base() {}
		/**
		 * Constructor.
		 * @param question - title text of the dialog.
		 * @param args     - items of the dialog.
		 */
		template<typename...ARGS>
		DialogSimple(const String question, ARGS...args)
			: Base(new DialogSimpleBase(question, args...))
		{}
	};
}
