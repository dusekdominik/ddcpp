#pragma once
namespace DD {
	/** Forward declaration. */
	enum Gpu::IIndexBufferIndices  : char;

	namespace Simulation {
		/** Interface for all the line models. */
		class ILineModel;

		/** Class defining common functionality of line model. */
		template<typename VERTEX, Gpu::IIndexBufferIndices INDEX>
		class LineModelBase;

		/** Model based on line primitive. */
		template<typename VERTEX, Gpu::IIndexBufferIndices INDEX>
		class LineModel;
	}
}

#include <DD/Gpu/Devices.h>
#include <DD/Gpu/IndexBuffer.h>
#include <DD/Gpu/VertexBuffer.h>
#include <DD/Simulation/ColorVertex.h>
#include <DD/Simulation/ICullableModel.h>

#include <DD/Debug.h>

namespace DD::Simulation {
	class ILineModel : public ICullableModel {};

	/************************************************************************/
	/* Unscent models                                                       */
	/************************************************************************/
	template<typename VERTEX, Gpu::IIndexBufferIndices INDEX>
	class LineModelBase
		: public ILineModel
	{
	public: // typedefs
		/** Helper typedef for deriving triangle type. */
		typedef Gpu::IndexBufferPrimitiveTraits<Gpu::IIndexBufferPrimitives::Lines, INDEX> traits_t;
		/** Line type. */
		typedef typename traits_t::primitive_t line_t;
		/** Index type. */
		typedef typename traits_t::index_t index_t;

	public: // properties
		Gpu::Device _device;
		/** Vertices of the model. */
		Gpu::VertexBuffer<VERTEX> Vertices;
		/** Index buffer mapping lines to vertices. */
		Gpu::IndexBuffer<Gpu::IIndexBufferPrimitives::Lines, INDEX> Lines;
		/** Pixel shader of the model. */
		Reference<Gpu::PixelShader> PixelShader;
		/** Vertex shader of the model. */
		Reference<Gpu::VertexShader> VertexShader;

	protected: // IModel
		virtual void iModelInit(Gpu::Device & device) override;
		virtual void iModelDraw(DrawArgs & args) override;

	public: // methods
		/** Add another triangle model into this one. */
		void AddLineModel(const LineModelBase & m);
		/** Add a line. */
		void AddLine(const VERTEX & a, const VERTEX & b);
		/** Add a polyline. */
		void AddLineStrip(const ArrayView1D<const VERTEX> & vertex);
		/** Check if model has any content. */
		bool IsEmpty() { return Vertices.Size() == 0 || Lines.Size() == 0; }
	};

	template<typename VERTEX, Gpu::IIndexBufferIndices INDEX = Gpu::IIndexBufferIndices::_16Bit>
	class LineModel
		: public LineModelBase<VERTEX, INDEX>
	{
	protected: // IModel
		virtual const char * iModelName() const override { return typeid(*this).name(); }

	public: // constructors
		/** Destructor. */
		virtual ~LineModel() override = default;
	};

	/************************************************************************/
	/* ColorVertex based model                                              */
	/************************************************************************/
	template<Gpu::IIndexBufferIndices INDEX>
	struct LineModel<ColorVertex, INDEX>
		: public LineModelBase<ColorVertex, INDEX>
	{
		using Base = LineModelBase<ColorVertex, INDEX>;
		using index_t = typename Base::index_t;
		using line_t = typename Base::line_t;

	protected: // IModel
		virtual const char * iModelName() const override { return typeid(*this).name(); }

	public: // methods
		using Base::AddLineStrip;
		/** Add axis-aligned bounding box. */
		void AddAABB(const float3 & min, const float3 & max, const Color & color = Color::Collection::RED);
		/** Add line defined by two vertices. */
		void AddLine(const ColorVertex & a, const ColorVertex & b) { Base::AddLine(a, b); }
		/** Add line defined by two positions and a color. */
		void AddLine(const float3 & a, const float3 & b, const Color & color = Color::Collection::RED);
		/** Add Y-aligned polyhedron. */
		void AddPolyhedron(f32 minY, f32 maxY, const ArrayView1D<const float2> & xz, const Color & color = Color::Collection::RED);
		/** Add axis-aligned square into a xz-plane. */
		void AddXZSquare(f32 minX, f32 minZ, f32 maxX, f32 maxZ, f32 Y, const Color & color = Color::Collection::RED);
		/** Add line strip. */
		void AddLineStrip(const ArrayView1D<const float3> & vertices, const Color & color = Color::Collection::RED);

	public: // constructors
		/** Destructor. */
		virtual ~LineModel() override = default;
	};
}

namespace DD::Simulation {
	/************************************************************************/
	/* LineModelBase                                                        */
	/************************************************************************/
	template<typename VERTEX, Gpu::IIndexBufferIndices INDEX>
	void LineModelBase<VERTEX, INDEX>::iModelInit(Gpu::Device & device) {
		_device = device;
		_boundingBox.Reset();
		for (const auto & vertex : Vertices)
			_boundingBox.Include(vertex.Position);
		_boundingSphere = _boundingBox.BoundingSphere();

		Vertices.Init(device);
		Lines.Init(device);

		PixelShader = device->GetPixelShader(L"PSSimple");
		VertexShader = device->GetVertexShader<VERTEX>(L"VSSimple");
	}


	template<typename VERTEX, Gpu::IIndexBufferIndices INDEX>
	void LineModelBase<VERTEX, INDEX>::iModelDraw(DrawArgs & args) {
		Vertices.Bind();
		Lines.Bind();
		_device->SetPrimitiveTopology(Gpu::Devices::PrimitiveTopologies::LineList);
		PixelShader->Set();
		VertexShader->Set();

		switch (args.Mode) {
		case IModel::DrawArgs::DrawModes::Normal:
			_device->DrawIndexed((u32)(Lines.Size() * 2));
			break;

		case IModel::DrawArgs::DrawModes::Fraction:
			_device->DrawIndexed((u32)(Lines.Size() * Math::Saturate(args.Fraction)) * 2);
			break;

		default:
			throw;
		}
	}


	template<typename VERTEX, Gpu::IIndexBufferIndices INDEX>
	void LineModelBase<VERTEX, INDEX>::AddLineModel(const LineModelBase & m) {
		index_t offset = (index_t)Vertices.Size();
		line_t offsetLine(offset, offset);
		for (const VERTEX & v : m.Vertices)
			Vertices.Add(v);
		for (const line_t & l : m.Lines)
			Lines.Add(l + offsetLine);
	}


	template<typename VERTEX, Gpu::IIndexBufferIndices INDEX>
	void LineModelBase<VERTEX, INDEX>::AddLine(const VERTEX & a, const VERTEX & b) {
		index_t offset = (index_t)Vertices.Size();
		line_t line((index_t)offset, (index_t)(offset + 1));
		Vertices.Add(a, b);
		Lines.Add(line);
	}


	template<typename VERTEX, Gpu::IIndexBufferIndices INDEX>
	void LineModelBase<VERTEX, INDEX>::AddLineStrip(const ArrayView1D<const VERTEX> & vertices) {
		// zero offset for correct indexing
		index_t zero = (index_t)Vertices.Size();
		// add vertices
		Vertices.AddIterable(vertices);
		// reserve space for lines
		if (vertices.Length())
			Lines.Reserve(vertices.Length() - 1);

		// supremum
		index_t limit = zero + (index_t)vertices.Length();
		// first lines
		line_t line = {zero, zero + 1};
		// add indices
		while (line.y < limit) {
			Lines.AppendUnsafe(line);
			++line.x;
			++line.y;
		}
	}


	/************************************************************************/
	/* LineModel<Simulation::ColorVertex>                                               */
	/************************************************************************/
	template<Gpu::IIndexBufferIndices INDEX>
	void LineModel<ColorVertex, INDEX>::AddXZSquare(f32 minX, f32 minZ, f32 maxX, f32 maxZ, f32 Y, const Color & c) {
		/**
		 *   D--------C
		 *  /        /
		 * A--------B
		 */
		const index_t A = (index_t)Base::Vertices.Size();
		const index_t B = A + 1;
		const index_t C = B + 1;
		const index_t D = C + 1;

		Base::Vertices.Add(
			ColorVertex(minX, Y, minZ, c),
			ColorVertex(maxX, Y, minZ, c),
			ColorVertex(maxX, Y, maxZ, c),
			ColorVertex(minX, Y, maxZ, c)
		);

		Base::Lines.Add(
			line_t(A, B),
			line_t(B, C),
			line_t(C, D),
			line_t(D, A)
		);
	}


	template<Gpu::IIndexBufferIndices INDEX>
	void LineModel<ColorVertex, INDEX>::AddPolyhedron(f32 minY, f32 maxY, const ArrayView1D<const float2> & xz, const Color & color) {
		const index_t offset = (index_t)Base::Vertices.Size();
		ColorVertex vertex;
		vertex.Color = color;
		// add vertices
		for (const float2 & point : xz) {
			vertex.Position = { point.x, minY, point.y };
			Base::Vertices.Add(vertex);
			vertex.Position = { point.x, maxY, point.y };
			Base::Vertices.Add(vertex);
		}
		// vertical lines
		for (index_t i = 0, m = (index_t)(2 * xz.Length()); i < m; i += 2)
			Base::Lines.Add(line_t(static_cast<index_t>(offset + i), static_cast<index_t>(offset + i + 1)));
		// horizontal lines
		for (index_t i = 2, m = (index_t)(2 * xz.Length()); i <= m; i += 2)
			Base::Lines.Add(
				line_t(static_cast<index_t>(offset + i % m), static_cast<index_t>(offset + i - 2)),
				line_t(static_cast<index_t>(offset + (i + 1) % m), static_cast<index_t>(offset + i - 1))
			);
	}


	template<Gpu::IIndexBufferIndices INDEX>
	void LineModel<ColorVertex, INDEX>::AddAABB(const float3 & min, const float3 & max, const Color & color) {
		/**
		 *   H--------G
		 *  /|       /|
		 * E--------F |
		 * | |      | |
		 * | D------|-C
		 * |/       |/
		 * A--------B
		 *
		 * A ~ minimum
		 * G ~ maximum
		 */
		const index_t A = (index_t)Base::Vertices.Size();
		const index_t B = A + 1;
		const index_t C = B + 1;
		const index_t D = C + 1;
		const index_t E = D + 1;
		const index_t F = E + 1;
		const index_t G = F + 1;
		const index_t H = G + 1;

		Base::Vertices.Add(
			ColorVertex({ min.x, min.y, min.z }, color),
			ColorVertex({ max.x, min.y, min.z }, color),
			ColorVertex({ max.x, min.y, max.z }, color),
			ColorVertex({ min.x, min.y, max.z }, color),
			ColorVertex({ min.x, max.y, min.z }, color),
			ColorVertex({ max.x, max.y, min.z }, color),
			ColorVertex({ max.x, max.y, max.z }, color),
			ColorVertex({ min.x, max.y, max.z }, color)
		);

		Base::Lines.Add(
			line_t(A,B), line_t(B,C), line_t(C,D), line_t(D,A),
			line_t(E,F), line_t(F,G), line_t(G,H), line_t(H,E),
			line_t(A,E), line_t(B,F), line_t(C,G), line_t(D,H)
		);
	}


	template<Gpu::IIndexBufferIndices INDEX>
	void LineModel<ColorVertex, INDEX>::AddLine(const float3 & a, const float3 & b, const Color & color) {
		// create vertices
		ColorVertex A, B;

		// assign values
		A.Position = a;
		A.Color = color;
		B.Position = b;
		B.Color = color;

		// register
		AddLine(A, B);
	}


	template<Gpu::IIndexBufferIndices INDEX>
	void LineModel<ColorVertex, INDEX>::AddLineStrip(
		const ArrayView1D<const float3> & inputVertices, const Color & color
	) {
		Base::Lines.Reserve(inputVertices.Length() - 1);
		for (index_t loffset = (index_t)Base::Vertices.Size() - 1, roffset = (index_t)Base::Vertices.Size(), index = 1; index < inputVertices.Length(); ++index)
			Base::Lines.AddUnsafe({(index_t)(index + loffset), (index_t)(index + roffset) });

		Base::Vertices.Reserve(inputVertices.Length());
		for (const float3 & v : inputVertices)
			Base::Vertices.AddUnsafe({ v, color });
	}
}
