#ifndef DD_SPHERE_VERTEX_H
#define DD_SPHERE_VERTEX_H

#include "ShaderHelpers.h"

#define SimpleInstanceDeclaration(parser) \
 parser(float4, Transformation0, INSTANCEPOSITION, 1, PerInstance, 1, 0)\
 parser(float4, Transformation1, INSTANCEPOSITION, 2, PerInstance, 1, 0)\
 parser(float4, Transformation2, INSTANCEPOSITION, 3, PerInstance, 1, 0)\
 parser(float4, Transformation3, INSTANCEPOSITION, 4, PerInstance, 1, 0)\
 parser(float3, Position,        INSTANCEPOSITION, 5, PerInstance, 1, 0)\
 parser(float3, Scale,           INSTANCECOLOR,    1, PerInstance, 1, 0)\
 parser(uint,   Id,              INSTANCECOLOR,    2, PerInstance, 1, 0)

#define SimpleInstanceVertexDeclaration(parser)\
 parser(float3, Position, POSITION, 0, PerVertex, 0, 1)

namespace DD {
	namespace Simulation {
		/**
		 * Instanced rendering for simple vertices.
		 */
		struct SimpleInstance {
			STATIC_TRAITS_DECLARATION
				SimpleInstanceDeclaration(PARSER_MEMORY)
		};

		struct SimpleInstanceVertex {
			STATIC_TRAITS_DECLARATION
				SimpleInstanceVertexDeclaration(PARSER_MEMORY)
		};
	}
}
#endif
