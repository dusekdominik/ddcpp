#include "ShaderHelpers.h"

namespace DD {
	struct CSInput { float3 P; float E; };

	struct CSOutput { int I; };
}