﻿#pragma once
namespace DD::Simulation {
	/** Model for point cloud representation. */
	struct PointCloudModel;
}

#include <DD/Reference.h>
#include <DD/Array.h>
#include <DD/Simulation/PointCloud.h>
#include <DD/Simulation/IModel.h>
#include <DD/Simulation/ModelGroup.h>
#include <DD/Simulation/QuadModel.h>

namespace DD::Simulation {
	struct PointCloudModel
		: public ICullableModel
	{
	private: // properties
		/** Check if model has been already initialized. */
		bool _inited;
		/** Size of single point. */
		f32 _pointSize;
		/** Point cloud holder. */
		PointCloud _pointCloud;
		/** Models of cells. */
		Array3D<Reference<QuadModel>> _models;
		/** Points. */
		Reference<IntersectableModelGroup> _pointModel;

	private: // methods
		/** Init point cloud models. */
		void initModels();
		/** Init quad-tree hierarchy for frustum culling. */
		void initFrustumCullingHierarchy(bool initialized = false);
		/** Initialize. */
		void init();

	protected: // IModel
		const char * iModelName() const override;
		void iModelInit(Gpu::Device &) override;
		void iModelDraw(DrawArgs & args) override;
		void iModelDrawCulled(const Frustum3F &, const float4x4 &, DrawArgs & args) override;
		bool iModelAsyncTick(Gpu::Device &, AsyncInitInterface *, Units::Time_MicroSecondULL &);
		i32 iModelAsyncTotal() const override { return (i32)_pointCloud->Grid().Length(); }

	public: // methods
		/** Initialize hierarchy - does not init rendering structures. */
		void PreInit() { init(); }
		/** Size of single point. */
		void PointSize(f32 pointSize);
		/** Size of single point. */
		f32 PointSize() const { return _pointSize; }

	public: // constructors
		/** Constructor. Undefined point size undefined, it will be hinted by point cloud source. */
		PointCloudModel(PointCloud & pointCloud, f32 pointSize = 0.f);
		/** Destructor. */
		virtual ~PointCloudModel() override = default;
	};
}
