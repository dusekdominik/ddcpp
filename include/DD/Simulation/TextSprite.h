#pragma once
namespace DD::Simulation {
	/** Simple sprite with text.*/
	struct TextSprite;
}

#include <DD/Simulation/ISprite.h>
#include <DD/Debug.h>
#include <DD/Time.h>

namespace DD::Simulation {
	struct TextSprite
		: public ISprite
	{
	protected: // properties
		/** Drawing primitive. */
		Gpu::D2D::Labels _text;

	public: // properties
		/** Called whenever sprite needs to be updated. */
		Event<Gpu::D2D::Labels &> OnUpdate;

	protected: // ISprite
		void iSpriteDraw(Gpu::D2D & device) override { device.DrawLabel(_text); }
		void iSpriteEnabled(bool value) override { OnUpdate(_text); }
		bool iSpriteEnabled() const override { return true; }
		void iSpriteExpired(bool) override {}
		bool iSpriteExpired() const override { return false; }
		void iSpriteInit(Gpu::D2D & device) {}
		void iSpriteUpdate(Units::Time_MicroSecondF delta, Units::Time_MicroSecondF time) override { OnUpdate(_text); }

	public: // constructors
		/** Constructor. */
		TextSprite(Text::StringView16 name, Gpu::D2D::Labels && label) : ISprite(name), _text(Fwd(label)) {}
	};

	struct TextSpriteEx
		: public TextSprite
	{
	protected:
		/** Drawing primitive. */
		Gpu::D2D::Rectangles _rectangle;
		/** Callback for processing background when the bbox is computed. */
		Action32<const Gpu::D2D::Boxes &, Gpu::D2D &> _callback;

	public:
		/** Text alignment. */
		Debug::Enum<Gpu::D2D::THAligns> TAlign;
		/** Block alignment. */
		Debug::Enum<Gpu::D2D::HAligns> HAlign;
		/** Block alignment. */
		Debug::Enum<Gpu::D2D::VAligns> VAlign;
		/** Is sprite enabled for drawing. */
		Debug::Boolean Enabled;
		/** Transparency of rendered text. */
		Debug::Range<u8> Alpha;
		/** Background of rendered text. */
		Debug::Enum<Colors> Background;
		/** Font size of rendered text. */
		Debug::Range<f32> Scale;

	protected: // ISprite
		void iSpriteDraw(Gpu::D2D & device) override { device.DrawLabel(_text); }
		void iSpriteEnabled(bool value) override { Enabled = value; }
		bool iSpriteEnabled() const override { return Enabled; }
		void iSpriteExpired(bool) override {}
		bool iSpriteExpired() const override { return false; }
		void iSpriteUpdate(Units::Time_MicroSecondF delta, Units::Time_MicroSecondF time) override {
			_text.THAlign = TAlign;
			_text.Box.HAlign = HAlign;
			_text.Box.VAlign = VAlign;
			_text.Scale = Scale;

			TextSprite::iSpriteUpdate(delta, time);
		}
		void iSpriteInit(Gpu::D2D & device) override {
			_callback = [this](const Gpu::D2D::Boxes & box, Gpu::D2D & device) {
				_rectangle.Box = box;
				// set padding
				_rectangle.Box.Origins -= float2(10.f, 10.f);
				_rectangle.Box.Dimensions += float2(20.f, 20.f);
				_rectangle.Background = Color::Collection()[+Background].SetA(Alpha);

				device.DrawRectangle(_rectangle);
			};
			_text.Callbacks.SizeComputed = _callback;
		}

	public: // constructors
		/** Constructor. */
		TextSpriteEx(Text::StringView16 name, Gpu::D2D::Labels && label)
			: TextSprite(name, Fwd(label))
			, TAlign(    label.THAlign, name, L"Text Alignment",          L"Size of text in render window.")
			, HAlign( label.Box.HAlign, name, L"Horizontal Alignment",    L"Block alignment of text in render window.")
			, VAlign( label.Box.VAlign, name, L"Vertical Alignment",      L"Block alignment of text in render window.")
			, Enabled(            true, name, L"Enabled",                 L"Is messaging text block enabled.")
			, Alpha(         0, 0, 255, name, L"Background transparency", L"Transparency of text background in render window.")
			, Background(Colors::BLACK, name, L"Background color",        L"Background color of the text area in render window.")
			, Scale(    1.f, 0.5f, 2.f, name, L"Text size",               L"Size of text in render window.")
		{}
	};
}
