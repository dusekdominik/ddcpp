#ifndef SHARED_SHADER_CONTEXT_H
#define SHARED_SHADER_CONTEXT_H

#if SHADER_CONTEXT
#define PARSER_MEMORY(type, name, definition, definitionNum, incidence, modul, slot) type name : definition##definitionNum;
#define PARSER_LAYOUT(type, name, definition)
#define NAMESPACE_BEGIN(name)
#define NAMESPACE_END
#define CONTEXT_LAYOUT(layout)
#define STATIC_TRAITS_DECLARATION
#define DEFINE_VERTEX_SHADER_INPUT(type)
#define CONSTANT_BUFFER(name, slot) cbuffer name : register(b##slot)
typedef uint2 u64;
typedef int ColorInt;

#else
#include <DD/Color.h>
#include <DD/Matrix.h>
#include <DD/Gpu/ShaderInput.h>
#include <DD/Vector.h>

template<unsigned i>
struct _CB { static constexpr unsigned SLOT = i; };
typedef DD::M44<float> matrix;
typedef DD::M44<float> float4x4;
typedef DD::Color ColorInt;
typedef unsigned uint;
typedef DD::V2<unsigned> uint2;

#define NAMESPACE_BEGIN(name) namespace name {
#define NAMESPACE_END }
#define CONTEXT_LAYOUT(layout) { layout };
#define PARSER_MEMORY(type, name, ...) type name;
#define PARSER_LAYOUT(type, name, definition, defNum, incidence, modul, slot) { #definition, defNum, DD::Gpu::ShaderInputFormat::_##type, slot, 0xffffffff, DD::Gpu::ShaderInputClassification::incidence, modul },
#define CONSTANT_BUFFER(name, slot) struct name : public _CB<slot>
#define STATIC_TRAITS_DECLARATION \
	static const unsigned int SIZE; \
	static const DD::Gpu::ShaderInput LAYOUT[]; \
	static const size_t LAYOUT_SIZE;
#define DEFINE_VERTEX_SHADER_INPUT(type) \
	const unsigned int type::SIZE = sizeof(type);\
	const DD::Gpu::ShaderInput type::LAYOUT[] = { type##Declaration(PARSER_LAYOUT) };\
	const size_t type::LAYOUT_SIZE = sizeof(type) == 0 ? 0 : sizeof(type::LAYOUT) / sizeof(D3D11_INPUT_ELEMENT_DESC);
#define DECLARE_VERTEX_SHADER_INPUT(type)\
	STATIC_TRAITS_DECLARATION \
	type##Definition(PARSER_MEMORY)


#endif // SHADER_CONTEXT
#endif