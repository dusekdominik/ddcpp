#ifndef CAMERA_CONSTANT_BUFFER_H
#define CAMERA_CONSTANT_BUFFER_H

#include "ShaderHelpers.h"
#define _CAMERA_FLAG_COLOR       (0)
#define _CAMERA_FLAG_DEPTH       (1 << 0)
#define _CAMERA_FLAG_HEIGHT      (1 << 1)
#define _CAMERA_FLAG_HEIGHT_TUNE (1 << 2)


NAMESPACE_BEGIN(DD::Simulation)

CONSTANT_BUFFER(CameraConstantBuffer, 0) {
	/** */
	matrix ViewProjectionTransformation;
	/** Origin of camera coordinate system. */
	float4 CameraO;
	/** Axis of width of camera coordinate system. */
	float4 CameraX;
	/** Axis of height of camera coordinate system. */
	float4 CameraY;
	/** Axis of depth of camera coordinate system. */
	float4 CameraZ;
	/**
	 *                  1
	 * -------------------------------------
	 * Tg(0.5 * FieldOfView in X coordinate)
	 */
	float CameraHTgFovXInv;
	/**
	 *                  1
	 * -------------------------------------
	 * Tg(0.5 * FieldOfView in Y coordinate)
	 */
	float CameraHTgFovYInv;
	/**
	 * Tg(0.5 * FieldOfView in X coordinate)
	 */
	float CameraHTgFovX;
	/**
	 * Tg(0.5 * FieldOfView in Y coordinate)
	 */
	float CameraHTgFovY;
	/** */
	u64 Time;
	/** */
	int Delta;
	/** */
	int CameraFlags;
	/** */
	float SceneHeightMin;
	/** */
	float SceneHeightRangeInv;
	/** */
	float SceneDepthInv;
	/** */
	float CameraConstantBufferPadding;
};

CONSTANT_BUFFER(ObjectConstantBuffer, 1) {
	/** */
	matrix ObjectTransformation;
};

CONSTANT_BUFFER(BillBoardConstantBuffer, 2) {
	/** */
	float2 BillboardSize;
	/** */
	float2 AlignmentBillboard;
};

CONSTANT_BUFFER(QuadConstantBuffer, 2) {
	/** */
	float PointSize;
	/** */
	ColorInt QuadColorTone;
	/** */
	int QuadColorToneEnabled = 1;
	/** */
	int QuadClass = -1;
};

CONSTANT_BUFFER(ComputeShaderConstantBuffer, 2) {
	float3 CSLineA, CSLineB;
	float2 ComputeShaderAlignment;
};

CONSTANT_BUFFER(EffectConstantsBuffer, 3) {
	/** Index of effect. */
	int Effect;
	/** Padding. */
	int3 Padding;
};

CONSTANT_BUFFER(ConvulutionBuffer, 3) {
	/** */
	float4 Kernel[32][32];
	/** */
	int KernelX;
	/** */
	int KernelY;
	/** */
	int Samples;
	/** */
	int ConvulutionBufferPadding;
	/**  */
	float2 Resolution;
	/** */
	float2 ResolutionInv;
};


NAMESPACE_END

#if SHADER_CONTEXT
float4 Convert2CameraSpace(float3 position, matrix objectTransformation) {
	matrix objectViewProjection = mul(ViewProjectionTransformation, objectTransformation);
	float4 extPosition = float4(position, 1);
	float4 newPosition = mul(objectViewProjection, extPosition);
	if (Effect)
		newPosition.z = 0;
	return newPosition;
}

float4 Convert2CameraSpace(float3 position) {
	return Convert2CameraSpace(position, transpose(ObjectTransformation));
}
#endif
#endif