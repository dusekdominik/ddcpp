#pragma once
namespace DD::Simulation {
	/** Interface for text messages. */
	struct IMessage;
}

#include <DD/Gpu/D2D.h>
#include <DD/Reference.h>
#include <DD/Types.h>
#include <DD/Units/Unit.h>

namespace DD::Simulation {
	struct IMessage
		: public virtual IReferential
	{
	protected: // IMessage interface
		/** */
		virtual void iMessageOnRegisterTry() {}
		/** */
		virtual void iMessageOnEnabled() {}
		/** Method is called, when message leaves message queue. */
		virtual void iMessageOnDisabled() {}
		/** Method for parsing delta times of message. */
		virtual bool iMessageEnabled(Units::Time_MicroSecondULL delta) = 0;
		/** Method testing if message is still valid. */
		virtual bool iMessageEnabled() const = 0;
		/** Message text. */
		virtual Gpu::D2D::Strings iMessageGetText() const { Gpu::D2D::Strings s; FeedText(s); return s; }
		/** Feed message text to a target. */
		virtual void iMessageFeed(Gpu::D2D::Strings & target) const { target.Append(GetText()); }

	public: // public interface
		void OnRegisterTry() { iMessageOnRegisterTry(); }
		void OnDisabled() { iMessageOnDisabled(); }
		void OnEnabled() { iMessageOnEnabled(); }
		bool Enabled(Units::Time_MicroSecondULL delta) { return iMessageEnabled(delta); }
		bool Enabled() const { return iMessageEnabled(); }
		void FeedText(Gpu::D2D::Strings & target) const { iMessageFeed(target); }
		Gpu::D2D::Strings GetText() const { return iMessageGetText(); }

	public: // constructors
		virtual ~IMessage() = default;
	};


	/** Alias.. */
	using Message = Reference<IMessage>;
}
