#ifndef DD_TEXTURE_VERTEX_H
#define DD_TEXTURE_VERTEX_H

#include "ShaderHelpers.h"

#define TextureVertexDeclaration(parser) \
 parser(float3, Position, POSITION, 0, PerVertex, 0, 0) \
 parser(float2, Texture, TEXTURE, 0, PerVertex, 0, 0)\
 parser(float3, Alignment, COLOR, 0, PerVertex, 0, 0)

namespace DD {
	namespace Simulation {
		/**
	 * TextureVertex
	 *
	 * Vertex declaring its position and UV coordinates of texture.
	 */
		struct TextureVertex {
			STATIC_TRAITS_DECLARATION
				TextureVertexDeclaration(PARSER_MEMORY)
		};
	}
}
#endif
