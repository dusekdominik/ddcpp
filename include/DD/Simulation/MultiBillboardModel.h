#pragma once
namespace DD::Simulation {
	/**
	 * Model of multiple billboards.
	 * Contains list filled by indices to the table with set of icons.
	 */
	class MultiBillboardModel;
}

#include <DD/List.h>
#include <DD/Simulation/BillboardModel.h>

namespace DD::Simulation {
	class MultiBillboardModel
		: public IModel
	{
	public: // nested
		/** Item with description of icons.  */
		struct Item {
			/** What icon. */
			size_t IconType;
			/** Index of icon in _icons. */
			size_t IconIndex;
			/** Index of icon in _models[x]. */
			size_t ModelIndex;
			/** Position of icon. */
			float3 Position;
		};

		/** Reference to an icon. */
		class IconRef {
			/** Reference of the owner. */
			MultiBillboardModel & _model;
			/** Index of item of the owner. */
			size_t _index;

		public: // operators
			/** Assign an icon type. */
			void operator=(size_t icon) { _model.SetItem(_index, icon); }
			/** Icon type. */
			operator size_t() const { return _model.GetItem(_index); }

		public: // constructors
			/** Constructor. */
			IconRef(MultiBillboardModel & model, size_t index)
				: _model(model), _index(index)
			{}
		};

		/** Reference to an BillboardModel. */
		typedef Reference<BillboardModel> billbord_r;

	private: // properties
		/** Device handle. */
		Gpu::Device _device;
		/** Reinit flag. */
		bool _reinit;
		/** List of models - each model ~ icon type. */
		List<billbord_r> _models;
		/** All icons - union of models. */
		List<Item> _icons;
	private: // methods
		/** Add an icon into the register. */
		template<typename...ARGS>
		void addIcon(const Icon & arg, ARGS...args) { AddIconClass(arg); addIcon(args...); }
		/** Add an icon into the register. */
		void addIcon() {}

	protected: // IModel
		const char * iModelName() const override { return "MultiBillboardModel"; }
		void iModelInit(Gpu::Device & device) override;
		void iModelDraw(DrawArgs & args) override;
		void iModelDrawCulled(const Frustum3F & frustum, const float4x4 & transform, DrawArgs & args) override;

	public: // methods
		/** Adds icon type. */
		void AddIconClass(const Icon & icon);
		/** Adds icon item. */
		size_t AddItem(size_t icon, const float3 & position);
		/** Clear all the icon instances, keeps icon classes. */
		void Clear();
		/** Returns icon type of item selected by given index. */
		size_t GetItem(size_t item);
		/** Sets icon type of item selected by given index. */
		void SetItem(size_t item, size_t iconType);

	public: // operators
		/** Indexer. */
		Reference<IconRef> operator[](size_t index) { return new IconRef(*this, index); }

	public: // constructors
		/** Constructor. */
		template<typename...ICONS>
		MultiBillboardModel(ICONS...icons) : _reinit(1) { addIcon(icons...); }
		/** Destructor. */
		virtual ~MultiBillboardModel() override = default;
	};
}
