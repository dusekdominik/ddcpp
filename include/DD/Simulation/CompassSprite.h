#pragma once
namespace DD::Simulation {
	/** */
	struct CompassSprite;
}

#include <DD/Gpu/D2D.h>
#include <DD/Profiler.h>
#include <DD/Simulation/Camera.h>
#include <DD/Simulation/ISprite.h>

namespace DD::Simulation {
	struct CompassSprite
		: public ISprite
	{
		bool _enabled = true;
		Camera & _camera;
		Gpu::D2D::Boxes _area;
		Gpu::D2D::Rectangles _clock;
		Gpu::D2D::Rectangles _center;
		Gpu::D2D::Lines _line;

	protected: // iSprite
		virtual void iSpriteDraw(Gpu::D2D & device) override {
			DD_PROFILE_SCOPE_STATS("Engine/Draw/Draw2D/Compass");
			Gpu::D2D::Area renderer = device.Relative(_area);
			renderer.DrawEllipse(_clock);
			renderer.DrawEllipse(_center);
			renderer.DrawLine(_line);
		}
		virtual void iSpriteEnabled(bool value) override { _enabled = value; }
		virtual bool iSpriteEnabled() const { return _enabled; }
		virtual void iSpriteExpired(bool value) override {}
		virtual bool iSpriteExpired() const override { return false; }
		virtual void iSpriteInit(Gpu::D2D & device) override {
			_area.Dimensions.x = 60.f;
			_area.Dimensions.y = 60.f;
			_area.HAlign = Gpu::D2D::HAligns::HRight;

			_clock.Background = 0x44FFFFFFU;
			_clock.Box.Dimensions.x = 40.f;
			_clock.Box.Dimensions.y = 40.f;
			_clock.Box.Origins.x = 10.f;
			_clock.Box.Origins.y = 10.f;
			_clock.Frame.Brush = 0xFF00FFFFU;
			_clock.Frame.Size.Value = 4.f;

			_center.Background = 0xFFFFFFFFU;
			_center.Box.Dimensions.x = 4.f;
			_center.Box.Dimensions.y = 4.f;
			_center.Box.Origins.x = 28.f;
			_center.Box.Origins.y = 28.f;

			_line.A = float2(30.f, 30.f);
			_line.Size.Value = 4.f;
			_line.Brush = 0xFF00FFFFU;
		}
		virtual void iSpriteUpdate(Units::Time_MicroSecondF delta, Units::Time_MicroSecondF time) override {
			float2 dir = _camera.Direction().xz;
			_line.B = _line.A + Math::Normalize(dir) * 30.f;
		}

	public: // constructors
		/** Constructor. */
		CompassSprite(Camera & camera) : ISprite(L"Compass") , _camera(camera) {}
		/** Destructor. */
		~CompassSprite() override = default;
	};
}
