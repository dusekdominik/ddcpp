#pragma once
namespace DD {
	/** Extern forward. */
	namespace Gpu { enum IIndexBufferIndices : char; }

	namespace Simulation {
		/** Interface for all the triangle models. */
		class ITriangleModel;

		/** Class defining common functionality of triangle model. */
		template<typename VERTEX, Gpu::IIndexBufferIndices INDEX>
		struct TriangleModelBase;

		/** Class defining vertex-specialized functionality for triangle models. */
		template<typename VERTEX, Gpu::IIndexBufferIndices INDEX>
		class TriangleModel;
	}
}

#include <DD/Array.h>
#include <DD/Color.h>
#include <DD/Gpu/Devices.h>
#include <DD/Gpu/IndexBuffer.h>
#include <DD/Gpu/PixelShader.h>
#include <DD/Gpu/VertexBuffer.h>
#include <DD/Gpu/Texture.h>
#include <DD/Gpu/VertexShader.h>
#include <DD/HeapLib.h>
#include <DD/Math.h>
#include <DD/Matrices.h>
#include <DD/Reference.h>
#include <DD/Simulation/ICullableModel.h>
#include <DD/Simulation/ColorVertex.h>
#include <DD/Simulation/TextureVertex.h>
#include <DD/Vector.h>
#include <DD/Math/Interval.h>

#define DD_TRIANGLEMODEL_ASSERT(c)

namespace DD::Simulation { // declarations
	class ITriangleModel : public ICullableModel {};

	/************************************************************************/
	/* Unscent models                                                       */
	/************************************************************************/
	template<typename VERTEX, Gpu::IIndexBufferIndices INDEX = Gpu::IIndexBufferIndices::_16Bit>
	struct TriangleModelBase
		: public ITriangleModel
	{
		/** Helper typedef for deriving triangle type. */
		using Traits = Gpu::IndexBufferPrimitiveTraits<Gpu::IIndexBufferPrimitives::Triangles, INDEX>;
		/** Triangle type. */
		using Triangle = typename Traits::primitive_t;
		/** Index type. */
		using Index = typename Traits::index_t;
		/** Vertex type. */
		using Vertex = VERTEX;

	public: // properties
		Gpu::Device _device;
		/** Vertices of model. */
		Gpu::VertexBuffer<Vertex> Vertices;
		/** Triangles of model. */
		Gpu::IndexBuffer<Gpu::IIndexBufferPrimitives::Triangles, INDEX> Triangles;
		/** Shaders of model. */
		Reference<Gpu::PixelShader> PixelShader;
		/** Shaders of model. */
		Reference<Gpu::VertexShader> VertexShader;

	protected: // IModel
		virtual void iModelInit(Gpu::Device & device) override;
		virtual void iModelDraw(DrawArgs & args) override;

	public: // methods
		/** Add rectangular field of vertices and apply simple triangulation to it. */
		void AddBlanket(const ArrayView2D<VERTEX> & vertices);
		/** Add another triangle model into this one. */
		void AddTriangleModel(const TriangleModelBase & m);
		/** Add another triangle model into this one with defined transformation. */
		void AddTriangleModel(const TriangleModelBase & m, const float3x3 & rotation, const float3 & position);
		/** Add a triangle. */
		void AddTriangle(const Vertex & v0, const Vertex & v1, const Vertex & v2);
		/** Clip mesh by bbox. */
		void Clip(const Math::Interval3F & box);
		/** Remove unused vertices. */
		void Clean();
		/** Check if model has any content. */
		bool IsEmpty() const { return Vertices.Size() == 0 || Triangles.Size() == 0; }
		/** Filter out all the triangles with normals pointing to other half-space than given vector. */
		template<typename T>
		void NormalFilter(const Vector<T, 3> & normal);
		/**
		 * Try to remove vertex multiplicities.
		 * Method presumes that vertex has comparer operators == and <.
		 * Otherwise the comparers can be overridden.
		 */
		template<typename COMPARER, typename EQCOMPARER>
		void Compress(
			const COMPARER & comparer = [](const Vertex & lhs, const Vertex & rhs) { return lhs < rhs; },
			const EQCOMPARER & eqComparer = [](const Vertex & lhs, const Vertex & rhs) { return lhs == rhs; }
		);
	};


	template<typename VERTEX, Gpu::IIndexBufferIndices INDEX = Gpu::IIndexBufferIndices::_16Bit>
	class TriangleModel
		: public TriangleModelBase<VERTEX, INDEX>
	{
	protected: // IModel
		virtual const char * iModelName() const override { return typeid(*this).name(); }
	};

	/************************************************************************/
	/* TextureVertex based model                                            */
	/************************************************************************/
	template<Gpu::IIndexBufferIndices INDEX>
	struct TriangleModel<TextureVertex, INDEX>
		: public TriangleModelBase<TextureVertex, INDEX>
	{
		using Base = TriangleModelBase<TextureVertex, INDEX>;
		/** Vertex indexing type. */
		using Index = typename Base::Index;
		/** Index set to define a triangle. */
		using Triangle = typename Base::Triangle;
		/** Vertex type */
		using Vertex = typename Base::Vertex;

	public: // nested
		/** Methods of triangulation of sphere-like shapes. */
		enum SphereTriangulation {
			/** Triangulation via polar coordinates. */
			Polar,
			/** Triangulation via subdivision of octahedron. */
			Octahedron
		};
		/** Visibility of a shape. */
		enum ShapeVisibility {
			/** Draw texture just inside a shape. */
			Inside = 1 << 0,
			/** Draw texture just outside a shape. */
			Outside = 1 << 1,
			/** Draw texture inside and outside a shape. */
			Both = 1 << 0 | 1 << 1
		};
		/** Default values. */
		struct Defaults {
			static constexpr i32 SphereTriangulationDepth = 5;
			static constexpr ShapeVisibility ShapeVisibility = ShapeVisibility::Outside;
			static constexpr SphereTriangulation SphereTriangulation = SphereTriangulation::Octahedron;
			static constexpr f32 SphereTriangulationRadius = 1.f;
			static constexpr const float3 & SphereTriangulationCenter = float3(0.f);
		};

	public: // properties
		/** Name of texture. */
		String TextureName;
		/** Texture reference. */
		Reference<::DD::Gpu::Texture> Texture;

	protected: // IModel
		const char * iModelName() const override { return typeid(*this).name(); }
		void iModelInit(Gpu::Device & device) override;
		void iModelDraw() override;

	protected: // methods
		/** Helper method for sphere creation. */
		Vertex sphereAverageVertex(const Vertex & a, const Vertex & b);
		/** Recursively split triangle into 4 sub-triangles. */
		void sphereTriangleSplit(Index a, Index b, Index c, Array<Triangle> & out);
		/** Add triangle via indices. */
		void sphereAddTriangle(Index a, Index b, Index c, ShapeVisibility visibility);
		/** Recursively split triangle into 4 sub-triangles, until given depth is reached. */
		void sphereTriangulationDepth(Index _a, Index _b, Index _c, u32 depth, ShapeVisibility visibility);
		/** Recursively split triangle into 4 sub-triangles, until triangles smaller than given surface. */
		void sphereTriangulationSurface(Index _a, Index _b, Index _c, f32 surface, ShapeVisibility visibility);

	public: // methods
		/** Add sphere via polar triangulation. */
		void AddSpherePolar(
			const float3 & center = Defaults::SphereTriangulationCenter,
			f32 radius = Defaults::SphereTriangulationRadius,
			i32 depth = Defaults::SphereTriangulationDepth,
			ShapeVisibility visibility = Defaults::ShapeVisibility
		);
		/** Add sphere via octahedron triangulation. */
		void AddSphereOctahedron(
			const float3 & center = Defaults::SphereTriangulationCenter,
			f32 radius = Defaults::SphereTriangulationRadius,
			i32 depth = Defaults::SphereTriangulationDepth,
			ShapeVisibility visibility = Defaults::ShapeVisibility
		);
		/**
		 *           J (0,r,0)
		 *           |  C (0,0,-r)
		 *           | /
		 *           |/
		 * C---------0---------B
		 * (-r,0,0) /|   (r,0,0)
		 *         / |
		 *       A=E | (0,0,r)
		 *           I (0,-r,r)
		 *
		 * (1,0)-----(1,1)  F0-F1-F2-F3
		 *   |         |    |  |  |  |
		 *   |(0.5,0.5)|    A--B--C--D
		 *   |         |    |  |  |  |
		 * (0,0)-----(0,1)  E0-E1-E2-E3
		 *
		 * @param        center - sphere center.
		 * @param        radius - sphere radius.
		 * @param         depth - precision of interpolation.
		 * @param    visibility - visibility of sphere.
		 * @param triangulation - method for approximation of sphere by a polytope.
		 */
		void AddSphere(
			const float3 & center = Defaults::SphereTriangulationCenter,
			f32 radius = Defaults::SphereTriangulationRadius,
			i32 depth = Defaults::SphereTriangulationDepth,
			ShapeVisibility visibility = Defaults::ShapeVisibility,
			SphereTriangulation triangulation = Defaults::SphereTriangulation
		);
		/**
		 * Using tetrahedron triangulation.
		 *
		 *           T (0,r,0)
		 *           |  C (0,0,-r)
		 *           | /
		 *           |/
		 * C---------0---------B
		 * (-r,0,0) /    (r,0,0)
		 *         /
		 *        A (0,0,r)
		 *
		 * (1,0)-----(1,1)  D---------C
		 *   |         |    |         |
		 *   |(0.5,0.5)|    |    T    |
		 *   |         |    |         |
		 * (0,0)-----(0,1)  A---------B
		 *
		 * @param        center - sphere center.
		 * @param        radius - sphere radius.
		 * @param         depth - precision of interpolation.
		 * @param    visibility - visibility of sphere.
		 */
		void AddDome(
			const float3 & center = Defaults::SphereTriangulationCenter,
			f32 radius = Defaults::SphereTriangulationRadius,
			i32 depth = Defaults::SphereTriangulationDepth,
			ShapeVisibility visibility = Defaults::ShapeVisibility
		);

	public: // constructors
		/** Basic constructor. */
		TriangleModel(String textureName) : TextureName(textureName) {}
		/** Destructor. */
		virtual ~TriangleModel() override = default;
	};


	/************************************************************************/
	/* ColorVertex based model                                              */
	/************************************************************************/
	template<Gpu::IIndexBufferIndices INDEX>
	struct TriangleModel<ColorVertex, INDEX> : public TriangleModelBase<ColorVertex, INDEX> {
		using Base = TriangleModelBase<ColorVertex, INDEX>;
		using Index = typename Base::Index;
		using Triangle = typename Base::Triangle;
		using Vertex = typename Base::Vertex;
		using Base::AddTriangle;

	protected: // IModel
		virtual const char * iModelName() const override { return "TriangleModel"; }

	protected: // methods
		/** Add triangulated. */
		void triangulation(Triangle triangle, Color color, u32 depth = 0);

	public: // methods
		/**
		 *    H------G
		 *   /|     /|
		 *  E------F |
		 *  | D----|-C
		 *  |/     |/
		 *  A------B
		 */
		void AddCube(
			const float3 & a,
			const float3 & b,
			const float3 & c,
			const float3 & d,
			const float3 & e,
			const float3 & f,
			const float3 & g,
			const float3 & h,
			Color color
		);
		/** Add axis aligned polyhedron. */
		void AddAABox(const float3 & min, const float3 & max, Color color) { AddCube(min, { max.x, min.y, min.z }, { max.x, min.y, max.z }, { min.x, min.y, max.z }, { min.x, max.y, min.z }, { max.x, max.y, min.z }, max, { min.x, max.y, max.z }, color); }
		/**
		 * Add dome shape by defining.
		 * Triangulation is calculated automatically.
		 *
		 *           T (0,t,0)
		 *           |  C (0,0,-r)
		 *           | /
		 *           |/
		 * C---------X---------B
		 * (-r,0,0) /    (r,0,0)
		 *         /
		 *        A (0,0,r)
		 *
		 * @param center - center of dome
		 * @param radius - radius of horizontal space of dome
		 * @param top    - radius of vertical space of dome
		 * @param color	 - color of dome
		 */
		void AddDome(float3 center, f32 radius, f32 top, Color color);
		/** Add polyhedron aligned around Y axis - zig zag colored. */
		void AddPolyhedron(f32 minHeight, f32 maxHeight, const ArrayView1D<const float2> & data, Color color0, Color color1);
		/** Add polyhedron aligned around Y axis. */
		void AddPolyhedron(f32 minHeight, f32 maxHeight, const ArrayView1D<const float2> & data, Color color);
		/** Add polyhedron aligned around Y axis with random color. */
		void AddPolyhedron(f32 minHeight, f32 maxHeight, const ArrayView1D<const float2> & data);
		/** Add polygon aligned around Y axis. */
		void AddPolygon(f32 height, const ArrayView1D<const float2> & points, Color color);
		/** Add polygon aligned around Y axis with random color. */
		void AddPolygon(f32 height, const ArrayView1D<const float2> & points);
		/** Add pyramid with a n-angular pyramid.  */
		void AddPyramidal(f32 polygonHeight, float3 top, const ArrayView1D<const float2> & points, Color color);
		/** Add pyramid with a n-angular pyramid with random color.  */
		void AddPyramidal(f32 polygonHeight, float3 top, const ArrayView1D<const float2> & points);
		/** Add simple single-color-quad. */
		void AddQuad(float3 a, float3 b, float3 c, float3 d, Color color);
		/** Add simple single-color-quad with random color. */
		void AddQuad(float3 a, float3 b, float3 c, float3 d);
		/** Add simple, single-color-triangle. */
		void AddTriangle(float3 a, float3 b, float3 c, Color color);
		/** Add simple, single-color-triangle with random color. */
		void AddTriangle(float3 a, float3 b, float3 c);
		/** Translate points in by given param. */
		void Translate(const float3 & translation);

	public: // constructors
		/** Destructor. */
		virtual ~TriangleModel() override = default;
	};
}


namespace DD::Simulation { // definitions
	/************************************************************************/
	/* TriangleModelBase                                                    */
	/************************************************************************/
#pragma region TriangleModelBase
	template<typename VERTEX, Gpu::IIndexBufferIndices INDEX>
	void TriangleModelBase<VERTEX, INDEX>::iModelInit(Gpu::Device & device) {
		_device = device;
		_boundingBox.Reset();
		for (const auto & vertex : Vertices)
			_boundingBox.Include(vertex.Position);
		_boundingSphere = _boundingBox.BoundingSphere();

		Vertices.Init(device);
		Triangles.Init(device);
		PixelShader = device->GetPixelShader(L"PSSimple");
		VertexShader = device->GetVertexShader<VERTEX>(L"VSSimple");
	}


	template<typename VERTEX, Gpu::IIndexBufferIndices INDEX>
	void TriangleModelBase<VERTEX, INDEX>::iModelDraw(DrawArgs & args) {
		switch (args.Mode)
		{
		case DrawArgs::DrawModes::Normal:
				Vertices.Bind();
				Triangles.Bind();
				_device->SetPrimitiveTopology(Gpu::Devices::PrimitiveTopologies::TriangleList);
				PixelShader->Set();
				VertexShader->Set();
				_device->DrawIndexed((u32)Triangles.Size() * 3);
				break;

			default:
				throw;
				break;
		}
	}


	template<typename VERTEX, Gpu::IIndexBufferIndices INDEX>
	void TriangleModelBase<VERTEX, INDEX>::AddTriangleModel(const TriangleModelBase & m) {
		const Triangle offset = (Index)Vertices.Size();
		const Triangle shift{ offset, offset, offset };
		Vertices.AddIterable(m.Vertices);
		Triangles.Reserve(m.Triangles.Size());
		for (const Triangle & triangle : m.Triangles) {
			Triangles.AddUnsafe(triangle + shift);
		}
	}


	template<typename VERTEX, Gpu::IIndexBufferIndices INDEX>
	void TriangleModelBase<VERTEX, INDEX>::AddTriangleModel(
		const TriangleModelBase & m,
		const float3x3 & transformation,
		const float3 & position
	) {
		// create offset for triangle indices
		const Index offset = (Index)Vertices.Size();
		const Triangle shift{ offset, offset, offset };
		// preallocate memory for vertices
		Vertices.Reserve(m.Vertices.Size());
		// copy from vertices into local vertex to make local changes
		for (Vertex vertex : m.Vertices) {
			vertex.Position = transformation * vertex.Position + position;
			Vertices.AddUnsafe(vertex);
		}
		// preallocate memory for indices
		Triangles.Reserve(m.Triangles.Size());
		// copy indices
		for (const Triangle & triangle : m.Triangles) {
			Triangles.AddUnsafe(triangle + shift);
		}
	}


	template<typename VERTEX, Gpu::IIndexBufferIndices INDEX>
	void TriangleModelBase<VERTEX, INDEX>::AddTriangle(
		const Vertex & v0, const Vertex & v1, const Vertex & v2
	) {
		Index index = (Index)Vertices.Size();
		Vertices.Add(v0, v1, v2);
		Triangles.Add(Triangle{ index, index + 1, index + 2 });
	}


	template<typename VERTEX, Gpu::IIndexBufferIndices INDEX>
	void TriangleModelBase<VERTEX, INDEX>::Clean() {
		// make reindexing table
		Array<Index> vertexIndexTable = Array<Index>::Uniform(Vertices.Size(), 0);
		// mark used vertices
		for (const Triangle & t : Triangles) {
			vertexIndexTable[t.x] = 1;
			vertexIndexTable[t.y] = 1;
			vertexIndexTable[t.z] = 1;
		}

		// count resting vertices after data shrink
		Index vertexCount = 0;
		// shrink data
		for (Index index = 0, limit = (Index)Vertices.Size(); index < limit; ++index) {
			// mark to be kept
			if (vertexIndexTable[index]) {
				// need for data move
				if (index != vertexCount) {
					// move data
					Vertices[vertexCount] = Fwd(Vertices[index]);
				}
				// set current position for triangle remapping
				vertexIndexTable[index] = vertexCount++;
			}
		}

		// update vertex buffer size
		Vertices.Resize(vertexCount);

		// remap indices
		for (Triangle & t : Triangles) {
			t.x = vertexIndexTable[t.x];
			t.y = vertexIndexTable[t.y];
			t.z = vertexIndexTable[t.z];
		}
	}


	template<typename VERTEX, Gpu::IIndexBufferIndices INDEX>
	void TriangleModelBase<VERTEX, INDEX>::Clip(const Math::Interval3F & box) {
		// select vertices to out of box
		Array<size_t> remove(Vertices.Size());
		for (Index index = 0, limit = (Index)Vertices.Size(); index < limit; ++index)
			remove[index] = box.ClipHash<size_t>(Vertices[index].Position);

		// select triangles containing the vertices
		// check if triangles has intersection re-mesh them, otherwise remove
		List<Triangle> newList;
		for (size_t triangleIndex = 0, triagleLimit = Triangles.Size(); triangleIndex < triagleLimit; ++triangleIndex) {
			// load triangle
			Triangle & triangle = Triangles[triangleIndex];

			// triangle not clipped, skip
			if (remove[triangle.x] == 0 && remove[triangle.y] == 0 && remove[triangle.z] == 0) {
				newList.Add(triangle);
				continue;
			}

			// triangle is fully clipped (all vertices clipped by same plane)
			size_t clip = remove[triangle.x] & remove[triangle.y] & remove[triangle.z];
			if (clip)
				continue;

			// what planes need to be applied for clipping
			size_t cleanFlags = remove[triangle.x] | remove[triangle.y] | remove[triangle.z];
			// convex polygon in 3d, initialized by triangle
			List<VERTEX, 16> polygon;
			polygon.AppendUnsafe(Vertices[triangle.x]);
			polygon.AppendUnsafe(Vertices[triangle.y]);
			polygon.AppendUnsafe(Vertices[triangle.z]);
			// do the clipping
			for (size_t flagIndex = 0; flagIndex < 6; ++flagIndex) {
				u8 flag = 1 << flagIndex;
				if (cleanFlags & flag) {
					enum Modes { less, greater } mode = (Modes)(flagIndex % 2);
					enum Dim {x, y, z} dimension = (Dim)(flagIndex / 2); // x-dim
					const auto & limit = mode == less ? box.Min[dimension] : box.Max[dimension];
					for (size_t i = 0; i < polygon.Size(); ++i) {
						// go by triples, focus on the center point
						VERTEX & B = polygon[(i + 1) % polygon.Size()];
						const auto & b = B.Position[dimension];
						const bool bValid = !(mode == less ? (b < limit) : (limit < b));
						// if center point does not break the box, continue
						if (bValid)
							continue;
						// prepare pint data
						VERTEX & A = polygon[i];
						const auto & a = A.Position[dimension];
						const bool aValid = !(mode == less ? (a < limit) : (limit < a));
						// prepare pint data
						VERTEX & C = polygon[(i + 2) % polygon.Size()];
						const auto & c = C.Position[dimension];
						const bool cValid = !(mode == less ? (c < limit) : (limit < c));
						//     B     ||  B------A/C
						//    / \    ||  |
						// ----------||------------- limit
						//  /     \  ||  |
						// C       A || C/A
						// move B towards A
						if (aValid && !cValid) {
							auto aratio = (b - limit ) / (b-a);
							B.Position -= (B.Position - A.Position) * aratio;
							B.Position[dimension] = limit;
						}
						// move B towards C
						else if (!aValid && cValid) {
							auto cratio = (b - limit ) / (b-c);
							B.Position -= (B.Position - C.Position) * cratio;
							B.Position[dimension] = limit;
						}
						// split vertex
						else if (aValid && cValid) {
							// compute new vertex
							VERTEX BA = B;
							auto aratio = (b - limit ) / (b-a);
							BA.Position -= (B.Position - A.Position) * aratio;
							BA.Position[dimension] = limit;
							// compute new vertex
							VERTEX BC = B;
							auto cratio = (b - limit ) / ( b-c);
							BC.Position -= (B.Position - C.Position) * cratio;
							BC.Position[dimension] = limit;
							// place back
							if (Math::Abs(1.f - aratio) < 1E-5) {
								B = BC;
							}
							else if (Math::Abs(1.f - cratio) < 1E-5) {
								B = BA;
							}
							else {
								B = BA;
								polygon.Insert((i + 2) % polygon.Size(), BC);
							}
						}
					}
				}
			}
			// re-triangulate
			Index centerIndex = (Index)Vertices.Size();
			Vertices.Add(Vertices[triangle.x]);
			Vertices[centerIndex].Position = 0.0f;
			for (auto & vertex : polygon) {
				// saturate
				vertex.Position = box.Saturate(vertex.Position);
				// accumulate centroid like point
				Vertices[centerIndex].Position += vertex.Position;
			}
			// normalize
			Vertices[centerIndex].Position *= 1.f / polygon.Size();

			for (Index i = 0, l = (Index)polygon.Size(); i < l; ++i) {
				// push the vertex to the collection
				Vertices.Add(polygon[i]);
				// check for degenerated
				auto & X = polygon[i].Position;
				auto & Y = polygon[(i + 1) %l].Position;
				auto & Z = Vertices[centerIndex].Position;
				bool dx = X.x == Y.x && Y.x == Z.x;
				bool dy = X.y == Y.y && Y.y == Z.y;
				bool dz = X.z == Y.z && Y.z == Z.z;
				// clipped triangle is not a line
				if (dx + dy + dz < 2) {
					newList.Add(Triangle{ centerIndex + i + 1, centerIndex + 1 + ((i + 1) % l), centerIndex });
					newList.Add(Triangle{ centerIndex + i + 1, centerIndex, centerIndex + 1 + ((i + 1) % l) });
				}
			}
		}

		// remove unused triangles
		(List<Triangle>&)Triangles = Fwd(newList);
		// remove unused vertices
		Clean();
	}


	template<typename VERTEX, Gpu::IIndexBufferIndices INDEX /*= Gpu::IIndexBufferIndices::_16Bit*/>
	void DD::Simulation::TriangleModelBase<VERTEX, INDEX>::AddBlanket(
		const ArrayView2D<VERTEX> & vertices
	) {
		// items must be at least 2x2
		DD_TRIANGLEMODEL_ASSERT(vertices.Lengths().UnaryAnd([](size_t i) {return 1 < i; }));

		// safe zero offset for indexing
		Index zero = (Index)Vertices.Size();
		// copy vertex data
		Vertices.AddIterable(vertices.PlainView());

		// create triangulation
		//Triangles.Reserve(2 * (vertices.Height() - 1) * (vertices.Width() - 1));
		size_t quadNum = vertices.Lengths().Convert([](size_t l) { return l - 1; }).Mul();
		Triangles.Reserve(2 * quadNum);
		for (size_t row = 1; row < vertices.Length(0); ++row) {
			for (size_t col = 1; col < vertices.Length(1); ++col) {
				// prepare indices
				// D---C
				// |   |
				// A---B
				Index a = zero + (Index)vertices.Offset({row, col - 1});
				Index b = zero + (Index)vertices.Offset({row, col});
				Index c = zero + (Index)vertices.Offset({row - 1, col});
				Index d = zero + (Index)vertices.Offset({ row - 1, col - 1 });

				// create triangles
				Triangle t0(a, b, c);
				Triangle t1(a, c, d);

				// save triangles
				Triangles.AddUnsafe(t0, t1);
			}
		}
	}


	template<typename VERTEX, Gpu::IIndexBufferIndices INDEX /*= Gpu::IIndexBufferIndices::_16Bit*/>
	template<typename T>
	void TriangleModelBase<VERTEX, INDEX>::NormalFilter(const DD::Vector<T, 3> & normal) {
		Triangles.RemoveAll([this, &normal](Triangle triangle) {
			const auto & a = Vertices[triangle.x].Position;
			const auto & b = Vertices[triangle.y].Position;
			const auto & c = Vertices[triangle.z].Position;

			auto triangleNormal = Math::Cross(a - b, b - c);
			return Math::Dot(triangleNormal, normal) >= 0;
		});

		Clean();
	}


	template<typename VERTEX, Gpu::IIndexBufferIndices INDEX>
	template<typename COMPARER, typename EQCOMPARER>
	inline void TriangleModelBase<VERTEX, INDEX>::Compress(const COMPARER & comparer, const EQCOMPARER & eqComparer) {
		// table for reindexing vertices
		Array<Index> indexTable(Vertices.Size());
		Array<Index> sortedVertices(Vertices.Size());
		for (Index i = 0, l = (Index)Vertices.Size(); i < l; ++i)
			sortedVertices[i], indexTable[i] = i;

		// sort verices according to given comparer
		HeapLib::Sort(sortedVertices, [this, &comparer](Index lhs, Index rhs) {
			return comparer(Vertices[lhs], Vertices[rhs]);
			});

		// head is the index of vertex that is gonna be preserved, scan is index that is being tested
		for (Index head = 0, scan = 1; scan < Vertices.Size();) {
			if (eqComparer(Vertices[head], Vertices[scan]))
				indexTable[scan++] = head;
			else
				head = scan++;
		}

		// apply reindexing
		for (Triangle & t : Triangles)
			t.x = indexTable[t.x], t.y = indexTable[t.y], t.z = indexTable[t.z];

		// remove unused vertices
		Clean();
	}
#pragma endregion


	/************************************************************************/
	/* TriangleModel<Simulation::ColorVertex>                                           */
	/************************************************************************/
#pragma region TriangleModel<Simulation::ColorVertex>
	template<Gpu::IIndexBufferIndices INDEX>
	void TriangleModel<ColorVertex, INDEX>::triangulation(
		Triangle triangle,
		Color color,
		u32 depth
	) {
		if (depth) {
			//      C      triangulation
			//     / \     scheme
			//    Z___Y
			//   / \ / \
			//  A___X___B
			// vertices of triangle
			const Vertex & A = Base::Vertices[triangle.x];
			const Vertex & B = Base::Vertices[triangle.y];
			const Vertex & C = Base::Vertices[triangle.z];
			// new vertices
			Vertex X, Y, Z;
			X.Color = Y.Color = Z.Color = color;
			X.Position = (A.Position + B.Position) * 0.5f;
			Y.Position = (B.Position + C.Position) * 0.5f;
			Z.Position = (C.Position + A.Position) * 0.5f;
			Index iX = (Index)Base::Vertices.Size();
			Base::Vertices.Add(X, Y, Z);
			// triangulate it
			triangulation(Triangle(triangle.x, iX, iX + 2), color, depth - 1);     // AXZ
			triangulation(Triangle(iX, triangle.y, iX + 1), color, depth - 1);     // XBY
			triangulation(Triangle(iX + 2, iX + 1, triangle.z), color, depth - 1); // ZYC
			triangulation(Triangle(iX + 0, iX + 1, iX + 2), color, depth - 1);     // XYZ
		}
		else {
			Base::Triangles.Add(triangle);
		}
	}


	template<Gpu::IIndexBufferIndices INDEX>
	void TriangleModel<ColorVertex, INDEX>::AddCube(
		const float3 & a,
		const float3 & b,
		const float3 & c,
		const float3 & d,
		const float3 & e,
		const float3 & f,
		const float3 & g,
		const float3 & h,
		Color color
	) {
		// index of A vertex
		Index A = (Index)(Base::Vertices.Size());
		Index B = A + 1;
		Index C = B + 1;
		Index D = C + 1;
		Index E = D + 1;
		Index F = E + 1;
		Index G = F + 1;
		Index H = G + 1;
		// add all vertices
		Base::Vertices.Add(
			Vertex{a, color},
			Vertex{b, color},
			Vertex{c, color},
			Vertex{d, color},
			Vertex{e, color},
			Vertex{f, color},
			Vertex{g, color},
			Vertex{h, color}
		);
		// register indices
		Base::Triangles.Add(
			Triangle(A, C, B),
			Triangle(A, D, C),
			Triangle(E, F, G),
			Triangle(E, G, H),
			Triangle(A, B, F),
			Triangle(A, F, E),
			Triangle(B, C, G),
			Triangle(B, G, F),
			Triangle(C, D, H),
			Triangle(C, H, G),
			Triangle(D, A, E),
			Triangle(D, E, H)
		);
	}


	template<Gpu::IIndexBufferIndices INDEX>
	void TriangleModel<ColorVertex, INDEX>::AddDome(
		float3 center,
		f32 radius,
		f32 top,
		Color color
	) {
		// index of A vertex
		Index iA = (Index)(Base::Vertices.Size());
		// init vertices
		float3 A(0.f, 0.f, 1.f);
		float3 B(1.f, 0.f, 0.f);
		float3 C(0.f, 0.f, -1.f);
		float3 D(-1.f, 0.f, 0.f);
		float3 T(0.f, 1.f, 0.f);
		// add initial vertices
		Base::Vertices.Add(
			Vertex({ A, color }),
			Vertex({ B, color }),
			Vertex({ C, color }),
			Vertex({ D, color }),
			Vertex({ T, color })
		);

		// add initial triangles
		Triangle ABT(iA + 0, iA + 1, iA + 4); // ABT
		Triangle ATB(iA + 0, iA + 4, iA + 1); // ATB
		Triangle BCT(iA + 1, iA + 2, iA + 4); // BCT
		Triangle BTC(iA + 1, iA + 4, iA + 2); // BTC
		Triangle CDT(iA + 2, iA + 3, iA + 4); // CDT
		Triangle CTD(iA + 2, iA + 4, iA + 3); // CTD
		Triangle DAT(iA + 3, iA + 0, iA + 4); // DAT
		Triangle DTA(iA + 3, iA + 4, iA + 0); // DTA

		// triangulation method
		triangulation(ABT, color, 3);
		triangulation(ATB, color, 3);
		triangulation(BCT, color, 3);
		triangulation(BTC, color, 3);
		triangulation(CDT, color, 3);
		triangulation(CTD, color, 3);
		triangulation(DAT, color, 3);
		triangulation(DTA, color, 3);

		// recompute coordinates
		float3 normCoef(radius, top, radius);
		for (size_t i = iA, m = Base::Vertices.Size(); i < m; ++i) {
			float3 & position = Base::Vertices[i].Position;
			position /= Math::Norm(position);
			position *= normCoef;
			position += center;
		}
	}


	template<Gpu::IIndexBufferIndices INDEX>
	void TriangleModel<ColorVertex, INDEX>::AddPolyhedron(
		f32 minHeight,
		f32 maxHeight,
		const ArrayView1D<const float2> & data,
		Color color0,
		Color color1
	) {
		size_t first = Base::Vertices.Size();
		size_t c = 0;
		Base::Vertices.Reserve(data.Length() * 2 + 2);
		for (const float2 & point : data) {
			const Color & color = ++c % 2 ? color0 : color1;
			ColorVertex v0 = { { point.x, maxHeight, point.y }, color };
			ColorVertex v1 = { { point.x, minHeight, point.y }, color };
			Base::Vertices.AddUnsafe(v0, v1);
		}

		Base::Vertices.AddUnsafe(
			ColorVertex{ {data[0].x, maxHeight, data[0].y}, color0 },
			ColorVertex{ {data[0].x, minHeight, data[0].y}, color1 }
		);

		Base::Triangles.Reserve(data.Length() * 4);
		for (size_t i = first, l = Base::Vertices.Size() - 2; i < l; ++i) {
			Base::Triangles.AddUnsafe(
				Triangle((Index)i, (Index)(i + 1), (Index)(i + 2)),
				Triangle((Index)i, (Index)(i + 2), (Index)(i + 1))
			);
		}
	}


	template<Gpu::IIndexBufferIndices INDEX>
	void TriangleModel<ColorVertex, INDEX>::AddPolyhedron(
		f32 minHeight,
		f32 maxHeight,
		const ArrayView1D<const float2> & data,
		Color color
	) {
		AddPolyhedron(minHeight, maxHeight, data, color, color);
	}


	template<Gpu::IIndexBufferIndices INDEX>
	void TriangleModel<ColorVertex, INDEX>::AddPolyhedron(
		f32 minHeight,
		f32 maxHeight,
		const ArrayView1D<const float2> & data
	) {
		static thread_local size_t COLOR = 0;
		const Color color0 = Color::Collection()[COLOR++];
		const Color color1 = Color::Collection()[COLOR];

		AddPolyhedron(minHeight, maxHeight, data, color0, color1);
	}


	template<Gpu::IIndexBufferIndices INDEX>
	void TriangleModel<ColorVertex, INDEX>::Translate(
		const float3 & translation
	) {
		for (Vertex & v : Base::Vertices) {
			v.Position += translation;
		}
	}


	template<Gpu::IIndexBufferIndices INDEX>
	void TriangleModel<ColorVertex, INDEX>::AddPolygon(
		f32 height,
		const ArrayView1D<const float2> & points,
		Color color
	) {
		float2 pseudocentroid = 0.f;
		for (const float2 & pt : points) {
			pseudocentroid += pt;
		}
		pseudocentroid /= (f32)points.Length();

		Index firstVertex = (Index)Base::Vertices.Size();
		Vertex v;
		v.Position.y = height;
		Base::Vertices.Reserve(points.Length() + 1);
		v.Color = color;
		for (const float2 & pt : points) {
			v.Position.xz = pt;
			Base::Vertices.AddUnsafe(v);
		}

		Index centroidIndex = (Index)Base::Vertices.Size();
		v.Position.xz = pseudocentroid;
		Base::Vertices.AddUnsafe(v);

		Base::Triangles.Reserve(points.Length() * 2);
		for (Index i = 0, m = (Index)points.Length(); i < m; ++i) {
			Index i0 = i + firstVertex;
			Index i1 = ((i + 1) % m) + firstVertex;
			Triangle t0(i0, i1, centroidIndex);
			Triangle t1(i1, i0, centroidIndex);
			Base::Triangles.AddUnsafe(t0, t1);
		}
	}


	template<Gpu::IIndexBufferIndices INDEX>
	void TriangleModel<ColorVertex, INDEX>::AddPolygon(
		f32 height,
		const ArrayView1D<const float2> & points
	) {
		static size_t COLOR = 0;
		const Color color = Color::Collection()[COLOR++];

		AddPolygon(height, points, color);
	}


	template<Gpu::IIndexBufferIndices INDEX>
	void TriangleModel<ColorVertex, INDEX>::AddPyramidal(
		f32 polygonHeight,
		float3 top,
		const ArrayView1D<const float2> & points,
		Color color
	) {
		Index firstVertex = (Index)Base::Vertices.Size();
		Vertex v;
		v.Position.y = polygonHeight;
		v.Color = color;
		Base::Vertices.Reserve(points.Length() + 1);
		for (const float2 & pt : points) {
			v.Position.xz = pt;
			Base::Vertices.AddUnsafe(v);
		}

		Index topIndex = (Index)Base::Vertices.Size();
		v.Position = top;
		Base::Vertices.AddUnsafe(v);

		Base::Triangles.Reserve(points.Length() * 2);
		for (Index i = 0, m = (Index)points.Length(); i < m; ++i) {
			Index i0 = i + firstVertex;
			Index i1 = ((i + 1) % m) + firstVertex;
			Triangle t0(i0, i1, topIndex);
			Triangle t1(i1, i0, topIndex);
			Base::Triangles.AddUnsafe(t0, t1);
		}
	}


	template<Gpu::IIndexBufferIndices INDEX>
	void TriangleModel<ColorVertex, INDEX>::AddPyramidal(
		f32 polygonHeight,
		float3 top,
		const ArrayView1D<const float2> & points
	) {
		static size_t COLOR = 0;
		const Color color = Color::Collection()[COLOR++];

		AddPyramidal(polygonHeight, points, color);
	}


	template<Gpu::IIndexBufferIndices INDEX>
	void TriangleModel<ColorVertex, INDEX>::AddTriangle(
		float3 a,
		float3 b,
		float3 c,
		Color color
	) {
		Index first = (Index)Base::Vertices.Size();
		Vertex v;

		v.Color = color;

		Base::Vertices.Reserve(3);
		v.Position = a;
		Base::Vertices.AddUnsafe(v);
		v.Position = b;
		Base::Vertices.AddUnsafe(v);
		v.Position = c;
		Base::Vertices.AddUnsafe(v);

		Base::Triangles.Add(
			Triangle(first, first + 1, first + 2),
			Triangle(first + 1, first, first + 2)
		);
	}


	template<Gpu::IIndexBufferIndices INDEX>
	void TriangleModel<ColorVertex, INDEX>::AddTriangle(
		float3 a,
		float3 b,
		float3 c
	) {
		static size_t COLOR = 0;
		const Color color = Color::Collection()[COLOR++];

		AddTriangle(a, b, c, color);
	}


	template<Gpu::IIndexBufferIndices INDEX>
	void TriangleModel<ColorVertex, INDEX>::AddQuad(
		float3 a,
		float3 b,
		float3 c,
		float3 d,
		Color color
	) {
		Index ia = (Index)Base::Vertices.Size();
		Index ib = ia + 1;
		Index ic = ia + 2;
		Index id = ia + 3;
		Vertex v;

		v.Color = color;

		Base::Vertices.Reserve(4);
		v.Position = a;
		Base::Vertices.AddUnsafe(v);
		v.Position = b;
		Base::Vertices.AddUnsafe(v);
		v.Position = c;
		Base::Vertices.AddUnsafe(v);
		v.Position = d;
		Base::Vertices.AddUnsafe(v);

		Base::Triangles.Add(
			Triangle(ia, ib, ic),
			Triangle(ib, ia, ic),
			Triangle(ia, id, ic),
			Triangle(id, ia, ic)
		);
	}


	template<Gpu::IIndexBufferIndices INDEX>
	void TriangleModel<ColorVertex, INDEX>::AddQuad(
		float3 a,
		float3 b,
		float3 c,
		float3 d
	) {
		static size_t COLOR = 0;
		const Color color = Color::Collection()[COLOR++];

		AddQuad(a, b, c, d, color);
	}
#pragma endregion

	/************************************************************************/
	/* TriangleModel<TextureVertex>                                         */
	/************************************************************************/
#pragma region TriangleModel<TextureVertex>
	template<Gpu::IIndexBufferIndices INDEX>
	void TriangleModel<TextureVertex, INDEX>::iModelInit(Gpu::Device & device) {
		Base::_device = device;
		for (Vertex & vertex : Base::Vertices)
			Base::_boundingBox.Include(vertex.Position);

		Base::_boundingSphere = Base::_boundingBox.BoundingSphere();

		Base::Vertices.Init(device);
		Base::Triangles.Init(device);
		Texture = device->GetTexture(TextureName);
		Base::PixelShader = device->GetPixelShader(L"PSTexture");
		Base::VertexShader = device->GetVertexShader<TextureVertex>(L"VSTexture");
	}


	template<Gpu::IIndexBufferIndices INDEX>
	void TriangleModel<TextureVertex, INDEX>::iModelDraw() {
		Base::Vertices.Bind();
		Base::Triangles.Bind();
		Texture->Bind2PS();
		Base::_device->SetPrimitiveTopology(Gpu::Devices::PrimitiveTopologies::TriangleList);
		Base::PixelShader->Set();
		Base::VertexShader->Set();
		Base::_device->DrawIndexed((u32)Base::Triangles.Size() * 3);
	}


	template<Gpu::IIndexBufferIndices INDEX>
	void TriangleModel<TextureVertex, INDEX>::sphereTriangleSplit(Index a, Index b, Index c, Array<Triangle> & out) {
		Index d = static_cast<Index>(Base::Vertices.Size());
		Index e = d + 1;
		Index f = e + 1;

		Base::Vertices.Add(sphereAverageVertex(Base::Vertices[a], Base::Vertices[b]));
		Base::Vertices.Add(sphereAverageVertex(Base::Vertices[c], Base::Vertices[b]));
		Base::Vertices.Add(sphereAverageVertex(Base::Vertices[a], Base::Vertices[c]));

		out[0] = Triangle(a, d, f);
		out[1] = Triangle(d, e, f);
		out[2] = Triangle(d, b, e);
		out[3] = Triangle(f, e, c);
	}


	template<Gpu::IIndexBufferIndices INDEX>
	typename TriangleModel<TextureVertex, INDEX>::Vertex
		Simulation::TriangleModel<TextureVertex, INDEX>::sphereAverageVertex(const Vertex & a, const Vertex & b) {
		Vertex result;
		result.Position = Math::Normalize(((a.Position + b.Position) * 0.5f));
		result.Texture = ((a.Texture + b.Texture) * 0.5f);
		return result;
	}


	template<Gpu::IIndexBufferIndices INDEX>
	void TriangleModel<TextureVertex, INDEX>::sphereAddTriangle(Index a, Index b, Index c, ShapeVisibility v) {
		if (v & ShapeVisibility::Inside)
			Base::Triangles.Add(Triangle{ a, b, c });
		if (v & ShapeVisibility::Outside)
			Base::Triangles.Add(Triangle{ b, a, c });
	}


	template<Gpu::IIndexBufferIndices INDEX>
	void TriangleModel<TextureVertex, INDEX>::sphereTriangulationDepth(Index a, Index b, Index c, u32 depth, ShapeVisibility visibility) {
		if (depth == 0) {
			sphereAddTriangle(a, b, c, visibility);
		}
		else {
			Array<Triangle, 4> arr;
			sphereTriangleSplit(a, b, c, arr);
			for (Triangle t : arr) {
				sphereTriangulationDepth(t.x, t.y, t.z, depth - 1, visibility);
			}
		}
	}


	template<Gpu::IIndexBufferIndices INDEX>
	void TriangleModel<TextureVertex, INDEX>::sphereTriangulationSurface(
		Index _a,
		Index _b,
		Index _c,
		f32 surface,
		ShapeVisibility visibility
	) {
		f32 a = (Base::Vertices[_b].Position - Base::Vertices[_c].Position).Norm();
		f32 b = (Base::Vertices[_a].Position - Base::Vertices[_c].Position).Norm();
		f32 c = (Base::Vertices[_a].Position - Base::Vertices[_b].Position).Norm();
		f32 h = (a + b + c) / 2;
		f32 s = Math::Sqrt(h * (h - a) * (h - b) * (h - c));

		if (s < surface) {
			sphereAddTriangle(_a, _b, _c, visibility);
		}
		else {
			Array<Triangle, 4> arr;
			sphereTriangleSplit(_a, _b, _c, arr);
			for (Triangle t : arr)
				sphereTriangulationSurface(t.x, t.y, t.z);
		}
	}


	template<Gpu::IIndexBufferIndices INDEX>
	void TriangleModel<TextureVertex, INDEX>::AddSpherePolar(
		const float3 & center,
		f32 radius,
		i32 depth,
		ShapeVisibility visibility
	)	{
		if (visibility) {
			Index s = 1 << depth;
			Index t = s * s;
			Index n = s + 1;
			Index o = n * n;

			Array<double3x3> rotationZ(n);
			for (Index row = 0; row < n; ++row)
				rotationZ[row] = Matrices::RotationZ(3.14159265 * ((1.0 * row / s) - 0.5));

			Array<double3x3> rotationY(n);
			for (Index col = 0; col < n; ++col)
				rotationY[col] = Matrices::RotationY(3.14159265 * (2.0 * col / s));

			Index numOfVertices = (Index)Base::Vertices.Size();

			Base::Vertices.Resize(o + numOfVertices);
			for (Index row = 0; row < n; ++row) {
				for (Index col = 0; col < n; ++col) {
					// double calculation for better precision
					double2 texture(((f64)col / s), ((f64)row / s));
					double3 position(1.0, 0.f, 0.f);
					position = position * rotationZ[row];
					position = position * rotationY[col];
					Base::Vertices[numOfVertices + row * n + col] = { center + Math::Normalize(position) * radius, texture };
				}
			}

			Base::Triangles.Reserve(t * 2);
			for (Index row = 0; row < s; ++row) {
				for (Index col = 0; col < s; ++col) {
					Index A = numOfVertices + row * n + col;
					Index B = numOfVertices + A + 1;
					Index D = numOfVertices + A + n;
					Index C = numOfVertices + D + 1;
					sphereAddTriangle(B, A, C, visibility);
					sphereAddTriangle(C, A, D, visibility);
				}
			}
		}
	}


	template<Gpu::IIndexBufferIndices INDEX>
	void TriangleModel<TextureVertex, INDEX>::AddSphereOctahedron(
		const float3 & center,
		f32 radius,
		i32 depth,
		ShapeVisibility visibility
	) {
		enum { A, B, C, D, E, IAB, IBC, ICD, IDE, JAB, JBC, JCD, JDE, SIZE };
		Index numOfVertices = static_cast<Index>(Base::Vertices.Size());
		Base::Vertices.Resize(numOfVertices + SIZE);
		Base::Vertices[numOfVertices +   A] = { {  0.f,  0.f,  1.f }, { 0.000f, 0.5f } };
		Base::Vertices[numOfVertices +   B] = { {  1.f,  0.f,  0.f }, { 0.250f, 0.5f } };
		Base::Vertices[numOfVertices +   C] = { {  0.f,  0.f, -1.f }, { 0.500f, 0.5f } };
		Base::Vertices[numOfVertices +   D] = { { -1.f,  0.f,  0.f }, { 0.750f, 0.5f } };
		Base::Vertices[numOfVertices +   E] = { {  0.f,  0.f,  1.f }, { 1.000f, 0.5f } };
		Base::Vertices[numOfVertices + IAB] = { {  0.f, -1.f,  0.f }, { 0.125f, 1.0f } };
		Base::Vertices[numOfVertices + IBC] = { {  0.f, -1.f,  0.f }, { 0.375f, 1.0f } };
		Base::Vertices[numOfVertices + ICD] = { {  0.f, -1.f,  0.f }, { 0.625f, 1.0f } };
		Base::Vertices[numOfVertices + IDE] = { {  0.f, -1.f,  0.f }, { 0.875f, 1.0f } };
		Base::Vertices[numOfVertices + JAB] = { {  0.f,  1.f,  0.f }, { 0.125f, 0.0f } };
		Base::Vertices[numOfVertices + JBC] = { {  0.f,  1.f,  0.f }, { 0.375f, 0.0f } };
		Base::Vertices[numOfVertices + JCD] = { {  0.f,  1.f,  0.f }, { 0.625f, 0.0f } };
		Base::Vertices[numOfVertices + JDE] = { {  0.f,  1.f,  0.f }, { 0.875f, 0.0f } };

		float3 up(0.f, 1.f, 0.f);
		float2 front(0.f, 1.f);
		// top part of sphere
		{
			size_t from = Base::Vertices.Size();
			sphereTriangulationDepth(numOfVertices + E, numOfVertices + D, numOfVertices + IDE, depth, visibility);
			sphereTriangulationDepth(numOfVertices + D, numOfVertices + E, numOfVertices + JDE, depth, visibility);
			sphereTriangulationDepth(numOfVertices + C, numOfVertices + D, numOfVertices + JCD, depth, visibility);
			sphereTriangulationDepth(numOfVertices + D, numOfVertices + C, numOfVertices + ICD, depth, visibility);
			size_t to = Base::Vertices.Size();

			for (size_t i = from; i < to; ++i) {
				Base::Vertices[i].Texture.y = (acos(cos(Base::Vertices[i].Position, up)) / 3.14159265f);
				Base::Vertices[i].Texture.x = 1.f - acos(cos(Base::Vertices[i].Position.xz, front)) / (3.14159265f * 2.f);
			}
		}
		// bottom part of sphere
		{
			size_t from = Base::Vertices.Size();
			sphereTriangulationDepth(numOfVertices + A, numOfVertices + B, numOfVertices + JAB, depth, visibility);
			sphereTriangulationDepth(numOfVertices + B, numOfVertices + A, numOfVertices + IAB, depth, visibility);
			sphereTriangulationDepth(numOfVertices + B, numOfVertices + C, numOfVertices + JBC, depth, visibility);
			sphereTriangulationDepth(numOfVertices + C, numOfVertices + B, numOfVertices + IBC, depth, visibility);
			size_t to = Base::Vertices.Size();

			for (size_t i = from; i < to; ++i) {
				Base::Vertices[i].Texture.y = Math::Saturate(acos(cos(Base::Vertices[i].Position, up)) / 3.14159265f);
				Base::Vertices[i].Texture.x = Math::Saturate(acos(cos(Base::Vertices[i].Position.xz, front)) / (3.14159265f * 2.f));
			}
		}

		for (Vertex & vertex : Base::Vertices) {
			vertex.Position *= radius;
			vertex.Position += center;
		}

	}


	template<Gpu::IIndexBufferIndices INDEX>
	void TriangleModel<TextureVertex, INDEX>::AddSphere(
		const float3 & center,
		f32 radius,
		i32 depth,
		ShapeVisibility visibility,
		SphereTriangulation triangulation
	) {
 		switch (triangulation) {
 			case SphereTriangulation::Octahedron:
 				AddSphereOctahedron(center, radius, depth, visibility);
 				break;
 			case SphereTriangulation::Polar:
 				AddSpherePolar(center, radius, depth, visibility);
 				break;
 			default:
 				throw;
 		}
	}


	template<Gpu::IIndexBufferIndices INDEX>
	void TriangleModel<TextureVertex, INDEX>::AddDome(
		const float3 & center,
		f32 radius,
		i32 depth,
		ShapeVisibility visibility
	) {
		Index offset = static_cast<Index>(Base::Vertices.Size());
		enum {A, B, C, D, T, SIZE};
		Base::Vertices.Resize(offset + SIZE);

		Base::Vertices[offset + A] = { { 0.0f, 0.0f, 1.0f }, { 0.0f, 0.0f } };
		Base::Vertices[offset + B] = { { 1.0f, 0.0f, 0.0f }, { 1.0f, 0.0f } };
		Base::Vertices[offset + C] = { { 0.f, 0.0f, -1.0f }, { 1.0f, 1.0f } };
		Base::Vertices[offset + D] = { { -1.0f, 0.0f, 0.f }, { 0.0f, 1.0f } };
		Base::Vertices[offset + T] = { { 0.0f, 1.0f, 0.0f }, { 0.5f, 0.5f } };

		sphereTriangulationDepth(A, B, T, depth, visibility);
		sphereTriangulationDepth(B, C, T, depth, visibility);
		sphereTriangulationDepth(C, D, T, depth, visibility);
		sphereTriangulationDepth(D, A, T, depth, visibility);

		for (Vertex & vertex : Base::Vertices) {
			vertex.Position *= radius;
			vertex.Position += center;
		}
	}
#pragma endregion
}
