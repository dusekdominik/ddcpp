#ifndef DD_SIMPLE_VERTEX_H
#define DD_SIMPLE_VERTEX_H

#include "ShaderHelpers.h"

#define SimpleVertexDeclaration(parser) \
 parser(float3, Position, POSITION, 0, PerVertex, 0, 0)

namespace DD {
	namespace Simulation {
		/**
		 * SimpleVertex
		 *
		 * The simplest (non-empty) vertex, declaring just its position.
		 * Vertex is not aligned to 128 bit.
		 */
		struct SimpleVertex {
			STATIC_TRAITS_DECLARATION
				SimpleVertexDeclaration(PARSER_MEMORY)
		};
	}
}
#endif