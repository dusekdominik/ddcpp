#pragma once
namespace DD::Simulation {
	class LasModel;
	struct ConvexHull2d;
	struct Region;
}

#define DDCGAL 0
#if DDCGAL
#pragma warning(disable : 4503)
#endif

#include <mutex>

#include <DD/Array.h>
#include <DD/Chain.h>
#include <DD/Color.h>
#include <DD/Polygon.h>
#include <DD/Delegate.h>
#include <DD/Las.h>
#include <DD/Reference.h>
#include <DD/String.h>
#include <DD/Simulation/Engine.h>
#include <DD/Simulation/ModelGroup.h>
#include <DD/Simulation/MultiBillboardModel.h>
#include <DD/Simulation/QuadModel.h>
#include <DD/Simulation/TriangleModel.h>

namespace DD::Simulation {
	struct ConvexHull2d {
	public: // nested
		struct Float2ComparerLexi {
			bool operator ()(const QuadVertex * q, const QuadVertex * p) const {
				return q->Position.x < p->Position.x || (q->Position.x == p->Position.x && q->Position.z < p->Position.z);
			}
		};
		struct Float2ComparerHeight {
			bool operator ()(const QuadVertex * q, const QuadVertex * p) const {
				return q->Position.y < p->Position.y;
			}
		};

	private: // statics
		/** helper for 2d cross-scalar value. */
		static float cross(const float2 & O, const float2 & A, const float2 & B) { return (A.x - O.x) * (B.y - O.y) - (A.y - O.y) * (B.x - O.x); }
		/** Converter. */
		static float2 toFloat2(const float3 & f3) { return float2(f3.x, f3.z); }
		/** Checks if b is on the left half plane of a-c direction. */
		static bool check(const float2 & a, const float2 & b, const float2 & c);

	public: // properties
		/** Characteristics of Hull. */
		float2 Min, Max, Center;
		/** Surface estimation via bounding square. */
		float Surface;
		/** Median height. */
		float Height;
		/** Points of hull contour. */
		List<float2> Hull;

	public:
		/** Checks if input point is inside the Hull. */
		bool IsIn(const float2 & a);
		/** Splits Hull lines longer than longLineDefinition. */
		void SplitLongLines(float longLineDefinition);
		/** Constructor from set of QuadVertices. */
		ConvexHull2d(List<QuadVertex *> &);
		/** Zero constructor. */
		ConvexHull2d() : Hull() {}
	};

	struct Region {
		typedef Vector<float, 6> histogram_t;

	public:
		/** Index of in icon collection. */
		Reference<MultiBillboardModel::IconRef> Icon;
		/** Unique histogram. */
		histogram_t Histogram;
		/** Type of region. */
		Las::Classification Classification;
		/** Id of region. */
		size_t Number;
		/** Points of region. */
		List<QuadVertex *> Points;
		/** Convex hull of region. */
		ConvexHull2d Hull;

	public:
		float3 IconPosition() const { return{Hull.Center.x, Hull.Height + 50.f, Hull.Center.y}; }
		/** Computes histogram of region. */
		void RecalculateHistogram();
		/** Check if point buffer of region is empty. */
		bool IsEmpty() const;
		/** Merges points and creates common cvx hull. */
		void Merge(const Region &);
		/** Helper counting how many percents of cvx hull is contained by another one. */
		float PercentageIn(const Region &);
		/** Removes convex hull and point buffer. */
		void Clear();
		/** Throws out present convex hull and creates new one. */
		void RecalculateHull();
		/** Sets point classification by region Number. */
		void Classify() { Classify((Number % 250) + 6); }
		/** Sets point classification by specified number. */
		void Classify(unsigned char c);
		/** Splits all lines of convex hull, which are longer than longLine. */
		void SplitLines(float longLine);
		/** Stics edges of convex hull to the region boundaries. checkArea defines search bounding space;  */
		void ConcaveHull(const float2 & checkArea, float shiftSize, float staticSize, float dynamicSize, size_t deCasetljau);
		/** Checks if any region's point is in bounding square defined by min, max. */
		bool Select(const float2 & min, const float2 & max) const;
		Reference<TriangleModel<Simulation::ColorVertex>> CreateHullModel() const;
		/** Checks if green is dominant color of region. Based on HSV. */
		bool IsGreen() const;

	public:
		Region(size_t pointCount = 1024) : Points(pointCount), Histogram(0.f) {}
	};



	class LasModel
		: public IModel
		, public IMessage
	{
	public:
		struct MapInfo;
		enum MorphologicalFilteringMethods { Linear = 0, Quadratic = 1, Exponential = 2 };
		struct SettingRegionGrowingOnBBoxes {
			float MAX_HEIGHT_TOLERANCE = 0.8f;
			float MIN_HEIGHT_TOLERANCE = 0.8f;
			float SLOPE_DEVIATION = 0.5f;
			float PARTITIONING = 0.8f;
		};

		struct SettingsMorphologicalFiltering {
			size_t ITERATIONS = 10;
			float SLOPE = 0.08f;
			float RADIUS2 = 0.1f;
			MorphologicalFilteringMethods METHOD = Linear;
		};

		struct SettingsRegionGrowingOnPoints {
			size_t MINIMUM_POINTS = 10000;
			size_t MAXIMUM_POINTS = 10000000;
			float DISTANCE_XZ = 1.f;
			float DISTANCE_Y = 0.5f;

		};

		struct SettingsRegionGrowingOnHistogram {
			double ABOVE_AVERAGE_HISTOGRAM = 3.0;
			float HISTOGRAM_HEIGHT_SKIP = 0.75f;
			float HISTOGRAM_GRANULARITY = 0.5f;
			float2 HISTOGRAM_SHIFT = float2(250.f, 250.f);
			float2 HISTOGRAM_AREA = float2(500.f, 500.f);
			float NEAREST_NEIGHBOR_MAXIMAL_DISATNCE = 5;
			float MAXIMAL_POLYGON_EDGE_DYNAMIC = 2.0;
			float MAXIMAL_POLYGON_EDGE_STATIC = 1.0;
			float FITTING_CONCAVE_HULL_SHIFT_SIZE = 1.0;
			float2 FITTING_CONCAVE_HULL_SEARCH_AREA = float2(2.5f, 2.5f);
			size_t FITTING_CONCAVE_HULL_DE_CASTELJAU_ITERATIONS = 5;
			size_t MINIMUM_POINTS = 10000;
			size_t MAXIMUM_POINTS = 1000000;
			float MAXIMUM_SURFACE = 250000.f;
			float MINIMUM_SURFACE = 50.f;
			float CONVEX_HULL_MERGE = 0.75f;
		};

		struct PointSegmentRelation { MapInfo * segment; QuadVertex * point; };

		enum struct MethodShared { None, MorphologicalFiltering, RegionGrowingOnPoint, RegionGrowingBasedOnHistogram };
		enum struct MSRegionGrowingOnPoints { None, ShowRegioningInfo };
		enum struct MSRegionGrowingOnHistogram { None, CreatingHistogram, SortingHistogram, CreatingConvexHull, CreatingConcaveHull, Growing };
		struct {
			MethodShared Method;
			union {
				struct { int MF; };
				struct { MSRegionGrowingOnPoints State; size_t EvaluatedPoints, RegionNumber; } RGOP;
				struct { MSRegionGrowingOnHistogram State; size_t TileCount; size_t TileProcessed; size_t EvaluatedPoints, RegionsCreated, RegionsAccepted; } RGBH;
			};

		} _sharedInformation;


	private:
		/*const*/ size_t GRID_HEIGHT, GRID_WIDTH;
		float GRID_UNIT;
		mutable std::mutex _queueMutex;
		bool _enabled;
		bool _valid;
		/** Is loading active. */
		bool _loadingIntoVRAM;
		/** Percentage of loader. */
		int _loadingVRAMCount;
		/** Size of one point - computed from surface and point count. */
		float _pointSize;
		/** Path of loader. @see Load(). */
		String _path;
		/** Loader of model from las format. @see Load(). */
		Reference<Las::FileReader> _reader;
		/** Saver of model into las format. @see Save() */
		Reference<Las::FileWriter> _writer;
		float3 _min, _max;

	public:
		/** Grid topology of models. */
		Array2D<MapInfo> _map;
		/** */
		MultiBillboardModel _iconModel;
		/** Model of bonding box. */
		ModelGroup _boundingBoxModel;
		/** Model of point cloud. */
		IntersectableModelGroup _pointModel;
		ModelGroup _triangleModel;
		/***/
		Chain<Region> _regions;
		String _appendInfo;
		/***/
		Event<Region&> RegionAccepted;
		Event<LasModel &> Loaded;

	protected:// Inherited via IMessage
		virtual bool iMessageEnabled(Units::Time_MicroSecondULL) override { return _enabled; }
		virtual bool iMessageEnabled() const override { return true; }
		virtual Gpu::D2D::Strings iMessageGetText() const override;

	protected:// Inherited via IModel
		virtual const char * iModelName() const override { return typeid(*this).name(); }
		virtual bool iModelEnabled() const override { return _enabled; }

		virtual void iModelInit(Gpu::Device & device) override;
		virtual void iModelDraw(DrawArgs & args) override;
		virtual void iModelDrawCulled(const Frustum3F & f, const float4x4 & transform, DrawArgs & args);

	protected:
		/** Converts _map into _pointModel. */
		void buildfrustumCullingHierarchy();

	public: // point classification methods
		size_t GetRegionCount() const { return _regions.Size(); }
		/** Ground classification based on mathematical morphology. */
		void MorphologicalFiltering(const SettingsMorphologicalFiltering & settings);
		/** Region growing. */
		void RegionGrowingOnBBox(const SettingRegionGrowingOnBBoxes & settings);
		void RegionGrowingOnPoints(const SettingsRegionGrowingOnPoints & settings);
		void growRegionOnPoints(const SettingsRegionGrowingOnPoints & settings, PointSegmentRelation init, float y);
		/** Auxiliary method for RegionGrowing. */
		void growRegionOnBBox(MapInfo * init, const SettingRegionGrowingOnBBoxes & settings);
		/** Height histogram heuristic. */
		void RegionGrowingBasedOnHeightHistogram(const SettingsRegionGrowingOnHistogram & settings, Engine & e);
		void RegionGrowingBasedOnHeightHistogramHeuristic(const SettingsRegionGrowingOnHistogram & settings);
		void growRegionHistogram(const SettingsRegionGrowingOnHistogram & settings, MapInfo * init);
		typedef Lambda<void(Region &)> tracker_t;
		void regionTracker(Engine & e, tracker_t track);

	public:
		bool IsValid() const { return _valid; }
		const Chain<Region> GetRegions() const { return _regions; }
		Las::Header & GetHeader() { return _reader->GetHeader(); }
		const unsigned GetTotalCount() const { return _reader->GetTotalCount(); }
		const float GetLoadRatio() const { return (float)_reader->GetReadCount() / (float)_reader->GetTotalCount(); }
		const float GetSaveRatio() const { if (_writer) return (float)_writer->GetWriteCount() / (float)_reader->GetTotalCount(); else return 0.f; }
		void Load();
		void Save(const String & path);
		void ExportToXML(const String & path);
		enum Models { EnabledNone = 0, EnabledTriangle = 1, EnabledPoint = 2, EnabledLine = 4, EnabledBill = 8, EnabledAll = 15 };
		void ModelSwitch(Models s);
		Models ModelSwitch();
		Array<MapInfo *> GetAABBSelectionSegements(const float2 & min, const float2 & max);
		Array<QuadVertex *> GetAABBSelectionPoints(const float2 & min, const float2 & max);
		Array<QuadVertex *> GetCVXSelection(const Polygon<float> & cvx);
		LasModel(const String & file);
		virtual ~LasModel() override {}
	};


	struct LasModel::MapInfo {
		static size_t PREALLOCATION_CONSTANT;
		typedef QuadVertex* iterator;

	private:
		/** Engine model. */
		Reference<QuadModel> _model;
		/** Region id. */
		size_t _region = 0;

	public: // forwards of model
		/** Iterator. */
		iterator begin() { return HasModel() ? _model->begin() : iterator(); }
		/** Iterator. */
		iterator end() { return HasModel() ? _model->end() : iterator(); }
		/** Number of points. */
		size_t Count() const { return HasModel() ? _model->Count() : 0; }
		/** Maximum height of contained data. */
		float MaxHeight() const { return HasModel() ? _model->GetBoundingBox().Max.y : -FLT_MAX; }
		/** Minimum height of contained data. */
		float MinHeight() const { return HasModel() ? _model->GetBoundingBox().Min.y : +FLT_MAX; }
		/** Model. */
		Reference<QuadModel> & Model() { return _model; }
		/** Model. */
		const Reference<QuadModel> & Model() const { return _model; }
		/** Checks if reference to model is valid. */
		bool HasModel() const { return _model.IsNotNull(); }

	public:
		/** Region id getter. */
		size_t Region() { return _region; }
		/** Region id setter. (Segment color). */
		void Region(size_t region);

	public:
		/** Adds point into Model. */
		void AddPoint(const QuadVertex & qv);
		/** Returns ratio of points in specified height range. */
		float RangeCheck(float minHeight, float maxHeight);

		MapInfo() = default;
		MapInfo(const MapInfo &) = default;
		MapInfo(MapInfo &&) = default;
	};
}