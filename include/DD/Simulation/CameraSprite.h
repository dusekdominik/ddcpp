#pragma once
namespace DD::Simulation {
	/** Text information about main camera position. */
	struct CameraSprite;
}

#include <DD/Simulation/TextSprite.h>
#include <DD/Gpu/D2D.h>

namespace DD::Simulation {
	struct CameraSprite
		: TextSprite
	{
	protected:

	};
}
