#pragma once
namespace DD::Simulation {
	/** Display string with currnt time-stamp.*/
	struct TimeSprite;
}

#include <DD/Simulation/TextSprite.h>
#include <DD/Time.h>

namespace DD::Simulation {
	struct TimeSprite
		: public TextSprite
	{
	protected: // ISprite
		void iSpriteUpdate(Units::Time_MicroSecondF delta, Units::Time_MicroSecondF time) override {
			Time::Stamp stamp = Time().GetStamp();
			_text.String.Clear().AppendFormat(
				"{3:4}/{4,2}/{5,2} {0:2,2}:{1:2,2}:{2:2,2}",
				stamp.Hour,    // 0
				stamp.Minute,  // 1
				stamp.Seconds, // 2
				stamp.Year,    // 3
				stamp.Month,   // 4
				stamp.Day      // 5
			);
		}

	public:
		/** Constructor. */
		TimeSprite(Text::StringView16 spriteName)
			: TextSprite(
				spriteName,
				Gpu::D2D::Labels{
					.Box = Gpu::D2D::Boxes{.Mode = Gpu::D2D::SSModes::Interval },
					.TVAlign = DD::Gpu::D2D::TVAligns::TTop,
				})
		{}
	};
}
