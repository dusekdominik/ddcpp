#ifndef DD_TEXTURE_PIXEL_H
#define DD_TEXTURE_PIXEL_H

#include "ShaderHelpers.h"

#define TexturePixelDeclaration(parser) \
	parser(float4, Position, SV_POSITION, 0, NONE, 0, 0)\
	parser(float2, Texture, TEXTURE, 0, NONE, 0, 0)

namespace DD {
	namespace Simulation {
		/**
		 * TexturePixel
		 *
		 * Output of vertex shaders for textured polygons.
		 * Contains position and UV coordinate of texture.
		 */
		struct TexturePixel {
			TexturePixelDeclaration(PARSER_MEMORY)
		};
	}
}
#endif
