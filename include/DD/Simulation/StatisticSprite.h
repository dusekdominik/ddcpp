#pragma once
namespace DD::Simulation {
	/** */
	struct StatisticSprite;
}

#include <DD/Debug.h>
#include <DD/Enum.h>
#include <DD/Profiler.h>
#include <DD/Simulation/ISprite.h>
#include <DD/String.h>

namespace DD::Simulation {
	struct StatisticSprite
		: public ISprite
	{
	public: // nested
		/** Helper struct for rendering statistic with labels and other visual helpers. */
		struct Drawing {
		/** Buffers for parsing texts - prevents unnecessary allocation. */
		struct {
			/** Text of maximal value of the statistic. */
			wchar_t Maximum[16];
			/** Text of minimal value of the statistic. */
			wchar_t Minimum[16];
			/** Text of average value of the statistic. */
			wchar_t Average[16];
			/** Text of average + std deviation value. */
			wchar_t PositiveDeviation[16];
			/** Text of average - std deviation value. */
			wchar_t NegativeDeviation[16];
			/** Text of the chart label. */
			wchar_t Label[64];
		} Buffers;
		/** Holder of all descriptors of primitives to be rendered. */
		struct {
			/** Descriptors for text drawing. */
			struct {
				/** Label of maximum. */
				Gpu::D2D::Labels Maximum;
				/** Label of minimum. */
				Gpu::D2D::Labels Minimum;
				/** Label of average. */
				Gpu::D2D::Labels Average;
				/** Label of average + standard deviation. */
				Gpu::D2D::Labels PositiveDeviation;
				/** Label of average - standard deviation. */
				Gpu::D2D::Labels NegativeDeviation;
				/** Label of the chart. */
				Gpu::D2D::Labels Label;
			} Texts;
			/** Descriptors of lines. */
			struct {
				/** Visualization of average of statistic. */
				Gpu::D2D::Lines Average;
				/** Visualization of average + standard deviation of statistic. */
				Gpu::D2D::Lines PositiveDeviation;
				/** Visualization of average - standard deviation of statistic. */
				Gpu::D2D::Lines NegativeDeviation;
			} Lines;
			/** Other primitives. */
			struct {
				/** background of the chart.*/
				Gpu::D2D::Rectangles Background;
			} Others;
		} Primitives;

		/** Formatting of labels of the statistics. */
		struct {
			/** Units of the statistic values. */
			StringLocal<8> Unit;
			/** Number format of statistic values. */
			StringLocal<8> NumberFormat;
		} Formatting;

		/** Callback of the statistic. */
		Action16<Gpu::D2D &, const Gpu::D2D::Statistics &, const Gpu::D2D::Boxes &, f32, f32, f32> Callback;

		/** Draw statistic. */
		void Draw(Gpu::D2D & d2d, Gpu::D2D::Statistics & statistic, const Text::StringView16 & label);

		/** Constructor. */
		StatisticSprite::Drawing(const Text::StringView16 & unit, const Text::StringView16 & numberFormat);
	};
		/** Statistic modes. */
		DD_ENUM_32(
			(Modes, "Mode of chart visualization"),
			(HIDDEN, 0, "No visualization."),
			(FLUENT, 1, "Chart with fluent indexing."),
			(FIXED,  2, "Chart with fixed indexing (caret based update).")
		);

	protected: // properties
		/** Is enabled. */
		bool _enabled = true;
		/** Dimensions of the statistic. */
		Debug::f32Range _offsetX, _offsetY, _sizeX, _sizeY;
		/** Mode of the statistic. */
		Debug::Enum<Modes> _mode;
		/** Scope which should be observed. */
		Debug::Options _scope;
		/** Statistics to be drawn. */
		Gpu::D2D::Statistics _statistics;
		/** Object providing drawing. */
		StatisticSprite::Drawing _drawer;

	protected: // ISprite
		void iSpriteDraw(Gpu::D2D & device) override;
		void iSpriteEnabled(bool value) override { _enabled = value; }
		bool iSpriteEnabled() const override { return _enabled && _mode->Value() != Modes::HIDDEN; }
		void iSpriteExpired(bool /*value*/) override {}
		bool iSpriteExpired() const override { return false; }
		void iSpriteInit(Gpu::D2D & device) override;
		void iSpriteUpdate(Units::Time_MicroSecondF, Units::Time_MicroSecondF) override;

	public: // constructors
		/** Constructor. */
		StatisticSprite(
			const Text::StringView16 & name,
			const Text::StringView16 & unit,
			const Text::StringView16 & numberFormat,
			Debug::f32Range offsetX,
			Debug::f32Range offsetY,
			Debug::f32Range sizeX,
			Debug::f32Range sizeY,
			Debug::Enum<Modes> mode,
			Debug::Options scope
		);
		/** Destructor. */
		~StatisticSprite() override = default;
	};
}


namespace DD::Simulation {
	inline void StatisticSprite::Drawing::Draw(Gpu::D2D & d2d, Gpu::D2D::Statistics & statistic, const Text::StringView16 & str) {
		statistic.Callbacks.Computed = Callback;
		Primitives.Texts.Label.String.Clear().Append(str).Color(Gpu::D2D::Fonts::DEFAULT_YELLOW);
		d2d.DrawStatistic(statistic);

		f32 upperBound = Primitives.Texts.Label.Box.Origins.y;
		f32 lowerBound = upperBound + Primitives.Texts.Label.Box.Dimensions.y;
		f32 scale = Primitives.Texts.Minimum.Scale;
		f32 devP = Primitives.Lines.PositiveDeviation.A.y;
		f32 devN = Primitives.Lines.NegativeDeviation.A.y;

		d2d.DrawLabel(Primitives.Texts.Minimum);
		d2d.DrawLabel(Primitives.Texts.Maximum);
		d2d.DrawLabel(Primitives.Texts.Average);
		d2d.DrawLine(Primitives.Lines.Average);
		d2d.DrawLabel(Primitives.Texts.Label);
		if (devN - devP > 50.f * scale) {
			if (Primitives.Lines.PositiveDeviation.A.y > upperBound + 20.f * scale) {
				d2d.DrawLabel(Primitives.Texts.PositiveDeviation);
				d2d.DrawLine(Primitives.Lines.PositiveDeviation);
			}
			if (Primitives.Lines.NegativeDeviation.A.y < lowerBound - 40.f * scale) {
				d2d.DrawLabel(Primitives.Texts.NegativeDeviation);
				d2d.DrawLine(Primitives.Lines.NegativeDeviation);
			}
		}
	}

	inline StatisticSprite::Drawing::Drawing(const Text::StringView16 & unit, const Text::StringView16 & numberFormat) {
		// set formatting
		Formatting.Unit = unit;
		Formatting.NumberFormat = numberFormat;
		// predefine alignment
		Primitives.Texts.NegativeDeviation.THAlign = Gpu::D2D::THAligns::TRight;
		Primitives.Texts.NegativeDeviation.TVAlign = Gpu::D2D::TVAligns::TTop;
		Primitives.Texts.PositiveDeviation.THAlign = Gpu::D2D::THAligns::TRight;
		Primitives.Texts.PositiveDeviation.TVAlign = Gpu::D2D::TVAligns::TTop;
		Primitives.Texts.Average.THAlign = Gpu::D2D::THAligns::TRight;
		Primitives.Texts.Average.TVAlign = Gpu::D2D::TVAligns::TTop;
		Primitives.Texts.Minimum.THAlign = Gpu::D2D::THAligns::TRight;
		Primitives.Texts.Minimum.TVAlign = Gpu::D2D::TVAligns::TBottom;
		Primitives.Texts.Maximum.THAlign = Gpu::D2D::THAligns::TRight;
		Primitives.Texts.Maximum.TVAlign = Gpu::D2D::TVAligns::TTop;
		Primitives.Texts.Label.THAlign = Gpu::D2D::THAligns::TLeft;
		Primitives.Texts.Label.TVAlign = Gpu::D2D::TVAligns::TTop;
		// capture local buffers to prevent allocation
		Primitives.Texts.Maximum.String.Text.Wrap(Buffers.Maximum);
		Primitives.Texts.Average.String.Text.Wrap(Buffers.Average);
		Primitives.Texts.Minimum.String.Text.Wrap(Buffers.Minimum);
		Primitives.Texts.PositiveDeviation.String.Text.Wrap(Buffers.PositiveDeviation);
		Primitives.Texts.NegativeDeviation.String.Text.Wrap(Buffers.NegativeDeviation);
		Primitives.Texts.Label.String.Text.Wrap(Buffers.Label);
		//
		Primitives.Lines.NegativeDeviation.Brush = Colors::YELLOW;
		Primitives.Lines.PositiveDeviation.Brush = Colors::YELLOW;
		Primitives.Lines.Average.Brush = Colors::RED;
		// set lambda callback
		Callback = [this](
			Gpu::D2D & device,
			const Gpu::D2D::Statistics & statistic,
			const Gpu::D2D::Boxes & box,
			f32 minimum,
			f32 maximum,
			f32 average
			) {
				// compute variation and standard deviation
				f32 varAcc = 0.f;
				for (f32 value : statistic.Statistic.Samples) {
					f32 diff = value - average;
					varAcc += diff * diff;
				}
				f32 var = varAcc / statistic.Statistic.Samples.Length();
				f32 stdDev = Math::Sqrt(var);
				// prepare positions
				Primitives.Texts.Maximum.Box = box;
				Primitives.Texts.Minimum.Box = box;
				Primitives.Texts.Average.Box = box;
				Primitives.Texts.PositiveDeviation.Box = box;
				Primitives.Texts.NegativeDeviation.Box = box;
				Primitives.Texts.Label.Box = box;
				Primitives.Others.Background.Box = box;
				Primitives.Others.Background.Box.Origins.x -= 3.f;
				Primitives.Others.Background.Box.Origins.y -= 2.f;
				Primitives.Others.Background.Box.Dimensions.x += 6.f;
				Primitives.Others.Background.Box.Dimensions.y += 4.f;
				// set texts
				Primitives.Texts.Maximum.String.Clear();
				Primitives.Texts.Minimum.String.Clear();
				Primitives.Texts.Average.String.Clear();
				Primitives.Texts.PositiveDeviation.String.Clear();
				Primitives.Texts.NegativeDeviation.String.Clear();
				Primitives.Texts.Maximum.String.Text.Append(maximum, Formatting.NumberFormat).Append(Formatting.Unit);
				Primitives.Texts.Minimum.String.Text.Append(minimum, Formatting.NumberFormat).Append(Formatting.Unit);
				Primitives.Texts.Average.String.Text.Append(average, Formatting.NumberFormat).Append(Formatting.Unit);
				Primitives.Texts.PositiveDeviation.String.Text.Append(average + stdDev, Formatting.NumberFormat).Append(Formatting.Unit);
				Primitives.Texts.NegativeDeviation.String.Text.Append(average - stdDev, Formatting.NumberFormat).Append(Formatting.Unit);
				Primitives.Texts.PositiveDeviation.String.Color(Gpu::D2D::Fonts::DEFAULT_YELLOW);
				Primitives.Texts.NegativeDeviation.String.Color(Gpu::D2D::Fonts::DEFAULT_YELLOW);
				Primitives.Texts.Average.String.Color(Gpu::D2D::Fonts::DEFAULT_YELLOW);
				Primitives.Texts.Maximum.String.Color(Gpu::D2D::Fonts::DEFAULT_GREEN);
				Primitives.Texts.Minimum.String.Color(Gpu::D2D::Fonts::DEFAULT_RED);
				// set lines
				Math::IntervalConverterF convert(minimum, maximum, box.Dimensions.y, 0.f);
				Primitives.Lines.Average.A.x = Primitives.Lines.PositiveDeviation.A.x = Primitives.Lines.NegativeDeviation.A.x = box.Origins.x;
				Primitives.Lines.Average.B.x = Primitives.Lines.PositiveDeviation.B.x = Primitives.Lines.NegativeDeviation.B.x = box.Origins.x + box.Dimensions.x;
				Primitives.Lines.Average.A.y = Primitives.Lines.Average.B.y = box.Origins.y + convert(average);
				Primitives.Lines.PositiveDeviation.A.y = Primitives.Lines.PositiveDeviation.B.y = box.Origins.y + convert(average + stdDev);
				Primitives.Lines.NegativeDeviation.A.y = Primitives.Lines.NegativeDeviation.B.y = box.Origins.y + convert(average - stdDev);
				// set text positions
				Primitives.Texts.Average.Box.Origins.y = Primitives.Lines.Average.A.y;
				Primitives.Texts.PositiveDeviation.Box.Origins.y = Primitives.Lines.PositiveDeviation.A.y;
				Primitives.Texts.NegativeDeviation.Box.Origins.y = Primitives.Lines.NegativeDeviation.A.y;
				// set scale
				f32 scale = box.Dimensions.y / 400.f;
				Primitives.Texts.Maximum.Scale = scale;
				Primitives.Texts.Minimum.Scale = scale;
				Primitives.Texts.Average.Scale = scale;
				Primitives.Texts.NegativeDeviation.Scale = scale;
				Primitives.Texts.PositiveDeviation.Scale = scale;
				Primitives.Texts.Label.Scale = scale;
				// set background
				Primitives.Others.Background.Background = Color::Collection::WHITE.SetA(64);
				device.DrawRectangle(Primitives.Others.Background);
		};
	}


	inline void DD::Simulation::StatisticSprite::iSpriteDraw(Gpu::D2D & device) {
		_drawer.Draw(device, _statistics, _scope->Value());
	}


	inline void DD::Simulation::StatisticSprite::iSpriteInit(Gpu::D2D &) {
		_statistics.Box.Mode = Gpu::D2D::SSModes::Interval;
		_statistics.Brush = Colors::BLUE;
		_statistics.Size.Value = 1;
	}


	inline void StatisticSprite::iSpriteUpdate(Units::Time_MicroSecondF, Units::Time_MicroSecondF) {
		// update option
		size_t pfSize = (size_t)Profiler::GetScopesCount();
		size_t optionsSize = _scope->Size();
		if (optionsSize != pfSize) {
			for (size_t i = optionsSize; optionsSize < pfSize; ++i) {
				const Profiler::Scope & scope = Profiler::GetScope(i);
				// if scope is not ready, wait until next call
				if (scope.ScopeName == 0)
					break;
				// add scope to options
				_scope->Add(StringLocal<64>(scope.ScopeName));
			}
		}
		// get scope, assign to statistic
		Profiler::Scope & scope = Profiler::GetScope(_scope);
		_statistics.Box.Origins.Set(_offsetX, _offsetY);
		_statistics.Box.Dimensions.Set(_sizeX, _sizeY);
		_statistics.Statistic.Samples = { ArrayView(scope.Buffer.Data), nullptr };
		if (_mode->Value() == Modes::FIXED)
			_statistics.Statistic.IndexOffset = 0;
		else
			_statistics.Statistic.IndexOffset = scope.Buffer.Index;
	}


	inline StatisticSprite::StatisticSprite(
		const Text::StringView16 & name,
		const Text::StringView16 & unit,
		const Text::StringView16 & numberFormat,
		Debug::f32Range offsetX,
		Debug::f32Range offsetY,
		Debug::f32Range sizeX,
		Debug::f32Range sizeY,
		Debug::Enum<Modes> mode,
		Debug::Options scope
	) : ISprite(name)
		, _drawer(unit, numberFormat)
		, _offsetX(offsetX)
		, _offsetY(offsetY)
		, _sizeX(sizeX)
		, _sizeY(sizeY)
		, _mode(mode)
		, _scope(scope)
	{
	}
}