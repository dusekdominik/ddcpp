#pragma once
namespace DD::Simulation {
	/** */
	struct MessageSprite;
}

#include <DD/Math/FunctionsLite.h>
#include <DD/Simulation/TextSprite.h>
#include <DD/Concurrency/ThreadSafeObject.h>

namespace DD::Simulation {
	struct MessageSprite
		: TextSpriteEx
	{
		/** Simple message descriptor. */
		struct Message {
			/** Text. */
			Gpu::D2D::Strings Text;
			/** Lifespan. */
			Units::Time_MicroSecondF Countdown;
		};
		/** collection of messages */
		using Messages = List<Message>;

	public: // statics
		/** Marker of invalid messages. */
		static constexpr auto InvalidMessages = [](const Message & m) { return m.Countdown == 0.f; };

		/** Dirty flag for string rebuilding. */
		bool _dirty = false;
		/** Collection of messages */
		Concurrency::ThreadSafeObject<Messages> _messages;

	protected:
		void iSpriteUpdate(Units::Time_MicroSecondF delta, Units::Time_MicroSecondF time) override {
			// lock object for accessing it
			auto proxy = *_messages;

			// process countdown
			bool removeNeeded = false;
			for (Message & message : *proxy) {
				message.Countdown = Math::UDiff(message.Countdown, delta);
				if (message.Countdown == 0.f)
					removeNeeded = true;
			}

			// filter out if any
			if (removeNeeded) {
				proxy->RemoveAll(InvalidMessages);
				_dirty = true;
			}

			// compose message label
			if (_dirty) {
				TextSprite::_text.String.Clear();
				for (Message & message : *proxy) {
					TextSprite::_text.String.Append(message.Text);
				}
			}

			TextSprite::iSpriteUpdate(delta, time);
		}

	public: // methods
		/** Add new message. */
		void Add(Message && message) {
			auto proxy = *_messages;
			proxy->Add(message);
			_dirty = true;
		}
		/** Clear messages. */
		void Clear() {
			auto proxy = *_messages;
			proxy->Clear();
			_dirty = true;
		}

	public: // constructors
		/** Constructor. */
		MessageSprite(Text::StringView16 name, Gpu::D2D::Labels && init) : TextSpriteEx(name, Fwd(init)) {}
	};
}
