#pragma once
namespace DD::Simulation {
	/** Interface for 2d rendering. */
	struct ISprite;
}

#include <DD/Delegate.h>
#include <DD/Gpu/D2D.h>
#include <DD/Reference.h>
#include <DD/String.h>
#include <DD/Types.h>
#include <DD/units/Unit.h>

namespace DD::Simulation {
	struct ISprite
		: public IReferential
	{
	protected: // properties
		/** Name of the sprite. */
		StringLocal<64> _name;

	public: // properties
		/** Event fired while sprite is expiring. */
		Event<ISprite *> OnExpiration;
		/** Event fired while engine is unregistering the sprite. */
		Event<ISprite *> OnUnregistration;
		/** Event fired while engine is registering the sprite. */
		Event<ISprite *> OnRegistration;

	protected: // interface
		virtual void iSpriteDraw(Gpu::D2D & device) = 0;
		virtual void iSpriteEnabled(bool) = 0;
		virtual bool iSpriteEnabled() const = 0;
		virtual void iSpriteExpired(bool) = 0;
		virtual bool iSpriteExpired() const = 0;
		virtual void iSpriteInit(Gpu::D2D & device) = 0;
		virtual void iSpriteUpdate(Units::Time_MicroSecondF delta, Units::Time_MicroSecondF time) = 0;

	public: // public interface
		/** Is drawing enabled. */
		void Enabled(bool enabled) { iSpriteEnabled(enabled); }
		/** Is drawing enabled. */
		bool Enabled() const { return iSpriteEnabled(); }
		/** Should be sprite released from engine. */
		void Expired(bool expired) { iSpriteExpired(expired); if (expired) OnExpiration(this); }
		/** Should be sprite released from engine. */
		bool Expired() const { return iSpriteExpired(); }
		/**
		 * Time for self update of the sprite.
		 * @param delta - delta time in microseconds since last rendering.
		 * @param time - microseconds since simulation started.
		 */
		void Update(Units::Time_MicroSecondF delta, Units::Time_MicroSecondF time) { iSpriteUpdate(delta, time); }
		/** Draw the sprite. */
		void Draw(Gpu::D2D & device) { iSpriteDraw(device); }
		/** Initialize sprite. */
		void Init(Gpu::D2D & device) { iSpriteInit(device); }
		/** Name of the sprite. */
		const String & Name() const { return _name; }

	public: // constructors
		/** Constructor. */
		ISprite(Text::StringView16 name) : _name(name) {}
	};

	/** Alias. */
	using Sprite = Reference<ISprite>;
}
