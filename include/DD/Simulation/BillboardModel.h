#pragma once
namespace DD::Simulation {
	/**
	 * Model of billboards.
	 * Billboard is 3d graphic keeping its y-axes fixed,
	 * and rotating its xz-axes orthogonal to camera direction.
	 *
	 * Billboard contains texture, size, and set of positions of billboard instances.
	 */
	class BillboardModel;
}

#include <DD/Gpu/ConstantBuffer.h>
#include <DD/Gpu/StructuredBuffer.h>
#include <DD/Gpu/PixelShader.h>
#include <DD/Gpu/Texture.h>
#include <DD/Gpu/VertexShader.h>
#include <DD/Icon.h>
#include <DD/Reference.h>
#include <DD/Simulation/ICullableModel.h>
#include <DD/Simulation/ConstantBuffers.h>
#include <DD/Simulation/SimpleVertex.h>

namespace DD::Simulation {
	class BillboardModel
		: public ICullableModel
	{
	public: // typedefs
		typedef SimpleVertex vertex_t;
		typedef Gpu::StructuredBuffer<vertex_t> buffer_t;

	private: // static properties
		static Reference<Gpu::PixelShader> _PIXEL_SHADER;
		static Reference<Gpu::VertexShader> _VERTEX_SHADER;

	private: // instance properties
		/** Device handle. */
		Gpu::Device _device;
		/** Info about texture - size and path. */
		Icon _icon;
		/** Texture of billboard. */
		Reference<Gpu::Texture> _texture;
		/** Buffer of positions. */
		buffer_t _points;
		/** Size of single billboard. */
		Gpu::ConstantBuffer<BillBoardConstantBuffer> _cb;

	public: // Inherited via IModel
		virtual const char * iModelName() const override { return "BillboardModel"; }
		virtual void iModelInit(Gpu::Device & device) override;
		virtual void iModelDraw(DrawArgs & args) override;

	public: // methods
		/** Adds a position of new instance. Reinit is not invoked automatically. */
		void Add(const float3 & x) { _points.Add(SimpleVertex{x}); }
		/** Adds a position of new instance. Reinit is not invoked automatically. */
		void Add(float x, float y, float z) { _points.Add(SimpleVertex{{ x, y, z }}); }
		/** Removes a position of given index. Reinit is not invoked automatically. */
		void Remove(size_t index) { _points.RemoveAt(index); }
		/** Removes all the positions. Reinit is not invoked automatically. */
		void Clear() { _points.Clear(); }
		/** Returns number of billboard instances. */
		size_t Size() const { return _points.Size(); }

	public: // constructors
		/** Constructor. */
		BillboardModel(const Icon & icon);
		/** Destructor. */
		virtual ~BillboardModel() override = default;
	};
}
