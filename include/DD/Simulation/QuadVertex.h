#ifndef DD_QUAD_VERTEX_H
#define DD_QUAD_VERTEX_H

#include "ShaderHelpers.h"

#define QuadVertexDeclaration(parser) \
 parser(float3, Position, POSITION, 0, PerVertex, 0, 0)\
 parser(ColorInt, Color, COLOR, 0, PerVertex, 0, 0)\
 parser(int, Backup, COLOR, 1, PerVertex, 0, 0)

namespace DD {
	namespace Simulation {
		/**
		 * QuadVertex
		 *
		 * Vertex designed for rendering quads.
		 * Use in structured buffers - not in vertex buffers.
		 */
		struct QuadVertex {
			STATIC_TRAITS_DECLARATION
				QuadVertexDeclaration(PARSER_MEMORY)
		};
	}
}
#endif
