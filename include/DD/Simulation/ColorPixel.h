#ifndef DD_COLOR_PIXEL_H
#define DD_COLOR_PIXEL_H

#include "ShaderHelpers.h"

#define ColorPixelDeclaration(parser) \
 parser(float4, Position, SV_POSITION, 0, NONE, 0, 0)\
 parser(float4, Color, COLOR, 0, NONE, 0, 0)\

namespace DD {
	namespace Simulation {
		/**
		 * ColorPixel
		 *
		 * Simple color output of vertex shader.
		 */
		struct ColorPixel {
			ColorPixelDeclaration(PARSER_MEMORY)
		};
	}
}
#endif