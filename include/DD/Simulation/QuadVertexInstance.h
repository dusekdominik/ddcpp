#ifndef DD_QUAD_VERTEX_INSTANCE_H
#define DD_QUAD_VERTEX_INSTANCE_H

#include "ShaderHelpers.h"

#define QuadVertexInstanceDeclaration(parser) \
 parser(float2, Shift, INSTANCEPOSITION, 1, PerVertex, 0, 1)

namespace DD {
	namespace Simulation {
		/**
		 * QuadVertexInstance
		 *
		 * Vertex designed for instanced rendering of quads.
		 * Containing information about shift from a central posiotion.
		 */
		struct QuadVertexInstance {
			STATIC_TRAITS_DECLARATION
				QuadVertexInstanceDeclaration(PARSER_MEMORY)
		};
	}
}
#endif
