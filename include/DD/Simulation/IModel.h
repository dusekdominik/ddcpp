#pragma once
namespace DD::Simulation {
	/**
	 * IModel
	 * Represents abstract model for rendering.
	 * Should contain description of vertex/index/structured buffers.
	 * Also should set shaders.
	 * For transformation properties @see Object, which is instance of IModel.
	 */
	class IModel;
}

#include <DD/Gpu/Device.h>
#include <DD/IProgress.h>
#include <DD/Primitives.h>
#include <DD/Reference.h>
#include <DD/Units/Unit.h>

namespace DD::Simulation {
	class IModel
		: public IReferential
	{
	public: // nested
		/** Interface for asynchronous initialization. */
		struct AsyncInitInterface {
			/** Model to be initialized. */
			IModel * Model;
			/** Begin-end counter for observing progress. */
			IProgress::ScopeCounter32 Counter;
			/**
			 * Tick of asynchronous initialization.
			 * @param device - device on which model should be initialized.
			 * @param limit  - number of microseconds reserved for initialization.
			 */
			bool Tick(Gpu::Device & device, Units::Time_MicroSecondULL & limit) { return Model->iModelAsyncTick(device, this, limit); }
			/**
			 * Constructor.
			 * @param model    - model to be asynchronously initialized.
			 * @param progress - progress observer of initialization state.
			 */
			AsyncInitInterface(IModel * model, IProgress * progress)
				: Model(model)
				, Counter(progress, model->iModelAsyncTotal())
			{}
		};

		/** Rendering arguments. */
		struct DrawArgs {
			/** Draw modes. */
			enum struct DrawModes {
				Normal,  // Common rendering
				Fraction // Only relative part of model will be rendered
			} Mode = DrawModes::Normal;
			/** How big portion of the model should be rendered - 0.0 nothing, 1.0 everything. */
			f64 Fraction = 1.0;
		};

	protected: // virtual interface
		/**
		 * Method for checking intersection of (transformed) model with given frustum.
		 * @param frustum - to be intersected with.
		 * @param matrix  - transformation of the model.
		 */
		virtual Intersetion iModelIntersects(const Frustum3F &, const float4x4 &) const { return PART_IN; }
		/** Is model enabled for rendering. */
		virtual bool iModelEnabled() const { return true; }
		/**
		 * Initialize model at one call (@see AsyncInitInterface).
		 * @param device on which will be model initialized.
		 */
		virtual void iModelInit(Gpu::Device & device) = 0;
		/**
		 * Reinitialize model.
		 * @param device on which will be model reinitialized.
		 */
		virtual void iModelReinit(Gpu::Device & device) { iModelInit(device); }
		/**
		 * Draw model.
		 * Transformation of the model is given by Object constant buffer.
		 */
		virtual void iModelDraw(DrawArgs &) = 0;
		/**
		 * Draw transformed model if not culled by frustum.
		 * @param f - frustum of the camera.
		 * @param t - transformation of the model.
		 */
		virtual void iModelDrawCulled(const Frustum3F & f, const float4x4 & t, DrawArgs & args) { if (Intersects(f, t)) Draw(args); }
		/**
		 * One tick of the asynchronous initialization (@see AsyncInitInterface).
		 * @param device    - device on which should be model initialized.
		 * @param interface - interface managing the async initialization.
		 * @param number    - number of microseconds reserved for one tick.
		 * @return true if the model has been fully initialized.
		 */
		virtual bool iModelAsyncTick(Gpu::Device &, AsyncInitInterface *, Units::Time_MicroSecondULL &) { throw; }
		/**
		 * Number of atoms which must be initialized.
		 * @see AsyncInitInterface::Counter.
		 */
		virtual i32 iModelAsyncTotal() const { return 0; }
		/**
		 * Name of the model - typically name of the class derived from IModel.
		 * Debugging feature.
		 */
		virtual const char * iModelName() const = 0;

	public: // public interface
		/** Name of model. */
		const char * Name() const { return iModelName(); }
		/** Is model enabled for rendering. */
		bool Enabled() const { return iModelEnabled(); }
		/** Init model for rendering. */
		void Init(Gpu::Device & device) { iModelInit(device); }
		/** Reinit model for rendering. */
		void Reinit(Gpu::Device & device) { iModelReinit(device); }
		/** Render model. */
		void Draw(DrawArgs & args) { iModelDraw(args); }
		/** Render transformed model if it is not culled. */
		void DrawFrustumCulled(const Frustum3F & f, const float4x4 & t, DrawArgs & args) { iModelDrawCulled(f, t, args); }
		/** Checks if number of vertices is maximal. */
		Intersetion Intersects(const Frustum3F & f, const float4x4 & t) const { return iModelIntersects(f, t); }

	public: // constructors
		/** Virtual destructor. */
		virtual ~IModel() = default;
	};

	/** Alias. */
	using Model = Reference<IModel>;
}

