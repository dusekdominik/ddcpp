#ifndef DD_EMPTY_VERTEX

#include "ShaderHelpers.h"

#define EmptyVertexDeclaration(parser)

namespace DD {
	namespace Simulation {
		/**
		 * EmptyVertex
		 *
		 * Vertex with no content.
		 * Designed for loading content not from vertex buffer, but from structured buffers.
		 */
		struct EmptyVertex {
			STATIC_TRAITS_DECLARATION
		};
	}
}
#endif
