#pragma once
namespace DD::Simulation {
	/** Visualization of the command console. */
	struct CommandConsoleSprite;
}

#include <DD/Gpu/D2D.h>
#include <DD/Input/CommandConsole.h>
#include <DD/Math/FunctionsLite.h>
#include <DD/Simulation/ISprite.h>

namespace DD::Simulation {
	struct CommandConsoleSprite
		: public ISprite
		, public Memory::NonCopyable
	{
		/** Shortcut. */
		using MicroSecondsF = Units::Time_MicroSecondF;
		using IntervalS = Math::Interval1<size_t>;

	public: // statics
		/** Tick time. */
		static constexpr MicroSecondsF Tick = 250000.f;
		/** Blink time. */
		static constexpr MicroSecondsF HalfTick = Tick * 0.5f;

	protected: // properties
		/** Main command line. */
		Gpu::D2D::Labels _line;
		/** History of commands. */
		Gpu::D2D::Labels _history;
		/** Line shadowing the main line with first hint. */
		Gpu::D2D::Labels _hint;
		/** Line suggesting other hints. */
		Gpu::D2D::Labels _hints;
		/** The console we're working with. */
		Input::CommandConsole _console;
		/** Time accumulator for cursor blinking. */
		Units::Time_MicroSecondF _tickTime = 0;

	protected: // ISprite
		void iSpriteDraw(Gpu::D2D & device) override {
			device.DrawLabel(_line);
			device.DrawLabel(_history);
			device.DrawLabel(_hint);
			device.DrawLabel(_hints);
		}
		void iSpriteEnabled(bool b) override { _console.Active(b); }
		bool iSpriteEnabled() const override { return _console.Active(); }
		void iSpriteExpired(bool b) override { }
		bool iSpriteExpired() const override { return false; }
		void iSpriteInit(Gpu::D2D & device) override {
			_line.Box.Mode = Gpu::D2D::SSModes::Interval;
			_line.TVAlign = Gpu::D2D::TVAligns::TBottom;
			_hint.Box.Mode = Gpu::D2D::SSModes::Interval;
			_hint.TVAlign = Gpu::D2D::TVAligns::TBottom;
			_hints.Box.Mode = Gpu::D2D::SSModes::Interval;
			_hints.TVAlign = Gpu::D2D::TVAligns::TBottom;
			_history.Box.Mode = Gpu::D2D::SSModes::Interval;
			_history.TVAlign = Gpu::D2D::TVAligns::TBottom;
		}
		void iSpriteUpdate(Units::Time_MicroSecondF delta, Units::Time_MicroSecondF time) override {
			updateHint();
			updateHints();
			updateLine(delta);
			updateHistory();
		}

	protected: // methods
		void updateHistory() {
			_history.String.Clear();
			_history.String.Append(_console.History).Append('\n').Append('\n');
		}
		void updateLine(Units::Time_MicroSecondF delta) {
			static DD::Gpu::D2D::Fonts hi = DD::Gpu::D2D::Fonts::DEFAULT_BLUE;
			hi->Underline = Gpu::D2D::FUnderlines::Yes;

			_line.String.Clear().Append(_console.Line.Text);
			if (_console.Line.IsCursorNaive())
				_line.String.Append('_');

			_line.String.Append('\n');

			// normalize time
			_tickTime = Math::Mod(_tickTime + delta, Tick);

			// highlight substring
			if (_tickTime < HalfTick || _console.Line.IsSelectionValid()) {

				IntervalS range = _console.Line.IsSelectionValid() ? IntervalS::From(_console.Line.Selection) : IntervalS{ _console.Line.Cursor, _console.Line.Cursor + 1 };
				_line.String.Color(hi, range);
			}
		}
		void updateHint() {
			static DD::Gpu::D2D::Fonts hintColor = DD::Gpu::D2D::Fonts::DEFAULT_WHITE;
			hintColor->Brush.SetA(128);

			_hint.String.Clear();
			if (_console.Hints.Size() && _console.Line.IsCursorNaive()) {
				_hint.String.Append(_console.Hints[0]).Append('\n');
				_hint.String.Color(hintColor);
			}
		}
		void updateHints() {
			_hints.String.Clear();
			if (_console.Line.IsCursorNaive()) {
				size_t index = 0;
				for (Text::StringView16 view : _console.Hints) {
					Text::StringReader16 reader(view);
					_hints.String
						.Append('[').Append(++index).Append(']')
						.Append(' ')
						.Append(reader.ReadNextNonWS())
						.Append(' ').Append(' ');
				}
			}
			else {
				if (_console.Hints.IsNotEmpty())
					_hints.String.Append(_console.Hints.First());
			}
		}

	public:
		/** Access console object. */
		Input::CommandConsole & Console() { return _console; }

	public:
		/** Constructor. */
		CommandConsoleSprite(DD::CommandLine::Program & program) : ISprite(L"ConsoleSprite"), _console(program) { }
	};
}
