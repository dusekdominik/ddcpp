#pragma once
namespace DD::Simulation {
	/** */
	struct ProgressBarSprite;
}

#include <DD/IProgress.h>
#include <DD/Simulation/ISprite.h>

namespace DD::Simulation {
	struct ProgressBarSprite
		: public ISprite
		, public IProgress
	{
	protected: // properties
		/** Should be displayed by engine. */
		bool _enabled = false;
		/** should be removed from engine. */
		bool _expired = false;
		/** Area of whole sprite. */
		Gpu::D2D::Boxes _box;
		/** Progress bar. */
		Gpu::D2D::Rectangles _loader;
		/** Background of progress bar. */
		Gpu::D2D::Rectangles _loaderBg;
		/** Background of whole sprite. */
		Gpu::D2D::Rectangles _bg;
		/** Value label. */
		Gpu::D2D::Labels _name;
		/** Percentage label. */
		Gpu::D2D::Labels _value;
		/** Callback storage for IProgress. */
		Action16<> _beginCallback;
		/** Callback storage for IProgress. */
		Action16<> _endCallback;

	protected: // ISprite
		virtual void iSpriteDraw(Gpu::D2D & device) override;
		virtual void iSpriteEnabled(bool value) override;
		virtual bool iSpriteEnabled() const override;
		virtual void iSpriteExpired(bool value) override;
		virtual bool iSpriteExpired() const override;
		virtual void iSpriteInit(Gpu::D2D & device) override;
		virtual void iSpriteUpdate(Units::Time_MicroSecondF delta, Units::Time_MicroSecondF time) override;

	public: // methods
		/** Set position for rendering. */
		void Box(const Gpu::D2D::Boxes & box) { _box = box; }

	public: // constructors
		/** Constructor. */
		ProgressBarSprite(const String & name);
		/** Destructor. */
		~ProgressBarSprite() override = default;
	};

	inline void ProgressBarSprite::iSpriteDraw(Gpu::D2D & device) {
		auto area = device.Relative(_box);
		f32 scale = (0.75f / 400.f) * area.Dimensions.x;
		_name.Scale = scale;
		_value.Scale = scale;
		area.DrawRectangle(_bg);
		area.DrawRectangle(_loaderBg);
		area.DrawRectangle(_loader);
		area.DrawLabel(_name);
		area.DrawLabel(_value);
	}

	inline void ProgressBarSprite::iSpriteEnabled(bool value) {
		_enabled = value;
	}

	inline bool ProgressBarSprite::iSpriteEnabled() const {
		return _enabled;
	}

	inline void ProgressBarSprite::iSpriteExpired(bool value) {
		_enabled = !(_expired = value);
	}

	inline bool ProgressBarSprite::iSpriteExpired() const {
		return _expired;
	}

	inline void ProgressBarSprite::iSpriteInit(Gpu::D2D & device) {
		_bg.Box.Mode = Gpu::D2D::SSModes::Interval;
		_bg.Box.HAlign = Gpu::D2D::HAligns::HLeft;
		_bg.Box.VAlign = Gpu::D2D::VAligns::VTop;
		_bg.Background = 0xAAAAAAAA;
		_bg.Frame.Brush = Colors::YELLOW;
		_bg.Frame.Size.Value = 1.0f;

		_loaderBg.Box.Origins.Set(0.0f, 0.5f);
		_loaderBg.Box.Dimensions.Set(1.0f, 0.5f);
		_loaderBg.Box.Mode = Gpu::D2D::SSModes::Interval;
		_loaderBg.Box.HAlign = Gpu::D2D::HAligns::HLeft;
		_loaderBg.Box.VAlign = Gpu::D2D::VAligns::VTop;
		_loaderBg.Background = 0xAAAAAAAA;

		_loader.Box = _loaderBg.Box;
		_loader.Background = 0xAA0000FF;

		_value.Box = _loaderBg.Box;
		_value.THAlign = Gpu::D2D::THAligns::TCenter;
		_value.TVAlign = Gpu::D2D::TVAligns::TCenter;

		_name.Box.Dimensions.Set(1.0f, 0.5f);
		_name.Box.HAlign = Gpu::D2D::HAligns::HLeft;
		_name.Box.VAlign = Gpu::D2D::VAligns::VTop;
		_name.Box.Mode = Gpu::D2D::SSModes::Interval;
		_name.THAlign = Gpu::D2D::THAligns::TCenter;
		_name.TVAlign = Gpu::D2D::THAligns::TCenter;
		_name.String.Append(Name());
	}

	inline void ProgressBarSprite::iSpriteUpdate(Units::Time_MicroSecondF delta, Units::Time_MicroSecondF time) {
		// if nothing is set, show zero on progress bar
		f32 value = 0;
		// get value from callback
		IProgress::Get(value);
		value = Math::Saturate(value);
		// bound value between zero and one, and convert to percentage
		_loader.Box.Dimensions.x = value;
		// create label with percentage
		_value.String.Clear()
			.AppendEx(value * 100.f, L".1")
			.Append(" %");

		_loader.Background = Color(value, 1.f - value, 0.f);
	}

	inline ProgressBarSprite::ProgressBarSprite(const String & name)
		: ISprite(name)
		, IProgress(_beginCallback, _endCallback)
		, _beginCallback([this]() { Enabled(true); })
		, _endCallback([this]() { Expired(true); }) {
	}
}
