#ifndef DD_SHADER_FUNCTIONS_H
#define DD_SHADER_FUNCTIONS_H

#include <DD/Simulation/ShaderHelpers.h>
#include <DD/Simulation/ConstantBuffers.h>

/** Color constants. */
static const float4 DD_COLOR_MAGENTA   = float4(1, 0, 1, 1);
static const float4 DD_COLOR_BLUE      = float4(0, 0, 1, 1);
static const float4 DD_COLOR_GREEN     = float4(0, 1, 0, 1);
static const float4 DD_COLOR_YELLOW    = float4(1, 1, 0, 1);
static const float4 DD_COLOR_RED       = float4(1, 0, 0, 1);
static const float4 DD_COLOR_UNDEFINED = float4(0, 0, 0, 0);

/** Convert 0-255 interval to 0.0 - 1.0. */
static const float DD_BYTE_2_SCALE = 0.00390625f;


/**
 * Mix two colors according to scale.
 * @param scale - [0.0 - 1.0] number.
 * @param lColor - color of low scale.
 * @param hColor - color of high scale.
 * @return mixed color.
 */
float4 Scale2Color(float scale, float4 lColor, float4 hColor) {
	return scale * hColor + (1.0f - scale) * lColor;
}

/**
 * Color scale.
 * Magenta - Blue - Green - Yellow - Red
 * @param [0.0 - 1.0] number.
 * @return colored scale.
 */
float4 Scale2Color(float scale) {

	// extend scale according to number of colors
	float scaleEx = scale * 4;
	// integral part
	int scaleI = (int)scaleEx;
	// fractional part
	float part = scaleEx - scaleI;
	// select color
	switch (scaleI) {
	case 0: return Scale2Color(part, DD_COLOR_MAGENTA, DD_COLOR_BLUE);
	case 1: return Scale2Color(part, DD_COLOR_BLUE, DD_COLOR_GREEN);
	case 2: return Scale2Color(part, DD_COLOR_GREEN, DD_COLOR_YELLOW);
	case 3: return Scale2Color(part, DD_COLOR_YELLOW, DD_COLOR_RED);
	case 4: return DD_COLOR_RED;
		// out of bounds - detectable by alpha channel
	default: return DD_COLOR_UNDEFINED;
	}
}

/**
 * Color numbers out of 0.0 - 1.0 scale.
 * @param scale number number.
 * @return red for values over 1 and blue for colors bellow 0.
 */
float4 OutScale2Color(float scale) {
	if (scale < 0)
		return DD_COLOR_BLUE;
	if (1 < scale)
		return DD_COLOR_RED;

	return DD_COLOR_UNDEFINED;
}

/** Get color of currently processed sample*/
float4 Color24RGB(int rgb) {
	const uint red = ((rgb >> 0) & 0xff);
	const uint green = ((rgb >> 8) & 0xff);
	const uint blue = ((rgb >> 16) & 0xff);

	return float4(
		float3(red, green, blue) * DD_BYTE_2_SCALE,
		1
	);
}

/** Get color of currently processed sample*/
float4 Color32RGBA(int rgb) {
	const uint red   = ((rgb >> 0) & 0xff);
	const uint green = ((rgb >> 8) & 0xff);
	const uint blue  = ((rgb >> 16) & 0xff);
	const uint alpha = ((rgb >> 24) & 0xff);

	return float4(red, green, blue, alpha) * DD_BYTE_2_SCALE;
}

/** Create scale value (0.0 - 1.0) from given world-space height. */
float WSHeightScale(float height) {
	return (height - SceneHeightMin) * SceneHeightRangeInv;
}

/**
 * Return the vertex color, expecting the float main color definition.
 * @param wsPosition - position in world-space coordinates.
 * @param csPosition - position in camera-space coordinates.
 * @param color      - input definition of vertex color.
 */
float4 ColorF(float3 wsPosition, float4 csPosition, float4 color) {
	switch (CameraFlags) {
		case _CAMERA_FLAG_COLOR:
			return color;
		case _CAMERA_FLAG_HEIGHT:
			return Scale2Color(WSHeightScale(wsPosition.y));
		case _CAMERA_FLAG_HEIGHT_TUNE:
			return OutScale2Color(WSHeightScale(wsPosition.y));
		case _CAMERA_FLAG_DEPTH:
			return Scale2Color(csPosition.w * SceneDepthInv);
		default:
			return float4(0, 0, 0, 0);
	}
}

/**
 * Return the vertex color, expecting the float main color definition.
 * @param wsPosition - position in world-space coordinates.
 * @param csPosition - position in camera-space coordinates.
 * @param color      - input definition of vertex color.
 */
float4 ColorI24(float3 wsposition, float4 csposition, int color) {
	return ColorF(wsposition, csposition, Color24RGB(color));
}

/**
 * Return the vertex color, expecting the float main color definition.
 * @param wsPosition - position in world-space coordinates.
 * @param csPosition - position in camera-space coordinates.
 * @param color      - input definition of vertex color.
 */
float4 ColorI32(float3 wsposition, float4 csposition, int color) {
	return ColorF(wsposition, csposition, Color32RGBA(color));
}
#endif // !DD_SHADER_FUNCTIONS_H