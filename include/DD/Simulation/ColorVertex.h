#ifndef DD_COLOR_VERTEX_H
#define DD_COLOR_VERTEX_H

#include "ShaderHelpers.h"

#define ColorVertexDeclaration(parser) \
 parser(float3,   Position, POSITION, 0, PerVertex, 0, 0)\
 parser(ColorInt, Color,    COLOR,    0, PerVertex, 0, 0)

namespace DD {
	namespace Simulation {
		/**
		 * ColorVertex
		 *
		 * Vertex designed for rendering colored polygons.
		 */
		struct ColorVertex {
			STATIC_TRAITS_DECLARATION
				ColorVertexDeclaration(PARSER_MEMORY)
		};
	}
}
#endif
