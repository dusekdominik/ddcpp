#pragma once
namespace DD::Controls {
	/**
	 * Ancestor of visual controls.
	 * Current implementation reflecting winapi.
	 * Cannot be used itself, derived classes.
	 */
	struct IControl;
}

#include <DD/Color.h>
#include <DD/Concurrency/ThreadSafeObject.h>
#include <DD/List.h>
#include <DD/Delegate.h>
#include <DD/Reference.h>
#include <DD/String.h>

namespace DD::Controls {
	struct IControl
		: public IReferential
	{
		/** Horizontal alignment of control. */
		enum struct HAlign { None, Left, Right, Center, Stretch };
		/** Vertical alignment of control. */
		enum struct VAlign { None, Bottom, Top, Center, Stretch };
		/** Alignment of text. */
		enum struct TAlign { Left, Center, Right, Justify };
		/** Modes for computing width. */
		enum struct SizeModes { Pixels, Percents, Partitions, Points, Adaptive };
		/** Rectangle for area description. */
		struct Rect { i32 Left = 0, Top = 0, Width = 0, Height = 0; };
		/** Descriptor of the size. */
		struct Size { SizeModes Mode; float Value; };
		/** Is control visible. */
		enum struct Visibility { Visible, Invisible };
		/** Description of position in grid. */
		struct GridPosition { i32 Row = 0, RowSpan = 1, Col = 0, ColSpan = 1; };
		/** Padding/Margin struct. */
		struct Padding { i32 Left = 0, Top = 0, Right = 0, Bottom = 0; };

		/** Default values. */
		struct Defaults {
			constexpr static wchar_t FontFamily[] = L"Tahoma";
			constexpr static i32 FontSize = 16;
			constexpr static Color Background = 0xFF000000;
			constexpr static i32 Left = 100;
			constexpr static i32 Top = 100;
			constexpr static i32 Width = 800;
			constexpr static i32 Height = 600;
			constexpr static HAlign HorizontalAlignment = HAlign::Stretch;
			constexpr static VAlign VerticalAlignment = VAlign::Stretch;
		};

	protected:  // properties
		/** Internal storage. */
		mutable void * _handles[3];
		/** Internal storage. */
		mutable void * _menuHandle = 0;
		/** Handle to the tooltip control. */
		mutable void * _tooltipHandle = 0;
		/** Children of control. */
		Concurrency::ThreadSafeObject<List<IControl*, 32>> _children;
		/** Parent window in visual tree. */
		IControl * _parent = 0;
		/** Vertical alignment of control. */
		VAlign _valign;
		/** Horizontal alignment of control. */
		HAlign _halign;
		/** Grid position of the control. */
		GridPosition _grid;
		/** Design size. */
		Rect _design;
		/** Actual computed size. */
		Rect _actual;
		/** Client area size. */
		Rect _client;
		/** Margin of window. */
		i32 _margin = 0;
		/** Scroll offsets. */
		i32 _hOffset = 0, _vOffset = 0;
		/** Scale of window. */
		f32 _scale = 1.f;
		/** Is window has been initialized. */
		bool _ready = false;
		/** Control is shown or hidden. */
		bool _show = true;
		/** Control function enabled, e.g. button control is enabled while it is clickable. */
		bool _enabled = true;
		/** Calculation of rect is dedicated to the parent. */
		bool _dedicatedRect = 0;
		/** Tooltip. */
		StringLocal<256> _tooltip;
		/** Name of current font. */
		StringLocal<32> _fontName;
		/** Size of the font. */
		i32 _fontSize;
		/** Background of the control. */
		Color _background;

	public: // events
		Event<> OnScaleChanged;
		/** Invoked if size of control changed.  */
		Event<> OnSizeChanged;
		/** Invoked if position of control changed.  */
		Event<> OnPositionChanged;
		/** Grid info has been changed. */
		Event<GridPosition, GridPosition> OnGridChanged;
		/** Invoked if child has been added. */
		Event<IControl *> OnChildAdd;
		/** Invoked if child has been removed. */
		Event<IControl *> OnChildRemove;

	public: // methods
		/** Recompute client area. */
		void recomputeClient();
		/** Recompute window area. */
		void recomputeWindow();
		/** Recompute size of control. */
		void recomputeRect();
		/** Recompute params actual, client, design.  */
		virtual void iControlRect(const Rect &);
		/** Initialize control. */
		virtual void iControlInit() = 0;
		/** Destroy control. */
		virtual void iControlDestroy();

	public: // methods
		/** Add children. */
		void Add(IControl * ctrl);
		/** Remove control. */
		void Remove();
		/** Remove children. */
		void Remove(IControl * ctrl);
		/** Initialize control and its children. */
		void Init(IControl * parent = nullptr);
		/** Destroy window and its children. */
		void Destroy();
		/** Native handle of  */
		void * NativeHandle() { return _handles[0]; }
		/** Native handle of window. */
		const void * NativeHandle() const { return _handles[0]; }
		/** Is control initialized. */
		bool Ready() const { return _ready; }

	public: // property methods
		/** Scale of control. */
		f32 Scale() const { return _scale; }
		/** Scale of control. */
		void Scale(f32 scale, bool autopropagation = true);
		/** Tooltip getter. */
		const String & Tooltip() const { return _tooltip; }
		/** Tooltip setter. */
		void Tooltip(const String & tip);
		/**
		 * Get grid info for IControl.
		 * These values are active while the IControl is used in a Grid or table container.
		 */
		GridPosition Grid() { return _grid; }
		/**
		 * Set grid info for IControl.
		 * These values are active while the IControl is used in a Grid or table container.
		 */
		void Grid(const GridPosition & grid);
		void GridCol(i32);
		void GridColSpan(i32);
		void GridRow(i32);
		void GridRowSpan(i32);
		/** Rectangle of client area in pixels. */
		const Rect & Client() const { return _client; }
		/** Designed size and position, this values are depending on alignment or grid settings in pixels. */
		const Rect & Design() const { return _design; }
		/** */
		void Design(const Rect &);
		/** Margin of the window in pixels */
		i32 Margin() const { return _margin; }
		/** Margin of the window in pixels */
		void Margin(i32 m) { _margin = m; }
		/** Horizontal position of control. */
		i32 Left() const { return _design.Left; }
		/** Horizontal position of control. */
		void Left(i32);
		/** Vertical position of control. */
		i32 Top() const { return _design.Top; }
		/** Vertical position of control. */
		void Top(i32);
		/** Horizontal size of control. */
		i32 Width() const { return _design.Width; }
		/** Horizontal size of control. */
		void Width(i32, bool scaled = false);
		/** Vertical size of control. */
		i32 Height() const { return _design.Height; }
		/** Vertical size of control. */
		void Height(i32, bool scaled = false);
		/** Horizontal alignment of control. */
		HAlign HorizontalAlignment() const { return _halign; }
		/** Horizontal alignment of control. */
		void HorizontalAlignment(HAlign align) { _halign = align; recomputeRect(); }
		/** Vertical alignment of control. */
		VAlign VerticalAlignment() const { return _valign; }
		/** Vertical alignment of control. */
		void VerticalAlignment(VAlign align) { _valign = align; recomputeRect(); }
		/** Get Background. */
		Color Background() const { return _background; }
		/** Set Background. */
		void Background(Color c);
		/** Set font of the window. */
		void Font(Text::StringView16 family, i32 size);
		/** Get font-family. */
		Text::StringView16 FontFamily() const { return _fontName; }
		/** */
		i32 FontSize() const { return _fontSize; }
		/** Show or hide window. */
		void Show(bool show);
		/** Is windows shown. */
		bool Show() const { return _show; }
		/** Is control enabled. E.g. disabled button is not clickable. */
		void Enabled(bool enabled);
		/** Is control enabled. E.g. disabled button is not clickable. */
		bool Enabled() const { return _enabled; }
		/** Is window size recalculated by itself or by external authority. */
		bool AutoRectRecalculation() const { return !_dedicatedRect; }
		/** Is window size recalculated by itself or by external authority. */
		void AutoRectRecalculation(bool value) { _dedicatedRect = !value; }

	protected: // constructors
		/** Constructor. */
		IControl();

	public: // constructors
		/** Virtual destructor. */
		virtual ~IControl() { Destroy(); }
	};
}
