#pragma once
namespace DD::Controls {
	/**
	 * Visual button control.
	 * @example
	 *   static Window myWindow = Window(WindowTypes::Basic);        // create window as visual place
	 *   static ButtonControl myButton = ButtonControl("OK button"); // create button with a text
	 *   myButton->Left(100);                                        // set dimensions and position
	 *   myButton->Top(100);
	 *   myButton->Width(100);
	 *   myButton->Height(32);
	 *   myButton->Click += []() { doWhateverYouWant(); };           // add routine to be done on click event
	 *   myWindow->Add(myButton);                                    // add button into the window
	 *   myWindow->Init();                                           // initialize
	 */
	struct ButtonControl;
}

#include <DD/Controls/IControl.h>
#include <DD/Delegate.h>
#include <DD/Reference.h>

namespace DD::Controls {
	class ButtonControlBase
		: public IControl
	{
	protected: // properties
		/** Alignment of button. */
		TAlign _talign = TAlign::Center;
		/** Frame between text and control edge. */
		IControl::Padding _padding;
		/** Text of the button. */
		StringLocal<32> _text;
		/** Resize delegate. */
		Delegate<> _resize;

	protected: // IControl
		virtual void iControlRect(const Rect &) override;
		virtual void iControlInit() override;

	public: // events
		/** Click event. */
		Event<> OnClick;

	public: //
		/** Getter of padding. */
		const IControl::Padding & Padding() const { return _padding; }
		/** Setter of padding. */
		void Padding(const IControl::Padding & padding) { _padding = padding; OnSizeChanged(); }
		/** Alignment of button. */
		TAlign TextAlignment() const { return _talign; }
		/** Alignment of button. */
		void TextAlignment(TAlign align);

	public: // constructors
		/** Constructor. */
		ButtonControlBase(const Text::StringView16 & str);
	};

	struct ButtonControl
		: public Reference<ButtonControlBase>
	{
		/** Nullptr constructor. */
		ButtonControl() : Reference() {}
		/** Constructor. */
		ButtonControl(const Text::StringView16 & text) : Reference(new ButtonControlBase(text)) {}
	};
}
