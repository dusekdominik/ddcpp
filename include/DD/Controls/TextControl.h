#pragma once
namespace DD::Controls {
	/**
	 * TextControl
	 * Control for text input from the user side.
	 *
	 * @example simple usage
	 *  static ::DD::Window window = ::DD::Window(::DD::Window::Types::Basic, "MyWindow");
	 *  ::DD::TextControl text = ::DD::TextControl("Hello world");
	 *  text->HorizontalAlignment(::DD::IControl::HAlign::Stretch);
	 *  text->VerticalAlignment(::DD::IControl::VAlign::Stretch);
	 *  window->Add(text);
	 *  window->Init();
	 */
	struct TextControl;
}

#include <DD/Controls/IControl.h>
#include <DD/Reference.h>
#include <DD/String.h>

namespace DD::Controls {
	class TextControlBase
		: public IControl
	{
		friend struct TextControl;

	protected: // properties
		bool _silent = false;
		/** Storage for the text. */
		StringLocal<256> _text;
		/** Frame between text and control edge. */
		IControl::Padding _padding;
		/** Text alignment. */
		TAlign _talign = TAlign::Left;

	public:
		/** Text changed event.  */
		Event<> OnTextChanged;
		/** Text changed event from program side. */
		Event<> OnTextUpload;
		/** Text changed event from user side. */
		Event<> OnTextDownload;

	protected: // IControl
		virtual void iControlInit() override;
		virtual void iControlRect(const Rect &) override;

	protected: // internal methods
		/** Get text from the gui side.  */
		void downloadText();
		/** Set gui text from the program side. */
		void uploadText();

	public: // methods
		/** Getter of text. */
		const String & Text() const { return _text; }
		/** Setter of text. */
		void Text(const DD::Text::StringView16 & text, bool silent = false);
		/** Getter of padding. */
		const IControl::Padding & Padding() const { return _padding; }
		/** Setter of padding. */
		void Padding(const IControl::Padding & padding) { _padding = padding; OnSizeChanged(); }
		/** Alignment of text getter. */
		TAlign TextAlignment() const { return _talign; }
		/** Alignment of text setter. */
		void TextAlignment(TAlign align);

	protected: // constructors
		/** Constructors. */
		TextControlBase(const String & text) : _text(text) {}
	};


	struct TextControl
		: public Reference<TextControlBase>
	{
		/** Nullptr constructor. */
		TextControl() : Reference() {}
		/** Constructor. */
		TextControl(const String & text) : Reference(new TextControlBase(text)) {}
	};
}
