#pragma once
namespace DD::Controls {
	/**
	 * Initializers for window constructor.
	 */
	enum struct WindowTypes { Basic };

	/**
	 * Abstraction of window.
	 *
	 * @example tray instead of close
	 *  static ::DD::Window w = ::DD::Window(Window::Types::Basic);
	 *  w->Init();
	 *  w->OnClose += [](bool & close) {
	 *    close = false;
	 *    w->Tray().Enable();
	 *    w->Show(false);
	 *  };
	 *  w->Tray().OnDoubleClick += [](){
	 *		w->Show(true);
	 *    w->Tray().Disable();
	 *  };
	 *
	 * @example window menu
	 *  static ::DD::Window w = ::DD::Window(Window::Types::Basic);
	 *  auto menu = w->Menu();
	 *  auto file = menu.AddItem("File");
	 *  file.AddItem("&Open", [](){ openTheFile(); });
	 *  file.AddItem("&Save", [](){ saveTheFile(); });
	 *  file.AddItem("Save as", [](){ saveAsTheFile(); });
	 *  w->Init();
	 */
	class Window;
}

#include <DD/Controls/IControl.h>
#include <DD/Delegate.h>
#include <DD/Input/Key.h>
#include <DD/String.h>
#include <DD/Types.h>

namespace DD::Controls {
	class WindowBase
		: public IControl
	{
		friend class Window;

	public: // nested
		/** Native view to system handles. */
		struct NativeView;
		/** Proxy object for access standard menu of the window. */
		struct MenuProxy;
		/** Proxy object for access of window trays. */
		struct TrayProxy;
		/** Storage of one tray icon. */
		struct TrayStore;

	protected: // properties
		/** Always on top. */
		bool _onTop = 0;
		/** List of trays. */
		List<TrayStore> _trays;
		/** Storage of window menu. */
		List<Event<i32>, 16> _menu;
		/** Mouse position. */
		i32 _mouseX, _mouseY;
		/** Title of window. */
		String _title;
		/** Predefined style of window. */
		WindowTypes _style;

	public: // properties
		/** Invoked when window is closed. */
		Event<bool&> OnClose;
		/** Invoked when window is closed. */
		Event<> OnLoad;
		/** Invoked when a key is pressed. */
		Event<Input::Key> OnKeyDown;
		/** Invoked when a key is released. */
		Event<Input::Key> OnKeyUp;
		/** Invoked when a mouse position changed. */
		Event<i32, i32> OnMouseMove;
		/** Invoked when a mouse scrolled. */
		Event<i32> OnMouseWheel;
		/** Invoked when left button is pressed. */
		Event<> OnMouseLeftDown;
		/** Invoked when middle button is pressed. */
		Event<> OnMouseMiddleDown;
		/** Invoked when right button is pressed. */
		Event<> OnMouseRightDown;
		/** Invoked when left button is released. */
		Event<> OnMouseLeftUp;
		/** Invoked when middle button is released. */
		Event<> OnMouseMiddleUp;
		/** Invoked when right button is released. */
		Event<> OnMouseRightUp;
		/** Invoked on double-click event. */
		Event<> OnMouseLeftDoubleClick;
		/** Invoked on double-click event. */
		Event<> OnMouseMiddleDoubleClick;
		/** Invoked on double-click event. */
		Event<> OnMouseRightDoubleClick;
		/** Invoked while topmost state changed. */
		Event<> OnAlwaysOnTop;
		/** Invoked when no incoming messages. */
		Event<> OnIdle;
		/** Invoked when window got keyboard focus. */
		Event<> OnGotFocus;
		/** Invoked when window lost keyboard focus. */
		Event<> OnLostFocus;
		/** Invoked if children is notifying. */
		Event<u64, u32> OnNotify;
		/** Timer tick event. */
		Event<i32> OnTimerTick;

	protected: // methods
		/** Internal. */
		void updateTitle();

	protected: // IControl
		virtual void iControlInit() override;
		virtual void iControlDestroy() override;

	public: // internal methods (!do not use!)
		/** Casting helper. */
		NativeView * nativeView() const;
		/** Internal. */
		void updateMouse(i32 x, i32 y);
		/** Internal. */
		void updatePosition(i32 l, i32 t);
		/** Internal. */
		void updateSize(u32 w, u32 h);

	public: // methods
		/** Has been already initialized. */
		bool Ready() const;
		/** Is window focused. */
		bool Focused() const;

	public: // property getters
		/** AlwaysOnTop. */
		bool AlwaysOnTop() const { return _onTop; }
		/** Mouse position relative to window client area. */
		i32 MouseX() { return _mouseX; }
		/** Mouse position relative to window client area. */
		i32 MouseY() { return _mouseY; }
		/** Title of window. */
		const String & Title() const { return _title; }
		/** Design style of window. */
		WindowTypes Style() const { return _style; }
		/**
		 * Clipboard of the window.
		 * @return empty string if clipboard does not contain valid content
		 *   or if opening of clipboard fails; otherwise return clipboard text.
		 */
		String Clipboard() const;

	public: // property setters
		/** Position of window. */
		WindowBase & Position(i32 x, i32 y);
		/** Size of window. */
		WindowBase & Size(u32 w, u32 h);
		/** Title of window. */
		WindowBase & Title(const String &);
		/** Style of window. Cannot be set after init. */
		WindowBase & Style(WindowTypes s);
		/** Clipboard of window. */
		WindowBase & Clipboard(const String & s);

	public: // commands
		/** Set window to be always on top. */
		void AlwaysOnTop(bool);
		/** Mouse position relative to window client area. */
		void Mouse(i32 x, i32 y);
		/** Close window. */
		void Close();
		/** Maximize window. */
		void Maximize();
		/** Minimize window. */
		void Minimize();
		/** Set timer. */
		void Timer(i32 timerId, u32 milliseconds);
		/** Unset timer. */
		void RemoveTimer(i32 timerId);
		/** Access standard menu of the window. */
		MenuProxy Menu();
		/** Access n-th tray of the window. */
		TrayProxy Tray(int trayId = 0);

	protected: // constructors
		/** Zero constructor. */
		WindowBase(WindowTypes type, const String & name);

	public: // constructors
		/** Destructor. */
		virtual ~WindowBase() = default;
	};

	struct WindowBase::MenuProxy {
		friend class WindowBase;

		static constexpr int MENU_FLAG = 1 << 15;

	protected:
		mutable void * _handle;
		WindowBase * _parent;
		List<Event<i32>> * _routines;

	public:
		/** Indexer. */
		MenuProxy operator[](int i) { return GetItem(i); }
		/** Number of sub menus. */
		int Size() const;
		/** Add submenu item. */
		MenuProxy AddItem(Text::StringView16 name);
		/**
		 * Add final item.
		 * @returns id of the menu item.
		 */
		i32 AddItem(Text::StringView16 name, Delegate<i32> routine);
		/** Insert submenu item. */
		MenuProxy InsertItem(int index, Text::StringView16 name);
		/**
		 * Insert final item.
		 * @returns id of the menu item.
		 */
		i32 InsertItem(i32 index, Text::StringView16 name, Delegate<i32> routine);
		/**
		 * Check item.
		 * @param id - accepts id of menu item.
		 */
		void CheckItem(i32 id, bool);
		/**
		 * Check item.
		 * @param id - accepts id of menu item.
		 */
		bool IsChecked(i32 id);
		/** Get item. */
		MenuProxy GetItem(int);
		/** Remove item. */
		void RemoveItem(int);
		/** Remove menu. */
		void Destroy();
	};


	struct WindowBase::TrayStore {
		/** Menu handle. */
		void * Menu = 0;
		/** On DoubleClick event. */
		Event<> OnDoubleClick;
		/** Events of possible context menu of the tray. */
		List<Event<i32>> TrayMenuRoutines;
	};


	struct WindowBase::TrayProxy {
		friend class WindowBase;

	protected: // properties
		WindowBase * _parent;
		int _index;

	public: // events
		/** DoubleClick event. */
		Delegate<> OnDoubleClick;

	public: // methods
		/** Enable Tray. */
		void Enable();
		/** Disable Tray. */
		void Disable();
		/** Tooltip message of the Tray.  */
		void Tooltip(const String & tip);
		/** Context menu of the tray. */
		MenuProxy Menu();
	};


	class Window
		: public Reference<WindowBase>
	{
	public: // nested
		typedef WindowBase::MenuProxy MenuProxy;
		typedef WindowBase::TrayProxy TrayProxy;
		typedef WindowTypes Types;

	public:
		/** Enable visualization of mouse cursor. */
		static void ShowCursor();
		/** Disable visualization of mouse cursor. */
		static void HideCursor();
		/** Set position of cursor. */
		static void SetCursor(i32 x, i32 y);
		/** Message loop handler. Must be placed into main thread. */
		static void Loop();

	public:
		/** Nullptr constructor. */
		Window() : Reference() {}
		/** Catch constructor. */
		Window(WindowBase * ptr) : Reference(ptr) {}
		/** Window constructor - select type of window. */
		Window(Types type, Text::StringView16 name) : Reference(new WindowBase(type, name)) {}
	};
}
