#pragma once
namespace DD::Controls {
	/**
	 * Types of TabViewControl.
	 */
	enum struct TabViewControlTypes { Basic };

	/**
	 * TabView control.
	 * Represents set of "cards", which are associated with a content.
	 *
	 * @example simple usage
	 *  static ::DD::Window window = ::DD::Window(::DD::Window::Types::Basic, "MyWindow");
	 *  ::DD::TabViewControl tabs = ::DD::TabViewControl(::DD::TabViewControl::Types::Basic);
	 *  ::DD::PanelControl tab1 = ::DD::PanelControl(::DD::PanelControl::Types::Basic);
	 *  ::DD::PanelControl tab2 = ::DD::PanelControl(::DD::PanelControl::Types::Basic);
	 *  tab1->Background(::DD::Colors::GREEN);
	 *  tab2->Background(::DD::Colors::RED);
	 *  tabs->AddTab("First tab", tab1);
	 *  tabs->AddTab("Second tab", tab2);
	 *  window->Add(tabs);
	 *  window->Init();
	 */
	struct TabViewControl;
}

#include <DD/Controls/IControl.h>
#include <DD/Delegate.h>
#include <DD/List.h>
#include <DD/Reference.h>

namespace DD::Controls {
	class TabViewControlBase
		: public IControl
	{
		friend struct TabViewControl;

		/** Description of a tab. */
		struct TabDesc { StringLocal<32> Name; Reference<IControl> Control; };

	protected: // properties
		/** index of current tab. */
		i32 _index = 0;
		/** List of the tabs. */
		List<TabDesc, 16> _tabs;

	public: // events
		/** TabChanged event. */
		Event<i32, i32> OnTabChanged;

	protected: // IControl
		virtual void iControlInit() override;
		virtual void iControlRect(const Rect &) override;

	public: // methods
		/** Append tab.  */
		void AddTab(const String & name, IControl * tab) { InsertTab((i32)_tabs.Size(), name, tab); };
		/** Insert tab into the n-th position. */
		void InsertTab(i32 index, const String & name, IControl * tab);
		/** Set tab name and its control. */
		void SetTab(i32 index, const String & name, IControl * tab);
		/** Set name and control of the selected tab. */
		void SetTab(const String & name, IControl * tab) { SetTab(Index(), name, tab); }
		/** Setter of the index of the current tab. */
		void Index(i32);
		/** Getter of the index of the current tab. */
		i32 Index() const { return _index; }
		/** Getter of first index of tab with given name. Returns -1 if there is no match. */
		i32 Name2Index(const Text::StringView16 & name) const;
		/** Control associated with the nth tab. */
		IControl * Tab(i32 index) { return _tabs[index].Control; }
		/** Control associated with the current tab. */
		IControl * Tab() { return _tabs[_index].Control; }
		/** Number of tabs. */
		size_t TabCount() const { return _tabs.Size(); }
		/** Name of tab with given index. */
		const String & TabName(i32 index) const { return _tabs[index].Name; }
		/** Name of currently selected tab. */
		const String & TabName() const { return TabName(Index()); }

	protected: // constructors
		/** Constructor. */
		TabViewControlBase(TabViewControlTypes);
	};


	struct TabViewControl
		: public Reference<TabViewControlBase>
	{
		typedef TabViewControlTypes Types;

		/** Nullptr constructor. */
		TabViewControl() : Reference() {}
		/** Constructor. */
		TabViewControl(Types type) : Reference(new TabViewControlBase(type)) {}
	};
}
