#pragma once
namespace DD::Controls {
	/**
	 * Slider control.
	 *
	 * @example simple usage
	 *  static ::DD::Window window = ::DD::Window(::DD::Window::Types::Basic, "MyWindow");
	 *  static ::DD::SliderControl slider = ::DD::SliderControl(0, 100, 0);
	 *  window->Add(slider);
	 *  window->Init();
	 */
	struct SliderControl;
}

#include <DD/Controls/IControl.h>
#include <DD/Delegate.h>
#include <DD/Reference.h>
#include <DD/String.h>

namespace DD::Controls {
	class SliderControlBase
		: public IControl
	{
		friend struct SliderControl;

	protected: // properties
		/** Slider settings. */
		i32 _min, _max, _value;

	public: // events
		/** Value changed event. */
		Event<i32, i32> OnValueChanged;
		/** Value changed event from program side. */
		Event<i32, i32> OnValueUpload;
		/** Value changed event from user side. */
		Event<i32, i32> OnValueDownload;

	protected: // IControl
		virtual void iControlInit() override;
		virtual void iControlRect(const Rect &) override;

	public: // internal methods
		/** Set current value from visual cursor into local storage. */
		void downloadValue();
		/** Set current value from local storage into visual control. */
		void uploadValue();
		/** Set minimum from local storage into visual control. */
		void uploadMin();
		/** Set maximum from local storage into visual control. */
		void uploadMax();

	public: // methods
		/** Getter of Maximum of the slider. */
		i32 Max() const { return _max; }
		/** Getter of minimum of the slider. */
		i32 Min() const { return _min; }
		/** Getter of current value of the slider. */
		i32 Value() const { return _value; }
		/** Setter of maximum of the slider. */
		void Max(i32 max);
		/** Setter of minimum of the slider. */
		void Min(i32 min);
		/** Setter of current value of the slider. */
		void Value(i32 value, bool silent = false);

	protected: // constructors
		/** Constructor.*/
		SliderControlBase(i32 min, i32 max, i32 value) : _min(min), _max(max), _value(value) {}
	};


	struct SliderControl
		: public Reference<SliderControlBase>
	{
		/** Nullptr constructor. */
		SliderControl() : Reference() {}
		/** Constructor. */
		SliderControl(i32 min, i32 max, i32 value) : Reference(new SliderControlBase(min, max, value)) {}
	};
}
