#pragma once
namespace DD::Controls {
	/**
	 * Visual checkbox control.
	 */
	struct ButtonControl;
}

#include <DD/Controls/IControl.h>
#include <DD/Delegate.h>
#include <DD/Reference.h>

namespace DD::Controls {
	class CheckboxControlBase
		: public IControl
	{
	protected: // properties
		/** Alignment of button. */
		TAlign _talign = TAlign::Left;
		/** Frame between text and control edge. */
		IControl::Padding _padding;
		/** Text of the button. */
		StringLocal<32> _text;
		/** Is checkbox checked. */
		bool _state = false;

	protected: // IControl
		virtual void iControlRect(const Rect &) override;
		virtual void iControlInit() override;

	protected: // methods
		void downloadState();

	public: // events
		/** State changed event. */
		Event<bool> OnStateChanged;
		/** State changed event invoke from user side. */
		Event<bool> OnStateDownload;
		/** State changed event invoke from program side. */
		Event<bool> OnStateUpload;

	public: //
		/** Getter of padding. */
		const IControl::Padding & Padding() const { return _padding; }
		/** Setter of padding. */
		void Padding(const IControl::Padding & padding) { _padding = padding; OnSizeChanged(); }
		/** Alignment of button. */
		TAlign BoxAlignment() const { return _talign; }
		/** Alignment of button. */
		void BoxAlignment(TAlign align);
		/** Is checkbox checked. */
		bool Checked() const { return _state; }
		/** Set checkbox checked or unchecked. */
		void Checked(bool checked, bool silent = false);

	public: // constructors
		/** Constructor. */
		CheckboxControlBase(const String & str, bool value);
	};

	struct CheckboxControl
		: public Reference<CheckboxControlBase>
	{
		/** Nullptr constructor. */
		CheckboxControl() : Reference() {}
		/** Constructor. */
		CheckboxControl(const String & text, bool value = false) : Reference(new CheckboxControlBase(text, value)) {}
	};
}
