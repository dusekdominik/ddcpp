#pragma once
namespace DD::Controls {
	/**
	 *
	 */
	enum struct OptionControlTypes { Basic	};

	/**
	 * Control for picking one item from a group of items.
	 *
	 * @example simple usage
	 *
	 */
	struct OptionControl;
}

#include <DD/Controls/IControl.h>
#include <DD/Delegate.h>
#include <DD/List.h>
#include <DD/Reference.h>
#include <DD/String.h>

namespace DD::Controls {
	class OptionControlBase
		: public IControl
	{
		friend struct OptionControl;

	protected: // properties
		/** Frame between options and control edge. */
		IControl::Padding _padding;
		/** Set of options in text form.  */
		List<StringLocal<32>, 64> _options;
		/** Index of selected item. */
		int _selected = 0;

	public:
		/** Selection changed event. */
		Event<i32, i32> OnSelectionChanged;
		Event<i32, i32> OnSelectionUpload;
		Event<i32, i32> OnSelectionDownload;

	protected: // IControl
		virtual void iControlInit() override;
		virtual void iControlRect(const Rect &) override;

	protected: // methods
		/** Download index from control into local storage. */
		void downloadIndex();

	public: // methods
		/** Add option. */
		void AddOption(const String & option);
		/** Clear option group. */
		void Clear();
		/** Selected index getter. */
		int Index() const { return _selected; }
		/** Selected index setter. */
		void Index(i32 index, bool silent = false);
		/** Add option. */
		void InsertOption(size_t index, const String & option);
		/** Remove option. */
		void RemoveOption(size_t index);
		/** String of selected item. */
		const String & Selected() const { return _options[_selected]; };
		/** Getter of padding. */
		const IControl::Padding & Padding() const { return _padding; }
		/** Setter of padding. */
		void Padding(const IControl::Padding & padding) { _padding = padding; OnSizeChanged(); }

	protected: // constructors
		/** Constructor. */
		OptionControlBase(OptionControlTypes type) : IControl() {}
	};


	struct OptionControl : public Reference<OptionControlBase> {
		typedef OptionControlTypes Types;
		/** Nullptr constructor. */
		OptionControl() : Reference() {}
		/** Constructor. */
		OptionControl(Types type) : Reference(new OptionControlBase(type)) {}
	};
}
