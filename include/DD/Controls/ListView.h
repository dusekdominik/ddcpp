#pragma once
namespace DD::Controls {
	/**
	 * Initializer types of list view.
	 */
	enum ListViewTypes { Basic };
	/**
	 * ListView control.
	 * @example
	 *   static Window myWindow = Window(WindowTypes::Basic);      // create window
	 *   static ListView myList = ListView(ListViewTypes::Basic);  // create lv
	 *   myWindow->Add(myList);                                    // add lv into the window
	 *   myWindow->Init();                                         // initialize
	 *   myList->Cols().Add("Col1", "Col2");                       // register cols
	 *   myList->Rows().Add("Cell 1x1", "Cell 1x2");               // insert row
	 *   myList->Rows().Add("Cell 2x1", "Cell 2x2");               // insert row
	 */
	struct ListView;
}

#include <DD/Controls/IControl.h>
#include <DD/List.h>

namespace DD::Controls {
	class ListViewBase
		: public IControl
	{
	public: // nested
		/** Description of column. */
		struct ColDescription { Size Width; };
		/** Proxy holder. */
		struct Proxy { ListViewBase * _proxy; };
		/** Proxy for access to column collection. */
		struct ColsProxy;
		/** Proxy for access to row collection. */
		struct RowsProxy;
		/** Proxy for access to single column. */
		struct ColProxy;
		/** Proxy for access to row column. */
		struct RowProxy;
		/** Proxy for access to single cell. */
		struct CellProxy;

	protected: // properties
		List<ColDescription, 8> _cols;
		int _rows = 0;

	protected: // IControl
		virtual void iControlInit() override;

	protected: // methods
		/** Set column width in pixel values. */
		void setColWidth(int i, int pixels);
		/** Recompute column width's in pixel values. */
		void recomputeCols();

	public: // interface
		/** Access to cols. */
		ColsProxy Cols();
		/** Access to rows. */
		RowsProxy Rows();

	public: // methods (raw, prefer interface access)
		/** Scroll to the given row to be visible. */
		void ShowRow(int n);
		/** Insert row into n-th position. */
		void InsertRow(int i);
		/** Insert col into n-th position. */
		void InsertCol(int i);
		/** Preallocates list to given number of items. */
		void ResizeBuffer(int i);
		/** Set text of cell. */
		void SetItemText(int row, int col, const String & text);
		/** Set column width via pixel values. */
		void SetColWidthPixel(int col, float pixels) { _cols[col].Width = { SizeModes::Pixels, pixels }; recomputeCols(); }
		/** Set column width via percents of client area. */
		void SetColWidthPercent(int col, float percents) { _cols[col].Width = {SizeModes::Percents, percents}; recomputeCols(); }
		/** Set column width via ration values. */
		void SetColWidthPartition(int col, float partition) { _cols[col].Width = {SizeModes::Partitions, partition}; recomputeCols(); }
		/** Set column caption. */
		void SetColText(int col, const String & name);
		/** Set alignment of column. */
		void SetColAlign(int col, TAlign align);
		/** Get number of columns. */
		int GetColCount() const { return (int)_cols.Size(); }
		/** Get number of rows. */
		int GetRowCount() const { return _rows; }

	public: // constructors
		/** Constructor. */
		ListViewBase(ListViewTypes type) { _valign = IControl::VAlign::Stretch, _halign = IControl::HAlign::Stretch; }
	};

	struct ListViewBase::RowsProxy
		: public ListViewBase::Proxy
	{
		/** Number of rows in collection. */
		int Size() const { return _proxy->GetRowCount(); }
		/** Add row by enumeration of texts, which should be set to single cells. */
		template<typename STRING, typename...STRINGS>
		void Add(const STRING & str, const STRINGS &...strings) { Add().Text(str, strings...); }

		/** Add row and return proxy to it. */
		RowProxy Add();
		/** Insert row and return proxy to it. */
		RowProxy Insert(int i);
		/** Get proxy to a row by index. */
		RowProxy operator[](int row);
		/** Get proxy to first row in collection. */
		RowProxy First();
		/** Get proxy to last row in collection. */
		RowProxy Last();
	};

	struct ListViewBase::ColsProxy
		: public ListViewBase::Proxy
	{
	protected: // helper methods
		template<typename STRING, typename...STRINGS>
		void batchCols(const STRING & str, const STRINGS &...batch) { Add().Text(str); batchCols(batch...); }
		void batchCols() {}

	public: // methods
		/** Number of columns in collection. */
		int Size() const { return _proxy->GetColCount(); }
		/** Add columns into collection by enumeration of the captions. */
		template<typename STRING, typename...STRINGS>
		void Add(const STRING & str, const STRINGS &...strings) { batchCols(str, strings...);  }
		/** Add single column into collection and return proxy to it. */
		ColProxy Add();
		/** Insert single column to collection and return proxy to it. */
		ColProxy Insert(int i);
		/** Get proxy to a col by index. */
		ColProxy operator[](int col);
		/** Get proxy to first column in collection. */
		ColProxy First();
		/** Get proxy to last column in collection. */
		ColProxy Last();
	};

	struct ListViewBase::ColProxy
		: public ListViewBase::Proxy
	{
		int _col;

	public:
		/** Alignment of column. */
		void Align(TAlign align) { _proxy->SetColAlign(_col, align); }
		/** Caption of column. */
		void Text(const String & name) { _proxy->SetColText(_col, name); }
		/** Width of column. */
		void WidthPixel(float pixels) { _proxy->SetColWidthPixel(_col, pixels); }
		/** Width of column. */
		void WidthPercent(float percents) { _proxy->SetColWidthPercent(_col, percents); }
		/** Width of column. */
		void WidthPartition(float partition) { _proxy->SetColWidthPartition(_col, partition); }
	};

	struct ListViewBase::RowProxy
		: public ListViewBase::Proxy
	{
		int _row;

	protected:
		template<int INDEX, typename STRING, typename...STRINGS>
		void batchText(const STRING & str, const STRINGS &...batch) { operator[](INDEX).Text(str); batchText<INDEX + 1>(batch...); }
		template<int INDEX>
		void batchText() { }

	public:
		/** Set texts of the row items. */
		template<typename...STRINGS>
		void Text(const STRINGS &...strings) { batchText<0>(strings...); }
		/** Scroll listview to be this row visible. */
		void Show() { _proxy->ShowRow(_row); }
		/** Get proxy to n-th cell. */
		CellProxy operator[](int col);
	};

	struct ListViewBase::CellProxy
		: public ListViewBase::Proxy
	{
		int _row, _col;

	public:
		/** Set text of the cell. */
		void Text(const String & name) { _proxy->SetItemText(_row, _col, name); }
		/** Get proxy to parent row. */
		RowProxy Row() { return { _proxy, _row }; }
		/** Get proxy to parent col. */
		ColProxy Col() { return ColProxy{ _proxy, _col }; }
	};

	struct ListView
		: public Reference<ListViewBase>
	{
		/** Nullptr constructor. */
		ListView() : Reference() {}
		/** Constructor. */
		ListView(ListViewTypes type) : Reference(new ListViewBase(type)) {}
	};
}
