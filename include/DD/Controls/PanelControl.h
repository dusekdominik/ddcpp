#pragma once
namespace DD::Controls {
	/**
	 *
	 */
	enum struct PanelControlTypes {
		/** Panel with no specific function. */
		Basic,
		/** Panel ignoring vertical position of items and stacking them into a column. */
		VerticalStack,
		/** Panel ignoring horizontal position of items and stacking them into a row. */
		HorizontalStack
	};

	/**
	 * Simple panel control.
	 * Providing basic functions as scrolling.
	 *
	 * @example simple usage
	 *  static Window window = Window(Window::Types::Basic, "MyWindow");
	 *  static PanelControl vStack = PanelControl(PanelControl::Types::VerticalStack);
	 *  static ButtonControl button1 = ButtonControl("myButton1");
	 *  static ButtonControl button2 = ButtonControl("myButton2");
	 *  static ButtonControl button3 = ButtonControl("myButton3");
	 *  vStack->Add(button1);
	 *  vStack->Add(button2);
	 *  vStack->Add(button3);
	 *  window->Add(vStack);
	 *  window->Init();
	 */
	struct PanelControl;
}

#include <DD/Controls/IControl.h>
#include <DD/Delegate.h>
#include <DD/Reference.h>
#include <DD/String.h>

namespace DD::Controls {
	class PanelControlBase
		: public IControl
	{
		friend struct PanelControl;

	protected: // properties
		/** Visibility of scrollbar. */
		Visibility _hbar, _vbar;
		/** Description of workspace rectangle. */
		int _maxX, _maxY, _minX, _minY;
		/** Description of workspace rectangle. */
		Rect _workspace;

	protected: // IControl
		virtual void iControlInit() override;
		virtual void iControlRect(const Rect &) override;

	public: //
		/** Scroll window by difference form actual offsets. */
		void updateOffset(int dx, int dy);
		/** Compute workspace rectangle from children rectangles.  */
		void recomputeWorkspace();
		/** Compute and set params for scrolling. */
		void recomputeScroll();

	public:
		/** Get scrollbar visibility. */
		Visibility HorizontalScrollBar() const { return _hbar; }
		/** Set scrollbar visibility; */
		void HorizontalScrollBar(Visibility visibility);
		/** Get scrollbar visibility. */
		Visibility VerticalScrollBar() const { return _vbar; }
		/** Set scrollbar visibility; */
		void VerticalScrollBar(Visibility visibility);
		/** Get current scroll offset. */
		int HorizontalOffset() const { return _hOffset; }
		/** Set current scroll offset. */
		void HorizontalOffset(int offset);
		/** Get current scroll offset. */
		int VerticalOffset() const { return _vOffset; }
		/** Set current scroll offset. */
		void VerticalOffset(int offset);
		/** Get current workspace area. */
		const Rect & Workspace() const { return _workspace; }

	protected: // constructors
		/** Constructor. */
		PanelControlBase(PanelControlTypes);
	};

	struct PanelControl
		: public Reference<PanelControlBase>
	{
		typedef PanelControlTypes Types;

		/** Nullptr constructor. */
		PanelControl() : Reference() {}
		/** Constructor. */
		PanelControl(Types types) : Reference(new PanelControlBase(types)) {}
	};
}
