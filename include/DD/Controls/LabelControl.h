#pragma once
namespace DD::Controls {
	/**
	 * Text label control.
	 *
	 * @example simple usage
	 *  static ::DD::Window window = ::DD::Window(::DD::Window::Types::Basic, "MyWindow");
	 *  static ::DD::LabelControl label = ::DD::LabelControl("Hello world");
	 *  label->HorizontalAlignment(::DD::IControl::HAlign::Stretch);
	 *  label->VerticalAlignment(::DD::IControl::VAlign::Stretch);
	 *  window->Add(label);
	 *  window->Init();
	 */
	struct LabelControl;
}

#include <DD/Controls/IControl.h>
#include <DD/Reference.h>
#include <DD/String.h>

namespace DD::Controls {
	class LabelControlBase
		: public IControl
	{
		friend struct LabelControl;

	protected: // properties
		/** Text of the label. */
		StringLocal<256> _text;
    /** Frame between text and control edge. */
		IControl::Padding _padding;
		/** Text alignment. */
		TAlign _talign = TAlign::Left;

	public: // events
		/** Text changing event. */
		Event<> OnTextChange;

	protected: // IControl
		virtual void iControlInit() override;
		virtual void iControlRect(const Rect &) override;

	protected: // methods
		/** Upload from local storage onto the label control. */
		void uploadText();

	public: // methods
		/** Getter of the text of the label. */
		const String & Text() { return _text; }
		/** Setter of the text of the label. */
		void Text(const String & text) { _text = text; uploadText(); OnTextChange(); }
		/** Getter of padding. */
		const IControl::Padding & Padding() const { return _padding; }
		/** Setter of padding. */
		void Padding(const IControl::Padding & padding) { _padding = padding; OnSizeChanged(); }
		/** Alignment of text getter. */
		TAlign TextAlignment() const { return _talign; }
		/** Alignment of text setter. */
		void TextAlignment(TAlign align);

	protected: // constructors
		/** Constructor. */
		LabelControlBase(const Text::StringView16 & text);

	public: // constructors
		/** Destructor. */
		~LabelControlBase();
	};


	struct LabelControl
		: public Reference<LabelControlBase>
	{
		/** Nullptr constructor. */
		LabelControl() : Reference() {}
		/** Constructor. */
		LabelControl(Text::StringView16 text) : Reference(new LabelControlBase(text)) {}
	};
}
