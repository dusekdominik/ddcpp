#pragma once
namespace DD::Controls {
	/**
	 * Console for having multiple instances of console.
	 * Right now there is no implementation of reading from control.
	 * Implementation is referential.
	 * @example
	 *   static Console log1("MyFirstLog");
	 *   static Console log1("MySecondLog");
	 *   log1->WriteLine("Hello world");
	 *   Log2->WriteLine("Hello World {0}", 2.0);
	 */
	struct Console;
}

#include <DD/Concurrency/CriticalSection.h>
#include <DD/Controls/ListView.h>
#include <DD/Controls/Window.h>
#include <DD/Reference.h>
#include <DD/Stopwatch.h>
#include <DD/String.h>

namespace DD::Controls {
	class ConsoleBase
		: public IReferential
	{
		friend struct Console;

	private: // properties
		/** Lock for MT access to console. */
		Concurrency::CriticalSection _section;
		/** Main control for console.  */
		ListView _consoleTable;
		/** Timestamps since initialization of constructor. */
		Stopwatch _watch;
		/** Window of console. */
		Window _consoleWindow;
		/** Counter for  logged messages. */
		size_t _counter;

	public: // methods
		/** Write line. */
		void WriteLine(const String & str);
		/** Write line from given arguments.*/
		template<typename...ARGS>
		void WriteLine(const ARGS &...args) { WriteLine((const String &)StringLocal<256>::From(args...)); }
		/** Write line from given format and arguments. */
		template<typename...ARGS>
		void WriteLineF(const char * format, const ARGS &...args) { WriteLine((const String &)StringLocal<256>::Format(format, args...)); }

	private: // constructors
		/** Constructor. */
		ConsoleBase(const String & name, i32 preallocation);
	};

	struct Console
		: public Reference<ConsoleBase>
	{
		/** Nullptr constructor. */
		Console() : Reference() {}
		/** Console constructor. */
		Console(const String & name, i32 alloc = 10000) : Reference(new ConsoleBase(name, alloc)) {}
	};
}
