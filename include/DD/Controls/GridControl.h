#pragma once
namespace DD::Controls {
	/**
	 * Types of grid.
	 */
	enum struct GridControlTypes {
		/** Standard grid, cols adnd rows must be set before children are add. */
		Basic,
		/** Generic grid - if child is added into not defined col/row, cols/rows are genrical filled.  */
		Generic,
	};

	/**
	 * Grid control.
	 * Panel with table alignment function.
	 *
	 * @warning colspan and rowspan are not supported yet.
	 * @example simple usage
	 *  static Window window = Window(Window::Types::Generic, "MyWindow");
	 *  static GridControl grid = GridControl(GridControl::Types::Basic);
	 *  grid->HorizontalAlignment(IControl::HAlign::Center);
	 *  grid->VerticalAlignment(IControl::VAlign::Center);
	 *  grid->Width(400);
	 *  grid->Height(400);
	 *  grid->SetDefaultCol({ IControl::SizeModes::Pixels, 40.f });
	 *  grid->SetDefaultRow({ IControl::SizeModes::Pixels, 40.f });
	 *  for (int i = 0; i < 10; ++i) for (int j = 0; j < 10; ++j) {
	 *    ButtonControl b = ButtonControl(String::From(i, "-", j));
	 *    b->HorizontalAlignment(IControl::HAlign::Stretch);
	 *    b->VerticalAlignment(IControl::VAlign::Stretch);
	 *    b->Grid({ i, 1, j, 1 });
	 *    grid->Add(b);
	 *  }
	 *  window->Add(grid);
	 *  window->Init();
	 */
	struct  GridControl;
}

#include <DD/Controls/IControl.h>
#include <DD/Controls/PanelControl.h>
#include <DD/List.h>
#include <DD/Reference.h>

namespace DD::Controls {
	class GridControlBase
		: public PanelControlBase
	{
		friend struct GridControl;

	private: // nested
		/** Descriptor of column. */
		struct ColDescriptor { List<IControl*, 16> Children; Size Width; float Left, ActualWidth; };
		/** Descriptor of Row. */
		struct RowDescriptor { List<IControl*, 16> Children; Size Height; float Top, ActualHeight; };

	public: // statics
		/** Constant for adaptive size. */
		static constexpr Size ADAPTIVE = { SizeModes::Adaptive, 0.f };

	protected: // properties
		/** Size of default row. */
		Size _defaultRow;
		/** Size of default col. */
		Size _defaultCol;
		/** Collection of cols. */
		List<ColDescriptor, 16> _cols;
		/** Collection of rows. */
		List<RowDescriptor, 16> _rows;

	protected: // methods
		/** Compute position and sizes of the children. */
		void recomputeGrid();
		void recomputeChild(IControl * ctrl);

	protected: // IControl
		virtual void iControlInit() override;

	public: // methods
		/** Set default sized row. */
		void AddRow() { AddRow(_defaultRow); }
		/** Add custom sized row. */
		void AddRow(Size size);
		/** Set size of default row. */
		void SetDefaultRow(Size size) { _defaultRow = size; }
		/** Add column with default size. */
		void AddCol() { AddCol(_defaultCol); }
		/** Add custom sized column. */
		void AddCol(Size size);
		/** Set size of default column. */
		void SetDefaultCol(Size size) { _defaultCol = size; }

	protected:
		/** Constructor. */
		GridControlBase(GridControlTypes);
	};


	struct GridControl
		: public Reference<GridControlBase>
	{
		typedef GridControlTypes Types;

		/** Nullptr constructor. */
		GridControl() : Reference() {}
		/** Standard constructor. */
		GridControl(Types type) : Reference(new GridControlBase(type)) {}
	};
}
