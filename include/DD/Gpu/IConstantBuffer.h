#pragma once
namespace DD::Gpu {
	/**
	 * Interface for constant buffers.
	 */
	class IConstantBuffer;
}

#include <DD/Gpu/IBuffer.h>
#include <DD/Types.h>

namespace DD::Gpu {
	class IConstantBuffer
		: public IBuffer
	{
		friend class Devices;

	public: // methods
		/** Bind to vertex/index shader. */
		void Bind(u32 slot);
		/** Bind to compute shader. */
		void Bind2CS(u32 slot /*= 0*/);
		/** Bind to pixel shader. */
		void Bind2PS(u32 slot /*= 0*/);
		/** Bind to vertex shader. */
		void Bind2VS(u32 slot /*= 0*/);
		/** Initialize. */
		void Init(Device & device, u32 bytes);
		/** Copy data into VRAM. */
		void Update(const void * source);
		/** Release. */
		void Release();

	public: // constructors
		/** Destructor. */
		~IConstantBuffer() { Release(); }
	};
}
