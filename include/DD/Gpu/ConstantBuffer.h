#pragma once
namespace DD::Gpu {
	/**
	 * ConstantBuffer is extension of IConstantBuffer, which supports simple mirroring between RAM and VRAM states.
	 *
	 * Example:
	 *  // declare name of cb and its slot (ideally in special header file, which can be included even into shaders and into c++ code)
	 *  CONSTANT_BUFFER(SampleBuffer, 1) {
	 *    float4 SampleOffset;
   *  };
	 *
	 *  // declare cb interface
	 *  ::DD::ConstantBuffer<SampleBuffer> sampleBuffer;
	 *  sampleBuffer.Init(device);
	 *  sampleBuffer->SampleOffset = float4(1.f, 2.f, 3.f, 4.f);
	 *  sampleBuffer.Update(device);
	 */
	template<typename DATA>
	class ConstantBuffer;
}

#include <DD/Gpu/IConstantBuffer.h>

namespace DD::Gpu {
	template<typename DATA>
	class ConstantBuffer
		: public IConstantBuffer
	{
		/** Safety check - constant buffer must be 128-bit-aligned.*/
		static_assert(sizeof(DATA) % 16 == 0, "Invalid size of constant buffer.");

	private: // properties
		/** Memory declaration of constant buffer. */
		DATA _data;

	public: // accession operators
		/** Accession to the constant buffer declaration. */
		DATA * operator->() { return &_data; }
		/** Accession to the constant buffer declaration. */
		const DATA * const operator->() const { return &_data; }
		/** Accession to the constant buffer declaration. */
		DATA & operator*() { return _data; }
		/** Accession to the constant buffer declaration. */
		const DATA & operator*() const { return _data; }

	public: // methods
		/** Bind constant buffer to the current shaders PS/VS/CS. */
		void Bind() { IConstantBuffer::Bind(DATA::SLOT); }
		/** Bind constant buffer to the current compute shader, slot is automatically deduced from constant buffer declaration (template argument). */
		void Bind2CS() { IConstantBuffer::Bind2CS(DATA::SLOT); }
		/** Bind constant buffer to the current pixel shader, slot is automatically deduced from constant buffer declaration (template argument). */
		void Bind2PS() { IConstantBuffer::Bind2PS(DATA::SLOT); }
		/** Bind constant buffer to the current vertex shader, slot is automatically deduced from constant buffer declaration (template argument). */
		void Bind2VS() { IConstantBuffer::Bind2VS(DATA::SLOT); }
		/** Updates actual RAM state of constant buffer to the VRAM. */
		void Update() { IConstantBuffer::Update(&_data); }
		/** Initialize constant buffer, size of VRAM allocation is automatically deduced from constant buffer declaration (template argument). */
		void Init(Device & device) { IConstantBuffer::Init(device, sizeof(DATA)); }

	public: // constructors
		/** Assignment operator - it is possible to copy CS data. */
		ConstantBuffer & operator=(const DATA & data) { _data = data; }
		/** Assignment operator - deleted - copying references to VRAM is dangerous. */
		ConstantBuffer & operator=(const ConstantBuffer & data) = delete;
		/** Basic constructor. */
		ConstantBuffer(const DATA & data) : IConstantBuffer(), _data(data) {}
		/** Zero constructor. */
		ConstantBuffer() : IConstantBuffer(), _data() {}
	};
}