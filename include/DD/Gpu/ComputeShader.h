#pragma once
namespace DD::Gpu {
	/** Compute shader. */
	class ComputeShader;
}

#include <DD/Gpu/Device.h>
#include <DD/Memory/DeferredImpl.h>
#include <DD/Memory/NonCopyable.h>
#include <DD/String.h>
#include <DD/Text/StringView.h>

namespace DD::Gpu {
	class ComputeShader
		: public Memory::NonCopyable
	{
		friend class Devices;

	public: // nested
		/** Internal properties. */
		struct DXModel;

	private: // properties
		/** Owner. */
		Device _device;
		/** Shader name. */
		String _name;
		/** Internal data. */
		Memory::DeferredImpl<DXModel, 8> _model;

	public: // methods
		/** Load Compute shader from *.cso file. */
		void Load(Device & device, const Text::StringView16 & name);
		/** Reload Compute shader. */
		void Reload(Device & device);
		/** Set Compute shader. */
		void Run(u32 x, u32 y, u32 z);

	public: // constructor
			/** Zero constructor. */
		ComputeShader();
		/** Constructor. */
		ComputeShader(Device & device, const Text::StringView16 & name) : ComputeShader() { Load(device, name); }
		/** Destructor. */
		~ComputeShader();
	};
}
