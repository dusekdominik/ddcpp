#pragma once
namespace DD::Gpu {
	/**
	 * Vertex shader abstraction.
	 * Use load method for load shader from *.cso file.
	 */
	class VertexShader;
}

#include <DD/Array.h>
#include <DD/Collections/Bin.h>
#include <DD/Gpu/Device.h>
#include <DD/Gpu/ShaderInput.h>
#include <DD/Memory/DeferredImpl.h>
#include <DD/Memory/NonCopyable.h>
#include <DD/String.h>
#include <DD/Text/StringView.h>

namespace DD::Gpu {
	class VertexShader
		: public Memory::NonCopyable
	{
		friend class Devices;

	public: // nested
		/** Internal representation. */
		struct DXModel;

	protected: // properties
		/** Reference to parent device (shader owner). */
		Device _device;
		/** Set of input parameters of shader function. */
		Array<ShaderInput> _layoutDefinition;
		/** Shader name. */
		String _name;
		/** Internal representation. */
		Memory::DeferredImpl<DXModel, 16> _model;

	public: // methods
		/** Set vertex shader and its layout. */
		void Set();
		/** Load shader from *.cso file. */
		void Load(Device & device, const Text::StringView16 & name, const void * layout, u32 size);
		/** Reload shader from *.cso file. */
		void Reload(Device & device) { Load(device, _name, _layoutDefinition.Ptr(), (u32)_layoutDefinition.Length()); }
		/** Load shader from *cso file with specified input layout. */
		template<typename...ARGS>
		void Load(Device & device, const Text::StringView16 & name) {
			_layoutDefinition = Array<ShaderInput>::Concatenate(
				Collections::Binify(Array2Pointer(ARGS::LAYOUT)...),
				Collections::Binify(ARGS::LAYOUT_SIZE...)
			);
			Load(device, name, _layoutDefinition.Ptr(), (u32)_layoutDefinition.Length());
		}

	public: // constructor
		/** Zero constructor. */
		VertexShader();
		/** Destructor. */
		~VertexShader();
	};
}
