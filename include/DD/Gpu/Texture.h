#pragma once
namespace DD::Gpu {
	/**
	 * Texture for rendering.
	 * Actually supported formats are:
	 *   1. DDS
	 */
	class Texture;
}

#include <DD/Gpu/Device.h>
#include <DD/Memory/DeferredImpl.h>
#include <DD/String.h>
#include <DD/Types.h>

namespace DD::Gpu {
	class Texture {
		friend class Devices;

	public: // nested
		/** Texture modes. */
		enum Modes { R8G8B8A8 = 28, R8 = 61 };
		/** Internal data view */
		struct DXModel;

	private: // properties
		/** Device handle. */
		Device _device;
		/** Source file name of texture. */
		String _filename;
		/** Handles of device, semantic is filled in cpp file. */
		Memory::DeferredImpl<DXModel, 32> _model;
		/** Color mode. */
		Modes _mode;

	public: // methods
		/** Bind to render target. */
		void Bind2RT();
		/** Bind to compute shader. */
		void Bind2CS();
		/** Bind to pixel shader. */
		void Bind2PS(u32 slot = 0);
		/** Bind to vertex shader. */
		void Bind2VS();
		/** Fill the whole texture by the given color. */
		void ClearRT(const float * color);
		/** Initialize texture. */
		void Init(Device & device);
		/** Was the texture initialized by a device. */
		bool IsInitialized() { return _device; }

	private: // constructors
		/** Load texture from a file.*/
		Texture(const String & filename);

	public: // constructors
		/** */
		Texture();
		/** RTV. */
		Texture(Modes);
		/** Destructor. */
		~Texture();
	};
}
