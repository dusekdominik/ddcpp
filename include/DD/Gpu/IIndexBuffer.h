#pragma once
namespace DD::Gpu {
	/** Enumerated types of indices. */
	enum IIndexBufferIndices : char { _8Bit = 62, _16Bit = 57, _32Bit = 42 };

	/** Class handling operations for index-buffers. */
	class IIndexBuffer;
}

#include <DD/Gpu/IBuffer.h>
#include <DD/Types.h>

namespace DD::Gpu {
	class IIndexBuffer
		: public IBuffer
	{
		friend class Devices;

	public: // methods
		/** Init index buffer. */
		void Init(Device & device, u32 bytes, const void * source);
		/** Bind index buffer. */
		void Bind(IIndexBufferIndices indexType, u32 offset);
		/** Release. */
		void Release();

	public: // constructors
		/** Destructor. */
		~IIndexBuffer() { Release(); }
	};
}
