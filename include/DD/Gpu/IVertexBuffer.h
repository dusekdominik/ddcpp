#pragma once
namespace DD::Gpu {
	/** Vertex buffer abstraction. */
	class IVertexBuffer;
}

#include <DD/Gpu/IBuffer.h>
#include <DD/Types.h>

namespace DD::Gpu {
	class IVertexBuffer
		: public IBuffer
	{
		friend class Devices;

	public: // methods
		/** Init vertex buffer. */
		void Init(Device & device, u32 bytes, u32 stride, const void * source);
		/** Bind vertex buffer. */
		void Bind(u32 stride, u32 offset);
		/** Release. */
		void Release();

	public: // constructors
		/** Destructor. */
		~IVertexBuffer() { Release(); }
	};
}
