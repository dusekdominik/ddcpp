#pragma once
namespace DD::Gpu {
	/** Main object for handling graphic device. */
	class Devices;
}

#define DD_DEVICE_STATISTIC 1

#include <DD/Boolean.h>
#include <DD/BTree.h>
#include <DD/Concurrency/Awaitable.h>
#include <DD/Concurrency/ThreadSafeObject.h>
#include <DD/Controls/Window.h>
#include <DD/Debug.h>
#include <DD/Enum.h>
#include <DD/FileSystem/Path.h>
#include <DD/Gpu/ConstantBuffer.h>
#include <DD/Gpu/Device.h>
#include <DD/Gpu/IIndexBuffer.h>
#include <DD/Gpu/IStructerdBuffer.h>
#include <DD/Gpu/IVertexBuffer.h>
#include <DD/Gpu/ComputeShader.h>
#include <DD/Gpu/PixelShader.h>
#include <DD/Gpu/Texture.h>
#include <DD/Gpu/VertexShader.h>
#include <DD/Lambda.h>
#include <DD/Memory/DeferredImpl.h>
#include <DD/Queue.h>
#include <DD/Reference.h>
#include <DD/ShaderCompiler.h>
#include <DD/String.h>

namespace DD::Gpu {
	class Devices {
		friend struct Device;

	public: // nested
		/** Rendering topology. */
		enum PrimitiveTopologies { Undefined = 0, PointList = 1, LineList = 2, LineStrip = 3, TriangleList = 4, TriangleStrip = 5 };
		/** Interface for async commanding device. Commands are proceed while Present method. */
		class AsyncInterface;
		/** Render target resolution. */
		struct Screen { size_t Width, Height; };
		/** Abstract screenshot. */
		struct IScreenshot;
		/** Screenshot type. */
		using Screenshot = Reference<IScreenshot>;
		/** Locally allocated command. */
		using Command = Action32<Device &>;
#if DD_DEVICE_STATISTIC
		struct StatisticResources { u32 IBCount = 0, VBCount = 0, CBCount = 0, SBCount = 0, RWSBCount = 0; };
		struct StatisticPrimitives { i32 TriangleLists = 0, TriangleStrips = 0, LineLists = 0, LineStrips = 0, PointLists = 0; };
		struct StatisticProcessing { i32  DrawCalls = 0, IBInits = 0, VBInits = 0, CBInits = 0, SBInits = 0, RWSBInits = 0; };
		struct StatisticMemory { i32 Bytes = 0, IBBytes = 0, VBBytes = 0, CBBytes = 0, SBBytes = 0, RWSBBytes = 0; };
		struct Statistics : public StatisticPrimitives, public StatisticProcessing, public StatisticMemory, public StatisticResources {};
		struct StatisticsExt : public Statistics { i32 *Primitive; };
#endif
		/** Description of device. */
		struct Description {
			/** Bytes. */
			size_t DedicatedVideoMemory;
			/** Bytes. */
			size_t DedicatedSystemMemory;
			/** Bytes. */
			size_t SharedSystemMemory;
			/** Name of the device. */
			StringLocal<128> Name;
		};
		/** Parameters for initialization. */
		struct InitParams {
			/** Window where render target will be placed. */
			DD::Controls::Window Window;
			/**
			 * Device selection callback.
			 * If callback is not set the first device will be used.
			 * @param Array<String> array with device descriptions.
			 * @return selected index of the device.
			 */
			DD::ILambda<u32(const ArrayView1D<Description> &)> * SelectDevice = 0;
		};
		/** Multi-sampling options. */
		DD_ENUM_32(
			(MSAA, "Multi-sampling settings."),
			(MSAA_1x, 1), (MSAA_2x, 2), (MSAA_4x, 4), (MSAA_8x, 8)
		);
		/**Internal data model. */
		struct DXDevice;

	protected: // properties
		/** Multi sampling options. */
		MSAA _msaa;
		/** Shader creation primitive. */
		Concurrency::CriticalSection _shaderLock;
		/** Cast handle. */
		Devices * _this;
		/** Number of references. */
		Concurrency::Atomic::i32 _references;
		/** Native handles of device. */
		mutable Memory::DeferredImpl<DXDevice, 512> _dxmodel;
		/** Render target window. */
		DD::Controls::Window _window;
		/** Is VSync enabled. */
		Boolean _alpha, _depth, _vsync, _wire;
		/** Name of used video card. */
		StringLocal<128> _videoCard;
		/** Table of all loaded compute shaders. */
		BTree<String, Reference<Gpu::ComputeShader>> _computeShaders;
		/** Table of all loaded pixel shaders. */
		BTree<String, Reference<Gpu::PixelShader>> _pixelShaders;
		/** Table of all loaded vertex shaders. */
		BTree<String, Reference<Gpu::VertexShader>> _vertexShaders;
		/** Table of all loaded textures. */
		BTree<String, Reference<Gpu::Texture>> _textures;
		/** Table of all registered commands in one frame. */
		Concurrency::ThreadSafeObject<Queue<Command, 64>> _cmds;
		/** Actual primitive topology. */
		PrimitiveTopologies _topology;
#if DD_DEVICE_STATISTIC
		/** Statistics of used resources. */
		StatisticsExt _statistics;
#endif

	protected: // methods
		/** Initialize device. */
		Devices & init(InitParams &);
		/** Releasing helper. */
		i32 releaseBuffer(Gpu::IBuffer & buffer);

	public: // constant buffer
		/** Init constant buffer. */
		void ConstantBufferInit(Gpu::IConstantBuffer & cb, u32 bytes);
		/** Bind constant buffer to compute shader. */
		void ConstantBufferBind2CS(Gpu::IConstantBuffer & cb, u32 slot);
		/** Bind constant buffer to vertex shader. */
		void ConstantBufferBind2VS(Gpu::IConstantBuffer & cb, u32 slot);
		/** Bind constant buffer to shader shader. */
		void ConstantBufferBind2PS(Gpu::IConstantBuffer & cb, u32 slot);
		/** Update constant buffer - fill GPU data. */
		void ConstantBufferUpdate(Gpu::IConstantBuffer & cb, const void * source);
		/** Release buffer. */
		void ConstantBufferRelease(Gpu::IConstantBuffer & cb);

	public: // index buffers
		/** Init index buffer. */
		void IndexBufferInit(Gpu::IIndexBuffer & ib, u32 bytes, const void * source);
		/** Bind index buffer. */
		void IndexBufferBind(Gpu::IIndexBuffer & ib, Gpu::IIndexBufferIndices indexType, u32 offset);
		/** Release buffer. */
		void IndexBufferRelease(Gpu::IIndexBuffer & ib);

	public: // vertex buffers
		/** Init vertex buffer. */
		void VertexBufferInit(Gpu::IVertexBuffer & vb, u32 bytes, u32 stride, const void * source);
		/** Bind vertex buffer. */
		void VertexBufferBind(Gpu::IVertexBuffer & vb, u32 stride, u32 offset);
		/** Bind two vertex buffers (for instanced rendering). */
		void VertexBufferBind(Gpu::IVertexBuffer & vb1, u32 stride1, u32 offset1, Gpu::IVertexBuffer & vb2, u32 stride2, u32 offset2);
		/** Release buffer. */
		void VertexBufferRelease(Gpu::IVertexBuffer & vb);

	public: // structured buffers
		/** Init read-only structured buffer. */
		void StructuredBufferInit(Gpu::IStructuredBuffer & sb, u32 bytes, u32 stride, const void * source);
		/** Bind read-only structured buffer to vertex shader. */
		void StructuredBufferBind2VS(Gpu::IStructuredBuffer & sb, u32 slot);
		/** Bind read-only structured buffer to compute shader. */
		void StructuredBufferBind2CS(Gpu::IStructuredBuffer & sb, u32 slot);
		/** Release buffer. */
		void StructuredBufferRelease(Gpu::IStructuredBuffer & sb);

	public: // RW buffers
		/** Init read-write structured buffer. */
		void RWStructuredBufferInit(Gpu::IRWStructuredBuffer & sb, u32 bytes, u32 stride, const void * source);
		/** Bind read-write structured buffer to compute shader. */
		void RWStructuredBufferBind2CS(Gpu::IRWStructuredBuffer & sb, u32 slot);
		/** Copy read-write structured buffer from VRAM to RAM. */
		void RWStructuredBufferRead(Gpu::IRWStructuredBuffer & sb, void * data, u32 size);
		/** Release buffer. */
		void RWStructuredBufferRelease(Gpu::IRWStructuredBuffer & sb);

	public: // textures
		/** Initialize texture from a file. Actually supports only *.dds format. */
		void TextureInit(Gpu::Texture & t);
		/** Bind texture to compute shader. */
		void TextureBind2CS(Gpu::Texture & t);
		/** Bind texture to pixel shader. */
		void TextureBind2PS(Gpu::Texture & t, u32 slot);
		/** Bind texture to compute shader. */
		void TextureBind2VS(Gpu::Texture & t);
		/** Bind texture to render target. */
		void TextureBind2RT(Gpu::Texture & t);
		/** */
		void TextureClearRT(Gpu::Texture & t, const float * color);

	public: // compute shaders
		/** Load compute shader from *.cso file, fill name without the extension. */
		void ComputeShaderLoad(Gpu::ComputeShader & cs, const Text::StringView16 & name);
		/** Set compute shader as actual on device. */
		void ComputeShaderSet(Gpu::ComputeShader & cs);
		/** Run compute shader. */
		void ComputeShaderDispatch(u32 x, u32 y, u32 z);

	public: // pixel shaders
		/** Load pixel shader from *.cso file, fill name without the extension. */
		void PixelShaderLoad(Gpu::PixelShader & ps, const Text::StringView16 & name);
		/** Set pixel shader as actual on device. */
		void PixelShaderSet(Gpu::PixelShader & ps);

	public: // vertex shaders
		/** Load vertex shader from *.cso file, fill name without the extension. */
		void VertexShaderLoad(Gpu::VertexShader & vs, const Text::StringView16 & name, const void * layout, u32 layoutSize);
		/** Set vertex shader as actual on device. */
		void VertexShaderSet(Gpu::VertexShader & vs);

	public: // device operations
		/** Clear render target. */
		Devices & Clear(const float * color);
		/** Clear depth. */
		Devices & ClearDepth(const float color = 1);
		/** Get depth buffer in texture format. */
		Reference<Gpu::Texture> GetDepthTexture();
		/** Main RTV in texture format. */
		Gpu::Texture & GetMainRenderTarget();
		/** Render actual immediate context based on vertex buffers.  */
		void Draw(u32 vertexCount);
		/** Render actual immediate context based on index-vertex buffers.  */
		void DrawIndexed(u32 indexCount);
		/** Render actual immediate context based on index-vertex-vertex buffers. */
		void DrawIndexedInstanced(u32 indexCount, u32 instanceCount);
		/** Device flush command - use only if you are using compute shader. */
		void Flush();
		/** Show result of rendering and proceed commands. */
		void Present();
		/** Set actual topology of renderer. */
		void SetPrimitiveTopology(PrimitiveTopologies topology);

	public: // render options
#if DD_DEVICE_STATISTIC
		/** Statistic of device resources. */
		Statistics Statistic() { return _statistics; }
#endif
		/** Alpha blending setter. */
		void AlphaBlending(Boolean::Value set);
		/** Alpha blending getter. */
		bool AlphaBlending();
		/** Depth buffer setter. */
		void DepthBuffer(Boolean::Value set);
		/** Depth buffer getter. */
		bool DepthBuffer() const;
		/** Multi sampling setter. */
		void MultiSampling(MSAA);
		/** Multi sampling getter. */
		MSAA MultiSampling() const;
		/** VSync setter. */
		void VSync(Boolean::Value set);
		/** VSync getter. */
		bool VSync() const { return _vsync; }
		/** Wireframe setter. */
		void Wireframe(Boolean::Value set);
		/** Wireframe getter. */
		bool Wireframe() const;
		/** Resize target. */
		void Resize(size_t w, size_t h);
		/** Add lambda command of type [](Device&){}. */
		template<typename LAMBDA>
		void AddCommand(const LAMBDA & lambda) { _cmds->PushBack(lambda); }

	public: // async parameters
		/** Capture screenshot. */
		Concurrency::Awaitable<Screenshot> CaptureScreenshotAsync();
    /** Capture screenshot. */
    void CaptureScreenshotAsync(const FileSystem::Path & path);
		/** Is alpha blending enabled. */
		Concurrency::AwaitBool AlphaBlendingAsync();
		/** Is depth buffer enabled. */
		Concurrency::AwaitBool DepthBufferAsync();
		/** Is VSync enabled. */
		Concurrency::AwaitBool VSyncAsync();
		/** Is wireframe-rendering enabled. */
		Concurrency::AwaitBool WireframeAsync();
		/** Resize render target. */
		void ResizeAsync(size_t w, size_t h);
		/** Enable/Disable alpha blending. */
		void AlphaBlendingAsync(Boolean::Value set);
		/** Enable/Disable depth buffer. */
		void DepthBufferAsync(Boolean::Value set);
		/** Enable/Disable VSync. */
		void VSyncAsync(Boolean::Value set);
		/** Enable/Disable wireframe. */
		void WireframeAsync(Boolean::Value set);
		/** Get async interface. Operations will be done when present is called. */
		AsyncInterface Async();

	public: // device storage
		/** Access to native handles. */
		void ** GetHandles() { return (void**)_dxmodel.Get(); }
		/** Create or look up compute shader. */
		template<typename...INPUT>
		Reference<Gpu::VertexShader> GetVertexShader(const Text::StringView16 & name);
		/** Create or look up compute shader. */
		Reference<Gpu::ComputeShader> GetComputeShader(const Text::StringView16 & name);
		/** Create or look up compute shader. */
		Reference<Gpu::PixelShader> GetPixelShader(const Text::StringView16 & name);
		/** Create texture from file. */
		Reference<Gpu::Texture> GetTexture(const Text::StringView16 & file);
		/** Reference to a window. */
		const DD::Controls::Window & GetWindow() const { return _window; }
		/** Get shader compiler based on hits device. */
		ShaderCompiler Compiler(const Text::StringView16 & path);
		/** Saves screenshot in PNG format (suffix .png is added automatically). */
		void CaptureScreenshot(const FileSystem::Path & filename);
		/** Capture screen shot from actual render buffer. */
		Screenshot CaptureScreenshot();
#if _DEBUG
		/** Report live objects. */
		void ReportLiveObjects();
#endif
		void Exit();

	public:
		operator Device & () { return reinterpret_cast<Device &>(_this); }

	public: // constructor
		/** Constructor. */
		Devices(InitParams & params);
		/** Destructor. */
		~Devices();
	};


	struct Devices::IScreenshot
		: public IReferential
	{
		/** Formats for screenshot file. */
		enum Formats { PNG, JPEG, BMP };

		/** Method for saving screenshot into a file. */
		virtual void Save(const FileSystem::Path & filename) = 0;
		/** Get screenshot in binary data, */
		virtual ConstByteView Get() = 0;

		/** Virtual destructor. */
		virtual ~IScreenshot() {}
	};


	class Devices::AsyncInterface {
		Devices & _d;

	public: // methods
		/** Device name. */
		const String & GraphicCardName() const { return _d._videoCard; }
		/** Alpha blending state. */
		Concurrency::AwaitBool  AlphaBlending() { return _d.AlphaBlendingAsync(); }
		/** Enable/Disable Alpha blending. */
		void AlphaBlending(Boolean::Value set) { _d.AlphaBlendingAsync(set); }
		/** Capture screenshot. */
		Concurrency::Awaitable<Screenshot> CaptureScreenshot() { return _d.CaptureScreenshotAsync(); }
    /** Capture screenshot. */
    void CaptureScreenshot(const String & path) { _d.CaptureScreenshotAsync(path); }
		/** Depth buffer state. */
		Concurrency::AwaitBool DepthBuffer() { return  _d.DepthBufferAsync(); }
		/** Enable/Disable Depth buffer. */
		void DepthBuffer(Boolean::Value set) { _d.DepthBufferAsync(set); }
		/** VSync state. */
		Concurrency::AwaitBool VSync() { return _d.VSyncAsync(); }
		/** Enable/Disable VSync. */
		void VSync(Boolean::Value set) { _d.VSyncAsync(set); }
		/** Resize render target. */
		void Resize(size_t w, size_t h) { _d.ResizeAsync(w, h); }
		/** Enable/Disable Wireframe.  */
		Concurrency::AwaitBool Wireframe() { return _d.WireframeAsync(); }
		/** Enable/Disable Wireframe.  */
		void Wireframe(Boolean::Value set) { _d.WireframeAsync(set); }
		/** Add lamda command of type [](Devices&){}. */
		template<typename LAMBDA>
		void AddCommand(const LAMBDA & lambda) { _d.AddCommand(lambda); }

	public: // constructor
		/** Constructor. */
		AsyncInterface(Devices & d) : _d(d) {}
	};
}

namespace DD::Gpu {
	template<typename...T>
	Reference<Gpu::VertexShader> Devices::GetVertexShader(const Text::StringView16 & name) {
		Concurrency::CriticalSection::Scope _scope(_shaderLock);
		Reference<Gpu::VertexShader> & vertexShader = _vertexShaders[name];
		if (vertexShader == nullptr) {
			vertexShader = new Gpu::VertexShader();
			vertexShader->Load<T...>(*this, name);
		}
		return vertexShader;
	}
}
