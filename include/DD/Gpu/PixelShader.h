#pragma once
namespace DD::Gpu {
	/** Holder for native handlers of pixel shader. */
	class PixelShader;
}

#include <DD/Gpu/Device.h>
#include <DD/Memory/DeferredImpl.h>
#include <DD/Memory/NonCopyable.h>
#include <DD/String.h>
#include <DD/Text/StringView.h>

namespace DD::Gpu {
	class PixelShader
		: public Memory::NonCopyable
	{
		friend class Devices;

	public: // nested
		/** Internal properties. */
		struct DXModel;

	private: // properties
		/** Owner. */
		Device _device;
		/** Shader name. */
		String _name;
		/** Internal data. */
		Memory::DeferredImpl<DXModel, 8> _model;

	public: // methods
		/** Load pixel shader from *.cso file. */
		void Load(Device & device, const Text::StringView16 & name);
		/** Reload pixel shader. */
		void Reload(Device & device);
		/** Set pixel shader. */
		void Set();

	public: // constructor
		/** Zero constructor. */
		PixelShader();
		/** Constructor. */
		PixelShader(Device & device, const Text::StringView16 & name) : PixelShader() { Load(device, name); }
		/** Destructor. */
		~PixelShader();
	};
}
