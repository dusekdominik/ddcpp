#pragma once

namespace DD::Gpu {
	/**
	 * Structured buffer
	 * VERTEX - item declaration
	 */
	template<typename VERTEX, size_t PREALLOCATION = 0>
	class StructuredBuffer;
	template<typename VERTEX, size_t PREALLOCATION = 0>
	class RWStructuredBuffer;
}

#include <DD/Gpu/IStructerdBuffer.h>
#include <DD/List.h>

namespace DD::Gpu {
	template<typename VERTEX, size_t PREALLOCATION>
	class StructuredBuffer
		: public List<VERTEX, PREALLOCATION>
		, public IStructuredBuffer
	{
		using ListBase = List<VERTEX, PREALLOCATION>;

	public: // methods
		u32 Bytes() const { return (u32)(ListBase::Size() * sizeof(VERTEX)); }
		/** Init structured buffer on VRAM. */
		void Init(Device & d) { IStructuredBuffer::Init(d, Bytes(), (u32)sizeof(VERTEX), (const void *)ListBase::begin()); }
		/** Release old data and init new on VRAM. */
		void Reinit(Device & device) { Release(); Init(device); }

	public: // constructors
		/** Constructor forwarder, @see DD::List constructors. */
		template<typename...ARGS>
		StructuredBuffer(ARGS...args) : IStructuredBuffer(), ListBase(args...) {}
	};


	template<typename VERTEX, size_t PREALLOCATION>
	class RWStructuredBuffer
		: public List<VERTEX, PREALLOCATION>
		, public IRWStructuredBuffer
	{
		/** Parent class. */
		using ListBase = List<VERTEX, PREALLOCATION>;

	public: // methods
		u32 Bytes() const { return (u32)(ListBase::Size() * sizeof(VERTEX)); }
		/** Init structured buffer on VRAM. */
		void Init(Device & d) { IRWStructuredBuffer::Init(d, Bytes(), (u32)sizeof(VERTEX), (const void *)ListBase::begin()); }
		/** */
		void Read() { IRWStructuredBuffer::Read((void *)ListBase::begin(), Bytes()); }
		/** Release old data and init new on VRAM. */
		void Reinit(Device & device) { Release(); Init(device); }

	public: // constructors
		/** Constructor forwarder, @see DD::List constructors. */
		template<typename...ARGS>
		RWStructuredBuffer(ARGS...args) : IRWStructuredBuffer(), ListBase(args...) {}
	};
}
