#pragma once
namespace DD::Gpu {
	/**
	 * Vertex Buffer
	 * VERTEX - declaration of single vertex (also contains traits for DX init).
	 */
	template<typename VERTEX>
	class VertexBuffer;
}

#include <DD/Gpu/IVertexBuffer.h>
#include <DD/List.h>

namespace DD::Gpu {
	template<typename VERTEX>
	class VertexBuffer
		: public List<VERTEX>
		, public IVertexBuffer
	{
		using BaseList = List<VERTEX>;

	public:
		/** Init data on VRAM. */
		void Init(Device & device) { IVertexBuffer::Init(device, (u32)BaseList::Size() * sizeof(VERTEX), sizeof(VERTEX), BaseList::begin()); }
		/** Standard bind. */
		void Bind() { IVertexBuffer::Bind(VERTEX::SIZE, 0); }
		/** Release actual data and create new. */
		void Reinit(Device & devices) { Release(), Init(devices); }

	public: // constructors
		/** Constructor forwarder. @see List */
		template<typename...ARGS>
		VertexBuffer(ARGS...args) : BaseList(args...) {}
	};
}
