#pragma once
namespace DD::Gpu {
	/** DXGI_FORMAT */
	enum ShaderInputFormat { _float4 = 2, _float3 = 6, _float2 = 16, _float = 41, _uint = 42, _int = 43, _ColorInt = 43 };

	/** Incidence. */
	enum ShaderInputClassification { PerVertex = 0, PerInstance = 1 };

	/**
	 * D3D11_INPUT_ELEMENT_DESC
	 * Wrapped dx structure.
	 */
	struct ShaderInput;
}

#include <DD/Types.h>

namespace DD::Gpu {
	struct ShaderInput {
		/** Typically POSITION, COLOR... https://learn.microsoft.com/en-us/windows/win32/direct3dhlsl/dx-graphics-hlsl-semantics */
		const char * SemanticName;
		/** Slot corresponding with @see SemanticName. */
		u32 SemanticIndex;
		/** Data type specification. */
		ShaderInputFormat Format;
		/** */
		u32 InputSlot;
		/** */
		u32 AlignedByteOffset;
		/** Instanced rendering, per vertex rendering. */
		ShaderInputClassification InputSlotClass;
		/** */
		u32 InstanceDataStepRate;
	};
}
