#pragma once
namespace DD::Gpu {
	/**
	 * Read-Only structured buffer interface.
	 */
	struct IStructuredBuffer;

	/**
	 * Read-Write structured buffer interface.
	 */
	struct IRWStructuredBuffer;
}

#include <DD/Gpu/IBuffer.h>
#include <DD/Memory/DeferredImpl.h>
#include <DD/Types.h>

namespace DD::Gpu {
	struct IStructuredBuffer
		: public IBuffer
	{
		friend class Devices;

		/** Internal properties. */
		struct DXModelEx;

		/** Internal data. */
		Memory::DeferredImpl<DXModelEx, 8> ModelEx;

		/** Init structured buffer. */
		void Init(Device & device, u32 bytes, u32 stride, const void * source);
		/** Bind structured buffer. */
		void Bind2VS(u32 slot = 0);
		/** Bind structured buffer. */
		void Bind2CS(u32 slot = 0);
		/** Release structured buffer. */
		void Release();

		/** Constructor. */
		IStructuredBuffer();
		/** Destructor. */
		~IStructuredBuffer();
	};


	struct IRWStructuredBuffer
		: public IBuffer
	{
		friend class Devices;

		/** Internal properties. */
		struct DXModelEx;

		/** internal data. */
		Memory::DeferredImpl<DXModelEx, 16> ModelEx;

		/** Init structured buffer for writing. */
		void Init(Device & device, u32 bytes, u32 stride, const void * source);
		/** Bind structured buffer. */
		void Bind2CS(u32 slot = 0);
		/** Read data. */
		void Read(void * data, u32 size);
		/** Release structured buffer. */
		void Release();

		/** Constructor. */
		IRWStructuredBuffer();
		/** Destructor. */
		~IRWStructuredBuffer();
	};
}
