#pragma once
namespace DD::Gpu {
	/** Forward reference. */
	class Devices;

	/** Reference (shared ref) to device. */
	struct Device;
}

namespace DD::Gpu {
	struct Device {
		typedef decltype(nullptr) nullptr_t;
	protected: // properties
		Devices * _device;

	public: // methods
		/** Copy assignment. */
		Device & operator=(const Device &);
		/** Move assignment. */
		Device & operator=(Device &&);
		/** Pointer assignment. */
		Device & operator=(Devices *);
		/** Release assignment. */
		Device & operator=(nullptr_t);
		/** Dereference. */
		Devices * operator->() { return _device; }
		/** Const dereference. */
		const Devices * operator->() const { return _device; }
		/** Nullptr check. */
		operator bool() const { return _device != nullptr;  }

	public: // constructor
		/** Zero constructor, initialize device to nullptr. */
		Device() : _device(0) {}
		/** Catch constructor. */
		Device(Devices *);
		/** Copy constructor. */
		Device(const Device &);
		/** Move constructor. */
		Device(Device &&);
		/** Destructor. */
		~Device();
	};
}
