#pragma once
namespace DD::Gpu {
	/**
	 * Object for D2D transition into DirectX 11 back buffer
	 * Warning: you have to have enabled flag D3D11_CREATE_DEVICE_BGRA_SUPPORT
	 * for more information @see https://msdn.microsoft.com/en-us/library/windows/desktop/ff476107(v=vs.85).aspx
	 * @example
	 *   DD::D2D writer(_device);
	 *   writer.Buffer().Append("Hello World");
	 *   writer.Draw();
	 */
	struct D2D;
}

#include <DD/Math.h>
#include <DD/Color.h>
#include <DD/Concurrency/Lock.h>
#include <DD/Concurrency/ThreadSafeObject.h>
#include <DD/Gpu/Devices.h>
#include <DD/Enum.h>
#include <DD/List.h>
#include <DD/Lambda.h>
#include <DD/Math/Interval.h>
#include <DD/Reference.h>
#include <DD/String.h>
#include <DD/Unique.h>

namespace DD::Gpu {
	struct D2D {
		friend class Gpu::Devices;

	private:
		/** Forward declaration of memory model. */
		struct DXMemoryModel;

	public: // nested
		/** Text horizontal alignment. */
		DD_ENUM_32(
			(THAligns, "Horizontal alignment of text in paragraph."),
			(TLeft,    0, "Align to left."),
			(TCenter,  1, "Align to center."),
			(TRight,   2, "Align to right."),
			(TJustify, 3, "Extends each line of text to left right.")
		);
		/** Text vertical alignment. */
		DD_ENUM_32(
			(TVAligns, "Vertical alignment of text in paragraph."),
			(TTop,    0, "Align to top."),
			(TCenter, 1, "Align to center."),
			(TBottom, 2, "Align to bottom.")
		);
		/** Horizontal block alignment. */
		DD_ENUM_32(
			(HAligns, "Horizontal alignment of block."),
			(HLeft,   0, "Align to left."),
			(HCenter, 1, "Align to center."),
			(HRight,  2, "Align to right.")
		);
		/** Vertical block alignment */
		DD_ENUM_32(
			(VAligns, "Vertical alignment of block."),
			(VBottom, 0, "Align to bottom."),
			(VCenter, 1, "Align to center."),
			(VTop,    2, "Align to top.")
		);
		/** Screen space mode. */
		DD_ENUM_32(
			(SSModes, "Mode of screen space coordinate computation."),
			(Pixel,    0, "Compute coordinates in pixels."),
			(Interval, 1, "Compute coordintes in interval [0.0...1.0]."),
			(Density,  2, "Compute coordintes in points accoding to dpi.")
		);
		/** Font settings. */
		DD_ENUM_32(
			(FWeights, "Weight of font."),
			(Light,  300, "Light weight (300)."),
			(Normal, 400, "Standard weight (400)."),
			(Bold,   700, "Bold weight (700).")
		);
		/** Font settings. Set via property Style. */
		DD_ENUM_32(
			(FStyles, "Style of font."),
			(Normal,  0, "Normal style."),
			(Oblique, 1, "Oblique style (not-shirinked italic)."),
			(Italic,  2, "Italic style.")
		);
		/** Font settings. Set via property Stretch. */
		DD_ENUM_32(
			(FStretches, "Stretch of letters."),
			(Condensed, 3, "Smaller stretch."),
			(Normal, 5, "Normal stretch."),
			(Expanded, 7, "Bigger stretch.")
		);
		/** Font underline settings. */
		DD_ENUM_32(
			(FUnderlines, "Stretch of letters."),
			(No, 0, "Not underlined."),
			(Yes, 1, "Underlined.")
		);

		/** Descriptor of fonts. */
		struct FontsDesc;
		/** referential fonts fonts. */
		struct Fonts;
		/** Screen space aa-box description. */
		struct Boxes;
		/** Size descriptor. */
		struct Sizes;
		/** Frame descriptor. */
		struct Frames;
		/** Rectangle (fill/frame) descriptor. */
		struct Rectangles;
		/** Line descriptor. */
		struct Lines;
		/** Line descriptor. */
		struct Polylines;
		/** Descriptor of statistics. */
		struct Statistics;
		/** Descriptor of formatted string (fonts). */
		struct Strings;
		/** Descriptor of text to drawing. */
		struct Labels;
		/** Locally allocated @see D2D::Strings. */
		template<size_t ALLOCATION>
		struct StringsLocal;
		/** Interface for drawing into a relative area. */
		struct Area;

	private: // properties
		/** Dots per inch of screen. */
		uint2 _dpi = 100;
		/** Rendering device. */
		Gpu::Device _device;
		/** Default font. */
		const Fonts & _defaultFont;
		/** Command buffer for asynchronous interface. */
		Concurrency::ThreadSafeObject<List<Action64<D2D&>, 32>> _cmds;
		/** Multi thread synchronization. */
		Concurrency::Lock _lock;
		/** Memory model containing direct draw specific objects. */
		Unique<DXMemoryModel> _dx;

	public: // methods
		/** Process render commands. */
		void Draw();
		/** Prepare command for drawing a rectangle. */
		void DrawRectangle(const Rectangles & rectangle);
		/** Prepare command for drawing an ellipse. */
		void DrawEllipse(const Rectangles & box);
		/** Prepare command for drawing a line. */
		void DrawLine(const Lines & line);
		/** Prepare command for drawing a polyline. */
		void DrawPolyline(const Polylines & line);
		/** Prepare command for drawing a chart. */
		void DrawStatistic(const Statistics & statistic);
		/** Render buffer into the texture. */
		void DrawLabel(const Labels & text);
		/** Get relative to draw in. */
		Area Relative(const Boxes & box);

	public: // constructor
		/** Constructor. */
		D2D(Gpu::Device & device);
		/** Destructor. */
		~D2D();
	};


	struct D2D::Sizes {
		/** Value of the size. */
		f32 Value = 1.f;
		/** The way how size will be scaled. */
		SSModes Mode = SSModes::Pixel;
	};


	struct D2D::FontsDesc {
		/** Default locale. */
		static constexpr Text::StringView16 DefaultLocale = L"en-US";

		/** Locale description https://www.rfc-editor.org/info/rfc5646. */
		DD::StringLocal<32> Locale;
		/** Name of font family. */
		DD::StringLocal<32> Name;
		/** Weight of font, e.g. bold. */
		FWeights Weight;
		/** Style of font, e.g. italics. */
		FStyles Style;
		/** Stretch of font. */
		FStretches Stretch;
		/** Is font underlined. */
		FUnderlines Underline = FUnderlines::No;
		/** Size of font. */
		Sizes Size;
		/** Solid color of the font. */
		Color Brush;
	};


	struct D2D::Fonts
		: public Reference<D2D::FontsDesc>
	{
		/** Predefined fonts. */
		static D2D::Fonts COURIER_18_YELLOW_BOLD,
			COURIER_12_BLACK, COURIER_12_BLUE, COURIER_12_GREEN, COURIER_12_RED, COURIER_12_WHITE, COURIER_12_YELLOW,
			COURIER_18_BLACK, COURIER_18_BLUE, COURIER_18_GREEN, COURIER_18_RED, COURIER_18_WHITE, COURIER_18_YELLOW,
			COURIER_24_BLACK, COURIER_24_BLUE, COURIER_24_GREEN, COURIER_24_RED, COURIER_24_WHITE, COURIER_24_YELLOW,
			COURIER_28_BLACK, COURIER_28_BLUE, COURIER_28_GREEN, COURIER_28_RED, COURIER_28_WHITE, COURIER_28_YELLOW,
			COURIER_32_BLACK, COURIER_32_BLUE, COURIER_32_GREEN, COURIER_32_RED, COURIER_32_WHITE, COURIER_32_YELLOW,
			DEFAULT_BLACK, DEFAULT_BLACK_GREAT, DEFAULT_BLACK_GREATER, DEFAULT_BLACK_SMALL, DEFAULT_BLACK_SMALLER,
			DEFAULT_BLUE, DEFAULT_BLUE_GREAT, DEFAULT_BLUE_GREATER, DEFAULT_BLUE_SMALL, DEFAULT_BLUE_SMALLER,
			DEFAULT_GREEN, DEFAULT_GREEN_GREAT, DEFAULT_GREEN_GREATER, DEFAULT_GREEN_SMALL, DEFAULT_GREEN_SMALLER,
			DEFAULT_RED, DEFAULT_RED_GREAT, DEFAULT_RED_GREATER, DEFAULT_RED_SMALL, DEFAULT_RED_SMALLER,
			DEFAULT_WHITE, DEFAULT_WHITE_GREAT, DEFAULT_WHITE_GREATER, DEFAULT_WHITE_SMALL, DEFAULT_WHITE_SMALLER,
			DEFAULT_YELLOW, DEFAULT_YELLOW_GREAT, DEFAULT_YELLOW_GREATER, DEFAULT_YELLOW_SMALL, DEFAULT_YELLOW_SMALLER,
			DEFAULT, DEFAULT_GREAT, DEFAULT_GREATER, DEFAULT_SMALL, DEFAULT_SMALLER;
		/** Nullptr constructor. */
		Fonts() : Reference() {}
		/** Basic constructor. @see Collection before creating your own fonts. */
		Fonts(const DD::Text::StringView16 & name, f32 size, Color brush, FWeights weight, FStyles style, FStretches stretch)
			: Reference(new FontsDesc{ FontsDesc::DefaultLocale, name, weight, style, stretch, FUnderlines::No, { size, SSModes::Pixel }, brush })
		{}
	};


	struct D2D::Boxes {
		/** Offsets of box. */
		float2 Origins = 0.f;
		/** Size of box. */
		float2 Dimensions = 1.f;
		/** Horizontal alignment of box.  */
		HAligns HAlign = HAligns::HLeft;
		/** Vertical alignment of box.  */
		VAligns VAlign = VAligns::VTop;
		/** The way how scale of Origins and Dimensions will be interpreted. */
		SSModes Mode = SSModes::Pixel;
	};


	struct D2D::Frames {
		/** Size of the frame. */
		Sizes Size = { 0.f, SSModes::Pixel };
		/** Solid color of frame. */
		Color Brush = 0;
	};


	struct D2D::Rectangles {
		/** */
		Boxes Box;
		/** */
		Color Background = 0;
		/** */
		Frames Frame;
	};


	struct D2D::Lines {
		/** Beginning and ending point of the line. */
		float2 A, B;
		/** Color of the line. */
		Color Brush;
		/** Size of the line width. */
		Sizes Size;
		/** Horizontal alignment of box.  */
		HAligns HAlign = HAligns::HLeft;
		/** Vertical alignment of box.  */
		VAligns VAlign = VAligns::VTop;
	};


	struct D2D::Polylines {
		/** Points of the poly-line. */
		Array<float2> Points;
		/** Color of the line. */
		Color Brush;
		/** Size of the line width. */
		Sizes Size;
		/** Horizontal alignment of box.  */
		HAligns HAlign = HAligns::HLeft;
		/** Vertical alignment of box.  */
		VAligns VAlign = VAligns::VTop;
		/** The way how scale of Points will be interpreted. */
		SSModes Mode = SSModes::Pixel;
	};


	struct D2D::Statistics {
		/** Input statistic. */
		struct {
			/** Offset to array (will be interpreted as modular). */
			size_t IndexOffset = 0;
			/** Samples of the statistic. */
			Array<f32> Samples;
		} Statistic;
		/**
		 * Minimal value of chart.
		 * If value will not be changed, minimum will be computed from samples.
		 */
		f32 Min = Math::ConstantsF::Max;
		/**
		 * Maximal value of chart.
		 * If value will not be changed, maximum will be computed from samples.
		 */
		f32 Max = Math::ConstantsF::Min;
		/** Area of the chart in the screen space. */
		Boxes Box;
		/** Color of the chart polyline. */
		Color Brush;
		/** Size of the chart polyline. */
		Sizes Size;
		/** Callbacks from runtime. Could serve for e.g. background rendering. */
		struct {
			/**
			 * Callback that statistic has been computed.
			 * @arg0 D2D        - 2D device.
			 * @arg1 Statistics - source statistic properties
			 * @arg2 Boxes      - box of the statistic in pixels and Left-Top origin.
			 * @arg3 f32        - minimal value of the set.
			 * @arg4 f32        - maximal value of the set.
			 * @arg5 f32        - average value of the set.
			 */
			IAction<D2D &, const Statistics &, const Boxes &, f32, f32, f32> * Computed = 0;
		} Callbacks;
	};


	struct D2D::Strings {
	public:  // nested & typedefs
		/** Represents part of formatting of string. */
		struct Tag {
			/** Boundaries of formatting. */
			size_t From, Length;
			/** Formatting type. */
			Fonts Font;
			/** Zero constructor. */
			Tag() = default;
			/** Standard constructor. */
			Tag(size_t from, size_t length, const Fonts & font)
				: From(from), Length(length), Font(font)
			{}
		};

	public: // properties
		/** String data. */
		DD::String Text;
		/** Formatting. */
		List<Tag, 16> Tags;

	public: // methods
		/** Color whole string. */
		D2D::Strings & Color(const Fonts & font);
		/** Color sequence. */
		D2D::Strings & Color(const Fonts & font, size_t from, size_t length);
		D2D::Strings & Color(const Fonts & font, DD::Math::Interval1<size_t> interval) { return Color(font, interval.Min[0], interval.Size()[0]); }

	public: // inherited methods
		/** Append given arg. */
		template<typename T>
		D2D::Strings & Append(const T & input);
		/** Append given arg. */
		template<typename T>
		D2D::Strings & AppendEx(const T & input, const DD::Text::StringView16 & format);
		/** Append given arg. */
		template<typename T>
		D2D::Strings & Append(const T & input, size_t size);
		/** Append given string. */
		template<size_t ALLOCATION>
		D2D::Strings & Append(const D2D::StringsLocal<ALLOCATION> & s) { return Append(static_cast<const D2D::Strings&>(s)); }
		/** Append given string. */
		template<>
		D2D::Strings & Append(const D2D::Strings & input);
		/** Append arg with given font. */
		template<typename T>
		D2D::Strings & Append(const Fonts & font, const T & input);
		/** Append formatted string. */
		template<typename...ARGS>
		D2D::Strings & AppendFormat(const char * format, const ARGS & ... args);
		/** Append formatted string. */
		template<typename...ARGS>
		D2D::Strings & AppendFormat(const wchar_t * format, const ARGS & ... args);
		/** Append formatted string. */
		template<typename...ARGS>
		D2D::Strings & AppendFormat(const DD::Text::StringView16 & format, const ARGS & ... args);
		/** Append formatted string. */
		template<typename...ARGS>
		D2D::Strings & AppendFormat(const Fonts & font, const char * format, const ARGS & ... args);
		/** Append formatted string. */
		template<typename...ARGS>
		D2D::Strings & AppendFormat(const Fonts & font, const wchar_t * format, const ARGS & ... args);
		/** Append formatted string. */
		template<typename...ARGS>
		D2D::Strings & AppendFormat(const Fonts & font, const DD::Text::StringView16 & format, const ARGS & ... args);
		/** Append multiple args. */
		template<typename...ARGS>
		D2D::Strings & AppendBatch(ARGS...input);
		/** Append multiple args with given font. */
		template<typename...ARGS>
		D2D::Strings & AppendBatch(const Fonts & font, ARGS...input);
		/** Append a sequence of given char. */
		template<typename...ARGS>
		D2D::Strings & AppendFill(ARGS...);
		/** Swap content with another string. */
		D2D::Strings & Swap(D2D::Strings & f);
		/** Clear the content. */
		D2D::Strings & Clear();
		/** Length of the string. */
		size_t Length() const { return Text.Length(); }
		/**
		 * Set a tag for highlight if given match exists.
		 * @warning pattern of match must be null terminated string.
		 */
		void Highlight(const Fonts & font, const wchar_t * pattern);

	public: // constructors
		/** Zero constructor. */
		Strings() : Text(), Tags() {}
		/** Constructor with string preallocation. */
		Strings(size_t allocation) : Text(allocation), Tags() {}
		/** Implicit constructor from a string. */
		Strings(const Text::StringView16 & string) : Text(string), Tags() {}
		/** Copy constructor. */
		//String(const DD::D2D::String &) = default;
	};


	template<size_t ALLOCATION>
	struct D2D::StringsLocal : public D2D::Strings {
	private: // properties
		/** Storage. */
		wchar_t _allocation[ALLOCATION];

	public: // constructors
		/** Constructor. */
		StringsLocal() : Strings() { Text.Wrap(_allocation); }
	};


	struct D2D::Labels {
		/** Descriptor of the label area.  */
		Boxes Box;
		/** Formatted text of the label. */
		Strings String;
		/** Horizontal alignment of text in the box. */
		THAligns THAlign = THAligns::TLeft;
		/** Vertical alignment of text in the box. */
		TVAligns TVAlign = TVAligns::TTop;
		/** Explicit scaling of the text. */
		f32 Scale = 1.f;
		/** Callbacks from runtime. Could serve for e.g. background rendering. */
		struct {
			/**  */
			IAction<const Boxes &, D2D &> * SizeComputed = 0;
		} Callbacks;
	};


	struct D2D::Area {
	protected: // properties
		/** Main rendering device. */
		D2D & _device;

	public: // properties
		/** Dimensions of relative area. */
		float2 Dimensions;
		/** Offsets of relative area. */
		float2 Offsets;

	public:	// methods
		/** Convert box into the relative area. */
		Boxes convert(const Boxes &);
		/** Convert rectangle into the relative area. */
		Rectangles convert(const Rectangles &);
		/** Convert line into the relative area. */
		Lines convert(const Lines &);
		/** Convert text into the relative area. */
		Labels convert(const Labels &);

	public:	// methods
		/** */
		void DrawLabel(const Labels & t) { return _device.DrawLabel(convert(t)); }
		/** Render ellipse. */
		void DrawEllipse(const Rectangles & r) { _device.DrawEllipse(convert(r)); }
		/** Render rectangle. */
		void DrawRectangle(const Rectangles & r) { _device.DrawRectangle(convert(r)); }
		/** Render line. */
		void DrawLine(const Lines & l) { _device.DrawLine(convert(l)); }

	public: // constructors
		/** Constructor. */
		Area(D2D &, const Boxes &);
	};
}


namespace DD::Gpu {
	inline D2D::Strings & D2D::Strings::Color(const Fonts & font) {
		return D2D::Strings::Color(font, 0, Text.Length());
	}

	inline D2D::Strings & D2D::Strings::Color(const Fonts & font, size_t from, size_t length) {
		Tags.Add(Tag({ from, length, font }));
		return *this;
	}

	template<typename T>
	D2D::Strings & D2D::Strings::Append(const T & input) {
		Text.Append(input);
		return *this;
	}

	template<typename T>
	D2D::Strings & D2D::Strings::AppendEx(const T & input, const DD::Text::StringView16 & format) {
		Text.Append(input, format);
		return *this;
	}

	template<typename T>
	D2D::Strings & D2D::Strings::Append(const T & input, size_t size) {
		Text.Append(Text::StringView(input, size));
		return *this;
	}

	template<>
	inline D2D::Strings & D2D::Strings::Append(const D2D::Strings & input) {
		size_t zero = Text.Length();
		Text.Append(input.Text);
		for (Tag tag : input.Tags) {
			tag.From += zero;
			Tags.Add(tag);
		}
		return *this;
	}

	template<typename T>
	D2D::Strings & D2D::Strings::Append(const Fonts & font, const T & input) {
		Tag tag;
		tag.Font = font;
		tag.From = Text.Length();
		Text.Append(input);
		tag.Length = Text.Length() - tag.From;
		Tags.Add(tag);
		return *this;
	}

	template<typename...ARGS>
	D2D::Strings & D2D::Strings::AppendFormat(const char * format, const ARGS & ... args) {
		Text.AppendFormat(format, args...);
		return *this;
	}

	template<typename...ARGS>
	D2D::Strings & D2D::Strings::AppendFormat(const wchar_t * format, const ARGS & ... args) {
		Text.AppendFormat(format, args...);
		return *this;
	}

	template<typename...ARGS>
	D2D::Strings & D2D::Strings::AppendFormat(const DD::Text::StringView16 & format, const ARGS & ... args) {
		Text.AppendFormat(format, args...);
		return *this;
	}

	template<typename...ARGS>
	D2D::Strings & D2D::Strings::AppendFormat(const Fonts & font, const char * format, const ARGS & ... args) {
		Tag tag;
		tag.Font = font;
		tag.From = Text.Length();
		Text.AppendFormat(format, args...);
		tag.Length = Text.Length() - tag.From;
		Tags.Add(tag);
		return *this;
	}

	template<typename...ARGS>
	D2D::Strings & D2D::Strings::AppendFormat(const Fonts & font, const wchar_t * format, const ARGS & ... args) {
		Tag tag;
		tag.Font = font;
		tag.From = Text.Length();
		Text.AppendFormat(format, args...);
		tag.Length = Text.Length() - tag.From;
		Tags.Add(tag);
		return *this;
	}

	template<typename...ARGS>
	D2D::Strings & D2D::Strings::AppendFormat(const Fonts & font, const DD::Text::StringView16 & format, const ARGS & ... args) {
		Tag tag;
		tag.Font = font;
		tag.From = Text.Length();
		Text.AppendFormat(format, args...);
		tag.Length = Text.Length() - tag.From;
		Tags.Add(tag);
		return *this;
	}

	template<typename...ARGS>
	D2D::Strings & D2D::Strings::AppendBatch(ARGS ...input) {
		Text.AppendBatch(input...);
		return *this;
	}

	template<typename...ARGS>
	D2D::Strings & D2D::Strings::AppendBatch(const Fonts & font, ARGS ...input) {
		Tag tag;
		tag.Font = font;
		tag.From = Text.Length();
		Text.AppendBatch(input...);
		tag.Length = Text.Length() - tag.From;
		Tags.Add(tag);
		return *this;
	}

	inline D2D::Strings & D2D::Strings::Swap(D2D::Strings & f) {
		Text.Swap(f.Text);
		Tags.Swap(f.Tags);
		return *this;
	}

	inline D2D::Strings & D2D::Strings::Clear() {
		Text.Clear();
		Tags.Clear();
		return *this;
	}

	inline void D2D::Strings::Highlight(const Fonts & font, const wchar_t * pattern) {
		size_t begin, end;
		if (Text.Match(pattern, begin, end).Length()) {
			Color(font, begin, end - begin);
		}
	}

	template<typename...ARGS>
	inline D2D::Strings & D2D::Strings::AppendFill(ARGS...args) {
		Text.AppendFill(args...);
		return *this;
	}
}