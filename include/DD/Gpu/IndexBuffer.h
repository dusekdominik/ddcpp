#pragma once
namespace DD::Gpu {
	enum IIndexBufferPrimitives : char;
	/**
	 * Index Buffer.
	 * PRIMITVE - declaration of primitive (line/triangle).
	 * INDEX - declaration of integer.
	 */
	template<IIndexBufferPrimitives PRIMITIVE, Gpu::IIndexBufferIndices INDEX>
	class IndexBuffer;
}

#include <DD/Gpu/IIndexBuffer.h>
#include <DD/List.h>
#include <DD/Vector.h>
#include <DD/Types.h>

namespace DD::Gpu {
	/************************************************************************/
	/* INDEX TRAITS                                                         */
	/************************************************************************/
	template<IIndexBufferIndices>
	struct IndexBufferIndexTraits { };
	template<>
	struct IndexBufferIndexTraits<IIndexBufferIndices::_8Bit> { typedef u8 index_t; };
	template<>
	struct IndexBufferIndexTraits<IIndexBufferIndices::_16Bit> { typedef u16 index_t; };
	template<>
	struct IndexBufferIndexTraits<IIndexBufferIndices::_32Bit> { typedef u32 index_t; };

	/************************************************************************/
	/* PRIMITIVE TRAITS                                                     */
	/************************************************************************/
	enum IIndexBufferPrimitives : char { Lines = 2, Triangles = 3, };

	template<IIndexBufferPrimitives PRIMITIVE, IIndexBufferIndices INDEX>
	struct IndexBufferPrimitiveTraits {
		typedef typename IndexBufferIndexTraits<INDEX>::index_t index_t;
		typedef Vector<index_t, static_cast<size_t>(PRIMITIVE)> primitive_t;
	};

	/************************************************************************/
	/* INDEX BUFFER                                                         */
	/************************************************************************/
	template<IIndexBufferPrimitives PRIMITIVE, IIndexBufferIndices INDEX>
	class IndexBuffer
		: public IIndexBuffer
		, public IndexBufferPrimitiveTraits<PRIMITIVE, INDEX>
		, public List<typename IndexBufferPrimitiveTraits<PRIMITIVE, INDEX>::primitive_t>
	{
		using primitive_t = typename IndexBufferPrimitiveTraits<PRIMITIVE, INDEX>::primitive_t;
		using BaseList = List<primitive_t>;

	public:
		/** Init index buffer on VRAM. */
		void Init(Device & device) { IIndexBuffer::Init(device, (u32)BaseList::Size() * sizeof(primitive_t), BaseList::begin()); }
		/** Bind index buffer. */
		void Bind(unsigned offset = 0) { IIndexBuffer::Bind(INDEX, offset); }
		/** Release actual data and create new. */
		void Reinit(Device & device) { Release(), Init(device); }

	public:
		template<typename...ARGS>
		IndexBuffer(ARGS...args) : BaseList(args...) {}
	};
}