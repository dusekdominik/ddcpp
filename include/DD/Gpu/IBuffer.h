#pragma once
namespace DD::Gpu {
	/** GPU buffer common functionality. */
	class IBuffer;
}

#include <DD/Gpu/Device.h>
#include <DD/Memory/DeferredImpl.h>
#include <DD/Memory/NonCopyable.h>

namespace DD::Gpu {
	class IBuffer
		: Memory::NonCopyable
	{
		friend class Devices;

	public: // nested
		/** Fwd declaration of internal data representation. */
		struct DXModel;

	protected: // properties
		/** Handle to device. */
		Device _device;
		/** Handle to buffer. */
		//void * _buffer;
		Memory::DeferredImpl<DXModel, 8> _model;

	public: // methods
		/** Has buffer been already initialized. */
		bool IsInitialized() const { return _device; }

	public: // constructor
		/** Zero constructor. */
		IBuffer();
		/** Destructor */
		~IBuffer();
	};
}