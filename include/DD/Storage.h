#pragma once
namespace DD {
	/**
	 * Interface providing calls to copy and move constructor
	 * and providing information about size to be allocated.
	 * Serves as companion structure to @see Storage.
	 */
	struct IStorable;

	/**
	 * Automatic implementation of IStorable interface to an object.
	 * @param INTERFACE - Interface of the should be used.
	 * @param INSTANCE  - target object to be implemented.
	 * @example common usage
	 *   struct IMyStruct {
	 *     virtual void MyCall() = 0;
	 *   };
	 *
	 *   struct MyStruct
	 *     : public StorableFactory<IMyStruct, MyStruct> // implement the IStorable automatically
	 *   {
	 *     void MyCall() override;                       // interface impl
	 *     MyStruct(const MyStruct &);                   // need to have a copy constructor
	 *     MyStruct(MyStruct &&);                        // need to have move constructor
	 *   };
	 */
	template<typename INTERFACE, typename INSTANCE>
	struct StorableFactory;

	/**
	 * Smart pointer with local allocation (similar to unique pointer).
	 * If data cannot be filled into local allocation, allocate externally.
	 * @param T    - type to be stored (typically a virtual interface)
	 * @param SIZE - number of locally allocated bytes for internal storage.
	 */
	template<typename T, size_t SIZE>
	struct Storage;
}

#include <DD/Types.h>
#include "TemplateHelpers.h"
#include <new>
#include <typeinfo>

#if _DEBUG
#define DD_STORAGE_INLINE
#else
#define DD_STORAGE_INLINE __forceinline
#endif

namespace DD {
	struct IStorable {
		template<typename, size_t>
		friend struct Storage;

	protected: // IStorable
		/** Move constructor provider. */
		virtual void iStorableMoveTo(void * target) = 0;
		/** Copy constructor provider. */
		virtual void iStorableCopyTo(void * target) const = 0;
		/** Allocation size in bytes. */
		virtual size_t iStorableSize() const = 0;
		/** Debug helper - name of stored struct. */
		virtual const char * iStorableName() const { throw; }

	public: // public interface
		/** Move constructor provider. */
		void MoveTo(void * target) { iStorableMoveTo(target); }
		/** Copy constructor provider. */
		void CopyTo(void * target) const { iStorableCopyTo(target); }
		/** Allocation size in bytes. */
		size_t Size() const { return iStorableSize(); }
		/** Debug helper - name of stored struct. */
		const char * Name() const { return iStorableName(); }

	public: // constructors
		/** Destructor. */
		virtual ~IStorable() {}
	};

	template<typename INTERFACE, typename INSTANCE>
	struct StorableFactory
		: public INTERFACE
	{
	protected: // IStorable
		void iStorableMoveTo(void * target) override { new (target) INSTANCE(Fwd(*(INSTANCE *)this)); }
		void iStorableCopyTo(void * target) const override { new (target) INSTANCE(*(INSTANCE *)this); }
		size_t iStorableSize() const override { return sizeof(INSTANCE); }
		const char * iStorableName() const override { return typeid(INSTANCE).name(); }

	protected:
		/** Fwd constructor. */
		StorableFactory() = default;
		/** Copy constructor. */
		StorableFactory(const StorableFactory &) = default;
		/** Move constructor. */
		StorableFactory(StorableFactory &&) = default;
	};

	template<typename T, size_t SIZE>
	struct Storage {
		/** Safety check. */
		static_assert(__is_base_of(IStorable, T), "Must be stored just IStorable struct.");

	protected: // statics
		/** Casting helper. */
		template<typename STORABLE>
		DD_STORAGE_INLINE static IStorable * safeCast(STORABLE & storable);
		/** Casting helper. */
		template<typename STORABLE>
		DD_STORAGE_INLINE static const IStorable * safeCast(const STORABLE & storable);

	protected: // properties
		/** Pointer to the data. */
		T * _ptr;
		/** Storage/Size descriptor. */
		union {
			/** If data are allocated locally, here is the storage. */
			u8 _storage[SIZE < sizeof(size_t) ? sizeof(size_t) : SIZE];
			/** If data are allocated on heap, here is stored allocated size. */
			size_t _size;
		};

	protected: // methods
		/**
		 * Reallocation of storage.
		 * @return unsent pointer to allocated storage.
		 */
		DD_STORAGE_INLINE void * realloc(size_t bytes);
		/** Release data. */
		DD_STORAGE_INLINE void release();
		/** Get size from ptr. */
		DD_STORAGE_INLINE size_t stored() const { return static_cast<const IStorable *>(_ptr)->iStorableSize(); }
		/** Get name from ptr. */
		DD_STORAGE_INLINE const char * name() const { return static_cast<const IStorable *>(_ptr)->iStorableName(); }

	public: // methods
		/** Are data allocated locally. */
		DD_STORAGE_INLINE bool IsInternal() const { return (void *)_ptr == (void *)_storage; }
		/** Are data allocated on heap. */
		DD_STORAGE_INLINE bool IsExternal() const { return (void *)_ptr != (void *)_storage; }
		/** Number of internally allocated bytes. */
		DD_STORAGE_INLINE size_t InternalAllocation() const { return SIZE; }
		/** Number of externally allocated bytes. */
		DD_STORAGE_INLINE size_t ExternalAllocation() const { return IsExternal() ? _size : 0; }
		/** Number of bytes allocated internally and externally. */
		DD_STORAGE_INLINE size_t Allocation() const { return InternalAllocation() + ExternalAllocation(); }
		/** Number of bytes used by the stored element. */
		DD_STORAGE_INLINE size_t Stored() const { return _ptr ? stored() : 0; }
		/** Name of stored ptr. */
		DD_STORAGE_INLINE const char * Name() const { return _ptr ? name() : 0; }

	public: // operators
		/** Dereference. */
		DD_STORAGE_INLINE T * operator->() { return _ptr; }
		/** Dereference. */
		DD_STORAGE_INLINE const T * operator->() const { return _ptr; }
		/** Dereference. */
		DD_STORAGE_INLINE T & operator*() { return *_ptr; }
		/** Dereference. */
		DD_STORAGE_INLINE const T & operator*() const { return *_ptr; }
		/** Copy assignment. */
		template<typename U>
		DD_STORAGE_INLINE Storage & operator=(const U & lvalue);
		/** Move assignment. */
		template<typename U>
		DD_STORAGE_INLINE Storage & operator=(U && rvalue);
 		/** Copy assignment. */
 		DD_STORAGE_INLINE Storage & operator=(const Storage & lvalue);
 		/** Move assignment. */
 		DD_STORAGE_INLINE Storage & operator=(Storage && rvalue);

	public: // constructors
		/** Zero constructor. */
		DD_STORAGE_INLINE Storage() : _size(0), _ptr(0) {}
		/** Capture constructor. */
		template<typename STORABLE>
		DD_STORAGE_INLINE Storage(const STORABLE & storable) : Storage() { operator=(storable); }
		/** Capture constructor. */
		template<typename STORABLE>
		DD_STORAGE_INLINE Storage(STORABLE && storable) : Storage() { operator=(Fwd(storable)); }
		/** Copy constructor. */
		DD_STORAGE_INLINE Storage(const Storage & lvalue) : Storage() { operator=(*lvalue._ptr); }
		/** Move constructor. */
		DD_STORAGE_INLINE Storage(Storage && rvalue) : Storage() { operator=(Fwd(*rvalue._ptr)); }
		/** Destructor. */
		DD_STORAGE_INLINE ~Storage() { release(); }
	};
}

namespace DD {
	template<typename T, size_t SIZE>
	template<typename STORABLE>
	DD_STORAGE_INLINE IStorable * Storage<T, SIZE>::safeCast(STORABLE & storable) {
		return &static_cast<IStorable &>(static_cast<T &>(storable));
	}


	template<typename T, size_t SIZE>
	template<typename STORABLE>
	DD_STORAGE_INLINE const IStorable * Storage<T, SIZE>::safeCast(const STORABLE & storable) {
		return &static_cast<const IStorable &>(static_cast<const T &>(storable));
	}


	template<typename T, size_t SIZE>
	template<typename U>
	DD_STORAGE_INLINE Storage<T, SIZE> & Storage<T, SIZE>::operator=(const U & lvalue) {
		size_t size = static_cast<const IStorable &>(static_cast<const T &>(lvalue)).Size();
		void * target = realloc(size);
		safeCast()->iStorableCopyTo(target);

		return *this;
	}


	template<typename T, size_t SIZE>
	template<typename U>
	DD_STORAGE_INLINE Storage<T, SIZE> & Storage<T, SIZE>::operator=(U && rvalue) {
		size_t size = safeCast(rvalue)->iStorableSize();
		void * target = realloc(size);
		safeCast(rvalue)->iStorableMoveTo(target);

		return *this;
	}


	template<typename T, size_t SIZE>
	DD_STORAGE_INLINE Storage<T, SIZE> & Storage<T, SIZE>::operator=(const Storage & lvalue) {
		// set if lvalue is not empty
		if (lvalue._ptr)
			return *this = *lvalue;
		// release otherwise
		release();

		return *this;
	}


	template<typename T, size_t SIZE>
	DD_STORAGE_INLINE Storage<T, SIZE> & Storage<T, SIZE>::operator=(Storage && rvalue) {
		if (rvalue.IsExternal()) {
			// release current data
			release();
			// take pointer from rvalue
			_ptr = rvalue._ptr;
			// reset rvalue
			rvalue._ptr = nullptr;
		}
		else {
			// move data
			rvalue->MoveTo(realloc(rvalue->Size()));
		}

		return *this;
	}

	template<typename T, size_t SIZE>
	DD_STORAGE_INLINE void * Storage<T, SIZE>::realloc(size_t bytes) {
		if (_ptr) {
			// get current size
			size_t size = stored();
			// destruct local data
			_ptr->~T();
			// if the allocated size is same
			if (size == bytes)
				return _ptr;
			// cannot be stored in local storage
			if (SIZE < bytes) {
				// external storage exists
				if (IsExternal()) {
					// reallocate on the same heap allocation
					if (bytes <= _size)
						return _ptr;
					// reallocate on bigger space
					free(_ptr);
				}
				// allocate new memory
				_ptr = (T *)malloc(bytes);
				_size = bytes;
				return _ptr;
			}
			// store in local storage
			else {
				// external storage exists, remove it
				if (SIZE < size)
					free(_ptr);
				// set ptr to local
				_ptr = (T *)_storage;
				return _ptr;
			}
		}

		if (SIZE < bytes) {
			// allocate new memory
			_ptr = (T *)malloc(bytes);
			_size = bytes;
			return _ptr;
		}
		else {
			_ptr = (T *)_storage;
			return _ptr;
		}
	}

	template<typename T, size_t SIZE>
	DD_STORAGE_INLINE void Storage<T, SIZE>::release() {
		if (_ptr) {
			// call destructor
			_ptr->~T();
			// deallocate if needed
			if (IsExternal())
				free(_ptr);
			// nullify
			_ptr = nullptr;
		}
	}
}

#undef DD_STORAGE_INLINE
