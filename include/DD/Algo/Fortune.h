#pragma once
namespace DD::Algo {
	/**
	 * AVL Tree node.
	 * Structure for representation so called beach-line in Fortune's algo.
	 */
	struct FortuneAVLNode;

	/** Edge of Voronoi diagram. */
	struct FortuneEdge;

	/** Vertex of Voronoi diagram (not generating point). */
	struct FortuneVertex;

	/** Event of Fortune's algo. */
	struct FortuneEvent;

	/** Main class for construction of Voronoi diagram. */
	struct Fortune;
}

#include <DD/BlockListEx.h>
#include <DD/Collections/Bin.h>
#include <DD/List.h>
#include <DD/Vector.h>
#include <DD/Heap.h>
#include <DD/Math/LineSegment.h>
#include <DD/Math/HyperSphere.h>

#define DD_FORTUNE_ASSERT(c) DD_LITE_DEBUG_ASSERT(c, "Fortune algo")
#define DD_FORTUNE_WARNING(text) DD_LITE_DEBUG_WARNING(text)

namespace DD::Algo {
	struct FortuneEvent {
		/** Breakpoint collection type. */
		using Breakpoints = Collections::BinArray<FortuneAVLNode *, 2>;

		/** Event Type. */
		enum EventTypes { UndefinedEvent, SiteEvent, CircleEvent, SkipEvent, } EventType = UndefinedEvent;
		/** Point where event happens. */
		double2 Point = 0.0;
		/* Circle event attributes. */
		Math::Circle2D Circle;
		/** Reference to the tree. */
		FortuneAVLNode * ArcNode = nullptr;
		/* Site event attributes. */
		i32 Index = -1;

		Breakpoints ComputeBreakpoints();
		/** COmparer for heap operations. */
		constexpr bool operator<(const FortuneEvent & rhs) const { return (Point.y == rhs.Point.y && Point.x < rhs.Point.x) || Point.y < rhs.Point.y; }
	};


	struct FortuneVertex {
		/** Vertex position. */
		double2 Point;
		/** Edge points to the point [-->o] */
		FortuneEdge * VoronoiEdge = nullptr;
	};


	struct FortuneEdge {
		/** Edge pair. */
		using Pair = Collections::Bin<FortuneEdge *, FortuneEdge *>;
		/** Vertex Modes */
		enum struct VertexModes : i32 { None, Vertex0, Vertex1, Vertex01  };

		/** Connect two edges. */
		static constexpr void Connect(FortuneEdge * p1, FortuneEdge * p2) { p1->NextEdge = p2; p2->PrevEdge = p1; }

		/** Index to point. */
		i32 PointIndex0 = -1;
		/** Index to point. */
		i32 PointIndex1 = -1;
		/** Vertex of Voronoi diagram. */
		FortuneVertex * VoronoiVertex = nullptr;
		/** Reverse way edge. */
		FortuneEdge * Mirror = nullptr;
		/** Sibling edge. */
		FortuneEdge * NextEdge = nullptr;
		/** Sibling edge. */
		FortuneEdge * PrevEdge = nullptr;

		/** Both vertices are present. */
		bool IsSegmentLine() const { return VoronoiVertex != nullptr && Mirror->VoronoiVertex != nullptr; }
		/** Return what vertices are available for the edge. */
		VertexModes VertexMode() const { return (VertexModes)((VoronoiVertex != nullptr) | ((Mirror->VoronoiVertex != nullptr) << 1)); }
		/** Vertex whereto the edge points. */
		double2 VoronoiVertex0() const { return VoronoiVertex->Point; }
		/** Vertex where from edge points. */
		double2 VoronoiVertex1() const { return Mirror->VoronoiVertex->Point; }
	};


	struct FortuneAVLNode {
		/** Connect two arc (leaf) nodes. */
		static constexpr void ConnectLeaves(FortuneAVLNode * prev, FortuneAVLNode * next) { prev->NextArc = next; next->PrevArc = prev; }

		/** Indices of the points */
		Collections::Bin<i32, i32> PointIndices;
		/** Tree balancing. */
		i32 TreeHeight = 1;
		/** Tree node. */
		FortuneAVLNode * LeftChild = nullptr;
		/** Tree node. */
		FortuneAVLNode * RightChild = nullptr;
		/** Tree node. */
		FortuneAVLNode * Parent = nullptr;
		/** Arc sibling in the tree, previous leaf. */
		FortuneAVLNode * PrevArc = nullptr;
		/** Arc sibling, next leaf. */
		FortuneAVLNode * NextArc = nullptr;
		/** Event pointer. */
		FortuneEvent * CircleEvent = nullptr;
		/** Edge of Voronoi diagram. */
		FortuneEdge * VoronoiEdge = nullptr;

	private:
		/** Update height from children. */
		void updateHeight();
		/** Compute balance between l/r subtrees. */
		i32 getBalance() const;
		/**
		 *  AVL rotation.
		 *     p           p
		 *    /           /
		 *   n     ->    r
		 *  / \         / \
		 * l   r       n  rr
		 *    / \     / \
		 *   rl rr   l  rl
		 */
		FortuneAVLNode * leftRotation();
		/**
		 * AVL rotation.
		 *        p           p
		 *       /           /
		 *      n     ->    l
		 *     / \         / \
		 *    l   r       ll  n
		 *   / \             / \
		 *  ll lr           lr  r
		 */
		FortuneAVLNode * rightRotation();

	public:
		/** Is the node a leaf. */
		bool IsLeaf() const { return PointIndices[0] == PointIndices[1]; }
		/** Is the node the root */
		bool IsRoot() const { return Parent == nullptr; }
		/** Check if the node is a left child with respect to its parent */
		bool IsLeft() const { return !IsRoot() && Parent->LeftChild == this; }
		/** Get sibling with respect to the node's parent. */
		FortuneAVLNode * GetSibling() { return IsLeft() ? Parent->RightChild : Parent->LeftChild; }
		/** Set conditionally left/right child.  */
		void SetChild(bool left, FortuneAVLNode * node) { left ? LeftChild = node : RightChild = node; }
		/** AVL R-L/L-R rebalancing of node if needed, @return new root of the rebalanced subtree. */
		FortuneAVLNode * BalanceNode();
		/** Apply balance node from leaf to root, return root node (can change during balance process). */
		FortuneAVLNode * BalanceTree();
		/**
		 *  Remove leaf l as on diagram, rebalance tree, @return tree root.
		 *         gp        gp
		 *        /  \      /  \
		 *       p    q -> r    q
		 *      / \
		 *     l   r
		 */
		FortuneAVLNode * RemoveLeaf();
	};


	struct Fortune {
	public: // statics
		/** Default scale for half-opened/opened edges. */
		static constexpr f64 DefaultScale = 100.0;
		/** Create dir from normal, CCW,  */
		static constexpr double2 Normal2Dir(double2 normal) { return { -normal.y, normal.x }; }
		/** Create dir from (denormalized) normal, CCW,  */
		static constexpr double2 Normal2Dir(double2 normal, f64 scale) { return Normal2Dir(normal) * (scale / Math::Norm(normal)); }

	public: // properties
		/** Reference to point generators. */
		const ArrayView1D<const double2> Points;
		/** Buffer for faces. */
		List<FortuneEdge *> Faces;
		/** Event queue. */
		MinHeap<FortuneEvent *> _eventHeap;
		/** Tree root. */
		FortuneAVLNode * Root;
		/** Current sweepline (Y) position. */
		f64 SweeplineY = 0.0;
		/** Node allocator. */
		BlockListEx<FortuneAVLNode, 255> _nodeAllocator;
		/** Edge allocator. */
		BlockListEx<FortuneEdge, 255> _edgeAllocator;
		/** Event allocator. */
		BlockListEx<FortuneEvent, 255> _eventAllocator;
		/** Vertex allocator. */
		BlockListEx<FortuneVertex, 255> _vertexAllocator;

	protected: // methods
		/**
		 *    n1   Create subtree, 2 nodes, 3 leaves.
		 *   /  \
		 *  l1  n2      @param rnode n1
		 *     /  \     @param lleaf l1
		 *    l2   l3   @param rleaf l3
		 */
		void createTwoNodeSubtree(i32, i32, FortuneAVLNode *& rnode, FortuneAVLNode *& lleaf, FortuneAVLNode *& rleaf);
		/**
		 *   n1  Create subtree, 1 node, 2 leaves.
		 *  /  \
		 * l1  l2
		 */
		void createOneNodeSubtree(i32, i32, FortuneAVLNode *& rnode, FortuneAVLNode *& lleaf, FortuneAVLNode *& rleaf);
		/** Find leaf (arc) at given value. */
		FortuneAVLNode * findLeaf(FortuneAVLNode * root, f64 x);
		/** Replacement method. */
		FortuneAVLNode * replaceNode(FortuneAVLNode * node, FortuneAVLNode * new_node);
		/** Tree ordering value, @return parabola focus if leaf node, breakpoint if internal node. */
		double computeNodeXValue(FortuneAVLNode * node);
		/** Allocate edge and its mirror. */
		FortuneEdge::Pair createEdgePair(i32 leftIndex, i32 rightIndex);;
		/** Add circle event if it is needed. */
		void checkCircleEvent(FortuneAVLNode * n1, FortuneAVLNode * n2, FortuneAVLNode * n3);
		/** Process a site event. */
		void processSiteEvent(FortuneEvent * e);
		/** Process circle event. */
		void processCircleEvent(FortuneEvent * e);

	public: // methods
		/** Create all events and process them. */
		void Process();
		/** Get Voronoi diagram as lines. */
		Array<Math::LineSegment2D> GetLines() const;
		/** Get Voronoi diagram as polygons, */
		Array<Array<double2>> GetPolygons(Math::Interval2D frame) const;
	};
}
