#pragma once
namespace DD {
	/**
	 * STL format.
	 * https://en.wikipedia.org/wiki/STL_(file_format)
	 */
	struct StandardTriangleLanguage;
}

#include <DD/Array.h>
#include <DD/FileSystem/Path.h>
#include <DD/Types.h>
#include <DD/Vector.h>

namespace DD {
	struct StandardTriangleLanguage {
	public: // nested
#pragma pack(push, 1)
		/** A single triangle record in this format. */
		struct Triangle {
			/** Normal and vertices coordinates. */
			float3 N, A, B, C;
			/** Syntactic reason. */
			u16 Attributes;
		};
#pragma pack(pop)
		/** Safety check. */
		static_assert(sizeof(Triangle) == 50, "Invalid packing");

	protected:
		/** Data of STL file. */
		Array<i8> _data;

	public: // methods
		/** Number of triangles. */
		u32 Count() const { return *(const u32 *)(_data.begin() + 80); }
		/** Iterator.. */
		const Triangle * begin() const { return (const Triangle *)(_data.begin() + 84); }
		/** Iterator. */
		const Triangle * end() const { return begin() + Count(); }
		/** Get n-th triangle. */
		const Triangle * At(size_t n) const { return begin() + n; }
		/** Load data from a file. */
		bool Load(const DD::FileSystem::Path & file) { return (_data = file.AsFile().Read()).Length() != 0; }

	public: // constructors
		/** Constructor. */
		StandardTriangleLanguage() = default;
	};
}
