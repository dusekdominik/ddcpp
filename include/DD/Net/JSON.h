#pragma once
namespace DD::Net::JSON {
	/** Complete set of types of JSON objects. */
	enum struct Types {
		Array,  // Array of items
		Number, // Simple element, number
		Map,    // Commonly referred as object, mapping between names and values
		Member, // Member of Map (read object) ~ pair of a name and a value
		String, // Text element
		Literal // Null, True or False
	};
	/** Type of literal values. */
	enum struct Literals { Null, True, False, Undefined };
	/** Parent of all JSON objects. */
	struct Any;
	/** Set of indexed items.  */
	struct Array;
	/** Null, true, or false. */
	struct Literal;
	/** Number value, currently f64. */
	struct Number;
	/** Object, set of key-value pairs. */
	struct Map;
	/** JSON string holder. */
	struct String;
	/** Key-value pair, item of @see Map. */
	struct Member;

	/** Text reader parsing JSON letter by letter. */
	struct Reader;
}

#include <DD/Array.h>
#include <DD/List.h>
#include <DD/String.h>
#include <DD/Types.h>
#include <DD/Unique.h>

namespace DD::Net::JSON {
	struct Any {
	public: // properties
		/** Type of the objects, serves for safe casting. */
		Types Type;
		/** Parent, pointer to the parent object if exists, null otherwise. */
		Any * Parent = nullptr;

	public: // value getters
		/** Get member by its name, if type is Map, null otherwise.  */
		Any * GetMember(const DD::Text::StringView8 & name);
		/** Get array item by index, or map member, null otherwise. */
		Any * GetIndex(size_t index);
		/** Get first item of array type, null otherwise. */
		Any * GetFirst();
		/** Get last item of an array. */
		Any * GetLast();
		/** Get n-th last item of array type, null otherwise. */
		Any * GetLast(size_t nth);
		/** Get numeric value of Number type, zero otherwise. */
		f64 GetNumber();
		/** Get string value of String type, otherwise empty string. */
		DD::Text::StringView16 GetString();
		/** Get number of array items, map members, zero otherwise. */
		size_t GetSize();
		/** Get literal value. */
		bool IsTrue();
		/** Get literal value. */
		bool IsFalse();
		/** Get literal value. */
		bool IsNull();

	public: // caster helpers
		Array * AsArray();
		Number * AsNumber();
		Map * AsMap();
		Member * AsMember();
		String * AsString();
		Literal * AsLiteral();
	};

	struct Literal : public Any {
		/** Literal value. */
		Literals Value;
		/** Constructor. */
		Literal() : Any{ Types::Literal } {}
	};

	struct Array : public Any {
		/** Array value. */
		::DD::List<Unique<Any>> Value;
		/** Constructor. */
		Array() : Any{ Types::Array } {}
	};

	struct Number : public Any {
		/** Number value. */
		f64 Value = 0.0;
		/** Constructor. */
		Number() : Any{ Types::Number } {}
	};

	struct String : public Any {
		/** String value. */
		::DD::String Value;
		/** Constructor. */
		String() : Any{ Types::String } {}
	};

	struct Member : public Any {
		/** Name of the pair. */
		::DD::String Name;
		/** Value of the pair. */
		Unique<Any> Value;
		/** Constructor. */
		Member() : Any{ Types::Member } {}
	};

	struct Map : public Any {
		/** Set of key-value pairs. */
		::DD::List<Unique<Member>> Value;
		/** Get value, by given string key. */
		Any * Get(const DD::Text::StringView8 & name);
		/** Constructor */
		Map() : Any{ Types::Map } {}
	};

	struct Reader {
	public: // nested
		/** Automaton states, https://www.json.org/json-en.html. */
		enum struct Stages {
			// Right after '{', expected whitespace, '"' or '}'
			_Object_0_Begin,
			// State after loading first name-value pair, expected ws or '"'
			_Object_1_WhitespaceBeforeName,
			// State after '"', expected letter until '"' is matched
			_Object_2_Name,
			// State after loading a name of name-value pair, expected ws or ':'
			_Object_3_WhitespaceAfterName,
			// State after ':', expected ws, when no ws met, stack state, load value
			_Object_4_WhitespaceBeforeValue,
			// State after value of name-value pair is loaded, expected ws, ',' or '}'
			_Object_5_WhitespaceAfterValue,
			// State after '[', expected ws
			_Array_0_Begin,
			// Expected ws, ',' or ']'
			_Array_1_WhitespaceAfterValue,
			// State after '"'
			_String_0_Letters,
			// Unscent state, expect ws, '{', '[', '"' or number.
			_Value_0_Whitespace,
			// State after '-.0123456789', unstack after first non-numeric is met
			_Number_0_Numbers,
			// Load literal null/true/false
			_Literal_0_Letters,
			// Unrecognized transition has been met
			_Error,
			// Select method according to current state
			_Autoselect
		};

	public: // statics
		/** Read entire url. */
		template<typename SEQUENCER>
		static Unique<Any> FromSequence(SEQUENCER & sequencer);

	protected: // properties
		/** Stack of reading states. */
		::DD::List<Stages, 16> _stages;
		/** Helper for reading current values */
		::DD::List<char, 64> _stringBuffer;
		/** Root of loaded JSON. */
		::DD::Unique<Any> _root;
		/** Currently loaded JSON. */
		Any * _current = nullptr;
		/** Target for new item. */
		Any ** _target;

	private: // methods
		template<Stages> void ReadLetter(char letter);
		template<> void ReadLetter<Stages::_Autoselect>(char letter);
		template<> void ReadLetter<Stages::_Array_0_Begin>(char letter);
		template<> void ReadLetter<Stages::_Array_1_WhitespaceAfterValue>(char letter);
		template<> void ReadLetter<Stages::_Object_0_Begin>(char letter);
		template<> void ReadLetter<Stages::_Object_1_WhitespaceBeforeName>(char letter);
		template<> void ReadLetter<Stages::_Object_2_Name>(char letter);
		template<> void ReadLetter<Stages::_Object_3_WhitespaceAfterName>(char letter);
		template<> void ReadLetter<Stages::_Object_4_WhitespaceBeforeValue>(char letter);
		template<> void ReadLetter<Stages::_Object_5_WhitespaceAfterValue>(char letter);
		template<> void ReadLetter<Stages::_Number_0_Numbers>(char letter);
		template<> void ReadLetter<Stages::_String_0_Letters>(char letter);
		template<> void ReadLetter<Stages::_Value_0_Whitespace>(char letter);
		template<> void ReadLetter<Stages::_Literal_0_Letters>(char letter);
		void Create(Any * abstract);

	public: // methods
		/** Transition table. */
		void ReadLetter(char letter);
		/** Check if parsing was done. */
		bool Valid() const { return _stages.IsEmpty() && _root != nullptr; }
		/** Root element of the JSON. */
		Any * Root() { return _root; }

	public: // constructors
		/** Constructor. */
		Reader() : _target(_root.Address()) { _stages.Add(Stages::_Value_0_Whitespace); }
	};
}

namespace DD::Net::JSON {
	template<typename SEQUENCER>
	Unique<Any> Reader::FromSequence(SEQUENCER & sequencer) {
		// prepare reader
		Reader reader;
		// push the entire sequence to the reader
		for (bool valid = sequencer.Reset(); valid; valid = sequencer.Next())
			reader.ReadLetter(sequencer.Get());
		// if data has been parsed, return the root element
		return reader.Valid() ? Fwd(reader._root) : nullptr;
	}
}
