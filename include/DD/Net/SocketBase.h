#pragma once
namespace DD::Net {
	/**
	 * Helper for working with sockets.
	 * Basically supports all the socket types.
	 * Because of possible confusion, functionality
	 * should be used only with specialized classes.
	 * not directly.
	 */
	struct SocketBase;

	/** State of socket. */
	class SocketStates;
}

#include <DD/Enum.h>
#include <DD/Net/IPv4.h>
#include <DD/Unique.h>

namespace DD::Net {
	DD_ENUM_32(
		(SocketStates, "States of a socket indicating validity or type of invalidity."),
		(VALID,         0, "Only valid state of socket. Socket can be used."),
		(NETWORK_RESET, 1, "Network dropped connection on reset."),
		(RESET_BY_PEER, 2, "Connection reset by peer. An existing connection was forcibly closed by the remote host."),
		(ALREADY_USED,  3, "Address already in use. Typically, only one usage of each socket address is permitted"),
		(REFUSED,       4, "No connection could be made because the target computer actively refused it."),
		(UNHANDLED,     5, "Unhandled invalid state of socket.")
	);

	struct SocketBase {
		/** A local ip. */
		static IPv4 GetLocalIp();

	private: // forwards
		 /** Struct representing platform dependent api for internal mechanism of socket. */
		struct API;

	protected: // properties
		/** Storage of platform dependent api. */
		Unique<API> _api;

	public: // properties
		/** Getter of IP. */
		const IPv4 & IPAddress() const;
		/** Validity check. */
		bool IsValid() const { return State() == SocketStates::VALID; }
		/** State of  */
		SocketStates State() const;

	public: // methods
		/** Accept connection by already listening TCP server. */
		void Accept(SocketBase & accepted);
		/** Bind TCPServer or UDPReceiver to an address. */
		void Bind();
		/** Connect TCPClient to an address. */
		void Connect();
		/** Close socket. */
		void Close();
		/** Initialize IP address. */
		void InitAddress();
		/** Listen by TCPServer for an acceptance. */
		void Listen();
		/** Receive data. */
		void Receive(char * buffer, int & length);
		/** Receive data and get about sender. */
		void ReceiveFrom(char * buffer, int & length, IPv4 & ip);
		/** Get size of received data. */
		void ReceiveSize(unsigned long &);
		/** Send data. */
		void Send(const char * buffer, int length);
		/** Should be called before closing. */
		void Shutdown();

	protected: // constructors
		 /** Constructor. */
		SocketBase();

	public: // constructors
		/** Destructor. */
		~SocketBase();
	};
}