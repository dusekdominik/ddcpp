#pragma once
namespace DD::Net {
	/**
	 * Simple TCP client.
	 * Example:
	 *  TCPClient client("192.168.1.1", "10000");
	 *  client.OnMessageReceived += [](UniqueArray<char> & data) { processSomeHow(data); };
	 *  client.StartReceiving();
	 */
	struct TCPClient;
	/**
	 * Helper class for referential implementation.
	 */
	struct TCPClientBase;
}

#include <DD/Concurrency/Thread.h>
#include <DD/Delegate.h>
#include <DD/Net/SocketBase.h>
#include <DD/Reference.h>

namespace DD::Net {
	struct TCPClientBase
		: protected SocketBase
	{
		using SocketBase::IPAddress;
		using SocketBase::IsValid;
		using SocketBase::Receive;
		using SocketBase::ReceiveFrom;
		using SocketBase::Send;
		using SocketBase::State;

	protected: // properties
		/** Thread holder for async receiving. */
		Concurrency::Thread _worker;

	public: // properties
		/** Delegate for processing incoming messages. */
		Event<Array<u8> &> OnMessageReceived;

	public: // methods
		/** Async receiving method. After enabling messages will be proceed via OnMessageReceived. */
		void StartReceiving();

	public: // constructors
		/** Zero constructor. */
		TCPClientBase() : SocketBase() {}
		/** Constructor. */
		TCPClientBase(const IPv4 & ip);
		/** Standard constructor. */
		TCPClientBase(const String & host, const String & port);
	};


	struct TCPClient
		: public Reference<TCPClientBase>
	{
		/** Constructor. */
		TCPClient(const String & host, const String & port) : Reference(new TCPClientBase(host, port)) {}
		/** Constructor. */
		TCPClient(const IPv4 & ip) : Reference(new TCPClientBase(ip)) {}
		/** Constructor. */
		TCPClient() : Reference() {}
	};
}
