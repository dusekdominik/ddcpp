#pragma once
namespace DD::Net {
	/**
	 * Simple TCP server.
	 * Example:
	 *  TCPServer server("localhost", "22222");
	 *  server.OnClientAccepted += [](TCPClient client) { doProtocol(client); };
	 *  server.StartAccepting();
	 */
	struct TCPServer;
	/**
	 * Helper class for referential implementation.
	 */
	struct TCPServerBase;
}

#include <DD/Concurrency/Thread.h>
#include <DD/Concurrency/ThreadSafeObject.h>
#include <DD/Delegate.h>
#include <DD/List.h>
#include <DD/Net/SocketBase.h>
#include <DD/Net/TCPClient.h>
#include <DD/Reference.h>

namespace DD::Net {
	struct TCPServerBase
		: protected SocketBase
	{
		using SocketBase::IPAddress;
		using SocketBase::IsValid;
		using SocketBase::State;

	protected: // properties
		/** Thread holder for async accepting. */
		Concurrency::Thread _worker;

	public: // properties
		/** Delegate for processing incoming connection. */
		Event<TCPClient> OnClientAccepted;
		/** Clients accepted by server. */
		Concurrency::ThreadSafeObject<List<TCPClient>> Clients;

	public: // methods
		/** Async accepting method. After enabling incoming connections will be proceed via OnClientAccepted. */
		void StartAccepting();

	public: // constructors
		/** Constructor. */
		TCPServerBase(Text::StringView16 host, Text::StringView16 port);
	};

	struct TCPServer : public Reference<TCPServerBase> {
		/** Zero Constructor. */
		TCPServer() : Reference() {}
		/** Any server. */
		TCPServer(u16 port) : Reference(new TCPServerBase(L"0.0.0.0", String::From(port))) {}
		/** Any server. */
		TCPServer(Text::StringView16 port) : Reference(new TCPServerBase(L"0.0.0.0", port)) {}
		/** Constructor. */
		TCPServer(Text::StringView16 host, Text::StringView16 port) : Reference(new TCPServerBase(host, port)) {}
	};
}
