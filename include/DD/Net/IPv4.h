#pragma once
namespace DD::Net {
	/**
	 * IPv4 provides information about IP and port.
	 * It is helper structure for make view onto binary data.
	 */
	union IPv4;
}

#include <DD/Macros.h>
#include <DD/String.h>
#include <DD/Types.h>

namespace DD::Net {
	union IPv4 {
		/** View separating port and address. */
		DD_PACK(struct { u16 _port; u32 _ip; });
		/** Byte view. */
		DD_PACK(struct { u8 _bytes[6]; });
		/** Port getter (endian flip). */
		u16 Port() const;
		/** IP getter (endian flip). */
		u32 IP() const;
		/**  */
		String IP2String() const;
		/**  */
		String Port2String() const;
		/** Ip4 in string form XXX.XXX.XXX.XXX:YYYYY. */
		String ToString() const;

		/** Constructor. */
		IPv4() : _port(), _ip() {}
	};
}
