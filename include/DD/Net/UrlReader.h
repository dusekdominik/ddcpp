#pragma once
namespace DD::Net {
	/** Sequencer for reading char by char form given url.  */
	struct UrlReader;
}

#include <DD/String.h>
#include <DD/Types.h>
#include <DD/Net/UrlBlockReader.h>

namespace DD::Net {
	struct UrlReader {
	protected: // properties
		/** Inner sequencer. */
		UrlBlockReader _blockReader;
		/** Index in the block */
		size_t _index = 0;

	public: // sequencer interface
		/** Get currently loaded char. */
		constexpr u8 Get() { return _blockReader.Get()[_index]; }
		/** Move to the next char. */
		bool Next();
		/** Reset reading */
		bool Reset() { _index = 0; return _blockReader.Reset() && _blockReader.Get().Length(); }

	public: // constructors
		/** Constructor. */
		UrlReader(Text::StringView16 url, Text::StringView8 post = nullptr) : _blockReader(url, post) {}
	};
}
