#pragma once
namespace DD::Net {
	/**
	 * Simple UDP emitter.
	 * Example:
	 *  const char HW[] = "Hello world";
	 *  UDPEmitter emitter("12345");
	 *  emitter.Send(HW, sizeof(HW));
	 */
	struct UDPEmitter;
	/**
	 * Helper class for referential implementation.
	 */
	struct UDPEmitterBase;
}

#include <DD/Reference.h>
#include <DD/Net/SocketBase.h>

namespace DD::Net {
	struct UDPEmitterBase
		: protected SocketBase
	{
		using SocketBase::IPAddress;
		using SocketBase::IsValid;
		using SocketBase::Send;
		using SocketBase::State;

	public: // constructors
		UDPEmitterBase(Text::StringView16 host, Text::StringView16 port);
	};


	struct UDPEmitter
		: public Reference<UDPEmitterBase>
	{
		/** Zero constructor. */
		UDPEmitter() : Reference() {}
		/** Broadcaster. */
		UDPEmitter(u16 port) : Reference(new UDPEmitterBase(L"255.255.255.255", String::From(port))) {}
		/** Broadcaster. */
		UDPEmitter(Text::StringView16 port) : Reference(new UDPEmitterBase(L"255.255.255.255", port)) {}
		/** Constructor. */
		UDPEmitter(Text::StringView16 host, Text::StringView16 port) : Reference(new UDPEmitterBase(host, port)) {}
	};
}