#pragma once
namespace DD::Net {
	/**
	 * Simple UDP receiver.
	 * Example:
	 *  UDPReceiver receiver("12345");
	 *  receiver.OnMessageReceived += [](UniqueArray<char> & message, const IPv4 & sender) { prceed message };
	 *  receiver.StartReceiving();
	 */
	struct UDPReceiver;
	/**
	 * Helper class for referential implementation.
	 */
	struct UDPReceiverBase;
}

#include <DD/Concurrency/Thread.h>
#include <DD/Delegate.h>
#include <DD/Net/SocketBase.h>

namespace DD::Net {
	struct UDPReceiverBase
		: protected SocketBase
	{
		using SocketBase::IPAddress;
		using SocketBase::IsValid;
		using SocketBase::Receive;
		using SocketBase::ReceiveFrom;
		using SocketBase::State;

	protected: // properties
		/** Thread holder for async receiving. */
		Concurrency::Thread _worker;

	public: // properties
		/** Delegate for processing incoming messages. */
		Event<Array<u8> &, const IPv4 &> OnMessageReceived;
		/** Async receiving method. After enabling messages will be proceed via OnMessageReceived. */
		void StartReceiving();

	public: // constructors
		/** Constructor. */
		UDPReceiverBase(Text::StringView16 host, Text::StringView16 port);
	};

	struct UDPReceiver : public Reference<UDPReceiverBase> {
		/** Zero constructor. */
		UDPReceiver() : Reference() {}
		/** Receive from any address. */
		UDPReceiver(u16 port) : Reference(new UDPReceiverBase(L"0.0.0.0", String::From(port))) {}
		/** Receive from any address. */
		UDPReceiver(Text::StringView16 port) : Reference(new UDPReceiverBase(L"0.0.0.0", port)) {}
		/** Constructor. */
		UDPReceiver(Text::StringView16 host, Text::StringView16 port) : Reference(new UDPReceiverBase(host, port)) {}
	};
}
