#pragma once
namespace DD::Net {
	/**
	 * Sequencer for reading blocks from given url.
	 */
	struct UrlBlockReader;
}

#include <DD/Array.h>
#include <DD/String.h>
#include <DD/Types.h>

namespace DD::Net {
	struct UrlBlockReader {
	public: // nested
		/** Platform-Native handles. */
		struct NativeView;

	public: //statics
		/** Block size. */
		static constexpr size_t BUFFER_SIZE = 4096;

	protected: // properties
		/** Buffer for one block. */
		Array<u8, BUFFER_SIZE> _buffer;
		/** Url that should be opened. */
		StringLocal<256> _url;
		/** Native data for given platform. */
		u8 _native[16];

	protected: // methods
		/** Reinterpret as platform-native. */
		NativeView * Native();

	public: // sequencer interface
		/** Get currently loaded block. */
		constexpr Array<u8> & Get() { return _buffer; }
		/** Get next block. */
		bool Next();
		/** Reset reading. */
		bool Reset();

	private: // constructors
		/** Initialize native view; */
		UrlBlockReader();

	public: // constructors
		/** Constructor. */
		UrlBlockReader(const String & url, Text::StringView8 post = nullptr);
		/** Destructor - destruct native data. */
		~UrlBlockReader();
	};
}
