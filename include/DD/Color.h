#pragma once
#pragma warning( disable : 4201 )
namespace DD {
	/**
	 * RGBA color system.
	 * 32-bit structure.
	 * Red, Green, Blue, Alpha.
	 */
	struct Color;
	/**
	 * HSV color system.
	 * 32-bit structure.
	 * Hue, Saturation, Value.
	 */
	struct ColorHSV;
	/**
	 * Collection of basic colors.
	 * Use static instance Color::Collection.
	 */
	class ColorCollection;
	/**
	 * Enum of standard colors.
	 */
	class Colors;
}

#include "Enum.h"
#include <DD/Types.h>
#include "Vector.h"

#define DD_COLORS WHITE, BLACK, RED, GREEN, BLUE, YELLOW,\
	CYAN, MAGENTA, OLIVE, NAVY, VIOLET, PINK, BEIGE, BROWN,\
  GREY, LIGHT_BLUE, LIGHT_GREY, ORANGE, DARK_ORANGE, GOLD


namespace DD {
	DD_ENUM_32((Colors, "Predefined rgb colors."), DD_COLORS);

	struct ColorHSV {
	public: // constants
		/** Hue color constant. */
		constexpr static u16 HUE_RED = 0;
		/** Hue color constant. */
		constexpr static u16 HUE_MAGENTA = 60;
		/** Hue color constant. */
		constexpr static u16 HUE_GREEN = 120;
		/** Hue color constant. */
		constexpr static u16 HUE_CYAN = 180;
		/** Hue color constant. */
		constexpr static u16 HUE_BLUE = 240;
		/** Hue color constant. */
		constexpr static u16 HUE_YELLOW = 300;
		/** Hue color constant. */
		constexpr static u16 HUE_TOLERANCE = 90;

	public: // properties
		union {
			/** Components. */
			struct { u16 h; u8 s, v; };
			/** Aggregated value. */
			u32 u32;
		};

	public: // methods
		/** Check if color is green. */
		bool IsGreen() const;
	};


	struct Color {
	public: // statics
		/** Collection of basic colors. */
		typedef ColorCollection Collection;
		/** Constant for 8-bit unsigned int - 32 bit float conversion. */
		static constexpr f32 BYTE2FLOAT = 1.f / 255.f;
		/** Luminance factors (includes conversion from byte to float). */
		static constexpr f32 LFACTOR_R = BYTE2FLOAT * 0.2126f;
		static constexpr f32 LFACTOR_G = BYTE2FLOAT * 0.7152f;
		static constexpr f32 LFACTOR_B = BYTE2FLOAT * 0.0722f;

	protected: // static methods
		/** Saturate value between 0 - 1. */
		static f32 saturate(f32 f);
		/** Convert srgb-channel to float rgb channel. */
		static f32 srgb(f32 f);

	public: // static methods
		/** Convert float srgb to Color type. */
		static Color FromSRGB(f32 r, f32 g, f32 b);
		/** Get temperature scale. */
		static Color FromScale(f32 scale);

	public: // properties
		union {
			/** Unsigned aggregated form. */
			u32	AggU32;
			/** Signed aggregated form. */
			i32	AggI32;
			/** RGBA components. */
			struct { u8 r, g, b, a; };
		};

	public: // converters
		/** Conversion to unsigned integer. */
		operator const u32 &() const { return AggU32; }
		/** Conversion to float3 integer. */
		operator float3() const { return{ BYTE2FLOAT * r, BYTE2FLOAT * g, BYTE2FLOAT * b }; }
		/** Conversion to float4 integer. */
		operator float4() const { return{ BYTE2FLOAT * r, BYTE2FLOAT * g, BYTE2FLOAT * b, BYTE2FLOAT * a }; }
		/** Conversion to HSV color system. */
		ColorHSV ToHSV() const;
		/** Set alpha channel. */
		Color & SetA(u8 value) { a = value; return *this; }
		/** Set red channel. */
		Color & SetR(u8 value) { r = value; return *this; }
		/** Set green channel. */
		Color & SetG(u8 value) { g = value; return *this; }
		/** Set blue channel. */
		Color & SetB(u8 value) { b = value; return *this; }
		/** Set alpha channel. */
		Color SetA(u8 value) const { return Color(*this).SetA(value); }
		/** Set red channel. */
		Color SetR(u8 value) const { return Color(*this).SetR(value); }
		/** Set green channel. */
		Color SetG(u8 value) const { return Color(*this).SetG(value); }
		/** Set blue channel. */
		Color SetB(u8 value) const { return Color(*this).SetB(value); }
		/** Get luminance from the color. */
		f32 Luminance() const { return Math::Saturate(r * LFACTOR_R + g * LFACTOR_G + b * LFACTOR_B); }

	public: // constructors
		/** Default constructor. */
		constexpr Color() : AggI32() {}
		/** Aggregated color. */
		constexpr Color(u32 color) : AggU32(color) {}
		/** Aggregated color. */
		constexpr Color(i32 color) : AggI32(color) {}
		/** Shade of gray 0 - 255. */
		Color(u8 c);
		/** Shade of gray 0.f - 255.f. */
		Color(f32 c);
		/** Float RGB constructor. */
		Color(f32 r, f32 g, f32 b);
		/** Float RGBA constructor. */
		Color(f32 r, f32 g, f32 b, f32 a);
		/** 0-255 RGBA constructor. */
		Color(u8 r, u8 g, u8 b, u8 a = 255);
		/** RGB constructor. */
		Color(const float3 & color);
		/** RGBA constructor. */
		Color(const float4 & color);
		/** Color from enum. */
		Color(Colors::Values);
		/** Create color from index in color collection. */
		Color(Colors);
	};


	class ColorCollection {
	public: // constants
		static constexpr Color WHITE       = Color(0xFFFFFFFFU);
		static constexpr Color BLACK       = Color(0xFF000000U);
		static constexpr Color RED         = Color(0xFF0000FFU);
		static constexpr Color GREEN       = Color(0xFF00FF00U);
		static constexpr Color BLUE        = Color(0xFFFF0000U);
		static constexpr Color YELLOW      = Color(0xFF00FFFFU);
		static constexpr Color MAGENTA     = Color(0xFFFF00FFU);
		static constexpr Color CYAN        = Color(0xFFFFFF00U);
		static constexpr Color OLIVE       = Color(0xFF008080U);
		static constexpr Color NAVY        = Color(0xFF800000U);
		static constexpr Color VIOLET      = Color(0xFFEE82EEU);
		static constexpr Color PINK        = Color(0xFFCBC0FFU);
		static constexpr Color BEIGE       = Color(0xFFDCF5F5U);
		static constexpr Color BROWN       = Color(0xFF2A2AA5U);
		static constexpr Color GREY        = Color(0xFFAAAAAAU);
		static constexpr Color LIGHT_BLUE  = Color(0xFFFFCCAAU);
		static constexpr Color LIGHT_GREY  = Color(0xFFEEEEEEU);
		static constexpr Color ORANGE      = Color(0xFF00A5FFU);
		static constexpr Color DARK_ORANGE = Color(0xFF008CFFU);
		static constexpr Color GOLD        = Color(0xFF00D7FFU);
		static constexpr Color COLORS[] = { DD_COLORS };

	public:
		/** Safe (modular) indexer. */
		constexpr const Color & operator[](size_t i) const { return COLORS[i % Colors::Size()]; }
		/** Iterator. */
		constexpr const Color * begin() const { return COLORS; }
		/** Iterator. */
		constexpr const Color * end() const { return COLORS + Colors::Size(); }

	public: // constructors
		/** Zero constructor. */
		constexpr ColorCollection() {}
	};
}