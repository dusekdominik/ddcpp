#pragma once
namespace DD {
	/**
	 * Linked list with block allocation.
	 * List allocates blocks of memory.
	 * @param T           - base-type of list.
	 * @param BLOCK_SIZE  - defines size of one block as number of items
	 *                    - of type T which can be stored in the block.
	 * @param LOCAL       - if set, first block will be allocated locally,
	 *                    - it could break the copy constructor.
	 */
	template<typename T, size_t BLOCK_SIZE = 256, bool LOCAL = 1>
	class BlockChain;
}

#include <DD/Polymorph.h>
#include <DD/Unique.h>

namespace DD {
	template<typename T, size_t BLOCK_SIZE, bool LOCAL>
	class BlockChain {
		/** Check of minimal size of a block. */
		static_assert(BLOCK_SIZE, "Size of block must be positive value.");

	public: // nested
		/** Implementation of sequencer. */
		template<typename LIST, typename BLOCK, typename TYPE>
		struct Sequencers;
		/** One block structure. */
		struct Block;

	protected: // properties
		/** First block deallocator. */
		Polymorph<Block, LOCAL> _firstBlock;
		/** Pointers to the beginning and the end of the list. */
		Block *_tail, *_head;
		/** Number of stored items in the list. */
		size_t _size;

	protected: // methods
		/** Get block by block index. */
		Block * getBlock(size_t index);
		/** Get block by block index. */
		const Block * getBlock(size_t index) const;

	public: // methods
		/** Indexer. */
		T & operator[](size_t index);
		/** Indexer. */
		const T & operator[](size_t index) const;
		/** Get first item of the list. */
		T & First() { return _head->At(0); }
		/** Get first item of the list. */
		const T & First() const { return _head->At(0); }
		/** Get last item of the list. */
		T & Last() { return _tail->At(_tail->_size - 1); }
		/** Get last item of the list. */
		const T & Last() const { return _tail->At(_tail->_size - 1); }
		/** Sequencer. */
		auto Sequencer() const { return Sequencers<const BlockChain, const Block, const T>(this); }
		/** Sequencer. */
		auto Sequencer() { return Sequencers<BlockChain, Block, T>(this); }
		/** Call destructor on all the stored items and reset size of the list to zero. */
		void Free();
		/**
		 * Add an item - without check validity of a block.
		 * @warning    - if you want to pass references into the method,
		 *             - explicitly enumerate them i the template list.
		 * @param args - arguments suited for constructor of the type.
		 */
		template<typename...ARGS>
		void UnsafeAdd(ARGS...args);
		/**
		 * Add an item - without check validity of a block.
		 * @warning    - if you want to pass references into the method,
		 *             - explicitly enumerate them in the template list.
		 * @param args - arguments suited for constructor of the type.
		 */
		template<typename...ARGS>
		void Add(ARGS...args);
		/** Add an item using copy constructor. */
		void Add(const T & item) { Add<const T &>(item); }
		/** Add an item using move constructor. */
		void Add(T && item) { Add<T &&>(static_cast<T &&>(item)); }

	public: // constructors
		/** Constructor. */
		BlockChain();
		/** Destructor. */
		~BlockChain() { Free(); }
	};


	template<typename T, size_t S, bool L>
	struct BlockChain<T, S, L>::Block {
		/** Size allocated for storing the items. */
		char _allocation[sizeof(T) * S];
		/** Number of stored items in the block. */
		size_t _size;
		/** Pointer to the next block. */
		Unique<Block> _next;

		/** Indexer. */
		T * At(size_t index);
		/** Indexer. */
		const T * At(size_t index) const;
		/** Call destructor on all the stored items and reset size. */
		void Free() { while (_size) At(--_size)->~T(); }

		/** COnstructor. */
		Block() : _size(0), _next(0) {}
		/** Destructor. */
		~Block() { Free(); }
	};


	template<typename T, size_t S, bool L>
	template<typename LIST, typename BLOCK, typename TYPE>
	struct BlockChain<T, S, L>::Sequencers {
	protected: // properties
		/** Owner of chain. */
		LIST * _list;
		/** Currently scanned block. */
		BLOCK * _block;
		/** Index in currently scanned block. */
		size_t _index;

	public: // ISequencer
		TYPE & Get() { return *_block->At(_index % S); }
		bool Next();
		bool Reset();

	public: // constructors
		/** Constructor. */
		Sequencers(LIST * list) : _list(list) {}
	};
}


namespace DD {
	/************************************************************************/
	/* Block                                                                */
	/************************************************************************/
	template<typename T, size_t S, bool L>
	T * BlockChain<T, S, L>::Block::At(size_t index) {
		return (T *)(_allocation + (sizeof(T) * index));
	}


	template<typename T, size_t S, bool L>
	const T * BlockChain<T, S, L>::Block::At(size_t index) const {
		return (const T *)(_allocation + (sizeof(T) * index));
	}


	/************************************************************************/
	/* Sequencer                                                            */
	/************************************************************************/
	template<typename T, size_t S, bool L>
	template<typename LIST, typename BLOCK, typename TYPE>
	bool BlockChain<T, S, L>::Sequencers<LIST, BLOCK, TYPE>::Next() {
		size_t index = ++_index % S;
		// no need for block update
		if (index != 0)
			return index < _block->_size;

		// no further block
		if (_block->_next == nullptr)
			return false;

		// block update
		_block = _block->_next;
		return index < _block->_size;
	}


	template<typename T, size_t S, bool L>
	template<typename LIST, typename BLOCK, typename TYPE>
	bool BlockChain<T, S, L>::Sequencers<LIST, BLOCK, TYPE>::Reset() {
		_block = _list->_head;
		_index = 0;

		return _block != nullptr && _block->_size;
	}


	/************************************************************************/
	/* List                                                                 */
	/************************************************************************/
	template<typename T, size_t S, bool L>
	void BlockChain<T, S, L>::Free() {
		// Free all blocks
		for (Block * block = _head; block; block = block->_next)
			block->Free();
		// reset size
		_size = 0;
		// reset tail
		_tail = _head;
	}


	template<typename T, size_t S, bool L>
	typename BlockChain<T, S, L>::Block * BlockChain<T, S, L>::getBlock(size_t index) {
		// start from the head of the list
		Block * block = _head;
		// count blocks linearly (linked list)
		while (index--)
			block = block->_next;

		return block;
	}


	template<typename T, size_t S, bool L>
	const typename BlockChain<T, S, L>::Block * BlockChain<T, S, L>::getBlock(size_t index) const {
		// start from the head of the list
		const Block * block = _head;
		// count blocks linearly (linked list)
		while (index--)
			block = block->_next;

		return block;
	}


	template<typename T, size_t S, bool L>
	template<typename ...ARGS>
	void BlockChain<T, S, L>::UnsafeAdd(ARGS ...args) {
		// place new data
		new (_tail->At(_tail->_size++)) T(args...);
		// update size
		++_size;
	}


	template<typename T, size_t S, bool L>
	template<typename ...ARGS>
	void BlockChain<T, S, L>::Add(ARGS ...args) {
		// if number of stored items has been exceeded reserve new by creating new block
		if (_tail->_size == S) {
			// block could be already created and abandoned (by removing items)
			if (_tail->_next == nullptr)
				_tail->_next = new Block;

			// move pointer to current tail
			_tail = _tail->_next;
		}

		// place
		UnsafeAdd<ARGS...>(static_cast<ARGS>(args)...);
	}


	template<typename T, size_t S, bool L>
	T & BlockChain<T, S, L>::operator[](size_t index) {
		size_t blockIndex = (index + 1) / S;
		size_t itemIndex = index % S;
		return *getBlock(blockIndex)->At(itemIndex);
	}


	template<typename T, size_t S, bool L>
	const T & BlockChain<T, S, L>::operator[](size_t index) const {
		size_t blockIndex = (index + 1) / S;
		size_t itemIndex = index % S;
		return *getBlock(blockIndex)->At(itemIndex);
	}


	template<typename T, size_t S, bool L>
	BlockChain<T, S, L>::BlockChain()
		: _firstBlock()
		, _head(_firstBlock.Ptr())
		, _tail(_firstBlock.Ptr())
		, _size(0)
	{}
}
