#pragma once
namespace DD {
	/**
	 * Simple perceptron.
	 * https://en.wikipedia.org/wiki/Perceptron 
	 *
	 * Example:
	 *
	 * typedef Perceptron<float, 2> PerceptronF;
	 * typedef PerceptronF::pairs_t Pairs;
	 * typedef PerceptronF::Pair Pair;
	 * PerceptronF perceptron;
	 * perceptron.Train(
	 * 	 Pairs::Generate(
	 *     Pair({ float2(1.6f, 2.0f), 1 }),
	 *     Pair({ float2(2.0f, 2.3f), 1 }),
	 *     Pair({ float2(1.6f, 2.9f), -1 }),
	 *     Pair({ float2(1.9f, 3.1f), -1 })
	 *   )
	 * );
	 */
	template<typename T, size_t D>
	class Perceptron;
}

#include "Vector.h"
#include "Array.h"

namespace DD {
	template<typename T, size_t D>
	class Perceptron {
	public: // nested
		/** Input definition. */
		typedef Vector<T, D> vector_t;
		/** Pair of input and its classification {-1, 1}. */
		struct Pair { vector_t Input; int Output; };
		/** Training set. */
		typedef Array<Pair> pairs_t;
	public: // statics
		/** Sets the maximum iteration of training algorithm. */
		constexpr static T MAXIMUM_ITERATIONS = 1000;
	private: // statics
		/** Signum method {-1, 0 , 1}. */
		static int sgn(const T & t);
	public: // properties
		/** Weights. */
		vector_t Weights;
		/** Bias. */
		T Bias;
		/** Learning params. */
		T LearningParam;
	public:
		/** Classify vector. Returns {-1, 1}. */
		template<typename M>
		int Classify(const Vector<T, D, M> & input) const;
		/** Check pair si classified incorrectly. */
		bool Error(const Pair & pair) const;
		/** Returns true if any input pair is classified incorrectly. */
		bool Error(const pairs_t & pairs) const;
		/** Update weights and bias according to given pair. */
		void Train(const Pair & p);
		/** Train until given set is not classified correctly or until MAXIMUM_ITERATIONS is not exceeded. */
		bool Train(const pairs_t & pairs);
		/** Zero constructor. */
		Perceptron();
	};
}

namespace DD {
	/** Signum method {-1, 0 , 1}. */
	template<typename T, size_t D>
	inline int Perceptron<T, D>::sgn(const T & t) {
		return ((T(0) < t) - (t < T(0)));
	}

	/** Check pair si classified incorrectly. */
	template<typename T, size_t D>
	inline bool Perceptron<T, D>::Error(const Pair & pair) const {
		return Classify(pair.Input) != pair.Output;
	}

	/** Returns true if any input pair is classified incorrectly. */
	template<typename T, size_t D>
	inline bool Perceptron<T, D>::Error(const pairs_t & pairs) const {
		for (const Pair & pair : pairs) {
			if (Error(pair)) {
				return true;
			}
		}
		return false;
	}

	/** Update weights and bias according to given pair. */
	template<typename T, size_t D>
	inline void Perceptron<T, D>::Train(const Pair & p) {
		if (Error(p)) {
			Weights += p.Input * (LearningParam * p.Output);
			Bias += (LearningParam * p.Output);
		}
	}

	/** Train until given set is not classified correctly or until MAXIMUM_ITERATIONS is not exceeded. */
	template<typename T, size_t D>
	inline bool Perceptron<T, D>::Train(const pairs_t & pairs) {
		for (size_t i = 0; i < MAXIMUM_ITERATIONS && Error(pairs); ++i) {
			for (const Pair & pair : pairs) {
				Train(pair);
			}
		}
		return !Error(pairs);
	}

	/** Zero constructor. */
	template<typename T, size_t D>
	inline Perceptron<T, D>::Perceptron()
		: Weights(vector_t::Zero)
		, Bias(0)
		, LearningParam(1)
	{}

	/** Classify vector. Returns {-1, 1}. */
	template<typename T, size_t D>
	template<typename M>
	inline int Perceptron<T, D>::Classify(const Vector<T, D, M>& input) const {
		return sgn(dot(Weights, input) + Bias);
	}
}