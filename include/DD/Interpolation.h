#pragma once
namespace DD::Interpolation {
	enum FlagSave { Save };
	/**
	 * Normalized linear interpolation for coordinates
	 * between (0.0, 0.0) - (1.0, 1.0)
	 * _01 = f(0, 1) +---+ _11 = f(1, 1)
	 *               |   |
	 * _00 = f(0, 0) +---+ _10 = f(1, 0)
	 *
	 * @example common usage
	 *   DD::Interpolation::NormalizedBilinearD interpolation(
	 *     1.0, // value at (0.0, 0.0)
	 *     2.0, // value at (0.0, 1.0)
	 *     3.0, // value at (1.0, 0.0)
	 *     4.0  // value at (1.0, 1.0)
	 *   );
	 *   auto i0 = interpolation(0.5, 0.5);
	 *   auto i1 = interpolation(0.75, 0.25);
	 */
	template<typename NUMBER>
	struct NormalizedBilinear;
	using NormalizedBilinearF = NormalizedBilinear<float>;
	using NormalizedBilinearD = NormalizedBilinear<double>;

	/**
	 * Bilinear interpolation for defined interval.
	 * _01 = f(_x0, _y1) +---+ _11 = f(_x1, _y1)
	 *                   |   |
	 * _00 = f(_x0, _y0) +---+ _10 = f(_x1, _y0)
	 *
	 * @example common usage
	 *   DD::Interpolation::BilinearD interpolation(
	 *     1.0,   // value at (100.0, 50.0)
	 *     2.0,   // value at (100.0, 150.0)
	 *     3.0,   // value at (200.0, 50.0)
	 *     4.0,   // value at (200.0, 150.0)
	 *     100.0, // begin at x
	 *     200.0, // end at x
	 *     50.0,  // begin at y
	 *     150.0  // end at y
	 *   );
	 *   auto i0 = interpolation(150.0, 100.0);
	 *   auto i1 = interpolation(125.0, 111.1);
	 */
	template<typename NUMBER>
	struct Bilinear;
	using BilinearF = Bilinear<float>;
	using BilinearD = Bilinear<double>;

	/**
	 *
	 * @example common usage
	 *   DD::Interpolation::BilinearGridD grid(
	 *     0.0, // begin at x
	 *     1.0, // end at x
	 *     10,  // number of interpolation points at x
	 *     0.0, // begin at y
	 *     1.0, // end at y
	 *     10   // number of interpolation points at y
	 *   );
	 *   // fill the grid by function values
	 *   for (size_t y = 0; y < 10; ++y) for (size_t x = 0; x < 10; ++x) {
	 *     grid[y][x] = DD::Random::Float64();
	 *   }
	 *   // get interpolation
	 *   auto i0 = interpolation(0.5, 0.5);
	 *   auto i1 = interpolation(0.75, 0.25);
	 */
	template<typename NUMBER>
	struct BilinearGrid;
	using BilinearGridF = BilinearGrid<float>;
	using BilinearGridD = BilinearGrid<double>;
}

#include <DD/Array.h>
#include <DD/Vector.h>

namespace DD::Interpolation {
	template<typename NUMBER>
	struct NormalizedBilinear {
		/** Values at square corners.*/
		NUMBER _00, _01, _10, _11;
		/**
		 * Functor for processing interpolation.
		 * @param x - x-coordinate, should be between 0.0 and 1.0
		 * @param y - y-coordinate, should be between 0.0 and 1.0
		 * @return interpolation result.
		 */
		NUMBER operator()(NUMBER x, NUMBER y) const;
		/** Call interpolation of vector. */
		NUMBER operator()(DD::V2<NUMBER> coords) const { return operator()(coords.x, coords.y); }
		/**
		 * Constructor.
		 * @param f00 - function value at (0.0, 0.0)
		 * @param f01 - function value at (0.0, 1.0)
		 * @param f10 - function value at (1.0, 0.0)
		 * @param f11 - function value at (1.0, 1.0)
		 */
		NormalizedBilinear(NUMBER f00, NUMBER f01, NUMBER f10, NUMBER f11);
	};

	template<typename NUMBER>
	struct Bilinear {
		/** Coordinates. */
		NUMBER _x0, _x1, _y0, _y1;
		/** Values at square corners. */
		NUMBER _00, _01, _10, _11;
		/** Inv of surface for normalization result. */
		NUMBER _squareInv;
		/**
		 * Functor for processing interpolation.
		 * @param x - x-coordinate, should be between _x0 and _x1
		 * @param y - y-coordinate, should be between _y0 and _y1
		 * @return interpolation result.
		 */
		NUMBER operator()(NUMBER x, NUMBER y) const;
		/** Call interpolation of vector. */
		NUMBER operator()(const V2<NUMBER> & coords) const { return operator()(coords.x, coords.y); }
		/**
		 * Constructor.
		 * @param f00 - function value at x0, y0
		 * @param f01 - function value at x0, y1
		 * @param f10 - function value at x1, y0
		 * @param f11 - function value at x1, y1
		 * @param x0 - begin of interpolation at x-coords
		 * @param x1 - end of interpolation at x-coords
		 * @param y0 - begin of interpolation at y-coords
		 * @param y1 - end of interpolation at y-coords
		 */
		Bilinear(NUMBER f00, NUMBER f01, NUMBER f10, NUMBER f11, NUMBER x0, NUMBER x1, NUMBER y0, NUMBER y1);
	};

	template<typename NUMBER>
	struct BilinearGrid {
		/** Begin and end of coordinate system. */
		NUMBER _x0, _x1, _y0, _y1;
		/** Dimensions of one interpolations square. */
		NUMBER _xd, _yd;
		/** Inverted dimensions of one square. */
		NUMBER _xdi, _ydi;
		/** Inversion of surface of one square . */
		NUMBER _squareInv;
		/** Grid with function values. */
		Array2D<NUMBER> _grid;
		/** Get row (y-axis) of grid of function values. */
		ArrayView1D<NUMBER> operator[](size_t index) { return _grid[index]; }
		/**
		 * Functor for processing interpolation.
		 * @param x - x-coordinate, should be between _x0 and _x1
		 * @param y - y-coordinate, should be between _y0 and _y1
		 * @return interpolation result.
		 */
		NUMBER operator()(NUMBER x, NUMBER y) const;
		/** Call interpolation of vector. */
		NUMBER operator()(const V2<NUMBER> & coord) const { return operator()(coord.x, coord.y); }
		/**
		 * Constructor.
		 * @param x0   - begin of interpolation at x-coords
		 * @param x1   - end of interpolation at x-coords
		 * @param xres - number of interpolation points
		 * @param y0   - begin of interpolation at y-coords
		 * @param y1   - end of interpolation at y-coords
		 * @param yres - number of interpolation points
		 */
		BilinearGrid(NUMBER x0, NUMBER x1, size_t xres, NUMBER y0, NUMBER y1, size_t yres);
		/** Zero constructor. */
		BilinearGrid() {}
	};
}

namespace DD::Interpolation {
	/************************************************************************/
	/* NormalizedBilinear                                                   */
	/************************************************************************/
	template<typename NUMBER>
	NUMBER NormalizedBilinear<NUMBER>::operator()(NUMBER x, NUMBER y) const {
		const NUMBER nx(NUMBER(1) - x);
		const NUMBER ny(NUMBER(1) - y);

		return nx * ny * _00 + x * ny * _10 + nx * y * _01 + x * y * _11;
	}

	template<typename NUMBER>
	NormalizedBilinear<NUMBER>::NormalizedBilinear(NUMBER f00, NUMBER f01, NUMBER f10, NUMBER f11)
		: _00(f00)
		, _01(f01)
		, _10(f10)
		, _11(f11)
	{}

	/************************************************************************/
	/* Bilinear                                                             */
	/************************************************************************/
	template<typename NUMBER>
	NUMBER Bilinear<NUMBER>::operator()(NUMBER x, NUMBER y) const {
		const NUMBER nx(_x1 - x);
		const NUMBER ny(_y1 - y);
		const NUMBER px(x - _x0);
		const NUMBER py(y - _y0);

		return (nx * ny * _00 + px * ny * _10 + nx * py * _01 + px * py * _11) * _squareInv;
	}

	template<typename NUMBER>
	Bilinear<NUMBER>::Bilinear(
		NUMBER f00,
		NUMBER f01,
		NUMBER f10,
		NUMBER f11,
		NUMBER x0,
		NUMBER x1,
		NUMBER y0,
		NUMBER y1
	) : _00(f00)
		, _01(f01)
		, _10(f10)
		, _11(f11)
		, _x0(x0)
		, _x1(x1)
		, _y0(y0)
		, _y1(y1)
		, _squareInv(NUMBER(1) / ((x1 - x0) * (y1 - y0)))
	{}

	/************************************************************************/
	/* BilinearGrid                                                         */
	/************************************************************************/
	template<typename NUMBER>
	NUMBER BilinearGrid<NUMBER>::operator()(NUMBER x, NUMBER y) const {
		const size_t xIndex = Math::Saturate((size_t)((x - _x0) * _xdi), 0, _grid.Lengths()[0] - 1);
		const size_t yIndex = Math::Saturate((size_t)((y - _y0) * _ydi), 0, _grid.Lengths()[1] - 1);
		const NUMBER * y0 = _grid[xIndex].Ptr();
		const NUMBER * y1 = _grid[xIndex + 1].Ptr();
		const NUMBER & _00 = y0[yIndex];
		const NUMBER & _01 = y0[yIndex + 1];
		const NUMBER & _10 = y1[yIndex];
		const NUMBER & _11 = y1[yIndex + 1];
		const NUMBER xb = _x0 + (xIndex)* _xd;
		const NUMBER xe = xb + _xd;
		const NUMBER yb = _y0 + (yIndex)* _yd;
		const NUMBER ye = yb + _yd;
		const NUMBER nx(xe - x);
		const NUMBER ny(ye - y);
		const NUMBER px(x - xb);
		const NUMBER py(y - yb);

		return (nx * ny * _00 + px * ny * _10 + nx * py * _01 + px * py * _11) * _squareInv;
	}

	template<typename NUMBER>
	BilinearGrid<NUMBER>::BilinearGrid(NUMBER x0, NUMBER x1, size_t xres, NUMBER y0, NUMBER y1, size_t yres)
		: _x0(x0)
		, _x1(x1)
		, _xd((x1 - x0) / NUMBER(xres))
		, _xdi(NUMBER(xres) / (x1 - x0))
		, _y0(y0)
		, _y1(y1)
		, _yd((y1 - y0) / NUMBER(yres))
		, _ydi(NUMBER(yres) / (y1 - y0))
		, _grid({ xres + 1, yres + 1 })
		, _squareInv(NUMBER(1) / (((x1 - x0) / xres) * (y1 - y0) / yres))
	{}
}