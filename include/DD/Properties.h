#pragma once
#include "Macros.h"
#include "XML.h"

/** Compile time hash for property name. */
#define _DD_PROPERTY_XML_HASH_CT(string) ::DD::Text::StringView(string).Hash()
/** Runtime hash for property name. */
//#define _DD_PROPERTY_XML_HASH_RT(string) ::DD::Text::StringView(string).Hash()
/** Extractor - name of property in native type. */
#define _DD_PROPERTY_NAME(...) _DD_OVERLOAD(_DD_PROPERTY_NAME, __VA_ARGS__)
#define _DD_PROPERTY_NAME_1(var) var
#define _DD_PROPERTY_NAME_2(var, name) name
#define _DD_PROPERTY_NAME_3(type, var, name, ...) name
/** Extractor - type of property. */
#define _DD_PROPERTY_TYPE(...) _DD_OVERLOAD(_DD_PROPERTY_TYPE, __VA_ARGS__)
#define _DD_PROPERTY_TYPE_1(var) decltype(var)
#define _DD_PROPERTY_TYPE_2(var, name) decltype(var)
#define _DD_PROPERTY_TYPE_3(type, var, name, ...) type
/** Extractor - name of property in string type. */
#define _DD_PROPERTY_STRING(...) DD_SCAT_16(_DD_PROPERTY_NAME(__VA_ARGS__))
/** Extractor - Initial xml tag of property. */
#define _DD_PROPERTY_XML_BEG(...) DD_SCAT_16(<, _DD_PROPERTY_NAME(__VA_ARGS__), >)
/** Extractor - Terminal xml tag of property. */
#define _DD_PROPERTY_XML_END(...) DD_SCAT_16(</, _DD_PROPERTY_NAME(__VA_ARGS__), >)
/** Extractor - name of property getter. */
#define _DD_PROPERTY_GETTER_NAME(...) _DD_OVERLOAD(_DD_PROPERTY_GETTER_NAME, __VA_ARGS__)
#define _DD_PROPERTY_GETTER_NAME_1(var) Get##var
#define _DD_PROPERTY_GETTER_NAME_2(var, name) name
#define _DD_PROPERTY_GETTER_NAME_3(type, var, name, ...) name
/** Extractor - name of property setter. */
#define _DD_PROPERTY_SETTER_NAME(...) _DD_OVERLOAD(_DD_PROPERTY_SETTER_NAME, __VA_ARGS__)
#define _DD_PROPERTY_SETTER_NAME_1(var) Set##var
#define _DD_PROPERTY_SETTER_NAME_2(var, name) name
#define _DD_PROPERTY_SETTER_NAME_3(type, var, name, ...) name
/** Extractor - name of storage variable. */
#define _DD_PROPERTY_VARIABLE(...) _DD_OVERLOAD(_DD_PROPERTY_VARIABLE, __VA_ARGS__)
#define _DD_PROPERTY_VARIABLE_1(var) DD_UNWRAP(var)
#define _DD_PROPERTY_VARIABLE_2(var, name) DD_UNWRAP(var)
#define _DD_PROPERTY_VARIABLE_3(type, var, name) DD_UNWRAP(var)
/** Extractor - declaration of storage variable (default or nothing). */
#define _DD_PROPERTY_DECLARATION(...) __DD_OVERLOAD(_DD_PROPERTY_DECLARATION, __VA_ARGS__)
#define _DD_PROPERTY_DECLARATION_1(...)
#define _DD_PROPERTY_DECLARATION_2(...)
#define _DD_PROPERTY_DECLARATION_3(...) _DD_PROPERTY_TYPE(__VA_ARGS__) _DD_PROPERTY_VARIABLE(__VA_ARGS__);
/** Extractor - Assigns value to a variable. */
#define _DD_PROPERTY_ASSIGN(...) ::DD::XML::DefaultSerializer::Assign(value, _DD_PROPERTY_VARIABLE(__VA_ARGS__));
/** Extractor - Case (part of a switch) label for a property. */
#define _DD_PROPERTY_OPTION(...) case (u64)Properties::_DD_PROPERTY_NAME(__VA_ARGS__):
/** Extractor - Inline definition of property setter. */
#define _DD_PROPERTY_GETTER(...) _DD_PROPERTY_TYPE(__VA_ARGS__) & _DD_PROPERTY_GETTER_NAME(__VA_ARGS__)() {return _DD_PROPERTY_VARIABLE(__VA_ARGS__);}
/** Extractor - Inline definition of property const getter. */
#define _DD_PROPERTY_CGETTER(...) const _DD_PROPERTY_TYPE(__VA_ARGS__) & _DD_PROPERTY_GETTER_NAME(__VA_ARGS__)() const {return _DD_PROPERTY_VARIABLE(__VA_ARGS__);}
/** Extractor - Inline definition of property setter. */
#define _DD_PROPERTY_SETTER(...) auto & _DD_PROPERTY_SETTER_NAME(__VA_ARGS__)(const _DD_PROPERTY_TYPE(__VA_ARGS__) & value) { _DD_PROPERTY_ASSIGN(__VA_ARGS__) return *this; }
/** Extractor - Body of enum.  */
#define _DD_PROPERTY_ENUM(...) _DD_PROPERTY_NAME(__VA_ARGS__) = _DD_PROPERTY_XML_HASH_CT(_DD_PROPERTY_STRING(__VA_ARGS__)),
/** Serialize property to xml. */
#define _DD_PROPERTY_TO_XML(...) ::DD::XML::Serialize(_DD_PROPERTY_VARIABLE(__VA_ARGS__), target.DefineElement(_DD_PROPERTY_STRING(__VA_ARGS__)));
/** Deserialize property from XML. */
#define _DD_PROPERTY_SET_XML(...) _DD_PROPERTY_OPTION(__VA_ARGS__) ::DD::XML::Deserialize(child, _DD_PROPERTY_VARIABLE(__VA_ARGS__)); break;
/** Visibility of setters. */
#define _DD_PROPERTY2_SETTER_PUBLIC(...)         public:     _DD_PROPERTY_SETTER(__VA_ARGS__)
#define _DD_PROPERTY2_SETTER_PROTECTED(...)      protected:  _DD_PROPERTY_SETTER(__VA_ARGS__)
#define _DD_PROPERTY2_SETTER_PRIVATE(...)
#define _DD_PROPERTY2_SETTER_PROTECTED_SET(...)  protected:  _DD_PROPERTY_SETTER(__VA_ARGS__)
#define _DD_PROPERTY2_SETTER_PRIVATE_SET(...)
/** Visibility of getters. */
#define _DD_PROPERTY2_GETTER_PUBLIC(...)         public:     _DD_PROPERTY_GETTER(__VA_ARGS__)
#define _DD_PROPERTY2_GETTER_PROTECTED(...)      protected:  _DD_PROPERTY_GETTER(__VA_ARGS__)
#define _DD_PROPERTY2_GETTER_PRIVATE(...)
#define _DD_PROPERTY2_GETTER_PROTECTED_SET(...)  protected:  _DD_PROPERTY_GETTER(__VA_ARGS__)
#define _DD_PROPERTY2_GETTER_PRIVATE_SET(...)
/** Visibility of constant getters. */
#define _DD_PROPERTY2_CGETTER_PUBLIC(...)        public:    _DD_PROPERTY_CGETTER(__VA_ARGS__)
#define _DD_PROPERTY2_CGETTER_PROTECTED(...)     protected: _DD_PROPERTY_CGETTER(__VA_ARGS__)
#define _DD_PROPERTY2_CGETTER_PRIVATE(...)
#define _DD_PROPERTY2_CGETTER_PROTECTED_SET(...) public:    _DD_PROPERTY_CGETTER(__VA_ARGS__)
#define _DD_PROPERTY2_CGETTER_PRIVATE_SET(...)   public:    _DD_PROPERTY_CGETTER(__VA_ARGS__)
/** Get unwrapped arguments. */
#define _DD_PROPERTY2_WRAP_PUBLIC(...)           __VA_ARGS__
#define _DD_PROPERTY2_WRAP_PROTECTED(...)        __VA_ARGS__
#define _DD_PROPERTY2_WRAP_PRIVATE(...)          __VA_ARGS__
#define _DD_PROPERTY2_WRAP_PROTECTED_SET(...)    __VA_ARGS__
#define _DD_PROPERTY2_WRAP_PRIVATE_SET(...)      __VA_ARGS__
/** Forwarders. */
#define _DD_PROPERTY2(method, visibility)        _DD_PROPERTY2_##method##_##visibility
#define _DD_PROPERTY2_FWD(fwd, ...)              _DD_PROPERTY_##fwd(_DD_PROPERTY2(WRAP, __VA_ARGS__))
#define _DD_PROPERTY2_FWD2(type, ...)            _DD_PROPERTY2(type, __VA_ARGS__)
#define _DD_PROPERTY2_SETTER(...)                _DD_PROPERTY2_FWD2(SETTER, __VA_ARGS__)
#define _DD_PROPERTY2_GETTER(...)                _DD_PROPERTY2_FWD2(GETTER, __VA_ARGS__)
#define _DD_PROPERTY2_CGETTER(...)               _DD_PROPERTY2_FWD2(CGETTER, __VA_ARGS__)
#define _DD_PROPERTY2_ENUM(...)                  _DD_PROPERTY2_FWD(ENUM, __VA_ARGS__)
#define _DD_PROPERTY2_DECLARATION(...)           _DD_PROPERTY2_FWD(DECLARATION, __VA_ARGS__)
#define _DD_PROPERTY2_TO_XML(...)                _DD_PROPERTY2_FWD(TO_XML, __VA_ARGS__)
#define _DD_PROPERTY2_SET_XML(...)               _DD_PROPERTY2_FWD(SET_XML, __VA_ARGS__)
/**
 * Property definition with standardized access and serialization methods.
 * DD_PROPERTIES(property1, property2, ..., propertyN)
 *
 * Property visibility:
 *   PUBLIC, PROTECTED, PRIVATE, PROTECTED_SET, PRIVATE_SET
 *
 * Property declaration:
 *   VISIBILITY(type, propertyName, getterName) - providing declaration
 *   VISIBILITY(propertyName, getterName) - not providing declaration
 *   VISIBILITY(propertyName) - not providing declaration, getter in GetName format
 *
 * @example Fast serialization implicitly using name of property
 * struct Example {
 *   char X, Y;
 *   DD_PROPERTIES(PRIVATE(X),PRIVATE(Y))
 * };
 *
 * @example Serialization using link to a variable
 * struct Example {
 *   union { int hash; char name[32]; } _data;
 *   DD_PROPERTIES(PUBLIC(_data.name, Name))
 * };
 *
 * @example Serialization including property definition
 * struct Example {
 *   typedef char string_t[32]; // static arrays
 *   DD_PROPERTIES(
 *     PUBLIC(string_t, _name, Name),
 *     PUBLIC(double, _value, Value),
 *     PUBLIC((pair<char, char>), _pair, PAIR)
 *   )
 */
#define DD_PROPERTIES(...)\
public: /* nested */\
	enum struct Properties : u64 { DD_XLIST(_DD_PROPERTY2_ENUM, __VA_ARGS__) };\
private: /* properties */\
	DD_XLIST(_DD_PROPERTY2_DECLARATION, __VA_ARGS__)\
	DD_XLIST(_DD_PROPERTY2_GETTER, __VA_ARGS__)\
	DD_XLIST(_DD_PROPERTY2_CGETTER, __VA_ARGS__)\
	DD_XLIST(_DD_PROPERTY2_SETTER, __VA_ARGS__)\
public: /* methods */ \
	void FromXml(const ::DD::XML::Element & source) { for (auto & child : source.Children) { switch (child.GetName().Hash()) { DD_XLIST(_DD_PROPERTY2_SET_XML, __VA_ARGS__) default: throw; } } }\
	void ToXml(::DD::XML::Element & target) const { DD_XLIST(_DD_PROPERTY2_TO_XML, __VA_ARGS__) }
